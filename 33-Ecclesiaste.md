# Qohelet (Ecclésiaste) (Ec.)

Signification : Prédicateur

Auteur : Shelomoh (Salomon)

Thème : Les raisonnements humains

Date de rédaction : 10ème siècle av. J.-C.

Ce livre, du fait de sa date de rédaction, est généralement attribué à Shelomoh en raison de l'allusion faite au premier chapitre et du style adopté. Le livre de Qohelet figure d'ailleurs dans le canon des livres reconnus d'inspiration divine.

La problématique centrale du livre est de savoir si la vie vaut la peine d'être vécue ou non. L'auteur y répondit en connaissance de cause car il avait obtenu tout ce que l'être humain pouvait désirer : les richesses, le luxe, la volupté, la sagesse... Sans pour autant incriminer Elohîm, il dressa le constat de ce qu'est l'expérience humaine. Selon lui, l'être humain vit dans un cycle d'éternels recommencements où tout n'est que vanité et tourment d'esprit.

## Chapitre 1

### Tout est vanité<!--Ec. 12:8.-->

1:1	Les paroles de Qohelet<!--Généralement traduit par Ecclésiaste.-->, fils de David, roi de Yeroushalaim.
1:2	Vanité des vanités, dit Qohelet, vanité des vanités, tout est vanité.

### Le cycle du temps

1:3	Quel avantage a l'être humain de tout son travail auquel il s'occupe sous le soleil ?<!--Ec. 2:22, 3:9.-->
1:4	Une génération passe et une autre génération vient, mais la Terre demeure toujours ferme.
1:5	Le soleil aussi se lève et le soleil se couche ; il soupire après le lieu d'où il se lève.
1:6	Le vent va vers le midi et il tourne vers le nord, le vent s'en va, tournant, tournant, et il revient sur ses circuits.
1:7	Tous les fleuves vont à la mer, mais la mer n'en est pas remplie. Les fleuves retournent au lieu d'où ils étaient partis pour revenir<!--Job 38:8-11 ; Ps. 104:9-10.--> à la mer.
1:8	Toutes les choses sont lassantes, et l'homme ne peut en parler. L'œil n'est jamais rassasié de voir<!--Pr. 27:20.--> et l'oreille ne se remplit pas de ce qu'elle entend.
1:9	Ce qui a été, c'est ce qui sera, et ce qui s'est fait, c'est ce qui se fera. Il n'y a rien de nouveau sous le soleil<!--Ec. 3:15.-->.
1:10	Y a-t-il quelque chose dont on puisse dire : Regarde cela, il est nouveau ? Il a déjà été dans les temps qui nous ont précédés.
1:11	On ne se souvient plus des choses d'autrefois. De même on ne se souviendra pas des choses à venir et ceux qui viendront n'en auront aucun souvenir.

### La sagesse des humains ne comble pas

1:12	Moi, Qohelet, j'ai été roi sur Israël à Yeroushalaim.
1:13	Et j'ai appliqué mon cœur à rechercher et à sonder par la sagesse tout ce qui se fait sous les cieux : c'est une occupation désagréable qu'Elohîm a donnée aux humains afin qu'ils s'y occupent<!--1 R. 4:30-34 ; Ec. 7:25.-->.
1:14	J'ai vu toutes les œuvres qui se font sous le soleil, et voici tout est vanité et tourment d'esprit.
1:15	Ce qui est courbé ne peut se redresser, et ce qui manque ne peut être compté.
1:16	J'ai parlé en mon cœur, disant : Voici, je suis devenu grand et j'ai surpassé en sagesse tous ceux qui ont été avant moi sur Yeroushalaim, et mon cœur a vu beaucoup de sagesse et de connaissance.
1:17	Et j'ai appliqué mon cœur à connaître la sagesse, et à connaître les sottises et la stupidité. J'ai reconnu que cela aussi était un tourment d'esprit.
1:18	Car là où il y a beaucoup de sagesse, il y a beaucoup de chagrin, et celui qui augmente sa connaissance, augmente sa douleur.

## Chapitre 2

### Les richesses ne comblent pas

2:1	Je me suis dit dans mon cœur : Allons, que je t'éprouve maintenant par la joie, recherche le bonheur ! Et voici, c'est encore une vanité<!--Lu. 12:19.-->.
2:2	J'ai dit concernant le rire : Il est insensé ! Et concernant la joie : À quoi sert-elle ?
2:3	J'ai recherché en mon cœur le moyen de livrer ma chair au vin, tout en me conduisant avec sagesse, et de m'attacher à la folie jusqu'à ce que je voie ce qu'il est bon pour les humains de faire sous les cieux pendant les jours de leur vie.
2:4	J'ai fait de grandes œuvres. Je me suis bâti des maisons. Je me suis planté des vignes.
2:5	Je me suis fait des jardins et des vergers, et j'y plantai des arbres fruitiers de toutes sortes.
2:6	Je me suis fait des réservoirs d'eau pour arroser la forêt où poussent les arbres.
2:7	J'ai acquis des hommes et des femmes esclaves, et j'ai eu des esclaves nés dans ma maison. Et j'ai eu plus de gros et de menu bétail que tous ceux qui ont été avant moi dans Yeroushalaim.
2:8	Je me suis aussi amassé de l'argent et de l'or, et des propriétés<!--Le mot hébreu signifie aussi « possession », « richesses », « trésor », etc. Voir Ex. 19:5 ; De. 7:6, 14:2, 26:18 ; Ps. 135:4 ; Mal. 3:17.--> qui se trouvent chez les rois et dans les provinces<!--1 R. 9:28, 10:10 ; 2 Ch. 1:15.-->. Je me suis acquis des chanteurs et des chanteuses, et les délices des humains, une concubine, des concubines<!--Certains traducteurs ont traduit la deuxième partie de ce verset par les termes « instruments de musique », or le sens du terme hébreu « shiddah » est incertain. D'autres l'ont traduit par « concubine », « harem » ou « épouse ».-->.
2:9	Je me suis agrandi et me suis accru plus que tous ceux qui ont été avant moi dans Yeroushalaim. Et ma sagesse demeura avec moi.
2:10	Enfin je n'ai rien refusé à mes yeux de tout ce qu'ils ont demandé, et je n'ai épargné aucune joie à mon cœur. Car mon cœur s'est réjoui de tout mon travail, et c'est là tout ce que j'ai eu de tout mon travail.
2:11	Mais ayant considéré toutes mes œuvres que mes mains avaient faites, et tout le travail auquel je m'étais occupé en les faisant, et voici, tout est tellement vanité et tourment d'esprit, que l'homme n'a aucun avantage à retirer de ce qu'on fait sous le soleil.

### Le sage et l'insensé ont le même sort

2:12	Puis je me suis mis à considérer tant la sagesse que les sottises et la folie, (En effet, que fera l'être humain qui viendra après le roi ? N'est-ce pas ce qu'on a déjà fait ?).
2:13	Et j'ai vu que la sagesse a beaucoup d'avantages sur la folie comme la lumière a beaucoup d'avantages sur les ténèbres.
2:14	Le sage a ses yeux à sa tête, et l'insensé marche dans les ténèbres. Mais j'ai aussi reconnu qu'un même accident<!--Le mot hébreu traduit par « accident » signifie aussi « événement non prévu », « destin » etc.--> leur arrive à tous<!--Ps. 49:11 ; Ec. 3:17, 9:2.-->.
2:15	C'est pourquoi je me suis dit dans mon cœur : L'accident qui arrive à l'insensé m'arrivera aussi. À quoi m'a-t-il donc servi d'être plus sage que lui ? C'est pourquoi je me suis dit dans mon cœur : Cela aussi est une vanité.
2:16	Car le souvenir du sage n'est pas plus éternel que celui de l'insensé, parce que ce qui est maintenant va être oublié dans les jours qui suivent. Le sage meurt aussi bien que l'insensé<!--Ec. 8:10, 9:5.--> !
2:17	C'est pourquoi j'ai haï cette vie, car les choses qui se sont faites sous le soleil m'ont déplu, car tout est vanité et tourment d'esprit.
2:18	J'ai aussi haï tout mon travail auquel je me suis occupé sous le soleil parce que je le laisserai à l'être humain qui viendra après moi<!--Ec. 4:8.-->.
2:19	Et qui sait s'il sera sage ou insensé ? Cependant il sera maître de tout mon travail auquel je me suis occupé et de ce en quoi j'ai été sage sous le soleil. Cela aussi est une vanité.
2:20	C'est pourquoi j'ai fait en sorte que mon cœur perde toute espérance de tout le travail auquel je m'étais occupé sous le soleil.
2:21	Car tel être humain a travaillé avec sagesse et adresse, et avec succès, mais c'est à un autre être humain qui n'y a pas travaillé que sera donnée sa part. Cela aussi est une vanité et un grand mal.
2:22	Car que revient-il à l'être humain de tout son travail et du désir de son cœur pour lequel il souffre sous le soleil ?
2:23	Car tous ses jours ne sont que douleur et son occupation n'est que chagrin. Même la nuit son cœur ne se repose pas. Cela aussi est une vanité<!--Ps. 90:9 ; Job 14:1.-->.
2:24	N'est-ce donc pas un bonheur pour l'être humain de manger, de boire et de faire jouir son âme du bonheur au milieu de son travail ? J'ai vu aussi que cela vient de la main d'Elohîm<!--Ec. 3:12,22, 5:18, 8:15.-->.
2:25	Car qui donc peut manger et éprouver du plaisir en dehors de moi ?
2:26	Car Elohîm donne à l'être humain qui lui est agréable, de la sagesse, de la connaissance et de la joie, mais au pécheur, il donne pour occupation de recueillir et d'amasser afin de donner à celui qui est agréable à Elohîm<!--Pr. 28:8.-->. Cela aussi est une vanité et un tourment d'esprit<!--Pr. 13:22, 28:8 ; Job 27:17.-->.

## Chapitre 3

### Il y a un temps pour toute chose

3:1	À toute chose sa saison et à tout désir sous les cieux son temps :
3:2	il y a un temps pour naître et un temps pour mourir, un temps pour planter et un temps pour arracher ce qui est planté,
3:3	un temps pour tuer et un temps pour guérir, un temps pour abattre et un temps pour bâtir,
3:4	un temps pour pleurer et un temps pour rire, un temps pour se lamenter et un temps pour sauter de joie,
3:5	un temps pour jeter des pierres et un temps pour ramasser des pierres, un temps pour embrasser et un temps pour s'éloigner des embrassements,
3:6	un temps pour chercher et un temps pour perdre, un temps pour garder et un temps pour jeter,
3:7	un temps pour déchirer et un temps pour coudre, un temps pour être silencieux et un temps pour parler,
3:8	un temps pour aimer et un temps pour haïr, un temps pour la guerre et un temps pour la paix.

### Elohîm fait toutes choses belles en leur temps

3:9	Quel avantage celui qui travaille a-t-il de sa peine ?
3:10	J'ai considéré cette occupation qu'Elohîm a donnée aux fils des humains pour s'y appliquer.
3:11	Il a fait toute chose belle en son temps. Aussi a-t-il mis l'éternité dans leur cœur, sans toutefois que l'être humain puisse comprendre du commencement à la fin<!--Ec. 8:17.-->, l'œuvre qu'Elohîm a faite.
3:12	C'est pourquoi j'ai reconnu qu'il n'y a rien de meilleur aux hommes, que de se réjouir et de se faire du bien pendant leur vie.
3:13	Et même si un être humain mange et boit, et jouit du bien-être de tout son travail, c'est un don d'Elohîm<!--Ec. 5:18, 8:15, 9:7.-->.
3:14	J'ai reconnu que tout ce qu'Elohîm fait subsiste à toujours, il n'y a rien à y ajouter et rien à en retrancher, et Elohîm le fait afin qu'on éprouve de la crainte devant lui.
3:15	Ce qui est a déjà été, et ce qui sera a déjà été, et Elohîm rappelle ce qui est passé.
3:16	J'ai encore vu sous le soleil qu'au lieu établi pour juger, il y a de la méchanceté et qu'au lieu établi pour la justice, il y a de la méchanceté.
3:17	Je me suis dit dans mon cœur : Elohîm jugera le juste et le méchant, car il y a là un temps pour toute chose et pour toute œuvre.
3:18	Je me suis dit dans mon cœur au sujet des fils de l'être humain, qu'Elohîm les éprouverait et qu'ils verraient qu'ils ne sont que des bêtes.
3:19	Car l'accident<!--Voir Ec. 2:14.--> des fils des humains et l'accident de la bête est un même accident. Comme meurt l'un, ainsi meurt l'autre. Tous ont un esprit<!--Le mot hébreu signifie aussi souffle.--> et la supériorité de l'être humain sur la bête est nulle. Car tout est vanité.
3:20	Tout va dans un même lieu. Tout a été fait de la poussière et tout retourne à la poussière<!--Ge. 3:19 ; Job 34:15 ; Ec. 6:6, 12:9.-->.
3:21	Qui sait si l'esprit des fils de l'être humain monte en haut et si l'esprit de la bête descend en bas dans la terre<!--Les animaux comme les humains ont une âme et un esprit (Ge. 1:20 ; Ez. 1:1-28). À leur mort, leurs esprits quittent leurs corps (Ja. 2:26). L'être humain régénéré reçoit le Saint-Esprit, ce qui n'est pas le cas des animaux (Ro. 8:16). Les animaux, tout comme la création tout entière, attendent leur rédemption, car ils ont été soumis à la corruption à cause du péché de l'être humain (Ro. 8:19-22). Dans le Royaume millénaire, il y aura des animaux (Es. 11:6-9).--> ?
3:22	J'ai donc vu qu'il n'y a rien de meilleur pour l'être humain que de se réjouir de ses œuvres : c'est là sa part. Car qui le ramènera pour voir ce qui sera après lui ?

## Chapitre 4

### Un monde injuste

4:1	Je me retournai et je vis toutes les oppressions qui se font sous le soleil. Voici les larmes des opprimés, ils n'ont personne pour les consoler ! La force est du côté de ceux qui les oppriment, et personne ne les console !
4:2	C'est pourquoi j'estime apaisés les morts qui sont déjà morts que les vivants qui sont encore vivants<!--Ec. 7:1.-->.
4:3	Et plus heureux que les deux celui qui n'a pas encore été, parce qu'il n'a pas vu les mauvaises actions qui se commettent sous le soleil.
4:4	J'ai vu que tout travail et tout succès dans le travail n'est que jalousie de l'un à l'égard de l'autre. Cela aussi est une vanité et un tourment d'esprit.
4:5	L'insensé se croise les mains et dévore sa propre chair<!--Pr. 6:10, 19:24, 24:33, 26:15.-->.
4:6	Mieux vaut le creux de la main pleine avec repos que les deux mains pleines avec travail et tourment d'esprit<!--Ps. 37:16 ; Pr. 15:16-17, 16:8.-->.
4:7	Je me suis mis à regarder une autre vanité sous le soleil :
4:8	tel est seul et n'a pas de second, il n'a ni fils ni frère, et cependant il ne met pas fin à son travail, même ses yeux ne se rassasient jamais de richesses. Et il ne se dit pas : Pour qui est-ce que je travaille et que je prive mon âme de bonnes choses ? Cela aussi est une vanité et une fâcheuse occupation<!--Ec. 2:26 ; Ps. 39:7 ; Lu. 12:20.-->.
4:9	Deux valent mieux qu'un, car ils ont un meilleur salaire de leur travail.
4:10	Car si l'un des deux tombe, l'autre relèvera son compagnon. Mais malheur à celui qui est seul ! S'il tombe, il n'a pas de second pour le relever.
4:11	De plus, s'ils couchent à deux, ils auront chaud, mais celui qui est seul, comment aura-t-il chaud ?
4:12	Et si quelqu'un a le dessus sur l'un ou sur l'autre, les deux peuvent lui résister, et la corde à trois fils ne se rompt pas vite.
4:13	Un enfant pauvre et sage vaut mieux qu'un roi vieux et insensé qui ne sait ce que c'est que d'être averti.
4:14	Car tel sort de prison pour régner, et de même tel étant né roi, devient pauvre dans son royaume.
4:15	J'ai vu tous les vivants qui marchent sous le soleil suivre le fils qui est la seconde personne après le roi et qui doit être à sa place ;
4:16	il n'y a pas de fin à tout ce peuple, à tous ceux devant qui il était. Oui, ceux qui viendront après ne se réjouiront plus à son sujet. Oui, cela aussi est une vanité et un tourment d'esprit.

### Le sacrifice des insensés

4:17	Quand tu entres dans la maison d'Elohîm, prends garde à ton pied, approche-toi pour écouter plutôt que pour offrir le sacrifice des insensés, car ils ne savent pas qu'ils font mal.

## Chapitre 5

5:1	Ne te presse pas d'ouvrir la bouche ! Que ton cœur ne se hâte pas de parler devant Elohîm ! Car Elohîm est au ciel et toi sur la Terre. C'est pourquoi use de peu de paroles !
5:2	Car comme le rêve vient de la multitude des occupations, ainsi la voix des insensés sort de la multitude des paroles<!--Pr. 10:19.-->.
5:3	Si tu as fait un vœu à Elohîm, ne tarde pas à l'accomplir, car il ne prend pas plaisir aux insensés. Accomplis donc le vœu que tu as fait<!--No. 30:3 ; De. 23:21.-->.
5:4	Il vaut mieux que tu ne fasses pas de vœu que d'en faire un et de ne pas l'accomplir<!--De. 23:21-22.-->.
5:5	Ne permets pas à ta bouche de faire pécher ta chair et ne dis pas devant le messager d'Elohîm que c'est un péché involontaire. Pourquoi YHWH s'irriterait-il de tes paroles et détruirait-il l'œuvre de tes mains ?
5:6	Car, comme dans la multitude des rêves il y a des vanités, aussi y en a-t-il beaucoup dans la multitude des paroles. Mais crains Elohîm<!--Ec. 10:14 ; Pr. 10:19.-->.
5:7	Si tu vois dans une province le pauvre opprimé, et le droit et la justice violés, ne t'étonne pas de cela, car celui qui est élevé est placé sous la surveillance d'un autre plus élevé, et il y en a encore d'autres qui sont élevés au-dessus d'eux<!--Es. 3:14-15.-->.
5:8	C'est un avantage pour le pays, un roi qui travaille dans les champs.

### Vanité des richesses

5:9	Celui qui aime l'argent n'est pas rassasié par l'argent<!--Jé. 6:13 ; Pr. 22:7, 28:16 ; Mt. 6:33, 7:7-11 ; Lu. 12:13-20 ; Ac. 20:33 ; 2 Co. 9:5 ; Ep. 4:19, 5:5 ; Col. 3:5 ; 1 Ti. 6:10 ; Hé. 13:5.--> et celui qui aime les richesses n'en est pas nourri. Cela aussi est une vanité.
5:10	Quand les bonnes choses se multiplient, ceux qui les mangent se multiplient aussi, et quel avantage en revient-il à leur possesseur, sinon qu'il le voit de ses yeux ?
5:11	Le sommeil de celui qui travaille est doux, qu'il mange peu ou beaucoup. Mais le rassasiement du riche ne le laisse pas dormir.
5:12	Il existe un malheur fâcheux que j'ai vu sous le soleil : la richesse que son possesseur garde pour son malheur.
5:13	Cette richesse se perd par un fâcheux accident, et s'il a engendré un fils, il n'aura rien entre les mains.
5:14	Et comme il est sorti du ventre de sa mère, il s'en retournera nu, s'en allant comme il était venu, et il n'emportera rien de son travail auquel il a employé ses mains<!--1 Ti. 6:7.-->.
5:15	Et c'est aussi un malheur fâcheux, que comme il est venu, il s'en va de même. Quel avantage a-t-il d'avoir travaillé pour du vent ?
5:16	Il mange aussi tous les jours de sa vie dans les ténèbres et se chagrine beaucoup, et son malheur va jusqu'à la fureur.
5:17	Voici ce que j'ai vu : c'est une chose bonne et belle de manger, de boire et de voir le bonheur dans tout son travail qu'il fait sous le soleil, pendant le nombre des jours de vie qu'Elohîm lui a donnés, car c'est là sa part.
5:18	En effet, si Elohîm a donné à un être humain, quel qu'il soit, des richesses et des biens, s'il lui a donné le pouvoir d'en manger, d'en prendre sa part et de se réjouir de son travail, c'est là un don d'Elohîm.
5:19	Car il ne se souviendra pas beaucoup des jours de sa vie, parce qu'Elohîm l'occupe avec la joie de son cœur.

## Chapitre 6

### Vanité de la vie de l'être humain

6:1	Il existe un malheur que j'ai vu sous le soleil et qui est fréquent parmi les humains :
6:2	c'est qu'il y a tel homme à qui Elohîm a donné des richesses, des biens et des honneurs, et qui ne manque de rien pour son âme de tout ce qu'il peut souhaiter, mais Elohîm ne lui accorde pas le pouvoir de s'en nourrir. Car c'est un étranger qui s'en nourrira. Cela est une vanité et un malheur fâcheux.
6:3	Si un homme engendrait 100 fils et vivait un grand nombre d'années, aussi nombreux que soient les jours de sa vie, si son âme ne s'est pas rassasiée de bonheur et si, de plus, il n'a pas de sépulture, je dis qu'un avorton est plus heureux que lui.
6:4	Car il est venu en vain et s'en va dans les ténèbres, et son nom est couvert de ténèbres.
6:5	Il n'a même pas vu le soleil, il n'a rien connu, et il a plus de repos que cet homme-là<!--Job 3:16.-->.
6:6	Et s'il vivait deux fois 1 000 ans sans jouir d'aucun bien, tous ne vont-ils pas dans un même lieu<!--Ec. 3:20 ; Job 3:13-19, 30:23 ; Ps. 89:48 ; Hé. 9:27.--> ?
6:7	Tout le travail de l'être humain est pour sa bouche, et cependant son âme n'est jamais satisfaite<!--Les richesses de ce monde ne peuvent jamais combler le vide de l'âme. Seul l'amour d'Elohîm peut réellement inonder nos âmes (Pr. 13:4).-->.
6:8	Car, qu'est-ce que le sage a de plus que l'insensé ? Ou quel avantage a le malheureux qui sait se conduire devant les vivants ?
6:9	Mieux vaut ce qu'on voit de ses yeux que les grandes recherches que fait l'âme. Cela aussi est une vanité et un tourment d'esprit<!--1 Ti. 6:9.-->.
6:10	Ce qui existe a déjà été appelé par son nom<!--Ec. 1:9, 3:15.--> et l'on sait ce qu'est l'humain : il ne peut contester avec celui qui est plus puissant que lui.
6:11	Quand on a beaucoup de choses, on a beaucoup de vanités. Quel avantage en a l'être humain ?
6:12	Car qui sait ce qui est bon pour l'être humain dans la vie, pendant les jours de la vie de sa vanité qu'il passe comme une ombre ? Et qui déclarera à l'être humain ce qui sera après lui sous le soleil<!--Ps. 144:4 ; Ec. 8:7,13, 10:14 ; Ja. 4:13-14.--> ?

## Chapitre 7

### La sagesse qu'enseigne la vie de l'être humain

7:1	Un nom<!--Vient de l'hébreu « shem » et signifie aussi « réputation », « renommée ». Voir Es. 56:5 ; Ap. 2:17.--> vaut mieux qu'une bonne huile, et le jour de la mort que le jour de la naissance<!--Pr. 22:1.-->.
7:2	Il vaut mieux aller dans une maison de deuil que d'aller dans une maison de festin, car c'est là la fin de tout être humain, et le vivant met cela dans son cœur.
7:3	Il vaut mieux le chagrin que le rire, car par la tristesse du visage le cœur devient joyeux<!--Ec. 8:1 ; 2 Co. 7:10.-->.
7:4	Le cœur des sages est dans la maison du deuil, mais le cœur des insensés est dans la maison de joie.
7:5	Il vaut mieux entendre la réprimande du sage que d'entendre la chanson des hommes insensés<!--Ps. 141:5 ; Pr. 13:18, 15:31-32.-->.
7:6	Car, comme le bruit des épines sous la chaudière, tel est le rire de l'insensé. Cela aussi est une vanité.
7:7	En effet, l’oppression fait agir le sage comme un fou et le don détruit le cœur.
7:8	Mieux vaut la fin d'une chose que son commencement. Mieux vaut l'homme qui est d'un esprit patient que l'homme qui est d'un esprit hautain.
7:9	Ne te précipite pas en ton esprit de t'irriter, car l'irritation repose dans le sein des insensés.
7:10	Ne dis pas : D'où vient que les jours passés ont été meilleurs que ceux-ci ? Car ce n'est pas par sagesse que tu demandes cela.
7:11	La sagesse est bonne avec un héritage, elle est un avantage pour ceux qui voient le soleil,
7:12	car on est à couvert à l'ombre de la sagesse de même qu'à l'ombre de l'argent. Mais la connaissance a cet avantage, que la sagesse fait vivre celui qui en est doué.
7:13	Regarde l'œuvre d'Elohîm : qui pourra redresser ce qu'il a renversé ?
7:14	Au jour du bonheur, sois heureux, et au jour du malheur, prends-y garde ! Car Elohîm a fait l'un exactement comme l'autre, afin que l'être humain ne trouve rien après lui.
7:15	J'ai vu tout ceci pendant les jours de ma vanité. Il y a tel juste qui périt dans sa justice et il y a tel méchant qui prolonge ses jours dans sa méchanceté<!--Ec. 8:14 ; Job 21:7-8.-->.
7:16	Ne te crois pas trop juste et ne te fais pas plus sage qu'il ne faut : pourquoi t'exposer à la ruine<!--Pr. 3:7 ; Ro. 12:16.--> ?
7:17	Ne sois pas méchant à l'excès et ne sois pas insensé : pourquoi mourrais-tu avant ton temps<!--Ec. 9:16.--> ?
7:18	Il est bon que tu retiennes ceci et que tu ne retires pas ta main de cela, car celui qui craint Elohîm sort de tout.
7:19	La sagesse donne plus de force au sage que 10 gouverneurs qui sont dans une ville.
7:20	En effet, il n'y a sur la Terre aucun être humain juste qui fasse ce qui est bon et qui ne pèche jamais<!--Ps. 14:3 ; Pr. 20:9 ; 2 Ch. 6:36 ; Ja. 3:2 ; Ro. 3:12 ; 1 Jn. 1:8.-->.
7:21	Ne mets donc pas ton cœur à toutes les paroles qu'on dira, afin que tu n'entendes pas ton serviteur médire de toi.
7:22	Car, ton cœur aussi a reconnu plusieurs fois que tu as pareillement mal parlé des autres.
7:23	Tout cela, je l'ai mis à l'épreuve par la sagesse. J'ai dit : Je serai sage, mais elle s'est éloignée de moi.
7:24	Ce qui est loin et ce qui est profond, qui le trouvera ?
7:25	Je me suis appliqué dans mon cœur à connaître, à sonder et à chercher la sagesse et la raison de tout, à connaître la méchanceté de la folie, de la bêtise et des sottises.
7:26	Et j'ai trouvé plus amère que la mort, la femme dont le cœur est un piège et un filet, et dont les mains sont des liens. Celui qui est agréable à Elohîm lui échappera, mais le pécheur sera pris par elle<!--Pr. 5:3-4, 6:26, 7:13-27, 9:13-16, 22:14.-->.
7:27	Voici, dit Qohelet, ce que j'ai trouvé en cherchant la raison de toutes choses, l'une après l'autre ;
7:28	c'est que jusqu'à présent, mon âme a cherché, mais que je n'ai pas trouvé, c'est que j'ai bien trouvé un homme entre 1 000, mais pas une femme entre elles toutes.
7:29	Seulement voici ce que j'ai trouvé : c'est qu'Elohîm a créé l'être humain juste, mais ils ont cherché beaucoup d'inventions.

## Chapitre 8

### L'obéissance aux autorités

8:1	Qui est tel que le sage ? Et qui sait ce que veulent dire les choses ? La sagesse de l'être humain fait briller son visage et son regard farouche en est changé<!--Ec. 7:3 ; Pr. 15:13.-->.
8:2	Je te le dis : Prends garde aux ordres du roi, et cela à cause du serment fait à Elohîm.
8:3	Ne te précipite pas de te retirer de devant sa face et ne persévère pas dans une chose mauvaise ! Car il fera tout ce qu'il lui plaira.
8:4	Parce que la parole du roi c'est la maîtrise. Qui lui dira : Que fais-tu ?
8:5	Celui qui garde le commandement ne connaîtra aucune chose mauvaise, et le cœur du sage connaîtra le temps et la justice.
8:6	Il y a en effet pour toute chose un temps et un jugement, autrement malheur sur malheur tombe sur l'être humain.
8:7	Car il ne sait pas ce qui arrivera : qui pourrait lui dire comment cela sera ?
8:8	L'être humain n'est pas maître de son souffle<!--Le souffle ou l'esprit de l'humain quitte son corps le jour de sa mort (Ps. 39:5 ; Ja. 2:26).--> pour pouvoir le retenir. Il n'a aucune maîtrise sur le jour de la mort ; il n'y a pas de délivrance dans ce combat, et la méchanceté ne délivrera pas son maître.
8:9	J'ai vu tout cela et j'ai appliqué mon cœur à toute œuvre qui se fait sous le soleil. Il y a un temps où l'être humain domine sur l'être humain pour son malheur.
8:10	Alors j'ai vu les méchants ensevelis et s'en aller tandis que ceux qui avaient agi avec droiture s'en aller loin du lieu saint et être oubliés dans la ville. Cela aussi est une vanité<!--Ec. 2:16, 9:5.-->.
8:11	Parce que la sentence contre les mauvaises œuvres ne s'exécute pas promptement, à cause de cela, le cœur des fils de l'être humain se remplit en eux de l'envie de faire le mal<!--Ec. 12:1.-->.
8:12	Car bien que le pécheur fasse le mal 100 fois et qu'il y persévère longtemps, je sais aussi qu'il y aura du bonheur pour ceux qui craignent Elohîm et qui révèrent sa face<!--Job 22:21 ; Pr. 1:33 ; Es. 3:10.-->.
8:13	Mais le bonheur n'est pas pour le méchant et il ne prolongera pas ses jours plus que l'ombre, parce qu'il n'a pas de crainte devant Elohîm.
8:14	Il y a une vanité qui arrive sur la Terre : c'est qu'il y a des justes auxquels il arrive selon l'œuvre des méchants, et il y a aussi des méchants auxquels il arrive selon l'œuvre des justes. Je dis que cela aussi est une vanité.
8:15	C'est pourquoi j'ai loué la joie, parce qu'il n'y a rien de bon sous le soleil pour l'être humain, que de manger et de boire, et de se réjouir ; c'est aussi ce qui lui restera de son travail durant les jours de sa vie qu'Elohîm lui donne sous le soleil.
8:16	Après avoir appliqué mon cœur à connaître la sagesse et à regarder les occupations qu'il y a sur la Terre (car l'homme ne donne ni jour ni nuit de repos à ses yeux),
8:17	j'ai vu toute l'œuvre d'Elohîm : non, l'être humain ne peut pas trouver l'œuvre qui se fait sous le soleil. L'être humain a beau travailler dur sans relâche pour la chercher, il n'est pas capable de la trouver, et même si le sage dit la connaître, il ne peut la trouver.

## Chapitre 9

### L'impuissance de la sagesse face à la mort

9:1	En effet, j'ai consacré mon cœur à tout cela pour rendre clair tout cela : que les justes, les sages et leurs actions sont dans la main d'Elohîm. Mais les humains ne connaissent ni l'amour ni la haine de tout ce qui est devant eux.
9:2	Tout arrive également à tous : un même accident<!--Voir Ec. 2:14.--> arrive au juste et au méchant, à celui qui est bon et pur et à celui qui est impur, à celui qui sacrifie et à celui qui ne sacrifie pas. L'homme bon est pareil au pécheur, celui qui jure comme celui qui craint de jurer.
9:3	C'est un mal parmi tout ce qui se fait sous le soleil, c'est qu'il y a pour tous un même accident<!--Voir Ec. 2:14.-->. Aussi le cœur des fils de l'homme est-il plein de méchanceté et la folie est dans leur cœur pendant leur vie. Après cela, ils vont chez les morts. Qui est celui qui voudrait leur être associé<!--Ec. 2:16 ; Job 9:22.--> ?
9:4	Il y a de l'espérance pour tous ceux qui sont encore vivants. Même un chien vivant vaut mieux qu'un lion mort.
9:5	En effet, les vivants savent qu'ils mourront, mais les morts ne savent rien et ne gagnent plus rien, car leur souvenir est oublié.
9:6	Aussi leur amour, leur haine et leur envie ont déjà péri, et ils n'auront plus aucune part à tout ce qui se fait sous le soleil.
9:7	Va donc, mange ton pain avec joie et bois gaiement ton vin, car depuis longtemps Elohîm prend plaisir à tes œuvres.
9:8	Que tes vêtements soient blancs en tout temps et que l'huile ne manque pas sur ta tête.
9:9	Vis joyeusement tous les jours de ta vie de vanité avec la femme que tu aimes, qui t'a été donnée sous le soleil, tous les jours de ta vanité, car c'est là ta part dans la vie, au milieu de ton travail que tu fais sous le soleil.
9:10	Tout ce que ta main trouve à faire, fais-le selon ton pouvoir, car dans le shéol où tu vas, il n'y a ni œuvre, ni pensée, ni connaissance, ni sagesse.
9:11	Je suis retourné et j’ai vu sous le soleil que la course n'est pas aux rapides, ni la guerre aux vaillants, ni le pain aux sages, ni la richesse aux intelligents, ni la grâce à ceux qui ont de la connaissance, car temps et événement leur arrivent à tous.
9:12	Car l'homme ne connaît pas son heure, comme les poissons qui sont pris au filet de malheur et les oiseaux qui sont pris au piège. Comme eux, les fils de l'homme sont enlacés au temps du malheur lorsqu'il tombe subitement sur eux.
9:13	J'ai aussi vu cette sagesse sous le soleil et elle m'a semblé grande.
9:14	Il y avait une petite ville avec peu d'hommes dans son sein. Un roi puissant marcha contre elle, l'investit et bâtit de grands forts contre elle.
9:15	Mais il s'y trouvait un homme pauvre et sage qui délivra la ville par sa sagesse. Et personne ne s'est souvenu de cet homme pauvre.
9:16	Alors j'ai dit : La sagesse vaut mieux que la force. Cependant, la sagesse du pauvre est méprisée et ses paroles ne sont pas écoutées.
9:17	Les paroles des sages doivent être écoutées plus paisiblement que le cri de celui qui domine parmi les insensés.
9:18	La sagesse vaut mieux que tous les instruments de guerre, mais un seul homme pécheur détruit beaucoup de bien.

## Chapitre 10

### La sagesse vaut mieux que la folie

10:1	Les mouches mortes font puer et fermenter les parfums du parfumeur, et un peu de folie produit le même effet à l'égard de celui qui est estimé pour sa sagesse et pour sa gloire.
10:2	Le cœur du sage est à sa droite, et le cœur de l'insensé est à sa gauche.
10:3	Et même quand l'insensé se met en chemin, le sens lui manque, et il dit de chacun : Il est insensé
10:4	Si l'esprit de celui qui domine s'élève contre toi, n'abandonne pas ton poste, car la guérison n'abandonne pas les grands péchés.
10:5	Il y a un mal que j'ai vu sous le soleil, comme une erreur qui procède du prince :
10:6	c'est que la folie est mise aux plus hauts lieux et que les riches sont assis dans un lieu bas.
10:7	J'ai vu des serviteurs sur des chevaux et des princes marchant sur terre comme des serviteurs.
10:8	Celui qui creuse la fosse y tombera, et celui qui fait une brèche dans une clôture, le serpent le mordra<!--Ps. 7:15 ; Pr. 26:27, 28:10.-->.
10:9	Celui qui remue des pierres hors de leur place, en sera blessé, et celui qui fend du bois se met en danger.
10:10	Si le fer est émoussé et qu'on n'en ait pas aiguisé le tranchant, il devra redoubler de force. Mais la sagesse a l'avantage de donner de l'adresse.
10:11	Si le serpent mord quand il n'est pas charmé, il n'y a pas d'avantage pour le maître de la langue.
10:12	Les paroles de la bouche du sage ne sont que grâce, mais les lèvres de l'insensé le réduisent à néant<!--Pr. 10:21.-->.
10:13	Le commencement des paroles de sa bouche est folie, et la fin de son discours est une méchante folie.
10:14	Or l'insensé multiplie les paroles. L'homme ne sait pas ce qui arrivera, et qui lui déclarera ce qui sera après lui ?
10:15	Le travail de l'insensé le fatigue, parce qu'il ne sait pas aller à la ville.
10:16	Malheur à toi, pays dont le roi est un jeune homme et dont les princes mangent dès le matin<!--Es. 3:4.--> !
10:17	Béni es-tu, pays dont le roi est le fils des nobles et dont les princes mangent au temps convenable, pour prendre des forces et non pour boire !
10:18	À cause des mains paresseuses, la charpente s'affaisse, et à cause des mains lâches, la maison a des gouttières.
10:19	On fait des pains pour se réjouir, et le vin réjouit les vivants, et l'argent répond à tout.
10:20	Ne maudis pas le roi, même dans ta pensée, et ne maudis pas le riche dans la chambre où tu couches, car l'oiseau du ciel emporterait ta voix, le Baal ailé<!--Le terme sémitique « baal » (en hébreu ba'al) signifie à l'origine « possesseur », « maître » ou « seigneur ». Le Baal ailé était une créature ailée. Utilisée au pluriel, l'expression « baalim de flèches » désignait des archers. Les écritures nous parlent de Baal-Zeboub (seigneur des mouches), un démon adoré à Ékron, l'une des villes des Philistins (2 R. 1:1-16). Baal-Zeboub a donné « Béelzéboul » dans les Évangiles (Mt. 10:25, 12:24,27 ; Lu. 11:15-19). Ce passage nous enseigne clairement que les démons épient les enfants d'Elohîm et vont ensuite faire leurs rapports à Satan afin de mieux les attaquer. Ils agissent comme des espions. Ces esprits sont comme des mouches et essayent de s'infiltrer partout.--> rapporterait tes paroles.

## Chapitre 11

### L'être humain travaille en tâtonnant

11:1	Jette ton pain à la surface des eaux, car avec le temps tu le retrouveras.
11:2	Donnes-en une part à sept et même à huit, car tu ne sais pas quel mal viendra sur la Terre.
11:3	Quand les nuages sont pleins, ils répandent la pluie sur la Terre. Si un arbre tombe, au sud ou au nord, il reste à la place où il est tombé.
11:4	Celui qui observe le vent ne sèmera pas et celui qui regarde les nuées ne moissonnera pas.
11:5	Comme tu ne sais pas quel est le chemin du vent ni comment se forment les os dans le ventre de celle qui est enceinte, ainsi tu ne connais pas l'œuvre d'Elohîm qui fait tout<!--Ceux qui sont nés d'en-haut sont insaisissables comme le vent (Jn. 3:8).-->.
11:6	Sème ta semence dès le matin et ne laisse pas reposer tes mains le soir, car tu ne sais pas ce qui réussira, ceci ou cela, ou si les deux seront également bons.
11:7	Il est vrai que la lumière est douce et qu'il est agréable aux yeux de voir le soleil.
11:8	Mais si un être humain vit de nombreuses années, qu'il se réjouisse et qu'il se souvienne des jours de ténèbres qui seront en grand nombre. Tout ce qui vient est vanité.

## Chapitre 12

### Message à la jeunesse

12:1	Jeune homme, réjouis-toi dans ton jeune âge, et que ton cœur te rende joyeux pendant les jours de ta jeunesse, et marche comme ton cœur te mène, et selon le regard de tes yeux. Mais, sache que pour toutes ces choses Elohîm t'amènera en jugement.
12:2	Ôte le chagrin de ton cœur et éloigne de toi le mal, car la noirceur<!--La noirceur de la chevelure, indiquant la jeunesse.--> et la jeunesse ne sont que vanité.
12:3	Mais souviens-toi de ton Créateur pendant les jours de ta jeunesse, avant que les jours mauvais arrivent et que viennent les années où tu diras : Je n'y prends aucun plaisir.
12:4	Avant que le soleil et la lumière, la lune et les étoiles s'obscurcissent, et que les nuages reviennent après la pluie.
12:5	Lorsque les gardes de la maison tremblent<!--« Les gardes de la maison » représentent les mains.--> et que les hommes talentueux<!--« Les hommes talentueux » sont les jambes.--> se courbent, et que celles qui moulent<!--Les dents sont « celles qui moulent ».--> cessent de travailler parce qu'elles sont diminuées, et quand ceux qui regardent par les fenêtres<!--Les yeux sont « ceux qui regardent par les fenêtres ».--> sont obscurcis.
12:6	Et quand les deux battants de la porte<!--Les lèvres sont « les deux battants de la porte ».--> se ferment sur la rue quand s'abaisse le bruit de la meule, quand on se lève au cri de l'oiseau et que toutes les filles du chant s'affaiblissent.
12:7	Quand on a même peur de ce qui est élevé et des terreurs en chemin. Quand l'amandier fleurit, que la sauterelle devient pesante, et que la baie qui stimule le désir se brise, car l'homme s'en va vers sa maison éternelle<!--La maison éternelle, c'est la Nouvelle Yeroushalaim pour les chrétiens (Ap. 21.) et pour les païens le lac de feu (Ap. 20:11-15).--> et ceux qui pleurent font le tour des rues.
12:8	Avant que la corde d'argent<!--Cette corde est comme le cordon ombilical, elle lie l'âme au corps. Lors de la mort, la corde d'argent est coupée.--> se détache, que le vase d'or<!--Le corps humain est comme un vase ou une tente qui renferme son esprit. Comme l'argile dans la main du potier, ainsi est l'être humain dans celle d'Elohîm. Avec cette argile, il décide souverainement de fabriquer de la même masse un vase d'honneur et un autre pour un usage vil (Jé. 18:4-6 ; Ro. 9:21 ; 2 Ti. 2:20-21).--> se brise, que la cruche se rompe sur la source, que la roue s'écrase sur la citerne.
12:9	Avant que la poussière ne retourne à la terre, comme elle y était, et que l'esprit ne retourne à Elohîm qui l'a donné<!--Yéhoshoua (Jésus), qui est Elohîm, a reçu l'esprit de Stephanos (Étienne). Voir Ac. 7:59.-->.

### Conclusion

12:10	Vanité des vanités, dit Qohelet, tout est vanité.
12:11	Plus Qohelet a été sage, plus il a enseigné la connaissance au peuple. Il a pesé, recherché et mis en ordre un grand nombre de proverbes.
12:12	Qohelet a cherché pour trouver des discours agréables. Ce qui a été écrit l'a été avec droiture, ce sont des paroles de vérité.
12:13	Les paroles des sages sont comme des aiguillons et les maîtres des recueils comme des clous plantés. Elles ont été données par un seul berger.
12:14	En plus de cela, mon fils, sois averti ! On n'en finirait pas, si l'on voulait faire un grand nombre de livres, et beaucoup d'étude est une fatigue pour le corps.
12:15	Voici la conclusion de tout le discours qui a été entendu : crains Elohîm et garde ses commandements, car c'est là le tout de l'être humain.
12:16	Car Elohîm amènera toute œuvre en jugement, au sujet de tout ce qui est caché, soit bien, soit mal.
