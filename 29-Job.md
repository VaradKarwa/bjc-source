# Iyov (Job) (Job)

Signification : Haï, ennemi et « je m'exclamerai »

Auteur : Inconnu

Thème : La souffrance

Date de rédaction : Incertaine

Iyov était un homme prospère et intègre auquel Elohîm rendit témoignage. Il subit une succession de malheurs en très peu de temps en perdant tout ce qui lui était cher. Après avoir cherché à se justifier et subi les railleries de sa femme et les accusations de ses amis, Iyov s'humilia devant Elohîm et comprit l'impuissance de sa propre justice. Cette histoire, dont on n'a aucune indication spatio-temporelle et qui pourtant parle à tous, est un encouragement pour le juste éprouvé.

Rappelant que la souffrance peut être le moyen choisi par Elohîm pour enseigner et se révéler, ce récit illustre la fidélité et la bonté de YHWH envers ceux qui le craignent.

## Chapitre 1

### Iyov et sa famille

1:1	Il y avait dans le pays d'Outs<!--Ge. 36:28.--> un homme appelé Iyov<!--Ez. 14:14 ; Ja. 5:11.-->. Cet homme était intègre<!--1 R. 8:61.--> et droit, craignant<!--Ps. 19:10 ; Pr. 1:7.--> Elohîm et se détournant du mal.
1:2	Il eut 7 fils et 3 filles.
1:3	Et son bétail était de 7 000 brebis, 3 000 chameaux, 500 paires de bœufs et 500 ânesses, avec un très grand nombre de serviteurs<!--Job 42:12-13.-->. Cet homme devint le plus grand de tous les fils de l'orient.
1:4	Or ses fils allaient et faisaient des festins dans leurs maisons, chacun son jour, et ils envoyaient appeler leurs trois sœurs pour manger et boire avec eux.
1:5	Et il arrivait que, quand les jours de festin étaient passés, Iyov les sanctifiait et les laissait aller. Il se levait de bonne heure le matin et offrait des holocaustes selon leur nombre à tous, car Iyov se disait : Peut-être mes fils ont-ils péché et ont-ils blasphémé contre Elohîm dans leurs cœurs. Iyov agissait toujours ainsi<!--Job 42:8.-->.
1:6	Or il arriva un jour que les fils d'Elohîm vinrent se présenter devant YHWH, et Satan<!--Es. 14:12 ; Ap. 12:9-10.--> vint aussi au milieu d'eux.
1:7	YHWH dit à Satan : D'où viens-tu ? Et Satan répondit à YHWH en disant : Je viens de courir çà et là sur la Terre, et de m'y promener<!--1 Pi. 5:8.-->.
1:8	YHWH dit à Satan : As-tu fixé ton cœur sur mon serviteur Iyov ? Il n'y a personne comme lui sur la Terre. C'est un homme parfait et droit, craignant Elohîm et se détournant du mal.
1:9	Et Satan répondit à YHWH en disant : Est-ce en vain que Iyov craint Elohîm ?
1:10	N'as-tu pas mis un rempart tout autour de lui, autour de sa maison, autour de tout ce qui lui appartient ? Tu as béni l'œuvre de ses mains, et son bétail s'accroît dans le pays.
1:11	Mais étends maintenant ta main, touche à tout ce qui lui appartient et il ne te bénira pas en face !
1:12	Et YHWH dit à Satan : Voici, tout ce qui lui appartient est dans ta main. Seulement, ne porte pas la main sur lui ! Et Satan sortit de devant la face de YHWH<!--1 R. 22:22.-->.

### Première attaque de Satan

1:13	Il arriva donc qu'un jour, comme ses fils et ses filles mangeaient et buvaient du vin dans la maison de leur frère aîné, un messager vint vers Iyov,
1:14	et lui dit : Les bœufs labouraient et les ânesses paissaient à côté d'eux.
1:15	Et ceux de Séba se sont jetés dessus, les ont pris et ont frappé les serviteurs au tranchant de l'épée. Et je me suis échappé moi seul, pour te le rapporter.
1:16	Cet homme parlait encore, lorsqu'un autre vint et dit : Le feu d'Elohîm est tombé du ciel, il a embrasé les brebis et les serviteurs, et les a consumés<!--2 R. 1:10-12.-->. Et je me suis échappé moi seul, pour te le rapporter.
1:17	Cet homme parlait encore, lorsqu'un autre vint et dit : Les Chaldéens<!--Ge. 11:28.--> se sont rangés en trois bandes, se sont jetés sur les chameaux et les ont pris, ils ont frappé les serviteurs au tranchant de l'épée, et je me suis échappé moi seul, pour te le rapporter.
1:18	Cet homme parlait encore, lorsqu'un autre vint et dit : Tes fils et tes filles mangeaient et buvaient du vin dans la maison de leur frère aîné,
1:19	voici, un grand vent est venu de l'autre côté du désert et a frappé contre les quatre coins de la maison. Elle est tombée sur les jeunes hommes, et ils sont tous morts, et je me suis échappé moi seul, pour te le rapporter.
1:20	Alors Iyov se leva, déchira<!--Job 2:12 ; Est. 4:1.--> sa robe et se rasa la tête. Puis il se jeta par terre, se prosterna,
1:21	et dit : Je suis sorti nu du ventre de ma mère et nu j'y retournerai<!--Ec. 5:14 ; 1 Ti. 6:7.-->. YHWH a donné et YHWH a enlevé<!--1 S. 2:6.-->. Que le nom de YHWH soit béni !
1:22	En tout cela, Iyov ne pécha pas et n'attribua rien d'insensé à Elohîm.

## Chapitre 2

### Deuxième attaque de Satan

2:1	Or il arriva un jour que les fils d'Elohîm vinrent pour se présenter devant YHWH, Satan<!--Za. 3:1-2.--> vint aussi au milieu d'eux pour se présenter devant YHWH.
2:2	YHWH dit à Satan : D'où viens-tu ? Satan répondit à YHWH en disant : Je viens de courir çà et là sur la Terre, et de m'y promener.
2:3	YHWH dit à Satan : As-tu fixé ton cœur sur mon serviteur Iyov ? Il n'y a personne comme lui sur la Terre. C'est un homme parfait et droit, craignant Elohîm, et se détournant du mal, et qui demeure toujours ferme dans son intégrité, quoique tu m'aies incité contre lui pour le détruire sans cause<!--Job 9:17.-->.
2:4	Et Satan répondit à YHWH en disant : Peau pour peau ! Tout ce qu'un homme possède, il le donne pour son âme.
2:5	Mais étends maintenant ta main, et frappe ses os et sa chair<!--Job 19:20.-->. Te bénira-t-il en face ?
2:6	YHWH dit à Satan : Voici, il est dans ta main : seulement, fais attention à son âme !
2:7	Ainsi Satan sortit de devant YHWH, et frappa Iyov d'un mauvais ulcère, depuis la plante de ses pieds jusqu'au sommet de la tête.
2:8	Iyov prit un tesson pour se gratter et s'assit sur la cendre<!--Jé. 6:26 ; Jon. 3:6.-->.

### Réaction de Iyov et de sa femme

2:9	Et sa femme lui dit : Tu tiens toujours ferme dans ton intégrité ? Bénis<!--Job 1:11.--> Elohîm et meurs !
2:10	Et il lui dit : Tu parles comme parlerait une de ces insensées ! Nous recevons d'Elohîm ce qui est bon, et nous n'en recevrions pas aussi ce qui est mauvais<!--Es. 45:7 ; Am. 3:6 ; La. 3:37.--> ? En tout cela, Iyov ne pécha pas par ses lèvres.

### Iyov et ses trois amis

2:11	Or trois des amis intimes de Iyov, Éliphaz de Théman, Bildad de Shouah et Tsophar de Naama, ayant appris que tout ce mal lui était arrivé, vinrent chacun de son lieu, après s'être convenus ensemble d'un jour pour venir lui présenter leurs condoléances et le consoler.
2:12	Ayant de loin levé les yeux, ils ne le reconnurent pas, et ils élevèrent la voix et pleurèrent. Ils déchirèrent chacun sa robe et jetèrent de la poussière vers le ciel, au-dessus de leur tête.
2:13	Et ils s'assirent par terre avec lui, pendant 7 jours et 7 nuits, et aucun d'eux ne lui dit une parole, car ils voyaient que sa douleur était très grande.

## Chapitre 3

### Lamentations de Iyov

3:1	Après cela, Iyov ouvrit la bouche et maudit son jour<!--Jé. 20:14 ; Job 10:18.-->.
3:2	Car prenant la parole, il dit :
3:3	Périsse le jour où je suis né, et la nuit où l'on a dit : Un enfant mâle est né !
3:4	Que ce jour-là ne soit que ténèbres, qu'Éloah ne le recherche pas d'en haut et que la lumière du jour ne l'éclaire plus !
3:5	Que les ténèbres et l'ombre de la mort<!--Job 10:21-22.--> le rachètent, que les nuées demeurent sur lui, que les éclipses du jour le terrifient !
3:6	Que l'obscurité couvre cette nuit-là, qu'elle ne se réjouisse pas d'être parmi les jours de l'année, qu'elle ne soit pas comptée parmi les mois !
3:7	Voici, que cette nuit soit stérile, et qu'aucun cri de joie n'y survienne.
3:8	Qu'ils la maudissent ceux qui maudissent les jours, ceux qui sont prêts à réveiller le léviathan<!--Ce nom vient de la mythologie phénicienne qui est en fait le monstre du chaos primitif. C'est également un monstre marin évoqué dans la Bible (Es. 27:1 ; Ps. 74:14, 104:26 ; Job 40:20, 41:1).--> !
3:9	Que les étoiles de son crépuscule soient obscurcies, qu'elle attende la lumière et qu'il n'y en ait pas, qu'elle attende sans succès la lumière et ne voie pas les rayons de l'aurore<!--Job 41:9.--> !
3:10	Parce qu'elle n'a pas fermé le ventre qui m'a porté, et qu'elle n'a pas caché le tourment loin de mes yeux.
3:11	Pourquoi ne suis-je pas mort dans le sein de ma mère ? Pourquoi n'ai-je pas expiré aussitôt que je suis sorti de ses entrailles<!--Job 10:18.--> ?
3:12	Pourquoi des genoux m'ont-ils reçu ? Pourquoi des mamelles m'ont-elles allaité ?
3:13	En effet, maintenant je serais couché, je serais tranquille, je dormirais et je me reposerais<!--Job 17:16.-->,
3:14	avec les rois et les conseillers de la Terre, qui se sont bâtis des ruines,
3:15	avec les princes qui ont possédé de l'or et qui ont rempli d'argent leurs maisons.
3:16	Ou que n'ai-je été comme un avorton caché<!--Ps. 58:9.-->, comme les petits enfants qui n'ont pas vu la lumière.
3:17	Là les méchants ne tourmentent plus personne, et là se reposent ceux qui sont fatigués.
3:18	Les prisonniers y sont tous en paix, ils n'entendent plus la voix de l'oppresseur.
3:19	Le petit et le grand sont là, et l'esclave n'est plus sujet à son maître.
3:20	Pourquoi la lumière est-elle donnée au misérable, et la vie à ceux qui ont le cœur dans l'amertume,
3:21	qui attendent en vain la mort et qui la recherchent plus que le trésor<!--Ap. 9:6.-->,
3:22	qui seraient ravis de joie et seraient dans l'allégresse s'ils avaient trouvé le sépulcre ?
3:23	À l'homme dont la voie est cachée, et qu'Éloah a enfermé de tous côtés<!--Job 19:8 ; La. 3:7.--> ?
3:24	Car avant ma nourriture viennent mes soupirs, et mes rugissements coulent comme des eaux.
3:25	Ce que je crains le plus m'arrive, et ce que je redoute le plus m'atteint.
3:26	Je n'ai pas eu de paix, je n'ai pas eu de repos, ni de calme, depuis que ce trouble m'est arrivé.

## Chapitre 4

### Premier discours d'Éliphaz

4:1	Alors Éliphaz de Théman prit la parole et dit :
4:2	Si nous essayons de t'adresser une parole, en seras-tu offensé ? Mais qui pourrait retenir ses paroles ?
4:3	Voici, tu as instruit beaucoup de gens, et tu as fortifié les mains affaiblies<!--Es. 35:3 ; Hé. 12:12.-->,
4:4	tes paroles ont affermi ceux qui chancelaient, et tu as fortifié les genoux qui pliaient<!--Job 16:5.-->.
4:5	Et maintenant que le malheur t'arrive, tu faiblis ! Maintenant que tu es atteint, tu es terrifié !
4:6	Ta piété n'est-elle pas ta confiance, et l'intégrité de ta conduite ton espérance ?
4:7	Rappelle, je te prie, dans ton souvenir : Quel est l'innocent qui a péri ? Quels sont les hommes droits qui ont été exterminés<!--Job 8:20.--> ?
4:8	Selon ce que j'ai vu, ceux qui labourent l'iniquité et qui sèment l'outrage les moissonnent<!--Job 15:35 ; Ga. 6:7.--> ;
4:9	ils périssent par le souffle d'Éloah, et ils sont consumés par le vent de ses narines<!--Ex. 15:8 ; Es. 11:4, 30:33 ; Job 15:30 ; 2 Th. 2:8.-->.
4:10	Le rugissement du lion, le cri d'un grand lion, les dents du jeune lion se brisent.
4:11	Le lion périt faute de proie, et les petits de la lionne sont dissipés.
4:12	Mais quant à moi, une parole est arrivée furtivement jusqu'à moi, et mon oreille en a entendu un peu.
4:13	Au moment où les visions de la nuit agitent la pensée, quand un profond sommeil saisit les hommes<!--Job 33:15.-->,
4:14	une frayeur et un tremblement me saisirent, et tous mes os tremblèrent.
4:15	Un esprit passa devant moi et fit hérisser les cheveux de ma chair.
4:16	Il se tint là et je ne reconnus pas sa face. Une représentation était devant mes yeux. Et j'entendis une voix basse :
4:17	L'homme serait-il juste devant Éloah ? L'homme serait-il pur devant celui qui l'a fait<!--Job 25:4.--> ?
4:18	Voici, il ne se fie pas à ses serviteurs, il impute des erreurs à ses anges<!--Job 15:15, 25:5 ; 2 Pi. 2:4.-->,
4:19	combien plus chez ceux qui habitent des maisons d'argile, qui ont leurs fondements dans la poussière, qui sont écrasés en présence d'une mite<!--Job 25:6.--> !
4:20	Du matin au soir ils sont brisés, et sans qu'on s'en aperçoive, ils périssent pour toujours.
4:21	L'excellence qui était en eux, n'a-t-elle pas été emportée ? Ils meurent sans être sages.

## Chapitre 5

5:1	Crie maintenant ! Y aura-t-il quelqu'un pour te répondre ? Et vers lequel des saints te tourneras-tu<!--Job 15:15.--> ?
5:2	En vérité, la colère tue le fou, et la jalousie fait mourir le naïf.
5:3	J'ai vu le fou qui s'enracinait<!--Jé. 12:1-2.-->, mais j'ai aussitôt maudit sa demeure.
5:4	Ses fils sont éloignés du salut ; ils sont écrasés à la porte, et personne ne les délivre<!--Ps. 119:155.--> !
5:5	Sa moisson est dévorée par des affamés qui viennent l'enlever même jusque dans les épines, et une trappe engloutit ses biens.
5:6	Le malheur ne sort pas de la poussière, et le travail ne germe pas de la terre.
5:7	Quoique l'être humain naisse pour être agité<!--Ge. 3:17-19 ; Job 14:1-5.-->, comme l'étincelle pour voler et s'élever.
5:8	Mais moi, j'aurais recours à El, et j'adresserais mes paroles à Elohîm.
5:9	Il fait des choses si grandes qu'on ne peut les sonder, et tant de choses merveilleuses qu'il est impossible de les compter<!--Ps. 72:18, 92:5 ; Job 9:10.-->.
5:10	Il répand la pluie sur la face de la Terre, et envoie les eaux sur les campagnes<!--De. 28:12 ; Ps. 135:7 ; Job 28:26, 38:25-26 ; Ac. 14:17.-->.
5:11	Il place en haut ceux qui sont abaissés, et ceux qui sont en deuil sont élevés jusqu'au salut<!--1 S. 2:7 ; Ez. 21:31 ; Ps. 113:7-8.-->.
5:12	Il anéantit les projets des hommes rusés, si bien que leurs mains ne peuvent les accomplir avec succès<!--Es. 8:10 ; Ps. 33:10 ; Né. 4:15.-->.
5:13	Il surprend les sages dans leur propre ruse<!--1 Co. 3:19.-->, et le conseil des méchants est renversé.
5:14	En plein jour, ils rencontrent les ténèbres, à midi, ils tâtonnent comme dans la nuit<!--De. 28:29.-->.
5:15	Mais il délivre les pauvres de l'épée de leur bouche et de la main des puissants<!--Ps. 12:3-4, 52:2, 57:4.-->.
5:16	Ainsi il arrive au pauvre ce qu'il a espéré<!--1 S. 2:8.-->, mais l'injuste a la bouche fermée<!--Es. 52:15 ; Ps. 63:12, 107:42 ; Pr. 10:6.-->.
5:17	Voici, heureux est l'homme qu'Éloah châtie ! Ne rejette donc pas le châtiment de Shaddaï<!--Ps. 94:12 ; Pr. 3:11-12 ; Hé. 12:5-6 ; Ap. 3:19.-->.
5:18	Car c'est lui qui fait la plaie et qui la bande. Il blesse et ses mains guérissent<!--De. 32:39 ; 1 S. 2:6-7 ; cp. Es. 30:26 ; Os. 6:1.-->.
5:19	Il te délivrera dans six afflictions, et à la septième le mal ne te touchera pas<!--Ps. 34:20, 91:3 ; Pr. 24:16.-->.
5:20	En temps de famine il te garantira de la mort, et en temps de guerre il te préservera de l'épée<!--Ps. 33:19, 37:19.-->.
5:21	Tu seras à l'abri du fléau de la langue, et tu n'auras pas peur de la dévastation quand elle arrivera<!--Ps. 31:21.-->.
5:22	Tu riras de la dévastation et de la famine, et tu n'auras pas peur des bêtes de la Terre<!--Es. 65:25 ; Ez. 34:25 ; Os. 2:20.-->.
5:23	Car tu feras une alliance avec les pierres des champs, et tu seras en paix avec les bêtes sauvages<!--Os. 2:20.-->.
5:24	Tu connaîtras la paix sous ta tente, tu visiteras tes troupeaux et il ne te manquera rien.
5:25	Tu verras ta postérité s'accroître, et tes descendants se multiplier comme l'herbe de la Terre<!--Ps. 72:16, 127:3-5, 128:6.-->.
5:26	Tu entreras au sépulcre dans la vieillesse, comme un monceau de gerbes s'entasse en sa saison<!--Pr. 9:11, 10:27.-->.
5:27	Voilà nous avons examiné la chose : il en est ainsi ! Écoute-le et sache-le pour toi-même !

## Chapitre 6

### Réponse de Iyov

6:1	Mais Iyov prit la parole et dit :
6:2	Si seulement il était possible de peser, de peser mon irritation, si l'on mettait ensemble mes calamités sur une balance !
6:3	Car elles seraient plus pesantes que le sable de la mer. C'est pourquoi mes paroles sont englouties<!--Pr. 27:3.--> !
6:4	Parce que les flèches de Shaddaï sont sur moi, mon esprit en suce le venin. Les terreurs<!--Job 30:15 ; Ps. 88:16-17.--> d'Éloah se dressent en bataille contre moi<!--Job 19:12 ; Ps. 38:2-3.-->.
6:5	L'âne sauvage<!--Job 39:8.--> brait-il auprès de l'herbe ? Le bœuf mugit-il auprès de son fourrage ?
6:6	Mange-t-on sans sel ce qui est fade ? Trouve-t-on du goût dans un blanc d'œuf ?
6:7	Mais pour moi les choses que je n'aurais pas seulement voulu toucher sont des saletés qu'il faut que je mange !
6:8	Qui fera que ma requête parvienne et qu'Éloah me donne ce que j'espère !
6:9	Qu'Éloah consente donc à m'écraser, qu'il secoue sa main et me brise<!--Job 7:16, 9:21, 10:1 ; cp. No. 11:15 ; 1 R. 19:4 ; Jon. 4:3,8.--> !
6:10	Mais j'ai encore cette consolation, quoique la douleur me consume, et qu'elle ne m'épargne pas, je n'ai pas transgressé les paroles du Saint<!--Yéhoshoua (Jésus) est le Saint d'Israël. Voir Mc. 1:24 ; Lu. 1:35, 4:34 et le commentaire en Ac. 3:14.-->.
6:11	Aurai-je encore la force d'espérer ? Quelle sera ma fin pour que je prolonge ma vie ?
6:12	Ma force est-elle une force de pierre ? Ma chair est-elle d'acier ?
6:13	Ne suis-je pas destitué de secours, et tout appui n'est-il pas éloigné de moi ?
6:14	Celui qui désespère a droit à la compassion de son ami, sinon il abandonnera la crainte<!--Ps. 19:10.--> de Shaddaï<!--Pr. 17:17.-->.
6:15	Mes frères m'ont trahi comme un torrent, comme le cours impétueux des torrents qui passent<!--Ps. 38:12, 41:10, 69:9 ; Jé. 15:19.-->,
6:16	qu'on ne voit pas à cause de la glace et sur lesquels s'entasse la neige.
6:17	Mais au temps de la chaleur, ils tarissent. Quand il commence à faire chaud, ils se dessèchent sur place.
6:18	Ils serpentent çà et là par les chemins, se réduisent à rien et se perdent.
6:19	Les troupes des voyageurs de Théma<!--Ge. 25:15.--> comptaient sur eux, ceux qui vont à Séba<!--1 R. 10:1 ; Ps. 72:10 ; Ez. 27:22-23.--> espéraient en eux ;
6:20	mais ils sont honteux d'avoir eu confiance, ils sont venus là, et ont été confondus.
6:21	Certes, vous êtes devenus inutiles pour moi. Vous voyez ma terreur et vous avez pris peur<!--Job 19:13 ; Ps. 31:12.--> !
6:22	Mais vous ai-je dit : Donnez-moi quelque chose ! Avec vos biens, faites des présents en ma faveur !
6:23	Délivrez-moi de la main de l'ennemi, et rachetez-moi de la main des méchants !
6:24	Instruisez-moi, et je me tairai. Faites-moi comprendre en quoi je me suis égaré.
6:25	Ô combien sont fortes les paroles de vérité ! Mais votre censure, à quoi tend-elle ?
6:26	Pensez-vous qu'il ne faille avoir que des paroles pour corriger, et que les discours d'un désespéré ne soient que du vent<!--Ec. 9:16.--> ?
6:27	Vous vous jetez même sur un orphelin et vous persécutez votre ami.
6:28	Mais maintenant je vous prie, regardez-moi bien, mentirais-je en votre présence ?
6:29	Revenez<!--Job 17:10.--> donc, je vous en prie, soyez sans injustice ! Revenez encore, ma justice est là<!--Job 27:5-6, 34:5 ; cp. Job 23:10, 42:1-6.-->.
6:30	Y a-t-il de l'injustice sur ma langue ? Et mon palais ne sait-il pas discerner mes calamités ?

## Chapitre 7

7:1	N'est-ce pas un temps de guerre que vit l'homme sur Terre ? Et ses jours ne sont-ils pas comme les jours d'un mercenaire ?
7:2	Comme l'esclave soupire après l'ombre, comme un ouvrier<!--Es. 16:14.--> attend son salaire<!--Ps. 39:5.-->,
7:3	ainsi il m'a été donné pour mon partage des mois qui ne m'apportent rien ; il m'a été assigné des nuits de travail<!--Ps. 6:6.-->.
7:4	Si je suis couché, je dis : Quand me lèverai-je ? Et quand est-ce que la nuit aura achevé sa mesure ? Et je suis plein d'inquiétudes jusqu'au point du jour<!--De. 28:67.-->.
7:5	Ma chair se couvre de vers et de monceaux de poussière, ma peau se crevasse et se dissout.
7:6	Mes jours passent plus légèrement que la navette d'un tisserand, et ils se consument sans espérance<!--Es. 38:12 ; Job 9:25, 17:11 ; Ja. 4:14.--> !
7:7	Souviens-toi que ma vie est un souffle ! Et que mes yeux ne reverront plus le bonheur<!--Es. 40:6 ; Ps. 78:39, 89:48, 102:12, 103:15 ; Job 8:9, 14:1-2 ; 1 Pi. 1:24.-->.
7:8	L'œil de ceux qui me regardent ne me verra plus. Tes yeux seront sur moi, et je ne serai plus.
7:9	La nuée se dissipe et s'en va, ainsi celui qui descend au shéol<!--Cp. Ha. 2:5 ; Lu. 16:23.--> ne remontera pas<!--Job 10:21-22, 14:7-14.--> ;
7:10	il ne reviendra plus dans sa maison, et le lieu qu'il habitait ne le reconnaîtra plus<!--Ps. 37:35-36, 103:16 ; Job 10:21.-->.
7:11	C'est pourquoi je ne retiendrai pas ma bouche, je parlerai dans l'angoisse de mon esprit, je discourrai dans l'amertume de mon âme<!--Job 10:1.-->.
7:12	Suis-je une mer ? Suis-je un monstre marin pour que tu poses autour de moi des gardes ?
7:13	Quand je dis : Mon lit me consolera, ma couche portera ma plainte,
7:14	alors tu me brises par des rêves, et tu me terrifies par des visions.
7:15	C'est pourquoi je choisirais d'être étranglé, et de mourir, plutôt que de conserver mes os.
7:16	Je refuse ! Je ne vivrai pas éternellement ! Retire-toi de moi, car mes jours ne sont que vanité<!--Job 10:20.-->.
7:17	Qu'est-ce que l'homme mortel pour que tu le regardes comme quelque chose de grand, pour que tu fixes sur lui ton cœur<!--Ps. 8:5, 144:3 ; Hé. 2:6.-->,
7:18	pour que tu le visites tous les matins, pour que tu l'éprouves<!--Job 23:10.--> à chaque instant ?
7:19	Quand détourneras-tu de moi ton regard ? Ne me permettras-tu pas d'avaler ma salive<!--Job 9:18.--> ?
7:20	Ai-je péché ? Qu'est-ce que cela te fait, gardien des humains<!--1 Ti. 4:10.--> ? Pourquoi m'as-tu pris pour cible ? Pourquoi suis-je à charge à moi-même ?
7:21	Et pourquoi ne pardonnes-tu pas ma transgression, et ne fais-tu pas passer mon iniquité ? Car je vais maintenant dormir dans la poussière. Tu me chercheras, et je ne serai plus.

## Chapitre 8

### Premier discours de Bildad

8:1	Alors Bildad de Shouah prit la parole et dit :
8:2	Jusqu'à quand parleras-tu ainsi, et les paroles de ta bouche seront-elles un vent impétueux<!--Job 15:2.--> ?
8:3	El renverserait-il le droit<!--Cp. Ge. 18:25.-->, et Shaddaï renverserait-il la justice<!--De. 32:4 ; Job 34:12 ; Da. 9:14 ; 2 Ch. 19:7.--> ?
8:4	Si tes fils ont péché contre lui, il les a livrés à leur transgression.
8:5	Si toi tu cherches El, si tu demandes grâce à Shaddaï<!--Cp. Job 5:17-27.--> ;
8:6	si tu es pur et droit, certainement il se réveillera pour toi, et fera prospérer la demeure de ta justice.
8:7	Et ton commencement<!--Za. 4:10.--> aura été petit, mais ta dernière condition sera bien plus grande<!--Job 42:12.-->.
8:8	Car, je te prie, interroge les générations précédentes, et applique-toi à t'informer soigneusement de leurs pères<!--De. 4:32, 32:7.-->.
8:9	Car nous sommes d'hier, et nous ne savons rien, parce que nos jours sont sur la Terre comme une ombre<!--Ps. 102:12, 144:4 ; 1 Ch. 29:15.-->.
8:10	Ceux-là ne t'enseigneront-ils pas, ne te parleront-ils pas, et ne tireront-ils pas de leur cœur des discours ? :
8:11	Le roseau croît-il sans marais ? Le papyrus pousse-t-il sans eau ?
8:12	Ne se flétrira-t-il pas même avant toute herbe<!--Cp. Jé. 17:5-8 ; Ps. 129:6.-->, bien qu'il soit encore dans sa verdure, et qu'on ne le cueille pas ?
8:13	Telles sont les sentiers de tous ceux qui oublient El<!--Ps. 9:18.-->, et l'espérance de l'athée périra<!--Ps. 1:4, 112:10 ; Pr. 10:28 ; Job 11:20, 27:8.-->.
8:14	Son espérance est brisée, et sa confiance est comme une toile d'araignée.
8:15	Il s'appuie sur sa maison mais elle ne tient pas, il la fortifie mais elle ne reste pas debout.
8:16	Mais l'homme intègre est plein de vigueur étant exposé au soleil, et ses jets poussent par-dessus son jardin,
8:17	ses racines s'entrelacent près de la fontaine, et il voit la maison de pierres.
8:18	Si on l'engloutit loin de son lieu, celui-ci le renie : Je ne t'ai jamais vu !
8:19	Telle est la joie que ses voies lui procurent. Puis sur le même sol, d'autres s'élèvent après lui.
8:20	El ne rejette pas l'homme parfait, il ne fortifie pas la main des méchants<!--Job 4:7.-->.
8:21	Il remplira de nouveau ta bouche de rires, et tes lèvres de cris de joie<!--Ps. 126:2.-->.
8:22	Ceux qui te haïssent seront revêtus de honte, et la tente des méchants ne sera plus<!--Ps. 35:26, 109:29.-->.

## Chapitre 9

### Réponse de Iyov

9:1	Mais Iyov prit la parole et dit :
9:2	En vérité, je sais qu'il en est ainsi. Comment l'homme mortel se justifierait-il<!--Ha. 2:4 ; Ga. 3:11 ; Ro. 1:17 ; Hé. 10:38.--> devant El<!--Ps. 25:4, 143:2 ; Job 15:14-16 ; Da. 9:11 ; Ro. 3:19.--> ?
9:3	S'il voulait contester avec lui, sur mille choses, il ne saurait lui répondre sur une seule<!--Es. 45:9-10.-->.
9:4	Elohîm est sage de cœur, et puissant en force. Qui peut s'endurcir contre lui et rester en paix<!--Job 12:13, 36:5, 37:23.--> ?
9:5	Il transporte les montagnes, et quand il les renverse dans sa fureur, elles ne s'en aperçoivent pas<!--Ps. 144:5.-->.
9:6	Il remue la Terre de sa place, et ses colonnes sont ébranlées<!--Ag. 2:6,21 ; Hé. 12:26.-->.
9:7	Il parle au soleil, et il ne se lève pas, il met un sceau sur les étoiles<!--Jos. 10:12.-->.
9:8	C'est lui seul qui étend les cieux<!--Ge. 1:6-8 ; Es. 44:24, 51:13 ; Ps. 104:2.-->, qui marche sur les hauteurs de la mer<!--Cp. Mt. 14:25.-->.
9:9	Il a fait la grande ourse, l'orion, les pléiades, et les étoiles des régions australes<!--Ge. 1:16 ; Am. 5:8 ; Ps. 89:12 ; Job 38:31-32.-->.
9:10	Il fait de grandes choses qu'on ne peut sonder, des merveilles sans nombre<!--Ps. 86:10, 139:6,17-18 ; Job 5:9, 37:5.-->.
9:11	Voici, il passe près de moi, et je ne le vois pas. Il passe encore, et je ne l'aperçois pas<!--Job 23:8-9, 35:14.-->.
9:12	Voilà, s'il enlève qui l'en détournera ? Et qui lui dira : Que fais-tu<!--Es. 45:9-10 ; Da. 4:35 ; Ro. 11:33-35.--> ?
9:13	Éloah ne revient pas sur sa colère, les orgueilleux qui viennent au secours s'inclinent sous lui<!--Job 26:12 ; Cp. Es. 30:7.-->.
9:14	Combien moins lui répondrais-je, moi, et comment choisirais-je mes paroles contre lui ?
9:15	Moi, je ne lui répondrai pas, quand bien même je serais juste, je demanderai grâce à mon juge<!--Job 23:1-7.-->.
9:16	Si je l'invoque et qu'il me réponde, je ne croirais pas qu'il ait écouté ma voix,
9:17	car il m'a écrasé du milieu d'un tourbillon, et il a ajouté plaie sur plaie, sans que je l'aie mérité<!--Job 6:29.-->,
9:18	il ne me permet pas de reprendre mon souffle, il me rassasie d'amertume<!--Job 7:19.-->.
9:19	S'il s'agit de force, voici, il est fort ! S'il s'agit de jugement, qui m'assignera ?
9:20	Si j'étais juste, ma propre bouche me condamnerait. Si j'étais parfait, elle prouverait ma perversité.
9:21	Suis-je parfait ? Mon âme ne le sait même pas. Je méprise ma vie<!--Job 10:1.-->.
9:22	Tout revient à une chose ! C'est pourquoi j'ai dit : Il consume aussi bien le parfait que le méchant<!--Cp. Ez. 21:3 ; Ec. 9:2-3 ; Mt. 5:45.-->.
9:23	Si un fléau donne soudainement la mort, il se rit de l'épreuve des innocents.
9:24	La Terre est livrée entre les mains du méchant, il couvre la face des juges de la Terre. Si ce n'est pas lui, qui est-ce donc ?
9:25	Or mes jours vont plus vite qu'un courrier, ils s'enfuient sans avoir vu le bien<!--Job 7:6-7.-->.
9:26	Ils avancent avec les barques de jonc, comme un aigle qui se précipite sur sa proie.
9:27	Je dis : J'oublierai ma plainte, je renoncerai à ma colère, je me fortifierai,
9:28	j'ai peur de toutes mes douleurs. Je sais que tu ne me considéreras pas comme innocent<!--Cp. Ps. 130:3.-->.
9:29	Je serai un méchant. Pourquoi travaillerais-je en vain ?
9:30	Quand je me laverais dans de l'eau de neige, et que je purifierais mes mains avec des produits pour nettoyer<!--Jé. 2:22.-->,
9:31	tu me plongerais dans le fossé et mes vêtements m'auraient en horreur.
9:32	Car il n'est pas un homme comme moi, pour que je lui réponde, et que nous allions ensemble en jugement<!--Es. 45:9 ; Jé. 49:19 ; Ec. 6:10 ; Ro. 9:20.-->.
9:33	Il n'y a personne qui prend connaissance de la cause qui serait entre nous, et qui met la main sur nous deux<!--Cp. 1 S. 2:25.-->.
9:34	Qu'il ôte sa verge de dessus moi, et que la frayeur que j'ai de lui ne me terrifie plus.
9:35	Je parlerai sans avoir peur de lui, mais ce n'est pas le cas et je reste seul avec moi-même !

## Chapitre 10

10:1	Mon âme est dégoûtée de ma vie ! Je m'abandonnerai à ma plainte, je parlerai dans l'amertume de mon âme.
10:2	Je dirai à Éloah : Ne me condamne pas ! Montre-moi pourquoi tu contestes avec moi !
10:3	Prends-tu plaisir à m'opprimer, à rejeter l'œuvre de tes mains, et à faire briller le conseil des méchants<!--Es. 64:7-8.--> ?
10:4	As-tu des yeux de chair ? Vois-tu comme voit un homme mortel ?
10:5	Tes jours sont-ils comme les jours de l'homme ? Tes années sont-elles comme les jours de l'homme,
10:6	pour que tu recherches mon iniquité, et que tu t'informes de mon péché ?
10:7	Tu sais que je ne suis pas méchant, et qu'il n'y a personne qui peut délivrer de ta main.
10:8	Tes mains m'ont formé, elles ont façonné toutes les parties de mon corps et tu m'engloutirais<!--Ge. 2:7 ; Ps. 119:73, 139:14-15.--> !
10:9	Souviens-toi, je te prie, que tu m'as façonné comme de l'argile<!--Ro. 9:20-21.-->, et que tu me feras retourner à la poussière.
10:10	Ne m'as-tu pas coulé comme du lait ? Et ne m'as-tu pas fait cailler comme un fromage ?
10:11	Tu m'as revêtu de peau et de chair, et tu m'as composé d'os et de nerfs.
10:12	Tu m'as donné la vie, et tu as usé de miséricorde envers moi. Tes soins continuels ont gardé mon esprit.
10:13	Et cependant tu gardais ces choses dans ton cœur ; mais je sais que cela était devant toi.
10:14	Si je pèche, tu m'observes et tu ne me tiens pas pour innocent de mon iniquité.
10:15	Si j'agis méchamment, malheur à moi ! Si je suis juste, je n'en lève pas la tête plus haut. Je suis rempli d'ignominie, mais regarde mon affliction.
10:16	Si je redresse la tête, tu me fais la chasse comme un lion. Tu y reviens et tu te montres merveilleux dans mon cas<!--Es. 38:13 ; La. 3:10.-->.
10:17	Tu renouvelles tes témoins contre moi, et tu multiplies ta colère contre moi. Tes armées se succèdent contre moi.
10:18	Mais pourquoi m'as-tu fait sortir de la matrice ? J'aurais expiré, et aucun œil ne m'aurait vu ;
10:19	et j'aurais été comme n'ayant jamais existé, et j'aurais été porté du ventre à la tombe.
10:20	Mes jours ne sont-ils pas en petit nombre ? Cesse donc de me fixer, pour que je puisse sourire un peu.
10:21	Avant que j'aille au lieu d'où je ne reviendrai plus, dans la terre de ténèbres et de l'ombre de la mort,
10:22	terre d'une grande obscurité, comme les ténèbres de l'ombre de la mort, où il n'y a aucun ordre, et où ne brille que des ténèbres.

## Chapitre 11

### Première accusation de Tsophar

11:1	Alors Tsophar de Naama prit la parole et dit :
11:2	Ne répondrait-on pas à la multitude de paroles ? L'homme éloquent serait-il juste ?
11:3	Tes vains discours feront-ils taire les gens ? Et quand tu te seras moqué, n'y aura-t-il personne qui te fasse honte ?
11:4	Car tu as dit : Mon enseignement est pur et je suis sans tache devant tes yeux !
11:5	Mais si Éloah voulait parler, ouvrir les lèvres contre toi,
11:6	s'il te racontait les secrets de sa sagesse, le double de sa sagesse, tu saurais qu'Éloah oublie une partie de ton iniquité.
11:7	Trouveras-tu le fond en Éloah en le sondant ? Connaîtras-tu parfaitement Shaddaï ?
11:8	Ce sont les hauteurs des cieux : Que feras-tu ? C'est plus profond que le shéol : Que pourras-tu en savoir ?
11:9	Son étendue est plus longue que la Terre et plus large que la mer.
11:10	S'il passe, s'il emprisonne, s'il convoque un tribunal, qui l'en empêchera ?
11:11	Car il connaît les hommes vains, il discerne par le regard les coupables<!--Ps. 10:11-14, 35:22.-->.
11:12	Mais l'homme vide de sens devient intelligent, quoique l'être humain naisse comme un ânon sauvage<!--Ec. 3:18.-->.
11:13	Si tu disposes ton cœur, et que tu étends tes mains vers lui,
11:14	si tu éloignes de toi l'injustice qui est dans ta main, et si tu ne permets pas que la méchanceté habite dans tes tentes,
11:15	alors certainement tu pourras élever ton visage sans tache. Tu seras ferme et tu ne craindras rien ;
11:16	tu oublieras tes peines, tu t'en souviendras comme des eaux écoulées.
11:17	La durée de la vie se lèvera pour toi plus brillante que le midi, et l'obscurité même sera comme le matin<!--Ps. 37:6, 112:4.-->.
11:18	Tu seras plein de confiance parce qu'il y aura de l'espérance. Tu creuseras, et tu reposeras sûrement<!--Lé. 26:6 ; Ps. 3:6 ; Pr. 3:24.-->.
11:19	Tu te coucheras, et il n'y aura personne pour te faire peur et beaucoup te caresseront le visage.
11:20	Mais les yeux des méchants seront consumés, leur refuge sera détruit. Et Leur espérance sera l'expiration de l'âme !

## Chapitre 12

### Réplique de Iyov

12:1	Mais Iyov reprit la parole et dit :
12:2	Vraiment, vous êtes tout un peuple, et avec vous mourra la sagesse !
12:3	Moi aussi, j'ai un cœur comme vous. Je ne vous suis pas inférieur. Qui n’a pas autant que ceux-là ?
12:4	Je deviens un objet de raillerie pour mes amis, moi qui invoque Éloah et à qui il répond. Le juste, l'innocent, un objet de raillerie !
12:5	Celui dont les pieds sont tout prêts de glisser est une lampe méprisée pour les pensées de celui qui est à son aise.
12:6	Les tentes des dévastateurs prospèrent, ceux qui provoquent El sont en sécurité, eux qui amènent un Éloah dans leurs mains<!--Jé. 12:1 ; Ps. 73:12.-->.
12:7	Mais interroge les bêtes, je te prie : elles te l'enseigneront, les oiseaux du ciel, et ils te le diront ;
12:8	parle à la Terre, et elle t'enseignera. Même les poissons de la mer te le raconteront.
12:9	Qui ne sait pas, parmi eux tous, que c'est la main de YHWH qui a fait cela ?
12:10	Lui dans la main duquel est l’âme de tout vivant, l'esprit de toute chair d'homme.
12:11	L'oreille ne discerne-t-elle pas les discours, ainsi que le palais savoure les mets ?
12:12	La sagesse est dans les vieillards, et l'intelligence est le fruit d'une longue vie.
12:13	Mais en Elohîm est la sagesse et la force, c'est à lui qu'appartiennent le conseil et l'intelligence<!--Da. 2:20.-->.
12:14	Voici, il démolit, et on ne rebâtit pas. Il enferme un homme et on ne lui ouvre pas<!--Es. 22:22 ; Ap. 3:7.-->.
12:15	Voici, il retient les eaux, et tout devient sec. Puis il les envoie, et elles renversent la Terre.
12:16	En lui résident la force et la sagesse, à lui est celui qui s'égare, et celui qui le fait égarer.
12:17	Il fait marcher nu-pieds les conseillers et rend fous les juges<!--2 S. 15:31, 17:14-23 ; Es. 19:12, 29:14 ; 1 Co. 1:19.-->.
12:18	Il libère du châtiment des rois. Il met une corde des prisonniers autour de leurs reins.
12:19	Il fait marcher nu-pieds les prêtres et il renverse les forts.
12:20	Il ôte le langage aux hommes les plus sûrs, et il enlève le jugement aux anciens.
12:21	Il répand le mépris sur les grands, il relâche la ceinture des forts<!--Es. 40:23.-->.
12:22	Il met en évidence les choses qui étaient cachées dans les ténèbres, et il met en lumière l'ombre de la mort<!--Ps. 139:11-12 ; Ec. 12:16 ; Mt. 10:26 ; 1 Co. 4:6.-->.
12:23	Il agrandit les nations et les fait périr. Il élargit les nations, puis il les ramène.
12:24	Il ôte le cœur aux chefs des peuples de la Terre, et les fait errer dans les déserts où il n'y a pas de chemin.
12:25	Ils tâtonnent dans les ténèbres, sans aucune clarté, et il les fait chanceler comme des gens ivres.

## Chapitre 13

13:1	Voici, mon œil a vu toutes ces choses, mon oreille les a entendues et comprises.
13:2	Ce que vous savez, je le sais moi aussi, je ne vous suis pas inférieur.
13:3	Mais je veux parler à Shaddaï, je veux plaider auprès de El.
13:4	Car vous inventez des mensonges, vous êtes tous des médecins sans valeur.
13:5	Qui donc vous fera taire, vous fera taire, cela vous servirait de sagesse<!--Pr. 17:28.--> !
13:6	Écoutez donc maintenant mon argument et soyez attentifs à la controverse de mes lèvres.
13:7	Est-ce pour El que vous proférez l'injustice, c'est pour lui que vous proférez la tromperie ?
13:8	Ferez-vous acception de sa personne, si vous plaidez la cause de El ?
13:9	Serait-il bon qu'il vous sonde ? Vous jouerez-vous de lui, comme on se joue d'un homme mortel ?
13:10	Il vous corrigera, vous corrigera<!--Voir commentaire en Ge. 2:16-17.-->, même si en secret vous faites acception de personnes.
13:11	Sa majesté ne vous terrifiera-t-elle pas ? Et sa frayeur ne tombera-t-elle pas sur vous ?
13:12	Vos souvenirs sont des proverbes de cendre, vos remparts sont des remparts d'argile !
13:13	Taisez-vous devant moi afin que je parle. Et peu importe alors ce qui m'arrivera.
13:14	Pourquoi prendrais-je ma chair entre mes dents et mettrais-je mon âme dans la paume de ma main<!--Jg. 12:3 ; 1 S. 19:5.--> ?
13:15	Voici, il peut me tuer, je n'espère plus rien. Mais je défendrai mes voies devant lui.
13:16	D'ailleurs, il sera lui-même ma délivrance. Mais l'athée ne viendra pas devant sa face<!--Ps. 1:5.-->.
13:17	Écoutez, écoutez mes paroles et prêtez l'oreille à ce que je vais vous déclarer !
13:18	Voici, j'ai préparé ma cause. Je sais que je serai justifié.
13:19	Qui voudrait contester avec moi ? Dès maintenant, je me tairais et je mourrais.
13:20	Seulement ne me fais pas ces deux choses, et alors je ne me cacherai pas devant ta face :
13:21	Retire ta main de dessus moi et que ta frayeur ne me terrifie pas.
13:22	Puis appelle-moi et je répondrai, ou bien je parlerai et tu me répondras.
13:23	Combien ai-je d'iniquités et de péchés ? Fais-moi connaître ma transgression et mon péché.
13:24	Pourquoi caches-tu ta face, et me tiens-tu pour ton ennemi ?
13:25	Déploieras-tu tes forces contre une feuille que le vent emporte ? Poursuivras-tu du chaume tout sec<!--1 S. 24:15.-->,
13:26	pour que tu écrives contre moi des choses amères<!--Ps. 25:7.-->,
13:27	pour que tu mettes mes pieds aux ceps, et observes tous mes chemins, et que tu suives les traces de mes pieds ?
13:28	Et pourtant, il s'use comme une chose pourrie, comme un vêtement que dévore la mite.

## Chapitre 14

14:1	L'être humain né de la femme ! Sa vie est courte et remplie d'agitations<!--Ps. 102:12, 103:15, 144:4 ; Ja. 4:14.-->.
14:2	Il sort comme une fleur, puis il est coupé, et il s'enfuit comme une ombre qui ne s'arrête pas<!--Es. 40:6 ; Ps. 90:6 ; 1 Pi. 1:24.-->.
14:3	Cependant c'est sur lui que tu ouvres les yeux, et tu me conduis en justice avec toi !
14:4	Qui peut faire sortir le pur de l'impur ? Personne<!--Es. 48:8 ; Pr. 22:15.-->.
14:5	Les jours de l'homme sont déterminés, le nombre de ses mois est entre tes mains. Tu lui as prescrit ses limites et il ne passera pas au-delà.
14:6	Retire-toi de lui, afin qu'il ait du relâche, jusqu'à ce que comme un mercenaire il ait achevé sa journée.
14:7	Car si un arbre est coupé, il y a de l'espérance : il repoussera encore et ne cessera d'avoir des jeunes pousses,
14:8	quoique sa racine ait vieilli dans la terre, et que son tronc soit mort dans la poussière.
14:9	Dès qu'il sent l'eau, il pousse de nouveau, et il produit des branches comme un arbre nouvellement planté.
14:10	Mais l'homme meurt et perd toute sa force, l'être humain expire et puis où est-il ?
14:11	Les eaux de la mer s'évaporent, le fleuve tarit et se dessèche.
14:12	Ainsi l'homme se couche pour ne plus se relever. Jusqu'à ce qu'il n'y ait plus de cieux, il ne se réveillera pas et ne sera pas réveillé de son sommeil.
14:13	Oh ! si tu me cachais dans le shéol, si tu me gardais à l'abri jusqu'à ce que ta colère soit passée ! si tu me fixais un terme après lequel tu te souviendrais de moi !
14:14	Si l'homme meurt, revivra-t-il ? J'attendrai donc tous les jours de mon combat, jusqu'à ce qu'il m'arrive du changement.
14:15	Tu appellerais et je te répondrais. Tu languirais après l'œuvre de tes mains.
14:16	Mais maintenant tu comptes mes pas, et tu veilles sur mon péché<!--Ps. 56:9, 139:2-4 ; Pr. 5:21.-->.
14:17	Mes transgressions sont scellées comme dans un sac, et tu as cousu ensemble mes iniquités<!--Os. 13:12.-->.
14:18	Mais la montagne même tombe et périt, le rocher est transporté de sa place,
14:19	les eaux broient les pierres et entraînent par leur débordement la poussière de la terre avec tout ce qu'elle a produit. De la même manière, tu détruis l'espérance de l'homme mortel.
14:20	Tu te montres toujours plus fort que lui, et il s'en va ; tu changes sa face et tu le renvoies au loin.
14:21	Que ses enfants soient honorés, il n'en sait rien. Qu'ils soient abaissés, il ne s'en soucie pas.
14:22	C'est dans sa chair à lui qu'il souffre, c'est son âme à lui qui se lamente.

## Chapitre 15

### Deuxième discours d'Éliphaz

15:1	Alors Éliphaz de Théman prit la parole et dit :
15:2	Un homme sage profère-t-il dans ses réponses une connaissance aussi légère que le vent, des opinions vaines ? Remplit-il son ventre du vent d'orient ?
15:3	Discute-t-il avec des discours inutiles, avec des paroles qui ne servent à rien ?
15:4	Tu anéantis en effet la piété, tu restreins la méditation devant El.
15:5	Car ton iniquité enseigne ta bouche, et tu as choisi un langage trompeur.
15:6	C'est ta bouche qui te condamne, et non pas moi, tes lèvres témoignent contre toi.
15:7	Es-tu le premier être humain né ? Ou as-tu été formé avant les montagnes<!--Ps. 90:2 ; Pr. 8:25.--> ?
15:8	As-tu entendu ce qui se dit dans le conseil secret d'Éloah ? Limites-tu la sagesse à toi-même<!--Es. 40:13 ; Jé. 23:18 ; Ro. 11:34.--> ?
15:9	Que sais-tu que nous ne sachions pas ? Quelle connaissance as-tu que nous n'ayons pas ?
15:10	Il y a parmi nous des cheveux blancs, des hommes âgés, plus riches de jours que ton père.
15:11	Tiens-tu pour peu de chose les consolations de El et les douces paroles que nous t'adressons ? ...
15:12	Pourquoi ton cœur t'emporte-t-il ? Et pourquoi clignes-tu les yeux ?
15:13	Car c'est contre El que tu pousses ton souffle et que tu fais sortir de ta bouche de tels discours !
15:14	Qu'est-ce que l'homme mortel pour qu'il soit pur, et celui qui est né de femme pour qu'il soit juste<!--Ps. 14:3 ; Pr. 20:9 ; Ec. 7:20.--> ?
15:15	Voici, Elohîm ne se fie pas à ses saints et les cieux ne sont pas purs à ses yeux,
15:16	combien plus est abominable et corrompu l'homme qui boit l'injustice comme de l'eau !
15:17	Je t'enseignerai, écoute-moi et je te raconterai ce que j'ai vu,
15:18	ce que les sages ont déclaré, ce qui venait de leurs pères et qu'ils n'ont pas caché.
15:19	C'est à eux seuls que la Terre a été donnée, et parmi lesquels l'étranger n'est pas passé.
15:20	Le méchant est dans les douleurs de l'accouchement tous les jours de sa vie, quel que soit le nombre des années réservées au tyran<!--Es. 48:22, 57:21.-->,
15:21	un cri de terreur est dans ses oreilles : au milieu de la paix il croit que le destructeur se jette sur lui<!--1 Th. 5:3.-->.
15:22	Il ne croit pas pouvoir sortir des ténèbres, car il est toujours épié par l'épée.
15:23	Il court après le pain, en disant : Où y en a-t-il ? Il sait que le jour de ténèbres est tout prêt, et il le touche comme avec la main<!--Ps. 109:10.-->.
15:24	La détresse et l'angoisse le terrifient, elles l'assaillent comme un roi prêt à l'assaut,
15:25	parce qu'il a levé sa main contre El, et qu'il a agi orgueilleusement envers Shaddaï.
15:26	Il a couru contre lui le cou tendu, sous le dos épais de ses boucliers.
15:27	La graisse a couvert tout son visage, ses reins produisent du gras excessif.
15:28	Il habite des villes détruites, des maisons désertes, tout près de n'être plus que des monceaux de pierres.
15:29	Et il n'en sera pas plus riche, car ses biens ne subsisteront pas, et leur entassement ne se répandra pas sur la terre.
15:30	Il ne pourra pas sortir des ténèbres. La flamme séchera ses jeunes pousses, il s'écartera par le souffle de sa bouche.
15:31	Qu'il ne compte pas sur la vanité par laquelle il a été séduit, car la vanité sera sa récompense.
15:32	Cela s'accomplira avant le temps, ses branches ne reverdiront plus.
15:33	On arrachera ses fruits non mûrs comme à une vigne, on jettera sa fleur comme celle d'un olivier.
15:34	Car l'assemblée des athées est stérile et le feu dévore les tentes de ceux qui aiment les pots-de-vin.
15:35	Ils conçoivent le mal et enfantent la méchanceté, leur ventre prépare la tromperie<!--Es. 59:4 ; Os. 10:13.-->.

## Chapitre 16

### Réponse de Iyov

16:1	Mais Iyov répondit, et dit :
16:2	J'ai souvent entendu de pareils discours. Vous êtes tous des consolateurs de malheur !
16:3	N'y aura-t-il pas une fin à des paroles légères comme le vent ? Et qu'est-ce qui te fait souffrir pour que tu répondes de cette manière ?
16:4	Moi aussi, je parlerais comme vous, si vous étiez à ma place. Je pourrais entasser des paroles contre vous et secouer ma tête contre vous !
16:5	Je vous fortifierais par mes discours, et le mouvement de mes lèvres vous soulagerait.
16:6	Si je parle, ma douleur ne sera pas soulagée. Si je me tais, en sera-t-elle diminuée ?
16:7	Mais maintenant il m'a accablé. Tu as dévasté toute mon assemblée.
16:8	Tu m'as saisi ! Ma maigreur en est témoin. Elle se lève en moi et en rend témoignage sur mon visage.
16:9	Sa fureur me déchire, il se déclare mon ennemi, il grince des dents contre moi, et étant devenu mon ennemi, il aiguise ses yeux contre moi.
16:10	Ils ouvrent grandement leur bouche contre moi, ils m’insultent et me frappent les joues, ils se réunissent tous ensemble contre moi.
16:11	El m'a enfermé chez l'injuste, il me fait tomber entre les mains des méchants.
16:12	J'étais en repos et il m'a écrasé. Il m'a saisi par la nuque et m'a brisé, et il m'a établi pour lui servir de cible.
16:13	Ses archers m'ont environné, il me perce les reins et ne m'épargne pas, il répand mon fiel par terre.
16:14	Il m'a brisé en me faisant plaie sur plaie, il a couru sur moi comme un homme puissant.
16:15	J'ai cousu un sac sur ma peau, j’ai enfoncé ma corne dans la poussière<!--Ps. 44:25, 119:25.-->.
16:16	Mon visage est rougissant à force de pleurer et l'ombre de mort est sur mes paupières.
16:17	Quoiqu'il n'y ait pas d'iniquité dans mes mains et que ma prière soit pure.
16:18	Terre, ne couvre pas mon sang, et qu'il n'y ait pas de place pour mon cri !
16:19	Mais maintenant voici, mon témoin est dans les cieux, mon témoin est dans les lieux élevés<!--Ap. 1:5, 3:14.-->.
16:20	Mes amis sont des moqueurs : mais mon œil fond en larmes devant Éloah.
16:21	Ô si l'homme discutait avec Éloah comme un être humain avec son ami intime !
16:22	Car mes années dont le nombre est compté arrivent et je marche dans un chemin d'où je ne reviendrai plus.

## Chapitre 17

17:1	Mon souffle est corrompu, mes jours s'éteignent, le sépulcre m'attend !
17:2	Certes, il n'y a que des moqueurs auprès de moi, et mon œil veille toute la nuit dans les chagrins qu'ils me font.
17:3	Je t'en prie, place donc toi-même ma caution près de toi ! Qui donc me frappera dans la main<!--Pr. 6:1, 17:18.--> ?
17:4	Tu caches à leur cœur l'intelligence, c'est pourquoi tu ne les élèveras pas<!--De. 29:4 ; Mt. 11:25.-->.
17:5	Celui qui parle avec flatterie à ses amis intimes, les yeux mêmes de ses enfants seront consumés.
17:6	Il a fait de moi la fable des peuples, je suis devenu un crachat devant eux.
17:7	Mon œil est obscurci par le chagrin, et tous les membres de mon corps sont comme une ombre<!--Ps. 6:7, 31:10.-->.
17:8	Les hommes droits en sont étonnés, et l'innocent s'élève contre l'athée.
17:9	Mais le juste tient ferme dans sa voie, et celui qui a les mains pures croît en force.
17:10	Mais quant à vous, revenez ! Venez, je vous prie ! Car je ne trouve pas de sage parmi vous !
17:11	Mes jours sont passés, mes desseins sont rompus, et les pensées de mon cœur sont dissipées.
17:12	Ils changent la nuit en jour, et face aux ténèbres, ils disent que la lumière est proche !
17:13	Certes, je n'ai plus qu'à attendre le shéol qui va être ma maison, je dresserai mon lit dans les ténèbres.
17:14	Je crie à la fosse : Tu es mon père ! et aux vers : Vous êtes ma mère et ma sœur !
17:15	Où est donc mon espérance ? Mon espérance, qui peut la voir ?
17:16	Elle descendra au fond du shéol, lorsque ensemble nous aurons du repos dans la poussière.

## Chapitre 18

### Deuxième discours de Bildad

18:1	Alors Bildad de Shouah prit la parole et dit :
18:2	Quand finirez-vous ces discours ? Écoutez, et puis nous parlerons :
18:3	Pourquoi sommes-nous regardés comme des bêtes, et pourquoi nous tenez-vous pour souillés à vos yeux ?
18:4	Toi qui te déchires dans ta colère, la Terre sera-t-elle abandonnée à cause de toi, et les rochers seront-ils transportés de leur place ?
18:5	Certainement, la lumière des méchants sera éteinte, et la flamme de leur feu ne brillera plus<!--Ps. 37:9-10.-->.
18:6	La lumière s'obscurcira sous sa tente, et la lampe qui éclaire au-dessus d'eux sera éteinte.
18:7	Ses pas vigoureux seront à l'étroit et son conseil le renversera.
18:8	Car il sera enlacé par ses pieds dans les filets, et il marchera sur des rets.
18:9	Un piège le saisira au talon, et une trappe le saisissant en aura le dessus.
18:10	La corde qui l'attend est cachée dans la terre, et un instrument de capture est caché sur son sentier.
18:11	De toutes parts des terreurs le terrifieront et feront courir ses pieds çà et là<!--Jé. 6:25, 46:5, 49:29.-->.
18:12	Sa vigueur sera affamée, et la détresse se tiendra à ses côtés.
18:13	Le premier-né de la mort dévorera les parties de sa peau, il dévorera ses parties.
18:14	Sa confiance sera arrachée de sa tente, et il sera conduit vers le roi des terreurs.
18:15	On habitera dans sa tente : elle ne sera plus à lui. Le soufre sera répandu sur sa demeure.
18:16	Ses racines sécheront au-dessous, et ses branches seront coupées en haut.
18:17	Sa mémoire périra sur la Terre, et on ne parlera plus de son nom sur les places<!--Ps. 109:13 ; Pr. 10:7.-->.
18:18	On le chassera de la lumière dans les ténèbres, et il sera exterminé du monde.
18:19	Il n'aura ni fils ni petit-fils parmi son peuple, et il n'aura personne qui lui survive dans ses demeures<!--Es. 14:20-22 ; Jé. 22:30 ; Ps. 37:28, 109:13.-->.
18:20	Ceux de l'ouest seront stupéfaits de son jour et ceux de l'orient en seront saisis de terreur.
18:21	Certainement, telles seront les demeures du pervers, et telle sera la place de celui qui n'a pas connu El !

## Chapitre 19

### Réponse de Iyov

19:1	Mais Iyov prit la parole et dit :
19:2	Jusqu'à quand affligerez-vous mon âme et m'accablerez-vous de paroles ?
19:3	Voilà dix fois que vous m'insultez. N'avez-vous pas honte de me traiter faussement ?
19:4	Mais même s'il est vrai que j'ai péché, mon erreur demeure avec moi.
19:5	Mais si vraiment vous voulez parler avec hauteur contre moi, et me reprocher mon opprobre,
19:6	sachez donc que c'est Éloah qui m'a renversé et qui a tendu son filet autour de moi.
19:7	Voici, je crie pour la violence qui m'est faite et je ne suis pas exaucé. Je crie au secours, et il n'y a pas de justice !
19:8	Il a tellement fermé mon chemin que je ne peux passer. Il a mis des ténèbres sur mes sentiers.
19:9	Il m'a dépouillé de ma gloire, il a ôté la couronne de dessus ma tête.
19:10	Il m'a détruit de tous côtés, et je m'en vais. Il a fait disparaître mon espérance comme celle d'un arbre que l'on arrache.
19:11	Il s'est enflammé de colère contre moi, et m'a traité comme l'un de ses ennemis<!--La. 2:5.-->.
19:12	Ses troupes sont venues ensemble, et elles ont dressé leur chemin contre moi, et se sont campées autour de ma tente<!--La. 2:22.-->.
19:13	Il a éloigné de moi mes frères, et ceux qui me connaissaient se sont écartés comme des étrangers<!--Ps. 88:9.-->.
19:14	Mes proches m'ont abandonné, et ceux que je connaissais m'ont oublié.
19:15	Ceux qui demeurent dans ma maison et mes servantes me tiennent pour un inconnu, et me traitent comme un étranger.
19:16	J'appelle mon serviteur, mais il ne me répond pas, quoique je l'aie supplié de ma propre bouche.
19:17	Mon haleine est devenue dégoûtante à ma femme, quoique je la supplie par les fils de mes entrailles.
19:18	Je suis méprisé même par des enfants. Si je me lève, ils parlent contre moi.
19:19	Tous ceux à qui je déclarais mes secrets m'ont en abomination, et tous ceux que j'aimais se sont tournés contre moi<!--Ps. 55:13-14.-->.
19:20	Mes os sont attachés à ma peau et à ma chair, et il ne me reste d'entier que la peau de mes dents<!--La. 4:8.-->.
19:21	Ayez pitié de moi, ayez pitié de moi, vous, mes amis ! Car la main d'Éloah m'a frappé.
19:22	Pourquoi me poursuivez-vous comme El me poursuit, sans pouvoir vous rassasier de ma chair<!--Ps. 27:2.--> ?
19:23	Si seulement mes paroles pouvaient être écrites, si seulement elles pouvaient être gravées dans un livre !
19:24	Avec un stylet de fer et de plomb, qu’elles soient à jamais gravées dans le roc !
19:25	Car je sais que mon Rédempteur est vivant, et qu'il se lèvera le dernier sur la Terre.
19:26	Après que ma peau aura été détruite, sans ma chair je verrai Éloah<!--Ps. 17:15.--> !
19:27	Je le verrai moi-même, et mes yeux le verront, et non un autre. Mes reins se consument dans mon sein.
19:28	Vous devrez plutôt dire : Pourquoi le persécutons-nous ? Puisque le fondement de mes paroles se trouve en moi.
19:29	Ayez peur devant l'épée, car l'épée est l'instrument de la colère contre les iniquités. C'est pourquoi sachez qu'il y a un jugement.

## Chapitre 20

### Dernier discours de Tsophar

20:1	Alors Tsophar de Naama prit la parole et dit :
20:2	C'est à cause de cela que mes pensées diverses me poussent à répondre, et à agir rapidement.
20:3	J'ai entendu la correction qui me couvre de honte, mais mon discernement me souffle la réponse.
20:4	Ne sais-tu pas que de tout temps, depuis que l'être humain a été placé sur la Terre,
20:5	le cri de joie des méchants est de courte durée, et que la joie de l'athée n'est que pour un moment<!--Ps. 37:35-36.--> ?
20:6	Quand sa hauteur monterait jusqu'aux cieux, et que sa tête atteindrait les nuées,
20:7	il périra pour toujours comme ses ordures et ceux qui le voyaient diront : Où est-il ?
20:8	Il s'envolera comme un rêve, et on ne le trouvera plus. Il s'enfuira comme une vision de nuit<!--Ps. 73:19-20.--> ;
20:9	l'œil qui le regardait ne le verra plus, le lieu qu'il habitait ne le contemplera plus.
20:10	Ses enfants rechercheront la faveur des pauvres, et ses mains restitueront ce que sa violence a ravi<!--Ps. 109:10.-->.
20:11	Ses os étaient pleins de jeunesse, mais celle-ci se couchera avec lui dans la poussière.
20:12	Si le mal est doux à sa bouche, et s'il le cache sous sa langue,
20:13	s'il l'épargne et ne le rejette pas, mais le retient dans son palais,
20:14	sa nourriture se transformera dans ses entrailles, elle deviendra au dedans de lui du venin de cobra.
20:15	Il a englouti les richesses, mais il les vomira. El les jettera hors de son ventre.
20:16	Il sucera du venin de cobra, la langue de la vipère le tuera.
20:17	Il ne verra plus les ruisseaux, les fleuves, les torrents de miel et de lait.
20:18	Il rendra le fruit de son travail, et ne l'avalera pas ; il restituera toutes ses richesses et n'en jouira plus<!--So. 2:10.-->.
20:19	Parce qu'il a foulé les pauvres et les a abandonnés, il a ruiné leur maison au lieu de la bâtir.
20:20	Certainement, il ne connaîtra pas de contentement dans son ventre, et il ne sauvera rien de ce qu'il a tant convoité<!--Ec. 5:12.-->.
20:21	Il ne lui restera rien à dévorer, c'est pourquoi il ne s'attendra plus à sa prospérité.
20:22	Après que la mesure de ses biens aura été remplie, il sera dans la misère ; toutes les mains de ceux qu'il a opprimés se jetteront sur lui.
20:23	S'il a eu de quoi remplir son ventre, Elohîm lui fera sentir l'ardeur de sa colère, et la fera pleuvoir sur lui, dans sa chair.
20:24	S'il s'enfuit devant les armes de fer, l'arc de cuivre le transpercera.
20:25	La flèche lancée contre lui sortira au travers de son corps, et le fer étincelant sortira de son fiel. Toutes sortes de frayeurs marcheront contre lui.
20:26	Toutes les ténèbres seront renfermées dans ses demeures les plus secrètes, un feu qu'on n'aura pas soufflé le consumera. L'homme qui restera dans sa tente sera malheureux<!--Ps. 12:6.-->.
20:27	Les cieux découvriront son iniquité et la Terre s'élèvera contre lui.
20:28	Le revenu de sa maison sera emporté. Tout s'écoulera au jour de la colère d'Elohîm.
20:29	C'est là la portion qu'Elohîm réserve à l'être humain méchant, et l'héritage qu'il aura de El pour ses discours.

## Chapitre 21

### Réponse de Iyov

21:1	Mais Iyov prit la parole et dit :
21:2	Écoutez, écoutez mon discours, et cela me tiendra lieu de consolation de votre part.
21:3	Supportez-moi, et je parlerai, et après que j'aurai parlé, moquez-vous.
21:4	Quant à moi, ma plainte s'adresse-t-elle à un être humain ? Et pourquoi mon esprit ne serait-il pas affligé ?
21:5	Regardez-moi, soyez étonnés, et mettez la main sur la bouche.
21:6	Quand je pense à cela, j'en suis tout étonné et un tremblement saisit ma chair.
21:7	Pourquoi les méchants vivent-ils ? Ils deviennent vieux et croissent même en puissance<!--Jé. 12:1 ; Ha. 1:3 ; Mal. 3:14-15.-->.
21:8	Leur postérité s'établit devant eux, et leurs rejetons s'élèvent sous leurs yeux.
21:9	Leurs maisons jouissent de la paix loin de la frayeur, et la verge d'Éloah n'est pas sur eux.
21:10	Leurs taureaux engendrent et n'y manquent pas, leurs vaches mettent bas et n'avortent pas<!--Ps. 144:13-14.-->.
21:11	Ils font sortir leurs jeunes enfants comme un troupeau, et leurs enfants s'ébattent.
21:12	Ils sautent au son du tambourin et de la harpe, et se réjouissent au son de la flûte.
21:13	Ils passent leurs jours dans le bonheur et en un instant, ils descendent dans le shéol.
21:14	Cependant ils disaient à El : Retire-toi de nous ! Car nous ne prenons pas plaisir à la connaissance de tes voies.
21:15	Qui est Shaddaï pour que nous le servions<!--Ex. 5:2.--> ? Et que gagnerons-nous à lui adresser nos prières<!--Ou faire sa connaissance.--> ?
21:16	Voici, leur bonheur n'est pas dans leur main<!--Ils ne sont pas maîtres de leur bonheur.-->. Que le conseil des méchants soit loin de moi<!--Ps. 1:1-2.--> !
21:17	Arrive-t-il souvent que la lampe des méchants s'éteigne, que le désastre vienne sur eux, qu'il leur distribue leurs portions dans sa colère<!--Ps. 11:5-6 ; Pr. 13:9.--> ?
21:18	Qu'ils soient comme la paille face au vent ou comme la paille qu'emporte la tempête<!--Ps. 1:4.-->.
21:19	Éloah réserve le malheur pour ses fils. Il le paiera et il le sait !
21:20	Ses yeux verront sa ruine, et il boira la colère de Shaddaï<!--Es. 51:17-22 ; Jé. 25:15 ; Ez. 23:31-32 ; Ap. 14:10.-->.
21:21	Et quel plaisir a-t-il à sa maison qu'il laisse après lui, quand le nombre de ses mois est achevé ?
21:22	Est-ce à El qu'on enseignera la connaissance, à celui qui juge ceux qui sont élevés<!--Ro. 11:34 ; 1 Co. 2:16.--> ?
21:23	L'un meurt dans toute sa vigueur, tranquille et en repos,
21:24	ses seaux sont remplis de lait, et ses os sont abreuvés de moelle.
21:25	Et l'autre meurt dans l'amertume de son âme, sans avoir goûté au bonheur.
21:26	Et néanmoins ils sont couchés ensemble dans la poussière, et les vers les couvrent.
21:27	Voici, je connais vos pensées, et les jugements que vous formez contre moi.
21:28	Car vous dites : Où est la maison de cet homme si puissant, et où est la tente dans laquelle les méchants demeuraient ?
21:29	N'avez vous jamais interrogé les voyageurs, et n'avez-vous pas appris par les rapports qu'ils vous ont faits,
21:30	que le méchant est réservé pour le jour de la ruine, pour le jour où les fureurs sont envoyées<!--Pr. 16:4 ; Ec. 9:12.--> ?
21:31	Mais qui lui reproche en face sa conduite ? Et qui lui rendra le mal qu'il a fait ?
21:32	Il sera néanmoins porté au sépulcre, et on veillera sur son tertre funéraire<!--Un tas de gerbes.-->.
21:33	Les mottes des vallées lui sont agréables. Tous les humains se laissent entraîner derrière lui, des gens sans nombre l'avaient précédé.
21:34	Comment pouvez-vous m'offrir des consolations si vaines ! Ce qui reste de vos réponses n'est qu'une action infidèle !

## Chapitre 22

### Dernier discours d'Éliphaz

22:1	Alors Éliphaz de Théman prit la parole et dit :
22:2	L'homme fort peut-il être utile à El ? C'est plutôt à lui-même que le sage est utile.
22:3	Cela fait-il plaisir à Shaddaï que tu sois juste ? Tire-t-il profit de l'intégrité de tes voies ?
22:4	Te reprend-t-il, entre-t-il en jugement avec toi à cause de la crainte qu'il a de toi ?
22:5	Ta méchanceté n'est-elle pas grande ? Et tes injustices ne sont-elles pas sans fin ?
22:6	Car tu as pris sans raison le gage de tes frères, tu as ôté la robe à ceux qui étaient nus<!--Ex. 22:21.--> ;
22:7	tu n'as pas donné de l'eau à boire à celui qui était fatigué, tu as refusé ton pain à celui qui avait faim.
22:8	La terre était à l'homme puissant, et celui qui était respecté y habitait.
22:9	Tu as renvoyé les veuves à vide, et les bras des orphelins ont été cassés.
22:10	C'est pour cela que les filets sont tendus autour de toi, et qu'une frayeur subite t'épouvante.
22:11	Et les ténèbres sont tellement autour de toi que tu ne vois pas, le débordement des eaux te couvre.
22:12	Éloah n'habite-t-il pas au plus haut des cieux ? Regarde donc la hauteur des étoiles : combien elles sont élevées.
22:13	Mais tu dis : Qu'est-ce que El connaît ? Jugera-t-il au travers les ténèbres épaisses ?<!--So. 1:12 ; Ps. 10:11-13, 94:7.-->
22:14	Les sombres nuées nous cachent à ses yeux, et il ne voit rien, il se promène sur le cercle des cieux.
22:15	Eh quoi ! N'as-tu pas pris garde à l'ancienne route dans laquelle ont marché les hommes d'iniquité ?
22:16	Ils ont été emportés avant le temps, leurs fondements sont un fleuve qui s'écoule.
22:17	Ils disaient à El : Retire-toi de nous ! Mais qu'est-ce que leur faisait Shaddaï ?
22:18	Il avait rempli leur maison de biens. Que le conseil des méchants soit donc loin de moi !
22:19	Les justes le verront, se réjouiront, et l'innocent se moquera d'eux<!--Ps. 107:42.-->.
22:20	Certainement, notre adversaire a péri, le feu a dévoré ce qui en restait<!--Ps. 37:20 ; Ec. 8:12-13.--> !
22:21	Attache-toi donc à Elohîm, je te prie, tu demeureras en repos, tu atteindras ainsi le bonheur.
22:22	Reçois, je te prie, la torah de sa bouche et mets ses paroles dans ton cœur<!--Ps. 119:72.-->.
22:23	Si tu retournes à Shaddaï, tu seras rétabli. Chasse l'injustice loin de ta tente.
22:24	Jette l'or dans la poussière, l'or d'Ophir parmi les rochers des torrents,
22:25	et Shaddaï sera ton or, ton argent et ta richesse.
22:26	Car alors tu feras de Shaddaï tes délices et tu lèveras vers Éloah ta face.
22:27	Tu le supplieras, et il t'entendra, et tu accompliras tes vœux<!--Ps. 50:14-15.-->.
22:28	Si tu as une résolution, elle te réussira. La lumière brillera sur tes voies<!--Ps. 97:11.-->.
22:29	Quand on aura abaissé quelqu'un, alors tu lui diras : Relève-toi ! Celui qui a les yeux baissés, il le sauvera<!--Pr. 29:23.-->.
22:30	Il délivrera celui qui n'est pas innocent, et il sera délivré à cause de la pureté de tes mains.

## Chapitre 23

### Réponse de Iyov

23:1	Mais Iyov répondit, et dit :
23:2	Aujourd'hui encore ma plainte est une révolte, et la main qui m'a frappé s'appesantit sur mon gémissement.
23:3	Oh ! Si je savais comment le trouver, j'irais jusqu'à son trône,
23:4	j'exposerai mon droit devant lui, je remplirais ma bouche d'arguments,
23:5	je saurais ce qu'il me répondrait, je comprendrais ce qu'il me dirait.
23:6	Contesterait-il avec moi par la grandeur de sa force ? Non, il mettrait seulement sa force en moi.
23:7	Ce serait un homme juste qui argumenterait avec lui, et je serais pour toujours délivré par mon juge.
23:8	Mais si je vais à l'orient, il n'y est pas. Si je vais à l'occident, je ne l'aperçois pas.
23:9	S'il se fait entendre au nord, je ne le vois pas. Se cache-t-il au sud, je ne l'y aperçois pas.
23:10	Il connaît la voie que j'ai suivie. S'il m'éprouvait, j'en sortirai pur comme l'or<!--1 Pi. 1:7.-->.
23:11	Mon pied s'est attaché à ses pas, j'ai pris garde à sa voie et je ne m'en suis pas détourné.
23:12	Je ne me suis pas non plus écarté du commandement de ses lèvres. J'ai gardé précieusement les paroles de sa bouche plus que mes propres statuts.
23:13	Mais s'il a une pensée, qui l'en détournera ? Et ce que son âme désire, il le fait<!--Ps. 115:3, 135:6.-->.
23:14	Il achèvera donc ce qu'il a résolu à mon sujet, et il y a chez lui beaucoup de choses comme celles-là.
23:15	C'est pourquoi je suis terrifié à cause de sa présence, et quand je le considère, je suis effrayé devant lui.
23:16	El a brisé mon cœur, Shaddaï m'a terrifié.
23:17	Non, je ne suis pas anéanti devant les ténèbres, mais il a recouvert ma face d'obscurité.

## Chapitre 24

24:1	Pourquoi des temps ne sont-ils pas mis en réserve par Shaddaï ? Pourquoi ceux qui le connaissent ne voient-ils pas ses jours ?
24:2	Ils déplacent les bornes, ils ravissent des troupeaux, et ils les font paître<!--De. 19:14, 27:17 ; Pr. 13:10, 22:28.-->.
24:3	Ils emmènent l'âne des orphelins, ils prennent pour gage le bœuf de la veuve.
24:4	Ils font retirer les pauvres du chemin, et les misérables du pays se cachent.
24:5	Voici, certains sont comme des ânes sauvages dans le désert, ils sortent pour faire leur ouvrage, se levant dès le matin pour la proie, la région aride leur fournit du pain pour leurs garçons.
24:6	Ils moissonnent le fourrage dans les champs, ils grappillent dans la vigne du méchant.
24:7	Ils font passer la nuit sans vêtements à ceux qu'ils ont dépouillés, et ils n'ont pas de couverture contre le froid<!--Lé. 19:13 ; De. 24:12-13.-->.
24:8	Ils sont tout mouillés par les averses des montagnes, et n'ayant pas d'abri, ils étreignent les creux des rochers.
24:9	Ils enlèvent l'orphelin à la mamelle et prennent des gages du pauvre.
24:10	Ils font marcher sans habits l'homme qu'ils ont dépouillé. Ils enlèvent à ceux qui n'avaient pas de quoi manger ce qu'ils avaient glané.
24:11	Ceux qui pressent les olives entre leurs murailles, et ceux qui foulent la vendange dans les cuves souffrent de soif.
24:12	Ils font gémir les hommes dans la ville, l’âme des blessés à mort crie au secours. Éloah ne fait rien d'insensé.
24:13	En voici d'autres qui se révoltent contre la lumière, ils n'en connaissent pas les voies, ils ne restent pas sur leurs sentiers.
24:14	Le meurtrier se lève au point du jour, et il tue le pauvre et l'indigent, et la nuit il est tel qu'un larron<!--Ps. 10:8-9.-->.
24:15	L'œil de l'adultère épie le soir, en disant : Aucun œil ne me verra, et il se couvre le visage<!--Ps. 64:6 ; Pr. 7:7-10.-->.
24:16	Ils percent durant les ténèbres les maisons qu'ils avaient marquées le jour, ils haïssent la lumière<!--Jn. 3:20.-->.
24:17	Car le matin est pour eux tous l'ombre de la mort. Si quelqu'un les reconnaît, ils ont les terreurs de l'ombre de la mort.
24:18	Il passera plus vite que la face des eaux, leur portion sera maudite sur la Terre, il ne verra pas le chemin des vignes !
24:19	La sécheresse et la chaleur absorbent l'eau des neiges : le shéol, ceux qui ont péché<!--Ps. 49:15.--> !
24:20	Le sein maternel les oublie, les vers font d'eux leurs délices. On ne se souvient plus d'eux, l'injuste est brisé comme du bois.
24:21	Il maltraite la femme stérile qui n'enfante pas et il ne fait pas de bien à la veuve !
24:22	Il attire les puissants par sa force. Lorsqu'il se lève, on n'est plus sûr de sa vie.
24:23	Il leur donne la sécurité et ils s'appuient sur elle, toutefois ses yeux veillent sur leurs voies.
24:24	Ils sont élevés en peu de temps, et ensuite ils ne sont plus : ils sont abaissés, ils sont emportés comme tous les autres, et sont coupés comme le bout d'un épi.
24:25	S'il n'en est pas ainsi, qui me convaincra de mensonge ? Qui réduira mes paroles à néant ?

## Chapitre 25

### Dernier discours de Bildad

25:1	Alors Bildad de Shouah prit la parole et dit :
25:2	Le règne et la terreur appartiennent à Elohîm, il maintient la paix dans ses lieux élevés.
25:3	Ses armées peuvent-elles se compter ? Et sur qui sa lumière ne se lève-t-elle pas<!--Mt. 5:45.--> ?
25:4	Et comment l'homme se justifierait-il devant El ? Et comment celui qui est né de femme serait-il pur ?
25:5	Voici, même la lune ne brille pas et les étoiles ne sont pas pures à ses yeux.
25:6	Combien moins l'homme mortel qui n'est qu'un ver, et le fils de l'être humain qui n'est qu'un vermisseau<!--Ps. 22:7.--> !

## Chapitre 26

### Réponse de Iyov

26:1	Mais Iyov répondit, et dit :
26:2	Ah, comme tu as secouru l'homme qui était sans force ! Ah, comme tu as sauvé le bras qui n'avait pas de puissance !
26:3	Ah, comme tu as donné des conseils à l'homme qui manquait de sagesse, et fait paraître l'abondance de ta sagesse !
26:4	À qui s'adressent tes paroles ? Et de qui est l'esprit qui est sorti de toi ?
26:5	Devant Elohîm les ombres<!--Vient d'un mot hébreu qui signifie « fantômes de morts, ombres, revenants » ou encore « esprits ».--> tremblent au-dessous des eaux et de leurs habitants.
26:6	Devant lui le shéol est nu, l'abîme est sans voile<!--Ps. 139:8-12 ; Pr. 15:11 ; Hé. 4:13.-->.
26:7	Il étend la direction nord sur le vide, il suspend la Terre sur le néant.
26:8	Il renferme les eaux dans ses nuages, et la nuée n'éclate pas sous leur poids<!--Ps. 104:2-3.-->.
26:9	Il couvre la face de son trône, et il étend sa nuée par-dessus.
26:10	Il a entouré les eaux avec des bornes jusqu'à ce qu'il n'y ait plus ni lumière ni ténèbres<!--Ge. 1:9 ; Jé. 5:22 ; Ps. 33:7, 104:9 ; Pr. 8:29.-->.
26:11	Les colonnes du ciel s'ébranlent et s'étonnent à sa menace.
26:12	Par sa force il soulève la mer, par son intelligence, son intelligence il en brise l'orgueil<!--Ps. 89:10.-->.
26:13	Il a orné les cieux par son Esprit, et de sa main, il a formé le serpent fuyard.
26:14	Si telles sont les bouts de ses voies, le petit bruit de ses actions que nous en percevons, qui pourra donc comprendre les éclats de tonnerre de sa puissance<!--Ec. 3:10.--> ?

## Chapitre 27

27:1	Et Iyov continua son discours sous forme de proverbe et dit :
27:2	El, qui met mon droit à l'écart, Shaddaï qui remplit mon âme d'amertume est vivant.
27:3	Aussi longtemps que j'aurai ma respiration et que le souffle d'Éloah sera dans mes narines,
27:4	mes lèvres ne prononceront rien d'injuste, et ma langue ne dira pas de chose fausse<!--Es. 33:15 ; Ps. 15:2, 24:4.-->.
27:5	Loin de moi de vous justifier ! Tant que je vivrai je n'abandonnerai pas mon intégrité.
27:6	Je tiens à ma justice et ne la lâcherai pas ! Mon cœur ne me fait de reproche sur aucun de mes jours.
27:7	Qu'il en soit de mon ennemi comme du méchant, et de celui qui se lève contre moi comme de l'injuste !
27:8	Quelle espérance reste-t-il à un athée quand Éloah coupe, quand il lui retire son âme<!--Mt. 16:26 ; Lu. 12:20.--> ?
27:9	Est-ce que El entend ses cris quand l'angoisse vient sur lui<!--Es. 1:15 ; Jé. 14:12 ; Ez. 8:18 ; Mi. 3:4 ; Ps. 18:41 ; Pr. 1:28 ; Jn. 9:31 ; Ja. 4:3.--> ?
27:10	Faisait-il de Shaddaï ses délices ? Invoquait-t-il Éloah en tout temps ?
27:11	Je vous enseignerai les voies de El, et je ne vous cacherai pas les projets de Shaddaï.
27:12	Voici, vous-mêmes, vous l'avez tous vu, et pourquoi vous laissez-vous aller à des pensées vaines ?
27:13	Voici la part que El réserve à l'être humain méchant, l'héritage que les violents reçoivent de Shaddaï.
27:14	Si ses enfants se sont multipliés, ce sera pour l'épée, et sa postérité n'aura pas même assez de pain.
27:15	Ses survivants seront bien ensevelis par la peste, et leurs veuves ne les pleureront pas<!--Ps. 78:64.-->.
27:16	Parce qu'il entasse l'argent comme la poussière, et qu'il entasse des habits comme on amasse de la boue,
27:17	le riche les entassera mais le juste s'en vêtira, et l'innocent partagera l'argent.
27:18	Il se bâtira une maison comme celle de la teigne, comme la cabane que fait un gardien<!--Ps. 49:18.-->.
27:19	Le riche tombe, et il ne se relève pas : il ouvre les yeux et il ne trouve rien.
27:20	Les frayeurs l'atteignent comme des eaux, le tourbillon l'enlève de nuit.
27:21	Le vent d'orient l'emporte et il s'en va, il l'arrache de sa demeure comme un tourbillon.
27:22	On lance sans pitié des traits contre lui. Il s'enfuit, s'enfuit, loin de leurs mains.
27:23	On battra des mains contre lui, et on le sifflera depuis le lieu qu'il habitait.

## Chapitre 28

28:1	Oui, il existe un endroit pour extraire l'argent, et un lieu pour affiner l'or.
28:2	Le fer est tiré de la poussière, et la pierre fondue produit le cuivre.
28:3	On met fin aux ténèbres, on cherche jusqu'à l'extrême limite les pierres cachées dans l'obscurité et dans l'ombre de la mort.
28:4	On fait une brèche pour un torrent loin des endroits habités, oubliés du pied de l'homme, ils sont suspendus, balancés loin des humains.
28:5	La terre d'où sort le pain est bouleversée dans ses entrailles comme par le feu.
28:6	Ses pierres sont le lieu du saphir, et l'on y trouve de la poudre d'or.
28:7	L'oiseau de proie n'en connaît pas le chemin, l'œil du vautour ne l'aperçoit pas.
28:8	Les fils des bêtes sauvages et majestueuses n'y ont pas marché, le lion n'y est jamais passé.
28:9	L'homme avance sa main sur le roc, il renverse les montagnes depuis la racine,
28:10	il fend des canaux dans les rochers, et son œil voit tout ce qu'il y a de précieux,
28:11	il arrête l'écoulement des eaux, et il fait sortir ce qui est caché.
28:12	Mais la sagesse, où se trouve-t-elle ? Où est le lieu du discernement ?
28:13	Le mortel n'en connaît pas l'estimation, et elle ne se trouve pas dans la Terre des vivants.
28:14	L'abîme dit : Elle n'est pas en moi. Et la mer dit : Elle n'est pas avec moi.
28:15	Elle ne se donne pas contre de l'or pur, elle ne s'achète pas au poids de l'argent<!--Pr. 3:14, 8:11, 16:16.-->.
28:16	Elle n'entre pas en balance avec l'or d'Ophir, ni avec l'Onyx précieux, ni avec le Saphir.
28:17	Ni l'or ni le diamant ne peuvent lui être comparés, et on ne peut l'échanger contre un vase d'or fin.
28:18	On ne se souvient ni du corail ni du cristal auprès d'elle. La sagesse vaut plus que les perles.
28:19	On ne la compare pas avec la topaze d'Éthiopie, on ne la met pas en balance avec l'or pur.
28:20	D'où vient donc la sagesse ? Où est la demeure du discernement ?
28:21	Elle est couverte aux yeux de tout homme vivant, et elle est cachée aux oiseaux des cieux.
28:22	L'abîme et la mort disent : Nous avons entendu de nos oreilles parler d'elle.
28:23	C'est Elohîm qui en sait le chemin, et qui sait où elle est.
28:24	Car il regarde jusqu'aux extrémités de la Terre, il voit tout sous les cieux<!--Ps. 14:2, 33:13-14, 102:20.-->.
28:25	Quand il façonnait le poids du vent, et qu'il pesait les eaux<!--Pr. 8:29.--> avec mesure,
28:26	quand il faisait une loi pour la pluie, et un chemin pour les voix de l’éclair,
28:27	alors il vit la sagesse et la manifesta, il l'établit et même il la sonda jusqu'au fond.
28:28	Puis il dit à l'être humain : La crainte d'Adonaï, voilà la sagesse, et se détourner du mal, c'est là le discernement<!--De. 4:6 ; Jé. 9:24 ; Ps. 111:10 ; Pr. 1:7, 9:10 ; Ec. 12:15.--> !

## Chapitre 29

### La prospérité passée de Iyov

29:1	Et Iyov continua son discours sous forme de proverbe et dit :
29:2	Oh ! Qui me rendra tel que j'étais autrefois, tel que j'étais aux jours où Éloah me gardait,
29:3	quand il faisait luire sa lampe sur ma tête, quand je marchais à sa lumière dans les ténèbres,
29:4	comme aux jours de mon automne, lorsque le secret d'Éloah était dans ma tente,
29:5	quand Shaddaï était encore avec moi, et que mes jeunes hommes m'entouraient,
29:6	quand je lavais mes pieds dans le lait, et que le rocher répandait près de moi des torrents d'huile<!--De. 32:13.--> !
29:7	Quand je sortais vers la porte passant par la ville, et que je me faisais préparer un siège dans la place,
29:8	les jeunes gens me voyaient et se cachaient, les vieillards se levaient et se tenaient debout.
29:9	Les princes arrêtaient leurs discours et mettaient la main sur leur bouche.
29:10	Les chefs retenaient leur voix et leur langue se collait à leur palais.
29:11	L'oreille qui m'entendait disait que j'étais béni, et l'œil qui me voyait me rendait témoignage,
29:12	car je délivrais l'affligé qui criait au secours, et l'orphelin qui n'avait personne pour le secourir<!--Ps. 72:12 ; Pr. 21:13.-->.
29:13	La bénédiction de celui qui périssait venait sur moi, et je faisais chanter de joie le cœur de la veuve.
29:14	J'étais revêtu de la justice, elle me servait de vêtement. Mon équité était pour moi comme une robe et une tiare<!--Es. 59:17 ; 1 Th. 5:8 ; Ep. 6:14-17.-->.
29:15	J'étais les yeux de l'aveugle et les pieds du boiteux.
29:16	J'étais le père des pauvres, j'examinais avec soin la cause d'un inconnu<!--Pr. 29:7.-->.
29:17	Je brisais les crocs de l'injuste, et je faisais tomber sa proie de ses dents<!--Ps. 58:7.-->.
29:18	C'est pourquoi je disais : Je mourrai dans mon lit, et je multiplierai mes jours comme les grains de sable.
29:19	Ma racine sera ouverte aux eaux, et la rosée demeurera toute la nuit sur mes branches<!--Jé. 17:5-8 ; Ps. 1:3.-->.
29:20	Ma gloire restera toujours nouvelle avec moi, et mon arc se renouvellera dans ma main.
29:21	On m'écoutait, et on attendait, et lorsque je donnais mon avis, on se tenait dans le silence.
29:22	Après mes discours, personne ne répondait, et ma parole distillait sur eux.
29:23	Ils m'attendaient comme on attend la pluie, ils ouvraient grandement leur bouche comme pour une pluie du printemps.
29:24	Je leur souriais, ils n'y croyaient pas, et la lumière de ma face, ils ne la faisaient pas tomber.
29:25	Je choisissais d'aller avec eux, et je m'asseyais à leur tête. Je demeurais comme un roi au milieu d'une troupe, comme quelqu'un qui console les endeuillés.

## Chapitre 30

### Son humiliation

30:1	Et maintenant, ceux qui sont plus jeunes que moi se moquent de moi, eux dont je méprisais trop les pères pour les mettre parmi les chiens de mon troupeau.
30:2	En effet, à quoi m'aurait servi la force de leurs mains ? En eux la vigueur a péri.
30:3	À cause de la pauvreté et de la famine, ils sont stériles et rongent les lieux arides depuis longtemps dévastés et déserts.
30:4	Ils coupent des herbes sauvages auprès des arbrisseaux, et la racine des genévriers pour se chauffer.
30:5	Ils sont chassés d'entre les hommes, et on crie après eux comme après un larron.
30:6	Ils habitent dans les creux des torrents, dans les trous de la Terre et des rochers.
30:7	Ils font du bruit entre les arbrisseaux, et ils s'attroupent entre les chardons.
30:8	Ce sont des hommes de néant et sans nom, abaissés plus bas que la terre.
30:9	Et maintenant je suis le sujet de leur chanson, et la matière de leur entretien<!--Ps. 69:12 ; La. 3:14.-->.
30:10	Ils m'ont en abomination et se tiennent loin de moi, ils ne craignent pas de me cracher au visage.
30:11	Parce qu'il a délié ma corde et m'a affligé, ils ont jeté dehors le frein devant moi.
30:12	De jeunes gens<!--Une couvée.-->, nouvellement nés, se placent à ma droite, ils poussent mes pieds, ils se fraient jusqu'à moi des sentiers pour mon désastre<!--Ps. 35:15.-->.
30:13	Ils détruisent mon sentier et travaillent à ma destruction, sans que personne leur vienne en aide.
30:14	Ils viennent contre moi comme par une brèche large, et ils se sont jetés sur moi à cause de ma désolation.
30:15	Les terreurs se tournent vers moi. Elles poursuivent ma noblesse comme un vent, mon salut<!--Vient de l'hébreu « yeshuw`ah ». C'est le nom de Yéhoshoua (Jésus). Voir Ge. 49:18.--> a passé comme un nuage<!--Os. 13:3.-->.
30:16	C'est pourquoi maintenant mon âme se répand en moi, les jours d'affliction m'ont atteint.
30:17	Il me perce les os pendant la nuit, et mes artères n'ont pas de relâche.
30:18	Il change mon vêtement par la grandeur de sa force, et il me serre de près, comme fait l'ouverture de ma tunique.
30:19	Il m'a jeté dans la boue, et je ressemble à la poussière et à la cendre.
30:20	Vers toi je crie au secours, et tu ne m'exauces pas. Je me tiens debout et tu ne me regardes pas.
30:21	Tu es pour moi sans compassion, tu me traites en ennemi par la force de ta main.
30:22	Tu m'élèves comme sur le vent, et tu m'y fais monter comme sur un chariot, et puis tu fais fondre toute ma substance.
30:23	Je sais donc que tu me conduis à la mort et dans la maison assignée à tous les vivants<!--Hé. 9:27.-->.
30:24	Mais il n'étendra pas sa main jusqu'au sépulcre. Quand il les aura tués, crieront-ils ?
30:25	N'ai-je pas pleuré sur celui qui passait de mauvais jours ? Mon âme n'était-elle pas affligée à cause du pauvre<!--Ro. 12:15.--> ?
30:26	Cependant lorsque j'attendais le bonheur, le mal m'est arrivé. Et quand j'espérais la clarté, les ténèbres sont venues.
30:27	Mes entrailles bouillonnent sans faire silence. Les jours d'affliction sont venus vers moi.
30:28	Je marche assombri, mais non par le soleil. Je me lève, je crie au secours en pleine assemblée.
30:29	Je suis devenu le frère des dragons et le compagnon de filles des autruches<!--Voir Ps. 102:7-8.-->.
30:30	Ma peau est noire sur moi, mes os sont brûlés par la sécheresse<!--La. 4:8, 5:10.-->.
30:31	C'est pourquoi ma harpe s'est changée en deuil, et ma flûte en voix de pleurs.

## Chapitre 31

### Iyov se justifie

31:1	J'avais fait un pacte avec mes yeux. Comment aurais-je regardé une vierge ?
31:2	Mais quelle est la part d'Éloah en haut ? L'héritage de Shaddaï dans les hauteurs ?
31:3	La détresse n'est-elle pas pour l'injuste, et la calamité pour ceux qui pratiquent la méchanceté ? 
31:4	N'a-t-il pas vu lui-même mes voies ? Ne compte-t-il pas tous mes pas<!--Pr. 5:21, 15:3 ; 2 Ch. 16:9.--> ?
31:5	Si j'ai marché dans le mensonge, si mon pied s'est hâté pour tromper,
31:6	qu'on me pèse dans des balances justes, et Éloah connaîtra mon intégrité.
31:7	Si mes pas se sont détournés du chemin, et si mon cœur a suivi mes yeux, et si quelque tache s'est attachée à mes mains,
31:8	que je sème et qu'un autre mange, et que mes rejetons soient déracinés !
31:9	Si mon cœur a été séduit par une femme, et si j'ai fait le guet devant la porte de mon prochain<!--Pr. 7.-->,
31:10	que ma femme soit déshonorée et qu'elle se prostitue à d'autres.
31:11	Car c'est une méchanceté, une iniquité punie par les juges,
31:12	c'est un feu qui dévore jusqu'à la destruction, et qui aurait détruit toutes mes récoltes dans leur racine.
31:13	Si j'ai méprisé le droit de mon serviteur ou de ma servante quand ils contestaient avec moi,
31:14	que ferais-je quand El se lèvera ? Que répondrai-je quand il me demandera des comptes ?
31:15	Celui qui m'a façonné dans le ventre ne l'a-t-il pas façonné aussi ? N'est-ce pas Un seul qui nous a placés dans le sein maternel<!--Pr. 14:31, 17:5.--> ?
31:16	Si j'ai refusé aux pauvres leur désir, si j'ai laissé se consumer les yeux de la veuve<!--Es. 10:2 ; Lu. 18:2-3.-->,
31:17	si j'ai mangé seul mon morceau de pain, sans que l'orphelin en ait eu sa part,
31:18	car, dès ma jeunesse il m'a élevé comme un père, guidé depuis le sein maternel !
31:19	Si j'ai vu le malheureux périr faute de vêtements, le pauvre manquer de couverture<!--Mt. 25:41-45.-->,
31:20	si ses reins ne m'ont pas béni, et s'il n'a pas été réchauffé par la laine de mes agneaux,
31:21	si j'ai levé la main contre l'orphelin, parce que je me voyais soutenu à la porte de la ville<!--Pr. 22:22.-->,
31:22	que l'os de mon épaule tombe et que mon bras soit cassé et séparé de l'os auquel il est joint !
31:23	Car j'ai eu la crainte de l'orage de El, et je ne saurais subsister devant sa majesté.
31:24	Si j'ai mis mon espérance dans l'or, et si j'ai dit à l'or fin : Tu es ma confiance<!--Mc. 10:24 ; 1 Ti. 6:17.-->,
31:25	si je me suis réjoui de l'abondance de mes biens, des richesses que ma main avait acquises<!--Ps. 62:11.-->,
31:26	si j'ai regardé le soleil lorsqu'il brillait le plus, et la lune quand elle marchait dans sa splendeur,
31:27	si mon cœur a été séduit en secret, et si ma bouche a embrassé ma main,
31:28	cela serait aussi une iniquité punissable par le juge, car j'aurais renié le El d'en haut !
31:29	Si je me suis réjoui du malheur de mon ennemi, si j'ai sauté d'allégresse quand le mal l'a atteint<!--Mt. 5:43-44.-->,
31:30	moi qui n'ai pas permis à ma langue de pécher en demandant sa mort par des malédictions.
31:31	Les hommes de ma maison n'ont pas dit : Qui n'a-t-il pas rassasié de sa chair<!--Ps. 27:2.--> ?
31:32	L'étranger n'a pas passé la nuit dehors, j'ai ouvert ma porte au passant<!--Ge. 19:1-2 ; De. 10:19 ; 1 Pi. 4:9 ; Hé. 13:2.-->.
31:33	Si j'ai couvert mes transgressions comme Adam, en cachant mon iniquité dans mon sein<!--Ge. 3:10-12 ; Pr. 28:13.-->,
31:34	parce que je redoutais la grande multitude, que le mépris des familles me faisait peur, et que je sois resté dans le silence, sans sortir de ma porte...
31:35	Qui me donnera quelqu’un qui m’entende ! Voici mon Tav<!--« Marque ». Voir le commentaire dans Ez. 9:6.--> ! Que Shaddaï me réponde ! Que l'homme qui lutte avec moi écrive un document !
31:36	Ne le porterais-je pas sur mon épaule ? Ne le lierais-je pas sur moi comme une couronne ?
31:37	Je lui raconterais tous mes pas, je m'approcherais de lui comme un prince.
31:38	Si ma terre crie contre moi, et si ses sillons pleurent,
31:39	si j'ai mangé son fruit sans argent, si j'ai tourmenté l'esprit de ceux qui la possédaient,
31:40	qu'elle me produise des épines au lieu de blé, et de l'ivraie au lieu de l'orge. C'est ici la fin des paroles de Iyov.

## Chapitre 32

### Discours d'Élihou : Reproches à Iyov et à ses amis

32:1	Et ces trois hommes cessèrent de répondre à Iyov, parce qu'il était juste à ses propres yeux.
32:2	Alors s'enflamma la colère d'Élihou, fils de Barakeël de Bouz, de la famille de Ram. Sa colère s'enflamma contre Iyov, parce qu'il déclarait juste son âme plutôt qu'Elohîm.
32:3	Sa colère s'enflamma contre ses trois amis, parce qu’ils ne trouvaient pas de réponse et qu’ils condamnaient Iyov.
32:4	Or Élihou avait attendu que Iyov eût parlé, parce qu'ils étaient tous plus âgés que lui.
32:5	Mais Élihou, voyant qu'il n'y avait aucune réponse dans la bouche de ces trois hommes, sa colère s'enflamma.
32:6	C'est pourquoi Élihou, fils de Barakeël, de Bouz, répondit, en disant : Je suis encore jeune, et vous, vous êtes des hommes âgés. C'est pourquoi j'ai eu peur et je craignais de vous exprimer mon opinion.
32:7	Je me disais : Les jours parleront, et le grand nombre des années fera connaître la sagesse.
32:8	En vérité, en l'homme il y a l'Esprit, le souffle de Shaddaï qui le rend intelligent<!--Da. 1:17, 2:21 ; Pr. 2:6 ; Ec. 2:26.-->.
32:9	Ce n'est pas le grand nombre d'années qui donne la sagesse, ce n'est pas la vieillesse qui discerne la justice.
32:10	C'est pourquoi je dis : Écoute-moi ! Et je dirai aussi mon opinion.
32:11	Voici j'ai attendu vos paroles, j'ai prêté l'oreille à votre intelligence jusqu'à ce que vous ayez examiné les discours.
32:12	Je vous ai considérés jusqu'à ce que j'ai vu qu'aucun de vous n'a convaincu Iyov, aucun n'a répondu à ses discours.
32:13	Ne dites pas : Nous avons trouvé la sagesse : c'est El qui le poursuit, et non pas un homme !
32:14	Or comme ce n'est pas contre moi qu'il a dirigé ses discours, aussi ce ne sera pas selon vos paroles que je lui répondrai.
32:15	Ils ont été brisés ! Ils n'ont plus rien répondu ! On leur a fait perdre la parole !
32:16	J'ai attendu jusqu'à ce qu'ils ne disent plus rien, car ils sont restés muets et ils n'ont plus répliqué.
32:17	Je répondrai donc pour moi et je dirai mon opinion.
32:18	Car je suis rempli de discours, et l'esprit dont je me sens rempli me presse.
32:19	Voici, mon ventre est comme un vin qui n’est pas ouvert et il éclate comme des outres neuves<!--Mt. 9:17 ; Mc. 2:22 ; Lu. 5:38.-->.
32:20	Je parlerai donc pour respirer facilement, j'ouvrirai mes lèvres et je répondrai.
32:21	Je ne ferai acception de personne et je ne donnerai un titre flatteur à aucun être humain.
32:22	Car je ne sais pas donner un titre flatteur : mon Créateur m'enlèverait sur-le-champ !

## Chapitre 33

### Discours d'Élihou sur la justice de El

33:1	C'est pourquoi Iyov, écoute mon discours, je te prie, et prête l'oreille à toutes mes paroles !
33:2	Voici, maintenant j'ouvre ma bouche, ma langue parle dans mon palais.
33:3	Mes paroles répondront à la droiture de mon cœur, mes lèvres prononceront une doctrine pure.
33:4	L'Esprit de El m'a fait, et le souffle de Shaddaï m'a donné la vie<!--Ge. 2:7.-->.
33:5	Si tu peux, réponds-moi, dresse-toi contre moi, demeure ferme !
33:6	Voici, je suis comme ta bouche devant El, moi aussi, j'ai été formé de l'argile<!--Ac. 14:15.-->.
33:7	Voici ma terreur ne te troublera pas, et ma main ne s'appesantira pas sur toi.
33:8	En effet, tu as parlé à mes oreilles, et j'ai entendu la voix de tes discours :
33:9	Je suis pur, sans transgression, je suis net, il n'y a pas d'iniquité en moi.
33:10	Voici, il cherche à rompre avec moi, il me considère comme son ennemi,
33:11	il met mes pieds dans les ceps, il surveille tous mes chemins.
33:12	Je te répondrai qu'en cela tu n'as pas été juste, car Éloah sera toujours plus grand que l'homme.
33:13	Pourquoi as-tu donc plaidé contre lui ? Car il ne rend pas compte de toutes ses actions.
33:14	El parle en effet une fois, et deux fois, mais l'on n'y prend pas garde.
33:15	Par des rêves, par des visions nocturnes, quand les hommes tombent dans un profond sommeil, quand ils dorment sur leur couche.
33:16	Alors il découvre l'oreille des hommes, et scelle leur correction,
33:17	afin de détourner l'être humain de ses actions, et de couvrir l'orgueil loin de l'homme fort.
33:18	Ainsi il garantit son âme de la fosse, et sa vie de l'épée.
33:19	L'homme est aussi châtié par des douleurs sur son lit, par l'agitation perpétuelle de ses os<!--Ps. 38:4.-->.
33:20	Alors sa vie prend en horreur le pain et son âme la viande désirable<!--Ps. 107:18.-->.
33:21	Sa chair est tellement consumée qu'elle ne paraît plus, ses os qu'on ne voyait pas, sont tellement brisés qu'ils sont mis à nu.
33:22	Son âme s'approche de la fosse, et sa vie des choses qui font mourir.
33:23	Mais s'il y a pour cet homme un messager qui parle pour lui, un d'entre les mille, pour lui annoncer la droiture,
33:24	alors Elohîm aura pitié de lui, et il dira : Garantis-le, afin qu'il ne descende pas dans la fosse ! J'ai trouvé la propitiation !
33:25	Et sa chair retrouvera la fraîcheur de la jeunesse, il reviendra aux jours de sa jeunesse.
33:26	Il suppliera Éloah, et Elohîm s'apaisera envers lui ; il lui fera voir sa face avec des cris de joie et lui rendra sa justice<!--Es. 58:9.-->.
33:27	Il regardera vers les hommes et dira : J'avais péché, j'avais renversé la justice, et cela ne m'avait pas profité.
33:28	Mais Elohîm a racheté mon âme afin qu'elle ne passe pas par la fosse, et ma vie voit la lumière !
33:29	Voilà, El fait toutes ces choses, deux et trois fois envers l'homme<!--Ps. 62:11.-->,
33:30	pour retirer son âme de la fosse, afin qu'elle soit éclairée de la lumière des vivants<!--Ps. 56:14.-->.
33:31	Sois attentif, Iyov, écoute-moi ! Tais-toi, et je parlerai !
33:32	Et si tu as quelque chose à dire, réponds-moi ! Parle, car je désire te justifier.
33:33	Sinon, écoute-moi, tais-toi et je t'enseignerai la sagesse.

## Chapitre 34

### Élihou accuse Iyov de se révolter

34:1	Élihou donc reprit la parole, et dit :
34:2	Vous, sages, écoutez mes discours ! Vous qui avez de l'intelligence, prêtez-moi l'oreille !
34:3	Car l'oreille discerne les discours comme le palais savoure ce qu'il mange.
34:4	Choisissons ce qui est juste, voyons entre nous ce qui est bon.
34:5	Car Iyov a dit : Je suis juste, mais El me refuse justice.
34:6	Mentirai-je à ma justice ? Ma flèche est incurable, bien qu’il n’y ait pas de transgression.
34:7	Y a-t-il un homme tel que Iyov, qui boit la moquerie comme de l'eau,
34:8	qui voyage avec ceux qui pratiquent la méchanceté, qui marche avec les hommes de méchanceté ?
34:9	Car il a dit : Il est inutile à l'homme de plaire à Elohîm<!--Mal. 3:14.-->.
34:10	C'est pourquoi, vous qui avez de l'intelligence, écoutez-moi ! Loin de El d'agir avec méchanceté, loin de Shaddaï<!--De. 32:4 ; Ps. 92:16 ; Ro. 9:14.--> l'injustice !
34:11	Car il rendra à l'être humain selon son œuvre, il fera trouver à chacun selon sa voie<!--Jé. 17:10, 32:19 ; Ez. 7:27 ; Pr. 24:12 ; Mt. 16:27 ; Ro. 2:6 ; 2 Co. 5:10 ; Ep. 6:8 ; Ap. 22:12.-->.
34:12	Non, en vérité, Elohîm ne commet pas l'injustice, Shaddaï ne renverse pas la justice.
34:13	Qui lui a remis le soin de la Terre ? Ou qui a établi le monde entier ?
34:14	S’il fixait son cœur sur qui que ce soit, s’il retirait à lui son Esprit et son souffle<!--Ps. 104:29.-->,
34:15	toute chair périrait ensemble, et l'être humain retournerait à la poussière<!--Ge. 3:19 ; Ec. 3:20, 12:9.-->.
34:16	Si donc tu as du discernement, écoute ceci, prête l'oreille au son de mes paroles !
34:17	Celui qui hait la justice gouvernera-t-il vraiment ? Et condamneras-tu le Juste, le Puissant ?
34:18	Dira-t-on à un roi : Bélial<!--Voir commentaire en De. 13:13.--> et aux princes : Vous êtes des méchants ?
34:19	Qui n'a pas d'égard à la personne des princes, et ne distingue le riche du pauvre, parce qu'ils sont tous l'ouvrage de ses mains<!--De. 10:17 ; 2 Ch. 19:7 ; Ac. 10:34 ; Ga. 2:6 ; Ro. 2:11 ; Ep. 6:9 ; Col. 3:25.--> ?
34:20	En un instant, ils meurent. Au milieu de la nuit, un peuple est ébranlé et passe, le puissant est écarté, non par une main.
34:21	Car les yeux d'Elohîm sont sur les voies de l'homme, il regarde tous ses pas.
34:22	Il n'y a ni ténèbres ni ombre de la mort où puissent se cacher ceux qui pratiquent la méchanceté.
34:23	Car Elohîm n'a pas besoin d'observer longtemps un homme pour le faire assigner devant lui en justice.
34:24	Il brise les hommes puissants sans faire d'enquête, et il en établit d'autres à leur place.
34:25	Il connaît donc leurs œuvres, il les renverse la nuit, et ils sont brisés.
34:26	Comme des méchants, il les châtie dans un lieu bien en vue.
34:27	Parce qu'ils se sont ainsi détournés de lui, et qu'ils n'ont considéré aucune de ses voies,
34:28	de sorte qu'ils ont fait monter le cri du pauvre jusqu'à lui, et qu'il a entendu le cri des affligés<!--Ja. 5:4.-->.
34:29	S’il donne de la tranquillité, qui condamnera ? S’il cache sa face, qui pourra le regarder ? Tous ensemble, soit une nation, soit un humain.
34:30	Afin que l'être humain athée ne règne pas, de peur qu'il soit un piège pour le peuple.
34:31	En effet, a-t-il dit à El : Je supporte et je ne me corromprai plus.
34:32	Montre-moi toi-même ce qui est au-delà de ce que je vois, et si j'ai commis une injustice, je ne recommencerai pas.
34:33	Est-ce d'après tes pensées qu'il doit rétribuer ? C'est toi qui rejettes, qui choisis, mais non pas moi. Ce que tu sais, dis-le donc !
34:34	Les hommes de cœur ainsi que l'homme sage qui m’écoutent me diront :
34:35	Iyov n'a pas parlé avec connaissance, et ses paroles manquent d'intelligence.
34:36	Je désire que Iyov soit éprouvé jusqu’à la fin pour ses réponses d’homme méchant.
34:37	Car il ajoute à son péché la rébellion, il applaudit au milieu de nous et il multiplie ses paroles contre El.

## Chapitre 35

### Élihou reproche à Iyov ses propos irréfléchis

35:1	Élihou poursuivit son discours et dit :
35:2	As-tu pensé rendre ta cause bonne quand tu as dit : Ma justice est au-dessus de celle de El ?
35:3	Quand tu dis : Que me sert-il, et que gagnerais-je de plus de ne pas pécher ?
35:4	Je te répondrai par mes discours, et à tes amis qui sont avec toi.
35:5	Regarde les cieux, et contemple-les ! Vois les nuées, elles sont plus hautes que toi !
35:6	Si tu pèches, que fais-tu contre lui ? Et si tes transgressions se multiplient, que lui fais-tu ?
35:7	Si tu es juste, que lui donnes-tu ? Que reçoit-il de ta main ?
35:8	À l'homme tel que toi ta méchanceté ! Au fils de l'être humain ta justice !
35:9	On crie à cause de la multitude des oppressions, on crie au secours à cause de la violence des grands.
35:10	Et personne ne dit : Où est l'Éloah qui m'a fait, qui donne des chants de joie dans la nuit,
35:11	qui nous enseigne plus que les bêtes de la Terre, et nous rend plus sages que les oiseaux des cieux ?
35:12	Alors on crie, mais lui ne répond pas, à cause de l'orgueil des méchants.
35:13	Vraiment, c'est en vain que l'on crie car El Shaddaï n'entend, ne le voit pas !
35:14	Quoique tu dises que tu ne le vois pas, le jugement est devant lui : attends-le donc !
35:15	Mais maintenant, parce que sa colère ne punit pas, et qu'il ignore cette grande folie,
35:16	Iyov ouvre donc sa bouche pour se plaindre, il multiplie les paroles sans intelligence.

## Chapitre 36

### Discours d'Élihou : El traite les hommes selon leurs œuvres

36:1	Élihou continua de parler, et dit :
36:2	Attends-moi un peu, et je te montrerai qu'il y a encore des paroles pour la cause d'Éloah.
36:3	Je prendrai mes opinions de loin, et je défendrai la justice du Créateur.
36:4	Car vraiment, mes discours ne sont pas des mensonges, et celui qui est avec toi est parfait dans sa connaissance.
36:5	El est puissant, mais il ne méprise personne. Il est puissant par la force de son cœur.
36:6	Il ne laisse pas vivre le méchant, et il fait droit aux pauvres.
36:7	Il ne détourne pas ses yeux de dessus les justes, il les place sur le trône avec les rois, il les y fait asseoir pour toujours, afin qu'ils soient élevés<!--Ps. 33:18, 34:16.-->.
36:8	S'ils sont liés de chaînes, s'ils sont pris dans les liens de l'affliction,
36:9	il leur fait connaître ce qu'ils ont fait, leurs transgressions et leur orgueil.
36:10	Alors il découvre leurs oreilles à la discipline, il leur dit de se détourner de la méchanceté.
36:11	S'ils écoutent, et s'ils le servent, ils achèvent leurs jours dans le bonheur, leurs années dans la joie.
36:12	S'ils n'écoutent pas, ils passent par l'épée, ils expirent sans connaissance.
36:13	Et les cœurs des athées se mettent en colère, ils ne crient pas au secours quand il les lie.
36:14	Leur âme meurt dans sa jeunesse, leur vie s'éteint parmi les hommes prostitués qui se prostituent.
36:15	Il sauve l’affligé par son affliction, et c'est par l'oppression qu'il lui découvre les oreilles.
36:16	Il t'écartera aussi de la bouche de la détresse, au large, loin de toute angoisse, et ta table sera chargée de viandes grasses<!--Ps. 50:15, 63:6.-->.
36:17	Mais tu es plein des jugements des méchants, le jugement et la justice te saisiront.
36:18	Que la colère ne t'incite donc pas à la moquerie, et ne te laisse pas pervertir par la grandeur de la rançon<!--Ps. 49:8.--> !
36:19	Ferait-il cas de ton opulence ? Il n'estimera ni les précieux minerais, ni les ressources de ta force.
36:20	Ne soupire pas après la nuit pendant laquelle les peuples s'évanouissent de leur place.
36:21	Garde-toi de retourner à la méchanceté, car c'est ce que tu as choisi plutôt que l'affliction.
36:22	El est élevé par sa puissance. Qui saurait enseigner comme lui ?
36:23	Qui lui ferait rendre compte de ses voies ? Qui lui a dit : Tu as fait une injustice ?
36:24	Souviens-toi de célébrer son œuvre que tous les hommes chantent.
36:25	Tout être humain les voit, chacun les contemple de loin.
36:26	Voici, El est grand et nous ne le connaissons pas. Quant au nombre de ses années, il est insondable<!--Es. 63:16 ; Ps. 93:2, 102:13 ; La. 5:19.-->.
36:27	Quand il attire les gouttes d'eau, il les distille en pluie, par sa vapeur.
36:28	Les nuages la font couler, ils la répandent sur les humains en abondance.
36:29	Et qui pourra comprendre l'étendue des nuages et le son éclatant de sa tente ?
36:30	Voici, il étend sa lumière sur elle, et il se cache jusque dans les profondeurs de la mer.
36:31	Car c'est par ces choses qu'il juge les peuples, qu'il donne la nourriture en abondance.
36:32	Il tient caché dans les paumes de ses mains la lumière<!--Allusion aux éclairs.-->, et lui ordonne de frapper ce qui se présente à sa rencontre.
36:33	Son grondement l'annonce, le troupeau même présage qu'il va monter.

## Chapitre 37

### Conclusion d'Élihou

37:1	Mon cœur même à cause de cela est tout tremblant, il sort de sa place.
37:2	Écoutez, écoutez le murmure de sa voix, le grondement qui sort de sa bouche<!--Ps. 29:3-9.--> !
37:3	Il le dirige sous tous les cieux, et sa lumière<!--Allusion aux éclairs.--> brille jusqu'aux extrémités de la Terre<!--Ps. 97:4.-->.
37:4	Après lui s'élève un grand bruit, il tonne de sa voix majestueuse. Et il ne tarde pas après que sa voix a été entendue<!--Jé. 10:13.-->.
37:5	Elohîm tonne avec sa voix d'une manière merveilleuse. Il fait de grandes choses que nous ne saurions comprendre.
37:6	Car il dit à la neige : Tombe sur la terre ! Aux averses, aux pluies, aux averses, même aux pluies les plus fortes !
37:7	Alors il met un sceau sur la main de tout être humain, afin que tous les hommes connaissent son œuvre.
37:8	Les bêtes entrent dans leurs tanières, et elles demeurent dans leurs repaires.
37:9	L'ouragan vient du fond du midi, et le froid vient des vents du nord.
37:10	Par son souffle, El donne la glace, et il réduit l'espace où se répandaient au large les eaux<!--Ps. 147:17-18.-->.
37:11	Il charge les nuages d’humidité et disperse les nuées par sa lumière.
37:12	C’est lui qui les fait tournoyer en cercles pour qu’ils accomplissent, selon ses directions, tout ce qu’il leur commande, dans le monde, sur la terre.
37:13	Il les fait arriver, soit comme une verge, soit pour la Terre, soit par bonté<!--Ex. 9:18-23 ; 1 S. 12:18-19.-->.
37:14	Iyov, prête l'oreille à cela ! Reste tranquille et considère les merveilles de El !
37:15	Sais-tu comment Éloah les arrange, et comment il fait briller la lumière de ses nuages ?
37:16	Connais-tu le balancement des nuages, les merveilles de celui qui est parfait en connaissance ?
37:17	Pourquoi tes vêtements sont-ils chauds quand il donne du repos à la Terre par le vent du midi ?
37:18	As-tu étendu avec lui les nuages, qui sont fermes comme un miroir en métal fondu ?
37:19	Fais-nous connaître ce que nous pourrions lui dire ! Car nous ne saurions rien dire par ordre à cause de nos ténèbres.
37:20	Doit-on lui rendre compte quand je parle ? Si un homme parle, il sera certainement englouti.
37:21	Et maintenant, on ne peut voir la lumière qui brille dans les nuages, mais un vent passe et les nettoie.
37:22	Du nord vient une clarté d'or. Éloah s'entoure d'une majesté redoutable.
37:23	Nous ne saurions comprendre Shaddaï, grand en puissance, en jugement et en abondante justice, il n'opprime personne !
37:24	C'est pourquoi les hommes le craignent, mais il ne les voit pas tous sages de cœur<!--Ps. 92:7 ; Ro. 1:21.-->.

## Chapitre 38

### YHWH interroge Iyov

38:1	Alors YHWH répondit à Iyov du milieu du tourbillon et dit :
38:2	Qui est celui qui obscurcit mes décisions par des paroles sans connaissance ?
38:3	Ceins maintenant tes reins comme un vaillant homme. Je t'interrogerai et tu me feras voir ta connaissance.
38:4	Où étais-tu quand je fondais la Terre ? Dis-le, si tu as du discernement<!--Pr. 8:29.-->.
38:5	Qui en a réglé les mesures, le sais-tu ? Ou qui a appliqué sur elle le niveau ?
38:6	Sur quoi ses bases sont-elles plantées ? Ou qui en a posé la pierre angulaire pour la soutenir<!--Ps. 104:5.-->
38:7	quand les étoiles du matin criaient ensemble en applaudissant, et que tous les fils d'Elohîm poussaient des cris de joie<!--Ps. 148:3.--> ?
38:8	Qui a enfermé la mer avec des portes, quand elle s'élança pour sortir du sein maternel,
38:9	quand je lui ai donné la nuée pour vêtement et les ténèbres épaisses pour langes,
38:10	j'ai brisé son élan par mon ordonnance et j’ai mis devant elle des barres et des portes,
38:11	et je lui ai dit : Tu viendras jusqu'ici, tu n'iras pas plus loin. Ici s'arrêtera l'orgueil de tes flots ?
38:12	As-tu, en tes jours, commandé au matin ? As-tu fait connaître à l’aurore sa place,
38:13	pour qu'elle saisisse les extrémités de la Terre et en secoue les méchants ?
38:14	Elle se transforme comme l’argile sous le sceau et se présente comme parée d’un vêtement.
38:15	Aux méchants est refusée leur lumière et le bras qui se lève est brisé<!--Ps. 10:15.-->.
38:16	Es-tu entré jusqu'aux sources de la mer ? Es-tu allé à la recherche de l'abîme ?
38:17	Les portes de la mort se sont-elles découvertes à toi ? As-tu vu les portes de l'ombre de la mort ?
38:18	As-tu compris l'étendue de la Terre ? Si tu sais tout cela, dis-le !
38:19	Où est donc le chemin où demeure la lumière ? Et où se trouve le lieu d'habitation des ténèbres,
38:20	pour que tu puisses les saisir à leur frontière et discerner le chemin de leur maison ?
38:21	Tu le sais, car alors tu étais né et le nombre de tes jours est grand !
38:22	Es-tu entré dans les trésors de neige ? As-tu vu les trésors de grêle,
38:23	que je réserve pour les temps de détresse, pour les jours de guerre et de bataille<!--Ex. 9:23 ; Jos. 10:11 ; Ap. 8:7.--> ?
38:24	Par quel chemin la lumière se divise-t-elle, et le vent d'orient se répand-il sur la Terre<!--Jn. 3:8.--> ?
38:25	Qui a ouvert un conduit aux inondations, et tracé la route de l'éclair et du tonnerre,
38:26	pour faire pleuvoir sur une terre sans habitants, sur un désert sans humains<!--Ps. 104:13-14, 147:8 ; Ac. 14:17.--> ;
38:27	pour rassasier les lieux solitaires et arides, et pour faire germer et sortir l'herbe ?
38:28	La pluie a-t-elle un père qui engendre les gouttes de la rosée ?
38:29	De quel sein est sortie la glace ? Et qui enfante le givre du ciel,
38:30	pour que les eaux se cachent comme une pierre, et que le dessus de l'abîme soit enchaîné ?
38:31	Peux-tu resserrer les liens des pléiades ou détacher les chaînes d'Orion<!--Am. 5:8.--> ?
38:32	Fais-tu sortir en leur temps les signes du zodiaque, et conduis-tu la Grande Ourse avec ses fils ?
38:33	Connais-tu les lois du ciel ? Disposes-tu de son pouvoir sur la Terre<!--Jé. 31:35-36 ; Ps. 104:4.--> ?
38:34	Élèves-tu la voix jusqu'aux nuages, afin qu’une abondance d’eaux te couvrent ?
38:35	Envoies-tu les éclairs ? Partent-ils ? Te disent-ils : Nous voici ?
38:36	Qui a mis la sagesse dans le cœur, ou qui a donné le discernement à l'esprit<!--Ec. 2:26.--> ?
38:37	Qui est-ce qui peut avec intelligence compter les nuages, et placer les outres des cieux,
38:38	quand la poussière coule comme du métal en fusion et que les mottes de terre se collent ensemble ?

## Chapitre 39

### L'omnipotence de YHWH

39:1	Chasses-tu de la proie pour la lionne, et apaises-tu la faim des lionceaux<!--Ps. 104:21.-->,
39:2	quand ils se tapissent dans leurs tanières et se tiennent aux aguets dans leur repaire ?
39:3	Qui est-ce qui apprête la nourriture au corbeau, quand ses petits crient au secours à El, et qu'ils vont errants, parce qu'ils n'ont pas de quoi manger<!--Ps. 104:27, 147:9 ; Mt. 6:26.--> ?
39:4	Sais-tu quand les chèvres de montagne mettent bas ? Observes-tu les biches des rochers quand elles font leurs petits<!--Ps. 29:9.--> ?
39:5	Comptes-tu les mois qu’elles accomplissent et connais-tu le temps où elles enfantent ?
39:6	Elles se courbent, elles laissent échapper leurs petits et se libèrent de leurs douleurs.
39:7	Leurs petits se fortifient, ils croissent en plein air, ils s'en vont et ne reviennent plus vers elles.
39:8	Qui a laissé aller libre l'âne sauvage ? Et qui a délié les liens de l'âne farouche,
39:9	auquel j'ai donné la région aride pour maison et la terre salée pour tabernacle<!--Jé. 2:24.--> ?
39:10	Il se rit du bruit des villes, il n'entend pas les cris d'un exacteur.
39:11	Les montagnes qu'il parcourt sont ses pâturages, et il cherche toute sorte de verdure.
39:12	Le buffle voudra-t-il te servir, ou demeurera-t-il à ta crèche ?
39:13	Lies-tu le buffle avec une corde pour labourer ? Ou rompra-t-il les mottes des vallées après toi ?
39:14	Te fies-tu à lui parce que sa force est grande, et lui abandonnes-tu ton travail ?
39:15	Comptes-tu sur lui pour rentrer ta semence, et pour l'amasser sur ton aire ?
39:16	As-tu donné aux paons ce plumage qui est si brillant, ou à l'autruche les ailes et les plumes ?
39:17	Néanmoins elle abandonne ses œufs à terre, et les fait échauffer sur la poussière.
39:18	Elle oublie que le pied peut les écraser, ou que les bêtes des champs peuvent les fouler.
39:19	Elle est dure envers ses petits, comme s'ils n'étaient pas siens. Son travail est vain, elle ne s'en inquiète pas.
39:20	Car Éloah l'a privée de sagesse et ne lui a pas donné le discernement.
39:21	À la première occasion, elle se dresse en haut, et se moque du cheval et de celui qui le monte.
39:22	Est-ce toi qui donnes au cheval la force, qui revêts son cou d'une crinière flottante ?
39:23	Fais-tu bondir le cheval comme la sauterelle ? Le son magnifique de ses narines est effrayant.
39:24	Il creuse la terre de son pied, il s'égaie dans sa force, il va à la rencontre d'un homme armé.
39:25	Il se rit de la frayeur, il ne s'épouvante de rien, et il ne se détourne pas de devant l'épée.
39:26	Sur lui retentit le carquois, brillent la lance et le javelot.
39:27	Frémissant et agité, il dévore la terre et ne supporte pas le son du shofar.
39:28	Au son bruyant du shofar, il dit : En avant ! Il flaire de loin la bataille, le tonnerre des capitaines et l'alarme de guerre.
39:29	Est-ce par ton discernement que l'épervier prend son vol, et qu'il étend ses ailes vers le midi ?
39:30	Est-ce par ton commandement que l'aigle s'élève, et qu'il place son nid sur les hauteurs<!--Jé. 49:16 ; Ab. 1:4.--> ?
39:31	C'est dans les rochers qu'il habite et passe les nuits, c'est sur une dent de rocher qu'il a sa forteresse.
39:32	De là, il découvre le gibier, ses yeux voient de loin.
39:33	Ses petits boivent le sang et là où sont des cadavres, il s'y trouve aussitôt<!--Mt. 24:28 ; Lu. 17:37 ; Ap. 19:17-21.-->.

### YHWH interroge Iyov

39:34	YHWH prit encore la parole et dit à Iyov :
39:35	Celui qui conteste avec Shaddaï, lui apprendra-t-il quelque chose ? Que celui qui dispute avec Éloah réponde à ceci !

### Réponse de Iyov

39:36	Alors Iyov répondit à YHWH et dit :
39:37	Voici, je suis insignifiant. Que te répondrais-je ? Je mets ma main sur ma bouche<!--Ps. 39:10.-->.
39:38	J'ai parlé une fois, mais je ne répondrai plus. J'ai même parlé deux fois et je n'ajouterai rien.

## Chapitre 40

### YHWH questionne encore Iyov

40:1	Et YHWH répondit à Iyov du milieu d'un tourbillon, et lui dit :
40:2	Ceins maintenant tes reins comme un vaillant homme ! Je t'interrogerai et tu m'enseigneras.
40:3	Anéantiras-tu mon jugement ? Me condamneras-tu pour te justifier<!--Ps. 51:6 ; Ro. 3:4.--> ?
40:4	As-tu un bras comme celui de El ? Tonnes-tu de la voix comme lui ?
40:5	Pare-toi maintenant de magnificence et de grandeur, et revêts-toi de majesté et de gloire.
40:6	Répands les fureurs de ta colère, d'un regard, humilie tous les orgueilleux.
40:7	Regarde tout orgueilleux, abaisse-le, et écrase les méchants sur la place,
40:8	cache-les tous ensemble dans la poussière, et bande leur visage dans un lieu caché.
40:9	Alors je te donnerai moi-même cette louange, que ta droite t'aura sauvée.
40:10	Voici le Béhémoth<!--Le sens exact de ce mot est inconnu. Certains pensent qu'il s'agit peut-être d'un dinosaure disparu. La traduction par hippopotame ne semble pas correcte.-->, que j'ai façonné comme toi ! Il mange de l'herbe comme le bœuf.
40:11	Regarde donc, sa force est dans ses reins, et sa puissance dans les muscles de son ventre.
40:12	Il plie sa queue aussi ferme qu'un cèdre. Les tendons de ses cuisses sont entrelacés,
40:13	ses os sont des tubes de cuivre, ses membres sont comme des barres de fer.
40:14	Il est le commencement des voies<!--Ou le chef-d'œuvre de El.--> de El. Celui qui l'a fait lui a donné son épée.
40:15	Car les montagnes lui apportent sa pâture, là où se jouent toutes les bêtes des champs.
40:16	Il se couche sous les lotus, caché dans les roseaux et les marécages.
40:17	Les lotus le couvrent de leur ombre, les saules du torrent l'enveloppent.
40:18	Si un fleuve déborde avec violence, il ne s'enfuit pas : Si le Yarden<!--Jourdain.--> se précipite dans sa gueule, il reste calme.
40:19	Il l'engloutit de ses yeux et son nez perce les pièges qu'il rencontre.

### L'interrogatoire continue

40:20	Attireras-tu le léviathan à l'hameçon ? Saisiras-tu sa langue avec une corde ?
40:21	Mettras-tu un jonc dans ses narines ? Lui perceras-tu la mâchoire avec un crochet ?
40:22	Accumulera-t-il les supplications ? Te parlera-t-il d'une voix douce ?
40:23	Fera-t-il un accord avec toi, et le prendras-tu pour esclave à toujours ?
40:24	Joueras-tu avec lui comme avec un oiseau ? L'attacheras-tu pour amuser les jeunes filles ?
40:25	Les pêcheurs en trafiquent-ils ? Le partagent-ils entre les marchands ?
40:26	Couvriras-tu sa peau de dards, et sa tête de harpons ?
40:27	Mets ta main sur lui, tu te souviendras de cette guerre et tu ne recommenceras plus !
40:28	Voici, tout espoir de le prendre est trompeur. À sa vue n'est-on pas terrassé ?

## Chapitre 41

41:1	Il n'y a pas d'homme assez courageux pour le réveiller. Et qui est capable de se tenir debout devant moi ?
41:2	Qui m'a donné le premier<!--Ro. 11:35.--> ? Je lui rendrai. Tout ce qui est sous tous les cieux est à moi<!--Ex. 19:5 ; De. 10:14 ; Ps. 24:1, 50:12 ; 1 Co. 10:26 ; Ro. 11:35.-->.
41:3	Je ne me tairai pas sur ses membres, sur sa force et sur la beauté de sa stature.
41:4	Qui découvrira son vêtement devant ma face ? Qui viendra freiner ses mâchoires par un mors ?
41:5	Qui ouvrira les portes devant sa face ? Autour de ses dents habite la terreur.
41:6	Les rangées de ses boucliers ne sont que magnificence. Elles sont étroitement serrées avec un sceau.
41:7	L'une se rapproche de l'autre, et le vent n'entre pas entre elles.
41:8	Elles sont jointes l'une à l'autre, elles s'entretiennent et ne se séparent pas.
41:9	Ses éternuements éclaireraient la lumière, et ses yeux sont comme les paupières de l'aube du jour.
41:10	Des lampes de feu sortent de sa bouche, et il en rejaillit des étincelles de feu.
41:11	Une fumée sort de ses narines comme d'un pot bouillant, ou d'une chaudière.
41:12	Son souffle enflammerait des charbons, et une flamme sort de sa gueule.
41:13	La force est dans son cou, et la terreur marche devant lui.
41:14	Sa chair est ferme, tout est massif en lui, rien n'y branle.
41:15	Son cœur est dur comme une pierre, même comme une pièce de la meule inférieure.
41:16	Les plus forts tremblent quand il s'élève, et ils ne savent où ils en sont, voyant comme il rompt tout.
41:17	Quand on l'atteint de l'épée, elle n'a aucun effet, ni la lance, ni le dard, ni la cuirasse.
41:18	Il considère le fer comme de la paille, le cuivre comme du bois pourri.
41:19	La flèche ne le fera pas fuir, les pierres d'une fronde sont pour lui comme du chaume.
41:20	Il tient les machines de guerre comme des brins de chaume et il se moque du javelot qu'on lance sur lui.
41:21	Il a sous son ventre des pointes aiguës : On dirait une herse qu'il étend sur la boue.
41:22	Il fait bouillonner l'eau profonde comme une chaudière, il transforme la mer en un vase rempli de parfum.
41:23	Il fait briller son sentier après lui, et on prendrait les profondeurs pour des cheveux gris d'un vieillard.
41:24	Il n'y a rien sur la Terre qui puisse lui être comparé, il a été fait pour ne rien craindre.
41:25	Tout ce qui est élevé, il le voit. Il est le roi de tous les fils de bêtes sauvages et majestueuses.

## Chapitre 42

### Iyov reconnaît la toute-puissance d'Elohîm et s'humilie

42:1	Alors Iyov répondit à YHWH et dit :
42:2	Je sais que tu peux tout, et qu'on ne saurait t'empêcher de faire ce que tu penses.
42:3	Qui est celui qui, sans connaissance, entreprend d'obscurcir mes desseins ? J'ai donc parlé, et je n'y comprends rien ; ces choses sont trop merveilleuses pour moi, et je n'y connais rien<!--Ps. 40:6, 131:1, 139:6.-->.
42:4	Écoute-moi maintenant, et je parlerai. Je t'interrogerai et tu m'instruiras.
42:5	J'avais entendu de mes oreilles parler de toi, mais maintenant mon œil t'a vu.
42:6	C'est pourquoi je me méprise et me repens sur la poussière et sur la cendre.

### Le rétablissement de Iyov

42:7	Or après que YHWH eut ainsi parlé à Iyov, il dit à Éliphaz de Théman : Ma colère s'est enflammée contre toi et contre tes deux amis, parce que vous n'avez pas parlé de moi avec droiture comme Iyov, mon serviteur.
42:8	C'est pourquoi prenez maintenant 7 taureaux et 7 béliers, allez auprès de mon serviteur Iyov, et offrez un holocauste pour vous. Iyov, mon serviteur, priera pour vous, et certainement j'exaucerai sa prière, afin que je ne vous traite pas selon votre folie. Car vous n'avez pas parlé de moi avec droiture, comme mon serviteur Iyov.
42:9	Ainsi, Éliphaz de Théman, Bildad de Shouah, et Tsophar de Naama vinrent et firent comme YHWH leur avait dit, et YHWH exauça la prière de Iyov.
42:10	Et YHWH retira Iyov de sa captivité quand celui-ci eut prié pour ses amis. YHWH lui ajouta le double de tout ce qu'il avait possédé.
42:11	Aussi tous ses frères, toutes ses sœurs et tous ceux qui l'avaient connu auparavant, vinrent vers lui, et mangèrent avec lui dans sa maison. Et lui ayant témoigné qu'ils compatissaient à son état, ils le consolèrent de tout le mal que YHWH avait fait venir sur lui. Chacun d'eux lui donna une pièce d'argent, chacun un anneau d'or.
42:12	Ainsi YHWH bénit le dernier état de Iyov plus que le premier, de sorte qu'il eut 14 000 brebis, et 6 000 chameaux, et 1 000 couples de bœuf et 1 000 ânesses.
42:13	Il eut aussi 7 fils et 3 filles :
42:14	Il appela la première du nom de Yemiymah, la deuxième du nom de Qetsiy`ah, et la troisième du nom de Qérèn-Happouk.
42:15	Et il ne se trouvait pas de femmes aussi belles que les filles de Iyov dans tout le pays. Leur père leur donna une part de l'héritage parmi leurs frères.
42:16	Et Iyov vécut, après ces choses, 140 ans, et il vit ses fils et les fils de ses fils jusqu'à la quatrième génération.
42:17	Puis Iyov mourut âgé et rassasié de jours.
