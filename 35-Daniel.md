# Daniye'l (Daniel) (Da.)

Signification : El est mon juge

Auteur : Daniye'l (Daniel)

Thème : Ascension et chute des royaumes

Date de rédaction : 6ème siècle av. J.-C.

Issu d'une famille princière de Yéhouda (Juda), Daniye'l fut déporté de Yeroushalaim (Jérusalem) à Babel (Babylone) pendant sa jeunesse, sous le règne de Neboukadnetsar. Lui et trois de ses amis – eux aussi de noble lignée – furent choisis pour être instruits selon la sagesse babylonienne en vue de servir le roi. Fervent dans sa foi en YHWH, Daniye'l – imité ensuite par ses compagnons – résolut de ne pas se souiller et obtint ainsi la faveur de son Elohîm. Son intégrité et sa crainte d'Elohîm lui valurent de miraculeuses victoires, de nombreuses distinctions et une grande sagesse. Daniye'l avait reçu du discernement pour expliquer rêves et visions et délivra plusieurs prophéties dont certaines se sont déjà accomplies, d'autres se réaliseront à la fin du temps des nations, au moment du retour du Mashiah.

Elohîm témoigna de la justice de Daniye'l au prophète Yehezkel (Ézéchiel) dont il fut contemporain.

## Chapitre 1

### Yéhouda livré à la captivité babylonienne

1:1	La troisième année du règne de Yehoyaqim, roi de Yéhouda, Neboukadnetsar, roi de Babel<!--Babylone.-->, vint contre Yeroushalaim et l'assiégea.
1:2	Adonaï livra entre ses mains Yehoyaqim<!--Après avoir organisé un premier siège de la ville de Yeroushalaim (Jérusalem) et une première déportation en 605 av. J.-C., le puissant roi babylonien Neboukadnetsar revint mater la révolte de Yehoyaqim qui complotait avec l'Égypte pour se défaire du protectorat auquel il avait été soumis. Le 16 mars 597 av. J.-C., le souverain babylonien assiégea de nouveau la cité, déporta le roi Yehoyakin, et une partie de ses dignitaires, et le remplaça par son oncle Tsidqiyah (Sédécias). C'est ainsi qu'en cette période de Pâque qui commémorait la délivrance du joug égyptien (Ex. 12), les Hébreux se retrouvèrent paradoxalement de nouveau esclaves.-->, roi de Yéhouda et une partie des vases de la maison d'Elohîm. Neboukadnetsar emporta les vases au pays de Shinear<!--Shinear : « le pays des deux fleuves ». C'est l'ancien nom du territoire qui est devenu Babylonie ou Chaldée. C'est le pays de Nimrod (Ge. 10:6-12). C'est à Shinear qu'on tenta de construire la tour de Babel et de mettre en place le premier gouvernement mondial.-->, dans la maison de son elohîm, il les mit dans la maison du trésor de son elohîm.
1:3	Le roi dit à Ashpenaz, chef de ses eunuques, de faire venir quelques-uns des fils d'Israël de race royale<!--Daniye'l était de la race royale (2 R. 20:16-19 ; Es. 39:6-7).--> et des principaux seigneurs,
1:4	des enfants en qui il n'y avait aucun défaut physique, beaux de figure, ayant de la perspicacité en toute sagesse, du savoir, de la connaissance, du discernement, pleins d'intelligence, qui avaient aussi en eux la force de se tenir dans le palais du roi, à qui l'on enseignerait les lettres et la langue des Chaldéens.
1:5	Le roi leur assigna pour provision chaque jour une portion de la viande royale et du vin dont il buvait, afin qu'on les nourrisse ainsi pendant trois ans au bout desquels ils se tiendraient devant le roi.
1:6	Il y avait parmi eux, d'entre les fils de Yéhouda, Daniye'l, Chananyah, Mishaël et Azaryah.
1:7	Mais le chef des eunuques leur donna d'autres noms : il donna à Daniye'l le nom de Beltshatsar, à Chananyah celui de Shadrac, à Mishaël celui de Méshac et à Azaryah celui d'Abed-Négo.

### La fermeté de Daniye'l à Babel

1:8	Or Daniye'l résolut dans son cœur de ne pas se souiller par la portion de la viande du roi, ni par le vin qu'il buvait. C'est pourquoi il supplia le chef des eunuques afin qu'il ne l'oblige pas à se souiller.
1:9	Elohîm accorda à Daniye'l bonté et compassion devant le chef des eunuques.
1:10	Le chef des eunuques dit à Daniye'l : Je crains le roi, mon seigneur, qui a ordonné ce que vous devez manger et boire, car pourquoi verrait-il vos visages plus défaits que ceux des enfants de votre âge ? Vous exposeriez alors ma tête auprès du roi !
1:11	Daniye'l dit à Meltsar, l'intendant à qui le chef des eunuques avait remis la surveillance de Daniye'l, Chananyah, Mishaël et Azaryah :
1:12	Éprouve, je te prie, tes serviteurs pendant 10 jours, et qu'on nous donne des légumes à manger et de l'eau à boire.
1:13	Après cela, tu regarderas nos visages et ceux des enfants qui mangent la portion de la viande royale. Puis tu agiras envers tes serviteurs selon ce que tu auras vu.
1:14	Il les écouta dans cette affaire et les éprouva pendant 10 jours.
1:15	Au bout des 10 jours, leurs aspects paraissaient meilleurs et plus gras en chair que tous les enfants qui mangeaient la portion de la viande royale.
1:16	Ainsi Meltsar prenait la portion de leur viande et le vin qu'ils devaient boire, et leur donnait des légumes.
1:17	Elohîm donna à ces quatre enfants de la connaissance et de l'intelligence dans toutes les lettres, et de la sagesse. Daniye'l comprenait toutes les visions et tous les rêves.
1:18	Et à la fin des jours fixés par le roi pour qu'on les lui amène, le chef des eunuques les présenta à Neboukadnetsar.
1:19	Le roi s'entretint avec eux et, parmi eux tous, il ne s'en trouva aucun comme Daniye'l, Chananyah, Mishaël et Azaryah. Ils entrèrent au service du roi.
1:20	Quant à toute parole de sagesse et de discernement sur laquelle le roi les interrogeait, il trouvait dix fois plus de force en eux que dans tous les magiciens et les astrologues qui étaient dans tout son royaume.
1:21	Et Daniye'l fut là jusqu'à la première année du roi Cyrus.

## Chapitre 2

### Les sages de Babel condamnés à mort

2:1	La deuxième année du règne de Neboukadnetsar, Neboukadnetsar rêva des rêves. Son esprit fut agité et son sommeil finit.
2:2	Alors le roi fit appeler les magiciens, les astrologues, les sorciers et les Chaldéens, pour qu'ils lui racontent ses rêves. Ils vinrent donc et se présentèrent devant le roi.
2:3	Le roi leur dit : J'ai rêvé un rêve. Mon esprit est agité en cherchant à connaître ce rêve<!--Ce rêve annonce la future mise en place d'un gouvernement mondial. Voir commentaire en Da. 7:3.-->.
2:4	Et les Chaldéens répondirent au roi en langue araméenne<!--Da. 2:4 à 7:28, le livre est écrit en araméen.--> : Ô roi, vis éternellement ! Dis le rêve à tes serviteurs et nous expliquerons son interprétation.
2:5	Mais le roi répondit et dit aux Chaldéens : La chose m'a échappé : si vous ne me faites connaître le rêve et son interprétation, vous serez mis en pièces et vos maisons seront réduites en un tas d'immondices.
2:6	Mais si vous m'expliquez le rêve et son interprétation, vous recevrez devant moi, des dons, des présents et un grand honneur. Quoi qu'il en soit, expliquez-moi le rêve et son interprétation.
2:7	Ils répondirent pour la seconde fois et dirent : Que le roi dise le rêve à ses serviteurs et nous expliquerons son interprétation.
2:8	Le roi répondit et dit : Je sais maintenant que vous ne cherchez qu'à gagner du temps, parce que vous voyez que la chose m'a échappée.
2:9	Mais si vous ne me faites pas connaître le rêve, il y aura un même décret contre vous tous. Car vous vous êtes mis d'accord d'avance pour dire devant moi une parole mensongère et corrompue en attendant que les temps changent. Quoi qu'il en soit, dites-moi le rêve et je saurai que vous pouvez m'expliquer son interprétation.
2:10	Les Chaldéens répondirent au roi et dirent : Il n'y a aucun être humain sur la Terre qui puisse expliquer ce que le roi demande. Et aussi il n'y a ni roi, ni seigneur, ni gouverneur qui ait jamais demandé une telle chose à quelque magicien, astrologue ou Chaldéen que ce soit.
2:11	Car la chose que le roi demande est extrêmement difficile et il n'y a personne qui puisse l'expliquer au roi, excepté les élahhs dont la demeure n'est pas avec la chair<!--Jn. 1:14.-->.
2:12	À cause de cela, le roi s'irrita et se mit dans une très grande colère, et ordonna qu'on mette à mort tous les sages de Babel.
2:13	Le décret fut donc publié. On mettait à mort les sages et l'on cherchait Daniye'l et ses compagnons pour les mettre à mort.

### Daniye'l implore la miséricorde d'Élahh

2:14	Alors Daniye'l détourna l'exécution du conseil et l'arrêt donné à Aryok, chef des gardes du roi, qui était sorti pour mettre à mort les sages de Babel.
2:15	Et il demanda et dit à Aryok, commandant du roi : Pourquoi le décret du roi est-il si sévère ? Aryok exposa la chose à Daniye'l.
2:16	Et Daniye'l entra et pria le roi de lui accorder du temps pour expliquer l'interprétation au roi.
2:17	Alors Daniye'l alla dans sa maison et informa de cette affaire Chananyah, Mishaël et Azaryah, ses compagnons,
2:18	pour implorer la compassion devant l'Élahh des cieux au sujet de ce secret, afin qu'on ne mette pas à mort Daniye'l et ses compagnons avec le reste des sages de Babel.

### Le rêve de la grande statue révélé à Daniye'l

2:19	Et le secret fut révélé à Daniye'l dans une vision pendant la nuit. Et Daniye'l bénit l'Élahh des cieux.
2:20	Daniye'l prit donc la parole et dit : Béni soit le Nom d'Élahh, d'éternité en éternité, car c'est à lui qu'appartiennent la sagesse et la force<!--Job 12:13 ; Ap. 5:12, 7:12.--> !
2:21	C'est lui qui change les temps et les saisons, qui renverse les rois et qui établit les rois, qui donne la sagesse aux sages et la connaissance à ceux qui ont de l'intelligence.
2:22	C'est lui qui révèle les choses profondes et cachées, il connaît les choses qui sont dans les ténèbres et la lumière demeure avec lui<!--De. 29:29 ; Es. 48:6 ; Jé. 33:3 ; Lu. 12:2-3 ; Hé. 4:13.-->.
2:23	Ô Élahh de nos pères ! Je rends grâces et je te loue de ce que tu m'as donné de la sagesse et de la force, et de ce que tu m'as maintenant fait connaître ce que nous t'avons demandé, en nous faisant connaître le secret du roi.
2:24	C'est pourquoi Daniye'l alla auprès d'Aryok, à qui le roi avait ordonné de faire périr les sages de Babel. Il alla et lui parla ainsi : Ne fais pas périr les sages de Babel, mais fais-moi entrer devant le roi et j'expliquerai au roi l'interprétation.
2:25	Alors Aryok conduisit promptement Daniye'l devant le roi et lui parla ainsi : J'ai trouvé parmi les fils captifs de Yéhouda un homme qui donnera au roi l'interprétation de son rêve.
2:26	Le roi prit la parole et dit à Daniye'l, qu'on nommait Beltshatsar : Es-tu capable de me faire connaître le rêve que j'ai eu et son interprétation ?
2:27	Et Daniye'l répondit en présence du roi et dit : Ce que le roi demande est un secret que les sages, les astrologues, les magiciens et les devins ne sont pas capables d'expliquer au roi.
2:28	Mais il y a dans les cieux l'Élahh qui révèle les secrets et qui a fait connaître au roi Neboukadnetsar ce qui doit arriver dans les derniers jours<!--« Les derniers jours » voir commentaires dans Ge. 49:1.-->. Voici ton rêve et les visions de ta tête que tu as eues sur ta couche.
2:29	Sur ta couche, ô roi, il t'est monté des pensées concernant ce qui arrivera après ce temps-ci. Et celui qui révèle les secrets t'a fait connaître ce qui doit arriver.
2:30	Et moi, ce n’est pas par une sagesse qui soit en moi plus qu’en aucun des vivants que ce secret m’a été révélé, mais c’est afin que l’interprétation en soit donnée au roi et que tu connaisses les pensées de ton cœur.
2:31	Ô roi, tu regardais et tu voyais une grande statue<!--Voir annexe « La statue de Neboukadnetsar ».-->. Cette statue était grande et d'une splendeur extraordinaire. Elle était debout devant toi et son apparence effrayante.
2:32	La tête de cette statue était en or très fin, sa poitrine et ses bras étaient en argent, son ventre et ses cuisses étaient en cuivre,
2:33	ses jambes étaient en fer et ses pieds étaient en partie en fer et en partie en argile.
2:34	Tu regardais cela, jusqu'à ce qu'une pierre se détacha sans l'aide d'une main, frappa les pieds en fer et en argile de la statue et les brisa.
2:35	Alors le fer, l'argile, le cuivre, l'argent et l'or, furent brisés ensemble et devinrent comme la paille de l'aire en été, que le vent transporte çà et là et nulle trace n'en fut retrouvée. Mais la pierre qui avait frappé la statue devint une grande montagne et remplit toute la terre.

### Premier empire universel : Babel (Babylone)<!--Cp. Da. 7:4.-->

2:36	C'est là le rêve. Nous en donnerons maintenant l'interprétation devant le roi.
2:37	Ô roi, tu es le roi des rois, parce que l'Élahh des cieux t'a donné le royaume, la puissance, la force et la gloire.
2:38	Il a remis entre tes mains, en quelque lieu qu'ils habitent, les enfants des humains, les bêtes des champs et les oiseaux du ciel, et il t'a fait dominer sur eux tous. C'est toi qui es la tête en or<!--Jé. 27:6-7.-->.

### Deuxième et troisième empire : la Médie et la Perse, et la Grèce<!--La Médie et la Perse : cp. Da. 7:5, 8:20 ; La Grèce : cp. Da. 7:6, 8:21.-->

2:39	Mais après toi, il s'élèvera un autre royaume, moindre que le tien, puis un autre royaume, un troisième qui sera en cuivre et qui dominera sur toute la Terre.

### Quatrième empire : Rome<!--Cp. Da. 7:7, 9:26.-->

2:40	Il y aura un quatrième royaume, fort comme du fer. De même que le fer brise et écrase tout, ainsi il brisera et écrasera tout, comme le fer qui met tout en pièces.
2:41	Puisque tu as vu que les pieds et les orteils étaient en partie en argile de potier et en partie en fer, ce royaume sera divisé, mais il y aura en lui quelque chose de la force du fer, parce que tu as vu le fer mêlé avec l'argile de potier.
2:42	Les doigts des pieds étaient en partie en fer et en partie en argile. Ce royaume sera en partie fort et en partie fragile.
2:43	Puisque tu as vu du fer mêlé a de l'argile de potier, ils seront mêlés au moyen de la semence<!--Vient de l'araméen « zera » qui signifie « semence » et « descendant ». Il y a l'idée des mariages.--> humaine, mais ils ne seront pas unis l'un à l'autre de même que le fer ne se mêle pas avec l'argile.

### Le Royaume du Mashiah

2:44	Dans les jours de ces rois, l'Élahh des cieux suscitera un Royaume qui ne sera jamais détruit, et ce Royaume ne passera pas à un autre peuple. Il brisera et anéantira tous ces royaumes-là, et lui-même sera établi éternellement.
2:45	C'est pourquoi, tu as vu que de la montagne, une pierre a été coupée sans l'aide d'une main et qu'elle a brisé le fer, le cuivre, l'argile, l'argent et l'or. Le grand Élahh a fait connaître au roi ce qui arrivera après cela. Or le rêve est véritable et son interprétation est certaine.

### YHWH, l'Élahh qui révèle les secrets

2:46	Alors le roi Neboukadnetsar tomba sur sa face et se prosterna devant Daniye'l et il ordonna qu'on lui donne de quoi faire des sacrifices et des offrandes de bonne odeur.
2:47	Le roi parla à Daniye'l et lui dit : En vérité, votre Élahh est l'Élahh des élahhs, et le Seigneur des rois, et il révèle les secrets, puisque tu as pu déclarer ce secret.
2:48	Alors le roi éleva Daniye'l en dignité et lui fit de nombreux et riches présents. Il l'établit gouverneur sur toute la province de Babel et chef suprême de tous les sages de Babel.
2:49	Daniye'l demanda au roi de remettre l'administration de la province de Babel à Shadrac, Méshac et Abed-Négo. Et Daniye'l se tenait à la porte du roi.

## Chapitre 3

### La statue d'or de Neboukadnetsar

3:1	Le roi Neboukadnetsar fit une statue d'or<!--Neboukadnetsar est un type de l'Anti-Mashiah (Antichrist) qui s'oppose aux plans d'Elohîm. Contrairement à la statue composée de plusieurs métaux qu'il avait vue en rêve, et où il était représenté par la tête en or (Da. 2:38), il s'est fait construire une statue entièrement en or, se déclarant ainsi symboliquement invincible et immortel. En agissant de la sorte, Neboukadnetsar s'était fait dieu et a exigé par conséquent d'être adoré (2 Th. 2:3-4). D'un point de vue prophétique, cette statue annonce la mise en place d'une religion mondiale, fruit d'un mélange entre la politique et la religion.-->, dont la hauteur était de 60 coudées<!--30 mètres de haut. Voir tableau de poids et de mesures.-->, et la largeur de 6 coudées<!--3 mètres.-->. Il la dressa dans la vallée de Doura, dans la province de Babel.
3:2	Le roi Neboukadnetsar envoya des messagers pour rassembler les satrapes<!--Gouverneur d'une province de Perse, haut fonctionnaire de l'Empire babylonien, et du royaume de Darius le Mède.-->, les intendants, les gouverneurs, les conseillers, les trésoriers, les jurisconsultes, les juges, et tous les magistrats des provinces, afin qu'ils se rendent à la dédicace de la statue que le roi Neboukadnetsar avait dressée.
3:3	Alors se rassemblèrent les satrapes, les intendants, les gouverneurs, les conseillers, les trésoriers, les jurisconsultes, les juges, et tous les magistrats des provinces, pour la dédicace de la statue que le roi Neboukadnetsar avait dressée. Ils se tenaient debout devant la statue que le roi Neboukadnetsar avait dressée.
3:4	Le héraut proclama avec force : Il vous est ordonné, peuples, nations, et hommes de toutes langues :
3:5	Au moment où vous entendrez le son de la corne, de la flûte, de la cithare, de la lyre, de la harpe, de la cornemuse, et de toutes sortes d'instruments de musique, vous vous jetterez à terre et vous adorerez la statue d'or que le roi Neboukadnetsar a dressée.
3:6	Quiconque ne se jettera pas à terre et n'adorera pas sera jeté à l'instant même au milieu de la fournaise de feu ardent.
3:7	C'est pourquoi, au moment même où tous les peuples entendirent le son de la corne, de la flûte, de la cithare, de la lyre, de la harpe, et de toutes sortes d'instruments de musique, tous les peuples, les nations, et les hommes de toutes les langues, se jetèrent à terre et adorèrent la statue d'or que le roi avait dressée.

### Le refus de l'idolâtrie

3:8	Alors à ce même moment, des hommes chaldéens s'approchèrent et accusèrent<!--Littéralement : « pour manger un morceau des Juifs ».--> les Juifs.
3:9	Et ils parlèrent et dirent au roi Neboukadnetsar : Roi, vis éternellement !
3:10	Toi-même, ô roi, tu as pris un décret d'après lequel tout être humain qui entendrait le son de la corne, de la flûte, de la cithare, de la lyre, de la harpe, de la cornemuse, et de toutes sortes d'instruments de musique, devrait se jeter à terre et adorer la statue d'or,
3:11	et que quiconque ne se jetterait pas à terre et ne l'adorerait pas, serait jeté au milieu d'une fournaise de feu ardent.
3:12	Il y a des hommes juifs, à qui tu as remis l’administration de la province de Babel, Shadrac, Méshac et Abed-Négo, ces hommes-là, ô roi, ne tiennent aucun compte de ton décret. Ils ne servent pas tes élahhs et n'adorent pas la statue d'or que tu as dressée.
3:13	Alors le roi Neboukadnetsar, saisi de colère et de rage, ordonna qu'on amène Shadrac, Méshac, et Abed-Négo. Et ces hommes furent amenés devant le roi.
3:14	Et le roi Neboukadnetsar prit la parole et leur dit : Est-il vrai, Shadrac, Méshac, et Abed-Négo, que vous ne servez pas mes élahhs et que vous n'adorez pas la statue d'or que j'ai dressée ?
3:15	Maintenant si vous êtes prêts, au moment où vous entendrez le son de la corne, de la flûte, de la cithare, de la lyre, de la harpe, de la cornemuse, et de toutes sortes d'instruments de musique, vous vous jetterez à terre, et vous adorerez la statue que j'ai faite. Si vous ne l'adorez pas, vous serez jetés à l'instant au milieu de la fournaise de feu ardent. Et qui est l'élahh qui vous délivrera de mes mains ?
3:16	Shadrac, Méshac et Abed-Négo répondirent et dirent au roi Neboukadnetsar : Nous n'avons pas besoin de te répondre sur ce sujet.
3:17	Voici, notre Élahh que nous servons est capable de nous délivrer de la fournaise de feu ardent, et il nous délivrera de ta main, ô roi !
3:18	Sinon, sache, ô roi, que nous ne servirons pas tes élahhs, et que nous n'adorerons pas la statue d'or que tu as dressée.

### L'épreuve de la fournaise de feu ardent

3:19	Alors Neboukadnetsar fut rempli de rage, et il changea de visage en tournant ses regards contre Shadrac, Méshac, et Abed-Négo. Il prit la parole et ordonna de chauffer la fournaise sept fois plus qu'on avait coutume de la chauffer.
3:20	Puis il commanda aux hommes les plus forts et les plus vaillants qui étaient dans son armée de lier Shadrac, Méshac, et Abed-Négo, pour les jeter dans la fournaise de feu ardent.
3:21	Et en même temps ces hommes furent liés avec leurs manteaux, leurs tuniques, leurs turbans et leurs vêtements, et ils furent jetés au milieu de la fournaise de feu ardent.
3:22	Et parce que l'ordre du roi était sévère, et que la fournaise était extraordinairement chauffée, la flamme tua les hommes qui y avaient jeté Shadrac, Méshac, et Abed-Négo.
3:23	Et ces trois hommes, Shadrac, Méshac, et Abed-Négo, tombèrent tous liés au milieu de la fournaise de feu ardent.

### La grandeur de YHWH, l'Élahh qui délivre

3:24	Alors le roi Neboukadnetsar fut effrayé et se leva précipitamment. Il prit la parole et il dit à ses conseillers : N'avons-nous pas jeté trois hommes liés au milieu du feu ? Ils répondirent et dirent au roi : Certainement, ô roi !
3:25	Il répondit et dit : Voici, je vois quatre hommes sans liens qui marchent au milieu du feu, et il n'y a en eux aucun dommage, et l'apparence du quatrième est semblable à celle d'un fils d'Élahh.
3:26	Alors Neboukadnetsar s'approcha vers la porte de la fournaise de feu ardent, et prenant la parole, il dit : Shadrac, Méshac, et Abed-Négo, serviteurs d'Élahh Très-Haut, sortez et venez ! Alors Shadrac, Méshac, et Abed-Négo sortirent du milieu du feu.
3:27	Puis les satrapes, les intendants, les gouverneurs, et les conseillers du roi se rassemblèrent pour contempler ces hommes-là, et le feu n'avait eu aucun pouvoir sur leurs corps, et aucun cheveu de leur tête n'était brûlé, et leurs caleçons n'étaient pas endommagés, et l'odeur du feu n'avait pas passé sur eux.
3:28	Alors Neboukadnetsar prit la parole et dit : Béni soit l'Élahh de Shadrac, Méshac, et Abed-Négo, lequel a envoyé son ange et délivré ses serviteurs qui ont eu confiance en lui, et qui ont changé<!--Vient de l'araméen « shena » qui signifie « changer, être altéré, être changé ».--> le commandement du roi et livré leur corps afin de ne servir et de n'adorer aucun autre élahh que leur Élahh<!--Mt. 4:10 ; Ac. 4:19, 5:29.-->.

### Shadrac, Méshac et Abed-Nego élevés par le roi

3:29	Voici donc le décret que j'ai pris : Tout homme, à quelque nation ou langue qu'il appartienne, qui parlera avec négligence<!--Vient de l'araméen « shalah » qui signifie « négligence, relâchement ».--> d'Élahh de Shadrac, Méshac, et Abed-Négo, sera mis en pièces, et sa maison sera réduite en un tas d'immondices, parce qu'il n'y a aucun autre élahh qui puisse délivrer comme lui.
3:30	Alors le roi fit réussir Shadrac, Méshac, et Abed-Négo dans la province de Babel.

## Chapitre 4

### La suprématie de YHWH déclarée aux nations

4:1	Le roi Neboukadnetsar, à tous les peuples, aux nations, aux hommes de toutes langues qui habitent sur toute la Terre : Que votre paix soit multipliée !
4:2	Il m'a semblé bon de vous expliquer les signes et les merveilles que l'Élahh Très-Haut a opérés à mon égard.
4:3	Ô que ses signes sont grands, et ses merveilles pleines de force ! Son règne est un règne éternel, et sa domination subsiste de génération en génération<!--Ps. 102:13 ; La. 5:19 ; Lu. 1:33.-->.

### La vision du grand arbre

4:4	Moi, Neboukadnetsar, j'étais tranquille dans ma maison et heureux dans mon palais,
4:5	lorsque j'ai eu un rêve qui m'a fait peur. Les pensées qui me poursuivaient sur ma couche et les visions de ma tête m'effrayaient.
4:6	J'ai pris alors un décret afin qu'on fasse venir devant moi tous les sages de Babel afin de me faire connaître l'interprétation du rêve.
4:7	Alors les magiciens, les astrologues, les Chaldéens et les devins sont venus. J'ai raconté le rêve devant eux, mais ils ont été incapables de m'en faire connaître l'interprétation.
4:8	À la fin, s'est présenté devant moi Daniye'l, dont le nom est Beltshatsar, selon le nom de mon élahh, et qui a en lui l'esprit des élahhs saints. Je lui ai raconté le rêve :
4:9	Beltshatsar, chef des magiciens, comme je sais que l'esprit des élahhs saints est en toi, et qu'aucun secret n'est contraignant pour toi, dis-moi les visions que j'ai eues en rêve, et donne-moi son interprétation.
4:10	Voici les visions de ma tête, pendant que j'étais sur ma couche. Je regardais, et voici, il y avait un arbre au milieu de la terre d'une grande hauteur.
4:11	Cet arbre était devenu grand et fort, sa cime atteignait les cieux, et on le voyait des extrémités de toute la terre.
4:12	Son feuillage était beau, et son fruit abondant, et il portait de la nourriture pour tous. Les bêtes des champs s'abritaient sous son ombre, les oiseaux du ciel habitaient dans ses branches, et toute chair se nourrissait de lui.
4:13	Dans les visions de ma tête que j'avais sur ma couche, je regardais, et voici, un de ceux qui veillent et qui sont saints descendit des cieux.
4:14	Il cria avec force et parla ainsi : Coupez l'arbre, et ébranchez-le ! Secouez son feuillage et dispersez son fruit ! Que les bêtes s'enfuient de dessous et les oiseaux du milieu de ses branches !
4:15	Mais laissez en terre le tronc où se trouvent ses racines, et liez-le avec des chaînes de fer et de cuivre, qu'il soit parmi l'herbe des champs. Qu'il soit trempé de la rosée des cieux, et qu'il ait, comme les bêtes, l'herbe de la terre pour partage.
4:16	Que son cœur d'homme soit changé, et qu'un cœur de bête lui soit donné, et que sept temps passent sur lui !
4:17	Cette sentence est le décret de ceux qui veillent, cette résolution est un ordre des saints, afin que les vivants sachent que le Très-Haut domine sur le royaume des humains, qu'il le donne à qui il lui plaît, et qu'il y établit le plus vil des humains<!--Cette vérité est confirmée par Paulos (Paul) dans Ro. 13:1. C'est Elohîm qui choisit souverainement qui il établit à la tête d'un pays. Selon les Écritures, toute autorité vient d'Elohîm.-->.
4:18	Voilà le rêve que j'ai eu, moi, le roi Neboukadnetsar. Toi donc Beltshatsar, donnes-en l'interprétation, puisque tous les sages de mon royaume ne peuvent me la donner. Mais toi, tu le peux, parce que l'esprit des élahhs saints est en toi.

### Interprétation de la vision

4:19	Alors Daniye'l, dont le nom est Beltshatsar, demeura épouvanté environ une heure, et ses pensées le troublaient. Le roi reprit et dit : Beltshatsar, que le rêve et son interprétation ne te troublent pas ! Et Beltshatsar répondit : Mon seigneur, que le rêve soit pour ceux qui te haïssent, et son interprétation pour tes ennemis !
4:20	L'arbre que tu as vu, qui était devenu grand et fort, dont la cime s'élevait jusqu'aux cieux, et qu'on voyait de tous les points de la terre ;
4:21	cet arbre, dont le feuillage était beau, et les fruits abondants, qui portait de la nourriture pour tous, sous lequel s'abritaient les bêtes des champs, et parmi les branches duquel les oiseaux du ciel habitaient,
4:22	c'est toi, ô roi, qui es devenu grand et fort, et ta grandeur s'est accrue et s'est élevée jusqu'aux cieux, et dont la domination s'étend jusqu'aux extrémités de la terre.
4:23	Le roi a vu l'un de ceux qui veillent et qui sont saints, descendre des cieux et dire : Coupez l'arbre et ébranchez-le ! Toutefois, laissez en terre le tronc où se trouvent ses racines, et liez-le avec des chaînes de fer et de cuivre, parmi l'herbe des champs ; qu'il soit arrosé de la rosée du ciel et que son partage soit avec les bêtes des champs, jusqu'à ce que sept temps soient passés sur lui.
4:24	Voici l'interprétation, ô roi, voici le décret du Très-Haut, qui s'accomplira sur mon seigneur, le roi.
4:25	On te chassera du milieu des humains, tu auras ta demeure avec les bêtes des champs, et l'on te donnera de l'herbe comme aux bœufs, et tu seras arrosé de la rosée du ciel, et sept temps passeront sur toi, jusqu'à ce que tu reconnaisses que le Très-Haut domine sur le royaume des humains, et qu'il le donne à qui il lui plaît.
4:26	L'ordre de laisser le tronc où se trouvent les racines de cet arbre signifie que ton royaume te sera rendu, dès que tu auras reconnu que les cieux dominent.
4:27	C'est pourquoi, ô roi, que mon conseil te soit agréable : rachète tes péchés par la justice et tes iniquités en faisant miséricorde aux pauvres, voici, ce sera une prolongation pour ta prospérité.

### Le roi Neboukadnetsar déchu à cause de son orgueil

4:28	Toutes ces choses arrivèrent au roi Neboukadnetsar.
4:29	Au bout de douze mois, comme il se promenait dans le palais royal de Babel,
4:30	le roi prit la parole et dit : N'est-ce pas ici Babel la grande, que j'ai bâtie pour être la maison royale, par la puissance de ma force et pour la gloire de ma magnificence ?
4:31	La parole était encore dans la bouche du roi, qu'une voix descendit du ciel, disant : Roi Neboukadnetsar, on t'annonce que ton royaume va t'être ôté.
4:32	On te chassera du milieu des humains, tu auras ta demeure avec les bêtes des champs. On te donnera de l'herbe à manger comme aux bœufs, et sept temps passeront sur toi, jusqu'à ce que tu reconnaisses que le Très-Haut domine sur le royaume des humains, et qu'il le donne à qui il lui plaît.
4:33	Au même instant, la parole s'accomplit sur Neboukadnetsar. Il fut chassé du milieu des humains, il mangea de l'herbe comme les bœufs, et son corps fut arrosé de la rosée du ciel jusqu'à ce que son poil croisse comme les plumes des aigles, et ses ongles comme ceux des oiseaux.

### Le roi Neboukadnetsar est rétabli ; il loue l'Elohîm Très-Haut

4:34	Mais à la fin de ces jours-là, moi Neboukadnetsar, je levai mes yeux vers le ciel, et la connaissance me revint. J'ai béni le Très-Haut, j'ai loué et glorifié celui qui vit éternellement, celui dont la domination est une domination éternelle, et dont le règne subsiste de génération en génération.
4:35	Tous les habitants de la Terre ne comptent pour rien devant lui. Il agit comme il lui plaît avec l'armée des cieux et avec les habitants de la Terre, et il n'y a personne qui empêche sa main, et qui lui dise : Que fais-tu<!--Es. 45:9 ; Jé. 23:18-22 ; Ps. 115:3 ; Job 9:12.--> ?
4:36	À l'instant même, la connaissance me revint, la gloire de mon royaume, ma magnificence et ma splendeur me furent rendues : mes conseillers et mes grands me redemandèrent, je fus rétabli dans mon royaume et ma grandeur fut extraordinairement augmentée.
4:37	Maintenant, moi, Neboukadnetsar, je loue, j'exalte, et je glorifie le Roi des cieux, dont toutes les œuvres sont véritables et les voies justes, et qui peut abaisser ceux qui marchent avec orgueil<!--De. 32:4 ; Es. 13:11 ; Ez. 17:24 ; Ps. 145:17.-->.

## Chapitre 5

### Les vases du temple souillés

5:1	Le roi Belshatsar donna un grand festin à ses seigneurs au nombre de 1 000, et en présence de ces 1 000 il buvait du vin.
5:2	Et ayant goûté au vin, Belshatsar ordonna qu'on apporte les vases d'or et d'argent que Neboukadnetsar, son père, avait enlevés du temple de Yeroushalaim<!--2 Ch. 36:10.-->, afin que le roi et ses seigneurs, ses femmes et ses concubines, s'en servent pour boire.
5:3	Alors furent apportés les vases d'or qui avaient été enlevés du temple, de la maison d'Élahh qui était à Yeroushalaim, et le roi, ses seigneurs, ses femmes et ses concubines s'en servirent pour boire.
5:4	Ils burent du vin et ils louèrent leurs élahhs d'or, d'argent, de cuivre, de fer, de bois et de pierre.

### L'écriture sur la muraille

5:5	Et à ce moment-là sortirent les doigts d'une main humaine et ils écrivirent, devant le chandelier, sur le plâtre de la muraille du palais royal ; et le roi voyait cette partie de main qui écrivait.
5:6	Alors le roi changea de couleur, et ses pensées l'effrayèrent, si bien que les jointures de ses reins se desserrèrent, et ses genoux se cognèrent l'un contre l'autre.
5:7	Puis le roi cria avec force qu'on fasse venir les astrologues, les Chaldéens et les devins ; et le roi prit la parole et dit aux sages de Babel : Quiconque lira cette écriture et m'expliquera son interprétation sera revêtu de pourpre, il aura un collier d'or à son cou, et sera le troisième dans le gouvernement du royaume.
5:8	Alors tous les sages du roi entrèrent, mais ils ne furent pas capables de lire l'écriture et d'en donner au roi l'interprétation.
5:9	Alors le roi Belshatsar fut très effrayé, il changea de couleur, et ses seigneurs furent consternés.

### Interprétation de l'écriture : division de l'Empire babylonien

5:10	Or la reine entra dans la maison du festin, à cause de ce qui était arrivé au roi et à ses seigneurs. La reine prit la parole et dit : Ô roi, vis éternellement ! Que tes pensées ne te troublent pas, et que ton visage ne change pas de couleur !
5:11	Il y a dans ton royaume un homme qui a en lui l'esprit des élahhs saints. Et du temps de ton père, on trouva en lui une lumière, une intelligence, et une sagesse semblable à la sagesse des élahhs. Aussi, le roi Neboukadnetsar, ton père, et le roi, ton père<!--Belshatsar était le petit-fils de Neboukadnetsar qui avait régné conjointement avec son père, Nabonide, à partir de 552 av. J.-C.-->, ô roi, l'établirent chef des magiciens, des astrologues, des Chaldéens et des devins,
5:12	parce qu'on trouva chez lui, chez Daniye'l, à qui le roi lui-même donna le nom de Beltshatsar, un esprit supérieur, de la connaissance et de l'intelligence, pour interpréter les rêves, pour expliquer les questions obscures et résoudre les choses difficiles. Que Daniye'l soit donc appelé et il expliquera l'interprétation.
5:13	Alors Daniye'l fut introduit devant le roi. Le roi prit la parole et dit à Daniye'l : Es-tu ce Daniye'l, l'un des fils des captifs de Yéhouda, que le roi, mon père, a amenés de Yéhouda ?
5:14	J'ai appris sur ton compte que tu as en toi l'esprit des élahhs, et qu'on trouve en toi une lumière, une intelligence et une sagesse extraordinaire.
5:15	On vient d'amener devant moi les sages et les astrologues, afin qu'ils lisent cette écriture et m'en donnent l'interprétation, mais ils ne sont pas capables d'expliquer l'interprétation de la chose.
5:16	J'ai appris que tu peux interpréter et résoudre les choses difficiles. Maintenant donc, si tu peux lire cette écriture, et m'en donner l'interprétation, tu seras revêtu de pourpre, tu porteras à ton cou un collier d'or, et tu seras le troisième dans le gouvernement du royaume.
5:17	Alors Daniye'l répondit et dit en présence du roi : Que tes dons soient à toi et donne tes récompenses à un autre ! Toutefois je lirai l'écriture au roi, et je lui en donnerai l'interprétation.
5:18	Ô roi ! L'Élahh Très-Haut avait donné à Neboukadnetsar, ton père, le royaume, la magnificence, la gloire et l'honneur.
5:19	Et à cause de la grandeur qu'il lui avait donnée, tous les peuples, les nations, et les hommes de toutes langues tremblaient de peur devant lui. Car il faisait mourir ceux qu'il voulait, et il laissait la vie à ceux qu'il voulait. Il élevait ceux qu'il voulait, et il abaissait ceux qu'il voulait.
5:20	Mais lorsque son cœur s'éleva et que son esprit s'endurcit jusqu'à l'arrogance, il fut renversé de son trône royal et dépouillé de sa gloire.
5:21	Il fut chassé du milieu des enfants d'humains, son cœur fut rendu semblable à celui des bêtes, et sa demeure fut avec les ânes sauvages. On lui donna comme aux bœufs de l'herbe à manger, et son corps fut arrosé de la rosée du ciel, jusqu'à ce qu'il reconnaisse que l'Élahh Très-Haut domine sur les royaumes des humains, et qu'il y établit ceux qu'il lui plaît.
5:22	Et toi aussi, Belshatsar, son fils, tu n'as pas humilié ton cœur, quoique tu saches toutes ces choses.
5:23	Mais tu t'es élevé contre le Seigneur des élahhs. Les vases de sa maison ont été apportés devant toi et vous vous en êtes servis pour boire du vin, toi et tes seigneurs, tes femmes et tes concubines. Tu as loué les élahhs en argent, en or, en cuivre, en fer, en bois et en pierre, qui ne voient pas, qui n'entendent pas et qui ne savent rien, et tu n'as pas glorifié l'Élahh dans la main duquel est ton souffle et toutes tes voies<!--Job 12:10, 33:4.-->.
5:24	Alors de sa part a été envoyée cette partie de main, et cette écriture a été gravée.
5:25	Voici l'écriture qui a été gravée : Mené, mené, teqel, parsîn.
5:26	Et voici l'interprétation de ces paroles. Mené (Compté) : Élahh a fait les comptes de ton règne et y a mis fin.
5:27	Teqel (Pesé) : tu as été pesé dans la balance et tu as été trouvé léger.
5:28	Parsîn (Divisé) : ton royaume a été divisé et donné aux Mèdes et aux Perses.
5:29	Alors Belshatsar donna des ordres, et l'on revêtit Daniye'l de pourpre, on lui mit un collier d'or au cou, et on publia qu'il serait le troisième dans le gouvernement du royaume.

### Règne de Darius, le Mède

5:30	Cette même nuit, Belshatsar, roi des Chaldéens, fut tué.
5:31	Darius, le Mède, reçut le royaume, étant âgé d'environ 62 ans<!--Es. 13:17, 21:2 ; Jé. 51:11.-->.

## Chapitre 6

6:1	Or Darius trouva bon d'établir sur le royaume 120 satrapes, qui devaient être répartis dans tout le royaume.
6:2	Il mit à leur tête trois chefs, au nombre desquels était Daniye'l, afin que ces satrapes leur rendent compte, et que le roi ne souffre aucun préjudice.
6:3	Mais Daniye'l surpassait les autres chefs et satrapes, parce qu'il y avait en lui un esprit supérieur et le roi pensait à l'établir sur tout le royaume.

### Daniye'l refuse l'idolâtrie et persévère dans la prière

6:4	Alors les chefs et les satrapes cherchèrent à trouver une occasion d'accuser Daniye'l au sujet des affaires du royaume. Mais ils ne purent trouver en lui aucune occasion, ni corruption, parce qu'il était digne de confiance, et qu'on ne trouvait en lui ni négligence, ni corruption.
6:5	Et ces hommes dirent : Nous ne trouverons aucune occasion d'accuser ce Daniye'l, à moins que nous n'en trouvions une dans la loi de son Élahh.
6:6	Alors ces chefs et ces satrapes se rendirent tumultueusement auprès du roi et lui parlèrent ainsi : Roi Darius, vis éternellement !
6:7	Tous les chefs de ton royaume, les intendants, les satrapes, les conseillers et les gouverneurs ont tenu conseil ensemble pour établir un statut royal et un décret de restriction ferme : tout être humain qui, dans l'espace de 30 jours, adressera une requête à quelque élahh ou à quelque être humain, excepté à toi, ô roi, sera jeté dans la fosse aux lions.
6:8	Maintenant donc, ô roi, établis le décret de restriction et signe l'écrit afin qu'il soit irrévocable, selon le décret des Mèdes et des Perses qui est immuable.
6:9	Là-dessus, le roi Darius signa l'écrit et le décret de restriction.
6:10	Lorsque Daniye'l sut que l'écrit était signé, il entra dans sa maison, où les fenêtres de sa chambre haute étaient ouvertes dans la direction de Yeroushalaim. Trois fois par jour il se mettait à genoux, priait et rendait grâce devant son Élahh, comme il le faisait auparavant<!--1 R. 8:44 ; Ps. 55:17-18.-->.
6:11	Alors ces hommes entrèrent tumultueusement, et ils trouvèrent Daniye'l qui priait et implorait une faveur devant son Elohîm.
6:12	Puis ils s'approchèrent du roi et lui parlèrent au sujet du décret de restriction du roi : N'as-tu pas signé un décret de restriction portant que tout être humain qui, dans l'espace de 30 jours, adresserait une requête à quelque élahh ou à quelque être humain, excepté à toi, ô roi, serait jeté dans la fosse aux lions ? Le roi répondit : La chose est certaine, selon la loi des Mèdes et des Perses, qui est irrévocable.
6:13	Ils prirent de nouveau la parole et dirent au roi : Daniye'l, l'un des captifs de Yéhouda, n'a tenu aucun compte de toi, ô roi, ni du décret de restriction que tu as signé, et il fait sa prière trois fois par jour.
6:14	Le roi fut très affligé quand il entendit cela. Il prit à cœur de délivrer Daniye'l, et jusqu'au coucher du soleil il s'efforça de le sauver.
6:15	Mais ces hommes entrèrent tumultueusement auprès du roi, et lui dirent : Sache, ô roi, que la loi des Mèdes et des Perses exige que tout décret de restriction ou tout statut établi par le roi soit irrévocable.

### Daniye'l demeure fidèle à Élahh face à la mort

6:16	Alors le roi ordonna qu'on amène Daniye'l, et qu'on le jette dans la fosse aux lions. Et le roi prenant la parole dit à Daniye'l : Ton Élahh que tu sers constamment sera celui qui te délivrera.
6:17	On apporta une pierre, et on la mit sur l'ouverture de la fosse. Le roi la scella de son anneau et de l'anneau de ses seigneurs afin que rien ne soit changé à l'égard de Daniye'l.

### YHWH fait justice à Daniye'l

6:18	Ensuite, le roi se rendit dans son palais ; il passa la nuit à jeun, il ne fit pas venir des danseuses<!--Le mot « danseuse » vient de l'araméen « dachavah » qui signifie « divertissement », « instrument de musique », « danseuse », « concubine », « musique ».--> auprès de lui, et il ne put se livrer au sommeil.
6:19	Puis le roi se leva au point du jour, avec l'aurore, et il alla précipitamment à la fosse aux lions.
6:20	En s'approchant de la fosse, il cria d'une voix triste : Daniye'l ! Le roi prit la parole et dit à Daniye'l : Daniye'l, serviteur d'Élahh vivant, ton Élahh, que tu sers avec persévérance, a-t-il pu te délivrer des lions ?
6:21	Alors Daniye'l dit au roi : Ô roi, vis éternellement !
6:22	Mon Élahh a envoyé son ange, et a tellement fermé la gueule des lions, qu'ils ne m'ont fait aucun mal, parce que j'ai été trouvé innocent devant lui, et même à ton égard, ô roi, je n'ai commis aucune faute.
6:23	Alors le roi fut extrêmement heureux pour lui et il ordonna qu'on fasse retirer Daniye'l de la fosse. Ainsi Daniye'l fut retiré de la fosse, et on ne trouva sur lui aucune blessure, parce qu'il avait cru en son Élahh.
6:24	Le roi ordonna que ces hommes qui avaient accusé Daniye'l, soient amenés et jetés dans la fosse aux lions, eux, leurs enfants et leurs femmes, et avant qu'ils soient parvenus au fond de la fosse, les lions se saisirent d'eux, et leur brisèrent tous les os.

### Les merveilles de YHWH proclamées aux nations

6:25	Après cela, le roi Darius écrivit à tous les peuples, à toutes les nations, aux hommes de toutes les langues, qui habitent sur toute la Terre : Que votre paix soit multipliée !
6:26	Voici le décret que j'établis : Dans toute l'étendue de mon royaume, on doit trembler et craindre devant l'Élahh de Daniye'l. Car il est l'Élahh vivant et il subsiste éternellement. Son Royaume ne sera jamais détruit, et sa domination durera jusqu'à la fin<!--Lu. 1:33.-->.
6:27	Il sauve et délivre, il fait des prodiges et des merveilles dans les cieux et sur la Terre, et il a délivré Daniye'l de la puissance des lions.
6:28	Ainsi Daniye'l prospéra sous le règne de Darius, et sous le règne de Cyrus, roi de Perse.

## Chapitre 7

### Rêve des quatre bêtes

7:1	La première année de Belshatsar, roi de Babel, Daniye'l eut un rêve et des visions de sa tête, étant sur sa couche. Ensuite il écrivit le rêve et il relata les principales choses.
7:2	Daniye'l donc parla et dit : Je regardais dans ma vision nocturne, et voici, les quatre vents des cieux se levèrent avec impétuosité sur la grande mer.
7:3	Puis quatre grandes bêtes<!--Le rêve de Daniye'l fait écho au rêve de la statue qu'a eu Neboukadnetsar en Da.2 (voir annexe « la statue de Neboukadnetsar »). Chaque bête représente un empire historique :\\- Le lion ailé pour l'Empire néo-babylonien (625 – 539 av. J.-C.) qui s'est démarqué par la rapidité de ses conquêtes (Jé. 4:13 ; Ha. 1:6-8).\\- L'ours pour l'Empire Médo-Perse (539 – 331 av. J.-C.). Les trois côtes dans la gueule de l'animal représentent respectivement la conquête de la Lydie (546 av. J.-C.), de la Babylonie (539 av. J.-C.) et de l'Égypte (524 av. J.-C.).\\- Le léopard ailé pour l'Empire Gréco-Macédonien (331 – 146 av. J.-C.), qui illustre bien la rapidité des conquêtes de cet Empire à l'ascension fulgurante.\\- La quatrième bête pour l'Empire romain (146 av. J.-C. – 476 ap. J.-C.) qui s'est illustré par l'immensité de son territoire, sa longévité et son caractère violent. Aujourd'hui, l'Empire romain n'existe plus officiellement. Il a toutefois été remplacé par l'Union européenne, son héritière géographique, culturelle et spirituelle qui, selon les Écritures, établira un gouvernement mondial. En effet, ce quatrième animal est l'équivalent de la bête qu'a vue Yohanan en Ap. 13. La particularité de cette créature c'est qu'elle a eu une blessure mortelle (la fin de l'Empire Romain en 476 ap. J.-C.) qui a été guérie (restauration de l'Empire romain sous une autre forme).--> montèrent de la mer, différentes les unes des autres.

### Premier empire : Babel (Babylone)<!--Cp. Da. 2:38.-->

7:4	La première était semblable à un lion, et avait des ailes d'aigle. Je la regardai jusqu'à ce que ses ailes furent arrachées. Elle fut soulevée de terre et mise debout sur ses pieds comme un être humain, et un cœur d'être humain lui fut donné.

### Deuxième empire : la Médie et la Perse<!--Cp. Da. 2:39, 8:20.-->

7:5	Et voici une autre bête, une seconde, semblable à un ours. Elle se tenait sur un côté, ayant trois côtes dans la gueule entre ses dents. On lui disait ainsi : Lève-toi, mange beaucoup de chair !

### Troisième empire : la Grèce<!--Cp. Da. 2:39, 8:21-22, 11:2-4.-->

7:6	Après cela je regardai, et voici une autre bête, semblable à un léopard. Elle avait sur son dos quatre ailes d'oiseau, et cette bête avait quatre têtes, et la domination lui fut donnée.

### Quatrième empire : Rome<!--Cp. Da. 2:40-43, 7:23-24, 9:26.-->

7:7	Après cela, je regardai dans mes visions nocturnes, et voici, il y avait une quatrième bête, effrayante, terrible et extraordinairement forte. Elle avait de grandes dents en fer. Elle mangeait, brisait et foulait aux pieds ce qui restait. Elle était différente de toutes les bêtes précédentes et avait dix cornes.

### Les dix cornes et la petite corne<!--Da. 7:24-27.-->

7:8	Je considérai ses cornes, et voici, une autre petite corne sortit du milieu d'elles, et trois des premières cornes furent arrachées par elle. Et sur cette corne, il y avait des yeux comme des yeux d'être humain, et une bouche qui proférait de grandes choses.

### Le règne de YHWH, l'Ancien des jours<!--Cp. Mt. 24:27-30 ; Ap. 19:11-21.-->

7:9	Je regardai jusqu'à ce que les trônes soient placés. Et l'Ancien des jours s'assit. Son vêtement était blanc comme la neige, et les cheveux de sa tête étaient comme de la laine pure<!--Voir Ap. 1:14.-->. Son trône était des flammes de feu, et ses roues un feu ardent.
7:10	Un fleuve de feu coulait et sortait de devant lui. Mille milliers le servaient et des myriades de myriades se tenaient devant lui. Le jugement se tint, et les livres furent ouverts<!--1 R. 22:19 ; Ps. 68:18 ; Ap. 5:11.-->.
7:11	Je regardai alors, à cause du bruit des grandes paroles que proférait la corne. Pendant que je regardais, la bête fut tuée, et son corps fut détruit et livré pour être brûlé au feu.
7:12	Les autres bêtes furent dépouillées de leur domination, mais une prolongation de vie leur fut accordée jusqu'à un temps déterminé.

### La domination du Fils de l'homme est éternelle<!--Cp. Ap. 5:1-14.-->

7:13	Je regardais dans ces visions de la nuit et je vis comme le Fils de l'homme qui venait avec les nuées des cieux. Il vint jusqu'à l'Ancien des jours, et on le fit approcher de lui<!--Jud. 1:15 ; Ap. 19:14.-->.
7:14	Et il lui donna la domination, la gloire et le règne. Tous les peuples, les nations et les langues le serviront. Sa domination est une domination éternelle qui ne passera pas, et son règne ne sera jamais détruit.

### Interprétation de la vision du quatrième animal

7:15	Pour moi, Daniye'l, mon esprit fut troublé au-dedans de mon corps, et les visions de ma tête m'effrayèrent.
7:16	Je m'approchai de l'un des assistants et lui demandai la vérité au sujet de tout cela. Il me parla, et me donna l'interprétation de ces choses, en disant :
7:17	Ces quatre grandes bêtes sont quatre rois, qui s'élèveront de la Terre.
7:18	Mais les saints d'Elyown<!--Terme araméen, qui signifie « le Très-Haut, le Plus Haut », correspondant à l'hébreu « 'elyown » (Elyon) ; cp. Ps. 7:18.--> recevront le Royaume, et ils posséderont le Royaume éternellement, d'éternité en éternité.
7:19	Alors, je désirai savoir la vérité sur la quatrième bête, qui était différente de toutes les autres, extraordinairement effrayante, qui avait des dents en fer et des ongles en cuivre, qui mangeait, brisait et foulait aux pieds ce qui restait,
7:20	et sur les dix cornes qu'elle avait à la tête, et sur l'autre corne qui était sortie et devant laquelle trois étaient tombées, sur cette corne qui avait des yeux, une bouche proférant de grandes choses, et une plus grande apparence que celle de ses associées.
7:21	Je regardai comment cette corne faisait la guerre aux saints et l'emportait sur eux<!--Ap. 13:2-7.-->,
7:22	jusqu'au moment où l'Ancien des jours vint donner droit aux saints d'Elyown, et que le temps arriva où les saints furent en possession du Royaume.
7:23	Il me parla donc ainsi : La quatrième bête est un quatrième royaume qui sera sur la Terre, différent de tous les royaumes, et qui dévorera toute la Terre, la foulera, et la brisera.

### Règne de l'homme impie et jugement d'Elohîm

7:24	Mais les dix cornes sont dix rois qui s'élèveront de ce royaume. Un autre s'élèvera après eux, il sera différent des premiers, et il abattra trois rois.
7:25	Il proférera des paroles contre le Très-Haut, il harcèlera les saints d'Elyown, et il aura l'intention de changer les temps et la loi. Les saints seront livrés entre ses mains pendant un temps, des temps, et la moitié d'un temps.
7:26	Mais le jugement se tiendra, et on lui ôtera sa domination, en la détruisant et la faisant périr, jusqu'à en voir la fin.
7:27	Afin que le règne, la domination, et la grandeur de tous les royaumes qui sont sous les cieux, soient donnés au peuple des saints d'Elyown. Son Royaume est un royaume éternel, et tous les royaumes lui seront assujettis et lui obéiront.
7:28	Jusqu'ici est la fin de cette parole. Quant à moi, Daniye'l, mes pensées m'effrayèrent beaucoup, et mon visage changea en moi, toutefois je gardai cette parole dans mon cœur.

## Chapitre 8

### Vision du bélier et du bouc

8:1	La troisième année du règne du roi Belshatsar, moi, Daniye'l, j'eus cette vision, en plus de celle que j'avais eue auparavant.
8:2	Je vis cette vision, et il arriva, comme je regardais, que j'étais à Suse, la capitale, dans la province d'Élam, et dans ma vision, je me trouvais près du fleuve d'Oulaï.
8:3	Et je levai mes yeux, je regardai, et voici, un bélier se tenait devant le fleuve, et il avait deux cornes. Les deux cornes étaient hautes, mais l'une était plus haute que l'autre, et la plus haute s'éleva la dernière.
8:4	Je vis ce bélier qui frappait de ses cornes à l'occident, au nord, et au midi. Aucune bête n'était capable de tenir devant lui, et il n'y avait personne qui puisse délivrer de sa puissance. Il agissait selon sa volonté et devint grand.
8:5	Comme je regardais attentivement, voici, un bouc d'entre les chèvres venait de l'occident, et parcourait toute la terre à sa surface, sans la toucher. Ce bouc avait entre les yeux une corne considérable.
8:6	Il arriva jusqu'au bélier qui avait deux cornes et que j'avais vu se tenant devant le fleuve, et il courut sur lui dans la fureur de sa force.
8:7	Je le vis arriver à proximité du bélier et manifester son amertume contre lui : il frappa le bélier et lui brisa les deux cornes, et le bélier n'avait aucune force pour tenir ferme contre lui. Il le jeta par terre et le piétina, et il n'y eut personne pour délivrer le bélier de sa main.
8:8	Alors le bouc d'entre les chèvres grandit extrêmement, mais lorsqu'il fut puissant, sa grande corne se brisa. Quatre grandes cornes s'élevèrent pour la remplacer, aux quatre vents des cieux.

### La petite corne renverse la vérité

8:9	De l'une d'elles sortit une petite corne<!--Antiochos IV Épiphane est le fils d'Antiochos III le Grand, né vers 215 av. J.-C. Il gouverna le royaume séleucide de 175 à 164 av. J.-C., date de sa mort. Ce dernier avait profané le temple de Yeroushalaim (Jérusalem) en sacrifiant des porcs sur l'autel (Voir commentaire en Mt. 24:15). Cette petite corne, qui fait tomber par terre une partie de l'armée des étoiles, agit de même que Satan au ciel qui avait fait chuter un tiers des étoiles, soit des anges (Ap. 12:3-4).-->, qui s'agrandit beaucoup vers le midi, vers l'orient et vers la noblesse<!--Vient d'un mot qui signifie aussi « gloire, honneur, chevreuil, gazelle ».-->.
8:10	Elle s'éleva même jusqu'à l'armée des cieux, elle fit tomber à terre une partie de l'armée et des étoiles, et elle les foula<!--Es. 14:12-15 ; Ez. 28:12-19.-->.
8:11	Et elle s'éleva même jusqu'au chef de l'armée, lui enleva le sacrifice perpétuel, et renversa la demeure de son sanctuaire.
8:12	L'armée fut livrée avec le sacrifice perpétuel, à cause de la transgression. La corne jeta la vérité par terre, elle agit et réussit.
8:13	Alors j'entendis un saint qui parlait et un autre saint disait à celui qui parlait : Jusqu'à quand durera cette vision sur le sacrifice perpétuel et sur la transgression qui cause la désolation ? Jusqu'à quand le sanctuaire et l'armée seront-ils foulés ?
8:14	Et il me dit : 2 300 soirs et matins, puis le sanctuaire sera purifié.

### La vision du bélier et du bouc interprétée

8:15	Quant à moi, Daniye'l, j'avais cette vision et j'en cherchai le discernement, voici, quelqu'un qui avait l'apparence d'un homme se tenait devant moi.
8:16	Et j'entendis la voix d'un être humain au milieu du fleuve Oulaï. Il cria et dit : Gabriy'el, explique-lui la vision !
8:17	Il vint donc près du lieu où je me tenais. Et tandis qu'il venait, je fus terrifié et je tombai sur ma face. Il me dit : Comprends, fils de l'homme, car la vision est pour le temps de la fin.
8:18	Comme il me parlait, je m'endormis profondément, la face contre terre. Il me toucha, et me fit tenir debout à la place où je me trouvais.
8:19	Et il dit : Voici, je vais t'apprendre ce qui arrivera à la fin de la colère, car il y a un temps fixé<!--Ge. 1:14.--> pour la fin.
8:20	Le bélier que tu as vu qui avait deux cornes, ce sont les rois des Mèdes et des Perses.
8:21	Le bouc velu, c'est le roi de Yavan<!--Yavan ou Grèce.-->. La grande corne entre ses yeux, c'est le premier roi.
8:22	Les quatre cornes qui se sont élevées pour remplacer cette corne brisée, ce sont quatre royaumes qui s'élèveront de cette nation, mais qui n'auront pas autant de force.

### Le roi impie, l'adversaire d'Elohîm ; la vision tenue secrète

8:23	À la fin de leur règne, lorsque les pécheurs seront consumés, il se lèvera un roi au visage féroce, comprenant les énigmes.
8:24	Sa puissance s'accroîtra, mais non par sa propre force. Il fera d'incroyables ravages, il réussira dans ses entreprises, et il détruira les puissants et le peuple des saints.
8:25	Par son habileté, il fera prospérer la tromperie dans sa main. Il aura de l'arrogance dans le cœur, et fera périr beaucoup d'hommes qui vivaient dans la paix, et il s'élèvera contre le Prince des princes, mais il sera brisé sans main.
8:26	Quant à la vision des soirs et des matins telle qu'elle a été dite, c'est la vérité. Mais toi, tiens secrète la vision, car elle se rapporte à un temps éloigné.
8:27	Et moi Daniye'l, je me sentis épuisé et je fus malade pendant quelques jours. Puis je me levai, et je m'occupai des affaires du roi. J'étais étonné de la vision, et personne n'en eut connaissance.

## Chapitre 9

### Supplications de Daniye'l à YHWH

9:1	La première année de Darius, fils d'Assuérus, de la race des Mèdes, lequel était établi roi sur le royaume des Chaldéens,
9:2	la première année de son règne, moi Daniye'l, je discernai par les livres, que le nombre des années dont YHWH avait parlé au prophète Yirmeyah<!--Jé. 25:11.--> pour finir les désolations de Yeroushalaim, était de 70 ans.
9:3	Je tournai ma face vers Adonaï Elohîm pour le chercher par la prière et la supplication, avec le jeûne, le sac et la cendre.
9:4	Je priai YHWH, mon Elohîm, et je lui fis ma confession : Ah ! Adonaï, El Gadowl<!--El Grand.--> et redoutable, toi qui gardes ton alliance et qui fais miséricorde à ceux qui t'aiment et qui gardent tes commandements !
9:5	Nous avons péché, nous avons commis l'iniquité, nous avons agi méchamment, nous avons été rebelles, et nous nous sommes détournés de tes commandements et de tes ordonnances.
9:6	Nous n'avons pas écouté tes serviteurs, les prophètes, qui ont parlé en ton Nom à nos rois, à nos chefs, à nos pères, et à tout le peuple du pays.
9:7	Ô Adonaï ! À toi est la justice, et à nous la confusion de face, en ce jour, aux hommes de Yéhouda, aux habitants de Yeroushalaim, et à tout Israël, à ceux qui sont près et à ceux qui sont loin, dans tous les pays où tu les as chassés, à cause des infidélités dont ils se sont rendus coupables envers toi<!--Ps. 106:6 ; La. 3:42 ; Né. 9:30.-->.
9:8	Adonaï, à nous est la confusion de face, à nos rois, à nos chefs, et à nos pères, parce que nous avons péché contre toi.
9:9	Auprès du Seigneur, notre Elohîm, la miséricorde et le pardon, car nous avons été rebelles envers lui.
9:10	Nous n'avons pas écouté la voix de YHWH, notre Elohîm, pour marcher dans sa torah, qu'il avait mise devant nous par le moyen de ses serviteurs, les prophètes.
9:11	Et tout Israël a transgressé ta torah, et s'est détourné pour ne pas écouter ta voix. Alors se sont répandues sur nous l’exécration et la malédiction qui sont écrites dans la torah de Moshè, serviteur d'Elohîm, parce que nous avons péché contre Elohîm<!--Lé. 26:14-39 ; Né. 1:6.-->.
9:12	Il a accompli les paroles qu'il avait prononcées contre nous, et contre nos chefs qui nous ont gouvernés, et il a fait venir sur nous un grand mal, et il n'en est jamais arrivé sous le ciel entier un semblable à celui qui est arrivé à Yeroushalaim.
9:13	Comme cela est écrit dans la torah de Moshè, ce mal est venu sur nous, et nous n'avons pas supplié la face de YHWH, notre Elohîm, pour nous détourner de nos iniquités, et pour nous rendre attentifs à ta vérité.
9:14	YHWH a veillé sur le mal que nous avons fait et il l'a fait venir sur nous parce que YHWH, notre Elohîm, est juste dans toutes les œuvres qu'il a faites, vu que nous n'avons pas obéi à sa voix.
9:15	Or maintenant, Adonaï, notre Elohîm ! Toi qui as fait sortir ton peuple du pays d'Égypte par ta main puissante, et qui t'es acquis un Nom comme il l'est aujourd'hui, nous avons péché, nous avons été méchants.
9:16	Adonaï, je te prie que selon ta justice, que ta colère et ton indignation se détournent de ta ville de Yeroushalaim, de la montagne de ta sainteté ! Car à cause de nos péchés et des iniquités de nos pères, Yeroushalaim et ton peuple sont en opprobre à tous ceux qui nous entourent.
9:17	Maintenant donc, ô notre Elohîm, écoute la prière et les supplications de ton serviteur, et pour l'amour d'Adonaï, fais briller ta face sur ton sanctuaire dévasté !
9:18	Mon Elohîm, prête l'oreille et écoute ! Ouvre tes yeux et regarde nos ruines, et la ville sur laquelle ton Nom est invoqué ! Car ce n'est pas à cause de nos justices que nous présentons nos supplications devant ta face, mais à cause de tes grandes compassions.
9:19	Adonaï, exauce ! Adonaï pardonne ! Adonaï sois attentif et agis ! Ne tarde pas, par amour pour toi, ô mon Elohîm ! Car ton Nom est invoqué sur ta ville, et sur ton peuple.

### Les 70 semaines

9:20	Je parlais encore, je priais, je confessais mon péché, et le péché de mon peuple d'Israël, et je présentais ma supplication à YHWH, mon Elohîm, en faveur de la sainte montagne de mon Elohîm.
9:21	Je parlais encore en prière, quand l'homme Gabriy'el, que j'avais vu auparavant dans une vision, étant épuisé de fatigue, s'approcha de moi au temps de l'offrande du soir.
9:22	Il me fit comprendre, me parla et dit : Daniye'l, je suis venu maintenant pour te donner de la perspicacité et du discernement.
9:23	La parole est sortie dès le commencement de tes supplications, et je suis venu pour te la déclarer, car tu es un bien-aimé. Sois attentif à la parole, et comprends la vision.
9:24	70 semaines<!--Il convient tout d'abord de préciser que cette prophétie concerne en premier lieu Israël en tant que nation. Daniye'l annonce une sorte de compte à rebours sur les événements qui interviendront depuis son époque jusqu'au jugement dernier. Les 70 semaines représentent chacune une année, soit 490 ans. L'ange Gabriy'el a indiqué à Daniye'l que le départ de la 70ème semaine aura lieu lorsque Yeroushalaim (Jérusalem) sera rebâtie (Da. 9:25). Or les Écritures ne citent qu'un seul décret relatif à la reconstruction de Yeroushalaim, à savoir celui du roi Artaxerxès qui, en 445 av. J.-C., au mois de Nisan, permit à Ezra (Esdras) de retourner à Yeroushalaim pour terminer sa reconstruction (Esd. 7:6-10, 9:9 ; Né. 2:5). Cette date est considérée comme le point de départ des 69 semaines qui vont jusqu'au Mashiah (Christ). Daniye'l annonce deux événements principaux qui doivent survenir. D'une part, le retranchement du Mashiah, c'est-à-dire la mort de Yéhoshoua ha Mashiah (Jésus-Christ) à la croix pour expier nos péchés (Da. 9:26) ; et d'autre part, une nouvelle destruction de la ville et de son sanctuaire (Da. 9:26). Cette prophétie s'est accomplie en l'an 70 avec l'entrée des troupes romaines menées par Titus dans Yeroushalaim. Il reste donc une semaine, c'est-à-dire sept années prophétiques que l'ange appelle « un temps, des temps », c'est-à-dire sept ans ; « et la moitié d'un temps », c'est-à-dire trois ans et demi (Da. 7:25). La dernière partie de ces sept années, à savoir trois ans et demi, 42 mois ou encore 1260 jours, correspond à la grande tribulation (Mt. 24:21-29 ; Mc. 13:24 ; Ap. 7:9-14, 11:1-3, 12:14, 13:5). Voir commentaire sur la grande tribulation en Ap. 7:14.--> ont été déterminées sur ton peuple et sur ta ville sainte, pour abolir la transgression et mettre fin aux péchés, pour faire la propitiation pour l'iniquité, pour amener la justice éternelle, pour mettre le sceau à la vision et à la prophétie et pour oindre le Saint des saints.
9:25	Tu sauras donc et tu comprendras que depuis la sortie de la parole pour restaurer et pour rebâtir Yeroushalaim jusqu'au Mashiah, le Chef, il y a 7 semaines et 62 semaines. Les places et les brèches seront rebâties, mais en des temps d'angoisse.
9:26	Après ces 62 semaines, le Mashiah sera retranché et n'aura rien. Le peuple du chef qui viendra détruira la ville et le sanctuaire, et sa fin arrivera comme par une inondation. Il est déterminé que les dévastations dureront jusqu'à la fin de la guerre.
9:27	Et il confirmera l'alliance avec plusieurs pour une semaine, et à la moitié de cette semaine il fera cesser le sacrifice et l'offrande. Puis sur l'aile<!--Le mot hébreu signifie aussi « extrémité, bord, bordure, coin ». Il est peut-être question d'une extrémité du temple.--> des abominations, il y aura celui qui cause la désolation<!--Voir Mt. 24:15 ; Mc. 13:14.--> jusqu'à ce que l'anéantissement décrété se déverse sur l'auteur de cette désolation.

## Chapitre 10

### Daniye'l voit la gloire du Mashiah

10:1	La troisième année de Cyrus, roi de Perse, une parole fut révélée à Daniye'l qui est appelé du nom de Beltshatsar. Cette parole est véritable et annonce une grande guerre. Il fut attentif à cette parole, et il eut le discernement de la vision.
10:2	En ce temps-là, moi Daniye'l, je fus dans les pleurs pendant trois semaines de jours.
10:3	Je ne mangeai aucune nourriture désirable, il n'entra ni viande ni vin dans ma bouche, et je ne m'oignis pas, je ne m'oignis pas jusqu'à ce que ces trois semaines de jours soient accomplies.
10:4	Le vingt-quatrième jour du premier mois, j'étais au bord du grand fleuve qui est Hiddékel.
10:5	Je levai les yeux, et je regardai, et voici, il y avait un homme vêtu de lin, et ayant sur les reins une ceinture d'or fin d'Ouphaz.
10:6	Son corps était comme de chrysolithe, et son visage brillait comme l'éclair, ses yeux étaient comme des flammes de feu, ses bras et ses pieds ressemblaient à du cuivre poli, et le son de sa voix était comme le bruit d'une multitude de gens<!--Ap. 1:12-15.-->.
10:7	Moi, Daniye'l, je vis seul la vision, et les hommes qui étaient avec moi ne la virent pas, mais ils furent saisis d'une grande frayeur et ils s'enfuirent pour se cacher.
10:8	Je restai seul et je vis cette grande vision. Il ne me resta aucune force, ma splendeur se transforma en ruine et je ne conservai aucune force.
10:9	J'entendis le son de ses paroles et, en entendant le son de ses paroles, je m'endormis profondément sur ma face, ayant ma face contre terre.

### Le combat du monde spirituel

10:10	Et voici, une main me toucha et me fit mettre sur mes genoux, et sur les paumes de mes mains.
10:11	Puis il me dit : Daniye'l, homme aimé d'Elohîm, sois attentif aux paroles que je vais te dire, et tiens-toi debout à la place où tu es ! Car je suis maintenant envoyé vers toi. Lorsqu'il m'eut ainsi parlé, je me tins debout en tremblant.
10:12	Et il me dit : Ne crains rien, Daniye'l, car dès le premier jour où tu as appliqué ton cœur à comprendre, et à t'humilier devant ton Elohîm, tes paroles ont été exaucées, et c'est à cause de tes paroles que je viens.
10:13	Mais le chef du royaume de Perse s’est tenu debout devant moi 21 jours. Mais voici, Miyka'el, l’un des premiers chefs, est venu à mon secours et je suis resté là, près des rois de Perse.
10:14	Je viens maintenant pour te faire connaître ce qui doit arriver à ton peuple dans les derniers jours, car la vision s'étend jusqu'à ces jours-là.
10:15	Pendant qu'il m'adressait ces paroles, je mis mon visage contre terre, et je gardai le silence.
10:16	Et voici, quelqu'un qui avait l'apparence des fils de l'homme toucha mes lèvres. J'ouvris la bouche, je parlai, et je dis à celui qui se tenait devant moi : Mon seigneur, à cause de la vision, des angoisses m'ont saisi et je n'ai conservé aucune force.
10:17	Comment le serviteur de mon seigneur pourrait-il parler avec mon seigneur ? Maintenant les forces me manquent, et je n'ai plus de souffle.
10:18	Alors celui qui avait l'apparence humaine me toucha encore, et me fortifia.
10:19	Puis il me dit : Ne crains rien, homme bien-aimé, que la paix soit avec toi ! Fortifie-toi, fortifie-toi ! Et comme il me parlait, je repris des forces, et je dis : Que mon seigneur parle, car tu m'as fortifié.
10:20	Il me dit : Ne sais-tu pas pourquoi je suis venu vers toi ? Maintenant je m'en retournerai pour combattre le chef de Perse et, quand je partirai, voici, le chef de Yavan viendra.
10:21	Mais je veux te faire connaître ce qui est écrit dans le livre de vérité. Et il n'y a personne qui me soutienne contre ceux-là, excepté Miyka'el, votre chef.

## Chapitre 11

### Succession des monarques jusqu'à l'homme impie<!--Da. 11-12.-->

11:1	Et moi, dans la première année de Darius, le Mède, je me tenais auprès de lui pour lui donner force et protection.
11:2	Et maintenant, je vais te faire connaître la vérité : voici, il y aura encore trois rois en Perse. Le quatrième amassera plus de richesses que les autres et, quand il sera puissant par ses richesses, il soulèvera tout le monde contre le royaume de Yavan.
11:3	Mais il s'élèvera un vaillant roi<!--Ce vaillant roi est Alexandre le Grand qui régna de 336 à 323 av. J.-C.-->, qui dominera avec une grande puissance, et fera ce qu'il voudra.
11:4	Mais dès qu'il sera établi, son royaume sera brisé et divisé<!--Le prophète fait ici allusion à la mort d'Alexandre le Grand (356 – 323 av. J.-C.) et au partage de son royaume entre quatre de ses principaux généraux : l'Asie Mineure pour Lysimaque (361 – 281 av. J.-C.), la Grèce et la Macédoine pour Cassandre (358 – 297 av. J.-C.), la Syrie et la Babylonie pour Séleucos Ier Nicator (358 – 281 av. J.-C.), l'Égypte, la Judée et une partie de la Syrie pour Ptolémée Ier Sôter (368 – 283 av. J.-C.).--> aux quatre vents des cieux, mais non pas pour sa postérité et non pas selon sa domination avec laquelle il avait gouverné, car son royaume sera déraciné et il passera à d'autres qu'à eux.
11:5	Le roi du midi<!--Le roi du midi est Ptolémée Ier Sôter (règne : 305 – 283 av. J.-C.), le chef plus fort que lui est Séleucos Ier Nicator (règne : 305 – 281 av. J.-C.).--> deviendra fort et puissant. Mais un de ses chefs sera plus puissant que lui et dominera, sa domination sera une grande domination.
11:6	Au bout de quelques années, ils feront alliance ensemble et la fille du roi du midi viendra vers le roi du nord pour redresser les affaires. Mais elle ne conservera pas la force du bras, et lui ne tiendra pas, ni son bras. Elle sera livrée avec ceux qui l'auront amenée, avec son père et avec celui qui aura été son soutien en ce temps-là.
11:7	Mais un rejeton de ses racines s'élèvera pour le remplacer<!--Ptolémée III Évergète (règne : 246 – 222 av. J.-C.).-->. Il viendra vers l'armée et entrera dans les forteresses du roi du nord. Il agira contre eux et il sera puissant.
11:8	Et même il emmènera captifs en Égypte leurs Elohîm, avec leurs images en métal fondu et avec leurs vases précieux d'argent et d'or. Puis il restera quelques années de plus que le roi du nord.
11:9	Et le roi du midi entrera dans son royaume, mais il s'en retournera dans son pays.
11:10	Ses fils<!--Ses fils sont les deux rois de Syrie : Séleucos III Sôter Keraunos (Ceraunus) (règne : 226 – 223 av. J.-C.) et Antiochos III le Grand (règne : 223 – 187 av. J.-C.).--> entreront en guerre et rassembleront une multitude nombreuse de troupes. Il viendra, il viendra et se répandra comme un torrent qui déborde, puis il reviendra et poussera la guerre jusqu'à la forteresse du roi du midi.
11:11	Et le roi du midi sera irrité, il sortira et combattra contre lui, contre le roi du nord. Il soulèvera une grande multitude, et les troupes du roi du nord seront livrées entre les mains du roi du midi.
11:12	Et après avoir défait cette multitude, le cœur du roi s'élèvera. Il s'élèvera et fera tomber des milliers, mais il ne triomphera pas.
11:13	Car le roi du nord reviendra et rassemblera une plus grande multitude que la première. Au bout de quelque temps, de quelques années, il viendra, il viendra avec une grande armée et de grandes richesses.
11:14	Et en ce temps-là, beaucoup s'élèveront contre le roi du midi, et des hommes violents parmi ton peuple se révolteront pour accomplir la vision, mais ils succomberont.
11:15	Le roi<!--Le roi du nord est Séleucos IV Philopator (règne : 187 – 175 av. J.-C.).--> du nord viendra, il élèvera des tertres et prendra les villes fortes. Les bras du midi et l'élite du roi ne résisteront pas, ils manqueront de force pour résister.
11:16	Celui qui marchera contre lui agira selon sa volonté et personne ne tiendra devant lui. Il s'arrêtera dans le pays de noblesse, exterminant ce qui tombera sous sa main.
11:17	Il tournera sa face pour entrer avec la force de tout son royaume et agira avec droiture avec lui<!--Le roi du midi.-->. Il lui donnera sa fille pour femme afin de le détruire, mais cela ne tiendra pas et ne sera pas à lui.
11:18	Il tournera sa face vers les îles, et il en prendra beaucoup, mais un chef mettra fin à l'opprobre qu'il voulait lui attirer, et il fera retomber sur lui son opprobre.
11:19	Il tournera sa face vers les forteresses de son pays où il chancellera et tombera, et on ne le trouvera plus.
11:20	Et un autre sera établi à sa place, qui fera passer un exacteur dans l'ornement du royaume, et en peu de jours il sera brisé, et ce ne sera ni par la colère ni par la guerre.
11:21	Et à sa place il en sera établi un autre qui sera méprisé, auquel on ne donnera pas l'honneur royal. Mais il viendra en paix, et il s'emparera du royaume par des flatteries.
11:22	Les troupes qui se répandront comme un torrent seront submergées devant lui, et brisées, de même qu'un chef de l'alliance.
11:23	Depuis l’alliance qu’on aura faite avec lui, il usera de tromperie, il montera et sera puissant par le moyen d’une petite nation.
11:24	Il entrera tranquillement dans les lieux les plus riches de la province, et il fera ce que n'avaient pas fait ses pères, ni les pères de ses pères. Il distribuera le butin, le pillage et les richesses. Et il formera des desseins contre les places fortes, et cela jusqu'à un certain temps.
11:25	Il excitera sa force et son cœur contre le roi du midi avec une grande armée. Et le roi du midi s'avancera en bataille avec une très grande et très forte armée, mais il ne résistera pas, car on formera des complots contre lui.
11:26	Ceux qui mangent les mets de sa table le briseront. Son armée se répandra comme un torrent et beaucoup de gens tomberont blessés mortellement.
11:27	Les deux rois chercheront dans leur cœur à se faire du mal et, à la même table, ils parleront avec fausseté. Mais cela ne réussira pas, car la fin ne viendra qu'au temps fixé.
11:28	Il retournera dans son pays avec de grandes richesses. Son cœur sera contre la sainte alliance, il agira contre elle, puis retournera dans son pays.
11:29	Ensuite il retournera au temps fixé, et il viendra contre le midi, mais cette fois, ce ne sera pas comme la première fois,
11:30	car les navires de Kittim viendront contre lui et il sera découragé. Il reviendra et exprimera de l'indignation contre la sainte alliance, il agira contre elle. Il reviendra et considèrera ceux qui abandonnent la sainte alliance.
11:31	Des forces se tiendront là de sa part et profaneront le sanctuaire, la forteresse. Elles aboliront le sacrifice perpétuel et dresseront l'abomination qui cause la désolation.
11:32	Et il corrompra par des flatteries ceux qui agissent méchamment à l'égard de l'alliance. Mais ceux du peuple qui connaîtront leur Elohîm agiront avec courage.
11:33	Et les plus intelligents parmi le peuple donneront instruction à beaucoup. Il en est qui succomberont pour un temps à l'épée et à la flamme, à la captivité et au pillage.
11:34	Dans le temps où ils succomberont, ils seront secourus avec un peu de secours, et beaucoup se joindront à eux par hypocrisie.
11:35	Et quelques-uns des hommes intelligents succomberont, afin qu'ils soient épurés, purifiés et blanchis, jusqu'au temps de la fin, car elle n'arrivera qu'au temps fixé.
11:36	Le roi fera ce qu'il voudra, il s'élèvera, il se glorifiera au-dessus de tout El. Il proférera des choses étranges contre le El<!--Es. 9:5.--> des Elohîm, il prospérera jusqu'à ce que la colère soit consommée, car ce qui est décrété sera exécuté.
11:37	Il n'aura égard ni aux elohîm de ses pères, ni au désir des femmes. Il n'aura égard à aucun Éloah, car il s'élèvera au-dessus de tout.
11:38	Mais, à la place, il honorera l'éloah des forteresses<!--Vient de l'hébreu « mâouzzim » généralement traduit par « Mahuzzim » qui signifie « lieu ou moyens de sécurité, protection, refuge, forteresse ».-->. Cet éloah que ses pères n'ont pas connu, il lui rendra des hommages avec de l'or et de l'argent, avec des pierres précieuses et des objets précieux.
11:39	C'est avec l'éloah étranger qu'il agira contre les endroits fortifiés, et ceux qui le reconnaîtront, ceux qui le reconnaîtront il les comblera de gloire, il les fera dominer sur beaucoup et leur distribuera des terres en récompense.
11:40	Au temps de la fin, le roi du midi se heurtera contre lui avec ses cornes. Et le roi du nord fondra sur lui comme une tempête, avec des chars et des cavaliers, et avec de nombreux navires. Il s'avancera dans les terres, se répandra comme un torrent et débordera.
11:41	Et il entrera dans le pays de noblesse, et beaucoup de pays succomberont, mais Édom, Moab et les principaux des enfants d'Ammon échapperont de sa main.
11:42	Il étendra sa main sur ces pays-là, et le pays d'Égypte n'échappera pas.
11:43	Il se rendra maître des trésors d'or et d'argent, et de toutes les choses précieuses de l'Égypte. Les Libyens et les Éthiopiens seront à sa suite.
11:44	Mais des nouvelles de l'orient et du nord viendront le troubler, et il partira avec une grande fureur, pour détruire et exterminer un grand nombre.
11:45	Et il dressera les tentes de son palais entre les mers, vers la glorieuse et sainte montagne. Puis il arrivera à la fin, et personne ne lui donnera du secours.

## Chapitre 12

### La résurrection pour le jugement éternel

12:1	Or en ce temps-là Miyka'el, ce grand chef qui tient ferme pour les enfants de ton peuple, tiendra ferme. Ce sera un temps de détresse, tel qu'il n'y en a pas eu de semblable depuis que les nations existent jusqu'à ce temps-là. En ce temps-là, ceux de ton peuple qui seront trouvés inscrits dans le livre seront sauvés.

### Les deux résurrections

12:2	Et beaucoup de ceux qui dorment dans la poussière de la Terre se réveilleront<!--Il est question ici de la résurrection. Tout d'abord, il y aura la résurrection des morts en Mashiah (Christ), lors du retour de Yéhoshoua ha Mashiah (Jésus-Christ) pour enlever l'Assemblée (Église) (1 Th. 4:13-17). Ensuite, il y aura celle de tous les saints ayant vécu avant Yéhoshoua ha Mashiah, et de toutes les personnes converties au Mashiah et mises à mort pendant la grande tribulation (Ap. 19-20). Enfin, la dernière résurrection interviendra à l'issue du millénium. Il s'agit de la résurrection des impies de tous les temps (Ap. 20:11-15). Voir également Jn. 5:24-29, 11:25.-->, les uns pour la vie éternelle, les autres pour la honte, pour l'aversion éternelle.
12:3	Ceux qui auront été prudents brilleront comme la splendeur du firmament, et ceux qui auront rendu justes beaucoup de gens, comme les étoiles, pour toujours et à perpétuité<!--Mt. 13:43.-->.

### Dernières paroles de YHWH à Daniye'l ; le livre scellé jusqu'au temps de la fin

12:4	Mais toi, Daniye'l, tiens secrètes ces paroles et scelle le livre jusqu'au temps de la fin. Beaucoup courront çà et là, et la connaissance augmentera<!--Ap. 5:2, 10:4.-->.
12:5	Et moi, Daniye'l, je regardai, et voici, deux autres hommes se tenaient debout, l'un en deçà du bord du fleuve, et l'autre au-delà du bord du fleuve.
12:6	Et l'un d'eux dit à l'homme vêtu de lin, qui se tenait au-dessus des eaux du fleuve : Quand sera la fin de ces merveilles ?
12:7	Et j'entendis l'homme vêtu de lin, qui se tenait au-dessus des eaux du fleuve. Il leva sa main droite et sa main gauche vers les cieux, et il jura par celui qui vit éternellement que ce sera dans un temps, des temps, et la moitié d'un temps, et que toutes ces choses s'accompliront quand la force du peuple saint sera entièrement brisée.
12:8	J'entendis, mais je ne compris pas, et je dis : Mon seigneur, quelle sera l'issue de ces choses ?
12:9	Il répondit : Va, Daniye'l, car ces paroles seront tenues secrètes et scellées jusqu'au temps de la fin.
12:10	Beaucoup seront purifiés, blanchis et éprouvés. Mais les méchants agiront avec méchanceté, et aucun des méchants ne comprendra, mais ceux qui seront prudents comprendront.
12:11	Or depuis le temps où cessera le sacrifice perpétuel et où sera dressée l'abomination de la désolation, il y aura 1 290 jours<!--Mt. 24:15 ; Mc. 13:14 ; Lu. 21:20.-->.
12:12	Béni est celui qui attendra et qui parviendra jusqu'à 1 335 jours !
12:13	Et toi, marche jusqu'à ta fin ! Néanmoins tu te reposeras et tu te tiendras debout pour ton héritage à la fin des jours.
