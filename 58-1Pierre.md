# 1 Petros (1 Pierre) (1 Pi.)

Signification : Roc, pierre

Auteur : Petros (Pierre)

Thème : La victoire sur la souffrance

Date de rédaction : Env. 65 ap. J.-C.

Cette lettre semble avoir été écrite à Rome, même si Petros y parlait de « Babel ». En ces temps de persécutions, les chrétiens devaient être prudents quant à la manière dont ils parlaient du pouvoir en place, c'est pourquoi ils utilisaient souvent des codes. C'est donc durant une période difficile que fut rédigée cette épître qui s'adressait à des assemblées d'Asie Mineure dont la plupart furent fondées par Paulos (Paul). À travers ces quelques lignes, Petros (Pierre) exhorte les frères et sœurs à tenir ferme dans la foi malgré les souffrances liées aux épreuves, et les encourage à espérer en Yéhoshoua ha Mashiah (Jésus-Christ), leur salut. Il finit cette épître en donnant des conseils quant à l'attitude à avoir au sein de l'assemblée.

## Chapitre 1

### Introduction

1:1	Petros, apôtre de Yéhoshoua Mashiah, aux étrangers de la diaspora du Pont<!--Le Pont : province formant presque la totalité de l'Asie Mineure.-->, de la Galatie, de la Cappadoce, de l'Asie et de la Bithynie,
1:2	élus selon la prescience d'Elohîm le Père, par la sanctification de l'Esprit afin d'obéir à Yéhoshoua Mashiah, et qui participent à l'aspersion de son sang : que la grâce et la paix vous soient multipliées !

### Les souffrances du chrétien et sa conduite pour un salut parfait

1:3	Béni soit l'Elohîm et Père de notre Seigneur Yéhoshoua le Mashiah, qui par sa grande miséricorde, nous a fait naître de nouveau<!--Le mot grec « anagennao » signifie « produire de nouveau, être né de nouveau » ou « renouvelé ».--> pour une espérance vivante, par le moyen de la résurrection de Yéhoshoua Mashiah d'entre les morts,
1:4	pour un héritage incorruptible, sans souillure, qui ne peut se faner, et qui est réservé dans les cieux pour nous,
1:5	qui, dans la puissance d'Elohîm, sommes gardés par le moyen de la foi, pour le salut qui est prêt à être révélé dans le dernier temps !
1:6	En cela vous vous réjouissez, quoique vous soyez maintenant affligés pour un peu de temps par diverses épreuves, vu que cela est convenable,
1:7	afin que l'épreuve de votre foi, beaucoup plus précieuse que l'or qui périt, mais qu'on éprouve au moyen du feu, soit trouvée un sujet de louange, d'honneur et de gloire, lors de la révélation de Yéhoshoua Mashiah.
1:8	Lequel vous aimez sans l'avoir vu ; en qui, sans le voir maintenant, mais croyant, vous vous réjouissez d'une joie inexprimable et glorieuse,
1:9	obtenant le salut de vos âmes qui est le but<!--Vient du grec « telos » qui signifie « la fin », « ce par quoi se termine une chose ».--> de votre foi.
1:10	C'est au sujet de ce salut que les prophètes, qui ont prophétisé concernant la grâce qui est en vous ont fait leurs recherches et leurs investigations.
1:11	Cherchant à découvrir à quel temps et à quelles circonstances se rapportaient les indications données par l'Esprit du Mashiah qui était en eux et qui, d'avance, attestait les souffrances du Mashiah et la gloire dont elles seraient suivies.
1:12	Mais il leur fut révélé que ce n'était pas pour eux-mêmes, mais pour nous, qu'ils administraient ces choses qui maintenant vous ont été annoncées par le moyen de ceux qui vous ont prêché l'Évangile par le Saint-Esprit envoyé du ciel, et dans lesquelles les anges désirent plonger leurs regards.
1:13	C'est pourquoi, ceignez les reins de votre entendement, soyez sobres et ayez une entière espérance dans la grâce qui vous est apportée, jusqu'à la révélation<!--Voir commentaire en 2 Th. 1:7.--> de Yéhoshoua Mashiah.
1:14	Comme des enfants obéissants, ne vous conformez pas à vos convoitises d'autrefois, pendant votre ignorance.
1:15	Mais, comme celui qui vous a appelés est saint, vous aussi de même soyez saints dans toute votre conduite,
1:16	parce qu'il est écrit : Soyez saints car je suis saint<!--Lé. 11:44.-->.
1:17	Et si vous invoquez le Père<!--Expression qui trouve son explication dans le fait que les prières adressées à Elohîm commencent en général par une invocation du nom divin. Voir Joë. 2:32 ; Ac. 15:17, 22:16 ; Ro. 10:12-14 ; 1 Co. 1:2 ; 2 Ti. 2:22.--> qui juge de façon impartiale selon l'œuvre de chacun, conduisez-vous avec crainte pendant le temps de votre séjour sur la Terre,
1:18	sachant que ce n'est pas par des choses corruptibles, comme l'argent ou l'or que vous avez été rachetés de votre vaine manière de vivre que vos ancêtres vous avaient transmise,
1:19	mais par le sang précieux du Mashiah, comme d'un agneau sans défaut et sans tache,
1:20	prédestiné en effet avant la fondation du monde, et manifesté dans les derniers temps à cause de vous.
1:21	Par son moyen vous croyez en Elohîm qui l'a ressuscité des morts et lui a donné la gloire, afin que votre foi et votre espérance reposent sur Elohîm.
1:22	Ayant purifié vos âmes en obéissant à la vérité par le Saint-Esprit, afin que vous ayez un amour fraternel qui soit sans hypocrisie, aimez-vous ardemment les uns les autres d'un cœur pur,
1:23	puisque vous êtes nés de nouveau<!--Voir 1 Pi. 1:3.--> par une semence non pas corruptible mais incorruptible, par le moyen de la parole d'Elohîm, laquelle vit et demeure éternellement.
1:24	Car toute chair est comme l'herbe, et toute la gloire de l'être humain comme la fleur de l'herbe. L'herbe sèche et sa fleur tombe,
1:25	mais la parole du Seigneur demeure éternellement<!--Es. 40:6-8.-->. Et cette parole est celle qui vous a été annoncée par l'Évangile.

## Chapitre 2

2:1	Ayant donc mis de côté<!--Voir Hé. 12:1.--> toute méchanceté, et toute espèce de tromperie, et d'hypocrisie, et d'envie et toute diffamation,
2:2	désirez ardemment, comme des enfants nouveau-nés, le lait spirituel et pur, afin que vous croissiez par lui,
2:3	si en effet vous avez goûté combien le Seigneur est bon<!--Petros (Pierre), l'apôtre, se réfère ici au Ps. 34:9 : « Goûtez et voyez combien YHWH est bon ! ». On peut noter que le nom propre YHWH a été remplacé par le nom commun « Seigneur ». Voir commentaire en Lu. 4:18-19.-->.
2:4	Vous approchant de lui, Pierre vivante, rejetée en effet par les humains, mais choisie et précieuse devant Elohîm.
2:5	Vous aussi, comme des pierres vivantes, vous êtes édifiés, maison spirituelle, sainte prêtrise, afin d'offrir des sacrifices spirituels, agréables à Elohîm par le moyen de Yéhoshoua Mashiah.
2:6	C'est pourquoi aussi, il est dit dans l'Écriture : Voici, je mets en Sion la principale pierre<!--Yéhoshoua Mashiah (Jésus-Christ) est la pierre rejetée par les bâtisseurs. Voir Es. 28:16 ; Ps. 118:22.--> de l'angle, choisie et précieuse. Et celui qui croit en elle ne sera pas confus.
2:7	C'est donc pour vous les croyants qu'elle a ce prix. Mais pour les rebelles, il est dit<!--Ps. 118:22.--> : La pierre que ceux qui bâtissaient ont rejetée est devenue la principale de l'angle,
2:8	et une pierre d'achoppement, et un rocher de scandale. Ils se heurtent contre la parole et sont rebelles, et c'est à cela qu'ils sont destinés.

### La position du croyant

2:9	Mais vous, vous êtes la race élue, la prêtrise royale, la nation sainte, le peuple acquis, afin que vous annonciez les vertus de celui qui vous a appelés des ténèbres à sa merveilleuse lumière.
2:10	Vous qui autrefois n'étiez pas son peuple, mais qui maintenant êtes le peuple d'Elohîm. Vous qui n'aviez pas obtenu miséricorde, mais qui maintenant avez obtenu miséricorde.
2:11	Mes bien-aimés, je vous exhorte, comme des étrangers et des voyageurs, à vous abstenir des désirs<!--Voir Ga. 5:16.--> charnels qui font la guerre à l'âme.
2:12	Ayant une bonne conduite parmi les nations, afin que là même où ils vous calomnient comme si vous étiez des malfaiteurs, ils remarquent vos bonnes œuvres et glorifient Elohîm au jour de sa visite<!--« Investigation, inspection, visite d'inspection ». Cet acte par lequel Elohîm visite les êtres humains, observe leurs voies, leurs caractères, pour leur accorder en partage joie ou tristesse.-->.
2:13	Soyez donc soumis à toute institution humaine, pour l'amour d'Elohîm : soit au roi, comme étant au-dessus des autres,
2:14	soit aux gouverneurs, parce qu'ils sont envoyés par lui pour punir en effet les malfaiteurs et pour approuver ceux qui font le bien.
2:15	Car c'est la volonté d'Elohîm qu'en faisant le bien vous museliez la bouche<!--Du grec « phimoo » : « fermer la bouche par une muselière ».--> à l'ignorance des gens insensés.
2:16	Comme libres, et ne considérant pas la liberté comme une couverture de la malice, mais agissant comme des esclaves d'Elohîm.
2:17	Honorez tout le monde, aimez tous vos frères, craignez Elohîm, honorez le roi.
2:18	Domestiques, soyez soumis en toute crainte à vos maîtres, non seulement à ceux qui sont bons et équitables, mais aussi à ceux qui sont tordus.
2:19	Car c'est une chose agréable à Elohîm si quelqu'un, à cause de la conscience qu'il a envers Elohîm, endure des afflictions en souffrant injustement.
2:20	Car quelle gloire y a-t-il à supporter patiemment des coups pour avoir commis des péchés ? Mais si vous souffrez quand vous faites le bien et que vous le supportez patiemment, c'est une grâce devant Elohîm.

### Les souffrances du Mashiah

2:21	Or, c'est à cela que vous avez été appelés, car Mashiah aussi a souffert pour nous, nous laissant un modèle, afin que vous suiviez ses traces :
2:22	lui qui n'a pas commis de péché et dans la bouche duquel il ne s'est pas trouvé de tromperie,
2:23	lui qui, insulté, n'insultait pas en retour<!--Riposter à des railleries.-->, maltraité, ne menaçait pas mais s'en remettait à celui qui juge justement,
2:24	lui qui a lui-même porté nos péchés dans son corps sur le bois, afin qu'étant morts au péché, nous vivions pour la justice. Lui dont la meurtrissure<!--Es. 53:5.--> vous a guéris.
2:25	Car vous étiez comme des brebis égarées, mais maintenant vous êtes retournés vers le Berger et le Surveillant de vos âmes.

## Chapitre 3

### La conduite du chrétien dans le mariage

3:1	Femmes, soyez de même soumises à vos maris, afin que si quelques-uns sont rebelles à la parole, ils soient gagnés sans paroles par le moyen de la conduite de leurs femmes,
3:2	lorsqu'ils auront remarqué votre conduite pure, dans la crainte.
3:3	Que votre parure ne soit pas celle de l'extérieur, qui consiste dans les cheveux tressés, les ornements d'or ou la magnificence des habits,
3:4	mais que votre parure consiste dans l'être caché dans le cœur, l'incorruptibilité d'un esprit doux et paisible, qui est d'un grand prix devant Elohîm.
3:5	Car c'est ainsi que se paraient aussi autrefois les saintes femmes qui espéraient en Elohîm, étant soumises à leurs maris,
3:6	comme Sarah, qui obéissait à Abraham et l'appelait son seigneur. C'est d'elle que vous êtes devenues les filles, en faisant ce qui est bien et sans vous laisser troubler par aucune crainte.
3:7	Vous de même, maris, montrez de la compréhension dans vos relations avec vos femmes, comme avec un vase<!--Petros (Pierre) utilise une métaphore connue des Grecs pour parler du corps : le vase.--> plus fragile, c'est-à-dire féminin ; les traitant avec honneur comme étant aussi ensemble héritiers de la grâce de la vie, afin que vos prières ne soient pas interrompues.
3:8	Enfin, soyez tous d'un seul esprit, sensibles aux autres, aimez-vous<!--Vient de philadelphos qui signifie « aimer quelqu'un comme un frère, une sœur ».--> comme des frères et des sœurs, compatissants et amicaux.
3:9	Ne rendez pas le mal pour le mal, ou l'injure pour l'injure<!--Mt. 5:44.-->, mais au contraire, bénissez. Sachant que c'est à cela que vous êtes appelés, afin d'hériter la bénédiction.
3:10	Car celui qui veut aimer la vie et voir des jours heureux, qu’il préserve sa langue du mal et ses lèvres de prononcer la tromperie.
3:11	Qu'il se détourne du mal et fasse ce qui est bon, qu'il recherche la paix et la poursuive,
3:12	parce que les yeux<!--Voir Ps. 33:18, 34:16.--> du Seigneur sont sur les justes, et ses oreilles sont attentives à leur supplication, mais la face du Seigneur est contre ceux qui font le mal.

### Souffrir en faisant le bien

3:13	Et qui vous maltraitera, si vous êtes les imitateurs de celui qui est bon ?
3:14	Mais si vous souffrez aussi à cause de la justice, vous êtes bénis. Ne craignez pas ce qu'ils craignent et ne soyez pas troublés.
3:15	Mais sanctifiez le Seigneur Elohîm<!--Voir Es. 8:12-13.--> dans vos cœurs, et soyez toujours prêts à répondre avec douceur et crainte, à quiconque vous demande une parole concernant l'espérance qui est en vous,
3:16	ayant une bonne conscience, afin que là même où ils diffament votre bonne conduite en Mashiah, ils soient pris de honte de ce qu'ils vous accusent faussement comme des malfaiteurs.
3:17	Car il vaut mieux, si telle est la volonté d'Elohîm, que vous souffriez en faisant le bien qu'en faisant le mal.

### Les souffrances du Mashiah

3:18	Parce que Mashiah aussi a souffert une fois pour les péchés, lui juste pour les injustes, afin de nous amener à Elohîm, ayant été en effet mis à mort selon la chair, mais il a été ramené à la vie par l'Esprit.
3:19	C'est aussi en lui qu'il est allé prêcher aux esprits en prison<!--La possibilité du salut après la mort n'a aucun fondement biblique (Hé. 9:27). Dans ce passage, il est fait mention des pécheurs qui ont vécu du temps de Noah et auxquels la parole d'Elohîm avait été annoncée. Voir aussi commentaire en Mt. 16:18.-->,
3:20	et qui furent rebelles autrefois, quand la patience d'Elohîm les attendait pour la dernière fois durant les jours de Noah, tandis que l'arche se préparait dans laquelle un petit nombre d'âmes, c'est-à-dire huit, furent préservées du danger à travers l'eau,
3:21	figure à laquelle correspond le baptême, qui n'est pas la purification des souillures de la chair, mais la demande faite à Elohîm d'une bonne conscience, et qui maintenant nous sauve par le moyen de la résurrection de Yéhoshoua Mashiah,
3:22	qui étant allé au ciel, est à la droite d'Elohîm, et à qui sont assujettis les anges, les dominations et les puissances.

## Chapitre 4

### Souffrir dans la chair

4:1	Ainsi donc, Mashiah ayant souffert pour nous dans la chair, vous aussi armez-vous de la même pensée. Car celui qui a souffert dans la chair a cessé de pécher,
4:2	afin de vivre, non plus selon les désirs des humains, mais selon la volonté d'Elohîm, pendant le temps qui lui reste à vivre dans la chair.
4:3	C'est déjà bien suffisant d'avoir accompli la volonté des nations, pendant le temps de notre vie passée, quand nous nous abandonnions à la luxure sans bride, aux convoitises, à l'ivrognerie, aux orgies, aux beuveries et aux idolâtries criminelles.
4:4	À ce propos, ils trouvent étrange que vous ne couriez plus avec eux dans le même débordement de libertinage, et ils vous calomnient.
4:5	Mais ils rendront compte à celui qui est prêt à juger les vivants et les morts.
4:6	Car c'est pour cela que l'Évangile a été aussi prêché aux morts, afin qu'ils soient en effet jugés selon les humains dans la chair, et qu'ils vivent selon Elohîm dans l'Esprit.

### L'exercice des dons de l'Esprit

4:7	Or la fin de toutes choses est proche : soyez donc modérés et sobres pour la prière.
4:8	Mais avant toutes choses, ayez les uns pour les autres un amour constant, parce que l'amour couvrira une multitude de péchés<!--Voir Pr. 10:12, 17:9 ; Ja. 5:20.-->.
4:9	Soyez hospitaliers les uns envers les autres, sans murmures.
4:10	Que chacun de vous rende service aux autres selon le don de grâce qu'il a reçu, comme de bons gestionnaires des diverses grâces d'Elohîm.
4:11	Si quelqu'un parle, qu’il le fasse comme oracles d'Elohîm. Si quelqu’un sert, que ce soit comme par la force qu'Elohîm fournit, afin qu'en toutes choses Elohîm soit glorifié par le moyen de Yéhoshoua Mashiah, auquel sont la gloire et la force, d'âge en âge. Amen !

### Se réjouir dans la souffrance

4:12	Mes bien-aimés, ne trouvez pas étrange quand vous êtes dans le feu de la tentation, comme s'il vous arrivait quelque chose de nouveau.
4:13	Mais réjouissez-vous de ce que vous participez aux souffrances du Mashiah, afin que lors de la révélation de sa gloire, vous vous réjouissiez avec allégresse.
4:14	Si l'on vous dit des injures pour le Nom du Mashiah, vous êtes bénis, parce que l'Esprit de gloire et d'Elohîm repose sur vous. Il est en effet blasphémé par eux, mais il est glorifié par vous.
4:15	En effet, qu'aucun de vous ne souffre comme meurtrier, ou voleur, ou malfaiteur ou comme se mêlant des affaires d'autrui,
4:16	mais si quelqu'un souffre comme chrétien, qu'il n'en ait pas honte, mais qu'il glorifie Elohîm en cela.
4:17	Parce que c'est le temps où le jugement va commencer par la maison d'Elohîm<!--Le jugement commence par la maison d'Elohîm. Ez. 9:1-11.-->. Mais si c'est d'abord par nous, quelle sera la fin de ceux qui sont rebelles à l'Évangile d'Elohîm ?
4:18	Et si le juste est difficilement sauvé, que deviendront l'impie et le pécheur ?
4:19	Que ceux donc aussi qui souffrent selon la volonté d'Elohîm, puisqu'ils font ce qui est bon, lui remettent leurs âmes, comme au fidèle Créateur.

## Chapitre 5

### Recommandations

5:1	J'exhorte les anciens qui sont parmi vous, moi qui suis ancien avec eux, témoin des souffrances du Mashiah et participant de la gloire qui doit être révélée :
5:2	faites paître le troupeau d'Elohîm qui est avec vous, en veillant sur lui non par contrainte, mais volontairement, non par empressement pour un gain, mais de bon cœur,
5:3	non pas en dominant comme des maîtres sur ceux qui sont l'héritage d'Elohîm, mais en étant des exemples pour le troupeau.
5:4	Et quand le Berger<!--Yéhoshoua (Jésus) est notre Souverain Pasteur. Voir Ps. 23 ; Jn. 10.--> en chef apparaîtra, vous obtiendrez la couronne incorruptible<!--« Imputrescible », « couronne composée d'amarante, fleur qui ne se fane jamais, symbole de perpétuité et d'immortalité ».--> de gloire.
5:5	De même, vous jeunes gens, soyez soumis aux anciens. Et vous soumettant tous les uns aux autres<!--Ep. 5:21.-->, revêtez-vous<!--Le verbe grec « egkomboomai » signifie « lien ou bande par laquelle deux choses sont liées ensemble, se revêtir ou se ceindre ». À l'époque, les esclaves avaient une écharpe blanche attachée à la ceinture afin de les distinguer des hommes libres. Dans ce passage, Petros (Pierre) fait allusion à cet usage, pour montrer la soumission réciproque des chrétiens. C'est aussi une référence au très humble vêtement, espèce de tablier, que les esclaves devaient porter pour ne pas se salir pendant leur travail.--> d'humilité, parce qu'Elohîm résiste aux orgueilleux, mais il fait grâce aux humbles.
5:6	Humiliez-vous donc sous la puissante main d'Elohîm, afin qu'il vous élève quand le temps sera venu.
5:7	Remettez-lui tout ce qui peut vous inquiéter, car il prend soin de vous.
5:8	Soyez sobres et veillez car le diable, votre adversaire, marche comme un lion rugissant, cherchant qui il dévorera.
5:9	Résistez-lui donc en demeurant fermes dans la foi, sachant que les mêmes souffrances sont imposées à vos frères qui sont dans le monde.

### Salutations

5:10	Et que l'Elohîm de toute grâce, qui nous a appelés à sa gloire éternelle en Yéhoshoua Mashiah, après que vous aurez souffert un peu de temps, vous rende parfaits, vous affermisse, vous fortifie et vous établisse !
5:11	À lui soient la gloire et la force, d'âge en âge ! Amen !
5:12	Je vous ai écrit brièvement par Silvanos, que je considère comme un frère fidèle, pour vous exhorter et pour témoigner que c'est bien dans la vraie grâce d'Elohîm que vous êtes établis.
5:13	Celle qui est à Babel<!--Le nom Babel signifie « la porte de El » ou « confusion par le mélange ». Généralement traduit par Babylone.-->, élue avec vous, vous salue, ainsi que Markos mon fils.
5:14	Saluez-vous les uns les autres par un baiser d'amour. Que la paix soit avec vous tous qui êtes en Mashiah Yéhoshoua ! Amen !
