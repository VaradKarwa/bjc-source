# Nahoum (Nahum) (Na.)

Signification : Consolation, qui a compassion

Auteur : Nahoum

Thème : La ruine de Ninive

Date de rédaction : 7ème siècle av. J.-C.

Nahoum d'Elkosh, contemporain d'Habaquq, exerça son service dans le royaume de Yéhouda (Juda). Il fut chargé d'annoncer la chute de Ninive (capitale de l'Empire d'Assyrie) qui s'était repentie quelques décennies plus tôt suite à la prédication de Yonah (Jonas). Mais elle multiplia de nouveau ses actes d'injustice et de violences au point de terroriser tous les peuples des alentours.

## Chapitre 1

1:1	Prophétie sur Ninive<!--Ninive était la capitale de l'ancien empire assyrien. Voir Jon. 1:1-2.-->. Le livre de la vision de Nahoum d'Elkosh.

### Jugement annoncé sur Ninive

1:2	YHWH est un El jaloux, il se venge, YHWH se venge, il est plein de fureur. YHWH se venge de ses adversaires et il garde sa colère contre ses ennemis.
1:3	YHWH est lent à la colère et grand par sa force, mais il ne laisse pas impuni, il ne laisse pas impuni. YHWH marche parmi les tourbillons et les tempêtes, les nuées sont la poussière de ses pieds<!--Es. 19:1 ; Ez. 30:3 ; Ap. 1:7.-->.
1:4	Il réprimande la mer et la fait tarir, il dessèche tous les fleuves. Le Bashân et le Carmel languissent, la fleur du Liban languit.
1:5	Les montagnes tremblent à cause de lui et les collines se fondent. La Terre se soulève devant sa présence et tous ceux qui y habitent.
1:6	Qui subsistera devant son indignation ? Et qui peut résister à l'ardeur de sa colère ? Sa fureur se répand comme un feu, et les rochers se brisent devant lui.
1:7	YHWH est bon, il est une forteresse au jour de la détresse, et il connaît ceux qui se confient en lui.
1:8	Il s'en va passer comme un débordement d'eaux, il fera une entière destruction de cet endroit, et les ténèbres poursuivront ses ennemis.

### Jugement contre les ennemis de YHWH

1:9	Que projetez-vous contre YHWH ? C'est lui qui fait une entière destruction. La détresse ne se lèvera pas deux fois,
1:10	car entrelacés comme des épines, et ivres de leur vin, ils seront consumés entièrement comme la paille sèche.
1:11	De toi est sorti celui qui méditait du mal contre YHWH, un conseiller de Bélial<!--Voir commentaire en De. 13:13.-->.
1:12	Ainsi parle YHWH : Bien qu'ils soient en paix et en grand nombre, ils seront certainement retranchés, et on passera outre. Or je t'ai affligé, mais je ne t'affligerai plus.
1:13	Maintenant je briserai son joug de dessus toi et je détacherai tes liens.
1:14	Voici ce qu'a ordonné YHWH contre toi : Tu n'auras plus de semence qui porte ton nom. Je retrancherai de la maison de tes elohîm les images taillées et celles en métal fondu ; j'en ferai ta tombe parce que tu es insignifiant.

## Chapitre 2

### Délivrance et réjouissance

2:1	Voici sur les montagnes les pieds de celui qui apporte de bonnes nouvelles<!--Es. 40:9, 52:7 ; Ro. 10:15.-->, qui publie la paix ! Ô Yéhouda, célèbre tes fêtes, accomplis tes vœux parce que Bélial<!--Voir commentaire en De. 13:13.--> ne passera plus au milieu de toi, il est entièrement retranché.

### Récit de la destruction de Ninive

2:2	Celui qui disperse est monté contre toi. Garde la forteresse ! Veille sur la route ! Fortifie tes reins ! Affermis bien tes forces !
2:3	Car YHWH restaure la majesté de Yaacov et la majesté d'Israël, parce que les dévastateurs les ont vidés et qu'ils ont détruit leurs sarments.
2:4	Le bouclier de ses hommes vaillants est rouge, ses hommes puissants sont teints de pourpre. Dans le feu des aciers apparaissent les chars, le jour qu'il a fixé pour la bataille, et les lances sont agitées.
2:5	Les chars s'élancent avec rapidité dans les rues, ils se précipitent sur les places, ils sont comme des flambeaux et courent comme des éclairs.
2:6	Il se souvient de ses hommes vaillants, mais ils chancellent dans leur marche. Ils se hâtent vers les murs et ils se préparent à la défense.
2:7	Les portes des fleuves sont ouvertes et le palais se fond.
2:8	C'est fixé : elle est découverte et emportée ; ses servantes gémissent de leur voix comme des colombes, frappant leurs poitrines comme un tambour.
2:9	Or Ninive depuis qu'elle existe est comme un réservoir plein d'eau, et les voilà qui fuient ! Arrêtez ! Arrêtez ! Mais personne ne se retourne.
2:10	Pillez l'argent ! Pillez l'or ! Car il n'y a pas de limite à ses richesses ni à la gloire de tous ses objets précieux.
2:11	On pille, on dévaste, on ravage ! Et les cœurs se fondent, les genoux chancellent. Que le tourment soit dans les reins de tous ! Et que leurs visages deviennent noirs comme un pot qui a été mis sur le feu !
2:12	Où est le repaire des lions, le pâturage des lionceaux, dans lequel se retiraient les lions, et où se tenaient le lion et la lionne, et le petit du lion, sans qu'aucun ne les effraie ?
2:13	Les lions déchiraient pour leurs petits, ils étranglaient pour leurs lionnes ; ils remplissaient leurs tanières de proies et leurs repaires de dépouilles.
2:14	Voici, j'en veux à toi, dit YHWH Sabaoth, je mettrai le feu à tes chars et je les réduirai en fumée, l'épée consumera tes lionceaux. Je retrancherai de la terre ta proie, et la voix de tes messagers ne sera plus entendue.

## Chapitre 3

### Méchanceté de Ninive

3:1	Malheur à la ville de sang ! Elle est toute pleine de mensonge, elle pille, les proies ne s'éloignent pas !
3:2	Bruit du fouet ! Bruit du mouvement des roues ! Le galop des chevaux ! Les chars bondissent !
3:3	Des cavaliers montant leurs chevaux ! L'épée flamboyante ! La lance étincelante ! La multitude des blessés ! Le grand nombre de cadavres ! Des cadavres à l'infini ! On trébuche ! On trébuche sur un grand nombre de cadavres !
3:4	À cause de la multitude des prostitutions de cette prostituée, d'une grâce excellente, maîtresse en sorcelleries, qui vendait les nations par ses prostitutions, et les familles par ses sorcelleries.
3:5	Voici, j'en veux à toi, dit YHWH Sabaoth, je relèverai ta robe jusqu'à ton visage. Je ferai voir ta nudité aux nations, et ta honte aux royaumes.
3:6	Je ferai tomber sur ta tête la peine de tes abominations, je te consumerai et je te donnerai en spectacle.
3:7	Et il arrivera que quiconque te verra, s'éloignera de toi et dira : Ninive est détruite ! Qui aura compassion d'elle ? Où te chercherai-je des consolateurs ?
3:8	Es-tu plus heureuse que No-Amon qui était assise au milieu des fleuves, entourée par l'eau, avec la mer pour remparts, la mer pour murailles ?
3:9	L'Éthiopie et l'Égypte étaient sa force, et ils étaient sans limites. Pouth et les Lybiens sont allés à son secours.
3:10	Elle aussi est allée en exil, en captivité ; ses enfants ont été écrasés aux carrefours de toutes les rues, et on a jeté le sort sur ses gens honorables, et tous ses grands ont été liés de chaînes.
3:11	Toi aussi, tu seras enivrée, tu te tiendras cachée, et tu chercheras un refuge contre l'ennemi.
3:12	Toutes tes forteresses sont des figues de prémices : quand on les secoue, ils tombent dans la bouche de celui qui veut les manger.
3:13	Voici, ton peuple sera comme des femmes au milieu de toi. Les portes de ton pays seront toutes ouvertes, elles seront ouvertes à tes ennemis, le feu consumera tes verrous.
3:14	Puise-toi de l'eau pour le siège ! Fortifie tes remparts ! Entre dans la boue, foule l'argile ! Et fortifie le four à brique !
3:15	Là, le feu te consumera, l'épée te retranchera, elle te dévorera comme des sauterelles. Multiplie-toi comme les sauterelles ! Multiplie-toi comme les sauterelles !
3:16	Tu as multiplié le nombre de tes marchands plus que les étoiles des cieux, les sauterelles s'étant répandues ont tout ravagé, et puis se sont envolées.
3:17	Tes princes sont comme les sauterelles, tes scribes comme des essaims de criquets qui campent sur les murs par temps froid : le soleil paraît, elles s'envolent et on ne reconnaît plus le lieu où elles étaient.
3:18	Tes bergers se sont endormis, ô roi d'Assyrie ! Tes grands hommes se tiennent dans leurs tentes, ton peuple est dispersé sur les montagnes, et nul ne le rassemble.
3:19	Il n'y a pas de remède à ta blessure, ta plaie est douloureuse. Tous ceux qui entendront parler de toi battront des mains sur toi. Car qui n'a pas continuellement éprouvé les effets de ta méchanceté ?
