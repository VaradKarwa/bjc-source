# 2 Hayyamim dibre (2 Chroniques) (2 Ch.)

Signification : Actes des journées

Auteur : Inconnu

Thème : La grandeur de Yéhouda

Date de rédaction : 5ème siècle av. J.-C.

Initialement, 1 et 2 Chroniques (Hayyamim dibre) ne constituaient qu'un seul ouvrage. Ce livre raconte le règne de Shelomoh (Salomon), la construction de la maison d'Elohîm et du palais. Il reprend ensuite l'histoire des royaumes d'Israël et de Yéhouda, du schisme à la captivité babylonienne, mettant en exergue l'instabilité du peuple dont le cœur balançait entre YHWH et les idoles.

## Chapitre 1

### YHWH élève Shelomoh qui demande la sagesse<!--1 R. 3:4-9 ; 1 Ch. 29:23-25.-->

1:1	Or Shelomoh, fils de David, se fortifia dans son royaume. YHWH, son Elohîm, fut avec lui et l'éleva extrêmement.
1:2	Shelomoh parla à tout Israël, aux chefs de milliers et de centaines, aux juges et à tous les princes de tout Israël, chefs des pères.
1:3	Shelomoh et toute l'assemblée avec lui allèrent au haut lieu qui était à Gabaon, car c'est là que se trouvait la tente d'assignation d'Elohîm, que Moshè, le serviteur de YHWH, avait fabriquée dans le désert.
1:4	Mais David avait fait monter l'arche d'Elohîm de Qiryath-Yéarim au lieu qu'il avait préparé. Il avait en effet dressé une tente pour elle à Yeroushalaim.
1:5	L'autel de cuivre que Betsaleel, fils d'Ouri, fils de Hour, avait fait, était là devant le tabernacle de YHWH. Et Shelomoh et l'assemblée y cherchèrent YHWH<!--Ex. 27:1-8, 36:1-2.-->.
1:6	Shelomoh offrit là, devant YHWH, 1 000 holocaustes, sur l'autel de cuivre qui était devant la tente d'assignation.
1:7	En cette nuit-là, Elohîm apparut à Shelomoh, et lui dit : Demande ce que tu veux que je te donne.
1:8	Et Shelomoh répondit à Elohîm : Tu as traité avec une grande bonté mon père David et tu m'as établi roi à sa place.
1:9	Maintenant, ô YHWH Elohîm ! Que ta parole à David, mon père, se confirme, car tu m'as établi roi sur un peuple nombreux comme la poussière de la terre.
1:10	Donne-moi donc maintenant de la sagesse et de l'intelligence, afin que je sorte et que j'entre devant ce peuple ! Car qui pourrait juger ton peuple, ce peuple si grand ?

### YHWH agrée la prière de Shelomoh et l'exauce<!--1 R. 3:10-28.-->

1:11	Et Elohîm dit à Shelomoh : Puisque c'est là ce qui est dans ton cœur, et que tu n'as demandé ni des richesses, ni des biens, ni de la gloire, ni la mort de ceux qui te haïssent, ni même des jours nombreux, mais que tu as demandé pour toi de la sagesse et de l'intelligence, afin de pouvoir juger mon peuple, sur lequel je t'ai établi roi,
1:12	la sagesse et l'intelligence te sont données. Je te donnerai aussi des richesses, des biens et de la gloire, comme n'en ont pas eu les rois qui ont été avant toi, et comme il n'en aura aucun après toi.
1:13	Puis Shelomoh s'en retourna à Yeroushalaim, du haut lieu qui était à Gabaon devant la tente d'assignation, et il régna sur Israël.
1:14	Shelomoh rassembla des chars et des cavaliers. Il avait 1 400 chars et 12 000 cavaliers, qu'il plaça dans les villes où il gardait ses chars et auprès du roi, à Yeroushalaim.
1:15	Et le roi fit que l'argent et l'or étaient aussi communs à Yeroushalaim que les pierres, et les cèdres, que les sycomores de la plaine.
1:16	C'était de l'Égypte que provenaient les chevaux de Shelomoh. Une caravane de marchands du roi allait les prendre par troupe à un prix convenu.
1:17	On faisait monter et sortir d'Égypte un char pour 600 sicles d'argent, et un cheval pour 150. On en amenait de même par eux pour tous les rois des Héthiens, et pour les rois de Syrie.

## Chapitre 2

### La prière de Shelomoh exaucée<!--1 R. 5:1-18, 7:13,14.-->

2:1	Or Shelomoh ordonna de bâtir une maison au Nom de YHWH, ainsi qu'une maison royale.
2:2	Et il fit un dénombrement de 70 000 hommes qui portaient les fardeaux, et de 80 000 qui coupaient le bois sur la montagne, et de 3 600 qui étaient commis sur eux.
2:3	Puis Shelomoh envoya vers Houram, roi de Tyr, pour lui dire : Agis envers moi comme tu as agi pour David, mon père, à qui tu as envoyé des cèdres, pour se bâtir une maison afin d'y habiter.
2:4	Voici, je vais bâtir une maison au Nom de YHWH, mon Elohîm, pour la lui consacrer, pour faire brûler devant lui de l'encens aromatique, pour présenter continuellement devant lui les pains de proposition, et pour offrir les holocaustes du matin et du soir, des shabbats, des nouvelles lunes, et des fêtes de YHWH, notre Elohîm, ce qui est perpétuel en Israël.
2:5	La maison que je vais bâtir sera grande, car notre Elohîm est plus grand que tous les elohîm.
2:6	Qui donc détiendrait le pouvoir de lui bâtir une maison, puisque les cieux et les cieux des cieux ne sauraient le contenir ? Et qui suis-je pour lui bâtir une maison, si ce n'est pour faire brûler des parfums devant sa face ?
2:7	Maintenant, envoie-moi un homme sage pour travailler l'or, l'argent, le cuivre et le fer, les étoffes teintes en pourpre, en cramoisi, en violet, sachant faire des sculptures, pour travailler avec les hommes sages que j'ai avec moi en Yéhouda et à Yeroushalaim, et que David, mon père, a préparés.
2:8	Envoie-moi aussi du Liban du bois de cèdre, de cyprès et de santal, car je sais que tes serviteurs savent couper les bois du Liban. Voici, mes serviteurs seront avec les tiens.
2:9	Qu'on me prépare du bois en grande quantité, car la maison que je vais bâtir sera grande et magnifique.
2:10	Et je donnerai à tes serviteurs qui couperont, qui abattront les bois, 20 000 cors de froment foulé, 20 000 cors d'orge, 20 000 baths de vin, et 20 000 baths d'huile.
2:11	Houram, roi de Tyr, répondit dans un écrit qu'il envoya à Shelomoh : C'est parce que YHWH aime son peuple qu'il t'a établi roi sur eux.
2:12	Et Houram dit : Béni soit YHWH, l'Elohîm d'Israël, qui a fait les cieux et la Terre, de ce qu'il a donné au roi David un fils sage, habile en prudence et en discernement, qui va bâtir une maison à YHWH, et une maison royale !
2:13	Je t'envoie donc un homme habile, plein de discernement, Houram-Abi,
2:14	fils d'une femme d'entre les filles de Dan et d'un père tyrien. Il sait travailler l'or, l'argent, le cuivre et le fer, les pierres et le bois, les étoffes teintes en pourpre rouge, en violet, en byssus et en cramoisi. Il sait faire toutes sortes de sculptures et imaginer toutes sortes d'objets d'art qu'on lui donne à faire. Il travaillera avec tes hommes habiles et avec les hommes habiles de mon seigneur David, ton père.
2:15	Et maintenant, que mon seigneur envoie à ses serviteurs le froment, l'orge, l'huile et le vin comme il l'a dit.
2:16	Et nous couperons des bois du Liban autant que tu en auras besoin, et nous te les amènerons en radeaux, par la mer, jusqu'à Yapho, et tu les feras monter à Yeroushalaim.
2:17	Alors Shelomoh compta tous les hommes étrangers qui étaient au pays d'Israël, d'après le dénombrement que David, son père, en avait fait. On en trouva 153 600.
2:18	Et il en établit 70 000 qui portaient des fardeaux, 80 000 qui taillaient les pierres dans la montagne, et 3 600 surveillants pour faire travailler le peuple.

## Chapitre 3

### Shelomoh commence la construction du temple<!--1 R. 6:1.-->

3:1	Shelomoh commença donc à bâtir la maison de YHWH à Yeroushalaim, sur la montagne de Moriyah, là où son père David avait eu une vision. C'était le lieu préparé par David, l'aire d'Ornan le Yebousien.
3:2	Il commença à bâtir, le second jour du second mois, la quatrième année de son règne.

### Les matériaux du temple et les dimensions<!--1 R. 6:2-38, 7:13-22.-->

3:3	Voici les fondements fixés par Shelomoh pour bâtir la maison d'Elohîm. La longueur en coudées de l'ancienne mesure était de 60 coudées, et la largeur de 20 coudées.
3:4	Le portique sur le devant avait 20 coudées de longueur, répondant à la largeur de la maison, et 120 de hauteur. Il le revêtit intérieurement d'or pur.
3:5	Il recouvrit la grande maison de bois de cyprès, la revêtit d'or fin et y fit mettre des palmes et des chaînes.
3:6	Il couvrit la maison de pierres précieuses en guise d'ornement. L'or était de l'or de Parvaïm.
3:7	Il couvrit d'or la maison, les poutres, les seuils, les parois et les portes, et il fit sculpter des chérubins sur les parois.
3:8	Il fit le Saint des saints, dont la longueur était de 20 coudées, correspondant à la largeur de la maison, et la largeur de 20 coudées. Il la couvrit d'or fin, pour une valeur de 600 talents<!--18 tonnes.-->.
3:9	Le poids des clous était de 50 sicles d'or. Il couvrit aussi d'or les chambres hautes.
3:10	Il fit dans le Saint des saints deux chérubins sculptés, et on les couvrit d'or.
3:11	La longueur des ailes des chérubins était de 20 coudées. L'aile du premier, longue de 5 coudées, touchait la paroi de la maison, et l'autre aile, longue de 5 coudées, touchait une aile de l'autre chérubin.
3:12	Et une aile de l'autre chérubin, longue de 5 coudées, touchait la paroi de la maison et l'autre aile longue de 5 coudées, joignait l'aile de l'autre chérubin.
3:13	Les ailes étendues de ces chérubins faisaient 20 coudées. Ils se tenaient debout sur leurs pieds, leurs faces tournées vers la maison.
3:14	Il fit le voile en étoffes violette, en pourpre rouge, en cramoisi et en byssus et il y représenta des chérubins.
3:15	Devant la maison, il fit deux colonnes de 35 coudées de hauteur et le chapiteau sur leur sommet était de 5 coudées.
3:16	Il fit des chaînes pareilles à celles qui étaient dans le lieu très-saint et les plaça sur le sommet des colonnes, et il fit 100 grenades qu'il attacha aux chaînes.
3:17	Il dressa les colonnes sur le devant du temple, l'une à droite, et l'autre à gauche ; il appela celle de droite du nom de Yakîn<!--Elohîm établit.-->, et celle de gauche du nom de Boaz<!--En lui est la force.-->.

## Chapitre 4

### L'autel de cuivre, la mer en métal fondu et les ustensiles du temple<!--1 R. 7:23-50.-->

4:1	Il fit aussi un autel en cuivre<!--Voir l'annexe « Le temple de Shelomoh - extérieur ».--> long de 20 coudées, large de 20 coudées et haut de 10 coudées.
4:2	Il fit la mer en métal fondu de 10 coudées d'un bord à l'autre, ronde tout autour, et haute de 5 coudées, et une circonférence que mesurait un cordon de 30 coudées.
4:3	Des figures de bœufs l'entouraient en dessous, 10 par coudée, faisant tout le tour de la mer ; il y avait deux rangées de bœufs fondus avec elle en une seule pièce.
4:4	Elle était posée sur 12 bœufs, dont 3 tournés vers le nord, 3 tournés vers l'occident, 3 tournés vers le sud, et 3 tournés vers l'orient. La mer était sur eux, et toute la partie postérieure de leur corps était en dedans.
4:5	Son épaisseur avait la largeur d'une main et son bord était comme le bord d'une coupe en fleur de lis. Elle pouvait contenir 3 000 baths.
4:6	Il fit aussi 10 cuves, et en mit 5 à droite et 5 à gauche, pour servir à la purification. On y lavait ce qui devait être offert en holocauste, tandis que la mer servait aux prêtres pour s'y laver.
4:7	Il fit 10 chandeliers en or d'après l'ordonnance et les mit dans le temple, 5 à droite et 5 à gauche.
4:8	Il fit aussi 10 tables et il les mit dans le temple, 5 à droite et 5 à gauche. Il fit 100 coupes en or.
4:9	Il fit encore le parvis des prêtres, le grand parvis et des portes pour ce parvis, et couvrit de cuivre ces portes.
4:10	Il mit la mer du côté droit, vers l'orient, face au sud-est.
4:11	Et Houram fit les cuves, les pelles et les bassins. Houram acheva de faire l'ouvrage qu'il faisait pour le roi Shelomoh dans la maison d'Elohîm :
4:12	deux colonnes, les bourrelets et les deux chapiteaux sur le sommet des colonnes ; les deux maillages pour couvrir les deux bourrelets des chapiteaux sur le sommet des colonnes ;
4:13	et les 400 grenades pour les deux maillages, deux rangs de grenades à chaque maille, pour couvrir les deux bourrelets des chapiteaux sur le sommet des colonnes.
4:14	Il fit aussi les bases, et il fit les cuves sur les bases ;
4:15	la mer et les 12 bœufs sous elle ;
4:16	les pots, les pelles et les fourchettes, et tous leurs ustensiles ; Houram-Abi les fit au roi Shelomoh, pour la maison de YHWH, en cuivre poli.
4:17	Le roi les fit fondre dans la plaine du Yarden<!--Jourdain.-->, dans une terre grasse, entre Soukkoth et Tseréda.
4:18	Et Shelomoh fit tous ces ustensiles en si grand nombre qu'on ne rechercha pas le poids du cuivre.
4:19	Shelomoh fit encore tous les ustensiles<!--Voir l'annexe « Le temple de Shelomoh - intérieur ».--> qui étaient dans la maison de YHWH : l'autel d'or, et les tables sur lesquelles on mettait le pain de proposition ;
4:20	les chandeliers et leurs lampes d'or fin, qu'on devait allumer devant le lieu très-saint, selon l'ordonnance ;
4:21	les fleurs, les lampes, et les mouchettes d'or, d'un or parfaitement pur ;
4:22	et les mouchettes, les bassins, les tasses et les encensoirs d'or fin. Quant à l'entrée de la maison, les portes intérieures conduisant dans le Saint des saints, et les portes de la maison pour entrer au temple étaient d'or.

## Chapitre 5

### L'arche de l'alliance dans le sanctuaire, YHWH manifeste sa gloire<!--1 R. 7:51-8:11.-->

5:1	Ainsi fut achevé tout l'ouvrage que Shelomoh fit pour la maison de YHWH. Puis Shelomoh fit apporter ce que David, son père, avait consacré : l'argent, l'or et tous les ustensiles ; et il les mit dans les trésors de la maison d'Elohîm.
5:2	Alors Shelomoh rassembla à Yeroushalaim les anciens d'Israël, et tous les chefs des tribus, les princes des pères des enfants d'Israël, pour transporter de la ville de David, qui est Sion, l'arche de l'alliance de YHWH.
5:3	Et tous les hommes d'Israël se rassemblèrent auprès du roi pour la fête ; c'était le septième mois.
5:4	Tous les anciens d'Israël vinrent, et les Lévites portèrent l'arche.
5:5	Ils transportèrent l'arche, la tente d'assignation, et tous les ustensiles sacrés qui étaient dans la tente. Les prêtres et les Lévites les emportèrent.
5:6	Or le roi Shelomoh, et toute l'assemblée d'Israël réunie auprès de lui étaient devant l'arche, sacrifiant du menu et du gros bétail en si grand nombre qu'on ne pouvait ni dénombrer ni compter.
5:7	Les prêtres portèrent l'arche de l'alliance de YHWH à sa place, dans le lieu très-saint de la maison, dans le Saint des saints, sous les ailes des chérubins.
5:8	Les chérubins étendaient les ailes sur l'endroit où devait être l'arche, et les chérubins couvraient l'arche et ses barres par-dessus.
5:9	Les barres avaient une longueur telle que leurs extrémités se voyaient en avant de l'arche, devant le lieu très-saint, mais elles ne se voyaient pas du dehors. Et l'arche a été là jusqu'à ce jour.
5:10	Il n'y avait dans l'arche que les deux tablettes que Moshè y avait mises en Horeb, quand YHWH traita alliance avec les enfants d'Israël à leur sortie d'Égypte.
5:11	Or il arriva que comme les prêtres sortaient du lieu saint (car tous les prêtres présents s'étaient sanctifiés, sans observer l'ordre des classes),
5:12	et que tous les Lévites qui étaient chanteurs, Asaph, Héman, Yedoutoun, leurs fils et leurs frères, vêtus de byssus, avec des cymbales, des luths et des harpes, se tenaient à l'orient de l'autel, il y avait avec eux 120 prêtres sonnant des trompettes,
5:13	il arriva, lorsque ceux qui sonnaient des trompettes et ceux qui chantaient furent comme un seul homme pour faire entendre une même voix en louant et en célébrant YHWH, et qu'ils élevèrent la voix avec des trompettes, des cymbales et des instruments de musique, en louant YHWH de ce qu'il est bon, car sa bonté est éternelle<!--Jé. 33:11 ; Ps. 118:29 ; Ps. 136.--> ! que la maison, la maison de YHWH fut remplie d'une nuée.
5:14	Les prêtres ne purent s'y tenir debout pour faire le service, à cause de la nuée. La gloire de YHWH remplissait en effet la maison d'Elohîm.

## Chapitre 6

### Shelomoh s'adresse à l'assemblée d'Israël<!--1 R. 8:12-21.-->

6:1	Alors Shelomoh dit : YHWH a dit qu'il habiterait dans les ténèbres épaisses<!--Nous avons ici une prophétie concernant la venue du Mashiah (Christ). Elohîm, qui est lumière, a accepté d'habiter dans les ténèbres afin de nous sauver (Mt. 4:16 ; Jn. 1:5).-->.
6:2	Et moi, j'ai bâti une maison qui sera ta demeure, et un domicile afin que tu y résides à toujours !
6:3	Puis le roi tourna son visage, et bénit toute l'assemblée d'Israël. Toute l'assemblée d'Israël se tenait debout.
6:4	Et il dit : Béni soit YHWH, l'Elohîm d'Israël, qui de sa bouche a parlé à David, mon père, et qui par sa main puissante accomplit ce qu'il avait déclaré en disant :
6:5	Depuis le jour où j'ai fait sortir mon peuple du pays d'Égypte, je n'ai pas choisi de ville entre toutes les tribus d'Israël pour y bâtir une maison afin que mon Nom y réside, et je n'ai pas choisi d'homme pour être chef de mon peuple d'Israël.
6:6	Mais j'ai choisi Yeroushalaim pour que mon Nom y réside, et j'ai choisi David pour qu'il règne sur mon peuple d'Israël.
6:7	Or David, mon père, avait à cœur de bâtir une maison au Nom de YHWH, l'Elohîm d'Israël.
6:8	Mais YHWH parla à David, mon père : Puisque tu as eu à cœur de bâtir une maison à mon Nom, tu as bien fait d'avoir eu cette intention.
6:9	Seulement, ce n'est pas toi qui bâtiras cette maison, mais ce sera ton fils, qui sortira de tes entrailles, qui bâtira cette maison à mon Nom<!--2 S. 7:1-14.-->.
6:10	YHWH a accompli la parole qu'il avait déclarée : j'ai succédé à David, mon père, et je me suis assis sur le trône d'Israël, comme YHWH l'avait dit, et j'ai bâti cette maison au Nom de YHWH, l'Elohîm d'Israël.
6:11	J'y ai mis l'arche où est l'alliance de YHWH, qu'il traita avec les enfants d'Israël.

### Prière de Shelomoh<!--1 R. 8:22-61.-->

6:12	Puis il se plaça devant l'autel de YHWH, en face de toute l'assemblée d'Israël, et il étendit ses mains.
6:13	Car Shelomoh avait fait une tribune en cuivre et l'avait mise au milieu du grand parvis. Elle était longue de 5 coudées, large de 5 coudées et haute de 3 coudées. Il s'y plaça, se mit à genoux en face de toute l'assemblée d'Israël et, étendant ses mains vers les cieux,
6:14	il dit : YHWH, Elohîm d'Israël ! Il n'y a ni dans les cieux ni sur la Terre d'Elohîm semblable à toi, qui gardes l'alliance et la miséricorde envers tes serviteurs qui marchent de tout leur cœur devant ta face.
6:15	Toi qui as tenu parole à ton serviteur David, mon père. Ce que tu lui avais promis, et ce que tu as déclaré de ta bouche, tu l'as accompli de ta main puissante, comme il paraît aujourd'hui.
6:16	Maintenant, YHWH, Elohîm d'Israël ! tiens la parole que tu as faite à ton serviteur David, mon père, en disant : Tu ne manqueras jamais devant moi d'un successeur assis sur le trône d'Israël, pourvu que tes fils prennent garde à leur voie pour marcher dans ma torah, comme tu as marché devant ma face.
6:17	Et maintenant, ô YHWH, Elohîm d'Israël ! que ta parole, que tu as déclarée à David, ton serviteur, soit confirmée !
6:18	Mais Elohîm habiterait-il vraiment sur la Terre avec les humains ? Voici, les cieux, même les cieux des cieux, ne peuvent te contenir, combien moins cette maison que j'ai bâtie !
6:19	Tournes-toi, ô YHWH, mon Elohîm, vers la prière de ton serviteur et vers sa supplication, pour écouter le cri et la prière que ton serviteur prie en face de toi.
6:20	Que tes yeux soient ouverts jour et nuit sur cette maison, sur le lieu où tu as promis de mettre ton Nom ! Écoute la prière que ton serviteur te fait en ce lieu.
6:21	Exauce les supplications de ton serviteur et de ton peuple d'Israël, quand ils prieront en ce lieu. Exauce des cieux, du lieu de ta demeure, exauce et pardonne !
6:22	Si quelqu'un pèche contre son prochain, et qu'on lui impose un serment pour le faire jurer, et qu'il vient prêter serment devant ton autel, dans cette maison,
6:23	écoute-le des cieux, agis et juge tes serviteurs, en donnant au méchant son salaire, et fais retomber sa conduite sur sa tête, en justifiant le juste, et lui rendant selon sa justice.
6:24	Quand ton peuple d'Israël sera battu par l'ennemi, pour avoir péché contre toi. S'ils retournent à toi, s'ils donnent gloire à ton Nom, s'ils t'adressent dans cette maison des prières et des supplications ;
6:25	toi, exauce-les des cieux, et pardonne le péché de ton peuple d'Israël, et ramène-les dans la terre que tu leur as donnée à eux et à leurs pères.
6:26	Quand les cieux seront fermés, et qu'il n'y aura pas de pluie, parce qu'ils auront péché contre toi, s'ils prient en ce lieu, s'ils donnent gloire à ton Nom, et s'ils se détournent de leurs péchés, parce que tu les auras affligés,
6:27	toi, exauce-les des cieux, et pardonne le péché de tes serviteurs et de ton peuple d'Israël, après que tu leur auras enseigné le bon chemin, par lequel ils doivent marcher, et envoie de la pluie sur la terre que tu as donnée en héritage à ton peuple.
6:28	Quand il y aura dans le pays la famine ou la peste, quand il y aura la sécheresse, la nielle, les sauterelles d'une espèce ou d'une autre, quand les ennemis les assiégeront dans leur pays, dans leurs portes, ou qu'il y aura un fléau, une maladie quelconque,
6:29	si un être humain, si tout ton peuple d'Israël fait entendre des prières et des supplications, et que chacun reconnaît sa plaie et sa douleur, et étend ses mains vers cette maison,
6:30	exauce-le des cieux, du lieu de ta demeure, et pardonne. Donne à chacun selon toutes ses voies, toi qui connais leur cœur. En effet, toi seul tu connais le cœur des fils des humains.
6:31	Ainsi ils te craindront et marcheront dans tes voies tout le temps qu'ils vivront sur la terre que tu as donnée à nos pères.
6:32	Et l'étranger, qui ne sera pas de ton peuple d'Israël, mais qui viendra d'un pays éloigné, à cause de ton grand Nom, de ta main puissante, et de ton bras étendu ; quand il viendra prier dans cette maison,
6:33	exauce-le des cieux, du lieu de ta demeure, et accorde tout ce que cet étranger réclamera de toi ! Afin que tous les peuples de la Terre connaissent ton Nom pour te craindre comme ton peuple d'Israël, et sachent que ton Nom est invoqué sur cette maison que j'ai bâtie.
6:34	Quand ton peuple sortira en guerre contre ses ennemis, par le chemin sur lequel tu l'auras envoyé, s'ils te prient, en regardant vers cette ville que tu as choisie et vers cette maison que j'ai bâtie à ton Nom,
6:35	exauce des cieux leur prière et leur supplication et fais-leur justice !
6:36	Quand ils pécheront contre toi, car il n'y a pas d'être humain qui ne pèche, quand tu seras en colère contre eux et que tu les livreras devant l'ennemi, quand ceux qui les auront faits captifs les emmèneront en captivité dans un pays lointain ou proche,
6:37	si dans le pays où ils seront captifs, ils rentrent en eux-mêmes, se repentent et t'adressent des supplications dans le pays de leur captivité, en disant : Nous avons péché, nous avons commis l'iniquité, nous avons agi méchamment !
6:38	S'ils retournent à toi de tout leur cœur et de toute leur âme, dans le pays de leur captivité où ils ont été emmenés captifs, et s'ils t'adressent des prières, les regards tournés vers leur pays que tu as donné à leurs pères, vers cette ville que tu as choisie, et vers cette maison que j'ai bâtie à ton Nom,
6:39	exauce des cieux, du lieu de ta demeure, leurs prières et leurs supplications, et fais-leur justice. Pardonne à ton peuple qui aura péché contre toi !
6:40	Maintenant, ô mon Elohîm, que tes yeux soient ouverts et que tes oreilles soient attentives à la prière qu'on te fera en ce lieu !
6:41	Et maintenant, YHWH Elohîm ! Lève-toi, viens au lieu de ton repos, toi et l'arche de ta puissance. YHWH Elohîm, que tes prêtres soient revêtus du salut, et que tes bien-aimés se réjouissent du bien que tu leur fais !
6:42	YHWH Elohîm, ne repousse pas la face de ton mashiah, souviens-toi des grâces accordées à David, ton serviteur.

## Chapitre 7

### YHWH répond par le feu : Sa gloire remplit la maison

7:1	Lorsque Shelomoh eut achevé de prier, le feu descendit du ciel et consuma l'holocauste et les sacrifices<!--Lé. 9:24 ; 1 R. 18:38.-->, et la gloire de YHWH remplit la maison.
7:2	Les prêtres ne pouvaient entrer dans la maison de YHWH, parce que la gloire de YHWH avait rempli la maison de YHWH.
7:3	Tous les enfants d'Israël virent descendre le feu et la gloire de YHWH sur la maison. Ils se courbèrent, le visage contre terre, sur le pavé, ils se prosternèrent et louèrent YHWH en disant : Car il est bon, car sa miséricorde demeure éternellement !

### Shelomoh et le peuple offrent des sacrifices à YHWH<!--1 R. 8:62-66.-->

7:4	Or le roi et tout le peuple offraient des sacrifices devant YHWH.
7:5	Le roi Shelomoh offrit un sacrifice de 22 000 bœufs, et 120 000 brebis. Ainsi, le roi et tout le peuple firent la dédicace de la maison d'Elohîm.
7:6	Les prêtres se tenaient à leurs fonctions, ainsi que les Lévites, avec les instruments de musique de YHWH, que le roi David avait faits pour louer YHWH en disant : Car sa miséricorde demeure éternellement ! ayant les Psaumes de David entre leurs mains. Et les prêtres sonnaient des trompettes vis-à-vis d'eux, et tout Israël se tenait debout.
7:7	Shelomoh consacra le milieu du parvis qui est devant la maison de YHWH. En effet, c'est là qu'il offrit les holocaustes et les graisses des sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->, parce que l'autel de cuivre que Shelomoh avait fait, ne pouvait contenir les holocaustes, les offrandes et les graisses.
7:8	Ainsi Shelomoh célébra, en ce temps-là, la fête pendant 7 jours, avec tout Israël. Il y avait une grande multitude, venue depuis l'entrée de Hamath jusqu'au torrent d'Égypte.
7:9	Le huitième jour, ils firent une assemblée solennelle, car ils firent la dédicace de l'autel pendant 7 jours, et la fête pendant 7 jours.
7:10	Le vingt-troisième jour du septième mois, il laissa aller le peuple dans ses tentes, se réjouissant et ayant le cœur plein de joie, à cause du bien que YHWH avait fait à David, à Shelomoh, et à Israël, son peuple.

### YHWH apparaît à Shelomoh<!--1 R. 9:1-9.-->

7:11	Shelomoh acheva donc la maison de YHWH et la maison du roi. Shelomoh réussit dans tout ce qui lui vint à cœur de faire dans la maison de YHWH et dans sa maison.
7:12	YHWH apparut à Shelomoh pendant la nuit, et lui dit : J'exauce ta prière, et je choisis ce lieu comme une maison de sacrifices.
7:13	Quand je fermerai les cieux, et qu'il n'y aura pas de pluie, et quand j'ordonnerai aux sauterelles de consumer le pays, et quand j'enverrai la peste parmi mon peuple,
7:14	si mon peuple, sur lequel mon Nom est invoqué, s'humilie, prie, et cherche ma face, et s'il se détourne de ses mauvaises voies, alors je l'exaucerai des cieux, je pardonnerai ses péchés, et je guérirai son pays.
7:15	Mes yeux seront désormais ouverts, et mes oreilles seront attentives à la prière faite en ce lieu.
7:16	Maintenant je choisis et je sanctifie cette maison afin que mon Nom y soit à toujours, mes yeux et mon cœur seront toujours là.
7:17	Et toi, si tu marches devant moi comme David, ton père, a marché, faisant tout ce que je t'ai ordonné, et si tu gardes mes lois et mes ordonnances,
7:18	j'affermirai le trône de ton royaume, comme je l'ai déclaré à David, ton père, en disant : Il ne te manquera pas de successeur qui règne en Israël.
7:19	Mais si vous vous détournez, et si vous abandonnez mes statuts et mes commandements que je vous ai prescrits, et si vous allez servir d'autres elohîm et vous prosterner devant eux,
7:20	je vous arracherai de mon pays que je vous ai donné, je rejetterai loin de moi cette maison que j'ai consacrée à mon Nom, et j'en ferai un sujet de parabole et de raillerie parmi tous les peuples.
7:21	Et quiconque passera près de cette maison qui aura été élevée, sera dans l'étonnement et dira : Pourquoi YHWH a-t-il ainsi traité ce pays et cette maison ?
7:22	Et on répondra : Parce qu'ils ont abandonné YHWH, l'Elohîm de leurs pères, qui les a fait sortir du pays d'Égypte, et qu'ils se sont attachés à d'autres elohîm, et qu'ils se sont prosternés devant eux, et les ont servis. Voilà pourquoi il a fait venir sur eux tous ces malheurs.

## Chapitre 8

### Les réalisations de Shelomoh<!--1 R. 9:15-28, 10:26-29.-->

8:1	Au bout de 20 ans, pendant lesquels Shelomoh bâtit la maison de YHWH et sa propre maison,
8:2	il bâtit les villes que Houram lui avait données et y fit habiter les enfants d'Israël.
8:3	Puis Shelomoh marcha contre Hamath de Tsoba, et la conquit.
8:4	Il bâtit Thadmor dans le désert, et toutes les villes servant de magasins qu'il bâtit dans le pays de Hamath.
8:5	Il bâtit Beth-Horon la haute, et Beth-Horon la basse, villes fortes de murailles, de portes et de barres ;
8:6	Baalath, et toutes les villes servant de magasins qu'avait Shelomoh, toutes les villes pour les chars, les villes pour la cavalerie, et tout ce que Shelomoh prit plaisir à bâtir à Yeroushalaim, au Liban, et dans tout le pays de sa domination.
8:7	Tout le peuple qui était resté des Héthiens, des Amoréens, des Phéréziens, des Héviens et des Yebousiens, qui n'étaient pas d'Israël,
8:8	leurs descendants, qui étaient restés après eux dans le pays, et que les enfants d'Israël n'avaient pas détruits, Shelomoh les leva comme des gens de corvée jusqu'à ce jour.
8:9	Shelomoh n'employa comme esclave pour ses travaux aucun des fils d'Israël, car ils étaient des hommes de guerre, les chefs de ses officiers, les chefs de ses chars et de ses hommes d'armes.
8:10	Voici le nombre des chefs de ceux qui étaient préposés aux travaux du roi Shelomoh : Ils étaient 250, ayant autorité sur le peuple.
8:11	Shelomoh fit monter la fille de pharaon de la cité de David dans la maison qu'il lui avait bâtie, car il dit : Ma femme n'habitera pas dans la maison de David, roi d'Israël, parce que les lieux où l'arche de YHWH est entrée sont saints.
8:12	Alors Shelomoh offrit des holocaustes à YHWH, sur l'autel de YHWH qu'il avait bâti devant le portique.
8:13	Il offrait chaque jour ce qui était prescrit par Moshè pour les shabbats, pour les nouvelles lunes, et pour les fêtes, trois fois par année, à la fête des pains sans levain, à la fête des semaines, et à la fête des tabernacles<!--Ex. 14:17 ; Lé. 23:1-44.-->.
8:14	Il établit, selon l'ordonnance de David, son père, les classes des prêtres selon leur fonction, et les Lévites selon leurs charges, pour célébrer YHWH et pour faire, jour par jour, le service en présence des prêtres, et les portiers, selon leurs classes, à chaque porte, car tel était le commandement de David, homme d'Elohîm.
8:15	Et on ne s'écarta pas du commandement du roi à l'égard des prêtres et des Lévites, en aucune chose, ni à l'égard des trésors.
8:16	Ainsi fut préparé tout l'ouvrage de Shelomoh, jusqu'au jour de la fondation de la maison de YHWH et jusqu'à ce qu'elle fut terminée. La maison de YHWH fut donc achevée.
8:17	Alors Shelomoh alla à Etsyôn-Guéber et à Éloth, sur le rivage de la mer, dans le pays d'Édom.
8:18	Et Houram lui envoya, sous la conduite de ses serviteurs, des navires et des serviteurs connaissant la mer. Ils allèrent avec les serviteurs de Shelomoh à Ophir, et ils y prirent 450 talents d'or, qu'ils apportèrent au roi Shelomoh.

## Chapitre 9

### La reine de Séba chez Shelomoh<!--1 R. 10:1-13.-->

9:1	Or la reine de Séba, ayant appris la renommée de Shelomoh, vint à Yeroushalaim pour éprouver Shelomoh par des énigmes. Elle avait une suite très nombreuse, et des chameaux portant des aromates, de l'or en grande quantité et des pierres précieuses. Elle vint auprès de Shelomoh, et elle lui parla de tout ce qu'elle avait dans le cœur.
9:2	Shelomoh lui expliqua toutes les choses dont elle parlait : il n'y avait rien de caché que Shelomoh ne pût lui expliquer.
9:3	Alors, la reine de Séba vit toute la sagesse de Shelomoh, et la maison qu'il avait bâtie,
9:4	les mets de sa table, la demeure de ses serviteurs, l'ordre de service et les vêtements de ceux qui le servaient, ses échansons et leurs vêtements, et les marches par où l'on montait à la maison de YHWH. Elle en perdit le souffle.
9:5	Elle parla ainsi au roi : Elle était donc vraie, la parole que j'ai entendue dans mon pays, sur tes paroles et sur ta sagesse !
9:6	Je ne croyais pas à ces paroles avant d'être venue et d'avoir vu de mes yeux. Et voici qu'on ne m'a pas raconté la moitié de la grandeur de ta sagesse ! Tu surpasses la rumeur que j'avais entendue.
9:7	Heureux tes hommes ! Heureux tes serviteurs qui se tiennent continuellement devant toi, et qui entendent ta sagesse !
9:8	Béni soit YHWH, ton Elohîm, qui a pris plaisir en toi pour te placer sur son trône comme roi pour YHWH, ton Elohîm ! C'est parce que ton Elohîm aime Israël et veut le faire subsister à jamais, qu'il t'a établi roi sur eux pour faire droit et justice.
9:9	Puis elle donna au roi 120 talents d'or, une très grande quantité d'aromates, et des pierres précieuses. Il n'y eut plus d'aromates tels que ceux que la reine de Séba donna au roi Shelomoh.
9:10	Les serviteurs de Houram et les serviteurs de Shelomoh, qui amenèrent de l'or d'Ophir, amenèrent aussi du bois de santal et des pierres précieuses.
9:11	Le roi fit de ce bois de santal les chemins qui allaient à la maison de YHWH et à la maison du roi, et des harpes et des luths pour les chanteurs. On n'en avait pas vu auparavant de semblable dans le pays de Yéhouda.
9:12	Le roi Shelomoh donna à la reine de Séba tout ce qu'elle désira, ce qu'elle demanda, plus qu'elle n'avait apporté au roi. Puis elle s'en retourna, revint dans son pays, elle et ses serviteurs.

### Les richesses de Shelomoh<!--Cp. 1 R. 4:1-34.-->

9:13	Le poids de l'or qui arrivait à Shelomoh chaque année était de 666 talents d'or,
9:14	outre ce qu'il retirait des hommes de voyage et des marchands qui en apportaient, et de tous les rois des Arabes et des gouverneurs de ces pays-là, qui apportaient de l'or et de l'argent à Shelomoh.
9:15	Le roi Shelomoh fit 200 grands boucliers en or battu, employant 600 sicles d'or battu pour chaque bouclier,
9:16	et 300 autres boucliers plus petits en or battu, employant 300 sicles d'or pour chaque bouclier. Le roi les mit dans la maison de la forêt du Liban.
9:17	Le roi fit aussi un grand trône en ivoire et il le couvrit d'or pur.
9:18	Ce trône avait 6 marches et un marchepied en or qui était accolé au trône. Il avait des accoudoirs de l'un et de l'autre côté du siège ; et deux lions se tenaient auprès des accoudoirs.
9:19	12 lions se tenaient là sur les 6 marches de part et d'autre. Rien de pareil n'avait été fait pour aucun royaume.
9:20	Et toutes les coupes à boire du roi Shelomoh étaient en or et toute la vaisselle de la maison de la forêt du Liban était en or pur. Rien n'était d'argent : on n'en faisait aucun cas du temps de Shelomoh.
9:21	Car les navires du roi allaient à Tarsis avec les serviteurs de Houram. Une fois tous les 3 ans arrivaient les navires de Tarsis, apportant de l'or, de l'argent, des dents d'éléphants, des singes et des paons.
9:22	Le roi Shelomoh fut plus grand que tous les rois de la Terre, tant en richesses qu'en sagesse.
9:23	Tous les rois de la Terre cherchaient à voir la face de Shelomoh, pour écouter la sagesse qu'Elohîm avait mise dans son cœur.
9:24	Et chacun d'eux apportait son présent : Des ustensiles en argent, des ustensiles en or, des vêtements, des armes, des aromates, des chevaux et des mulets, et il en était ainsi année après année.
9:25	Shelomoh avait 4 000 écuries pour ses chevaux, avec des chars et 12 000 cavaliers qu'il plaça dans les villes où il gardait ses chars et à Yeroushalaim près du roi.
9:26	Il dominait sur tous les rois depuis le fleuve jusqu'au pays des Philistins, et jusqu'à la frontière d'Égypte.
9:27	Et le roi fit que l'argent était aussi commun à Yeroushalaim que les pierres, et les cèdres aussi nombreux que les sycomores qui sont dans les plaines.
9:28	On tirait des chevaux pour Shelomoh de l'Égypte et de tous les pays.

### Mort de Shelomoh<!--1 R. 11:1-40.-->

9:29	Le reste des actions de Shelomoh, les premières et les dernières, cela n'est-il pas écrit dans le livre de Nathan le prophète, dans la prophétie d'Achiyah de Silo, et dans la vision de Ye`diy le voyant, concernant Yarobam, fils de Nebath ?
9:30	Shelomoh régna 40 ans à Yeroushalaim sur tout Israël.
9:31	Puis Shelomoh s'endormit avec ses pères, et on l'ensevelit dans la cité de David, son père. Rehabam, son fils, régna à sa place.

## Chapitre 10

### Rehabam (Roboam) règne sur Israël<!--1 R. 12:1-15.-->

10:1	Rehabam se rendit à Sichem, car tout Israël était venu à Sichem pour l'établir roi.
10:2	Et il arriva que Yarobam, fils de Nebath, qui était en Égypte, où il s'était enfui de devant le roi Shelomoh, l'eut appris, il revint d'Égypte.
10:3	Car on envoya l'appeler. Ainsi Yarobam et tout Israël vinrent et parlèrent à Rehabam, en disant :
10:4	Ton père a mis sur nous un joug pesant. Allège maintenant cette rude servitude de ton père, et ce joug pesant qu'il a mis sur nous, et nous te servirons.
10:5	Alors il leur dit : Revenez vers moi dans 3 jours. Et le peuple s'en alla.
10:6	Le roi Rehabam demanda conseil aux vieillards qui avaient été auprès de Shelomoh, son père, pendant sa vie, et il leur parla ainsi : Comment, et quelle parole conseillez-vous de répondre à ce peuple ?
10:7	Et ils lui répondirent en ces termes : Si tu es bon envers ce peuple, si tu es favorable envers eux, et que tu leur dises de bonnes paroles, ils seront tes serviteurs pour toujours.
10:8	Mais il laissa le conseil que les vieillards lui avaient donné, et il demanda conseil aux enfants qui avaient grandi avec lui et qui se tenaient en sa présence.
10:9	Et il leur dit : Que me conseillez-vous ? Quelle parole allons-nous retourner à ce peuple qui m'a parlé en disant : Allège le joug que ton père a mis sur nous ?
10:10	Et les enfants qui avaient grandi avec lui, lui parlèrent en disant : Tu répondras en disant à ce peuple qui t'a parlé et t'a dit : Ton père a mis sur nous un joug pesant, mais toi, allège-le ; tu leur répondras donc : Mon petit doigt est plus gros que les reins de mon père.
10:11	Or mon père a mis sur vous un joug pesant, mais moi, je rendrai votre joug encore plus pesant. Mon père vous a châtiés avec des fouets, mais moi, je vous châtierai avec des scorpions.

### Rehabam (Roboam) délaisse le conseil des anciens

10:12	Trois jours après, Yarobam, avec tout le peuple, vint vers Rehabam, suivant ce qu'avait dit le roi : Revenez vers moi dans 3 jours.
10:13	Mais le roi leur répondit durement. Le roi Rehabam délaissa le conseil des anciens,
10:14	et leur parla suivant le conseil des enfants, en disant : Mon père a mis sur vous un joug pesant, mais moi, j'y ajouterai encore. Mon père vous a châtiés avec des fouets, mais moi, je vous châtierai avec des scorpions.
10:15	Le roi n'écouta donc pas le peuple. Cela fut en effet conduit par Elohîm pour que s'accomplisse la parole que YHWH avait dite par l'intermédiaire d'Achiyah de Silo<!--1 R. 11:30-39.--> à Yarobam, le fils de Nebath.

### Israël se détache de la maison de David<!--1 R. 12:16-19.-->

10:16	Quand tout Israël vit que le roi ne les écoutait pas, le peuple répondit au roi, en disant : Quelle part avons-nous avec David ? Nous n'avons pas d'héritage avec le fils d'Isaï. Israël, chacun à ses tentes ! Et toi David, pourvois maintenant à ta maison. Ainsi, tout Israël s'en alla dans ses tentes.
10:17	Mais quant aux enfants d'Israël qui habitaient les villes de Yéhouda, Rehabam régna sur eux.
10:18	Alors le roi Rehabam envoya Hadoram, qui était préposé aux impôts, mais les enfants d'Israël le lapidèrent à coups de pierres et il mourut. Et le roi Rehabam se hâta de monter sur un char pour s'enfuir à Yeroushalaim.
10:19	C'est ainsi qu'Israël s'est rebellé contre la maison de David, jusqu'à ce jour.

## Chapitre 11

### YHWH interdit la guerre entre Yéhouda et Israël<!--1 R. 12:21-24.-->

11:1	Rehabam, étant arrivé à Yeroushalaim, rassembla la maison de Yéhouda et de Benyamin, 180 000 hommes de guerre et sélectionnés afin de combattre contre Israël, pour le ramener sous le règne de Rehabam.
11:2	Mais la parole de YHWH vint à Shema’yah, homme d'Elohîm, en ces termes :
11:3	Parle à Rehabam, fils de Shelomoh, roi de Yéhouda, et à ceux d'Israël qui sont en Yéhouda et en Benyamin, et dis-leur :
11:4	Ainsi parle YHWH : Ne montez pas et ne faites pas la guerre à vos frères ! Que chacun de vous retourne dans sa maison, car c'est par moi que cette chose est arrivée. Et ils obéirent aux paroles de YHWH, et ils s'en retournèrent sans aller contre Yarobam<!--1 R. 12:21-24.-->.
11:5	Rehabam demeura donc à Yeroushalaim, et il bâtit des villes fortes en Yéhouda.
11:6	Il bâtit Bethléhem, Étham, Tekoa,
11:7	Beth-Tsour, Soco, Adoullam,
11:8	Gath, Maréshah, Ziph,
11:9	Adoraïm, Lakis, Azéqah,
11:10	Tsorea, Ayalon et Hébron, qui étaient en Yéhouda et en Benyamin, et en fit des villes fortes.
11:11	Il les fortifia et y mit des gouverneurs, des provisions de vivres, d'huile et de vin.
11:12	Dans chacune de ces villes, il mit des boucliers et des lances, et il les rendit extrêmement fortes. Ainsi Yéhouda et Benyamin lui furent soumis.

### Les prêtres et les Lévites soutiennent Rehabam

11:13	Les prêtres et les Lévites, qui étaient dans tout Israël, vinrent de toutes leurs contrées se joindre à lui.

### Yarobam abandonne YHWH<!--1 R. 12:26-30, 14:7-8.-->

11:14	Car les Lévites abandonnèrent leurs faubourgs et leurs possessions et vinrent en Yéhouda et à Yeroushalaim, parce que Yarobam et ses fils les avaient rejetés des fonctions de prêtres pour YHWH.
11:15	Car il s'était établi des prêtres pour les hauts lieux, pour les boucs, et pour les veaux qu'il avait faits.
11:16	Et à leur suite, ceux d'entre toutes les tribus d'Israël qui avaient appliqué leur cœur à chercher YHWH, l'Elohîm d'Israël, vinrent à Yeroushalaim pour sacrifier à YHWH, l'Elohîm de leurs pères.
11:17	Ils fortifièrent le royaume de Yéhouda et affermirent Rehabam, fils de Shelomoh, pendant 3 ans. En effet, ils marchèrent pendant 3 ans dans la voie de David et de Shelomoh.

### Les femmes et les enfants de Rehabam

11:18	Or Rehabam prit pour femme : Mahalath, fille de Yeriymoth, fils de David et d'Abichaïl, fille d'Éliab, fils d'Isaï.
11:19	Elle lui enfanta des fils : Yéoush, Shemaryah et Zaham.
11:20	Après elle, il prit Ma'akah, fille d'Absalom, qui lui enfanta Abiyah, Attaï, Ziza et Shelomiyth.
11:21	Rehabam aima Ma'akah, fille d'Absalom, plus que toutes ses femmes et ses concubines. Car il prit 18 femmes et 60 concubines, et il engendra 28 fils et 60 filles.
11:22	Rehabam établit pour chef Abiyah, fils de Ma'akah, comme prince entre ses frères, car il voulait le faire roi.
11:23	Il agit prudemment et dispersa tous ses fils dans toutes les contrées de Yéhouda et de Benyamin, dans toutes les villes fortes. Il leur donna de quoi vivre en abondance, et demanda pour eux une multitude de femmes.

## Chapitre 12

### Rehabam affermi, il abandonne YHWH<!--1 R. 14:21-24.-->

12:1	Et il arriva que lorsque la royauté de Rehabam fut affermie et qu'il eut acquis de la force, il abandonna la torah de YHWH, et tout Israël avec lui.

### YHWH veut livrer Yéhouda à Shishak<!--1 R. 14:25-28.-->

12:2	C'est pourquoi il arriva que la cinquième année du roi Rehabam, Shishak, roi d'Égypte, monta contre Yeroushalaim, parce qu'ils avaient péché contre YHWH.
12:3	Il avait 1 200 chars et 60 000 cavaliers, et le peuple qui vint avec lui d'Égypte, des Libyens, des Soukkiens et des Éthiopiens, était innombrable.
12:4	Il prit les villes fortes qui appartenaient à Yéhouda, et vint jusqu'à Yeroushalaim.
12:5	Alors Shema’yah, le prophète, vint vers Rehabam et les chefs de Yéhouda, qui s'étaient rassemblés à Yeroushalaim à cause de Shishak, et leur dit : Ainsi parle YHWH : Vous m'avez abandonné. Moi aussi je vous abandonne aux mains de Shishak.
12:6	Alors les chefs d'Israël et le roi s'humilièrent, et dirent : YHWH est juste !
12:7	Et quand YHWH vit qu'ils s'humiliaient, la parole de YHWH vint à Shema’yah, et il lui dit : Ils se sont humiliés. Je ne les détruirai pas. Mais je leur donnerai sous peu la délivrance, et ma fureur ne se répandra pas sur Yeroushalaim par la main de Shishak.
12:8	Toutefois, ils lui seront asservis, afin qu'ils sachent ce que c'est que de me servir ou de servir les royaumes de la Terre.
12:9	Shishak, roi d'Égypte, monta donc contre Yeroushalaim. Il prit les trésors de la maison de YHWH et les trésors de la maison du roi, il prit tout. Il prit les boucliers en or que Shelomoh avait faits.
12:10	Le roi Rehabam fit des boucliers de cuivre à leur place, et il les mit entre les mains des chefs des coureurs qui gardaient la porte de la maison du roi.
12:11	Et toutes les fois que le roi entrait dans la maison de YHWH, les coureurs venaient et les portaient. Puis ils les rapportaient dans la chambre des coureurs.
12:12	Ainsi comme il s'était humilié, la colère de YHWH se détourna de lui, et ne le détruisit pas entièrement. Et il y avait encore de bonnes choses en Yéhouda.

### Mort de Rehabam<!--1 R. 14:21,29,31.-->

12:13	Le roi Rehabam se fortifia donc dans Yeroushalaim, et régna. Il avait 41 ans quand il devint roi, et il régna 17 ans à Yeroushalaim, la ville que YHWH avait choisie de toutes les tribus d'Israël, pour y mettre son Nom. Sa mère s'appelait Na`amah, l'Ammonite.
12:14	Il fit du mal, car il ne disposa pas son cœur à chercher YHWH.
12:15	Or les actions de Rehabam, les premières et les dernières, ne sont-elles pas écrites dans les livres de Shema’yah le prophète, et d'Iddo le voyant, parmi les registres généalogiques ? Les guerres entre Rehabam et Yarobam furent continuelles.
12:16	Rehabam s'endormit avec ses pères, et il fut enseveli dans la cité de David et Abiyah, son fils, régna à sa place.

## Chapitre 13

### Abiyah règne sur Yéhouda ; guerre entre Israël et Yéhouda<!--1 R. 15:1-8.-->

13:1	La dix-huitième année du roi Yarobam, Abiyah commença à régner sur Yéhouda.
13:2	Il régna 3 ans à Yeroushalaim. Sa mère s'appelait Miykayah, fille d'Ouriel, de Guibea. Or il y eut guerre entre Abiyah et Yarobam.
13:3	Abiyah engagea la guerre avec une armée d'hommes vaillants, 400 000 hommes sélectionnés. Yarobam se rangea en bataille contre lui avec 800 000 hommes sélectionnés, hommes vaillants et talentueux.
13:4	Et Abiyah se leva du haut de la montagne de Tsemaraïm, parmi les montagnes d'Éphraïm, et dit : Yarobam et tout Israël, écoutez-moi !
13:5	Ne savez-vous pas que YHWH, l'Elohîm d'Israël, a donné pour toujours la royauté sur Israël à David, à lui et à ses fils, par une alliance de sel<!--Sel : voir commentaire en Lé. 2:13.--> !
13:6	Mais Yarobam, fils de Nebath, serviteur de Shelomoh, fils de David, s'est élevé et s'est rebellé contre son seigneur.
13:7	Et des gens sans valeur, des fils de Bélial<!--Voir commentaire en De. 13:13.-->, se sont rassemblés auprès de lui et se sont fortifiés contre Rehabam, fils de Shelomoh. Or Rehabam était un jeune homme craintif et sans force devant eux.
13:8	Et maintenant, vous vous dites être forts devant la royauté de YHWH, qui est aux mains des fils de David ! Vous êtes une multitude et vous avez avec vous les veaux d'or que Yarobam vous a fabriqués pour qu'ils soient vos elohîm.
13:9	N'avez-vous pas rejeté les prêtres de YHWH, les fils d'Aaron, et les Lévites ? Et ne vous êtes-vous pas faits des prêtres comme les peuples des autres pays ? Quiconque venait avec un jeune taureau et 7 béliers pour remplir sa main devenait prêtre de ce qui n'est pas Elohîm.
13:10	Mais quant à nous, YHWH est notre Elohîm, et nous ne l'avons pas abandonné. Les prêtres qui font le service de YHWH sont fils d'Aaron, et ce sont les Lévites qui tiennent cette fonction.
13:11	Nous faisons brûler pour YHWH, chaque matin et chaque soir, des holocaustes et de l'encens aromatique. Les pains de proposition sont rangés sur la table pure, et on allume le chandelier en or avec ses lampes, chaque soir. Car nous gardons ce que YHWH, notre Elohîm, veut qu'on garde. Mais vous, vous l'avez abandonné.
13:12	Et voici, nous avons avec nous, à notre tête, Elohîm et ses prêtres, prêts à sonner de la trompette pour faire retentir l'alarme de guerre contre vous. Fils d'Israël, ne combattez pas contre YHWH, l'Elohîm de vos pères, car cela ne vous réussira pas.
13:13	Mais Yarobam fit une embuscade par un détour, et arriva derrière eux ; de sorte que les Israélites étaient en face de Yéhouda, qui avait l'embuscade par-derrière.
13:14	Ceux de Yéhouda se retournèrent et voici ils avaient la bataille par-devant et par-derrière. Alors ils crièrent à YHWH, et les prêtres sonnèrent des trompettes.

### Victoire de Yéhouda sur Israël

13:15	Les hommes de Yéhouda poussèrent un cri, et au cri de guerre des hommes de Yéhouda, YHWH frappa Yarobam et tout Israël devant Abiyah et Yéhouda.
13:16	Les fils d'Israël s'enfuirent devant ceux de Yéhouda, parce qu'Elohîm les livra entre leurs mains.
13:17	Abiyah et son peuple les frappèrent d'un grand coup, et 500 000 hommes sélectionnés d'Israël tombèrent blessés à mort.
13:18	Ainsi, les enfants d'Israël furent humiliés en ce temps-là. Les enfants de Yéhouda devinrent plus forts, parce qu'ils s'étaient appuyés sur YHWH, l'Elohîm de leurs pères.
13:19	Abiyah poursuivit Yarobam, et lui prit ces villes : Béth-El et les villes de son ressort, Yeshanah et les villes de son ressort, Éphron et les villes de son ressort.

### Mort de Yarobam<!--1 R. 14:19,20.-->

13:20	Yarobam ne détenait plus de force durant le temps d'Abiyah. Et YHWH le frappa, et il mourut.

### Les femmes et les fils d'Abiyah<!--1 R. 15:7-8.-->

13:21	Mais Abiyah se fortifia. Il prit 14 femmes, et engendra 22 fils et 16 filles.
13:22	Le reste des discours d'Abiyah, sa conduite et ses discours sont écrits dans le Midrash<!--« Étude », « exposition », « enregistrement », « histoire », « écrits de nature didactique ».--> du prophète Iddo.
13:23	Abiyah s'endormit avec ses pères, et on l'ensevelit dans la cité de David et Asa, son fils, régna à sa place. De son temps, le pays fut en repos pendant 10 ans.

## Chapitre 14

### Asa règne sur Yéhouda, il rétablit l'ordre de YHWH<!--1 R. 15:11.-->

14:1	Asa fit ce qui est bon et droit aux yeux de YHWH, son Elohîm.
14:2	Il ôta les autels étrangers et les hauts lieux. Il brisa les statues et mit en pièces les ashérim.
14:3	Et il dit à Yéhouda de chercher YHWH, l'Elohîm de leurs pères, et de pratiquer la torah et les commandements.
14:4	Il ôta de toutes les villes de Yéhouda les hauts lieux et les colonnes consacrés au soleil<!--De. 17:1-7.-->. Et le royaume fut en repos devant lui.
14:5	Il bâtit des villes fortes en Yéhouda, car le pays fut en repos. Et pendant ces années-là, il n'y eut pas de guerre contre lui, parce que YHWH lui donna du repos.
14:6	Et il dit à Yéhouda : Bâtissons ces villes, et entourons-les de murailles, de tours, de portes et de barres. Le pays est encore devant nous, parce que nous avons recherché YHWH, notre Elohîm. Nous l'avons recherché, et il nous a donné du repos de toutes parts. Ainsi, ils bâtirent et prospérèrent.

### Asa s'appuie sur YHWH et triomphe de Zérach<!--2 Ch. 16:1-10.-->

14:7	Or Asa avait dans son armée 300 000 hommes de Yéhouda, portant le grand bouclier et la lance, et 280 000 de Benyamin, portant le bouclier et tirant de l'arc, tous hommes vaillants et talentueux.
14:8	Mais Zérach, l'Éthiopien, sortit contre eux avec une armée de mille milliers et de 300 chars : il vint jusqu'à Maréshah.
14:9	Asa alla au-devant de lui, et ils se rangèrent en bataille dans la vallée de Tsephata, près de Maréshah.
14:10	Alors Asa cria à YHWH, son Elohîm, et dit : YHWH ! Toi seul peux nous secourir, que l'on soit nombreux ou sans force ! Aide-nous, YHWH, notre Elohîm ! Car nous nous appuyons sur toi, et nous sommes venus en ton Nom contre cette multitude. Tu es YHWH, notre Elohîm : Que l'homme ne prévale pas contre toi !
14:11	Et YHWH frappa les Éthiopiens devant Asa et devant Yéhouda, et les Éthiopiens s'enfuirent.
14:12	Asa et le peuple qui était avec lui les poursuivirent jusqu'à Guérar et les Éthiopiens tombèrent sans pouvoir sauver leur vie, car ils furent brisés devant YHWH et son armée. On emporta un très grand butin.
14:13	Ils frappèrent aussi toutes les villes autour de Guérar, car la terreur de YHWH était sur eux. Ils pillèrent toutes ces villes, car il s'y trouvait un grand butin.
14:14	Ils frappèrent aussi les tentes des troupeaux, et emmenèrent des brebis et des chameaux en abondance. Puis ils retournèrent à Yeroushalaim.

## Chapitre 15

### Azaryah le prophète avertit Asa

15:1	Alors l'Esprit d'Elohîm fut sur Azaryah, fils d'Oded.
15:2	Et il sortit au-devant d'Asa, et lui dit : Asa, et tout Yéhouda et Benyamin, écoutez-moi ! YHWH est avec vous quand vous êtes avec lui. Si vous le cherchez, vous le trouverez ; mais si vous l'abandonnez, il vous abandonnera.
15:3	Pendant de nombreux jours Israël a été sans vrai Elohîm, sans prêtre qui l'enseignait, et sans torah.
15:4	Mais dans leur détresse, ils sont revenus vers YHWH, l'Elohîm d'Israël. Ils l'ont cherché, et ils l'ont trouvé<!--Ps. 107:19-20.-->.
15:5	Dans ces temps-là, il n'y avait pas de sûreté pour ceux qui allaient et venaient, car il y avait de grands troubles parmi tous les habitants du pays.
15:6	Une nation était écrasée par une autre nation, et une ville par une autre ville, car Elohîm les agitait par toutes sortes d'angoisses.
15:7	Mais vous, fortifiez-vous, et que vos mains ne se relâchent pas, car il y a une récompense pour vos œuvres.

### Asa écoute les paroles d'Azaryah<!--1 R. 15:12-15.-->

15:8	Or dès qu'Asa eut entendu ces paroles et la prophétie d'Oded le prophète, il se fortifia et fit disparaître les abominations de tout le pays de Yéhouda et de Benyamin, et des villes qu'il avait prises dans les montagnes d'Éphraïm, et il restaura l'autel de YHWH qui était devant le portique de YHWH.
15:9	Puis il rassembla tout Yéhouda et Benyamin, et ceux d'Éphraïm, de Menashè et de Shim’ôn, qui habitaient avec eux. En effet, un grand nombre de gens d'Israël s'étaient ralliés en voyant que YHWH son Elohîm, était avec lui.
15:10	Ils se rassemblèrent donc à Yeroushalaim le troisième mois de la quinzième année du règne d'Asa.
15:11	Et ils sacrifièrent ce jour-là à YHWH 700 bœufs et 7 000 brebis, sur le butin qu'ils avaient amené.
15:12	Et ils rentrèrent dans l'alliance pour chercher YHWH, l'Elohîm de leurs pères, de tout leur cœur et de toute leur âme.
15:13	De sorte qu'on devait faire mourir quiconque ne chercherait pas YHWH, l'Elohîm d'Israël, petit ou grand, homme ou femme.
15:14	Et ils jurèrent à YHWH, à grande voix, avec des cris de joie, et au son des shofars et des cors.
15:15	Tout Yéhouda se réjouit de ce serment, parce qu'ils avaient juré de tout leur cœur et qu'ils avaient cherché YHWH de leur plein gré, et qu'ils l'avaient trouvé. Et YHWH leur donna du repos de toutes parts.
15:16	Le roi Asa retira même à Ma'akah, sa mère, la dignité de reine-mère, parce qu'elle avait fait une chose horrible pour Asherah. Asa abattit la chose horrible, l'écrasa et la brûla près du torrent de Cédron.
15:17	Mais les hauts lieux ne furent pas ôtés du milieu d'Israël. Néanmoins, le cœur d'Asa fut intègre tout le long de ses jours.
15:18	Il remit dans la maison d'Elohîm les choses que son père avait consacrées, avec ce qu'il avait lui-même consacré, l'argent, l'or et les ustensiles.
15:19	Et il n'y eut pas de guerre jusqu'à la trente-cinquième année du règne d'Asa.

## Chapitre 16

### Alliance d'Asa et du roi de Syrie contre Israël<!--1 R. 15:16-22 ; cp. 1 R. 15:27, 16:7.-->

16:1	La trente-sixième année du règne d'Asa, Baesha, roi d'Israël, monta contre Yéhouda, et il bâtit Ramah, pour empêcher quiconque de sortir et d'entrer vers Asa, roi de Yéhouda.
16:2	Alors Asa sortit de l'argent et de l'or des trésors de la maison de YHWH et de la maison royale, et il envoya dire à Ben-Hadad, roi de Syrie, qui habitait à Damas :
16:3	Il y a alliance entre nous, et entre mon père et ton père. Voici, je t'envoie de l'argent et de l'or. Va, romps l'alliance que tu as avec Baesha, roi d'Israël, afin qu'il s'éloigne de moi.
16:4	Ben-Hadad écouta le roi Asa, et il envoya les chefs de son armée contre les villes d'Israël, et ils frappèrent Iyôn, Dan, Abel-Maïm, et tous les magasins des villes de Nephthali.
16:5	Et aussitôt que Baesha l'apprit, il cessa de bâtir Ramah et suspendit ses travaux.
16:6	Alors le roi Asa prit tout Yéhouda pour emporter les pierres et les bois avec lesquels Baesha bâtissait Ramah. On s'en servit pour bâtir Guéba et Mitspa.

### Hanani condamne l'alliance d'Asa

16:7	En ce temps-là, Hanani le voyant, vint vers Asa, roi de Yéhouda, et lui dit : Parce que tu t'es appuyé sur le roi de Syrie, et que tu ne t'es pas appuyé sur YHWH, ton Elohîm, l'armée du roi de Syrie a échappé de ta main.
16:8	Les Éthiopiens et les Libyens n'étaient-ils pas une grande armée, ayant des chars et une multitude de cavaliers ? Mais parce que tu t'étais appuyé sur YHWH, il les livra entre tes mains.
16:9	Car les yeux de YHWH parcourent toute la Terre, pour soutenir ceux dont le cœur est tout entier à lui. Tu as agi follement dans cette affaire, car désormais tu auras des guerres.
16:10	Asa fut irrité contre le voyant, et le mit dans une maison, en prison, car il était indigné contre lui à ce sujet. Asa opprima aussi, en ce temps-là, quelques-uns du peuple.

### Mort d'Asa<!--1 R. 15:23-24.-->

16:11	Or voici, les actions d'Asa, les premières et les dernières, sont écrites dans le livre des rois de Yéhouda et d'Israël.
16:12	Asa fut malade des pieds la trente-neuvième année de son règne, et sa maladie fut très grave. Toutefois, il ne chercha pas YHWH dans sa maladie, mais les médecins.
16:13	Puis Asa s'endormit avec ses pères, et il mourut la quarante et unième année de son règne.
16:14	On l'ensevelit dans le sépulcre qu'il s'était creusé dans la cité de David. On le coucha dans un lit qui était rempli de parfums et d'aromates, composés par le travail d'un parfumeur et l'on en brûla pour lui une quantité considérable.

## Chapitre 17

### Yehoshaphat règne sur Yéhouda, il recherche YHWH<!--1 R. 15:24.-->

17:1	Yehoshaphat, son fils, régna à sa place et se fortifia contre Israël.
17:2	Il mit des troupes dans toutes les villes fortes de Yéhouda, et mit des garnisons dans le pays de Yéhouda, et dans les villes d'Éphraïm qu'Asa, son père, avait prises.
17:3	YHWH fut avec Yehoshaphat, parce qu'il marcha dans les premières voies de David, son père, et qu'il ne chercha pas les Baalim.
17:4	Mais il chercha l'Elohîm de son père, et il marcha dans ses commandements, et non pas selon ce que faisait Israël.
17:5	YHWH affermit donc le royaume entre ses mains ; et tout Yéhouda apportait des présents à Yehoshaphat, et il eut en abondance des richesses et de la gloire.
17:6	Son cœur grandit dans les voies de YHWH, et il ôta encore de Yéhouda les hauts lieux et les ashérim.
17:7	La troisième année de son règne, il envoya ses chefs : Ben-Haïl, Obadyah, Zekaryah, Netanél et Miykayah, pour enseigner dans les villes de Yéhouda,
17:8	et avec eux les Lévites Shema’yah, Nethanyah, Zebadyah, Asaël, Shemiramoth, Yehonathan, Adoniyah, Tobiyah et Tob-Adoniyah, Lévites, et avec eux Éliyshama et Yehoram, les prêtres.
17:9	Ils enseignèrent dans Yéhouda, ayant avec eux le livre de la torah de YHWH. Ils firent le tour de toutes les villes de Yéhouda, et enseignèrent parmi le peuple.

### Affermissement du règne de Yehoshaphat

17:10	La terreur de YHWH fut sur tous les royaumes des pays qui entouraient Yéhouda, et ils ne firent pas la guerre à Yehoshaphat.
17:11	On apporta aussi à Yehoshaphat des présents de la part des Philistins, et un impôt en argent. Les Arabes lui amenèrent aussi du bétail, 7 700 béliers et 7 700 boucs.
17:12	Ainsi Yehoshaphat s'élevait jusqu'au plus haut degré de gloire. Et il bâtit en Yéhouda des châteaux et des villes pour servir de magasins.
17:13	Il fit de grands travaux dans les villes de Yéhouda, et il avait à Yeroushalaim de vaillants hommes et talentueux comme hommes de guerre.
17:14	Voici leur dénombrement, selon les maisons de leurs pères. Les chefs de milliers de Yéhouda furent Adna le chef, avec 300 000 hommes vaillants et talentueux.
17:15	Et après lui, Yehohanan le chef, avec 280 000 hommes.
17:16	À ses côtés, Amacyah, fils de Zicri, qui s'était volontairement offert à YHWH, avec 200 000 hommes vaillants et talentueux.
17:17	De Benyamin, Éliada, homme vaillant et talentueux, avec 200 000 hommes, armés d'arcs et de boucliers,
17:18	à côté de lui Yehozabad, avec 180 000 hommes équipés pour le combat.
17:19	Tels sont ceux qui étaient au service du roi, outre ceux que le roi avait placés dans toutes les villes fortes de Yéhouda.

## Chapitre 18

### Yehoshaphat s'allie à Achab contre les Syriens<!--1 R. 22:2-4.-->

18:1	Or Yehoshaphat, ayant beaucoup de richesses et de gloire, devint gendre d'Achab.
18:2	Et au bout de quelques années, il descendit vers Achab, à Samarie. Achab tua pour lui, et pour le peuple qui était avec lui, un grand nombre de brebis et de bœufs, et l'incita à monter contre Ramoth en Galaad.
18:3	Achab, roi d'Israël, dit à Yehoshaphat, roi de Yéhouda : Viendras-tu avec moi contre Ramoth en Galaad ? Et il lui répondit : Compte sur moi comme sur toi, et sur mon peuple comme sur ton peuple, nous irons avec toi à la guerre.

### Les prophètes de mensonge encouragent Achab<!--1 R. 22:5-12.-->

18:4	Puis Yehoshaphat dit au roi d'Israël : Consulte aujourd'hui, je te prie, la parole de YHWH.
18:5	Le roi d'Israël rassembla les prophètes, au nombre de 400, et leur dit : Irons-nous à la guerre contre Ramoth en Galaad, ou dois-je y renoncer ? Ils répondirent : Monte et Elohîm la livrera entre les mains du roi.
18:6	Mais Yehoshaphat dit : N'y a-t-il pas encore ici quelque prophète de YHWH afin que nous l'interrogions ?
18:7	Le roi d'Israël dit à Yehoshaphat : Il y a encore un homme par qui l'on pourrait consulter YHWH, mais je le hais parce qu'il ne me prophétise rien de bon, mais toujours du mal : c'est Miykayeh<!--Michée.-->, fils de Yimla. Yehoshaphat dit : Que le roi ne parle pas ainsi !
18:8	Alors le roi d'Israël appela un eunuque, et dit : Fais promptement venir Miykah, fils de Yimla.
18:9	Or le roi d'Israël et Yehoshaphat, roi de Yéhouda, étaient assis, chacun sur son trône, revêtus de leurs habits, et ils étaient assis sur la place qui se trouve à l'entrée de la porte de Samarie ; et tous les prophètes prophétisaient en leur présence.
18:10	Alors Tsidqiyah, fils de Kenaana, s'étant fait des cornes de fer, dit : Ainsi parle YHWH : Avec ces cornes tu heurteras les Syriens jusqu'à les détruire.
18:11	Tous les prophètes prophétisaient de même, en disant : Monte à Ramoth en Galaad ! Tu connaîtras le succès et YHWH la livrera entre les mains du roi.

### Miykayeh (Michée) annonce la défaite et la mort d'Achab<!--1 R. 22:13-28, 22:29-40.-->

18:12	Or le messager qui était allé appeler Miykayeh, lui parla et lui dit : Voici les paroles des prophètes, d'une seule bouche elles annoncent ce qui est bon au roi. Je t'en prie, que ta parole soit comme celle de l'un d'eux ! Déclare ce qui est bon !
18:13	Mais Miykayeh répondit : YHWH est vivant ! Je déclarerai ce que mon Elohîm dira.
18:14	Il vint donc vers le roi, et le roi lui dit : Miykah, devons-nous aller à la guerre contre Ramoth en Galaad, ou bien nous en abstenir ? Et il répondit : Montez-y ! Vous connaîtrez le succès et ils seront livrés entre vos mains.
18:15	Et le roi lui dit : Combien de fois me faudra-t-il te faire jurer de ne me dire que la vérité au Nom de YHWH ?
18:16	Et il répondit : J'ai vu tout Israël dispersé sur les montagnes comme un troupeau de brebis qui n'a pas de berger. Et YHWH a dit : Ces gens n'ont pas de seigneur. Que chacun retourne en paix dans sa maison !
18:17	Alors le roi d'Israël dit à Yehoshaphat : Ne t'ai-je pas dit qu'il ne prophétise rien de bon quand il s'agit de moi, mais seulement du mal ?
18:18	Et Miykayeh dit : Écoute la parole de YHWH ! J'ai vu YHWH assis sur son trône, et toute l'armée des cieux se tenant à sa droite et à sa gauche.
18:19	Et YHWH dit : Qui est-ce qui séduira Achab, roi d'Israël, afin qu'il monte et qu'il tombe à Ramoth en Galaad ? Et celui-ci dit ainsi, et celui-là dit ainsi.
18:20	Alors un esprit s'avança et se tint devant YHWH, et dit : Moi, je le séduirai. YHWH lui dit : Comment ?
18:21	Il répondit : Je sortirai, dit-il, et je serai un esprit de mensonge<!--Achab a été frappé de l'esprit d'égarement (2 Th. 2:9-11). Voir commentaires en Ge. 6:3 ; Mt. 12:31.--> dans la bouche de tous ses prophètes. Et YHWH dit : Tu le séduiras et tu le vaincras aussi. Sors et fais comme tu l'as dit !
18:22	Maintenant voici, YHWH a mis un esprit de mensonge dans la bouche de tes prophètes que voici, et YHWH a prononcé du mal contre toi.
18:23	Alors Tsidqiyah, fils de Kenaana, s'étant approché, frappa Miykayeh sur la joue et dit : Par quel chemin l'Esprit de YHWH est-il sorti de moi pour te parler ?
18:24	Et Miykayeh répondit : Voici, tu le verras au jour où tu iras de chambre en chambre pour te cacher !
18:25	Alors le roi d'Israël dit : Prenez Miykayeh, et emmenez-le vers Amon, chef de la ville, et vers Yoash, fils du roi.
18:26	Et vous direz : Ainsi parle le roi : Mettez cet homme en maison d'arrêt, et nourrissez-le du pain d'oppression et de l'eau d'oppression, jusqu'à ce que je revienne en paix.
18:27	Et Miykayeh dit : Si tu reviens, si tu reviens en paix, YHWH n'a pas parlé par moi. Et il dit : Entendez cela peuples, vous tous qui êtes ici !
18:28	Le roi d'Israël monta donc avec Yehoshaphat, roi de Yéhouda, à Ramoth en Galaad.
18:29	Le roi d'Israël dit à Yehoshaphat : Je me déguiserai pour aller à la guerre, mais toi, revêts-toi de tes habits. Ainsi le roi d'Israël se déguisa, et ils allèrent à la guerre.
18:30	Or le roi des Syriens avait donné cet ordre aux chefs de ses chars, disant : Vous ne combattrez ni petit ni grand, mais seulement le roi d'Israël.
18:31	Les chefs des chars aperçurent Yehoshaphat, et dirent : C'est le roi d'Israël ! Et ils se tournèrent vers lui pour le combattre, mais Yehoshaphat poussa un cri, et YHWH le secourut, et Elohîm les éloigna de lui.
18:32	Quand les chefs des chars virent que ce n'était pas le roi d'Israël, ils se détournèrent de lui.
18:33	Alors quelqu'un tira de son arc au hasard et frappa le roi d'Israël entre les jointures de la cuirasse. Le roi dit à son conducteur de char : Tourne-toi, et sors-moi du camp, car je suis blessé.
18:34	La guerre fut si violente ce jour-là que le roi d'Israël dut être maintenu debout sur son char face aux Syriens jusqu'au soir, et il mourut vers le coucher du soleil.

## Chapitre 19

### Yehuw dénonce l'alliance de Yehoshaphat avec Achab

19:1	Yehoshaphat roi de Yéhouda, revint en paix dans sa maison, à Yeroushalaim.
19:2	Mais Yehuw, fils de Hanani, le voyant, sortit au-devant du roi Yehoshaphat, et lui dit : Faut-il donner du secours au méchant, ou aimer ceux qui haïssent YHWH ? À cause de cela, YHWH est irrité contre toi.
19:3	Mais il s'est trouvé de bonnes choses en toi, puisque tu as ôté du pays les ashérim, et tu as appliqué ton cœur à rechercher Elohîm.
19:4	Yehoshaphat demeura à Yeroushalaim. Puis, il ressortit de nouveau parmi le peuple, depuis Beer-Shéba jusqu'à la montagne d'Éphraïm, et il les ramena à YHWH, l'Elohîm de leurs pères.

### Yehoshaphat organise la justice

19:5	Il établit aussi des juges dans le pays, dans toutes les villes fortes de Yéhouda, de ville en ville.
19:6	Et il dit aux juges : Veillez sur ce que vous ferez, car vous n'exercez pas la justice de la part d'un être humain, mais de la part de YHWH, qui sera avec vous quand vous prononcerez les jugements.
19:7	Maintenant, que la crainte de YHWH soit sur vous. Prenez garde à ce que vous ferez, car il n'y a chez YHWH, notre Elohîm, ni injustice, ni égard à l'apparence des personnes, ni acceptation de pot-de-vin.
19:8	Yehoshaphat établit aussi à Yeroushalaim des Lévites, des prêtres, et des chefs des pères d'Israël, pour le jugement de YHWH et pour les contestations, car on revenait à Yeroushalaim.
19:9	Il leur donna des ordres, en disant : Vous agirez ainsi dans la crainte de YHWH, avec fidélité et avec intégrité de cœur.
19:10	Dans toute contestation qui vous sera soumise par vos frères qui habitent dans leurs villes, qu'il s'agisse d'un meurtre, d'une torah, d'un commandement, d'un statut ou d'une ordonnance, vous les instruirez, afin qu'ils ne se rendent pas coupables envers YHWH, et que sa colère ne vienne pas sur vous et sur vos frères. Vous agirez ainsi afin de ne pas être coupables.
19:11	Et voici, Amaryah, le grand-prêtre, sera au-dessus de vous pour toutes les affaires de YHWH, et Zebadyah, fils de Yishmael, prince de la maison de Yéhouda, pour toutes les affaires du roi. Pour secrétaires, vous avez devant vous les Lévites. Fortifiez-vous et agissez, et que YHWH soit avec le bon !

## Chapitre 20

### Menaces des ennemis de Yéhouda, prière de Yehoshaphat

20:1	Et il arriva, après ces choses, que les fils de Moab et les fils d'Ammon, et avec eux les Maonites, vinrent contre Yehoshaphat pour lui faire la guerre.
20:2	On vint le rapporter à Yehoshaphat, en disant : Il vient contre toi une grande multitude depuis l'autre côté de la mer, depuis la Syrie, les voici à Hatsatson-Thamar, qui est En-Guédi.
20:3	Alors Yehoshaphat eut peur et décida de chercher YHWH. Il publia un jeûne pour tout Yéhouda.
20:4	Et Yéhouda se rassembla pour chercher YHWH, et l'on vint même de toutes les villes de Yéhouda pour chercher YHWH.
20:5	Et Yehoshaphat se tint au milieu de l'assemblée de Yéhouda et de Yeroushalaim, dans la maison de YHWH, devant le nouveau parvis.
20:6	Il dit : YHWH, Elohîm de nos pères ! N'es-tu pas Elohîm dans les cieux, toi qui domines sur tous les royaumes des nations ? Ne tiens-tu pas dans ta main la force et la puissance, de sorte que personne ne peut résister ?
20:7	N'est-ce pas toi, ô notre Elohîm, qui as dépossédé les habitants de ce pays devant ton peuple d'Israël, et qui l'as donné pour toujours à la postérité d'Abraham, qui t'aimait ?
20:8	Ils y ont habité et t'y ont bâti un sanctuaire pour ton Nom, en disant :
20:9	S'il nous arrive quelque malheur, l'épée, le jugement, la peste, ou la famine, nous nous tiendrons devant cette maison, et en ta présence, car ton Nom est dans cette maison. Nous crierons à toi dans notre détresse, et tu exauceras et tu délivreras !
20:10	Maintenant, voici les enfants d'Ammon et de Moab, et ceux de la montagne de Séir, chez lesquels tu ne permis pas à Israël d'entrer quand il venait du pays d'Égypte, car il se détourna d'eux, et ne les détruisit pas.
20:11	Voici, pour nous récompenser, ils viennent nous chasser de ton héritage, dont tu nous as fait hériter.
20:12	Ô notre Elohîm ! Ne seras-tu pas juge contre eux ? Car nous sommes sans force devant cette grande multitude qui vient contre nous, et nous ne savons que faire, mais nos yeux sont sur toi.
20:13	Or tout Yéhouda se tenait devant YHWH, même avec leurs petits enfants, leurs femmes et leurs fils.

### YHWH répond à Yehoshaphat

20:14	Alors l'Esprit de YHWH saisit au milieu de l'assemblée Yachaziy'el, fils de Zekaryah, fils de Benayah, fils de Yéiël, fils de Mattanyah, Lévite, d'entre les fils d'Asaph,
20:15	et il dit : Soyez attentifs, tout Yéhouda et habitants de Yeroushalaim, et toi, roi Yehoshaphat ! Ainsi parle YHWH : N'ayez pas peur et ne soyez pas effrayés en face de cette grande multitude, car ce ne sera pas à vous de combattre, mais à Elohîm.
20:16	Descendez demain vers eux ; les voici qui montent par la montée de Tsits, et vous les trouverez à l'extrémité de la vallée, en face du désert de Yerouel.
20:17	Ce ne sera pas à vous de combattre dans cette bataille. Présentez-vous, tenez-vous là et voyez la délivrance que YHWH va vous donner. Yéhouda et Yeroushalaim, n'ayez pas peur et ne soyez pas effrayés ! Demain, sortez au-devant d'eux et YHWH sera avec vous.
20:18	Alors Yehoshaphat s'inclina le visage contre terre, et tout Yéhouda et les habitants de Yeroushalaim tombèrent devant YHWH pour se prosterner devant YHWH.
20:19	Et les Lévites, d'entre les fils des Kehathites et d'entre les fils des Koréites, se levèrent pour célébrer YHWH, l'Elohîm d'Israël, d'une voix grande et haute.

### YHWH délivre Yéhouda des armées ennemies

20:20	Ils se levèrent de bonne heure le matin, et sortirent vers le désert de Tekoa. Et comme ils sortaient, Yehoshaphat se tint debout et dit : Écoutez-moi Yéhouda et vous, habitants de Yeroushalaim ! Croyez en YHWH, votre Elohîm et vous serez soutenus. Croyez en ses prophètes et vous réussirez.
20:21	Puis, ayant consulté le peuple, il établit des chanteurs de YHWH qui célébraient sa sainte majesté tout en marchant devant l'armée et disaient : Louez YHWH, car sa miséricorde dure à toujours<!--Ps. 136.--> !
20:22	Et au moment où ils commencèrent le chant et la louange, YHWH mit des embuscades contre les fils d'Ammon, de Moab, et ceux de la montagne de Séir, qui venaient contre Yéhouda. Et ils furent battus.
20:23	Les fils d'Ammon et de Moab se levèrent contre les habitants de la montagne de Séir pour les dévouer par interdit et les exterminer. Quand ils en eurent fini avec les habitants de Séir, ils s'aidèrent l'un l'autre à se détruire mutuellement.
20:24	Et quand Yéhouda fut arrivé sur la hauteur d'où l'on voit le désert, ils regardèrent vers cette multitude, et voici, c'étaient des cadavres gisant à terre, et personne n'avait échappé.
20:25	Ainsi Yehoshaphat et son peuple vinrent pour piller leurs dépouilles, et ils trouvèrent parmi les cadavres des biens en abondance, et des objets précieux. Ils en saisirent tant, qu'ils ne pouvaient tout porter. Ils pillèrent le butin pendant 3 jours, car il était considérable.
20:26	Le quatrième jour, ils se rassemblèrent dans la vallée de Beraca, car c'est là qu'ils bénirent YHWH. C'est pourquoi on a appelé ce lieu, jusqu'à ce jour, la vallée de Beraca.
20:27	Et tous les hommes de Yéhouda et de Yeroushalaim, et Yehoshaphat à leur tête, s'en retournèrent, revenant à Yeroushalaim avec joie, car YHWH les avait réjouis au sujet de leurs ennemis.
20:28	Ils entrèrent donc à Yeroushalaim, dans la maison de YHWH, avec des luths, des harpes et des trompettes.
20:29	Et la crainte d'Elohîm fut sur tous les royaumes des autres pays, lorsqu'ils apprirent que YHWH avait combattu contre les ennemis d'Israël.
20:30	Ainsi le royaume de Yehoshaphat fut tranquille, et son Elohîm lui donna du repos de toutes parts.

### Règne de Yehoshaphat, son alliance coupable<!--1 R. 22:41-49.-->

20:31	Yehoshaphat régna donc sur Yéhouda. Il était âgé de 35 ans quand il devint roi, et il régna 25 ans à Yeroushalaim. Sa mère s'appelait Azouba, fille de Shilchi.
20:32	Il marcha sur la voie de son père Asa. Il ne s'en détourna pas, faisant ce qui est droit aux yeux de YHWH.
20:33	Seulement les hauts lieux ne furent pas ôtés, et le peuple n'avait pas encore le cœur fermement attaché à Elohîm de ses pères.
20:34	Or le reste des actions de Yehoshaphat, les premières et les dernières, voici, elles sont écrites dans les mémoires de Yehuw, fils de Hanani, insérées dans le livre des rois d'Israël.
20:35	Après cela, Yehoshaphat, roi de Yéhouda, s'associa avec Achazyah, roi d'Israël, dont la conduite était impie.
20:36	Il s'associa avec lui pour faire des navires, afin d'aller à Tarsis. Ils fabriquèrent des navires à Etsyôn-Guéber.
20:37	Alors Éliézer, fils de Dodavah, de Maréshah, prophétisa contre Yehoshaphat, en disant : Parce que tu t'es associé avec Achazyah, YHWH a détruit ton œuvre. Et les navires furent brisés, et ne purent aller à Tarsis.

## Chapitre 21

### Yehoram (Yoram) règne sur Yéhouda<!--1 R. 22:50 ; 2 R. 8:16-19.-->

21:1	Puis Yehoshaphat s'endormit avec ses pères, et il fut enseveli avec eux dans la cité de David. Et Yehoram, son fils, régna à sa place.
21:2	Il avait des frères, fils de Yehoshaphat : Azaryah, Yechiy'el, Zekaryah, Azaryah, Miyka'el et Shephatyah. Tous ceux-là étaient fils de Yehoshaphat, roi d'Israël.
21:3	Leur père leur avait fait de grands dons en argent, en or et de choses précieuses, avec des villes fortes en Yéhouda. Mais il avait donné le royaume à Yehoram, parce qu'il était le premier-né.
21:4	Quand Yehoram fut élevé sur le royaume de son père, et s'y fut fortifié, il tua avec l'épée tous ses frères, et quelques-uns aussi des chefs d'Israël.
21:5	Yehoram était âgé de 32 ans quand il devint roi, et il régna 8 ans à Yeroushalaim.
21:6	Il marcha dans la voie des rois d'Israël, comme avait fait la maison d'Achab, car la fille d'Achab était sa femme, et il fit ce qui est mal aux yeux de YHWH.
21:7	Toutefois, YHWH, à cause de l'alliance qu'il avait traitée avec David, ne voulut pas détruire la maison de David, selon qu'il avait dit qu'il lui donnerait une lampe, à lui et à ses fils, pour toujours.

### Rébellion d'Édom et de Libnah<!--1 R. 8:20-23.-->

21:8	De son temps, Édom se rebella contre le pouvoir de Yéhouda et se donna un roi.
21:9	Yehoram se mit donc en marche avec ses chefs et tous ses chars. S'étant levé de nuit, il battit les Édomites qui l'entouraient, et tous les chefs des chars.
21:10	Néanmoins, Édom se rebella contre le pouvoir de Yéhouda jusqu'à ce jour. En ce même temps, Libnah se rebella aussi contre son pouvoir, parce qu'il avait abandonné YHWH, l'Elohîm de ses pères.
21:11	Il fit aussi des hauts lieux dans les montagnes de Yéhouda. Il poussa les habitants de Yeroushalaim à la prostitution, et il y entraîna ceux de Yéhouda.

### Eliyah prononce un jugement sur Yehoram (Yoram)

21:12	Alors il lui vint un écrit de la part d'Eliyah, le prophète, disant : Ainsi parle YHWH, l'Elohîm de David, ton père : Parce que tu n'as pas suivi le chemin de Yehoshaphat, ton père, ni celui d'Asa, roi de Yéhouda,
21:13	mais que tu as suivi les voies des rois d'Israël, et que tu as poussé à la prostitution Yéhouda et les habitants de Yeroushalaim, comme s'est prostituée la maison d'Achab, et que tu as tué tes frères, meilleurs que toi, la maison même de ton père ;
21:14	voici, YHWH frappera d'une grande plaie ton peuple, tes fils, tes femmes et tous tes biens.
21:15	Et toi, il te frappera d'une grande maladie, d'une maladie d'intestins qui s'aggravera de jour en jour, au point que tes intestins sortiront de toi à cause de la maladie.

### YHWH excite les Philistins et les Arabes contre Yehoram (Yoram)

21:16	YHWH souleva contre Yehoram l'esprit des Philistins et des Arabes qui étaient à côté des Éthiopiens.
21:17	Ils montèrent contre Yéhouda et le mirent en morceaux. Ils emmenèrent captifs tous les biens qui se trouvaient dans la maison du roi, ainsi que ses fils et ses femmes, de sorte qu'il ne lui resta d'autre fils que Yehoachaz, le plus jeune de ses fils.

### Mort de Yehoram (Yoram)

21:18	Après tout cela, YHWH frappa ses entrailles d'une maladie sans remède.
21:19	Et il arriva, de jour en jour, et au temps où la seconde année tirait à sa fin, que ses entrailles sortirent par la force de son mal et il mourut dans ses mauvaises maladies. Son peuple ne brûla pas sur lui de parfums, comme il l'avait fait pour ses pères.
21:20	Il était âgé de 32 ans quand il devint roi, et il régna 8 ans à Yeroushalaim. Il s'en alla sans être regretté, et on l'ensevelit dans la cité de David, mais non dans les sépulcres des rois.

## Chapitre 22

### Achazyah règne sur Yéhouda<!--2 R. 8:24-29.-->

22:1	Les habitants de Yeroushalaim firent régner à sa place Achazyah, le plus jeune de ses fils, parce que les troupes qui étaient venues au camp avec les Arabes avaient tué tous les plus âgés. C'est ainsi qu'Achazyah, fils de Yehoram, roi de Yéhouda, régna.
22:2	Achazyah était âgé de 42 ans quand il devint roi, et il régna un an à Yeroushalaim. Le nom de sa mère était Athalyah, fille d'Omri.
22:3	Il marcha lui aussi sur les voies de la maison d'Achab, car sa mère devint sa conseillère pour agir méchamment.
22:4	Il fit donc ce qui est mal aux yeux de YHWH, comme la maison d'Achab, parce qu'ils furent ses conseillers après la mort de son père, pour sa ruine.

### Achazyah livré aux mains de Yehuw<!--2 R. 8:28-29, 9:1-30.-->

22:5	Conduit par leurs conseils, il alla avec Yehoram, fils d'Achab, roi d'Israël, à la guerre à Ramoth en Galaad, contre Hazaël, roi de Syrie. Et les Syriens frappèrent Yoram,
22:6	qui s'en retourna à Yizre`e'l, pour guérir des blessures que les Syriens lui avaient faites à Ramah, lorsqu'il faisait la guerre contre Hazaël, roi de Syrie. Azaryah, fils de Yehoram, roi de Yéhouda, descendit pour voir Yehoram, le fils d'Achab, à Yizre`e'l, parce qu'il était malade.
22:7	Et ce fut pour son entière ruine, qui procédait d'Elohîm, qu'Achazyah vint auprès de Yoram. En effet, quand il fut arrivé, il sortit avec Yehoram pour aller au-devant de Yehuw, fils de Nimshi, que YHWH avait oint pour retrancher la maison d'Achab.
22:8	Et comme Yehuw faisait justice de la maison d'Achab<!--2 R. 10:12-30.-->, il trouva les chefs de Yéhouda et les fils des frères d'Achazyah, qui servaient Achazyah, et il les tua.
22:9	Il chercha ensuite Achazyah, qui s'était caché en Samarie. On le prit, et on l'amena vers Yehuw qui le fit mourir. Puis on l'ensevelit, car on dit : C'est le fils de Yehoshaphat, qui cherchait YHWH de tout son cœur. Il ne resta personne de la maison d'Achazyah pour détenir le pouvoir du royaume.

### Yoash échappe au massacre de sa famille<!--2 R. 11:1-3.-->

22:10	Or Athalyah, mère d'Achazyah, voyant que son fils était mort, se leva et fit périr toute la race royale de la maison de Yéhouda.
22:11	Mais Yehoshab`ath, fille du roi Yehoram, prit Yoash, fils d'Achazyah, en le dérobant d'entre les fils du roi qu'on faisait mourir. Elle le mit avec sa nourrice dans la chambre des lits. Ainsi Yehoshab`ath, fille du roi Yehoram et femme de Yehoyada, le prêtre, étant la sœur d'Achazyah, le cacha de la vue d'Athalyah, qui ne put le faire mourir.
22:12	Il fut ainsi caché avec eux dans la maison d'Elohîm 6 ans, tandis qu'Athalyah régnait sur le pays.

## Chapitre 23

### Yoash devient roi grâce à Yehoyada<!--2 R. 11:4-12.-->

23:1	Mais la septième année, Yehoyada prit courage et traita alliance avec les chefs de centaines, Azaryah, fils de Yeroham, Yishmael, fils de Yehohanan, Azaryah, fils d'Obed, Ma`aseyah, fils d'Adayah, et Élishaphath, fils de Zicri.
23:2	Ils firent le tour de Yéhouda pour rassembler de toutes les villes de Yéhouda les Lévites et les chefs des pères d'Israël, puis ils vinrent à Yeroushalaim.
23:3	Et toute cette assemblée traita alliance avec le roi dans la maison d'Elohîm. Yehoyada leur dit : Voici, c'est le fils du roi qui régnera, selon la parole de YHWH au sujet des fils de David.
23:4	Vous ferez donc ceci : le tiers qui parmi vous entre en service au shabbat, prêtres et Lévites, fera la garde des seuils.
23:5	Un autre tiers se tiendra dans la maison du roi, et un tiers à la porte de Yesod. Tout le peuple sera dans les parvis de la maison de YHWH.
23:6	Que personne n'entre dans la maison de YHWH, sauf les prêtres et les Lévites de service : ils entreront, car ils sont sanctifiés. Tout le reste du peuple gardera les ordres de YHWH.
23:7	Les Lévites environneront le roi de toutes parts, tenant chacun leurs armes à la main, et donneront la mort à quiconque voudra entrer dans la maison. Vous serez avec le roi quand il entrera et quand il sortira.
23:8	Les Lévites et tout Yéhouda firent tout ce que Yehoyada, le prêtre, avait ordonné. Ils prirent chacun leurs gens, tant ceux qui entraient en service que ceux qui en sortaient au shabbat, car Yehoyada, le prêtre, n'avait exempté aucune classe.
23:9	Et Yehoyada, le prêtre, donna aux chefs de centaines les lances, les grands et les petits boucliers qui provenaient du roi David, et qui étaient dans la maison d'Elohîm.
23:10	Puis il rangea tout le peuple autour du roi, chacun tenant ses armes à la main, du côté droit du temple jusqu'au côté gauche de la maison, près de l'autel et de la maison.
23:11	Alors ils firent sortir le fils du roi, et mirent sur lui la couronne et le témoignage. Ils l'établirent roi, et Yehoyada et ses fils l'oignirent et dirent : Vive le roi !

### Mort d'Athalyah<!--2 R. 11:13-16.-->

23:12	Mais Athalyah, entendant le bruit du peuple qui courait et célébrait le roi, vint vers le peuple, dans la maison de YHWH.
23:13	Elle regarda, et voici, le roi se tenait près de la colonne, à l'entrée ; les chefs et les trompettes étaient près du roi. Tout le peuple du pays était dans la joie, et l'on sonnait des trompettes ; les chanteurs, avec des instruments de musique, dirigeaient les chants de louanges. Alors Athalyah déchira ses vêtements et dit : Conspiration ! Conspiration !
23:14	Le prêtre Yehoyada fit sortir les chefs de centaines qui étaient à la tête de l'armée, et leur dit : Faites-la sortir hors des rangs, et que celui qui la suivra soit mis à mort par l'épée ! Car le prêtre avait dit : Ne la mettez pas à mort dans la maison de YHWH.
23:15	Ils mirent donc la main sur elle pour la faire entrer dans la maison du roi, par l'entrée de la porte des chevaux. C'est là qu'ils la mirent à mort.

### Yehoyada fait asseoir Yoash sur le trône de Yéhouda<!--2 R. 11:17-20.-->

23:16	Puis Yehoyada traita, avec tout le peuple et le roi, une alliance pour être le peuple de YHWH.
23:17	Et tout le peuple entra dans la maison de Baal pour la détruire. Ils brisèrent ses autels et ses images et ils tuèrent devant les autels Matthan, prêtre de Baal.
23:18	Yehoyada remit aussi les fonctions de la maison de YHWH entre les mains des prêtres, des Lévites, comme David les avait répartis dans la maison de YHWH, afin qu'ils élèvent des holocaustes à YHWH, comme cela est écrit dans la torah de Moshè, avec joie et avec des chants, selon les ordonnances de David.
23:19	Il établit aussi les portiers aux portes de la maison de YHWH, afin qu'aucune personne impure de quelque manière que ce soit ne puisse y entrer.
23:20	Il prit les chefs de centaines, hommes considérés, qui avaient de l'autorité parmi le peuple, et tout le peuple du pays. Il fit descendre le roi, de la maison de YHWH à la maison du roi, en entrant par la porte supérieure ; et ils firent asseoir le roi sur le trône royal.
23:21	Alors tout le peuple du pays se réjouit, et la ville fut tranquille, bien qu'on eût mis à mort Athalyah par l'épée.

## Chapitre 24

### Yoash règne sur Yéhouda ; ses travaux sur le temple<!--2 R. 11:21-12:8.-->

24:1	Yoash était âgé de 7 ans quand il devint roi, et il régna 40 ans à Yeroushalaim. Le nom de sa mère était Tsibyah, de Beer-Shéba.
24:2	Yoash fit ce qui est droit aux yeux de YHWH, pendant toute la vie de Yehoyada, le prêtre.
24:3	Et Yehoyada prit pour lui deux femmes, et il engendra des fils et des filles.
24:4	Après cela Yoash eut la pensée de restaurer la maison de YHWH.
24:5	Il rassembla donc les prêtres et les Lévites, et leur dit : Allez vers les villes de Yéhouda et recueillez de l'argent dans tout Israël, d'année en année, autant que cela sera nécessaire pour réparer la maison de votre Elohîm. Et hâtez cette affaire ! Mais les Lévites ne se hâtèrent pas.
24:6	Alors le roi appela Yehoyada, leur chef, et lui dit : Pourquoi n'as-tu pas veillé à ce que les Lévites aient apporté de Yéhouda et de Yeroushalaim, l'impôt sur l'assemblée d'Israël, selon Moshè, serviteur de YHWH, pour la tente du témoignage ?
24:7	Car Athalyah la méchante et ses fils ont ravagé la maison d'Elohîm, ils ont même employé pour les Baalim toutes les choses consacrées à la maison de YHWH.

### Offrandes volontaires pour la réparation du temple<!--2 R. 12:9-16.-->

24:8	Et le roi ordonna qu'on fasse un seul coffre et qu'on le mette à la porte de la maison de YHWH, à l'extérieur.
24:9	On fit proclamer dans Yéhouda et dans Yeroushalaim qu'il fallait apporter à YHWH la contribution prescrite par Moshè, serviteur d'Elohîm, à Israël dans le désert.
24:10	Tous les chefs et tout le peuple s'en réjouirent. Ils vinrent et mirent dans le coffre jusqu'à ce qu'il soit plein.
24:11	Et il arrivait que, quand c’était le temps d’apporter le coffre aux soins du roi par la main des lévites, et qu’on voyait qu’il y avait beaucoup d’argent, le secrétaire du roi et le commissaire du grand-prêtre venaient et vidaient le coffre, puis ils le rapportaient et le remettaient à sa place. Ils faisaient ainsi jour après jour, et ils recueillaient de l'argent en abondance.
24:12	Le roi et Yehoyada le donnaient à ceux qui étaient chargés de l'ouvrage pour le service de la maison de YHWH. Ceux-ci engageaient des tailleurs de pierres et des charpentiers pour réparer la maison de YHWH, et aussi des ouvriers pour le fer et le cuivre, afin de réparer la maison de YHWH.
24:13	Ceux qui étaient chargés des travaux se mirent à l'œuvre et la restauration progressa entre leurs mains. Ils rétablirent la maison d'Elohîm dans son état et l'affermirent.
24:14	Lorsqu'ils eurent achevé, ils apportèrent devant le roi et devant Yehoyada le reste de l'argent, et l'on en fit des ustensiles pour la maison de YHWH, des ustensiles pour le service et pour les holocaustes, des coupes et d'autres ustensiles en or et en argent. Et on offrit continuellement des holocaustes dans la maison de YHWH, tant que vécut Yehoyada.

### Mort de Yehoyada, Yoash abandonne YHWH<!--2 R. 12:9-16.-->

24:15	Or Yehoyada devint vieux et rassasié de jours et il mourut. Il était âgé de 130 ans quand il mourut.
24:16	On l'ensevelit dans la cité de David avec les rois, car il avait fait du bien à Israël, et à l'égard d'Elohîm et de sa maison.
24:17	Mais, après la mort de Yehoyada, les chefs de Yéhouda vinrent et se prosternèrent devant le roi. Alors le roi les écouta.
24:18	Ils abandonnèrent la maison de YHWH, l'Elohîm de leurs pères, et ils servirent les ashérim et les faux elohîm. La colère de YHWH fut sur Yéhouda et sur Yeroushalaim, parce qu'ils s'étaient ainsi rendus coupables.
24:19	YHWH envoya parmi eux des prophètes, pour les faire retourner à lui par leurs avertissements, mais ils ne voulurent pas les écouter.
24:20	Alors l'Esprit d'Elohîm revêtit Zekaryah, fils de Yehoyada, le prêtre, et se tenant devant le peuple, il leur dit : Elohîm m'a parlé ainsi : Pourquoi transgressez-vous les commandements de YHWH ? Vous ne prospérerez pas, car vous avez abandonné YHWH et il vous abandonnera aussi.
24:21	Mais ils se liguèrent contre lui et le lapidèrent avec des pierres, par ordre du roi, dans le parvis de la maison de YHWH.
24:22	Ainsi le roi Yoash ne se souvint pas de la bonté dont Yehoyada, père de Zekaryah, avait usé envers lui, et il tua son fils, qui dit en mourant : YHWH le voit, et il en demandera compte !

### Invasion des Syriens, conspiration et mort de Yoash<!--2 R. 12:17-21 ; cp. 2 R. 13:7.-->

24:23	Et il arriva, au retour de l’année, que l'armée de Syrie monta contre Yoash, et entra en Yéhouda et à Yeroushalaim. Ils détruisirent parmi le peuple tous les chefs du peuple et envoyèrent au roi de Damas tout leur butin.
24:24	Car c'est avec peu d'hommes qu'était venue l'armée de Syrie, mais YHWH livra entre ses mains une armée très nombreuse, parce qu'ils avaient abandonné YHWH, l'Elohîm de leurs pères. Ainsi les Syriens exercèrent des jugements contre Yoash.
24:25	Quand ils s'éloignèrent de lui, après l'avoir laissé dans de grandes souffrances, ses serviteurs conspirèrent contre lui, à cause du sang des fils de Yehoyada, le prêtre. Ils le tuèrent sur son lit, et il mourut. On l'ensevelit dans la cité de David, mais on ne l'ensevelit pas dans les sépulcres des rois.
24:26	Et voici ceux qui conspirèrent contre lui : Yehozabad, fils de Shimeath, femme ammonite, et Yehozabad, fils de Shimrith, femme moabite.
24:27	Quant à ses fils et à la grande charge qui reposa sur lui, et à la réparation de la maison d'Elohîm, voici, ces choses sont écrites dans le Midrash<!--« Étude », « exposition », « enregistrement », « histoire », « écrits de nature didactique ».--> du livre des rois. Amatsyah, son fils, régna à sa place.

## Chapitre 25

### Amatsyah règne sur Yéhouda<!--2 R. 12:21, 14:1-6.-->

25:1	Amatsyah devint roi à l'âge de 25 ans, et il régna 29 ans à Yeroushalaim. Le nom de sa mère était Yehoaddan, de Yeroushalaim.
25:2	Il fit ce qui est droit aux yeux de YHWH, mais non d'un cœur entier.
25:3	Il arriva, après qu'il fut affermi dans son royaume, qu'il fit mourir ses serviteurs qui avaient tué le roi, son père.
25:4	Mais il ne fit pas mourir leurs fils, car il fit selon ce qui est écrit dans la torah<!--De. 24:16 ; Ez. 18:20.-->, dans le livre de Moshè, où YHWH a donné ce commandement en disant : Les pères ne mourront pas pour les fils, et les fils ne mourront pas pour les pères. Mais chacun mourra pour son péché.

### Amatsyah en guerre contre les Édomites, sa victoire<!--2 R. 14:7.-->

25:5	Puis Amatsyah rassembla ceux de Yéhouda, et il les rangea selon les familles des pères, par chefs de milliers et par chefs de centaines, pour tout Yéhouda et Benyamin. Il en fit le dénombrement depuis l'âge de 20 ans et au-dessus. Il trouva 300 000 jeunes hommes, propres à l'armée, maniant la lance et le bouclier.
25:6	Il engagea encore 100 000 vaillants hommes et talentueux d'Israël pour 100 talents d'argent.
25:7	Mais un homme d'Elohîm vint à lui, et lui dit : Ô roi ! Que l'armée d'Israël ne marche pas avec toi, car YHWH n'est pas avec Israël ni avec tous ces fils d'Éphraïm.
25:8	Car s'ils viennent, tu auras beau te montrer courageux à la guerre, Elohîm te fera trébucher devant l'ennemi, car Elohîm a la puissance d'aider et de faire trébucher.
25:9	Amatsyah dit à l'homme d'Elohîm : Mais que faire des 100 talents que j'ai donnés à la troupe d'Israël ? L'homme d'Elohîm dit : YHWH peut t'en donner beaucoup plus.
25:10	Ainsi Amatsyah sépara les troupes qui lui étaient venues d'Éphraïm, et les fit retourner chez elles. Mais leur colère s'enflamma très ardemment contre Yéhouda, et ces hommes retournèrent chez eux dans une grande colère.
25:11	Alors Amatsyah prit courage, conduisit son peuple et s'en alla dans la vallée du sel, où il battit 10 000 hommes des fils de Séir.
25:12	Les fils de Yéhouda prirent 10 000 hommes vivants, et les ayant amenés sur le sommet d'un rocher, ils les jetèrent du sommet de ce rocher, de sorte qu'ils furent tous brisés.
25:13	Mais les hommes de la troupe qu'Amatsyah avait renvoyée, afin qu'ils n'aillent pas avec lui à la guerre, firent une incursion dans les villes de Yéhouda, depuis Samarie jusqu'à Beth-Horon. Ils y tuèrent 3 000 personnes et emportèrent un gros butin.

### Idolâtrie d'Amatsyah<!--2 R. 14:7.-->

25:14	Et il arriva, après qu’Amatsyah fut de retour de la défaite des Édomites, qu’il apporta les elohîm des fils de Séir, et se les établit pour elohîm. Il se prosterna devant eux et leur brûla de l'encens.
25:15	Et la colère de YHWH s'enflamma contre Amatsyah, et il envoya vers lui un prophète qui lui dit : Pourquoi as-tu recherché les elohîm d'un peuple qui n'ont pas délivré leur peuple de ta main ?
25:16	Et il arriva, comme il parlait au roi, que celui-ci lui dit : T'a-t-on établi conseiller du roi ? Cesse maintenant ! Pourquoi veux-tu qu'on te tue ? Et le prophète se retira, mais en disant : Je sais qu'Elohîm a résolu de te détruire, parce que tu as fait cela, et que tu n'as pas écouté mon conseil.

### Défaite d'Amatsyah contre Israël<!--2 R. 14:8-14.-->

25:17	Puis Amatsyah, roi de Yéhouda, ayant tenu conseil, envoya vers Yoash, fils de Yehoachaz, fils de Yehuw, roi d'Israël, pour lui dire : Viens, voyons-nous en face !
25:18	Mais Yoash, roi d'Israël, envoya dire à Amatsyah, roi de Yéhouda : L'épine du Liban envoya dire au cèdre du Liban : Donne ta fille pour femme à mon fils ! Et les bêtes sauvages qui sont au Liban passèrent et foulèrent l'épine.
25:19	Voici, tu dis que tu as frappé les Édomites, et ton cœur s'est élevé pour te glorifier. Maintenant, reste dans ta maison ! Pourquoi t'engagerais-tu dans un combat où tu tomberais, et Yéhouda avec toi ?
25:20	Mais Amatsyah ne l'écouta pas. En effet, Elohîm avait résolu de le livrer aux mains de Yoash parce qu'il eût recours aux elohîm d'Édom.
25:21	Yoash, roi d'Israël, monta et ils se virent en face, lui et Amatsyah, roi de Yéhouda, à Beth-Shémesh, qui est de Yéhouda.
25:22	Yéhouda fut battu en face d'Israël, et chacun s'enfuit dans sa tente.
25:23	Yoash, roi d'Israël, prit Amatsyah, roi de Yéhouda, fils de Yoash, fils de Yehoachaz, à Beth-Shémesh. Il l'emmena à Yeroushalaim et fit une brèche de 400 coudées dans la muraille de Yeroushalaim, depuis la porte d'Éphraïm jusqu'à la porte de l'angle.
25:24	Il prit l'or, l'argent, tous les vases qui se trouvaient dans la maison d'Elohîm sous la garde d'Obed-Édom, les trésors de la maison du roi. Il prit aussi des enfants en otages et il retourna à Samarie.

### Assassinat d'Amatsyah<!--2 R. 14:17-20.-->

25:25	Amatsyah, fils de Yoash, roi de Yéhouda, vécut 15 ans, après que Yoash, fils de Yehoachaz, roi d'Israël, mourut.
25:26	Le reste des actions d'Amatsyah, les premières et les dernières, voici cela n'est-il pas écrit dans le livre des rois de Yéhouda et d'Israël ?
25:27	Or depuis le moment où Amatsyah se détourna de YHWH, on fit une conspiration contre lui à Yeroushalaim, et il s'enfuit à Lakis, mais on le poursuivit à Lakis, et on le fit mourir.
25:28	Puis on le transporta sur des chevaux, et on l'ensevelit avec ses pères dans la ville de Yéhouda.

## Chapitre 26

### Ouzyah règne sur Yéhouda ; il est fidèle à YHWH<!--2 R. 14:21-15:4.-->

26:1	Alors, tout le peuple de Yéhouda prit Ouzyah<!--Généralement traduit par Ozias, également appelé Azaryah (voir 2 R. 15:1-2).-->, âgé de 16 ans, et l'établit roi à la place de son père Amatsyah.
26:2	Ce fut lui qui bâtit Éloth, et la ramena sous la puissance de Yéhouda, après que le roi se fut endormi avec ses pères.
26:3	Ouzyah était âgé de 16 ans quand il devint roi, et il régna 52 ans à Yeroushalaim. Le nom de sa mère était Yekolyah, de Yeroushalaim.
26:4	Il fit ce qui est droit aux yeux de YHWH, comme avait fait Amatsyah, son père.
26:5	Il s'appliqua à rechercher Elohîm pendant les jours de Zekaryah, qui avait une intelligence dans les visions d'Elohîm et pendant les jours où il rechercha YHWH, Elohîm le fit prospérer.
26:6	Il sortit et fit la guerre contre les Philistins. Il brisa la muraille de Gath, la muraille de Yabneh, et la muraille d'Asdod. Il bâtit des villes dans le pays d'Asdod et chez les Philistins.
26:7	Elohîm le secourut contre les Philistins et contre les Arabes qui habitaient à Gour-Baal, et contre les Maonites.
26:8	Même les Ammonites faisaient des présents à Ouzyah, et sa renommée parvint jusqu'à l'entrée de l'Égypte, car il était devenu très puissant.
26:9	Ouzyah bâtit des tours à Yeroushalaim, sur la porte de l'angle, sur la porte de la vallée, sur l'angle, et il les fortifia.
26:10	Il bâtit des tours dans le désert, et il creusa de nombreux puits, parce qu'il avait de nombreux troupeaux dans la plaine et dans la campagne, des laboureurs et des vignerons sur les montagnes, et au Carmel. En effet, il aimait l'agriculture.
26:11	Ouzyah avait une armée pour faire la guerre, allant au combat par bandes, selon le compte de leur dénombrement fait par Yéiël le scribe, et Ma`aseyah le commissaire, et sous la conduite de Chananyah l'un des chefs du roi.
26:12	Le nombre total des chefs des pères, des vaillants guerriers, était de 2 600.
26:13	Il y avait sous leur conduite une force armée de 307 500 hommes talentueux et capables de faire la guerre avec force afin de soutenir le roi contre l'ennemi.
26:14	Ouzyah leur procura, pour toute l'armée, des boucliers, des lances, des casques, des cuirasses, des arcs et des pierres de fronde.
26:15	Il fit faire à Yeroushalaim des machines inventées par un ingénieur, pour être placées sur les tours et sur les angles, pour lancer des flèches et de grosses pierres. Et sa renommée se répandit au loin. Il fut en effet extraordinairement soutenu, jusqu'à ce qu'il devienne très puissant.

### Ouzyah pèche et est frappé de lèpre<!--2 R. 15:5-7,32.-->

26:16	Mais dès qu'il fut puissant, son cœur s'éleva pour le corrompre. Et il pécha contre YHWH, son Elohîm : Il entra dans le temple de YHWH pour brûler de l'encens sur l'autel de l'encens.
26:17	Mais Azaryah le prêtre, entra après lui, et avec lui 80 prêtres de YHWH, hommes vaillants,
26:18	qui s'opposèrent au roi Ouzyah, et lui dirent : Ce n'est pas à toi, Ouzyah, d'offrir le parfum à YHWH, mais aux prêtres, fils d'Aaron, qui sont consacrés pour cela. Sors du sanctuaire, car tu as péché ! Et cela ne sera pas à ta gloire devant YHWH Elohîm.
26:19	Alors Ouzyah, qui avait à la main un encensoir pour faire brûler de l'encens, se mit en colère. Et comme il s'irritait contre les prêtres, la lèpre parut sur son front, en présence des prêtres, dans la maison de YHWH, près de l'autel de l'encens.
26:20	Azaryah, le principal prêtre, le regarda ainsi que tous les prêtres. Et voici, il avait de la lèpre sur le front. Ils le pressèrent et lui-même se hâta de sortir, parce que YHWH l'avait frappé.
26:21	Le roi Ouzyah fut ainsi lépreux jusqu'au jour de sa mort. Il habita seul comme lépreux dans une maison séparée, car il était exclu de la maison de YHWH. Et Yotham, son fils, avait la charge de la maison du roi, jugeant le peuple du pays.
26:22	Yesha`yah, fils d'Amots, le prophète, a écrit le reste des actions d'Ouzyah, les premières et les dernières.
26:23	Ouzyah s'endormit avec ses pères, et on l'ensevelit avec ses pères dans le champ de la sépulture des rois, car on disait : Il est lépreux. Et Yotham, son fils, régna à sa place.

## Chapitre 27

### Yotham règne sur Yéhouda ; sa mort<!--2 R. 15:7,32-38.-->

27:1	Yotham était âgé de 25 ans quand il devint roi, et il régna 16 ans à Yeroushalaim. Le nom de sa mère était Yerousha, fille de Tsadok.
27:2	Il fit ce qui est droit aux yeux de YHWH, tout comme Ouzyah, son père, avait fait, mais il n'entra pas dans le temple de YHWH. Néanmoins, le peuple se corrompait encore.
27:3	Ce fut lui qui bâtit la porte supérieure de la maison de YHWH, et il bâtit beaucoup sur les murs de la colline.
27:4	Il bâtit des villes sur les montagnes de Yéhouda, des châteaux et des tours dans les forêts.
27:5	Il fut en guerre avec le roi des fils d'Ammon, et fut le plus fort. Cette année-là, les fils d'Ammon lui donnèrent 100 talents d'argent, 10 000 cors de froment, et 10 000 d'orge. Les fils d'Ammon lui en donnèrent autant la seconde et la troisième année.
27:6	Yotham devint donc très puissant, parce qu'il avait affermi ses voies devant YHWH, son Elohîm.
27:7	Le reste des discours de Yotham, tous ses combats et sa conduite, voici, toutes ces choses sont écrites dans le livre des rois d'Israël et de Yéhouda.
27:8	Il était âgé de 25 ans quand il devint roi, et il régna 16 ans à Yeroushalaim.
27:9	Puis Yotham s'endormit avec ses pères, et on l'ensevelit dans la cité de David. Et Achaz, son fils, régna à sa place.

## Chapitre 28

### Achaz règne sur Yéhouda<!--2 R. 15:38-16:4.-->

28:1	Achaz était âgé de 20 ans quand il devint roi, et il régna 16 ans à Yeroushalaim. Il ne fit pas ce qui est droit aux yeux de YHWH, comme David, son père.
28:2	Il marcha sur la voie des rois d'Israël. Il fit même des images en métal fondu pour les Baalim.
28:3	Il brûla des parfums dans la vallée du fils de Hinnom, et il brûla ses fils au feu, suivant les abominations des nations que YHWH avait chassées devant les enfants d'Israël.
28:4	Il offrait aussi des sacrifices et brûlait des parfums dans les hauts lieux, sur les collines, et sous tout arbre vert.

### La Syrie et Israël envahissent Yéhouda<!--2 R. 16:5-6.-->

28:5	C'est pourquoi YHWH, son Elohîm, le livra entre les mains du roi de Syrie. Les Syriens le battirent et lui prirent un grand nombre de prisonniers, qu'ils emmenèrent à Damas. Il fut livré aussi entre les mains du roi d'Israël, qui lui fit endurer une grande défaite.
28:6	Car Pékach, fils de Remalyah, tua en un seul jour en Yéhouda 120 000 hommes, tous des fils talentueux, parce qu'ils avaient abandonné YHWH, l'Elohîm de leurs pères.
28:7	Zicri, homme vaillant d'Éphraïm, tua Ma`aseyah, fils du roi, et Azrikam, chef de la maison, et Elkana, le second après le roi.
28:8	Les fils d'Israël emmenèrent prisonniers 200 000 de leurs frères, tant femmes que fils et filles. Ils leur prirent beaucoup de butin, et ils emmenèrent le butin à Samarie.

### Les captifs de Yéhouda libérés grâce à Oded

28:9	Or il y avait un prophète de YHWH nommé Oded. Il sortit au-devant de cette armée qui revenait à Samarie, et leur dit : Voici, YHWH, l'Elohîm de vos pères, étant indigné contre Yéhouda, les a livrés entre vos mains, et vous les avez tués avec une colère telle qu'elle est parvenue aux cieux.
28:10	Et maintenant, vous pensez assujettir les fils de Yéhouda et de Yeroushalaim pour serviteurs et pour servantes ! Mais n'êtes-vous pas également coupables envers YHWH, votre Elohîm ?
28:11	Maintenant écoutez-moi, et ramenez les prisonniers que vous vous êtes faits parmi vos frères, car la colère ardente de YHWH est sur vous.
28:12	Alors quelques-uns des chefs des fils d'Éphraïm, Azaryah, fils de Yehohanan, Berekyah, fils de Meshillémoth, Yehizqiyah, fils de Shalloum, et Amasa, fils de Hadlaï, s'élevèrent contre ceux qui retournaient de la guerre,
28:13	et leur dirent : Vous ne ferez pas entrer ici ces captifs. C'est pour nous rendre coupables devant YHWH, voulez-vous en rajouter à nos péchés et à notre culpabilité. Car nous sommes déjà grandement coupables, et une colère ardente est sur Israël.
28:14	Alors les soldats abandonnèrent les captifs et le butin devant les chefs et toute l'assemblée.
28:15	Et des hommes, désignés par leurs noms, se levèrent, prirent les captifs, utilisèrent le butin pour revêtir tous ceux d'entre eux qui étaient nus avec des vêtements et des chaussures. Ils leur donnèrent à manger et à boire, les oignirent et ils conduisirent sur des ânes tous ceux qui étaient affaiblis pour les emmener à Yeriycho, la ville des palmiers, auprès de leurs frères. Ensuite ils revinrent à Samarie.

### Achaz fait appel aux Assyriens<!--2 R. 15:29, 16:7-18.-->

28:16	En ce temps-là, le roi Achaz envoya demander du secours aux rois d'Assyrie.
28:17	Les Édomites étaient revenus, avaient battu Yéhouda et avaient emmené des prisonniers.
28:18	Les Philistins s'étaient aussi jetés sur les villes de la plaine et du sud de Yéhouda. Ils avaient pris Beth-Shémesh, Ayalon, Guedéroth, Soco et les villes de son ressort, Thimna et les villes de son ressort, Guimzo et les villes de son ressort, et ils y demeurèrent.
28:19	Car YHWH humilia Yéhouda, à cause d'Achaz, roi d'Israël, parce qu'il avait mis le désordre en Yéhouda, et qu'il avait commis des transgressions contre YHWH.
28:20	Tilgath-Pilnéser, roi d'Assyrie, vint vers lui, mais il l'assiégea, et ne le fortifia pas.
28:21	Or Achaz dépouilla la maison de YHWH, la maison du roi et celle des chefs, pour faire des dons au roi d'Assyrie, mais sans avoir du secours.

### Achaz irrite YHWH par ses péchés

28:22	Dans le temps de sa détresse, il continua à pécher contre YHWH, lui, le roi Achaz.
28:23	Il sacrifia aux elohîm de Damas qui l'avaient battu, et il dit : Puisque les elohîm des rois de Syrie leur viennent en aide, je leur sacrifierai, afin qu'ils me viennent en aide. Mais ils furent la cause de sa chute et de celle de tout Israël.
28:24	Or Achaz rassembla les ustensiles de la maison d'Elohîm, et il mit en pièces les ustensiles de la maison d'Elohîm. Il ferma les portes de la maison de YHWH, et se fit des autels dans tous les coins de Yeroushalaim.
28:25	Il fit des hauts lieux dans chaque ville de Yéhouda, pour offrir des parfums à d'autres elohîm. Il irrita YHWH, l'Elohîm de ses pères.

### Mort d'Achaz<!--2 R. 16:19-20.-->

28:26	Quant au reste de ses discours et de toutes ses voies, les premières et les dernières, voici, ils sont écrits dans le livre des rois de Yéhouda et d'Israël.
28:27	Puis Achaz s'endormit avec ses pères, et on l'ensevelit dans la ville de Yeroushalaim, car on ne le mit pas dans les sépulcres des rois d'Israël. Et Yehizqiyah, son fils, régna à sa place.

## Chapitre 29

### Yehizqiyah (Ézéchias) règne sur Yéhouda ; le réveil du peuple<!--2 R. 18:1-7 ; cp. Es. 36-39.-->

29:1	Yehizqiyah devint roi à l'âge de 25 ans, et il régna 29 ans à Yeroushalaim. Le nom de sa mère était Abiyah, fille de Zekaryah.
29:2	Il fit ce qui est droit aux yeux de YHWH, tout comme avait fait David, son père.
29:3	La première année de son règne, au premier mois, il ouvrit les portes de la maison de YHWH, et il les répara.
29:4	Il fit venir les prêtres et les Lévites, et les rassembla dans la place orientale.
29:5	Et il leur dit : Écoutez-moi, Lévites ! Sanctifiez-vous et sanctifiez la maison de YHWH, l'Elohîm de vos pères, et ôtez du lieu saint tout ce qui est impur.
29:6	Car nos pères ont péché, ils ont fait ce qui est mal aux yeux de YHWH, notre Elohîm. Ils l'ont abandonné, ils ont détourné leurs faces du tabernacle de YHWH et lui ont tourné le dos.
29:7	Ils ont même fermé les portes du portique et ont éteint les lampes, ils n'ont fait ni monter d'offrandes, ni brûler de l'encens et des holocaustes à Elohîm d'Israël dans le lieu saint.
29:8	C'est pourquoi la colère de YHWH a été sur Yéhouda et sur Yeroushalaim, et il les a livrés à la terreur, à la dévastation et à la moquerie, comme vous le voyez de vos yeux.
29:9	Car voici, nos pères sont tombés par l'épée, et nos fils, nos filles et nos femmes sont en captivité.
29:10	Maintenant donc j'ai à cœur de traiter alliance avec YHWH, l'Elohîm d'Israël, pour que son ardente colère se détourne de nous.
29:11	Or mes fils, cessez d'être négligents, car YHWH vous a choisis, afin que vous vous teniez devant lui à son service, comme ses serviteurs, pour lui brûler des parfums.
29:12	Les Lévites se levèrent : Machath, fils d'Amasaï, et Yoel, fils d'Azaryah, des fils des Kehathites ; et des fils des Merarites, Kis, fils d'Abdi, Azaryah, fils de Yehalléleel ; et des Guershonites, Yoach, fils de Zimma, et Éden, fils de Yoach ;
29:13	et des fils d'Élitsaphan : Shimri et Yéiël ; des fils d'Asaph : Zekaryah et Mattanyah ;
29:14	et des fils d'Héman : Yechiy'el et Shimeï ; des fils de Yedoutoun : Shema’yah et Ouzziel.
29:15	Ils rassemblèrent leurs frères, se sanctifièrent, puis ils entrèrent selon l'ordre du roi, et d'après la parole de YHWH, pour purifier la maison de YHWH.
29:16	Ainsi les prêtres entrèrent à l'intérieur de la maison de YHWH pour la purifier. Ils firent sortir dans le parvis de la maison de YHWH toutes les impuretés qu'ils trouvèrent dans le temple de YHWH. Les Lévites les prirent pour les emporter dehors, au torrent de Cédron.
29:17	Ils commencèrent à sanctifier le temple le premier jour du premier mois. Le huitième jour du mois, ils entrèrent au portique de YHWH, et ils sanctifièrent la maison de YHWH pendant 8 jours. Le seizième jour du premier mois, ils avaient achevé.
29:18	Puis ils se rendirent chez le roi Hizqiyah, et dirent : Nous avons purifié toute la maison de YHWH, l'autel des holocaustes et ses ustensiles, la table des pains de proposition et ses ustensiles<!--Ex. 29.-->.
29:19	Nous avons remis en état et sanctifié tous les ustensiles que le roi Achaz avait rendus odieux pendant son règne, par ses transgressions ; ils sont maintenant devant l'autel de YHWH.

### Nouvelle consécration du temple

29:20	Alors le roi Yehizqiyah se leva de bonne heure, rassembla les chefs de la ville, et monta à la maison de YHWH.
29:21	Ils amenèrent 7 taureaux, 7 béliers, 7 agneaux et 7 boucs sans défaut, en sacrifice pour le péché, pour le royaume, pour le sanctuaire et pour Yéhouda<!--Lé. 4:3-26.-->. Puis le roi dit aux prêtres, fils d'Aaron, de les faire monter en offrande sur l'autel de YHWH.
29:22	Ils égorgèrent donc les bœufs, et les prêtres recueillirent le sang et en aspergèrent l'autel. Ils égorgèrent les béliers et aspergèrent l'autel de ce sang. Ils égorgèrent les agneaux et aspergèrent l'autel de ce sang.
29:23	Puis on fit approcher les boucs du sacrifice pour le péché, devant le roi et devant l'assemblée, et ils posèrent leurs mains sur eux<!--Lé. 8:14.-->.
29:24	Alors, les prêtres les égorgèrent et firent un sacrifice pour le péché avec leur sang sur l'autel, afin de faire la propitiation pour tout Israël. En effet, le roi avait ordonné cet holocauste et ce sacrifice pour le péché pour tout Israël.
29:25	Il plaça aussi les Lévites dans la maison de YHWH, avec des cymbales, des luths et des harpes, comme l'avait ordonné David, Gad, le voyant du roi, et Nathan le prophète, car c'était un commandement de YHWH, par ses prophètes.
29:26	Les Lévites se tinrent donc là avec les instruments de David, et les prêtres avec les trompettes.
29:27	Alors Hizqiyah ordonna de faire monter en offrande l'holocauste sur l'autel. Et au moment où commença l'holocauste, le cantique de YHWH commença aussi, avec les trompettes et les instruments de David, roi d'Israël.
29:28	Toute l'assemblée se prosterna en chantant le cantique et les trompettes sonnèrent, et cela continua jusqu'à ce que l'holocauste fût achevé.
29:29	Et quand on eut achevé de faire monter l'holocauste, le roi et tous ceux qui se trouvaient avec lui fléchirent les genoux et se prosternèrent.
29:30	Puis le roi Yehizqiyah et les chefs dirent aux Lévites de célébrer YHWH par les paroles de David et d'Asaph le voyant, et ils le célébrèrent dans des réjouissances et s'inclinèrent pour se prosterner.
29:31	Alors Yehizqiyah prit la parole, et dit : Vous avez maintenant consacré vos mains à YHWH. Approchez-vous, amenez des sacrifices et faites des sacrifices d'action de grâces dans la maison de YHWH. Et l'assemblée amena des sacrifices et firent des sacrifices d'action de grâces, et tous ceux qui étaient d'un cœur volontaire offrirent des holocaustes.
29:32	Le nombre des holocaustes que l'assemblée offrit fut de 70 taureaux, 100 béliers, 200 agneaux, le tout en holocauste à YHWH.
29:33	Et les autres choses consacrées furent, 600 bœufs, et 3 000 brebis.
29:34	Mais les prêtres étaient en petit nombre et ne purent dépouiller tous les holocaustes. Leurs frères, les Lévites, les aidèrent jusqu'à ce que cette œuvre fut achevée, et jusqu'à ce que les autres prêtres se soient sanctifiés, car les Lévites furent plus droits de cœur que les prêtres pour se sanctifier.
29:35	Il y eut aussi un grand nombre d'holocaustes, avec les graisses des offrandes de paix et avec les libations des holocaustes. Ainsi, le service de la maison de YHWH fut rétabli.
29:36	Yehizqiyah et tout le peuple se réjouirent de ce qu'Elohîm avait ainsi disposé le peuple, car les choses se firent instantanément.

## Chapitre 30

### Rétablissement de la Pâque

30:1	Puis Yehizqiyah envoya dire à tout Israël et à Yéhouda, et il écrivit aussi des lettres à Éphraïm et à Menashè, pour les faire venir à la maison de YHWH à Yeroushalaim, pour célébrer la Pâque en l'honneur de YHWH, l'Elohîm d'Israël.
30:2	Le roi, ses chefs et toute l'assemblée avaient tenu un conseil à Yeroushalaim afin de célébrer la Pâque au second mois<!--No. 9:10-11.-->,
30:3	car on ne pouvait la célébrer au temps ordinaire, parce qu'il n'y avait pas un nombre suffisant de prêtres sanctifiés, et que le peuple n'était pas rassemblé à Yeroushalaim.
30:4	La chose parut juste aux yeux du roi et aux yeux de toute l'assemblée.
30:5	Ils décidèrent de faire une publication dans tout Israël, depuis Beer-Shéba jusqu'à Dan, pour que l'on vienne à Yeroushalaim célébrer la Pâque pour YHWH, l'Elohîm d'Israël. Car elle n'était pas célébrée par la multitude depuis longtemps conformément à ce qui était écrit.
30:6	Les coureurs allèrent donc avec des lettres de la part du roi et de ses chefs, partout en Israël et en Yéhouda. Conformément à l'ordre du roi, ils disaient : Enfants d'Israël, retournez à YHWH, l'Elohîm d'Abraham, de Yitzhak et d'Israël, et afin qu'il revienne vers vous, qui êtes le reste échappé de la main des rois d'Assyrie.
30:7	Ne soyez pas comme vos pères ni comme vos frères, qui ont péché contre YHWH, l'Elohîm de leurs pères, c'est pourquoi il les a livrés à la désolation, comme vous le voyez.
30:8	Maintenant, ne raidissez pas votre cou comme vos pères. Tendez les mains vers YHWH, venez à son sanctuaire consacré pour toujours, servez YHWH, votre Elohîm, et son ardente colère se détournera de vous.
30:9	Car si vous revenez à YHWH, vos frères et vos fils trouveront grâce auprès de ceux qui les ont emmenés captifs, et ils reviendront en ce pays parce que YHWH, votre Elohîm, est compatissant et miséricordieux. Il ne détournera pas sa face de vous, si vous revenez à lui.
30:10	Les coureurs passaient ainsi de ville en ville, par le pays d'Éphraïm et de Menashè jusqu'à Zebouloun, mais on riait et on se moquait d'eux.
30:11	Toutefois, quelques-uns d'Asher, de Menashè et de Zebouloun s'humilièrent, et vinrent à Yeroushalaim.
30:12	La main d'Elohîm fut aussi sur Yéhouda, pour leur donner un même cœur, afin d'exécuter l'ordre du roi et des chefs, selon la parole de YHWH.
30:13	C'est pourquoi il s'assembla un grand peuple à Yeroushalaim pour célébrer la fête des pains sans levain<!--Ex. 12:15 ; Lé. 23:6.-->, au second mois. Ce fut une très grande assemblée.
30:14	Ils se levèrent et ôtèrent les autels qui étaient à Yeroushalaim. Ils ôtèrent aussi tous ceux où l'on brûlait de l'encens, et ils les jetèrent dans le torrent de Cédron.
30:15	Puis on immola la Pâque au quatorzième jour du second mois. Les prêtres et les Lévites, pris de honte, s'étaient sanctifiés et avaient amené les holocaustes dans la maison de YHWH.
30:16	Ils se tinrent à leur poste, selon leur charge, d'après la torah de Moshè, homme d'Elohîm. Et les prêtres faisaient l'aspersion du sang qu'ils recevaient des mains des Lévites.
30:17	En effet, comme beaucoup de gens dans l'assemblée ne s'étaient pas sanctifiés, les Lévites eurent la charge d'immoler la Pâque pour tous ceux qui n'étaient pas purs, afin de les consacrer à YHWH.
30:18	Car une grande partie du peuple, la plupart de ceux d'Éphraïm, de Menashè, de Yissakar et de Zebouloun, ne s'étaient pas purifiés et mangèrent la Pâque contrairement à ce qui est écrit. Mais Yehizqiyah pria pour eux, en disant : Que YHWH, qui est bon, tienne la propitiation pour faite,
30:19	pour quiconque a disposé son cœur à rechercher Elohîm, YHWH, l'Elohîm de leurs pères, bien qu'il ne soit pas purifié conformément au sanctuaire !
30:20	YHWH exauça Yehizqiyah, et guérit le peuple.
30:21	Les enfants d'Israël qui se trouvèrent à Yeroushalaim célébrèrent donc la fête des pains sans levain, pendant 7 jours, dans une grande réjouissance. Les Lévites et les prêtres célébraient YHWH jour après jour, avec les instruments qui retentissaient à la louange de YHWH.
30:22	Yehizqiyah parla au cœur de tous les Lévites, qui agissaient prudemment ayant une bonne compréhension à l'égard du service de YHWH. Ils mangèrent pendant la fête, 7 jours durant, offrant des sacrifices d'offrande de paix, et louant YHWH, l'Elohîm de leurs pères.

### 7 jours supplémentaires pour la Pâque

30:23	Puis toute l'assemblée fut d'avis de célébrer 7 autres jours. Et ils célébrèrent ces 7 jours dans la joie.
30:24	Car Hizqiyah, roi de Yéhouda, offrit à l'assemblée 1 000 taureaux et 7 000 brebis, et les chefs donnèrent à l'assemblée 1 000 taureaux et 10 000 brebis. De plus, un grand nombre de prêtres s'étaient sanctifiés.
30:25	Toute l'assemblée de Yéhouda, avec les prêtres et les Lévites, et toute l'assemblée venue d'Israël, ainsi que les étrangers venus du pays d'Israël, et ceux qui habitaient en Yéhouda, se réjouirent.
30:26	Il y eut une grande joie à Yeroushalaim, car depuis le temps de Shelomoh, fils de David, roi d'Israël, il ne s'était pas fait une telle chose dans Yeroushalaim.
30:27	Puis les prêtres et les Lévites se levèrent et bénirent le peuple, et leur voix fut entendue, leur prière parvint jusqu'aux cieux, jusqu'à la sainte demeure de YHWH.

## Chapitre 31

### Destruction des idoles et organisation des services du temple

31:1	Lorsque tout cela fut achevé, tous ceux d'Israël qui s'étaient retrouvés là allèrent dans les villes de Yéhouda et brisèrent les statues, abattirent les ashérim et renversèrent les hauts lieux et les autels, dans tout Yéhouda et Benyamin, dans Éphraïm et Menashè, jusqu'à détruire tout<!--2 R. 18:4.-->. Puis tous les enfants d'Israël retournèrent dans leurs villes, chacun dans sa possession.
31:2	Et Yehizqiyah rétablit les classes des prêtres et des Lévites, selon leur partage, chacun suivant sa charge, tant les prêtres que les Lévites, pour les holocaustes et les offrandes de paix, pour faire le service, célébrer et chanter les louanges aux portes du camp de YHWH.
31:3	Le roi donna une portion de ses biens pour les holocaustes, pour les holocaustes du matin et du soir, pour les holocaustes des shabbats, des nouvelles lunes et des fêtes, comme cela est écrit dans la torah de YHWH.
31:4	Il dit au peuple, aux habitants de Yeroushalaim, de donner la portion des prêtres et des Lévites, afin de s'appliquer à la torah de YHWH.
31:5	Dès que la chose fut publiée, les enfants d'Israël amenèrent en abondance les premières récoltes de blé, de vin, de l'huile, de miel et de tous les produits des champs. Ils apportèrent aussi en abondance les dîmes de tout.
31:6	Les enfants d'Israël et de Yéhouda, qui demeuraient dans les villes de Yéhouda, apportèrent aussi les dîmes du gros et du menu bétail ainsi que les dîmes des choses saintes consacrées à YHWH, leur Elohîm, et ils les mirent par tas.
31:7	Ils commencèrent à former les tas au troisième mois, et ils les achevèrent au septième mois.
31:8	Alors Yehizqiyah et les chefs vinrent voir les tas, et ils bénirent YHWH et son peuple d'Israël.
31:9	Yehizqiyah interrogea les prêtres et les Lévites au sujet de ces tas.
31:10	Le grand-prêtre Azaryah, de la maison de Tsadok, lui répondit, et parla ainsi : Depuis qu'on a commencé à apporter des offrandes à la maison de YHWH, nous avons mangé et avons été rassasiés, et il en est resté en abondance, car YHWH a béni son peuple, et le reste est cette grande quantité.
31:11	Alors Yehizqiyah leur dit de préparer des chambres dans la maison de YHWH et ils les préparèrent.
31:12	On y apporta fidèlement les offrandes et les dîmes, les choses consacrées. Konanyah, le Lévite, en eut l'intendance, et Shimeï, son frère, était son second.
31:13	Yechiy'el, Azazyah, Nachath, Asaël, Yeriymoth, Yozabad, Éliel, Yismakyah, Machath, et Benayah, étaient commis sous l'autorité de Konanyah et de Shimeï, son frère, d'après l'indication du roi Yehizqiyah, et d'Azaryah, chef de la maison d'Elohîm.
31:14	Koré, le Lévite, fils de Yimnah, portier de l'orient, avait la charge des offrandes volontaires offertes à Elohîm, pour distribuer l'offrande élevée à YHWH, et les choses consacrées et saintes.
31:15	Il avait sous sa direction Éden, Minyamin, Yéshoua, Shema’yah, Amaryah, et Shekanyah, dans les villes des prêtres, pour distribuer fidèlement les portions à leurs frères, grands et petits, suivant leurs divisions,
31:16	à ceux qui étaient enregistrés comme mâles, depuis l'âge de 3 ans et au-delà, à tous ceux qui entraient dans la maison de YHWH, pour les affaires de jour après jour, pour y accomplir leur service, selon leurs fonctions et leurs divisions ;
31:17	aux prêtres et aux Lévites enregistrés selon la maison de leurs pères, depuis ceux de 20 ans et au-delà, selon leurs charges et selon leurs divisions ;
31:18	à ceux de toute l'assemblée enregistrés avec leurs petits enfants, leurs femmes, leurs fils et leurs filles, car ils se consacraient avec fidélité aux choses saintes ;
31:19	et pour les enfants d'Aaron, les prêtres, qui étaient à la campagne et dans les faubourgs de leurs villes, dans chaque ville, il y avait des gens désignés par leur nom, pour distribuer les portions à tous les mâles des prêtres, et à tous les Lévites enregistrés.
31:20	C'est ainsi que Yehizqiyah agit dans tout Yéhouda. Il fit ce qui est bon, droit et véritable, devant YHWH, son Elohîm.
31:21	Il travailla de tout son cœur et il réussit dans tout l'ouvrage qu'il entreprit pour le service de la maison d'Elohîm, et pour la torah, et pour les commandements, en recherchant son Elohîm.

## Chapitre 32

### Menaces de Sanchérib, roi d'Assyrie<!--2 R. 19:17-37, 19:8-13 ; Es. 36:2-20.-->

32:1	Après que ces choses furent bien établies, Sanchérib, roi d'Assyrie, vint et entra en Yéhouda, et campa contre les villes fortes, dans l'intention de faire une brèche.
32:2	Yehizqiyah, voyant que Sanchérib était venu, et qu'il se tournait vers Yeroushalaim pour lui faire la guerre,
32:3	tint conseil avec ses chefs et ses vaillants hommes pour boucher les sources d'eau qui étaient hors de la ville, et ils l'aidèrent.
32:4	Un peuple nombreux se rassembla et ils bouchèrent toutes les sources et le torrent qui coule par le milieu de la contrée, en disant : Pourquoi les rois d'Assyrie trouveraient-ils à leur venue de l'eau en abondance ?
32:5	Il se fortifia et rebâtit toute la muraille qui avait des brèches et l'éleva jusqu'aux tours. Il bâtit une autre muraille en dehors, répara le Millo dans la cité de David et fabriqua une quantité d'armes et de boucliers.
32:6	Il donna des chefs de guerre au peuple, les rassembla auprès de lui sur la place de la porte de la ville et parla à leur cœur, en disant :
32:7	Fortifiez-vous, soyez forts ! N'ayez pas peur et ne soyez pas effrayés devant le roi d'Assyrie et devant toute la multitude qui est avec lui, car avec nous il y a un plus grand qu'avec lui :
32:8	Avec lui est le bras de la chair, mais avec nous est YHWH, notre Elohîm, pour nous aider et pour combattre dans nos combats. Et le peuple s'appuya sur les paroles de Yehizqiyah, roi de Yéhouda.
32:9	Après cela, Sanchérib, roi d'Assyrie, pendant qu'il était devant Lakis, ayant avec lui toutes les forces de son royaume, envoya ses serviteurs à Yeroushalaim vers Yehizqiyah, roi de Yéhouda, et vers tous ceux de Yéhouda qui étaient à Yeroushalaim, pour leur dire :
32:10	Ainsi parle Sanchérib, roi d'Assyrie : Sur qui vous confiez-vous pour que vous restiez à Yeroushalaim pour y être assiégés ?
32:11	Yehizqiyah ne vous incite-t-il pas pour vous livrer à la mort, par la famine et par la soif, en vous disant : YHWH, notre Elohîm, nous délivrera de la main du roi d'Assyrie ?
32:12	Cet Yehizqiyah n'a-t-il pas ôté les hauts lieux et les autels, et n'a-t-il pas ordonné à Yéhouda et à Yeroushalaim : Vous vous prosternerez devant un seul autel pour y brûler le parfum ?
32:13	Ne savez-vous pas ce que nous avons fait, moi et mes pères, à tous les peuples des autres pays ? Les elohîm des nations de ces pays ont-ils pu de quelque manière que ce soit délivrer leur pays de ma main ?
32:14	Quel est celui de tous les elohîm de ces nations, que mes pères ont entièrement détruites, qui ait pu délivrer son peuple de ma main, pour que votre Elohîm puisse vous délivrer de ma main ?
32:15	Maintenant donc, que Hizqiyah ne vous abuse pas, et qu'il ne vous incite plus de cette manière, et ne le croyez pas ! Car aucun Éloah d'aucune nation ni d'aucun royaume n'a pu délivrer son peuple de ma main ni de la main de mes pères. Combien moins votre Elohîm vous délivrerait-il de ma main ?
32:16	Ses serviteurs parlèrent encore contre YHWH Elohîm, et contre Yehizqiyah, son serviteur.
32:17	Il écrivit aussi une lettre pour blasphémer contre YHWH, l'Elohîm d'Israël, en parlant ainsi : De même que les elohîm des nations des pays n'ont pu délivrer leur peuple de ma main, de même l'Elohîm de Yehizqiyah ne pourra délivrer son peuple de ma main.
32:18	Et ses serviteurs crièrent à grande voix en hébreu au peuple de Yeroushalaim qui était sur la muraille, pour l'effrayer et le terrifier et par suite prendre la ville.
32:19	Ils parlèrent d'Elohîm de Yeroushalaim comme des elohîm des peuples de la Terre, qui ne sont qu'un ouvrage de mains humaines.

### Prière de Yehizqiyah (Ézéchias) et exaucement de YHWH<!--2 R. 19:14-37 ; Es. 36:21-37:35.-->

32:20	Alors le roi Yehizqiyah, et Yesha`yah, le prophète, fils d'Amots, prièrent à ce sujet et crièrent vers les cieux.
32:21	Et YHWH envoya un ange, dans le camp du roi d'Assyrie, qui extermina tous les vaillants hommes, les princes et les chefs, en sorte qu'il retourna dans son pays, la honte au visage. Il entra dans la maison de son elohîm et là, ceux qui étaient sortis de ses entrailles le firent tomber par l'épée.
32:22	C'est ainsi que YHWH sauva Yehizqiyah et les habitants de Yeroushalaim de la main de Sanchérib, roi d'Assyrie, et de la main de tout homme, et il les protégea de toutes parts.
32:23	Beaucoup apportèrent des offrandes à YHWH, à Yeroushalaim, et des choses précieuses à Yehizqiyah, roi de Yéhouda, qui après cela fut élevé aux yeux de toutes les nations.

### Maladie et guérison de Yehizqiyah (Ézéchias)<!--2 R. 20:1-11.-->

32:24	En ces jours-là, Yehizqiyah fut malade à la mort. Il pria YHWH qui l'exauça et lui accorda un miracle.
32:25	Mais Yehizqiyah ne fut pas reconnaissant du bienfait qu'il avait reçu, car son cœur s'éleva, et il y eut des maux contre lui, contre Yéhouda et Yeroushalaim.
32:26	Mais Yehizqiyah s'humilia de l'élévation de son cœur, lui et les habitants de Yeroushalaim, et la colère de YHWH ne vint plus sur eux durant les jours de Yehizqiyah.

### Fin du règne de Yehizqiyah, sa mort<!--2 R. 20:12-21 ; cp. Es. 39.-->

32:27	Yehizqiyah eut de très grandes richesses et de gloire. Il se fit des trésors d'argent, d'or, de pierres précieuses, d'aromates, de boucliers, et de toutes sortes d'objets précieux,
32:28	des magasins pour les récoltes de blé, de moût et d'huile, des étables pour toutes sortes de bétail, avec des rangées dans les étables.
32:29	Il se fit aussi des villes, et il acquit des troupeaux du gros et du menu bétail en abondance, car Elohîm lui avait donné de très grandes richesses.
32:30	Ce fut Yehizqiyah, qui boucha le canal du haut des eaux de Guihon, et les conduisit directement en bas, vers l'occident de la cité de David. Ainsi Yehizqiyah réussit dans tout ce qu'il fit.
32:31	Toutefois, lorsque les princes de Babel envoyèrent des messagers vers lui pour s'informer du prodige qui s'était produit dans le pays, Elohîm l'abandonna pour le mettre à l'épreuve, afin de connaître tout ce qui était dans son cœur<!--Es. 39.-->.
32:32	Le reste des discours de Yehizqiyah, ses bonnes œuvres, voici, ils sont écrits dans la vision de Yesha`yah, le prophète, fils d'Amots, dans le livre des rois de Yéhouda et d'Israël.
32:33	Puis Yehizqiyah s'endormit avec ses pères, et on l'ensevelit au plus haut des sépulcres des fils de David. Tout Yéhouda, et Yeroushalaim lui firent honneur à sa mort, et Menashè, son fils régna à sa place.

## Chapitre 33

### Menashè (Manassé), roi impie de Yéhouda<!--2 R. 21:1-9.-->

33:1	Menashè était âgé de 12 ans quand il devint roi, et il régna 55 ans à Yeroushalaim.
33:2	Il fit ce qui est mal aux yeux de YHWH, suivant les abominations des nations que YHWH avait chassées devant les enfants d'Israël.
33:3	Il rebâtit les hauts lieux que Yehizqiyah, son père, avait démolis, il éleva des autels pour les Baalim, il fit des idoles d'Asherah, et se prosterna devant toute l'armée des cieux et la servit.
33:4	Il bâtit aussi des autels dans la maison de YHWH, dont YHWH avait dit : C'est à Yeroushalaim que sera mon Nom pour toujours.
33:5	Il bâtit des autels à toute l'armée des cieux, dans les deux parvis de la maison de YHWH.
33:6	Il fit passer ses fils par le feu dans la vallée du fils de Hinnom. Il pratiquait la voyance, la divination et la sorcellerie. Il établit des gens qui évoquaient les esprits et ceux qui avaient un esprit de divination. Il fit de plus en plus ce qui est mauvais aux yeux de YHWH pour l'irriter.
33:7	Il plaça aussi une image taillée, une idole qu'il avait faite, dans la maison d'Elohîm, dont Elohîm avait dit à David, et à Shelomoh, son fils : Je mettrai à perpétuité mon Nom dans cette maison et dans Yeroushalaim, que j'ai choisie entre toutes les tribus d'Israël,
33:8	et je n'éloignerai plus le pied d'Israël de dessus la terre que j'ai assignée à leurs pères, pourvu seulement qu'ils prennent garde à faire tout ce que je leur ai ordonné, selon toute la loi, les préceptes et les ordonnances prescrites par Moshè.
33:9	Menashè donc fit s'égarer Yéhouda et les habitants de Yeroushalaim, jusqu'à faire pire que les nations que YHWH avait exterminées de devant les enfants d'Israël.

### YHWH avertit Menashè<!--2 R. 21:10-16.-->

33:10	YHWH parla à Menashè et à son peuple, mais ils ne furent pas attentifs.

### Menashè emmené captif se repent<!--2 R. 21:17-18.-->

33:11	Alors YHWH fit venir contre eux les chefs de l'armée du roi d'Assyrie. Ils mirent Menashè dans les fers, le lièrent avec une double chaîne en cuivre et l'emmenèrent à Babel.
33:12	Et dès qu'il fut dans l'angoisse, il supplia la face de YHWH, son Elohîm, et il s'humilia beaucoup devant l'Elohîm de ses pères.
33:13	Il le pria et celui-ci se laissa implorer par lui. Il entendit sa supplication et le fit retourner à Yeroushalaim, dans son royaume. Menashè reconnut alors que c'est YHWH qui est Elohîm.
33:14	Après cela, il bâtit une muraille extérieure à la cité de David, vers l'occident de Guihon, dans la vallée, jusqu'à l'entrée de la porte des poissons. Il environna la colline et l'éleva à une grande hauteur. Il établit aussi des chefs d'armée dans toutes les villes fortes de Yéhouda.
33:15	Il ôta de la maison de YHWH l'idole, et les elohîm étrangers, et tous les autels qu'il avait bâtis sur la montagne de la maison de YHWH et à Yeroushalaim, et les jeta hors de la ville.
33:16	Puis il rebâtit l'autel de YHWH et y sacrifia des sacrifices d'offrande de paix et d'action de grâces. Il ordonna à Yéhouda de servir YHWH, l'Elohîm d'Israël.
33:17	Toutefois, le peuple sacrifiait encore dans les hauts lieux, mais seulement à YHWH, son Elohîm.
33:18	Le reste des actions de Menashè, et la prière qu'il fit à son Elohîm, et les paroles des voyants qui lui parlaient, au Nom de YHWH, l'Elohîm d'Israël, voilà, toutes ces choses sont écrites dans les actes des rois d'Israël.
33:19	Sa prière, et comment Elohîm se laissa fléchir par sa prière, ses péchés et ses infidélités, les lieux sur lesquels il bâtit des hauts lieux, et dressa des ashérim et des images taillées, avant de s'être humilié, voici cela est écrit dans les discours des voyants.
33:20	Puis Menashè s'endormit avec ses pères, et on l'ensevelit dans sa maison. Et Amon, son fils, régna à sa place.

### Amon règne brièvement sur Yéhouda<!--2 R. 21:18-26.-->

33:21	Amon était âgé de 22 ans quand il devint roi, et il régna 2 ans à Yeroushalaim.
33:22	Il fit ce qui est mal aux yeux de YHWH, comme avait fait Menashè, son père. Il sacrifia à toutes les images taillées que Menashè, son père, avait faites, et il les servit.
33:23	Mais il ne s'humilia pas devant YHWH, comme s'était humilié Menashè, son père, mais se rendit de plus en plus coupable.
33:24	Et ses serviteurs ayant fait une conspiration contre lui, le firent mourir dans sa maison.
33:25	Mais le peuple du pays frappa tous ceux qui avaient conspiré contre le roi Amon. Et le peuple du pays établit pour roi, à sa place, Yoshiyah, son fils.

## Chapitre 34

### Yoshiyah (Josias) règne sur Yéhouda ; ses réformes<!--2 R. 22:1-2.-->

34:1	Yoshiyah était âgé de 8 ans quand il devint roi, et il régna 31 ans à Yeroushalaim.
34:2	Il fit ce qui est droit aux yeux de YHWH. Il marcha sur les voies de David, son père et ne s'en détourna ni à droite ni à gauche.
34:3	La huitième année de son règne, lorsqu'il était jeune, il commença à rechercher l'Elohîm de David, son père, et à la douzième année, il commença à purifier Yéhouda et Yeroushalaim des hauts lieux, des ashérim, des images taillées et des images en métal fondu.
34:4	On renversa en sa présence les autels des Baalim, et il abattit les tentes solaires<!--Tentes solaires : lieux d'idolâtrie.--> qui étaient par-dessus. Il brisa les ashérim, les images taillées et les images en métal fondu, et, les ayant réduites en poussière, il la répandit sur les sépulcres de ceux qui leur avaient sacrifié.
34:5	Puis il brûla les os des prêtres sur leurs autels, et il purifia ainsi Yéhouda et Yeroushalaim.
34:6	Il fit la même chose dans les villes de Menashè, d'Éphraïm et de Shim’ôn, et jusqu'à Nephthali, dans leurs ruines et tout autour.
34:7	Il démolit les autels et mit en pièces les ashérim et les images taillées, et il les réduisit en poussière. Il abattit toutes les tentes solaires dans tout le pays d'Israël. Puis il revint à Yeroushalaim.

### Restauration du temple<!--2 R. 22:3-7.-->

34:8	La dix-huitième année de son règne, après avoir purifié le pays et le temple, il envoya Shaphan, fils d'Atsalyah, et Ma`aseyah, chefs de la ville, et Yoach, fils de Yoachaz, commis sur les registres, pour réparer la maison de YHWH, son Elohîm.
34:9	Ils vinrent vers Chilqiyah, le grand-prêtre et lui donnèrent l'argent qui avait été apporté dans la maison d'Elohîm. Les Lévites gardiens du seuil l'avaient reçu des mains de Menashè, d'Éphraïm et de tout le reste d'Israël ainsi que de tout Yéhouda et Benyamin et des habitants de Yeroushalaim.
34:10	On le remit entre les mains de ceux qui avaient la charge de l'ouvrage, qui étaient préposés sur la maison de YHWH. Et ceux qui avaient la charge de l'ouvrage et qui travaillaient dans la maison de YHWH le distribuèrent pour réparer et fortifier la maison de YHWH.
34:11	Ils le donnèrent aux charpentiers et aux maçons, pour acheter des pierres de taille et du bois pour les poutres et pour la charpente des maisons que les rois de Yéhouda avaient détruites.
34:12	Ces hommes s'employaient fidèlement à cet ouvrage. Yahath et Obadyah, Lévites d'entre les fils de Merari, étaient préposés sur eux, et Zekaryah et Meshoullam, d'entre les fils des Kehathites, pour les diriger. Ces Lévites avaient tous de l'intelligence pour les instruments de musique.
34:13	Ils surveillaient ceux qui portaient les fardeaux, et dirigeaient tous ceux qui faisaient l'ouvrage, dans quelque service que ce soit : les scribes, les administrateurs et les portiers, d'entre les Lévites.

### Le livre de la torah redécouvert<!--2 R. 22:8-10.-->

34:14	Au moment où l'on sortit l'argent qui avait été apporté dans la maison de YHWH, Chilqiyah, le prêtre, trouva le livre de la torah de YHWH, donné par Moshè.
34:15	Alors Chilqiyah, prenant la parole, dit à Shaphan, le secrétaire : J'ai trouvé le livre de la torah dans la maison de YHWH. Et Chilqiyah donna le livre à Shaphan.
34:16	Shaphan apporta le livre au roi, et rapporta tout au roi, en disant : Les mains de tes serviteurs ont fait tout ce qui leur a été donné à faire.
34:17	Ils ont amassé l'argent qui se trouvait dans la maison de YHWH, et l'ont livré entre les mains des administrateurs, et entre les mains de ceux qui ont la charge de l'ouvrage.
34:18	Shaphan, le secrétaire, raconta en disant au roi : Chilqiyah, le prêtre, m'a donné un livre. Et Shaphan le lut devant le roi.

### Lecture du livre de la torah<!--2 R. 22:11-13.-->

34:19	Et il arriva que, quand le roi entendit les paroles de la torah, il déchira ses vêtements.
34:20	Il ordonna à Chilqiyah, à Achikam, fils de Shaphan, à Abdon, fils de Miykah, à Shaphan, le secrétaire, et à Asayah, serviteur du roi, en disant :
34:21	Allez consulter YHWH pour moi et pour ce qui reste en Israël et en Yéhouda, concernant les paroles du livre qui a été trouvé. Car la colère de YHWH est grande, et elle s'est déversée sur nous, parce que nos pères n'ont pas gardé la parole de YHWH, pour faire selon tout ce qui est écrit dans ce livre.

### Instruction d'Houldah, la prophétesse<!--2 R. 22:14-20.-->

34:22	Chilqiyah et les hommes du roi allèrent vers Houldah, la prophétesse, femme de Shalloum, fils de Thokehath, fils de Hasra, garde des vêtements, laquelle demeurait à Yeroushalaim, dans un autre quartier, et lui en parlèrent.
34:23	Alors elle leur répondit : Ainsi parle YHWH, l'Elohîm d'Israël : Dites à l'homme qui vous a envoyés vers moi :
34:24	Ainsi parle YHWH : Voici, je vais faire venir le malheur sur ce lieu et sur ses habitants, toutes les malédictions du serment qui sont écrites dans le livre qu'on a lu devant le roi de Yéhouda.
34:25	Parce qu'ils m'ont abandonné, et qu'ils ont brûlé de l'encens, qu'ils ont brûlé de l'encens à d'autres elohîm, pour m'irriter par toutes les œuvres de leurs mains, ma colère s'est déversée sur ce lieu, et elle ne sera pas éteinte.
34:26	Mais quant au roi de Yéhouda, qui vous a envoyés pour consulter YHWH, vous lui direz : Ainsi parle YHWH, l'Elohîm d'Israël, au sujet des paroles que tu as entendues :
34:27	Parce que ton cœur a été touché, et que tu t'es humilié devant Elohîm, quand tu as entendu ses paroles contre ce lieu et contre ses habitants, et que t'étant humilié devant moi, tu as déchiré tes vêtements et pleuré devant moi, je t'ai aussi entendu, dit YHWH.
34:28	Voici, je vais te recueillir avec tes pères, et tu seras recueilli dans tes sépulcres en paix, et tes yeux ne verront pas tout ce mal que je vais faire venir sur ce lieu et sur ses habitants. Et ils rapportèrent cette parole au roi.

### Renouvellement de l'alliance avec YHWH<!--2 R. 23:1-3.-->

34:29	Alors le roi envoya rassembler tous les anciens de Yéhouda et de Yeroushalaim.
34:30	Le roi monta à la maison de YHWH avec tous les hommes de Yéhouda et les habitants de Yeroushalaim, les prêtres et les Lévites, et tout le peuple, depuis le plus grand jusqu'au plus petit ; et on lut devant eux toutes les paroles du livre de l'alliance, qui avait été trouvé dans la maison de YHWH.
34:31	Et le roi se tint debout à sa place et il traita devant YHWH cette alliance qu'ils suivraient YHWH, et qu'ils garderaient ses commandements, ses témoignages et ses lois, chacun de tout son cœur et de toute son âme, en pratiquant les paroles de l'alliance écrites dans ce livre.
34:32	Et il fit tenir debout tous ceux qui se trouvèrent à Yeroushalaim et en Benyamin, et les habitants de Yeroushalaim firent selon l'alliance d'Elohîm, l'Elohîm de leurs pères.
34:33	Yoshiyah ôta de tous les pays qui appartenaient aux enfants d'Israël, toutes les abominations, et il obligea tous ceux qui se trouvaient en Israël à servir YHWH, leur Elohîm. Pendant toute sa vie, ils ne se détournèrent pas de YHWH, l'Elohîm de leurs pères.

## Chapitre 35

### Yoshiyah (Josias) rétablit la Pâque<!--2 R. 23:21-27.-->

35:1	Or Yoshiyah célébra la Pâque pour YHWH à Yeroushalaim, et on immola la Pâque le quatorzième jour du premier mois.
35:2	Il rétablit les prêtres dans leurs charges, et les encouragea au service de la maison de YHWH.
35:3	Il dit aussi aux Lévites qui font discerner tout Israël et qui étaient consacrés à YHWH : Mettez l'arche sainte dans la maison que Shelomoh, fils de David, roi d'Israël, a bâtie. Qu'elle ne soit plus une charge sur vos épaules. Maintenant, servez YHWH, votre Elohîm, et son peuple d'Israël.
35:4	Tenez-vous prêts, selon les maisons de vos pères, selon vos divisions suivant l'écrit de David, roi d'Israël, et suivant l'écrit de Shelomoh, son fils.
35:5	Tenez-vous dans le lieu saint pour vos frères, les fils du peuple, selon les classes des maisons des pères et selon que chaque famille des Lévites est partagée.
35:6	Immolez la Pâque, sanctifiez-vous, et préparez-la pour vos frères, afin qu'ils puissent la faire selon la parole que YHWH a donnée par Moshè.
35:7	Yoshiyah éleva une offrande pour les gens du peuple et pour tous ceux qui se trouvaient là, des troupeaux d'agneaux et de chevreaux, au nombre de 30 000, et 3 000 bœufs, le tout pour la Pâque. Cela fut pris sur les biens du roi.
35:8	Ses chefs élevèrent une offrande de bon gré pour le peuple, aux prêtres et aux Lévites. Chilqiyah, Zekaryah et Yechiy'el, princes de la maison d'Elohîm, donnèrent aux prêtres, pour la Pâque, 2 600 agneaux, et 300 bœufs.
35:9	Konanyah, Shema’yah et Netanél, ses frères, et Chashabyah, Yéiël et Yozabad, qui étaient les princes des Lévites, élevèrent une offrande de 5 000 agneaux aux Lévites pour faire la Pâque, et 500 bœufs.
35:10	Le service étant préparé, les prêtres se tinrent à leurs postes, et les Lévites suivant leurs divisions, selon l'ordre du roi.
35:11	Puis on immola la Pâque. Les prêtres firent l'aspersion avec le sang reçu de la main des Lévites, et ceux-ci les dépouillaient.
35:12	Ils mirent à part les holocaustes, pour les donner aux gens du peuple, suivant les divisions des maisons de leurs pères, afin de les offrir à YHWH, selon ce qui est écrit dans le livre de Moshè ; ils firent de même pour les bœufs.
35:13	Ils firent cuire la Pâque au feu, selon l'ordonnance. Ils firent cuire dans des chaudières, des chaudrons et des poêles, les choses consacrées. Ils les apportèrent rapidement à tous les enfants du peuple.
35:14	Puis ils apprêtèrent ce qui était pour eux et pour les prêtres, car les prêtres, fils d'Aaron, furent occupés jusqu'à la nuit à élever en offrande les holocaustes et les graisses. C'est pourquoi les Lévites apprêtèrent ce qui était pour eux et pour les prêtres, fils d'Aaron.
35:15	Les chanteurs, fils d'Asaph, étaient à leur place, selon l'ordre de David, d'Asaph, d'Héman et de Yedoutoun, le voyant du roi. Les portiers étaient à chaque porte, ils n'eurent pas à se détourner de leur service, car leurs frères les Lévites apprêtaient ce qui était pour eux.
35:16	Ainsi, tout le service de YHWH, en ce jour-là, fut réglé pour faire la Pâque et pour élever en offrande les holocaustes sur l'autel de YHWH, selon l'ordre du roi Yoshiyah.
35:17	Les fils d'Israël qui s'y trouvèrent célébrèrent donc la Pâque en ce temps-là, et la fête des pains sans levain pendant 7 jours.
35:18	Il ne s'était pas fait de semblable Pâque en Israël depuis les jours de Shemouél le prophète, et aucun des rois d'Israël n'avait fait une Pâque pareille comme le fit Yoshiyah, avec les prêtres et les Lévites, et tout Yéhouda et Israël, qui s'y étaient trouvés avec les habitants de Yeroushalaim.
35:19	Cette Pâque fut célébrée la dix-huitième année du règne de Yoshiyah.

### Blessure et mort de Yoshiyah (Josias)<!--2 R. 23:28-30.-->

35:20	Après tout cela, quand Yoshiyah eut réparé la maison de YHWH, Néco, roi d'Égypte, monta pour faire la guerre à Carkemish, sur l'Euphrate. Yoshiyah sortit à sa rencontre.
35:21	Mais Néco envoya vers lui des messagers pour lui dire : Qu'y a-t-il entre nous, roi de Yéhouda ? Ce n'est pas à toi que j'en veux aujourd'hui, mais à une maison qui me fait la guerre et Elohîm m'a dit de me hâter. Désiste-toi donc de venir contre Elohîm, qui est avec moi, de peur qu'il ne te détruise.
35:22	Cependant Yoshiyah ne détourna pas sa face de lui, mais se déguisa pour combattre contre lui et il n'écouta pas les paroles de Néco, qui venaient de la bouche d'Elohîm. Il vint donc pour combattre dans la vallée de Meguiddo.
35:23	Les archers tirèrent sur le roi Yoshiyah et le roi dit à ses serviteurs : Transportez-moi, car je suis très blessé.
35:24	Ses serviteurs l'ôtèrent du char, le mirent sur un second char qu'il avait, et le menèrent à Yeroushalaim. Il mourut et il fut enseveli dans les sépulcres de ses pères, et tous ceux de Yéhouda et de Yeroushalaim menèrent le deuil de Yoshiyah.
35:25	Yirmeyah chanta un chant funèbre sur Yoshiyah. Tous les chanteurs et toutes les chanteuses ont parlé de Yoshiyah dans leurs chants funèbres jusqu'à ce jour, et on en a fait une coutume en Israël. Voici, ces choses sont écrites dans les chants funèbres.
35:26	Le reste des discours de Yoshiyah, et ses œuvres de piété, selon ce qui est écrit dans la torah de YHWH,
35:27	ses premiers et ses derniers discours sont écrits dans le livre des rois d'Israël et de Yéhouda.

## Chapitre 36

### Yehoachaz règne brièvement sur Yéhouda<!--2 R. 23:31-33.-->

36:1	Alors le peuple du pays prit Yehoachaz, fils de Yoshiyah, et on l'établit roi à Yeroushalaim, à la place de son père.
36:2	Yoachaz était âgé de 23 ans quand il devint roi, et il régna 3 mois à Yeroushalaim.
36:3	Le roi d'Égypte le destitua à Yeroushalaim, et condamna le pays à une amende de 100 talents d'argent et d'un talent d'or.

### Règne de Yehoyaqim, déportation à Babel (Babylone)<!--2 R. 23:34-24:4-9.-->

36:4	Le roi d'Égypte établit pour roi sur Yéhouda et Yeroushalaim Élyakim, frère de Yehoachaz, et il changea son nom en celui de Yehoyaqim. Puis Néco prit Yoachaz, son frère, et l'emmena en Égypte.
36:5	Yehoyaqim était âgé de 25 ans quand il devint roi, et il régna 11 ans à Yeroushalaim. Il fit ce qui est mal aux yeux de YHWH, son Elohîm.
36:6	Neboukadnetsar, roi de Babel, monta contre lui et le lia avec une double chaîne en cuivre pour le mener à Babel.
36:7	Neboukadnetsar emporta aussi à Babel des ustensiles de la maison de YHWH, et il les mit dans son temple à Babel.
36:8	Le reste des actions de Yehoyaqim, et les abominations qu'il commit, et ce qui fut trouvé en lui, cela est écrit dans le livre des rois d'Israël et de Yéhouda. Et Yehoyakin, son fils, régna à sa place.
36:9	Yehoyakin était âgé de 8 ans quand il devint roi, et il régna 3 mois et 10 jours à Yeroushalaim. Il fit ce qui est mal aux yeux de YHWH.

### Tsidqiyah (Sédécias) le dernier roi de Yéhouda, autres déportations à Babel<!--2 R. 24:10-20 ; cp. 2 R. 25:1-21 ; Jé. 39:8-10.-->

36:10	Et l'année suivante, le roi Neboukadnetsar l'envoya chercher et le fit conduire à Babel avec les ustensiles précieux de la maison de YHWH, et il établit roi sur Yéhouda et Yeroushalaim, Tsidqiyah, son frère.
36:11	Tsidqiyah était âgé de 21 ans quand il devint roi, et il régna 11 ans à Yeroushalaim.
36:12	Il fit ce qui est mal aux yeux de YHWH, son Elohîm. Il ne s'humilia pas devant Yirmeyah le prophète, qui lui parlait de la part de YHWH.
36:13	Et même il se rebella contre le roi Neboukadnetsar, qui l'avait fait prêter serment par le Nom d'Elohîm. Il raidit son cou, et il obstina son cœur pour ne pas retourner à YHWH, l'Elohîm d'Israël.
36:14	Pareillement, tous les chefs des prêtres et le peuple furent infidèles et continuèrent de plus en plus à pécher, selon toutes les abominations des nations. Ils rendirent impure la maison que YHWH avait sanctifiée dans Yeroushalaim.
36:15	Or YHWH, l'Elohîm de leurs pères, les avait sommés par ses messagers qu'il envoya de bonne heure, car il voulait épargner son peuple et sa propre demeure.
36:16	Mais ils se moquèrent des messagers d'Elohîm, ils méprisèrent ses paroles et traitèrent ses prophètes de séducteurs, jusqu'à ce que la fureur de YHWH monta contre son peuple au point qu'il n'y eut plus de remède.
36:17	C'est pourquoi il fit monter contre eux le roi des Chaldéens, qui tua par l'épée leurs jeunes hommes dans la maison de leur sanctuaire ; il n'épargna ni le jeune homme, ni la vierge, ni le vieillard, ni l'homme à cheveux blancs ; il les livra tous entre ses mains.
36:18	Il fit apporter à Babel tous les ustensiles de la maison d'Elohîm, grands et petits, les trésors de la maison de YHWH, et les trésors du roi et ceux de ses chefs.
36:19	Ils brûlèrent la maison d'Elohîm, ils démolirent les murailles de Yeroushalaim, ils livrèrent au feu tous ses palais et détruisirent tout ce qu'il y avait comme objets précieux.
36:20	Puis le roi de Babel transporta à Babel le reste qui échappa à l'épée, et ils furent ses esclaves et ceux de ses fils, jusqu'à la domination du royaume de Perse,
36:21	pour accomplir la parole de YHWH par la bouche de Yirmeyah : jusqu'à ce que la terre ait pris plaisir à ses shabbats, elle se reposa tout le temps où elle fut dévastée, pour accomplir les 70 années.

### L'édit de Cyrus autorise les Juifs à retourner dans leurs villes

36:22	Or la première année de Cyrus, roi de Perse, afin que la parole de YHWH prononcée par Yirmeyah soit accomplie, YHWH réveilla l'esprit de Cyrus, roi de Perse, qui fit publier dans tout son royaume, et même par écrit, en disant :
36:23	Ainsi parle Cyrus, roi de Perse : YHWH, l'Elohîm des cieux, m'a donné tous les royaumes de la Terre, et lui-même m'a ordonné de lui bâtir une maison à Yeroushalaim, qui est en Yéhouda. Qui d'entre vous est de son peuple ? Que YHWH, son Elohîm, soit avec lui, et qu'il monte !
