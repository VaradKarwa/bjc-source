# Yonah (Jonas) (Jon.)

Signification : Colombe

Auteur : Yonah (Jonas)

Thème : La miséricorde divine

Date de rédaction : 8ème siècle av. J.-C.

Yonah, contemporain d'Amos, exerça son service dans le royaume du nord sous le règne de Yarobam II (Jéroboam II). Alors que YHWH lui ordonna d'aller prêcher la repentance à Ninive (capitale de l'Empire d'Assyrie), sous peine de la détruire, il refusa et tenta de s'enfuir loin de lui. Toutefois, dans sa miséricorde, Elohîm généra une situation pour pousser Yonah à accomplir sa mission. À travers ce récit, on comprend que la voie de la repentance est ouverte à tous et qu'Elohîm veut sauver tous les humains sans exception.

## Chapitre 1

1:1	Or la parole de YHWH vint à Yonah, fils d'Amitthaï, en disant :

### Désobéissance et fuite de Yonah

1:2	Lève-toi, va à Ninive<!--Capitale de l'Empire d'Assyrie.-->, la grande ville, et crie contre elle ! Car leur méchanceté est montée jusqu'à moi.
1:3	Mais Yonah se leva pour s'enfuir à Tarsis, loin de la face de YHWH et il descendit à Yapho, où il trouva un navire qui allait à Tarsis. Il paya le prix du transport et y entra pour aller à Tarsis, loin de la face de YHWH.
1:4	Mais YHWH fit lever un grand vent sur la mer, et il y eut une grande tempête sur la mer, de sorte que le navire semblait se briser.
1:5	Et les mariniers eurent peur, et ils crièrent chacun à son elohîm, et jetèrent dans la mer les objets qui étaient dans le navire pour l'alléger. Yonah descendit au fond du navire, se coucha et s'endormit profondément.
1:6	Le chef des marins s'approcha de lui et lui dit : Qu'as-tu dormeur ? Lève-toi, crie à ton Elohîm ! Peut-être ton Elohîm pensera à nous et nous ne périrons pas.
1:7	Puis ils se dirent l'un à l'autre : Venez, tirons au sort pour savoir qui est la cause de ce malheur. Ils tirèrent au sort et le sort tomba sur Yonah.
1:8	Alors ils lui dirent : Dis-nous quelle est la cause de ce malheur. Quel est ton métier et d'où viens-tu ? Quel est ton pays et de quel peuple es-tu ?
1:9	Et il leur répondit : Je suis hébreu et je crains YHWH, l'Elohîm des cieux, qui a fait la mer et la terre sèche.
1:10	Alors ces hommes eurent peur, ils furent saisis d'une grande crainte, et lui dirent : Pourquoi as-tu fait cela ? Car ces hommes savaient qu'il fuyait loin de la face de YHWH, parce qu'il le leur avait déclaré.
1:11	Et ils lui dirent : Que te ferons-nous pour que la mer se calme ? Car la mer était de plus en plus agitée.

### Yonah dans le ventre du poisson

1:12	Il leur répondit : Prenez-moi, et jetez-moi dans la mer, et la mer se calmera, car je sais que c'est moi la cause de cette grande tempête.
1:13	Et ces hommes ramaient pour revenir sur la terre sèche, mais ils ne le purent, car la mer s'agitait toujours plus contre eux.
1:14	Alors ils invoquèrent YHWH, et dirent : Oh ! Je te prie YHWH, ne nous fais pas périr à cause de la vie de cet homme, et ne mets pas sur nous le sang innocent ! Car toi, YHWH, tu agis comme il te plaît<!--Ps. 115:3.-->.
1:15	Alors ils prirent Yonah, et le jetèrent dans la mer. Et la fureur de la mer s'arrêta.
1:16	Et ces hommes eurent peur, ils furent saisis d'une grande crainte de YHWH, et ils offrirent des sacrifices à YHWH, et firent des vœux.

## Chapitre 2

### La prière de Yonah exaucée par YHWH

2:1	Or YHWH ordonna à un grand poisson d'engloutir Yonah, et Yonah fut dans le ventre du poisson trois jours et trois nuits.
2:2	Yonah pria YHWH, son Elohîm, dans le ventre du poisson.
2:3	Et il dit : Dans ma détresse, j'ai invoqué YHWH et il m'a exaucé. Du sein du shéol, j'ai crié au secours et tu as entendu ma voix<!--Ps. 18:5-7.-->.
2:4	Tu m'as jeté dans les profondeurs, au cœur de la mer, et le courant m'a environné. Tous tes flots et toutes tes vagues ont passé sur moi.
2:5	Je disais : Je suis chassé loin de tes yeux ! Cependant, je verrai encore le temple de ta sainteté.
2:6	Les eaux m'ont environné jusqu'à l'âme. L'abîme m'a enveloppé, les roseaux ont lié ma tête.
2:7	Je suis descendu jusqu'aux bases des montagnes, la terre fermait sur moi ses barres pour toujours, mais tu m'as fait monter vivant de la fosse, YHWH, mon Elohîm !
2:8	Quand mon âme s'était affaiblie en moi, je me suis souvenu de YHWH, et ma prière est parvenue à toi, jusqu'au palais de ta sainteté.
2:9	Ceux qui s'adonnent à des vanités mensongères abandonnent ta miséricorde.
2:10	Mais moi, je t'offrirai des sacrifices avec un cri d'actions de grâces, j'accomplirai les vœux que j'ai faits. Car le salut vient de YHWH<!--Le mot « salut », de l'hébreu « Yeshuw'ah », a la même racine que le Nom de notre Seigneur : « Yéhoshoua ». Yonah (Jonas) a compris que seul Yéhoshoua (Jésus) pouvait le sauver d'une mort certaine.-->.
2:11	Alors YHWH parla au poisson, et le poisson vomit Yonah sur la terre sèche.

## Chapitre 3

### Repentance nationale à Ninive

3:1	La parole de YHWH vint à Yonah une seconde fois, en disant :
3:2	Lève-toi, va à Ninive, la grande ville, et proclames-y la publication que je t'ordonne !
3:3	Yonah se leva et alla à Ninive, suivant la parole de YHWH. Or Ninive était une grande ville devant Elohîm, de trois jours de marche.
3:4	Et Yonah entra dans la ville et commença par y marcher toute une journée. Il criait et disait : Encore 40 jours, et Ninive sera renversée !
3:5	Les gens de Ninive crurent en Elohîm, ils publièrent un jeûne et se vêtirent de sacs, depuis le plus grand d'entre eux jusqu'au plus petit.
3:6	Et cette parole parvint au roi de Ninive. Il se leva de son trône, ôta son manteau, se couvrit d'un sac et s'assit sur la cendre.
3:7	Puis, il fit faire une proclamation et publier dans Ninive par décret du roi et de ses grands : Que les humains, les bêtes, les bœufs et les brebis, ne goûtent de rien, ne paissent pas et ne boivent pas d'eau !
3:8	Que les humains et les bêtes soient couverts de sacs, qu'ils crient à Elohîm avec force, et que chacun revienne de sa mauvaise voie, des actions violentes que ses mains ont commises !
3:9	Qui sait si Elohîm ne reviendra pas et ne se repentira pas, et s'il ne reviendra pas de son ardente colère, en sorte que nous ne périssions pas ?
3:10	Et Elohîm vit leurs œuvres : ils revenaient de leur mauvaise voie. Alors Elohîm se repentit du mal qu'il avait dit qu'il leur ferait et ne le fit pas.

## Chapitre 4

### La miséricorde est accordée à Ninive ; Yonah mécontent

4:1	Yonah le prit mal, très mal et il se fâcha.
4:2	Il pria YHWH et dit : Oh ! Je te prie YHWH, n'est-ce pas là ce que je disais quand j'étais encore dans mon pays ? C'est pourquoi j'ai voulu m'enfuir à Tarsis. Car je savais que tu es un El compatissant, miséricordieux, lent à la colère et riche en bonté, et qui te repens du mal<!--Joë. 2:13.-->.
4:3	Maintenant donc, YHWH, prends-moi donc la vie, car la mort vaut mieux pour moi que la vie.

### YHWH réprimande Yonah

4:4	Et YHWH répondit : Fais-tu bien de te fâcher ?
4:5	Alors Yonah sortit de la ville et s'assit à l'orient de la ville, là il se fit une cabane et y resta à l'ombre, jusqu'à ce qu'il vît ce qui arriverait à la ville.
4:6	YHWH Elohîm ordonna à un ricin de croître au-dessus de Yonah, pour donner de l'ombre sur sa tête et pour le délivrer de son mal. Yonah éprouva une grande joie à cause de ce ricin.
4:7	Puis Elohîm prépara pour le lendemain, lorsque l'aube du jour monta, un ver qui frappa le ricin, et il sécha.
4:8	Et il arriva que quand le soleil fut levé, Elohîm prépara un vent chaud d'orient qu'on n'apercevait pas, et le soleil frappa la tête de Yonah, au point qu'il s'évanouit. Il demanda la mort pour son âme et dit : La mort est meilleure pour moi que la vie.
4:9	Elohîm dit à Yonah : Fais-tu bien de te fâcher ainsi au sujet de ce ricin ? Et il répondit : Je fais bien de me fâcher jusqu'à la mort.
4:10	Et YHWH dit : Tu as pitié du ricin pour lequel tu n'as pas travaillé et que tu n'as pas fait croître, qui a été le fils d'une nuit et a péri comme le fils d'une nuit.
4:11	Et moi, n'épargnerais-je pas Ninive, cette grande ville, dans laquelle il y a plus de 120 000 humains qui ne savent pas distinguer leur main droite de leur main gauche, et où il y a aussi une grande quantité de bêtes ?
