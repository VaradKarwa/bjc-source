# Zekaryah (Zacharie) (Za.)

Signification : Yah se souvient

Auteur : Zekaryah (Zacharie)

Thème : Les deux avènements du Mashiah

Date de rédaction : 6ème siècle av. J.-C.

Zekaryah, contemporain de Chaggay (Aggée), exerça son service en Yéhouda (Juda) au retour des exilés de Babel (Babylone), où il était né. Il annonça la venue du Mashiah et raconta de manière très précise différents épisodes de sa vie que l'on retrouve également dans les évangiles. Il dévoila aussi quelques-uns des attributs du Sauveur ; qui serait premièrement rejeté, mais finalement accepté par le peuple juif pendant le millénium. Le Mashiah est ici présenté comme le Grand-Prêtre, le Germe, le Roi de paix, le Fils de David...

## Chapitre 1

### YHWH avertit son peuple

1:1	Le huitième mois de la deuxième année de Darius, la parole de YHWH vint à Zekaryah, le prophète, fils de Berekyah, fils d'Iddo, en disant :
1:2	YHWH a été irrité, il a été irrité contre vos pères.
1:3	C'est pourquoi tu leur diras : Ainsi parle YHWH Sabaoth : Revenez à moi, dit YHWH Sabaoth, et je reviendrai à vous, dit YHWH Sabaoth<!--Es. 31:6 ; Jé. 3:12 ; Joë. 2:12.-->.
1:4	Ne soyez pas comme vos pères, auxquels les précédents prophètes criaient, en disant : Ainsi parle YHWH Sabaoth : Détournez-vous de vos mauvaises voies et de vos mauvaises actions ; mais ils n'ont pas écouté et n'ont pas été attentifs à ce que je leur disais, dit YHWH<!--La. 5:7 ; Esd. 9:7 ; Né. 9:16 ; 2 Ch. 29:6.-->.
1:5	Vos pères, où sont-ils ? Et ces prophètes-là devaient-ils vivre éternellement ?
1:6	Cependant mes paroles et mes ordonnances que j'avais données aux prophètes, mes serviteurs, n'ont-elles pas atteint vos pères ? De sorte que s'étant convertis, ils ont dit : Comme YHWH Sabaoth avait résolu d'agir à notre égard, selon nos voies et selon nos actions, ainsi il a agi envers nous.

### Le cavalier sur un cheval roux

1:7	Le vingt-quatrième jour du onzième mois, qui est le mois de Shebat, dans la deuxième année de Darius, la parole de YHWH vint à Zekaryah, le prophète, fils de Berekyah, fils d'Iddo, en disant :
1:8	Je voyais de nuit une vision, et voici, un homme était monté sur un cheval roux et il se tenait parmi les myrtes qui étaient dans un lieu creux. Il y avait derrière lui des chevaux roux, fauves et blancs<!--Ap. 6:2-4.-->.
1:9	Et je dis : Mon Seigneur ! Que signifient ces choses ? Et l'ange qui parlait avec moi me dit : Je te montrerai ce que signifient ces choses.
1:10	Et l'homme qui se tenait parmi les myrtes répondit et dit : Ce sont ceux que YHWH a envoyés pour parcourir la terre.
1:11	Et ils répondirent à l'Ange de YHWH qui se tenait parmi les myrtes, et dirent : Nous avons parcouru la Terre et voici que toute la Terre est en repos et tranquille.

### Compassion de YHWH pour Yeroushalaim (Jérusalem)

1:12	Alors l'Ange de YHWH répondit et dit : YHWH Sabaoth, jusqu'à quand n'auras-tu pas compassion de Yeroushalaim et des villes de Yéhouda, contre lesquelles tu es irrité depuis 70 ans<!--Yirmeyah (Jérémie) prophétisa que la captivité babylonienne durerait 70 ans (Jé. 25:11-12, 29:10). Les 70 ans commencèrent à la déportation de la famille royale à Babel (Babylone) en 605 av. J.-C. (2 R. 24 ; Da. 1) et se terminèrent avec la première vague de retours conduite par Zorobabel (Esd. 1). Les Israélites furent emmenés en captivité en plusieurs vagues. Le livre d'Esdras (Ezra) raconte les deux premières. En 538 av. J.-C., Zorobabel mena la première vague et fut nommé gouverneur (Ag. 1:1). Le grand-prêtre Yéhoshoua (Esd. 3:2) et les prophètes Chaggay (Aggée) et Zekaryah (Esd. 5:1-2) le secondaient. Leur plus grand défi fut de rebâtir le temple. Puisque la seule tribu à être retournée en masse fut celle de Yéhouda, le reste du peuple fut appelé « les Juifs » (Esd. 4:23).--> ?
1:13	Et YHWH répondit à l'Ange qui parlait avec moi, par de bonnes paroles, par des paroles de consolation.
1:14	Puis l'Ange qui parlait me dit : Crie, en disant : Ainsi parle YHWH Sabaoth : Je suis jaloux pour Yeroushalaim et pour Sion d'une grande jalousie,
1:15	et je suis dans une grande colère et fâché contre les nations qui sont à leur aise, car je n'étais que peu irrité, mais elles ont aidé au mal.
1:16	C'est pourquoi ainsi parle YHWH : Je suis revenu à Yeroushalaim avec compassion. Ma maison y sera rebâtie, dit YHWH Sabaoth, et le cordeau sera étendu sur Yeroushalaim.
1:17	Crie encore en disant : Ainsi parle YHWH Sabaoth : Mes villes regorgeront encore de biens, et YHWH consolera encore Sion, et il choisira encore Yeroushalaim.

### Vision des quatre cornes et des quatre forgerons

1:18	Puis je levai les yeux et je regardai, et je vis quatre cornes<!--Da. 7:7-11, 8:22 ; Ap. 13:1-11.-->.
1:19	Alors je dis à l'Ange qui parlait avec moi : Que veulent dire ces choses ? Et il me répondit : Ce sont les cornes qui ont dispersé Yéhouda, Israël et Yeroushalaim.
1:20	Puis YHWH me montra quatre forgerons.
1:21	Et je dis : Que viennent-ils faire ? Et il répondit et dit : Ce sont les cornes qui ont dispersé Yéhouda au point que personne ne lève la tête ; mais ceux-ci sont venus pour les effrayer, et pour abattre les cornes des nations qui ont levé la corne contre le pays de Yéhouda pour le disperser.

## Chapitre 2

### Yeroushalaim (Jérusalem) mesurée au cordeau

2:1	Je levai encore mes yeux et je regardai, et voici, un homme qui avait à la main un cordeau pour mesurer,
2:2	auquel je dis : Où vas-tu ? Et il me répondit : Je vais mesurer Yeroushalaim pour voir quelle est sa largeur et quelle est sa longueur.
2:3	Et voici, l'Ange qui parlait avec moi sortit, et un autre ange sortit à sa rencontre.

### YHWH, la gloire de Yeroushalaim (Jérusalem)

2:4	Et il lui dit : Cours, et parle à ce jeune homme-là en disant : Yeroushalaim sera habitée comme les villes ouvertes à cause de la multitude d'êtres humains et de bêtes qui seront au milieu d'elle<!--Né. 1:3, 2:13.-->.
2:5	Mais je serai pour elle, dit YHWH, une muraille de feu tout autour, et je serai sa gloire au milieu d'elle<!--Es. 60:19.-->.
2:6	Ha ! Ha ! Fuyez, dit YHWH, fuyez hors du pays du nord ! Car je vous ai dispersés vers les quatre vents des cieux, dit YHWH.
2:7	Ha ! Sion qui demeure avec la fille de Babel<!--Es. 48:20, 52:11 ; Jé. 50:8, 51:6.-->, Sauve-toi !
2:8	Car ainsi parle YHWH Sabaoth, lequel après la gloire, m'a envoyé vers les nations qui vous ont pillées. Car celui qui vous touche, touche à la prunelle de son œil<!--De. 32:10 ; Ps. 17:8.-->.
2:9	Car voici, je vais lever ma main contre elles, et elles seront la proie de leurs serviteurs, et vous saurez que YHWH Sabaoth m'a envoyé.
2:10	Pousse des cris de joie et réjouis-toi, fille de Sion ! Car voici, je viens<!--Yéhoshoua ha Mashiah (Jésus-Christ) est YHWH qui vient ! C'est la seconde venue de Yéhoshoua ha Mashiah qui est évoquée ici (Es. 40:10-11 ; Za. 12:10-14, 14:1-10 ; Ac. 1:1-11 ; Ap. 1:7-8, 22:12-17).--> et j'habiterai au milieu de toi, dit YHWH.
2:11	Beaucoup de nations se joindront à YHWH en ce jour-là et deviendront mon peuple. J'habiterai au milieu de toi, et tu sauras que YHWH Sabaoth m'a envoyé vers toi.
2:12	Et YHWH possédera Yéhouda comme sa part dans la terre sainte, et il choisira encore Yeroushalaim.
2:13	Que toute chair se taise devant la face de YHWH, car il s'est réveillé de sa demeure sainte !

## Chapitre 3

### Yéhoshoua, le grand-prêtre justifié ; promesse de la venue du Germe de YHWH

3:1	Puis YHWH me fit voir Yéhoshoua, le grand-prêtre, se tenant debout devant l'Ange de YHWH, et Satan qui se tenait debout à sa droite pour s'opposer à lui<!--Dans le but de l'accuser.-->.
3:2	Et YHWH dit à Satan : Que YHWH te réprime, Satan ! Que YHWH qui a choisi Yeroushalaim, te réprime ! N'est-ce pas là un tison sauvé du feu<!--Am. 4:11 ; Jud. 1:9.--> ?
3:3	Or Yéhoshoua était vêtu de vêtements sales et il se tenait debout devant l'Ange.
3:4	L'Ange prit la parole et parla à ceux qui étaient debout devant lui, disant : Ôtez-lui ces vêtements sales ! Puis il lui dit : Regarde, je t'enlève ton iniquité et je te revêts d'habits de fête.
3:5	Je dis : Qu'on mette sur sa tête un turban pur ! Et ils mirent un turban pur sur sa tête, puis ils lui mirent des vêtements<!--Ap. 19:8.-->. L'Ange de YHWH était présent.
3:6	Alors l'Ange de YHWH fit cette déclaration à Yéhoshoua et dit :
3:7	Ainsi parle YHWH Sabaoth : Si tu marches dans mes voies et si tu observes mes commandements, tu jugeras ma maison, tu garderas mes parvis et je te donnerai libre accès parmi ceux qui sont là debout.
3:8	Écoute maintenant Yéhoshoua, grand-prêtre, toi et tes compagnons qui sont assis devant toi, car ce sont des hommes qui serviront de signes. Certainement voici, je ferai venir mon serviteur, le Germe<!--Le Germe est un autre nom de Yéhoshoua ha Mashiah (Jésus-Christ), notre Seigneur (Es. 4:2).-->.
3:9	Car voici, quant à la pierre<!--Yéhoshoua ha Mashiah (Jésus-Christ) est le Rocher des âges (Es. 8:13-17 ; Ap. 5:1-7).--> que j'ai mise devant Yéhoshoua, sur cette pierre qui n'est qu'une<!--Cette pierre est une (« e'had »), c'est-à-dire indivisible (De. 6:4).-->, il y a sept yeux. Voici, je graverai moi-même ce qui doit y être gravé, dit YHWH Sabaoth. J'ôterai en un jour l'iniquité de ce pays.
3:10	En ce jour-là, dit YHWH Sabaoth, chacun de vous appellera son prochain sous la vigne et sous le figuier.

## Chapitre 4

### Le chandelier d'or et ses sept lampes au milieu des deux oliviers

4:1	Puis l'Ange qui me parlait revint, et il me réveilla comme un homme que l'on réveille de son sommeil.
4:2	Il me dit : Que vois-tu ? Et je répondis : Je regarde, et voici, il y a un chandelier tout en or, surmonté d'un vase et portant ses sept lampes, avec sept conduits pour les sept lampes qui sont au sommet du chandelier<!--Ap. 1:12-13.-->.
4:3	Et il y a deux oliviers près de lui, l'un à la droite du vase et l'autre à sa gauche.
4:4	Alors je pris la parole et je dis à l'Ange qui me parlait : Mon Seigneur, que signifient ces choses ?
4:5	L'Ange qui me parlait répondit et me dit : Ne sais-tu pas ce que signifient ces choses ? Je dis : Non, mon Seigneur !
4:6	Alors il répondit et me parla, disant : Ceci est la parole que YHWH adresse à Zorobabel : Ce n'est pas par la puissance ni par la force, mais par mon Esprit, dit YHWH Sabaoth.
4:7	Qui es-tu, grande montagne, devant Zorobabel ? Tu seras aplanie. Il fera sortir la pierre du sommet aux cris de : Grâce, grâce pour elle !

### Zorobabel encouragé à achever l'œuvre

4:8	De nouveau la parole de YHWH vint à moi en disant :
4:9	Les mains de Zorobabel ont fondé cette maison, et ses mains l'achèveront. Et tu sauras que YHWH Sabaoth m'a envoyé vers vous.
4:10	Car qui est-ce qui a méprisé le jour des faibles commencements ? Ils se réjouiront en voyant la pierre d'étain dans la main de Zorobabel. Ces sept<!--Les sept yeux de YHWH sont aussi les sept yeux de l'Agneau (Ap. 5:6). Ces yeux représentent l'omniscience et l'omniprésence de Yéhoshoua ha Mashiah (Jésus-Christ) (Za. 3:9, 4:10, 14:7 ; Jn. 16:30 ; Ac. 1:24 ; Ap. 21:17).--> sont les yeux de YHWH qui parcourent toute la Terre.
4:11	Je répondis et lui dis : Que signifient ces deux oliviers, à la droite et à la gauche du chandelier ?
4:12	Je pris la parole pour la seconde fois et je lui dis : Que signifient ces deux branches d'olivier qui sont près des deux conduits d'or, d'où l'or découle ?
4:13	Il me répondit et dit : Ne sais-tu pas ce que signifient ces choses ? Et je dis : Non, mon Seigneur.
4:14	Et il dit : Ce sont les deux fils<!--L'identité de ces deux individus est inconnue.--> de l'huile qui se tiennent devant le Seigneur de toute la Terre.

## Chapitre 5

### La malédiction répandue sur Israël

5:1	Je me retournai et levai mes yeux pour regarder et je vis un rouleau qui volait.
5:2	Alors il me dit : Que vois-tu ? Je répondis : Je vois un rouleau qui vole dont la longueur est de 20 coudées et la largeur de 10 coudées.
5:3	Il me dit : C'est la malédiction qui sort sur la face de tout le pays. Car selon elle tout voleur sera chassé d'ici, et selon elle, toute personne qui jure faussement par mon Nom sera chassée d'ici.
5:4	Je déploierai cette malédiction dit YHWH Sabaoth, et elle entrera dans la maison du voleur et dans la maison de celui qui jure faussement en mon Nom, et elle logera au milieu de leur maison et la consumera avec son bois et ses pierres.

### L'épha s'en va au pays de Shinear

5:5	L'Ange qui me parlait sortit et me dit : Lève maintenant tes yeux et regarde ce qui sort là !
5:6	Et je dis : Qu'est-ce que c'est ? Et il répondit : C'est l'épha<!--L'épha était une unité de mesure utilisée dans le commerce des céréales, souvent à des fins frauduleuses (De. 25:14 ; Am. 8:5 ; Mi. 6:10.).--> qui sort dehors. Puis il dit : C'est leur œil sur toute la terre.
5:7	Et voici, on portait un disque de plomb et une femme était assise au milieu de l'épha<!--Cette femme représente la grande prostituée décrite en Ap. 17, avec sa coupe d'or pleine d'abominations et des impuretés de sa relation sexuelle illicite (v. 4). Cette femme est la figure du « mystère de l'iniquité qui est déjà à l'œuvre » (2 Th. 2:7).-->.
5:8	Il dit : C'est là la méchanceté<!--Ou l'iniquité.-->. Puis il la jeta au milieu de l'épha, et il jeta la pierre de plomb sur l'ouverture.
5:9	Je levai les yeux et je regardai, et voici, deux femmes sortirent<!--Ces deux femmes sont décrites avec « des ailes de cigogne » (oiseau impur sous Moshè : Lé. 11:19) et portées par le vent (dans les Écritures, le vent est constamment lié au jugement : Es. 7:2, 26:6, 41:16 ; Job 27:20-22, 30:22).-->. Le vent soufflait dans leurs ailes : elles avaient des ailes comme les ailes de la cigogne. Et elles enlevèrent l'épha entre la terre et le ciel.
5:10	Je dis à l'Ange qui me parlait : Où emportent-elles l'épha ?
5:11	Il me répondit : C'est pour lui bâtir une maison dans le pays de Shinear<!--Shinear ou Babylonie (Chaldée) (Ge. 10:6-12).-->. Et quand elle sera fermement établie, il sera déposé là sur sa base.

## Chapitre 6

### Les quatre vents des cieux

6:1	Je levai encore les yeux et je regardai, et voici, quatre chars<!--Dans les Écritures, les chars et les chevaux représentent souvent la puissance d'Elohîm exerçant un jugement sur la terre (Jé. 46:9-10 ; Joë. 2:3-11). Ce jugement concerne le monde entier (Ap. 6:1-8).--> sortaient d'entre deux montagnes. Et ces montagnes étaient des montagnes de cuivre.
6:2	Au premier char, il y avait des chevaux roux, au deuxième char, des chevaux noirs,
6:3	au troisième char, des chevaux blancs, et au quatrième char, des chevaux tachetés et vigoureux<!--Vient d'un mot hébreu qui signifie « fort, pommelé, couleur d'une pie ».-->.
6:4	Je pris la parole et je dis à l'Ange qui me parlait : Mon Seigneur, que veulent dire ces choses ?
6:5	L'ange répondit et me dit : Ce sont les quatre vents des cieux, qui sortent du lieu où ils se tenaient devant le Seigneur de toute la Terre.
6:6	Les chevaux noirs qui sont à l'un des chars, se dirigent vers le pays du nord, et les blancs sortent après eux, et les tachetés se dirigent vers le pays du sud.
6:7	Ensuite les vigoureux sortirent et demandèrent à aller parcourir la Terre. L'Ange leur dit : Allez et parcourez la Terre ! Et ils parcoururent la Terre.
6:8	Puis il m'appela et me parla, en disant : Voici, ceux qui se dirigent vers le pays du nord ont apaisé mon Esprit dans le pays du nord.

### Le règne du Germe de YHWH

6:9	Et la parole de YHWH vint à moi, en disant :
6:10	Prends de la main des exilés : de Heldaï, de Tobiyah et de Yekda`yah, et tu iras toi-même ce jour-là, tu iras dans la maison de Yoshiyah, fils de Tsephanyah, où ils se sont rendus en arrivant de Babel.
6:11	Prends de l'argent et de l'or et fais-en des couronnes, et mets-les sur la tête de Yéhoshoua, fils de Yehotsadaq, le grand-prêtre.
6:12	Et parle-lui, en disant : Ainsi a parlé YHWH Sabaoth, disant : Voici un homme dont le nom est Germe<!--Es. 4:2.-->, qui germera de son lieu et qui bâtira le temple de YHWH<!--C'est YHWH, c'est-à-dire Yéhoshoua ha Mashiah (Jésus-Christ) lui-même, qui bâtit son temple (Ps. 127:1-2 ; Mt. 16:18).-->.
6:13	C'est lui qui bâtira le temple de YHWH et y portera la majesté. Il s'assiéra et dominera sur son trône, il sera prêtre<!--Yéhoshoua ha Mashiah (Jésus-Christ) est notre Grand-Prêtre (Hé. 6:20, 7:1-28).--> sur son trône ; et entre eux deux il y aura un conseil de paix.
6:14	Les couronnes seront pour Hélem, Tobiyah et Yekda`yah, et pour Hen, fils de Tsephanyah, un souvenir dans le temple de YHWH.
6:15	Et ceux qui sont éloignés viendront et bâtiront le temple de YHWH. Vous saurez que YHWH Sabaoth m'a envoyé vers vous. Et ceci arrivera si vous écoutez, si vous écoutez la voix de YHWH votre Elohîm.

## Chapitre 7

### Le jeûne formaliste

7:1	Puis il arriva la quatrième année du roi Darius, que la parole de YHWH vint à Zekaryah, le quatrième jour du neuvième mois, qui est le mois de Kisleu.
7:2	Lorsqu'on eut envoyé Sharetser et Réguem-Mélec avec ses gens à Béth-El pour supplier la face de YHWH,
7:3	et pour parler aux prêtres de la maison de YHWH Sabaoth, et aux prophètes, en disant : Dois-je pleurer au cinquième mois, me tenant séparé comme j'ai déjà fait pendant plusieurs années ?
7:4	La parole de YHWH Sabaoth vint à moi en disant :
7:5	Parle à tout le peuple du pays et aux prêtres, et dis-leur : Quand vous avez jeûné et pleuré au cinquième mois et au septième, et cela depuis 70 ans, avez-vous célébré ce jeûne par amour pour moi ?
7:6	Et quand vous buvez et mangez, n'est-ce pas vous qui mangez et vous qui buvez<!--Es. 58:3-4.--> ?
7:7	Ne connaissez-vous pas les paroles qu'a proclamées YHWH par le moyen des premiers prophètes, lorsque Yeroushalaim était habitée et paisible avec ses villes alentours et que le midi et la plaine étaient habités ?

### YHWH n'exauce pas les pécheurs

7:8	Puis la parole de YHWH vint à Zekaryah en disant :
7:9	Ainsi parlait YHWH Sabaoth, en disant : Rendez véritablement la justice et exercez la miséricorde et la compassion chacun envers son frère.
7:10	N'opprimez pas la veuve et l'orphelin, l'étranger et le pauvre, et ne méditez aucun mal dans vos cœurs chacun contre son frère<!--Ex. 22:21 ; Es. 1:23 ; Jé. 5:28 ; Pr. 22:22-23.-->.
7:11	Mais ils n'ont pas voulu écouter, ils eurent l'épaule rebelle, ils ont appesanti leurs oreilles pour ne pas entendre.
7:12	Et ils ont rendu leur cœur dur comme le diamant pour ne pas écouter la torah et les paroles que YHWH Sabaoth adressait par son Esprit, par le moyen des premiers prophètes. C'est pourquoi il y a eu une grande colère de la part de YHWH Sabaoth.
7:13	Et il arriva que, comme il appelait et qu'ils n'ont pas écouté, de même ils ont appelé et je n'ai pas écouté dit YHWH Sabaoth<!--Es. 1:15 ; Jé. 11:11 ; Pr. 1:28.-->.
7:14	Je les ai dispersés comme par un tourbillon parmi toutes les nations qu'ils ne connaissaient pas. Le pays a été dévasté derrière eux, il n'y a plus eu ni allants ni venants. Et d'un pays de délices ils ont fait un désert.

## Chapitre 8

### Rétablissement du royaume d'Israël

8:1	De nouveau la parole de YHWH Sabaoth vint à moi en disant :
8:2	Ainsi parle YHWH Sabaoth : Je suis jaloux pour Sion d'une grande jalousie, et je suis jaloux pour elle d'une grande fureur.
8:3	Ainsi parle YHWH : Je retourne à Sion, et j'habiterai au milieu de Yeroushalaim. Et Yeroushalaim sera appelée ville fidèle, et la montagne de YHWH Sabaoth sera appelée montagne sainte<!--Es. 1:26.-->.
8:4	Ainsi parle YHWH Sabaoth : Il y aura encore des vieillards et des femmes âgées, assis dans les rues de Yeroushalaim, et chacun aura son bâton à la main, à cause du grand nombre de leurs jours.
8:5	Les places de la ville seront remplies de fils et de filles, jouant dans ses places.
8:6	Ainsi parle YHWH Sabaoth : Si cela paraît merveilleux aux yeux du reste de ce peuple en ces jours-là, à mes yeux aussi cela sera-t-il merveilleux ? dit YHWH Sabaoth
8:7	Ainsi parle YHWH Sabaoth : Voici, je délivre mon peuple du pays de l'orient et du pays du soleil couchant.
8:8	Et je les ferai venir et ils habiteront dans Yeroushalaim, et ils seront mon peuple, et je serai leur Elohîm dans la vérité et la justice.

### Promesse de délivrance

8:9	Ainsi parle YHWH Sabaoth : Que vos mains soient fortifiées, vous qui entendez aujourd'hui ces paroles de la bouche des prophètes qui parurent au jour où la maison de YHWH fut fondée, et où le temple allait être bâti<!--Ag. 2:4.-->.
8:10	Car avant ces jours-là, il n'y avait pas de salaire pour l'être humain ni de salaire pour la bête. Il n'y avait pas de paix pour ceux qui entraient et sortaient, à cause de la détresse, et je lâchais tous les hommes les uns contre les autres.
8:11	Mais maintenant je ne serai pas pour le reste de ce peuple comme les premiers jours, dit YHWH Sabaoth.
8:12	Car les semailles prospéreront, la semence de paix sera là : la vigne rendra son fruit, la terre donnera ses produits, et les cieux donneront leur rosée. Je ferai hériter toutes ces choses au reste de ce peuple.
8:13	Et il arrivera que comme vous avez été une malédiction parmi les nations, maison de Yéhouda et maison d'Israël, ainsi je vous sauverai et vous serez une bénédiction. N'ayez pas peur et fortifiez vos mains<!--Ge. 1:11 ; Ap. 22:2.--> !
8:14	Car ainsi parle YHWH Sabaoth : Comme j'ai eu la pensée de vous affliger, lorsque vos pères ont provoqué ma colère, dit YHWH Sabaoth, et que je ne m'en suis pas repenti,
8:15	ainsi je reviens en arrière et j'ai résolu en ces jours de faire du bien à Yeroushalaim et à la maison de Yéhouda. N'ayez pas peur !

### Ordonnances pour le peuple

8:16	Voici les choses que vous devez faire : Que chacun dise la vérité à son prochain, jugez selon la vérité et prononcez un jugement en vue de la paix dans vos portes<!--Ex. 20:16 ; Mt. 19:18 ; Lu. 18:20 ; Ep. 4:25.-->,
8:17	que personne ne projette du mal dans son cœur contre son prochain et n'aimez pas le faux serment, car ce sont là des choses que je hais, dit YHWH<!--Ps. 5:5, 11:5 ; Pr. 6:16-19.-->.
8:18	Puis la parole de YHWH Sabaoth vint à moi en disant :
8:19	Ainsi parle YHWH Sabaoth : Le jeûne du quatrième mois, le jeûne du cinquième, le jeûne du septième et le jeûne du dixième seront changés pour la maison de Yéhouda en joie et en allégresse, et en fêtes solennelles de réjouissance. Aimez donc la vérité et la paix<!--Ep. 4:15.-->.

### Les nations adoreront YHWH, le seul Elohîm

8:20	Ainsi parle YHWH Sabaoth : Il viendra encore des peuples et des habitants de plusieurs villes.
8:21	Les habitants d'une ville iront à l'autre, en disant : Allons, allons implorer la face de YHWH et chercher YHWH Sabaoth ! Nous irons aussi !
8:22	Et beaucoup de peuples et de puissantes nations viendront rechercher YHWH Sabaoth à Yeroushalaim<!--Jérusalem est appelée à devenir le centre d'adoration de la terre et la capitale du monde à cause de la présence d'Elohîm (Es. 66:23 ; Za. 14:16-21).-->, et implorer la face de YHWH.
8:23	Ainsi parle YHWH Sabaoth : Il arrivera en ces jours-là, que dix hommes de toutes les langues des nations prendront et tiendront le pan de la robe d'un homme juif, et diront : Nous irons avec vous, car nous avons entendu qu'Elohîm est avec vous.

## Chapitre 9

### YHWH juge les villes aux alentours d'Israël

9:1	Prophétie, parole de YHWH sur le pays de Hadrac. Elle s'arrête sur Damas, car YHWH a l'œil sur les humains et sur toutes les tribus d'Israël.
9:2	Elle s'arrête aussi sur Hamath, à la frontière de Damas, sur Tyr et Sidon, quoique chacune d'elles soit très sage.
9:3	Car Tyr s'est bâti une forteresse. Elle a amassé l'argent comme la poussière et l'or fin comme la boue des rues<!--Ez. 28:3-17.-->.
9:4	Voici qu'Adonaï en prendra possession, il frappera sur mer sa puissance, et elle-même sera dévorée par le feu<!--Ez. 26:3-4.-->.
9:5	Askalon le verra, et elle sera dans la crainte, Gaza aussi le verra, et un violent tremblement la saisira, ainsi qu'Ékron, car son espoir sera confondu. Et il n'y aura plus de roi à Gaza, et Askalon ne sera plus habitée<!--So. 2:4.-->.
9:6	Et un bâtard habitera dans Asdod, et j'abattrai l'orgueil des Philistins.
9:7	J'ôterai le sang de la bouche de chacun d'eux et leurs abominations d'entre leurs dents. Lui aussi restera pour notre Elohîm, il sera comme un chef en Yéhouda, et Ékron sera comme le Yebousien.
9:8	Je camperai autour de ma maison pour la défendre contre une armée, contre les allants et les venants, et l'oppresseur ne passera plus près d'eux, car maintenant mes yeux sont fixés sur elle.

### Prophétie sur la première venue du Mashiah

9:9	Sois transportée d'allégresse, fille de Sion ! Pousse des cris de joie, fille de Yeroushalaim ! Voici, ton Roi vient à toi, il est juste et vainqueur, il est monté sur un âne, sur un âne, le petit d'une ânesse<!--Cette prophétie s'est accomplie 500 ans après. Effectivement, Yéhoshoua (Jésus) est entré à Yeroushalaim (Jérusalem) monté sur un âne (Mt. 21:1-11 ; Lu. 19:28-40 ; Jn. 12:12-19).-->.

### YHWH délivrera son peuple

9:10	Je retrancherai d'Éphraïm les chars et de Yeroushalaim les chevaux, et les arcs de guerre seront aussi retranchés. Et le Roi parlera de paix aux nations, et sa domination s'étendra d'une mer à l'autre, depuis le fleuve jusqu'aux extrémités de la Terre<!--Es. 57:19 ; Ps. 2:8, 72:8.-->.
9:11	Quant à toi, à cause de ton alliance scellée par le sang, je retirerai tes captifs de la fosse où il n'y a pas d'eau.
9:12	Retournez à la forteresse, captifs pleins d'espérance ! Aujourd'hui même je le déclare, je te rendrai le double.
9:13	Après que j'aurai tendu Yéhouda comme mon arc, et que j'aurai rempli le carquois avec Éphraïm, et que j'aurai, ô Sion, réveillé tes enfants contre tes enfants, ô Yavan, et que je t'aurai rendue telle l'épée d'un puissant homme :
9:14	alors YHWH au-dessus d'eux apparaîtra, et ses dards partiront comme l'éclair. Et Adonaï YHWH sonnera du shofar, il s'avancera dans le tourbillon du midi.
9:15	YHWH Sabaoth les protégera, ils dévoreront après avoir foulé aux pieds les pierres de fronde. Ils boiront et seront bruyants comme des hommes ivres, ils se rempliront de vin comme un bassin, comme les coins de l'autel.
9:16	YHWH, leur Elohîm, les sauvera en ce jour-là, comme le troupeau de son peuple, car ils sont les pierres d'une couronne qui brilleront dans son pays.
9:17	Car combien est grande sa bonté ! Quelle beauté ! Le froment fera croître les jeunes hommes, et le vin doux rendra ses vierges éloquentes.

## Chapitre 10

### YHWH rassemblera son peuple

10:1	Demandez à YHWH la pluie, au temps de la pluie du printemps<!--La pluie de la première saison correspond aux fortes précipitations qui tombent en Israël pendant quelques jours durant les mois de novembre et décembre. Très attendues par les fermiers, ces pluies rendent le sol, habituellement très dur, labourable et donc propice à être ensemencé. La pluie de l'arrière-saison, semblable à la première, tombe quant à elle juste avant la moisson.--> ! YHWH produira des éclairs et il vous donnera une averse, une pluie, il donnera à chacun de l'herbe dans son champ.
10:2	Car les théraphim ont des paroles vaines, et les devins prophétisent le mensonge, ils profèrent des rêves vains et consolent par la vanité. C'est pourquoi ils sont errants comme des brebis, ils sont malheureux parce qu'il n'y a pas de berger<!--Jé. 23:21-30 ; Ez. 34:2 ; Mt. 9:36.-->.
10:3	Ma colère s'est enflammée contre ces bergers, et je châtierai ces boucs. Car YHWH Sabaoth visite son troupeau, la maison de Yéhouda, et il les a rangés en bataille comme son cheval d'honneur.
10:4	De lui sortira l'Angle<!--L'Angle ou la Pierre angulaire (Yéhoshoua ha Mashiah) : Es. 8:13-17 ; 1 Pi. 2:7.-->, de lui sortira le clou, de lui sortira l'arc de bataille, et de lui sortiront tous les chefs ensemble.
10:5	Ils seront comme des vaillants hommes foulant la boue des rues dans la bataille, et ils combattront, parce que YHWH sera avec eux, et ceux qui sont montés sur des chevaux seront couverts de honte.
10:6	Car je fortifierai la maison de Yéhouda et je sauverai la maison de Yossef. Je les ramènerai et je les ferai habiter en repos, parce que j'aurai compassion d'eux, et ils seront comme si je ne les avais pas rejetés, car je suis YHWH, leur Elohîm, et je les exaucerai.
10:7	Et ceux d'Éphraïm seront comme un homme vaillant, et leur cœur se réjouira comme par le vin. Leurs enfants le verront et se réjouiront, leur cœur se réjouira en YHWH.
10:8	Je sifflerai pour les rassembler, car je les rachète, et ils se multiplieront comme ils se multipliaient.
10:9	Et après que je les aurai dispersés parmi les peuples, ils se souviendront de moi dans les pays éloignés, ils vivront avec leurs enfants et ils reviendront.
10:10	Ainsi je les ramènerai du pays d'Égypte, je les rassemblerai de l'Assyrie, je les ferai venir au pays de Galaad et au Liban, et il n'y aura pas assez d'espace pour eux.
10:11	Il passera la mer de détresse, et il frappera les flots de la mer, et toutes les profondeurs du fleuve seront desséchées. L'orgueil de l'Assyrie sera abattu et le sceptre d'Égypte sera ôté.
10:12	Je les fortifierai en YHWH, et ils marcheront en son Nom, dit YHWH.

## Chapitre 11

### Les houlettes du vrai berger

11:1	Liban, ouvre tes portes, et que le feu dévore tes cèdres !
11:2	Cyprès, gémis, car le cèdre est tombé, parce que les choses magnifiques ont été ravagées ! Chênes de Bashân, gémissez, car la forêt inaccessible est coupée !
11:3	Les bergers poussent des cris de lamentations, parce que leur magnificence est ravagée. On entend le cri de rugissement des lionceaux, parce que l'orgueil du Yarden<!--Jourdain.--> est abattu.
11:4	Ainsi parle YHWH, mon Elohîm : Pais les brebis exposées au carnage !
11:5	Leurs possesseurs les égorgent, sans qu'on les tienne pour coupables, et celui qui les vend dit : Béni soit YHWH, car je m'enrichis ! Et leurs bergers ne les épargnent pas.
11:6	Car je n'ai plus de pitié pour les habitants du pays, dit YHWH ! Mais voici, je livre les humains aux mains les uns des autres et aux mains de leur roi. Ils ravageront le pays et je ne le délivrerai pas de leurs mains.
11:7	Alors je me mis donc à paître les brebis exposées au carnage, qui sont véritablement les plus misérables du troupeau. Puis je pris deux verges. J'appelai l'une Grâce et l'autre Cordon, et je fis paître les brebis.
11:8	Et je supprimai les trois bergers en un mois, car mon âme était impatiente à leur sujet, et leur âme aussi avait pour moi du dégoût.
11:9	Et je dis : Je ne vous ferai plus paître ! Que celle qui va mourir, meure ! Que celle qui va périr, périsse ! Que celles qui restent, se dévorent la chair les unes les autres !
11:10	Puis je pris ma verge appelée Grâce et je la brisai pour rompre mon alliance que j'avais traitée avec tous ces peuples.
11:11	Elle fut rompue en ce jour-là, et les plus malheureuses brebis<!--Les plus malheureuses brebis sont le reste d'Israël.--> qui prirent garde à moi, reconnurent ainsi que c'était la parole de YHWH.
11:12	Je leur dis : Si cela est bon à vos yeux, donnez-moi mon salaire, sinon, ne me le donnez pas. Alors ils pesèrent<!--Mt. 26:15, 27:9-10.--> pour mon salaire 30 pièces d'argent<!--Selon la torah de Moshè (loi de Moïse), pour racheter un mâle de 20 à 60 ans, ayant fait un vœu, il fallait payer 50 sicles d'argent (Lé. 27:3). Pour dédommager un préjudice causé par un bœuf ayant frappé un esclave, on devait donner 30 sicles d'argent au maître de l'esclave et lapider le bœuf (Ex. 21:32). Or le prix du Seigneur a été estimé à 30 sicles d'argent, comme pour les esclaves.-->.
11:13	YHWH me dit : Jette-le au potier, ce prix honorable auquel ils m'ont estimé ! Alors je pris les 30 pièces d'argent et les jetai dans la maison de YHWH pour le potier.
11:14	Puis je brisai ma seconde verge, appelée Cordon, pour rompre la fraternité entre Yéhouda et Israël.

### Les faux bergers (pasteurs)

11:15	YHWH me dit : Prends-toi encore l'équipage d'un berger insensé.
11:16	Car voici, je susciterai dans le pays un berger qui ne prendra pas soin de celles qui sont détruites. Il ne cherchera pas celles qui sont dispersées, il ne guérira pas celles qui sont brisées, il ne soutiendra pas celles qui sont debout, mais il dévorera la chair des plus grasses et il déchirera jusqu'aux cornes de leurs pieds.
11:17	Malheur au berger bon à rien qui abandonne les brebis ! Que l'épée fonde sur son bras et sur son œil droit ! Que son bras se dessèche, qu'il se dessèche et que son œil droit s'éteigne, qu'il s'éteigne !

## Chapitre 12

### Yeroushalaim (Jérusalem), une coupe d'étourdissement pour les nations

12:1	Prophétie, parole de YHWH, sur Israël. Ainsi parle YHWH qui a étendu les cieux et fondé la Terre, et qui a formé l'esprit de l'être humain au-dedans de lui :
12:2	Voici, je ferai de Yeroushalaim une coupe d'étourdissement pour tous les peuples d'alentour. Il en sera même ainsi pour Yéhouda lors du siège de Yeroushalaim<!--Ap. 16:12-16.-->.
12:3	Et il arrivera ce jour-là, que je ferai de Yeroushalaim une pierre pesante pour tous les peuples. Tous ceux qui la soulèveront seront meurtris, meurtris, et toutes les nations de la Terre se rassembleront contre elle.
12:4	En ce temps-là, dit YHWH, je frapperai d'étourdissement tous les chevaux, et de folie ceux qui les monteront. Mais j'aurai les yeux ouverts sur la maison de Yéhouda, et je frapperai d'aveuglement tous les chevaux des peuples.
12:5	Les chefs de Yéhouda diront en leur cœur : Les habitants de Yeroushalaim sont notre force, par YHWH Sabaoth, leur Elohîm.
12:6	En ce jour-là, je ferai des chefs de Yéhouda comme un foyer de feu parmi du bois, et comme une torche enflammée parmi des gerbes. Ils dévoreront à droite et à gauche tous les peuples d'alentour, et Yeroushalaim sera encore habitée à sa place, à Yeroushalaim.
12:7	YHWH sauvera premièrement les tentes de Yéhouda afin que la gloire de la maison de David, la gloire des habitants de Yeroushalaim ne s'élève pas au-dessus de Yéhouda.
12:8	En ce jour-là, YHWH protégera les habitants de Yeroushalaim et le plus faible parmi eux sera, en ce jour-là, comme David. La maison de David sera comme Elohîm, comme l'Ange de YHWH devant leur face.
12:9	Et il arrivera qu'en ce jour-là, je chercherai à détruire toutes les nations qui viendront contre Yeroushalaim.

### Repentance et délivrance d'Israël

12:10	Et je répandrai sur la maison de David et sur les habitants de Yeroushalaim l'Esprit de grâce et de supplications<!--Joë. 2:28-30.-->, et ils regarderont vers Aleph Tav<!--Voir Ap. 1:8, 21:6 et 22:13. Yéhoshoua est l'Aleph et le Tav qu'on a transpercé.-->, lequel ils ont percé<!--Il est question ici du Seigneur Yéhoshoua (Jésus) : le Mashiah (Christ) (Ap. 1:7).-->. Et ils pleureront<!--Au retour du Mashiah, il y aura une repentance et une conversion nationale d'Israël (Ro. 11:26).--> sur lui comme on pleure sur un fils unique, et ils pleureront amèrement sur lui comme on pleure sur un premier-né.
12:11	En ce jour-là, il y aura un grand deuil à Yeroushalaim, comme le deuil d'Hadadrimmon dans la vallée de Meguiddon.
12:12	Le pays sera dans le deuil, chaque famille à part : la famille de la maison de David à part et les femmes de cette maison-là à part, la famille de la maison de Nathan à part et les femmes de cette maison-là à part,
12:13	la famille de la maison de Lévi à part et les femmes de cette maison-là à part, la famille de Shimeï à part et ses femmes à part.
12:14	Toutes les autres familles, chaque famille à part et leurs femmes à part.

## Chapitre 13

### YHWH ôte les faux prophètes

13:1	En ce jour-là, il y aura une source ouverte en faveur de la maison de David et des habitants de Yeroushalaim, pour le péché et pour la souillure.
13:2	Il arrivera aussi en ce jour-là, dit YHWH Sabaoth, que je retrancherai du pays les noms des faux elohîm, et on n'en fera plus mention. J'ôterai aussi du pays les faux prophètes et l'esprit d'impureté.
13:3	Et il arrivera que quand quelqu'un prophétisera dorénavant, son père et sa mère qui l'ont engendré, lui diront : Tu ne vivras plus car tu as prononcé des mensonges au Nom de YHWH ! Son père et sa mère qui l'ont engendré le transperceront quand il prophétisera.
13:4	Et il arrivera en ce jour-là que les prophètes seront honteux chacun de sa vision quand ils prophétiseront, et ils ne seront plus vêtus du manteau de poil pour tromper.
13:5	Chacun d'eux dira : Je ne suis pas prophète, mais je suis un homme qui travaille la terre, car un être humain m'a acquis dès ma jeunesse<!--La Bible version Martin a traduit par : « On m'a appris à gouverner du bétail ».-->.
13:6	Et si on lui demande : Que veulent donc dire ces blessures que tu as aux mains ? Et il répondra : C'est dans la maison de mes amis qu'on me les a faites.

### Le Mashiah, le Bon Berger

13:7	Épée, réveille-toi contre mon Berger<!--Mon Berger : il est question de Yéhoshoua ha Mashiah (Jésus-Christ), le Bon Berger (Ps. 23 ; Jn. 10:1-17).--> et sur l'homme qui est mon compagnon ! dit YHWH Sabaoth. Frappe le Berger<!--Frappe le Berger : cette prophétie fait référence à la crucifixion du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) (Ge. 3:15 ; Mt. 26:31 ; Mc. 14:27, 14:50, 15:19).--> et les brebis seront dispersées, et je tournerai ma main vers les faibles.

### Le reste du peuple éprouvé et purifié

13:8	Et il arrivera dans tout le pays, dit YHWH, que les deux tiers seront retranchés et périront, mais l'autre tiers y restera.
13:9	Je mettrai ce tiers dans le feu et je le purifierai comme on purifie l'argent, je l'éprouverai comme on éprouve l'or. Il invoquera mon Nom et je l'exaucerai ; je dirai : C'est ici mon peuple ! Et il dira : YHWH est mon Elohîm<!--Ps. 50:15, 91:15, 144:15 ; 1 Pi. 1:6-7.--> !

## Chapitre 14

### Le jour de YHWH

14:1	Voici, le jour de YHWH<!--Les expressions « jour de YHWH » ou « jour du Seigneur » sont récurrentes dans la Bible (19 fois dans le Tanakh et 42 fois dans les Écrits de la Nouvelle Alliance). Elles désignent un temps arrêté par le Seigneur durant lequel il interviendra de manière directe et personnelle dans les affaires humaines. En ce jour là, Elohîm exprimera sa colère et exercera sa justice sur les impies d'Israël (Es. 22 ; Jé. 30:1-17 ; Joë. 1-2 ; Am. 5 ; So. 1) et ceux du monde entier (Ez. 38-39 ; Za. 14).--> arrive, et tes dépouilles seront partagées au milieu de toi, Yeroushalaim.
14:2	Je rassemblerai toutes les nations à Yeroushalaim pour qu'elles lui fassent la guerre<!--Joë. 3 ; Ap. 16:12-16.--> : la ville sera prise, les maisons pillées, et les femmes violées ; la moitié de la ville ira en captivité, mais le reste du peuple ne sera pas retranché de la ville.
14:3	Alors YHWH sortira et il combattra contre ces nations, comme il a combattu au jour de la bataille.

### Retour visible et glorieux du Seigneur

14:4	Ses pieds se poseront en ce jour sur la Montagne des Oliviers<!--Ce sont les pieds de Yéhoshoua ha Mashiah (Jésus-Christ) (Ac. 1:10-11).-->, qui est vis-à-vis de Yeroushalaim, du côté de l'orient. Et la Montagne des Oliviers se fendra par le milieu, à l'orient et à l'occident, de sorte qu'il y aura une très grande vallée. Une moitié de la montagne reculera vers le nord, et l'autre moitié vers le midi.
14:5	Vous fuirez alors dans la vallée de mes montagnes, car la vallée des montagnes s'étendra jusqu'à Atzel. Vous fuirez comme vous avez fui devant le tremblement de terre, au temps d'Ouzyah, roi de Yéhouda. Alors YHWH, mon Elohîm, viendra, et tous les saints seront avec lui<!--Ce passage confirme clairement que Yéhoshoua ha Mashiah (Jésus-Christ) est YHWH (Es. 34:5, 40:10-11, 62:11-12 ; 1 Th. 3:13 ; Jud. 14-15.).-->.
14:6	Et il arrivera, en ce jour-là, qu'il n'y aura pas de lumière précieuse, mais du froid et de la glace.
14:7	Ce sera un jour unique, connu de YHWH, et qui ne sera ni jour ni nuit, mais au temps du soir il y aura de la lumière.
14:8	Et il arrivera qu'en ce jour-là, des eaux vives<!--Ez. 47:1-12 ; Ap. 22:1-2.--> sortiront de Yeroushalaim, la moitié d'elles coulera vers la mer orientale, et l'autre moitié vers la mer occidentale. Il en sera ainsi été et hiver.

### Le Royaume du Mashiah

14:9	YHWH sera Roi sur toute la Terre. En ce jour-là, YHWH sera un, et son Nom sera un<!--Littéralement « e'had ». Voir commentaire en Ge. 1:5. Voir également De. 6:4 ; Ja. 2:19 ; Ga. 3:20 ; 1 Ti. 2:5.-->.
14:10	Toute la terre deviendra comme la région aride, depuis Guéba jusqu'à Rimmon, au midi de Yeroushalaim. Et Yeroushalaim sera exaltée et restera à sa place, depuis la porte de Benyamin jusqu'à l'endroit de la première porte, jusqu'à la porte des angles, et depuis la tour de Hananeel jusqu'aux pressoirs du roi.
14:11	On y habitera, et elle ne sera plus vouée à une entière destruction. Yeroushalaim sera habitée en sécurité.
14:12	Voici la plaie dont YHWH frappera tous les peuples qui auront fait la guerre contre Yeroushalaim : il fera pourrir leur chair tandis qu'ils seront sur leurs pieds, leurs yeux pourriront dans leurs orbites et leur langue pourrira dans leur bouche.
14:13	Et il arrivera en ce jour-là, que YHWH produira un grand trouble parmi eux. Car chacun saisira la main de son prochain, et la main de l'un s'élèvera contre la main de l'autre.
14:14	Yéhouda combattra aussi dans Yeroushalaim, et les richesses de toutes les nations d'alentour y seront amassées : l'or, l'argent et des vêtements en très grand nombre.
14:15	Et la même plaie sera sur les chevaux, les mulets, les chameaux, les ânes et sur toutes les bêtes qui seront dans ces camps : cette plaie sera semblable à l'autre.

### Adoration de YHWH Sabaoth dans le Royaume

14:16	Et il arrivera que tous ceux qui resteront de toutes les nations venues contre Yeroushalaim, monteront en foule d'année en année pour adorer le Roi, YHWH Sabaoth, et pour célébrer la fête des tabernacles.
14:17	S'il y a des familles de la Terre qui ne montent pas à Yeroushalaim pour adorer le Roi, YHWH Sabaoth, la pluie ne tombera pas sur elles.
14:18	Si la famille d'Égypte ne monte pas, si elle ne vient pas, la pluie ne tombera pas sur elle, elle sera frappée de la plaie dont YHWH frappera les nations qui ne monteront pas pour célébrer la fête des tabernacles.
14:19	Ce sera la peine du péché de l'Égypte, et du péché de toutes les nations qui ne monteront pas pour célébrer la fête des tabernacles.
14:20	En ce jour-là, il sera écrit sur les clochettes des chevaux : Sainteté à YHWH ! Et les chaudières dans la maison de YHWH seront comme les coupes devant l'autel.
14:21	Toute chaudière qui sera à Yeroushalaim et dans Yéhouda, sera consacrée à YHWH Sabaoth. Et tous ceux qui offriront des sacrifices viendront et s'en serviront pour cuire les viandes, et il n'y aura plus de marchands dans la maison de YHWH Sabaoth, en ce jour-là.
