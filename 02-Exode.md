# Shemot (Exode) (Ex.)

Signification : Noms

Auteur : Probablement Moshè (Moïse)

Thème : La délivrance

Date de rédaction : Env. 1450 – 1410 av. J.-C.

Les fils de Yaacov (Jacob) s'étaient retrouvés en Égypte pour survivre à une famine qui avait frappé la terre entière pendant plusieurs années. Grâce à leur frère Yossef, alors gouverneur d'Égypte, ils bénéficièrent d'un bon traitement. Mais la mort de ce dernier et la montée au pouvoir d'un nouveau pharaon (probablement Ramsès II) inaugurèrent une période de quatre siècles de souffrances pour le peuple élu.

En effet, les Hébreux avaient été réduits en esclavage. En réponse aux cris de douleur de son peuple, Elohîm suscita Moshè (Moïse), dont le nom signifie « tiré de ». Ce descendant de Lévi fut élevé dans le palais de pharaon, mais dut s'enfuir parce qu'il avait tué un Égyptien. Après 40 ans passés dans le pays de Madian, l'Elohîm qui s'appelle « JE SUIS » se révéla à Moshè sur la montagne d'Horeb et lui confia la mission d'aller délivrer son peuple du joug égyptien.

Ce livre retrace la sortie d'Égypte, et le début de la traversée du désert, jalonnée de prodiges exceptionnels.

## Chapitre 1

### Après la mort de Yossef

1:1	Voici les noms des fils d'Israël qui entrèrent en Égypte avec Yaacov. Ils y entrèrent chacun avec sa famille :
1:2	Reouben, Shim’ôn, Lévi et Yéhouda,
1:3	Yissakar, Zebouloun et Benyamin,
1:4	Dan et Nephthali, Gad et Asher.
1:5	Toutes les personnes issues des reins de Yaacov étaient 70 âmes. Yossef était alors en Égypte.
1:6	Yossef mourut ainsi que tous ses frères et toute cette génération-là.
1:7	Les enfants d'Israël portèrent du fruit et grouillèrent ; ils se multiplièrent et devinrent extrêmement, extrêmement puissants, au point de remplir<!--De. 26:5 ; Ac. 7:17.--> le pays.

### Les enfants d'Israël esclaves en Égypte

1:8	Depuis, il s'éleva un nouveau roi sur l'Égypte, qui n'avait pas connu Yossef.
1:9	Et il dit à son peuple : Voici, le peuple des enfants d'Israël est plus grand et plus puissant que nous.
1:10	Agissons donc sagement avec lui, de peur qu'il ne se multiplie, et que, s'il survenait une guerre, il ne se joigne à nos ennemis, ne fasse la guerre contre nous, et qu'il ne s'en aille du pays.
1:11	Ils établirent donc sur le peuple des chefs de travaux forcés afin de l'affliger par des travaux forcés. C'est ainsi qu'il bâtit les villes de Pithom et de Ramsès, pour servir de magasins à pharaon.
1:12	Mais plus ils l'affligeaient, plus il se multipliait et croissait en toute abondance, c'est pourquoi ils haïssaient les enfants d'Israël<!--Ps. 105:24.-->.
1:13	Et les Égyptiens firent travailler les enfants d'Israël par force<!--Ge. 15:13.-->.
1:14	Ils leur rendirent la vie amère par des travaux difficiles en argile et en briques, et par toutes sortes de travaux des champs, ainsi que tous les travaux qu'ils leur imposaient avec cruauté.
1:15	Le roi d'Égypte parla aussi aux sages-femmes des Hébreux. Le nom de l'une était Shiphra et le nom de la seconde Poua.
1:16	Il leur dit : Quand vous accoucherez les femmes des Hébreux, et que vous les verrez sur les sièges, si c'est un fils, mettez-le à mort ; mais si c'est une fille, qu'elle vive.
1:17	Mais les sages-femmes craignirent Elohîm et ne firent pas ce que le roi d'Égypte leur avait dit, car elles laissèrent vivre les fils.
1:18	Alors le roi d'Égypte appela les sages-femmes et leur dit : Pourquoi avez-vous fait cela et avez-vous laissé vivre les fils ?
1:19	Les sages-femmes répondirent à pharaon : Parce que les femmes des Hébreux ne sont pas comme les Égyptiennes, car elles sont vigoureuses. Elles accouchent avant que la sage-femme n'arrive auprès d'elles.
1:20	Elohîm fit du bien aux sages-femmes et le peuple multiplia et devint très puissant.
1:21	Et il arriva, parce que les sages-femmes craignirent Elohîm, qu'il leur fit des maisons.
1:22	Alors pharaon donna cet ordre à tout son peuple, disant : Jetez dans le fleuve tous les fils qui naîtront, mais laissez vivre toutes les filles.

## Chapitre 2

### Naissance de Moshè (Moïse)<!--Hé. 11:23-27.-->

2:1	Un homme de la maison de Lévi s'en alla et prit une fille de Lévi<!--No. 26:59.-->.
2:2	Cette femme conçut et enfanta un fils. Voyant qu'il était beau, elle le cacha pendant trois mois<!--Hé. 11:23.-->.
2:3	Mais ne pouvant le tenir caché plus longtemps, elle prit une arche en papyrus, qu'elle enduisit de bitume et de poix ; puis elle y mit l'enfant et le déposa parmi les roseaux sur le bord du fleuve.
2:4	Et la sœur de cet enfant se tenait loin pour savoir ce qui lui arriverait.
2:5	La fille de pharaon descendit à la rivière pour se baigner, et ses servantes se promenaient sur le bord de la rivière, et ayant vu l'arche au milieu des roseaux, elle envoya une de ses servantes pour la prendre.
2:6	Et elle l'ouvrit et vit l'enfant, et voici, c'était un petit garçon qui pleurait. Elle en fut touchée de compassion et dit : C'est un enfant des Hébreux !
2:7	Alors sa sœur dit à la fille de pharaon : Veux-tu que j'aille te chercher une nourrice parmi les femmes des Hébreux afin qu'elle allaite cet enfant pour toi ?
2:8	La fille de pharaon lui répondit : Va ! Et la jeune fille s'en alla et appela la mère de l'enfant.
2:9	Et la fille de pharaon lui dit : Emmène cet enfant et allaite-le pour moi, je te donnerai ton salaire. Et la femme prit l'enfant et l'allaita.
2:10	Quand l'enfant fut devenu grand, elle l'amena à la fille de pharaon et il fut pour elle comme un fils. Elle l'appela du nom de Moshè parce que, dit-elle : Je l'ai tiré des eaux.

### Moshè prend à cœur le sort d'Israël ; fuite à Madian

2:11	Or il arriva, en ce temps-là, que Moshè, étant devenu grand, sortit vers ses frères et vit leurs travaux forcés. Il vit un homme égyptien qui frappait un homme hébreu, un de ses frères<!--Hé. 11:24-25.-->.
2:12	Et ayant regardé çà et là, et voyant qu'il n'y avait personne, il tua l'Égyptien et le cacha dans le sable.
2:13	Il sortit le second jour et vit deux hommes hébreux qui se querellaient. Il dit au coupable : Pourquoi frappes-tu ton prochain ?
2:14	Lequel répondit : Qui t'a établi prince et juge sur nous ? Veux-tu me tuer comme tu as tué l'Égyptien ? Et Moshè eut peur et se dit : Sûrement l'affaire est connue.
2:15	Or pharaon entendit parler de cette affaire et chercha à tuer Moshè. Mais Moshè s'enfuit de la face de pharaon, s'arrêta dans le pays de Madian et s'assit près d'un puits.
2:16	Or le prêtre de Madian avait sept filles qui vinrent puiser de l'eau, et elles remplirent les auges pour abreuver le troupeau de leur père.
2:17	Mais des bergers survinrent et les chassèrent. Alors Moshè se leva, les sauva et fit boire leur troupeau.
2:18	Et quand elles furent revenues chez Reouel, leur père, il leur dit : Comment se fait-il que vous soyez revenues si tôt aujourd'hui ?
2:19	Elles répondirent : Un homme égyptien nous a délivrées de la main des bergers. Il a même puisé, puisé de l'eau pour nous et a fait boire le troupeau.
2:20	Il dit à ses filles : Où est-il ? Pourquoi avez-vous laissé là cet homme ? Appelez-le et qu'il mange du pain.
2:21	Et Moshè consentit à demeurer auprès de cet homme, qui donna sa fille Tsipporah<!--Séphora.--> à Moshè.
2:22	Et elle enfanta un fils, et il l'appela du nom de Guershom car, dit-il : Je séjourne dans un pays étranger.

### YHWH entend les cris de son peuple

2:23	Or il arriva, durant ces nombreux jours, que le roi d'Égypte mourut. Les enfants d'Israël soupirèrent à cause de la servitude et ils crièrent. Leur cri monta jusqu'à Elohîm, à cause de la servitude<!--No. 20:15-16.-->.
2:24	Elohîm entendit leurs gémissements, et Elohîm se souvint de l'alliance qu'il avait traitée avec Abraham, Yitzhak et Yaacov.
2:25	Elohîm regarda les enfants d'Israël et Elohîm sut...

## Chapitre 3

### YHWH se révèle à Moshè dans le buisson ardent

3:1	Or Moshè fut berger du troupeau de Yithro<!--Jéthro.-->, son beau-père, prêtre de Madian. Il conduisit le troupeau derrière le désert et vint à la montagne d'Elohîm à Horeb.
3:2	Et l'Ange de YHWH lui apparut dans une flamme de feu, du milieu d'un buisson. Il regarda, et voici, le buisson était tout en feu, et le buisson ne se consumait pas.
3:3	Alors Moshè dit : Je me détournerai maintenant et je regarderai cette grande vision, pourquoi le buisson ne se consume pas.
3:4	Et YHWH vit que Moshè s'était détourné pour regarder. Elohîm l'appela du milieu du buisson, en disant : Moshè ! Moshè ! Et il répondit : Me voici !
3:5	Et Elohîm dit : N'approche pas d'ici, ôte tes sandales de tes pieds, car le lieu où tu te tiens est une terre sainte.
3:6	Il dit aussi : Je suis l'Elohîm de ton père, l'Elohîm d'Abraham, l'Elohîm de Yitzhak et l'Elohîm de Yaacov<!--Mt. 22:32 ; Mc. 12:26 ; Lu. 20:37 ; Ac. 7:32.-->. Moshè cacha son visage, parce qu'il craignait de regarder vers Elohîm.
3:7	Et YHWH dit : J'ai vu, j'ai vu l'affliction de mon peuple qui est en Égypte et j'ai entendu le cri qu'ils ont poussé à cause de leurs oppresseurs, car je connais leurs douleurs.
3:8	C'est pourquoi je suis descendu pour le délivrer de la main des Égyptiens et pour le faire monter de ce pays jusque dans un pays bon et vaste, un pays où coulent le lait et le miel ; dans le lieu où sont les Cananéens, les Héthiens, les Amoréens, les Phéréziens, les Héviens et les Yebousiens.
3:9	Maintenant, voici le cri des enfants d'Israël est venu jusqu'à moi, et j'ai vu aussi l'oppression dont les Égyptiens les oppriment.
3:10	Maintenant donc viens, et je t'enverrai vers pharaon et tu feras sortir mon peuple, les enfants d'Israël, hors d'Égypte<!--Os. 12:14 ; Mi. 6:4.-->.
3:11	Et Moshè répondit à Elohîm : Qui suis-je, moi, pour aller vers pharaon, et pour faire sortir de l'Égypte les enfants d'Israël ?
3:12	Et Elohîm lui dit : Va, car je suis avec toi. Et voici quel sera pour toi le signe que c'est moi qui t'envoie : quand tu auras fait sortir mon peuple d'Égypte, vous servirez Elohîm sur cette montagne.

### YHWH révèle son Nom à Moshè

3:13	Et Moshè dit à Elohîm : Voici, quand j'irai vers les enfants d'Israël et que je leur aurai dit : L'Elohîm de vos pères m'a envoyé vers vous, s'ils me disent alors : Quel est son Nom ? Que leur dirai-je ?
3:14	Et Elohîm dit à Moshè : JE SUIS QUI JE SUIS<!--Ehyeh asher ehyeh ou Hayah asher hayah. Je serai qui je serai.-->. Il dit aussi : Tu parleras ainsi aux enfants d'Israël : JE SUIS<!--JE SUIS (« ehyeh ou hayah » en hébreu), c'est de là que vient le Nom de YHWH. Or le Nom de Yéhoshoua signifie « YHWH est salut ». Elohîm révèle son Nom à Moshè : « JE SUIS QUI JE SUIS ». Or Yéhoshoua ha Mashiah s'est ouvertement attribué ce Nom en Jn. 8:58. N'ayant compris ni le plan d'Elohîm ni qui était celui qui les visitait, les religieux juifs ont voulu le lapider, car ils estimaient qu'il blasphémait. Car en déclarant être « JE SUIS », Yéhoshoua ha Mashiah proclamait ouvertement sa divinité (Ro. 9:5), chose que les Juifs ne pouvaient concevoir. Dans l'évangile de Jean (Yohanan), Yéhoshoua déclare clairement qu'il est le « JE SUIS » en Ex. 3:14. « JE SUIS le Pain de vie » (Jn. 6:35), « JE SUIS la Lumière du monde » (Jn. 8:12), « JE SUIS le Bon Berger » (Jn. 10:11), « JE SUIS la Porte » (Jn. 10:7), « JE SUIS la Résurrection » (Jn. 11:25), « JE SUIS le Chemin, la Vérité et la Vie » (Jn. 14:6), « JE SUIS le Véritable cep » (Jn. 15:1).--> m'a envoyé vers vous.
3:15	Elohîm dit encore à Moshè : Tu parleras ainsi aux enfants d'Israël : YHWH, l'Elohîm de vos pères, l'Elohîm d'Abraham, l'Elohîm de Yitzhak et l'Elohîm de Yaacov m'a envoyé vers vous. C'est là mon Nom pour l'éternité, c'est là mon souvenir d'âge en âge<!--Yéhoshoua est le Roi des âges 1 Ti. 1:17.-->.
3:16	Va et rassemble les anciens d'Israël, et dis-leur : YHWH, l'Elohîm de vos pères, l'Elohîm d'Abraham, de Yitzhak et de Yaacov, m'est apparu en disant : Je vous ai visités, je vous ai visités, à cause de ce qu'on vous fait en Égypte.
3:17	Et j'ai dit : Je vous ferai monter hors de l'affliction de l'Égypte vers le pays des Cananéens, des Héthiens, des Amoréens, des Phéréziens, des Héviens et des Yebousiens, qui est un pays où coulent le lait et le miel.
3:18	Ils écouteront ta voix. Tu iras toi et les anciens d'Israël vers le roi d'Égypte et vous lui direz : YHWH, l'Elohîm des Hébreux, est venu nous rencontrer. Maintenant, je te prie, laisse-nous aller à trois journées de marche dans le désert pour sacrifier à YHWH, notre Elohîm.
3:19	Or je sais que le roi d'Égypte ne vous laissera pas partir, à moins d'y être contraint par une main forte.
3:20	Mais j'étendrai ma main et je frapperai l'Égypte par toutes les merveilles que je ferai au milieu d'elle. Après cela, il vous laissera aller.
3:21	Et je ferai trouver grâce à ce peuple aux yeux des Égyptiens, et il arrivera que, quand vous partirez, vous ne vous en irez pas à vide.
3:22	Mais chacune demandera à sa voisine et à l'hôtesse de sa maison, des vases d'argent, des vases d'or, et des vêtements, que vous mettrez sur vos fils et sur vos filles, ainsi vous dépouillerez les Égyptiens.

## Chapitre 4

### Moshè résiste en évoquant l'incrédulité du peuple

4:1	Et Moshè répondit et dit : Mais voici, ils ne me croiront pas et n'écouteront pas ma voix. Car ils diront : YHWH ne t'est pas apparu.
4:2	Et YHWH lui dit : Qu'est-ce que tu as dans ta main ? Il répondit : Une verge.
4:3	Et Elohîm lui dit : Jette-la par terre. Il la jeta par terre et elle devint un serpent. Et Moshè fuyait devant lui.
4:4	Et YHWH dit à Moshè : Étends ta main et saisis sa queue. Il étendit sa main et le pressa, et il devint une verge dans sa main.
4:5	C'est là ce que tu feras, afin qu'ils croient que YHWH, l'Elohîm de leurs pères, l'Elohîm d'Abraham, l'Elohîm de Yitzhak et l'Elohîm de Yaacov, t'est apparu.
4:6	YHWH lui dit encore : Mets maintenant ta main dans ton sein. Et il mit sa main dans son sein, puis il la retira, et voici, sa main était lépreuse, comme de la neige.
4:7	Et Elohîm lui dit : Remets ta main dans ton sein. Il remit sa main dans son sein et la retira de son sein, et voici, elle était redevenue comme le reste de sa chair.
4:8	Mais s'il arrive qu'ils ne te croient pas et qu'ils n'écoutent pas la voix du premier signe, ils croiront à la voix du second signe.
4:9	S'il arrive qu'ils ne croient pas à ces deux signes et qu'ils n'écoutent pas ta voix, tu prendras de l'eau du fleuve et tu la répandras sur la terre, et les eaux que tu auras prises du fleuve, deviendront du sang sur la terre.

### Moshè résiste en évoquant son incapacité à parler

4:10	Et Moshè répondit à YHWH : Excuse-moi, Adonaï ! Je ne suis pas un homme qui ait, ni d'hier ni d'avant-hier, la parole aisée, ni même depuis que tu parles à ton serviteur, car j'ai la bouche et la langue empêchées.
4:11	Et YHWH lui dit : Qui a fait la bouche de l'être humain ? Et qui rend muet ou sourd, voyant ou aveugle ? N'est-ce pas moi YHWH<!--Ps. 94:9.--> ?
4:12	Va donc maintenant, je serai avec ta bouche et je t'enseignerai ce que tu auras à dire<!--Lu. 12:12 ; Mt. 10:19 ; Mc. 13:11.-->.
4:13	Et Moshè répondit : Excuse-moi, Adonaï ! Envoie, s'il te plaît, par la main de qui tu enverras.
4:14	Et la colère de YHWH s'enflamma contre Moshè et il lui dit : Aaron le Lévite n'est-il pas ton frère ? Je sais qu'il parlera très bien, et même le voilà qui vient à ta rencontre, et quand il te verra, il se réjouira dans son cœur.
4:15	Tu lui parleras et tu mettras ces paroles dans sa bouche. Et moi, je suis avec ta bouche et avec sa bouche et je vous enseignerai ce que vous aurez à faire.
4:16	C'est lui qui parlera pour toi au peuple et il arrivera qu'il sera ta bouche, et toi, tu seras Elohîm pour lui.
4:17	Tu prendras aussi dans ta main cette verge, avec laquelle tu feras ces signes-là.

### Moshè accepte sa mission et part en Égypte

4:18	Ainsi Moshè s'en alla et retourna vers Yether<!--Jéthro.-->, son beau-père, et lui dit : Je te prie, que je m'en aille et que je retourne vers mes frères qui sont en Égypte, pour voir s'ils vivent encore. Et Yithro lui dit : Va en paix !
4:19	Or YHWH dit à Moshè au pays de Madian : Va et retourne en Égypte, car tous ceux qui en voulaient à ta vie sont morts.
4:20	Moshè prit sa femme et ses fils, les mit sur un âne, et retourna au pays d'Égypte. Moshè prit aussi la verge d'Elohîm dans sa main.
4:21	Et YHWH avait dit à Moshè : Quand tu t'en iras pour retourner en Égypte, tu prendras garde à tous les miracles que j'ai mis dans ta main, et tu les feras devant pharaon. Mais moi, j'endurcirai son cœur et il ne laissera pas aller le peuple.
4:22	Tu diras donc à pharaon : Ainsi parle YHWH : Israël est mon fils, mon premier-né<!--Os. 11:1.-->.
4:23	Et je te dis : Laisse aller mon fils pour qu'il me serve. Si tu refuses de le laisser aller, voici, je tuerai ton fils, ton premier-né.
4:24	Or il arriva que, comme Moshè était en chemin dans l'hôtellerie, YHWH le rencontra et chercha à le faire mourir.
4:25	Et Tsipporah prit un caillou dur, coupa le prépuce de son fils et le jeta à ses pieds, et dit : En effet, tu es pour moi un époux de sang !
4:26	Alors YHWH se retira de lui. Et Tsipporah dit : Époux de sang ! à cause de la circoncision.

### YHWH envoie Aaron vers Moshè

4:27	Et YHWH dit à Aaron : Va dans le désert, au-devant de Moshè. Il y alla donc et le rencontra sur la montagne d'Elohîm, et l'embrassa.
4:28	Et Moshè raconta à Aaron toutes les paroles de YHWH qui l'avait envoyé, et tous les signes qu'il lui avait ordonné de faire.
4:29	Moshè donc poursuivit son chemin avec Aaron et rassemblèrent tous les anciens des enfants d'Israël.
4:30	Et Aaron rapporta toutes les paroles que YHWH avait dites à Moshè, et il exécuta les signes aux yeux du peuple.
4:31	Et le peuple crut. Ils apprirent que YHWH avait visité les enfants d'Israël, qu'il avait vu leur affliction, et ils s'inclinèrent et se prosternèrent.

## Chapitre 5

### Pharaon s'oppose à Moshè<!--Ex. 5-14.-->

5:1	Après cela, Moshè et Aaron se rendirent auprès de pharaon et lui dirent : Ainsi parle YHWH, l'Elohîm d'Israël : Laisse aller mon peuple, afin qu'il me célèbre une fête dans le désert.
5:2	Mais pharaon dit : Qui est YHWH pour que j'obéisse à sa voix et que je laisse aller Israël ? Je ne connais pas YHWH et je ne laisserai pas aller Israël.
5:3	Et ils dirent : L'Elohîm des Hébreux est venu au-devant de nous. Permets-nous de faire trois journées de marche dans le désert, et que nous sacrifions à YHWH, notre Elohîm, de peur qu'il ne nous frappe par la peste ou par l'épée.
5:4	Et le roi d'Égypte leur dit : Moshè et Aaron, pourquoi détournez-vous le peuple de son ouvrage ? Allez maintenant à vos travaux forcés.
5:5	Pharaon dit aussi : Voici, le peuple de ce pays est maintenant en grand nombre, et vous lui feriez cesser leurs travaux forcés !
5:6	Et ce jour-là, pharaon donna cet ordre aux oppresseurs établis sur le peuple et à ses commissaires, en disant :
5:7	Vous ne donnerez plus de paille à ce peuple pour faire des briques comme hier et avant-hier, mais qu'ils aillent eux-mêmes se ramasser de la paille.
5:8	Néanmoins, vous leur imposerez la quantité de briques qu'ils faisaient hier et avant-hier, sans en rien diminuer. Car ce sont des paresseux. C'est pour cela qu'ils crient, en disant : Allons et sacrifions à notre Elohîm !
5:9	Que l'on impose encore plus de travail à ces gens, qu'ils s'en occupent et qu'ils ne prêtent plus attention aux paroles de mensonge.
5:10	Alors les oppresseurs du peuple et ses commissaires sortirent et parlèrent au peuple, disant : Ainsi parle pharaon : Je ne vous donnerai plus de paille.
5:11	Allez vous-mêmes et prenez de la paille où vous en trouverez, mais votre travail ne sera en rien diminué.
5:12	Alors le peuple se répandit dans tout le pays d'Égypte, pour ramasser du chaume au lieu de paille.
5:13	Et les oppresseurs les pressaient, en disant : Achevez vos ouvrages, chaque jour sa tâche, comme lorsque la paille vous était fournie.
5:14	On frappait même les commissaires des enfants d'Israël, établis sur eux par les oppresseurs de pharaon, et on leur disait : Pourquoi n'avez-vous pas achevé votre tâche en faisant des briques hier et aujourd'hui, comme hier et avant-hier<!--« Hier et avant-hier », c'est-à-dire « auparavant ».--> ?
5:15	Alors les commissaires des enfants d'Israël vinrent crier à pharaon, en disant : Pourquoi agis-tu ainsi envers tes serviteurs ?
5:16	On ne donne pas de paille à tes serviteurs, et toutefois on nous dit : Faites des briques ! Voici qu'on frappe tes serviteurs, et c'est ton peuple qui pèche !
5:17	Et il répondit : Vous êtes des paresseux, des paresseux ! C'est pourquoi vous dites : Allons, sacrifions à YHWH !
5:18	Maintenant : Allez travailler ! On ne vous donnera pas de paille, mais vous fournirez la même quantité de briques.
5:19	Les commissaires des enfants d'Israël virent qu'ils souffraient, puisqu'on leur disait : Vous ne diminuerez rien de vos briques sur la tâche de chaque jour.
5:20	Et en sortant de chez pharaon, ils rencontrèrent Moshè et Aaron qui se tenaient là pour les rencontrer.
5:21	Ils leur dirent : Que YHWH vous regarde et qu'il juge ! Car vous nous avez mis en mauvaise odeur devant pharaon et devant ses serviteurs, leur mettant l'épée à la main pour nous tuer.
5:22	Alors Moshè retourna vers YHWH et dit : Adonaï, pourquoi as-tu fait du mal à ce peuple ? Pourquoi m'as-tu envoyé ?
5:23	Car depuis que je suis allé vers pharaon pour parler en ton Nom, il a fait du mal à ce peuple et tu n'as pas délivré ton peuple.

## Chapitre 6

### YHWH fortifie Moshè et rappelle son alliance avec Israël

6:1	YHWH dit à Moshè : Tu verras maintenant ce que je ferai à pharaon. Par main forte, il les laissera partir, par main forte, il les chassera de son pays !
6:2	Elohîm parla encore à Moshè et lui dit : Je suis YHWH.
6:3	Je suis apparu à Abraham, à Yitzhak et à Yaacov, comme El Shaddaï<!--Voir Ge. 17:1, 28:3, 35:11, 43:14, 48:3, 49:25.-->, mais je ne me suis pas fait connaître d'eux sous mon Nom de YHWH.
6:4	J'ai aussi établi mon alliance avec eux, pour leur donner le pays de Canaan, le pays de leurs pèlerinages, dans lequel ils ont demeuré comme étrangers.
6:5	Et j'ai entendu les gémissements des enfants d'Israël, que les Égyptiens tiennent en esclavage, et je me suis souvenu de mon alliance.
6:6	C'est pourquoi dis aux enfants d'Israël : Je suis YHWH, et je vous ferai sortir de dessous les travaux forcés des Égyptiens, et je vous délivrerai de leur servitude, je vous rachèterai à bras étendu, et par de grands jugements.
6:7	Et je vous prendrai pour être mon peuple, je suis votre Elohîm, et vous saurez que je suis YHWH votre Elohîm, qui vous fais sortir de dessous les travaux forcés de l'Égypte.
6:8	Et je vous ferai entrer dans le pays au sujet duquel j'ai levé ma main pour le donner à Abraham, à Yitzhak et à Yaacov, et je vous le donnerai en héritage. Je suis YHWH.
6:9	Moshè donc parla de cette manière aux enfants d'Israël. Mais ils n'écoutèrent pas Moshè, à cause de l'angoisse de leur esprit et à cause de leur dure servitude.
6:10	Et YHWH parla à Moshè, en disant :
6:11	Va et dis à pharaon, roi d'Égypte, qu'il laisse sortir les enfants d'Israël de son pays.
6:12	Alors Moshè parla devant YHWH, en disant : Voici, les enfants d'Israël ne m'ont pas écouté. Comment pharaon m'écoutera-t-il, moi, qui suis incirconcis des lèvres ?
6:13	Mais YHWH parla à Moshè et à Aaron, et leur ordonna d'aller trouver les enfants d'Israël, et pharaon, roi d'Égypte, pour faire sortir les enfants d'Israël du pays d'Égypte.

### Les chefs d'Israël

6:14	Voici les chefs des pères : les fils de Reouben, premier-né d'Israël : Hénoc et Pallou, Hetsron et Carmi. Ce sont là les familles de Reouben<!--Ge. 46:9 ; No. 26:5 ; 1 Ch. 5:3.-->.
6:15	Les fils de Shim’ôn : Yemouel, Yamin, Ohad, Yakîn et Tsochar, et Shaoul, fils d'une Cananéenne ; ce sont là les familles de Shim’ôn.
6:16	Voici les noms des fils de Lévi selon leur naissance : Guershon, Kehath et Merari. Les années de la vie de Lévi furent de 137 ans.
6:17	Les fils de Guershon : Libni et Shimeï, selon leurs familles.
6:18	Les fils de Kehath : Amram, Yitshar, Hébron et Ouzziel. Et les années de la vie de Kehath furent de 133 ans.
6:19	Les fils de Merari : Machli et Moushi. Ce sont là les familles de Lévi selon leurs générations.
6:20	Or Amram prit Yokebed, sa tante, pour femme, qui lui enfanta Aaron et Moshè. Les années de la vie d'Amram furent de 137 ans.
6:21	Et les fils de Yitshar : Koré, Népheg et Zicri.
6:22	Et les fils d'Ouzziel : Mishaël, Eltsaphan et Sithri.
6:23	Aaron prit pour femme Élishéba, fille d'Amminadab, sœur de Nahshôn, qui lui enfanta Nadab, Abihou, Èl’azar et Ithamar.
6:24	Et les fils de Koré : Assir, Elkana et Abiasaph. Ce sont là les familles des Korites.
6:25	Èl’azar, fils d'Aaron, prit pour femme une des filles de Poutiel, qui lui enfanta Phinées. Ce sont là les chefs des pères des Lévites selon leurs familles.
6:26	Or c'est à cet Aaron et ce Moshè que YHWH dit : Faites sortir les enfants d'Israël du pays d'Égypte selon leurs armées.
6:27	Ce sont eux qui parlèrent à pharaon, roi d'Égypte, pour faire sortir d'Égypte les enfants d'Israël. C'est ce Moshè et c'est cet Aaron.
6:28	Le jour où YHWH parla à Moshè dans le pays d'Égypte,
6:29	YHWH parla à Moshè et dit : Je suis YHWH. Dis à pharaon, roi d'Égypte, toutes les paroles que je t'ai dites.
6:30	Et Moshè dit en présence de YHWH : Voici, je suis incirconcis des lèvres, comment pharaon m'écoutera-t-il ?

## Chapitre 7

### L'appel de Moshè confirmé

7:1	Et YHWH dit à Moshè : Voici, je t'ai fait Elohîm pour pharaon, et Aaron ton frère sera ton prophète.
7:2	Tu diras tout ce que je t'ordonnerai, et Aaron ton frère parlera à pharaon pour qu'il laisse aller les enfants d'Israël hors de son pays.
7:3	J'endurcirai le cœur de pharaon et je multiplierai mes signes et mes miracles dans le pays d'Égypte.
7:4	Pharaon ne vous écoutera pas. Je mettrai ma main sur l'Égypte et je sortirai mes armées, mon peuple, les enfants d'Israël, du pays d'Égypte, par de grands jugements.
7:5	Les Égyptiens sauront<!--Les nations sauront que Yéhoshoua ha Mashiah (Jésus-Christ) est l'Elohîm d'Israël lorsqu'il reviendra en Sion pour délivrer et restaurer son peuple (Za. 14).--> que je suis YHWH quand j'aurai étendu ma main sur l'Égypte, et que j'aurai fait sortir du milieu d'eux les enfants d'Israël.
7:6	Moshè et Aaron firent comme YHWH leur avait ordonné. Ainsi firent-ils.
7:7	Or Moshè était âgé de 80 ans, et Aaron de 83 ans, quand ils parlèrent à pharaon.

### La verge d'Aaron devient un serpent

7:8	YHWH parla à Moshè et à Aaron, en disant :
7:9	Quand pharaon vous parlera, en disant : Faites un miracle ! tu diras alors à Aaron : Prends ta verge, jette-la devant pharaon et elle deviendra un serpent.
7:10	Moshè donc et Aaron allèrent auprès de pharaon et agirent comme YHWH le leur avait ordonné. Aaron jeta sa verge devant pharaon et devant ses serviteurs et elle devint un serpent.
7:11	Mais pharaon fit venir aussi les sages et les sorciers, et les magiciens d'Égypte, eux aussi firent autant par leurs flammes<!--Le terme hébreu « lahat » signifie « flamme (d'une épée angélique) ». Terme que l'on trouve également dans Ge. 3:24.-->.
7:12	Ils jetèrent donc chacun leurs verges et elles devinrent des serpents, mais la verge d'Aaron engloutit leurs verges.
7:13	Le cœur de pharaon s'endurcit et il ne les écouta pas, comme YHWH l'avait dit.

### Les eaux du fleuve changées en sang

7:14	YHWH dit à Moshè : Le cœur de pharaon est endurci, il a refusé de laisser aller le peuple.
7:15	Va-t'en dès le matin vers pharaon. Voici, il sortira pour aller près de l'eau. Tu te présenteras donc devant lui sur le bord du fleuve. Tu prendras dans ta main la verge qui a été changée en serpent.
7:16	Et tu lui diras : YHWH, l'Elohîm des Hébreux, m'avait envoyé vers toi pour te dire : Laisse aller mon peuple, afin qu'il me serve dans le désert, mais voici, tu ne m'as pas écouté jusqu'ici.
7:17	Ainsi parle YHWH : À ceci tu sauras que je suis YHWH. Je vais frapper de la verge qui est dans ma main les eaux du fleuve, et elles seront changées en sang.
7:18	Et le poisson qui est dans le fleuve mourra, le fleuve deviendra puant, et les Égyptiens éprouveront du dégoût à boire des eaux du fleuve.
7:19	YHWH parla aussi à Moshè : Dis à Aaron : Prends ta verge et étends ta main sur les eaux des Égyptiens, sur leurs rivières, sur leurs ruisseaux, et sur leurs marais, et sur toutes leurs masses d'eaux, et elles deviendront du sang. Il y aura du sang par tout le pays d'Égypte, dans les vases de bois et de pierre.
7:20	Moshè donc et Aaron firent ce que YHWH avait ordonné. Aaron leva la verge, et il frappa les eaux du fleuve, sous les yeux de pharaon et de ses serviteurs. Toutes les eaux du fleuve furent changées en sang.
7:21	Et le poisson qui était dans le fleuve mourut, et le fleuve devint tellement puant que les Égyptiens ne pouvaient plus boire les eaux du fleuve. Il y eut du sang dans tout le pays d'Égypte.
7:22	Et les magiciens d'Égypte en firent de même par leurs enchantements. Et le cœur de pharaon s'endurcit tellement, qu'il ne les écouta pas, comme YHWH l'avait dit.
7:23	Et pharaon leur ayant tourné le dos, alla dans sa maison, et ne prit même pas à cœur ces choses qu'il avait vues.
7:24	Or tous les Égyptiens creusèrent autour du fleuve pour avoir de l'eau à boire, parce qu'ils ne pouvaient plus boire de l'eau du fleuve.
7:25	7 jours s’accomplirent après que YHWH eut frappé le fleuve.

## Chapitre 8

### Invasion de grenouilles

8:1	Après cela, YHWH dit à Moshè : Va vers pharaon et dis-lui : Ainsi parle YHWH : Laisse aller mon peuple afin qu'il me serve.
8:2	Si tu refuses de le laisser aller, voici, je vais frapper par des grenouilles toutes tes contrées.
8:3	Le fleuve grouillera de grenouilles. Elles monteront et entreront dans ta maison, dans la chambre où tu couches, sur ton lit, dans les maisons de tes serviteurs, parmi tout ton peuple, dans tes fours et dans tes cuves de pétrissage.
8:4	Ainsi, les grenouilles monteront sur toi, sur ton peuple et sur tous tes serviteurs.
8:5	YHWH donc dit à Moshè : Dis à Aaron : Étends ta main avec ta verge sur les fleuves, sur les rivières et sur les marais, et fais monter les grenouilles sur le pays d'Égypte.
8:6	Et Aaron étendit sa main sur les eaux de l'Égypte, et les grenouilles montèrent et couvrirent le pays d'Égypte.
8:7	Mais les magiciens firent de même par leurs enchantements et firent monter des grenouilles sur le pays d'Égypte.
8:8	Alors pharaon appela Moshè et Aaron, et leur dit : Intercédez auprès de YHWH afin qu'il détourne les grenouilles de moi et de mon peuple, et je laisserai aller le peuple afin qu'il sacrifie à YHWH.
8:9	Et Moshè dit à pharaon : Glorifie-toi à mon sujet ! Pour quand dois-je intercéder pour toi, pour tes serviteurs et pour ton peuple, afin d'exterminer les grenouilles loin de toi et de tes maisons ? Il en restera seulement dans le fleuve.
8:10	Alors il répondit : Pour demain. Et Moshè dit : Il sera fait selon ta parole, afin que tu saches que personne n'est semblable à YHWH notre Elohîm.
8:11	Les grenouilles donc se détourneront de toi, de tes maisons, de tes serviteurs et de ton peuple. Il en restera seulement dans le fleuve.
8:12	Alors Moshè et Aaron sortirent de chez pharaon. Moshè cria à YHWH au sujet des grenouilles qu'il avait fait venir sur pharaon.
8:13	Et YHWH agit selon la parole de Moshè. Ainsi les grenouilles moururent dans les maisons, dans les villages et dans les champs.
8:14	On les amassa par monceaux, et le pays en devint puant.
8:15	Mais pharaon, voyant qu'il y avait du répit, endurcit son cœur et ne les écouta pas, comme YHWH l'avait dit.

### Invasion de poux

8:16	Et YHWH dit à Moshè : Dis à Aaron : Étends ta verge et frappe la poussière de la terre. Elle deviendra des poux dans tout le pays d'Égypte.
8:17	Et ils firent ainsi. Aaron étendit sa main avec sa verge et frappa la poussière de la terre. Elle fut changée en poux, sur les humains et sur les bêtes. Toute la poussière du pays fut changée en poux dans tout le pays d'Égypte.
8:18	Les magiciens voulurent faire de même par leurs enchantements, pour produire des poux, mais ils ne purent pas. Les poux furent donc tant sur les humains que sur les bêtes.
8:19	Alors les magiciens dirent à pharaon : C'est le doigt d'Elohîm<!--Lu. 11:20.--> ! Toutefois, le cœur de pharaon s'endurcit et il ne les écouta pas, comme YHWH l'avait dit.

### Invasion d'insectes

8:20	Puis YHWH dit à Moshè : Lève-toi de bon matin et présente-toi devant pharaon. Voici, il sortira près de l'eau. Tu lui diras : Ainsi parle YHWH : Laisse aller mon peuple afin qu'il me serve.
8:21	Car si tu ne laisses pas aller mon peuple, voici, je vais envoyer contre toi, contre tes serviteurs, contre ton peuple et contre tes maisons, un mélange d'insectes. Les maisons des Égyptiens seront remplies de ce mélange, et la terre aussi sur laquelle ils seront<!--Ps. 78:43, 105:31.-->.
8:22	Et ce jour-là, je ferai une distinction pour la région de Gosen où se tient mon peuple, et là il n'y aura pas de mélange d'insectes afin que tu saches que je suis YHWH au milieu de cette région.
8:23	Et je fixerai une rançon afin de séparer ton peuple de mon peuple. Ce signe sera pour demain.
8:24	Et YHWH le fit ainsi. Un grand mélange d'insectes entra dans la maison de pharaon et dans chaque maison de ses serviteurs, et dans tout le pays d'Égypte et le pays fut dévasté par ce mélange.

### Pharaon trompe Moshè

8:25	Et pharaon appela Moshè et Aaron, et leur dit : Allez, sacrifiez à votre Elohîm dans ce pays.
8:26	Mais Moshè dit : Il n'est pas convenable d'agir ainsi. Car ce que nous sacrifierions à YHWH notre Elohîm, serait en abomination aux Égyptiens. Voici, si nous offrons sous leurs yeux des sacrifices qui sont en abomination aux Égyptiens, ne nous lapideraient-ils pas ?
8:27	Nous irons dans le désert, à trois journées de marche, et nous sacrifierons à YHWH notre Elohîm, comme il nous le dira.
8:28	Alors pharaon dit : Je vous laisserai aller pour sacrifier dans le désert à YHWH votre Elohîm. Toutefois, vous ne vous éloignerez pas, vous ne vous éloignerez pas en y allant. Intercédez pour moi.
8:29	Moshè dit : Voici, je sors de chez toi et j'intercéderai auprès de YHWH afin que le mélange d'insectes se détourne demain de pharaon, de ses serviteurs et de son peuple. Mais que pharaon ne continue pas à se moquer en ne laissant pas aller le peuple pour sacrifier à YHWH.
8:30	Alors Moshè sortit de chez pharaon et intercéda auprès de YHWH.
8:31	Et YHWH agit selon la parole de Moshè. Le mélange d'insectes se détourna de pharaon, de ses serviteurs et de son peuple. Il ne resta pas un seul insecte.
8:32	Mais pharaon endurcit son cœur cette fois encore et ne laissa pas aller le peuple.

## Chapitre 9

### La mort des troupeaux

9:1	Alors YHWH dit à Moshè : Va vers pharaon et dis-lui : Ainsi parle YHWH, l'Elohîm des Hébreux : Laisse aller mon peuple afin qu'il me serve.
9:2	Car si tu refuses de le laisser aller et si tu le retiens encore,
9:3	voici, la main de YHWH sera sur ton bétail qui est dans les champs, tant sur les chevaux que sur les ânes, sur les chameaux, sur les bœufs et sur les brebis, et il y aura une très grande mortalité.
9:4	Et YHWH distinguera le bétail des Israélites du bétail des Égyptiens, afin que rien de ce qui est aux enfants d'Israël ne meure.
9:5	Et YHWH fixa un temps, en disant : Demain, YHWH fera cela dans le pays.
9:6	YHWH donc fit cela dès le lendemain. Tout le bétail des Égyptiens mourut, mais du bétail des enfants d'Israël, il n'en mourut pas un seul.
9:7	Et pharaon envoya examiner, et voici, il n'y avait pas une seule bête morte du bétail des enfants d'Israël. Toutefois, le cœur de pharaon s'endurcit et il ne laissa pas aller le peuple.

### Des ulcères sur les Égyptiens et les bêtes

9:8	Alors YHWH dit à Moshè et à Aaron : Remplissez vos mains de cendre de fournaise et que Moshè la jette vers le ciel sous les yeux de pharaon.
9:9	Et elle deviendra de la poussière sur tout le pays d'Égypte, et elle provoquera sur les humains et sur les bêtes, des ulcères bourgeonnant en pustules, dans tout le pays d'Égypte.
9:10	Ils prirent donc de la cendre de fournaise et se tinrent devant pharaon. Moshè la jeta vers le ciel et il se forma des ulcères bourgeonnant en pustules tant sur les humains que sur les bêtes.
9:11	Et les magiciens ne purent se tenir devant Moshè, à cause des ulcères. Car les magiciens avaient des ulcères comme tous les Égyptiens.
9:12	Et YHWH endurcit le cœur de pharaon, et il ne les écouta pas, comme YHWH l'avait déclaré à Moshè.

### L'Égypte frappée par la grêle et le feu

9:13	Puis YHWH parla à Moshè : Lève-toi de bon matin et présente-toi devant pharaon, et dis-lui : Ainsi parle YHWH, l'Elohîm des Hébreux : Laisse aller mon peuple afin qu'il me serve.
9:14	Car cette fois, je vais envoyer toutes mes plaies contre ton cœur, contre tes serviteurs et contre ton peuple afin que tu saches que personne n'est semblable à moi sur toute la terre.
9:15	Car maintenant si j'avais étendu ma main, je t'aurais frappé de la peste, toi et ton peuple, et tu serais effacé de la terre.
9:16	Mais je t'ai laissé debout afin que tu voies ma force et qu'on publie mon Nom par toute la Terre<!--Ro. 9:17.-->.
9:17	T'élèves-tu encore contre mon peuple en ne le laissant pas partir ?
9:18	Voici, je ferai pleuvoir demain à cette même heure, une grêle tellement forte qu'il n'y en a pas eu de semblable en Égypte, depuis le jour où elle fut fondée jusqu'à maintenant.
9:19	Maintenant donc, envoie rassembler ton bétail et tout ce que tu as à la campagne. Car la grêle tombera sur tous les humains et sur toutes les bêtes qui se trouveront dans la campagne et qui n'auront pas été rassemblés à la maison, et ils mourront.
9:20	Ceux d'entre les serviteurs de pharaon qui craignirent la parole de YHWH, firent promptement retirer dans les maisons, leurs serviteurs et leurs bêtes.
9:21	Mais ceux qui ne prirent pas à cœur la parole de YHWH, laissèrent leurs serviteurs et leurs bêtes à la campagne.
9:22	Et YHWH dit à Moshè : Étends ta main vers le ciel et il y aura de la grêle sur tout le pays d'Égypte, sur les humains et sur les bêtes, et sur toutes les herbes des champs au pays d'Égypte.
9:23	Moshè donc étendit sa verge vers le ciel, et YHWH envoya des tonnerres et de la grêle, et le feu se promenait sur la terre. YHWH fit pleuvoir de la grêle sur le pays d'Égypte.
9:24	Il y eut de la grêle et le feu jaillissait au milieu de la grêle. Elle était si grosse qu'il n'y en avait pas eu de semblable sur toute la terre d'Égypte depuis qu'elle existe en tant que nation.
9:25	La grêle frappa dans tout le pays d'Égypte tout ce qui était aux champs, depuis les humains jusqu'aux bêtes. La grêle frappa aussi toutes les herbes des champs et brisa tous les arbres des champs.
9:26	Ce fut seulement dans la région de Gosen, où étaient les enfants d'Israël, qu'il n'y eut pas de grêle.

### Pharaon continue d'endurcir son cœur

9:27	Alors pharaon envoya appeler Moshè et Aaron, et leur dit : Cette fois-ci, j'ai péché. YHWH est juste, mais mon peuple et moi sommes méchants.
9:28	Suppliez YHWH pour qu'il n'y ait plus des voix d'Elohîm et de la grêle et je vous laisserai aller, on ne vous arrêtera plus.
9:29	Alors Moshè dit : Aussitôt que je sortirai de la ville, j'étendrai mes mains vers YHWH et les voix cesseront. Il n'y aura plus de grêle, afin que tu saches que la Terre est à YHWH<!--Ps. 24:1.-->.
9:30	Mais quant à toi et tes serviteurs, je sais que vous ne craignez pas encore YHWH Elohîm.
9:31	Or le lin et l'orge avaient été frappés, car l'orge était en épis et c'était la floraison du lin.
9:32	Mais le blé et l'épeautre ne furent pas frappés, parce qu'ils sont tardifs.
9:33	Moshè donc sortit de chez pharaon pour aller hors de la ville. Il étendit ses mains vers YHWH, et les tonnerres cessèrent, et la grêle et la pluie ne tombèrent plus sur la terre.
9:34	Pharaon, voyant que la pluie, la grêle et les tonnerres avaient cessé, continua encore de pécher, et il endurcit son cœur, lui et ses serviteurs.
9:35	Et le cœur de pharaon s'endurcit et il ne laissa pas aller les enfants d'Israël, comme YHWH l'avait dit par l'intermédiaire de Moshè.

## Chapitre 10

### Invasion de sauterelles

10:1	Et YHWH dit à Moshè : Va vers pharaon, car j'ai endurci son cœur et le cœur de ses serviteurs, afin de placer mes signes au milieu d'eux.
10:2	Et afin que tu racontes à ton fils et au fils de ton fils, les signes que j'accomplirai sur les Égyptiens et les prodiges que je ferai au milieu d'eux, et que vous sachiez que je suis YHWH.
10:3	Moshè donc et Aaron vinrent vers pharaon, et lui dirent : Ainsi parle YHWH, l'Elohîm des Hébreux : Jusqu'à quand refuseras-tu de t'humilier devant moi ? Laisse aller mon peuple afin qu'il me serve.
10:4	Car si tu refuses de laisser aller mon peuple, voici, je ferai venir demain des sauterelles dans ton territoire.
10:5	Elles couvriront la face de la terre et l'on ne pourra plus voir la terre. Elles dévoreront le reste de ce qui a échappé, ce que la grêle vous a laissé. Elles dévoreront tous les arbres qui poussent dans vos champs.
10:6	Et elles rempliront tes maisons, et les maisons de tous tes serviteurs, et les maisons de tous les Égyptiens, ce que tes pères n'ont pas vu ni les pères de tes pères, depuis qu'ils existent sur la terre jusqu'à ce jour. Puis, ayant tourné le dos à pharaon, il sortit d'auprès de lui.
10:7	Et les serviteurs de pharaon lui dirent : Jusqu'à quand cet homme sera-t-il un piège pour nous ? Laisse aller ces gens et qu'ils servent YHWH, leur Elohîm. Ne sais-tu pas encore que l'Égypte périt ?
10:8	Alors on fit revenir Moshè et Aaron vers pharaon, il leur dit : Allez ! Servez YHWH, votre Elohîm. Qui sont ceux qui iront ?
10:9	Et Moshè répondit : Nous irons avec nos jeunes hommes et nos vieillards, avec nos fils et nos filles. Nous irons avec nos brebis et nos bœufs, car nous avons à célébrer une fête à YHWH.
10:10	Alors il leur dit : Que YHWH soit avec vous, comme je laisserai aller vos petits enfants ! Prenez garde, car le mal est devant vous.
10:11	Cela ne se passera pas ainsi. Allez maintenant, vous les hommes, et servez YHWH, car c'est ce que vous demandiez. Et on les chassa de la présence de pharaon.
10:12	Alors YHWH dit à Moshè : Étends ta main sur le pays d'Égypte pour faire venir les sauterelles, afin qu'elles montent sur le pays d'Égypte, qu'elles dévorent toute l'herbe de la terre, tout ce que la grêle a laissé.
10:13	Moshè étendit donc sa verge sur le pays d'Égypte et YHWH amena sur le pays, tout ce jour-là et toute la nuit un vent d'orient. Le matin vint et le vent d'orient enleva les sauterelles.
10:14	Et les sauterelles montèrent sur tout le pays d'Égypte, elles se posèrent sur tout le territoire de l'Égypte en très grand nombre. Avant elles, il n'y avait pas eu de sauterelles semblables, et après elles, il n'y en aura pas de pareilles.
10:15	Et elles couvrirent la face de tout le pays, si bien que le pays en fut obscurci. Elles dévorèrent toute l'herbe de la terre, tout le fruit des arbres que la grêle avait laissé ; il ne resta aucune verdure aux arbres ni aux herbes des champs, dans tout le pays d'Égypte.
10:16	Aussitôt pharaon se hâta d'appeler Moshè et Aaron, et dit : J'ai péché contre YHWH votre Elohîm et contre vous.
10:17	Mais pardonne, je te prie, mon péché pour cette fois seulement. Intercédez auprès de YHWH votre Elohîm afin qu'il éloigne de moi encore cette mort.
10:18	Il sortit donc de chez pharaon et intercéda auprès de YHWH.
10:19	Et YHWH fit lever un vent d'occident très fort qui enleva les sauterelles et les précipita dans la Mer Rouge. Il ne resta pas une seule sauterelle dans tout le territoire de l'Égypte.
10:20	Mais YHWH endurcit le cœur de pharaon et il ne laissa pas aller les enfants d'Israël.

### Les ténèbres sur les Égyptiens

10:21	Puis YHWH dit à Moshè : Étends ta main vers les cieux, qu'il y ait sur le pays d'Égypte des ténèbres si épaisses, qu'on puisse les toucher à la main.
10:22	Moshè étendit donc sa main vers les cieux, et il y eut d'épaisses ténèbres dans tout le pays d'Égypte, pendant 3 jours<!--Ps. 105:28.-->.
10:23	L'homme ne voyait pas son frère et personne ne se leva de sa place pendant 3 jours. Mais pour tous les enfants d'Israël, il y avait de la lumière là où ils habitaient.

### Pharaon tente encore de compromettre Moshè

10:24	Alors pharaon appela Moshè et dit : Allez ! Servez YHWH. Seulement que vos brebis et vos bœufs restent. Vos petits enfants iront aussi avec vous.
10:25	Moshè répondit : Tu mettras toi-même entre nos mains de quoi faire des sacrifices et des holocaustes à YHWH notre Elohîm.
10:26	Et même nos troupeaux viendront aussi avec nous, il n'en restera pas un sabot. Car nous en prendrons pour servir YHWH, notre Elohîm, et nous ne savons pas ce que nous choisirons pour offrir à YHWH, jusqu'à ce que nous soyons arrivés en ce lieu là.
10:27	Mais YHWH endurcit le cœur de pharaon et il ne voulut pas les laisser aller.
10:28	Et pharaon lui dit : Va-t'en ! Arrière de moi ! Garde-toi de voir ma face, car le jour où tu verras ma face, tu mourras.
10:29	Alors Moshè répondit : Tu as bien dit, je ne verrai plus ta face<!--Hé. 11:27.-->.

## Chapitre 11

### Pharaon méprise l'avertissement sur la mort des premiers-nés

11:1	Et YHWH dit à Moshè : Je ferai venir encore une plaie sur pharaon et sur l'Égypte. Après cela, il vous renverra d'ici. Il vous renverra, et même, il vous chassera, il vous chassera complètement d'ici.
11:2	Parle maintenant aux oreilles du peuple et dis-leur : Que chacun demande à son voisin, et chacune à sa voisine, des vases d'argent et des vases d'or.
11:3	Or YHWH fit trouver grâce au peuple devant les Égyptiens. Et même Moshè passait pour un grand homme dans le pays d'Égypte, tant parmi les serviteurs de pharaon que parmi le peuple.
11:4	Et Moshè dit : Ainsi parle YHWH : Vers le milieu de la nuit, je passerai au travers de l'Égypte,
11:5	et tout premier-né mourra dans le pays d'Égypte, depuis le premier-né de pharaon qui doit s'asseoir sur son trône jusqu'au premier-né de la servante qui est derrière la meule, ainsi que tous les premiers-nés des bêtes.
11:6	Et il y aura un grand cri dans tout le pays d'Égypte, tel qu'il n'y en a jamais eu et qu'il n'y en aura jamais de semblable.
11:7	Mais contre tous les enfants d'Israël, pas même un chien ne remuera sa langue, depuis l'homme jusqu'aux bêtes, afin que vous sachiez qu'Elohîm distingue entre l’Égypte et Israël.
11:8	Et tous tes serviteurs viendront vers moi et se prosterneront devant moi, en disant : Sors, toi, et tout le peuple qui est avec toi. Après cela, je sortirai. Ainsi, Moshè sortit de chez pharaon dans une ardente colère.
11:9	YHWH donc dit à Moshè : Pharaon ne vous écoutera pas, afin que mes miracles soient multipliés dans le pays d'Égypte.
11:10	Et Moshè et Aaron firent tous ces miracles-là devant pharaon. Et YHWH endurcit tellement le cœur de pharaon, qu'il ne laissa pas aller les enfants d'Israël hors de son pays.

## Chapitre 12

### La première Pâque

12:1	Or YHWH parla à Moshè et à Aaron dans le pays d'Égypte, en disant :
12:2	Ce mois-ci sera pour vous le premier des mois, il sera pour vous le premier des mois de l'année.
12:3	Parlez à toute l'assemblée d'Israël, en disant : Le dixième jour de ce mois, que chaque homme prenne un agneau ou un chevreau par maison de père, un agneau ou un chevreau par maison.
12:4	Mais si la maison est trop petite pour un agneau, on le prendra avec le voisin le plus proche de sa maison, selon le nombre de personnes. Vous compterez pour cet agneau selon ce que chacun peut manger.
12:5	Ce sera un agneau ou un chevreau sans défaut, mâle, âgé d'un an<!--La Pâque juive était célébrée le 14ème jour du premier mois de l'année juive soit, le 14 du mois de Nisan (Ex. 12:2 ; No. 9:1-5). L'agneau pascal était une préfiguration de Yéhoshoua ha Mashiah (Jésus-Christ) : l'agneau d'Elohîm qui ôte le péché du monde (Jn. 1:29). Ses caractéristiques sont les suivantes : \\- L'agneau devait nécessairement être un mâle sans défaut (Ex. 12:5). Yéhoshoua est l'enfant mâle mis au monde par une vierge, il n'a pas été affecté par le sang corrompu d'Adam, il est donc sans défaut (Es. 7:14 ; Mt. 1:20-21). Pour être certains de la perfection de l'animal, les Hébreux devaient l'examiner pendant 4 jours avant de l'immoler (Ex. 12:3-6). Il est à noter que la torah exigeait que 2 ou 3 témoins soient présents pour constater un crime ou un péché (De. 17:6, 19:15), ces 4 jours font donc office de 4 témoins pour attester de la pureté de l'animal. De même, les 4 auteurs de l'Évangile attestent la sainteté du Seigneur. De plus, avant sa mise à mort, le Seigneur a été examiné par deux législations : juive (le sanhédrin) et romaine (Ponce Pilate, Hérode). Ces deux législations attestèrent, malgré elles, son innocence (Mt. 26:60, 27:24 ; Mc. 14:55-56, 15:14 ; Lu. 23:4 ; Jn. 18:31, 19:6) et confirmèrent qu'il était sans défaut et donc digne d'être offert en sacrifice.\\- YHWH avait prescrit aux Hébreux d'immoler l'agneau entre les deux soirs (Ex. 12:6), c'est-à-dire avant le crépuscule, entre la neuvième et la onzième heure. Yéhoshoua fut arrêté la nuit de Pâque (Mc. 14:12-41). Sa crucifixion eut lieu le lendemain, à la troisième heure (Mc. 15:25), et sa mort survint à la neuvième heure (Mt. 27:45). L'agneau devait être rôti au feu puis consommé avec du pain sans levain et des herbes amères (Ex. 12:8). Le feu symbolise le jugement que le Seigneur a pris sur lui à cause de nos péchés (Es. 53:5 ; Ro. 4:25 ; 1 Pi. 1:18-20). Le pain sans levain est une autre image de Yéhoshoua, le pain de vie (Jn. 6:35) sans aucun péché (1 Co. 5:8). Les herbes amères préfigurent, quant à elles, l'affliction et la souffrance du Seigneur (Hé. 2:10).-->. Vous le prendrez d'entre les brebis ou d'entre les chèvres.
12:6	Et vous le garderez jusqu'au quatorzième jour de ce mois, et toute la congrégation de l'assemblée d'Israël l'égorgera entre les deux soirs.
12:7	Et ils prendront de son sang et le mettront sur les deux poteaux et sur le linteau de la porte des maisons, où ils le mangeront.
12:8	Et ils en mangeront la chair rôtie au feu cette nuit-là. Ils la mangeront avec des pains sans levain et avec des herbes amères.
12:9	N'en mangez rien de cru, ni qui ait été bouilli dans l'eau, mais rôti au feu, sa tête, ses jambes et ses entrailles.
12:10	Et ne laissez aucun reste jusqu'au matin, mais s'il en reste quelque chose le matin, vous le brûlerez au feu.
12:11	Et vous le mangerez ainsi : vos reins seront ceints, vous aurez vos sandales à vos pieds et votre bâton à la main, et vous le mangerez à la hâte. C'est la Pâque de YHWH.

### Le sang qui sauve ; l'instauration de la fête de la Pâque

12:12	Car je passerai cette nuit-là par le pays d'Égypte, et je frapperai tout premier-né au pays d'Égypte, depuis les humains jusqu'aux bêtes et j'exercerai des jugements sur tous les elohîm de l'Égypte. Je suis YHWH.
12:13	Le sang sera pour vous un signe sur les maisons où vous serez : quand je verrai le sang, je passerai par-dessus vous, et il n'y aura pas sur vous de plaie de destruction quand je frapperai le pays d'Égypte.
12:14	Et ce jour là, vous conserverez le souvenir de ce jour et vous le célébrerez comme une fête à YHWH. Vous le célébrerez comme une fête solennelle par un statut perpétuel, de génération en génération.
12:15	Vous mangerez pendant 7 jours des pains sans levain, et dès le premier jour, vous ôterez le levain de vos maisons. Car quiconque mangera du pain levé, depuis le premier jour jusqu'au septième, cette personne-là sera retranchée d'Israël.
12:16	Le premier jour, il y aura une sainte convocation, et il y aura de même au septième jour une sainte convocation. On ne fera aucune œuvre ces jours-là, vous préparerez seulement ce que chaque personne doit manger.
12:17	Vous prendrez donc garde aux pains sans levain, parce qu'en ce même jour, j'ai fait sortir vos armées du pays d'Égypte. Vous observerez donc ce jour-là de génération en génération par un statut perpétuel.
12:18	Au premier mois, le quatorzième jour du mois, au soir, vous mangerez des pains sans levain jusqu'au vingt et unième jour du mois, au soir.
12:19	Il ne se trouvera pas de levain dans vos maisons pendant 7 jours, car quiconque mangera du pain levé, cette personne-là sera retranchée de l'assemblée d'Israël, tant celui qui habite comme étranger que celui qui est né dans le pays.
12:20	Vous ne mangerez pas de pain levé dans tous les lieux où vous habiterez, vous mangerez des pains sans levain.
12:21	Puis Moshè appela tous les anciens d'Israël et leur dit : Choisissez et prenez un agneau ou un chevreau selon vos familles, et immolez la Pâque.
12:22	Puis vous prendrez un bouquet d'hysope et le tremperez dans le sang qui sera dans un bassin, et vous arroserez du sang qui sera dans le bassin, le linteau et les deux poteaux. Aucun de vous ne sortira de la porte de sa maison jusqu'au matin.
12:23	Car YHWH passera pour frapper l'Égypte et il verra le sang sur le linteau et sur les deux poteaux, et YHWH passera par-dessus la porte, et ne permettra pas que le destructeur entre dans vos maisons pour frapper.
12:24	Vous garderez ceci comme une ordonnance perpétuelle, pour toi et pour tes fils.
12:25	Quand donc vous serez entrés dans le pays que YHWH vous donnera, comme il l'a dit, vous observerez ce service.
12:26	Et quand vos fils vous diront : Que signifie pour vous ce service ?
12:27	Alors vous répondrez : C'est le sacrifice de la Pâque pour YHWH, qui passa en Égypte par-dessus les maisons des enfants d'Israël, quand il frappa l'Égypte et qu'il préserva nos maisons. Alors le peuple s'inclina et se prosterna.
12:28	Ainsi les enfants d'Israël s'en allèrent et firent comme YHWH l'ordonna à Moshè et à Aaron. Ainsi firent-ils.

### Les premiers-nés d'Égypte frappés

12:29	Et il arriva qu'au milieu de la nuit, YHWH frappa tous les premiers-nés du pays d'Égypte, depuis le premier-né de pharaon qui devait s'asseoir sur son trône, jusqu'aux premiers-nés des captifs qui étaient dans la prison, et tous les premiers-nés des bêtes.
12:30	Et pharaon se leva de nuit, lui et ses serviteurs et tous les Égyptiens. Il y eut un grand cri en Égypte, car il n'y avait pas de maison où il n'y eût un mort<!--No. 8:17 ; Ps. 78:51, 105:36 ; Hé. 11:28.-->.

### Israël sort d'Égypte

12:31	Il appela donc Moshè et Aaron de nuit, et leur dit : Levez-vous, sortez du milieu de mon peuple, tant vous que les enfants d'Israël, allez et servez YHWH, comme vous l'avez dit.
12:32	Prenez aussi votre petit et votre gros bétail, comme vous l'avez dit, et allez-vous-en et bénissez-moi.
12:33	Et les Égyptiens pressaient le peuple et se hâtaient de les faire sortir du pays, car ils disaient : Nous sommes tous morts !
12:34	Le peuple donc prit sa pâte avant qu'elle soit levée, ayant leurs cuves de pétrissage serrées dans leurs vêtements sur leurs épaules.
12:35	Et les enfants d'Israël firent selon la parole de Moshè, et demandèrent aux Égyptiens des vases d'argent et d'or, et des vêtements.
12:36	Et YHWH fit trouver grâce au peuple auprès des Égyptiens, qui les leur prêtèrent. Ainsi ils dépouillèrent les Égyptiens.
12:37	Ainsi, les enfants d'Israël étant partis de Ramsès, vinrent à Soukkoth, environ 600 000 hommes de pied, sans les enfants.
12:38	Beaucoup de races mélangées<!--Peuple mêlé.--> montèrent avec eux, ainsi que des brebis, des bœufs et un bétail extrêmement nombreux.
12:39	Et ils firent cuire des gâteaux sans levain avec la pâte qu'ils avaient emportée d'Égypte, et qui n'était pas levée. Car ils avaient été chassés d'Égypte, sans pouvoir s'attarder et sans avoir fait de provisions pour eux.
12:40	La durée du séjour des enfants d'Israël en Égypte fut de 430 ans<!--Ge. 15:13 ; Ac. 7:6 ; Ga. 3:17.-->.
12:41	Il arriva donc au bout de 430 ans, il arriva en ce même jour, que toutes les armées de YHWH sortirent du pays d'Égypte.
12:42	Ce fut là une nuit de veille pour YHWH, parce qu'il les fit sortir du pays d'Égypte. Cette nuit-là est à observer en l'honneur de YHWH, par tous les enfants d'Israël, de génération en génération<!--De. 16:1-6.-->.
12:43	YHWH dit aussi à Moshè et à Aaron : Voici le statut de la Pâque : Aucun étranger n'en mangera.
12:44	Quant à tout esclave, homme acquis à prix d'argent, tu le circonciras, et alors il en mangera.
12:45	L'étranger et le mercenaire n'en mangeront pas.
12:46	On la mangera dans une même maison, et vous n'emporterez pas de chair hors de la maison, et vous n'en briserez aucun os<!--Voir Jn. 19:36.-->.
12:47	Toute l'assemblée d'Israël la fera.
12:48	Et quand un étranger qui séjournera chez toi voudra faire la Pâque pour YHWH, que tout mâle qui lui appartient soit circoncis, et alors il s'approchera pour la faire, et il sera comme celui qui est né dans le pays. Mais aucun incirconcis n'en mangera.
12:49	Il y aura une même torah pour celui qui est né dans le pays et pour l'étranger qui habite parmi vous.
12:50	Tous les enfants d'Israël agirent comme YHWH l'avait ordonné à Moshè et à Aaron. C'est ainsi qu'ils agirent.
12:51	Il arriva donc en ce même jour que YHWH fit sortir les enfants d'Israël du pays d'Égypte, selon leurs armées.

## Chapitre 13

### Consécration des premiers-nés à YHWH

13:1	Et YHWH parla à Moshè, et dit :
13:2	Consacre-moi tout premier-né, tout premier-né qui ouvre la matrice parmi les fils d'Israël, tant des humains que des bêtes, il m'appartient<!--Lé. 27:26-27 ; No. 3:13, 8:17 ; Lu. 2:22-23.-->.
13:3	Moshè donc dit au peuple : Souvenez-vous de ce jour où vous êtes sortis d'Égypte, de la maison des esclaves. Car YHWH vous en a fait sortir par sa main puissante ; on ne mangera donc pas de pain levé.
13:4	Vous sortez aujourd'hui dans le mois où les épis<!--Nisan (ou Abib) = Mars-Avril. Voir De. 16:1.--> mûrissent.
13:5	Quand donc YHWH t'aura introduit dans le pays des Cananéens, des Héthiens, des Amoréens, des Héviens et des Yebousiens, qu'il a juré à tes pères de te donner, et qui est un pays où coulent le lait et le miel, alors tu feras ce service durant ce mois-ci.
13:6	Pendant 7 jours tu mangeras des pains sans levain et le septième jour il y aura une fête à YHWH.
13:7	On mangera durant 7 jours des pains sans levain. On ne verra pas de pain levé chez toi, et l'on ne verra pas de levain chez toi dans toutes tes contrées.
13:8	Et ce jour-là tu expliqueras ces choses à tes enfants, en disant : C'est à cause de ce que YHWH a fait pour moi lorsque je suis sorti d'Égypte.
13:9	Et ce sera pour toi un signe sur ta main, et comme un rappel entre tes yeux, afin que la torah de YHWH soit dans ta bouche, car YHWH t'a fait sortir d'Égypte par sa main puissante<!--De. 6:8, 11:18.-->.
13:10	Tu observeras ce statut au temps fixé d'année en année.
13:11	Et quand YHWH t'aura introduit dans le pays des Cananéens, selon qu'il a juré à toi et à tes pères, et qu'il te l'aura donné,
13:12	tu consacreras à YHWH tout premier-né qui ouvre la matrice, même tout premier-né des animaux que tu auras, les mâles appartiendront à YHWH.
13:13	Et tu rachèteras avec un agneau ou un chevreau, tout premier-né de l'ânesse, et si tu ne le rachètes pas, tu lui briseras la nuque. Tu rachèteras aussi tout premier-né de l'être humain parmi tes fils.
13:14	Et quand ton fils t'interrogera à l'avenir, en disant : Que veut dire ceci ? Alors tu lui diras : C'est par la force de sa main que YHWH nous a fait sortir d'Égypte, de la maison des esclaves.
13:15	Il est arrivé que pharaon s'obstinait à ne pas nous laisser partir, alors YHWH a tué tous les premiers-nés dans le pays d'Égypte, depuis les premiers-nés des humains jusqu'aux premiers-nés des bêtes. Voilà pourquoi je sacrifie à YHWH tout premier-né mâle qui ouvre la matrice et je rachète tout premier-né de mes fils.
13:16	Ce sera pour toi un signe sur ta main, un fronteau entre tes yeux, car c'est par la force de sa main que YHWH nous a fait sortir d'Égypte.

### Début du voyage, YHWH dirige son peuple

13:17	Or, lorsque pharaon laissa partir le peuple, Elohîm ne les conduisit pas par le chemin du pays des Philistins, qui était pourtant le plus proche. Elohîm se dit en effet : De peur que le peuple ne se repente, quand ils verront la guerre et qu'ils ne retournent en Égypte.
13:18	Mais Elohîm fit tourner le peuple par le chemin du désert, vers la Mer Rouge. Ainsi, les enfants d'Israël montèrent en armes hors du pays d'Égypte.
13:19	Et Moshè prit avec lui les ossements de Yossef, parce que Yossef avait fait jurer, il avait fait jurer les enfants d'Israël en leur disant : Elohîm vous visitera, il vous visitera et vous transporterez donc avec vous mes ossements d'ici<!--Ge. 50:25 ; Jos. 24:32.-->.
13:20	Et ils partirent de Soukkoth et campèrent à Étham, qui est à l'extrémité du désert.
13:21	Et YHWH marchait devant eux, le jour dans une colonne de nuée pour les conduire par le chemin, et la nuit dans une colonne de feu pour les éclairer, afin qu'ils marchent jour et nuit<!--No. 9:13-23, 10:34 ; De. 1:33 ; Né. 9:12-19 ; 1 Co. 10:1.-->.
13:22	Et il ne retira pas la colonne de nuée le jour, ni la colonne de feu la nuit de devant le peuple.

## Chapitre 14

### Pharaon et son armée à la poursuite d'Israël

14:1	Et YHWH parla à Moshè et dit :
14:2	Parle aux enfants d'Israël et dis-leur : Qu'ils se détournent et qu'ils campent devant Pi-Hahiroth, entre Migdol et la mer, vis-à-vis de Baal-Tsephon. Vous camperez vis-à-vis de ce lieu-là, près de la mer<!--No. 33:7.-->.
14:3	Pharaon dira des enfants d'Israël : Ils sont confus dans le pays, le désert les a enfermés.
14:4	Et j'endurcirai le cœur de pharaon et il vous poursuivra. Ainsi je serai glorifié par le moyen de pharaon et de toute son armée, et les Égyptiens sauront que je suis YHWH. C'est ce qu'ils firent.
14:5	Or on avait rapporté au roi d'Égypte que le peuple s'enfuyait, et le cœur de pharaon et de ses serviteurs fut changé à l'égard du peuple, et ils se dirent : Qu'est-ce que nous avons fait en laissant aller Israël, de sorte qu'il ne nous servira plus ?
14:6	Alors il fit atteler son char et il prit son peuple avec lui.
14:7	Il prit donc 600 chars sélectionnés et tous les chars de l'Égypte, chacun d'eux monté par des officiers.
14:8	Et YHWH endurcit le cœur de pharaon, roi d'Égypte, qui poursuivit les enfants d'Israël. Or les fils d'Israël étaient sortis à main levée<!--Lé. 26:13 ; No. 33:3.-->.
14:9	Les Égyptiens les poursuivirent et tous les chevaux des chars de pharaon, ses cavaliers et son armée les atteignirent, comme ils étaient campés près de la mer, vers Pi-Hahiroth vis-à-vis de Baal-Tsephon.
14:10	Et pharaon approchait. Les enfants d'Israël levèrent leurs yeux, et voici, les Égyptiens marchaient après eux. Et les fils d'Israël eurent une grande frayeur et crièrent à YHWH.
14:11	Ils dirent aussi à Moshè : Est-ce qu'il n'y avait pas des sépulcres en Égypte pour que tu nous aies emmenés pour mourir dans le désert ? Que nous as-tu fait en nous faisant sortir d'Égypte ?
14:12	N'est-ce pas ce que nous te disions en Égypte, en disant : Retire-toi de nous et que nous servions les Égyptiens ? Car nous aimons mieux les servir que de mourir dans le désert.

### Délivrance miraculeuse par YHWH

14:13	Et Moshè dit au peuple : N'ayez pas peur, arrêtez-vous et voyez la délivrance que YHWH vous donnera aujourd'hui. Car les Égyptiens que vous voyez aujourd'hui, vous ne les verrez plus.
14:14	YHWH combattra pour vous, et vous, tenez-vous tranquilles.
14:15	Et YHWH dit à Moshè : Pourquoi cries-tu vers moi ? Parle aux enfants d'Israël et qu'ils marchent.
14:16	Et toi, lève ta verge, étends ta main sur la mer, fends-la et que les enfants d'Israël entrent au milieu de la mer à sec.
14:17	Et quant à moi, voici, je vais endurcir le cœur des Égyptiens afin qu'ils entrent après eux, et je serai glorifié par le moyen de pharaon, de toute son armée, de ses chars et de ses cavaliers.
14:18	Et les Égyptiens sauront que je suis YHWH, quand j'aurai été glorifié par le moyen de pharaon, de ses chars et de ses cavaliers.
14:19	Et l'Ange d'Elohîm qui marchait devant le camp d'Israël partit et s'en alla derrière eux, et la colonne de nuée partit de devant eux et se tint derrière eux.
14:20	Elle vint entre le camp des Égyptiens et le camp d'Israël. Elle était nuée et ténèbres, et elle éclairait la nuit. Ils ne s'approchèrent pas les uns des autres pendant toute la nuit.
14:21	Alors Moshè étendit sa main sur la mer, et YHWH fit reculer la mer toute la nuit par un puissant vent d'orient et il mit la mer à sec. Les eaux se fendirent<!--Jos. 4:23 ; Ps. 66:6, 106:9 ; Hé. 11:29.-->.
14:22	Et les enfants d'Israël entrèrent au milieu de la mer à sec, et les eaux leur servaient de mur à droite et à gauche.
14:23	Les Égyptiens les poursuivirent, tous les chevaux du pharaon avec ses chars et ses cavaliers entrèrent après eux au milieu de la mer.
14:24	Mais il arriva que sur la veille du matin, YHWH étant dans la colonne de feu et dans la nuée, regarda le camp des Égyptiens et le mit en déroute.
14:25	Il ôta les roues de leurs chars et alourdit leur marche. Alors les Égyptiens dirent : Fuyons de devant les Israélites, car YHWH combat pour eux contre les Égyptiens.
14:26	Et YHWH dit à Moshè : Étends ta main sur la mer, et les eaux retourneront sur les Égyptiens, sur leurs chars et sur leurs cavaliers.
14:27	Moshè donc étendit sa main sur la mer et, vers le matin, la mer revint à sa place habituelle. Les Égyptiens en fuyant la rencontrèrent, et YHWH se débarrassa des Égyptiens au milieu de la mer.
14:28	Car les eaux retournèrent et couvrirent les chars et les cavaliers de toute l'armée de pharaon, qui étaient entrés après les Israélites dans la mer, et il n'en resta pas un seul.
14:29	Mais les enfants d'Israël marchèrent au milieu de la mer à sec, et les eaux leur servaient de mur à droite et à gauche.
14:30	Ainsi, YHWH délivra, en ce jour-là, Israël de la main des Égyptiens. Israël vit sur le bord de la mer les Égyptiens morts.
14:31	Israël vit donc la grande puissance que YHWH avait déployée contre les Égyptiens. Le peuple craignit YHWH et il crut en YHWH et en Moshè, son serviteur.

## Chapitre 15

### Cantique de délivrance

15:1	Alors Moshè et les enfants d'Israël chantèrent ce cantique à YHWH. Ils parlèrent, disant : Je chanterai à YHWH car il s'est levé, il s'est levé : il a jeté dans la mer le cheval et celui qui le montait.
15:2	Yah<!--Yah est la forme raccourcie de YHWH. Voir le dictionnaire en annexe.--> est ma force et ma louange, et il est devenu mon salut<!--Il est devenu mon Yeshuw`ah.-->. C'est mon El : je lui dresserai un tabernacle, c'est l'Elohîm de mon père, je l'exalterai.
15:3	YHWH est un homme de guerre, YHWH est son Nom.
15:4	Il a jeté dans la mer les chars de pharaon et son armée, l'élite de ses capitaines a été submergée dans la Mer Rouge.
15:5	Les profondeurs les ont couverts, ils sont descendus au fond des eaux comme une pierre<!--Né. 9:11.-->.
15:6	Ta droite, ô YHWH, s'est montrée magnifique en force ! Ta droite, ô YHWH, a brisé l'ennemi<!--Ps. 77:16, 118:15-16.--> !
15:7	Tu as ruiné par la grandeur de ta majesté ceux qui s'élevaient contre toi. Tu as lâché ta colère et elle les a consumés comme du chaume.
15:8	Par le souffle de tes narines, les eaux se sont amoncelées, les eaux courantes se sont arrêtées comme une digue, les abîmes se sont figés au cœur de la mer.
15:9	L'ennemi disait : Je poursuivrai, j'atteindrai, je partagerai le butin, mon âme en sera pleine. Je tirerai mon épée, ma main les détruira !
15:10	Tu as soufflé de ton vent, la mer les a couverts ! Ils se sont enfoncés comme du plomb au plus profond des eaux.
15:11	Qui est comme toi parmi les elohîm, ô YHWH ! Qui est comme toi, magnifique en sainteté, redoutable, digne de louanges, faisant des choses merveilleuses ?
15:12	Tu as étendu ta main droite, la terre les a engloutis.
15:13	Tu as conduit par ta miséricorde ce peuple que tu as racheté. Tu l'as conduit par ta force à la demeure de ta sainteté.
15:14	Les peuples l'ont entendu et ils en ont tremblé : la douleur a saisi les habitants du pays des Philistins.
15:15	Alors les princes d'Édom seront troublés, et le tremblement saisira les puissants de Moab, tous les habitants de Canaan se fondront.
15:16	La terreur et la crainte tomberont sur eux. Ils deviendront muets comme une pierre par la grandeur de ton bras, jusqu'à ce que ton peuple soit passé, ô YHWH ! jusqu'à ce que ce peuple que tu as acquis soit passé<!--De. 2:25, 11:25 ; Jos. 2:9.-->.
15:17	Tu les introduiras et les planteras sur la montagne de ton héritage, au lieu que tu as préparé pour ta demeure, YHWH ! au sanctuaire, Adonaï, que tes mains ont établi !
15:18	YHWH régnera à jamais et à perpétuité.
15:19	Car les chevaux de pharaon, ses chars et ses cavaliers sont entrés dans la mer, et YHWH a fait retourner sur eux les eaux de la mer, mais les enfants d'Israël ont marché à sec au milieu de la mer.
15:20	Et Myriam, la prophétesse, sœur d'Aaron, prit à la main un tambourin, et toutes les femmes sortirent derrière elle, avec des tambourins et des danses.
15:21	Et Myriam leur répondait : Chantez à YHWH car il s'est levé, il s'est levé : il a jeté dans la mer le cheval et celui qui le montait.

### YHWH pourvoit pour son peuple

15:22	Après cela, Moshè fit partir les Israélites de la Mer Rouge, et ils partirent vers le désert de Shour. Après 3 jours de marche dans le désert, ils ne trouvèrent pas d'eau.
15:23	De là, ils vinrent à Marah, mais ils ne purent boire les eaux de Marah parce qu'elles étaient amères. C'est pourquoi son nom fut appelé Marah.
15:24	Et le peuple murmura contre Moshè en disant : Que boirons-nous ?
15:25	Et Moshè cria à YHWH, et YHWH lui montra<!--« Montra » de l'hébreu « yarah » qui veut également dire « enseigner », « signaler », « lancer », « instruire », « informer », « montrer », « jeter » etc.--> un certain bois qu'il jeta dans les eaux, et les eaux devinrent douces. Il lui proposa là une ordonnance et une loi, et il l'éprouva là,
15:26	et lui dit : Si tu écoutes, si tu écoutes la voix de YHWH, ton Elohîm, si tu fais ce qui est droit à ses yeux, si tu prêtes l'oreille à ses commandements, si tu gardes toutes ses ordonnances, je ne ferai venir sur toi aucune des infirmités que j'ai fait venir sur l'Égypte, car je suis YHWH-Rapha<!--YHWH qui te guérit. De. 7:12-15.-->.
15:27	Puis ils vinrent à Élim, où il y avait 12 fontaines d'eau et 70 palmiers. Et ils campèrent là, près des eaux.

## Chapitre 16

### YHWH envoie la manne

16:1	Et toute l'assemblée des enfants d'Israël étant partie d'Élim, vint dans le désert de Sin, qui est entre Élim et Sinaï, le quinzième jour du second mois après qu'ils furent sortis du pays d'Égypte.
16:2	Et toute l'assemblée des enfants d'Israël murmura dans ce désert contre Moshè et Aaron.
16:3	Et les enfants d'Israël leur dirent : Ah ! Pourquoi ne sommes-nous pas morts par la main de YHWH dans le pays d'Égypte, quand nous étions assis près des pots de viande et que nous mangions du pain à satiété ? Car vous nous avez amenés dans ce désert pour faire mourir de faim toute cette assemblée<!--No. 11:4 ; 1 Co. 10:10.-->.
16:4	Et YHWH dit à Moshè : Voici, je vais vous faire pleuvoir du pain pour vous depuis le ciel. Le peuple sortira et recueillera la chose jour par jour. Ainsi, je le mettrai à l'épreuve et je verrai s'il marche ou non dans ma torah.
16:5	Mais qu'ils apprêtent au sixième jour ce qu'ils auront apporté, et qu'il y ait le double de ce qu'ils recueilleront chaque jour.
16:6	Moshè et Aaron dirent à tous les enfants d'Israël : Ce soir vous saurez que YHWH vous a fait sortir du pays d'Égypte.
16:7	Et au matin vous verrez la gloire de YHWH, parce qu'il a entendu vos murmures, qui sont contre YHWH. Car qui sommes-nous pour que vous murmuriez contre nous ?
16:8	Et Moshè dit : YHWH vous donnera ce soir de la viande à manger et, au matin, du pain à satiété, parce que YHWH entend vos murmures que vous murmurez contre lui. Car que sommes-nous ? Vos murmures ne sont pas contre nous, mais contre YHWH.
16:9	Et Moshè dit à Aaron : Dis à toute l'assemblée des enfants d'Israël : Approchez-vous de la présence de YHWH, car il a entendu vos murmures.
16:10	Or il arriva qu'aussitôt qu'Aaron eut parlé à toute l'assemblée des enfants d'Israël, ils regardèrent vers le désert, et voici, la gloire de YHWH apparut dans la nuée.
16:11	Et YHWH parla à Moshè, en disant :
16:12	J'ai entendu les murmures des enfants d'Israël. Parle-leur et dis-leur : Entre les deux soirs, vous mangerez de la chair, et au matin vous serez rassasiés de pain. Vous saurez que je suis YHWH votre Elohîm.
16:13	Le soir, des cailles montèrent et couvrirent le camp, et au matin, il y avait une couche de rosée tout autour du camp.
16:14	Et lorsque cette couche de rosée fut dissipée, voici, sur la surface du désert il y avait une petite chose ronde, aussi petite que la gelée blanche sur le sol.
16:15	Quand les enfants d'Israël la virent, chaque homme dit à son frère : C'est la manne ! car ils ne savaient ce que c'était. Et Moshè leur dit : C'est le pain que YHWH vous donne à manger<!--Ps. 105:40.-->.

### Récolte de la manne

16:16	Voici ce que YHWH a ordonné : Que chacun en recueille autant qu'il lui en faut pour sa nourriture, un omer par tête, selon le nombre de vos personnes. Chacun en prendra pour ceux qui sont dans sa tente.
16:17	Les enfants d'Israël firent donc ainsi, et les uns en recueillirent plus, les autres moins.
16:18	Et ils le mesuraient par omer. Celui qui en recueillait beaucoup n'avait rien de trop, et celui qui en recueillait peu, n'en manquait pas. Mais chacun recueillait ce qu'il lui fallait pour manger.
16:19	Et Moshè leur avait dit : Que personne n'en laisse rien de reste jusqu'au matin !
16:20	Certains n'obéirent pas à Moshè et en laissèrent jusqu'au matin, mais les vers s'y mirent et cela sentait mauvais. Et Moshè se fâcha contre ces gens.
16:21	Ainsi, chacun en recueillait tous les matins autant qu'il lui en fallait pour se nourrir, et lorsque la chaleur du soleil était venue, elle se fondait.
16:22	Mais le sixième jour, ils recueillirent du pain en double, 2 omers pour chacun. Les princes de l'assemblée vinrent pour le rapporter à Moshè.

### Le shabbat<!--Né. 9:13-14 ; Mt. 12:1.-->

16:23	Il leur dit : C'est ce que YHWH a dit : Demain est le repos, le shabbat consacré à YHWH. Faites cuire ce que vous avez à cuire, faites bouillir ce que vous avez à bouillir, et tout ce qu'il y aura en surplus, mettez-le en réserve pour le garder jusqu'au matin.
16:24	Ils le mirent donc en réserve, comme Moshè l'avait ordonné, et il ne sentit pas mauvais, et il n'y eut pas de vers dedans.
16:25	Alors Moshè dit : Mangez-le aujourd'hui, car c'est aujourd'hui le repos de YHWH. Aujourd'hui vous n'en trouverez pas dans les champs.
16:26	Durant 6 jours vous le recueillerez, mais le septième est le shabbat, il n'y en aura pas ce jour-là.
16:27	Au septième jour, quelques-uns du peuple sortirent pour en recueillir, mais ils n'en trouvèrent pas.
16:28	YHWH dit à Moshè : Jusqu'à quand refuserez-vous de garder mes commandements et ma torah ?
16:29	Considérez que YHWH vous a donné le shabbat, c'est pourquoi il vous donne au sixième jour du pain pour deux jours. Que chacun reste à sa place et que personne ne sorte du lieu où il est le septième jour<!--Voir commentaire en Mt. 24:20.-->.
16:30	Le peuple donc se reposa le septième jour.
16:31	La maison d'Israël appelait cela du nom de manne<!--Le mot « manne » vient de l'hébreu « man » et veut dire « Qu'est-ce que cela ? ». La manne est une image de Yéhoshoua (Jésus) le Pain de vie descendu du ciel (Jn. 6:32-52). La consommation quotidienne du Pain de vie, qui est aussi la parole d'Elohîm, apporte la vie éternelle.-->. Elle était comme de la semence de coriandre blanche et ayant le goût d'un gâteau au miel.
16:32	Moshè dit : Voici ce que YHWH a ordonné : Qu'on en remplisse un omer pour le garder pour vos générations, afin qu'on voie le pain que je vous ai fait manger dans le désert, après vous avoir fait sortir du pays d'Égypte.
16:33	Moshè dit à Aaron : Prends un vase et mets-y un plein d'omer de manne, et pose-le devant YHWH, afin qu'il soit préservé pour vos générations.
16:34	Comme YHWH l'avait ordonné à Moshè, Aaron le posa devant le témoignage pour qu'il y soit préservé.
16:35	Les enfants d'Israël mangèrent la manne durant 40 ans, jusqu'à leur arrivée dans un pays habité. Ils mangèrent la manne, jusqu'à leur arrivée aux frontières du pays de Canaan.
16:36	Or un omer est la dixième partie d'un épha.

## Chapitre 17

### Miracle de l'eau qui sort du rocher

17:1	Toute l'assemblée des enfants d'Israël partit du désert de Sin, selon les marches que YHWH leur avait ordonnées. Ils campèrent à Rephidim, où il n'y avait pas d'eau à boire pour le peuple.
17:2	Le peuple contesta avec Moshè. Et ils lui dirent : Donnez-nous de l'eau à boire. Et Moshè leur dit : Pourquoi contestez-vous avec moi ? Pourquoi tentez-vous YHWH<!--No. 20:2-5.--> ?
17:3	Le peuple eut donc soif dans ce lieu, faute d'eau, et le peuple murmura contre Moshè. Il disait : Pourquoi nous as-tu fait monter hors d'Égypte, pour nous faire mourir de soif, nous, nos enfants et nos troupeaux ?
17:4	Moshè cria à YHWH, en disant : Que ferai-je à ce peuple ? Encore un peu et ils me lapideront.
17:5	YHWH répondit à Moshè : Passe devant le peuple et prends avec toi des anciens d'Israël, prends aussi dans ta main la verge avec laquelle tu as frappé le fleuve, et viens !
17:6	Voici, je vais me tenir là devant toi sur le rocher d'Horeb. Tu frapperas le rocher, il en sortira des eaux et le peuple en boira. Moshè donc fit ainsi aux yeux des anciens d'Israël<!--De. 9:8 ; Ps. 78:15 ; 1 Co. 10:4.-->.
17:7	Il appela ce lieu du nom de Massa et Meriba, à cause de la querelle des enfants d'Israël, et parce qu'ils avaient tenté YHWH, en disant : YHWH est-il au milieu de nous ou non ?

### Bataille et victoire contre Amalek

17:8	Alors Amalek vint et livra bataille contre Israël à Rephidim<!--De. 25:17-18.-->.
17:9	Moshè dit à Yéhoshoua : Choisis-nous des hommes et sors pour combattre contre Amalek. Je me tiendrai demain sur le sommet de la colline, et la verge d'Elohîm sera dans ma main.
17:10	Yéhoshoua fit comme Moshè lui avait ordonné en combattant contre Amalek. Mais Moshè, Aaron et Hour montèrent au sommet de la colline.
17:11	Et il arrivait que lorsque Moshè élevait sa main, Israël était alors le plus fort, mais quand il reposait sa main, alors Amalek était le plus fort.
17:12	Les mains de Moshè étant devenues pesantes, ils prirent une pierre et la mirent sous lui, et il s'assit dessus. Aaron et Hour soutenaient ses mains, l'un d'un côté, et l'autre de l'autre côté. Ainsi, ses mains furent fermes jusqu'au coucher du soleil.
17:13	Yéhoshoua donc défit Amalek et son peuple au tranchant de l'épée.
17:14	YHWH dit à Moshè : Écris cela dans un livre pour en garder le souvenir et mets-le aux oreilles de Yéhoshoua, car j'effacerai, j'effacerai la mémoire d'Amalek de dessous les cieux.
17:15	Moshè bâtit un autel et l'appela du nom de : YHWH-Nissi<!--« YHWH est ma bannière ». C'est le nom donné par Moshè à l'autel qu'il construisit pour célébrer la défaite d'Amalek (Ex. 17:15). En No. 21:8-9, Moshè éleva une bannière sur laquelle il avait fixé un serpent en cuivre pour la guérison des malades.-->.
17:16	Il dit aussi : Parce qu'une main s'est levée contre le trône de Yah, YHWH est en guerre contre Amalek de génération en génération.

## Chapitre 18

### Yithro (Jéthro) conseille Moshè

18:1	Or Yithro, prêtre de Madian, beau-père de Moshè, apprit toutes les choses que YHWH avait faites pour Moshè et pour Israël, son peuple : comment YHWH avait fait sortir Israël de l'Égypte.
18:2	Yithro, beau-père de Moshè, prit Tsipporah la femme de Moshè, après que Moshè l'eut renvoyée,
18:3	et les deux fils de cette femme, dont l'un s'appelait Guershom, car il avait dit : Je suis un étranger sur une terre étrangère,
18:4	et l'autre Éliézer, car il avait dit : L'Elohîm de mon père m'a secouru et m'a délivré de l'épée de pharaon.
18:5	Yithro donc, beau-père de Moshè, vint vers Moshè avec ses fils et sa femme dans le désert, où il était campé, à la montagne d'Elohîm.
18:6	Il fit dire à Moshè : Yithro ton beau-père, vient vers toi, avec ta femme et ses deux fils avec elle.
18:7	Moshè sortit à la rencontre de son beau-père. Il se prosterna et l'embrassa. Ils s'informèrent mutuellement<!--Littéralement « l'homme et son ami ».--> de leur santé, puis ils entrèrent dans la tente.
18:8	Moshè raconta à son beau-père toutes les choses que YHWH avait faites à pharaon et aux Égyptiens en faveur d'Israël, et toute la détresse qui les avait atteints en chemin, et comment YHWH les avait délivrés.
18:9	Yithro se réjouit de tout le bien que YHWH avait fait à Israël, parce qu'il les avait délivrés de la main des Égyptiens.
18:10	Puis Yithro dit : Béni soit YHWH qui vous a délivrés de la main des Égyptiens et de la main de pharaon, qui a délivré le peuple de la main des Égyptiens !
18:11	Je sais maintenant que YHWH est plus grand que tous les elohîm, car dans cette affaire où ils ont agi avec orgueil, il a eu le dessus sur eux.
18:12	Yithro, beau-père de Moshè, apporta aussi un holocauste et des sacrifices pour les offrir à Elohîm. Et Aaron et tous les anciens d'Israël vinrent pour manger du pain avec le beau-père de Moshè, dans la présence d'Elohîm.
18:13	Et il arriva, le lendemain, comme Moshè siégeait pour juger le peuple, et que le peuple se tenait devant Moshè depuis le matin jusqu'au soir,
18:14	que le beau-père de Moshè vit tout ce qu'il faisait au peuple, et il lui dit : Qu'est-ce que tu fais à l'égard de ce peuple ? Pourquoi es-tu assis seul, et tout le peuple se tient devant toi depuis le matin jusqu'au soir ?
18:15	Et Moshè répondit à son beau-père : C'est que le peuple vient à moi pour consulter Elohîm.
18:16	Quand ils ont quelque cas, ils viennent vers moi. Je juge l'homme et son compagnon et je leur fais connaître les ordonnances d'Elohîm et sa torah.
18:17	Mais le beau-père de Moshè lui dit : La chose que tu fais n'est pas bonne.
18:18	Tu te faneras, tu te faneras, toi et ce peuple qui est avec toi. En effet, la chose est trop lourde pour toi, tu ne pourras l'accomplir tout seul.
18:19	Écoute donc ma voix ! Je te donne un conseil et qu'Elohîm soit avec toi ! Sois pour ce peuple devant Elohîm et introduis toi-même leurs cas auprès d'Elohîm.
18:20	Enseigne-leur les ordonnances et la torah, fais-leur connaître la voie par laquelle ils auront à marcher et ce qu'ils auront à faire.
18:21	Et regarde parmi tout le peuple des hommes talentueux, craignant Elohîm, des hommes de vérité, haïssant le gain injuste. Établis-les chefs de milliers, chefs de centaines, chefs de cinquantaines et chefs de dizaines.
18:22	Et qu'ils jugent le peuple en tout temps, mais qu'ils te présentent tous les cas importants et qu'ils jugent eux-mêmes tous les cas sans importance. Allège-toi et qu'ils les portent avec toi.
18:23	Si tu fais cela, et qu'Elohîm te l'ordonne, tu pourras subsister, et tout le peuple parviendra en paix à destination.
18:24	Moshè donc obéit à la parole de son beau-père et fit tout ce qu'il lui avait dit.
18:25	Ainsi, Moshè choisit parmi tout Israël des hommes talentueux et les établit chefs sur le peuple, chefs de milliers, chefs de centaines, chefs de cinquantaines et chefs de dizaines.
18:26	Et ils jugeaient le peuple en tout temps. Ils présentaient à Moshè les cas difficiles et ils jugeaient eux-mêmes tous les cas sans importance.
18:27	Puis Moshè laissa partir son beau-père, qui s'en alla dans son pays.

## Chapitre 19

### DÉBUT DE LA PÉRIODE DE LA LOI MOSAÏQUE OU DE LA PREMIÈRE ALLIANCE

19:1	Au premier jour du troisième mois, après que les enfants d'Israël furent sortis du pays d'Égypte, en ce même jour-là, ils vinrent dans le désert de Sinaï.
19:2	Étant donc partis de Rephidim, ils vinrent dans le désert de Sinaï et campèrent dans le désert. Et Israël campa vis-à-vis de la montagne.
19:3	Et Moshè monta vers Elohîm, car YHWH l'avait appelé de la montagne pour lui dire : Tu parleras ainsi à la maison de Yaacov et tu annonceras ceci aux enfants d'Israël :
19:4	Vous avez vu ce que j'ai fait aux Égyptiens et comment je vous ai portés sur des ailes d'aigle et vous ai amenés à moi.
19:5	Maintenant donc, si vous obéissez, si vous obéissez à ma voix et si vous gardez mon alliance, alors vous serez ma propriété parmi tous les peuples, car toute la Terre est à moi<!--C'est ici que la période de la loi ou Première Alliance est promulguée. Le fait d'avoir réuni les textes de Bereshit (Genèse) à Malakhi (Malachie) sous l'appellation « Ancien Testament » a induit beaucoup de personnes en erreur quant à leur compréhension du plan d'Elohîm pour nos vies. Tout d'abord, l'emploi du mot « testament » est inapproprié puisqu'on ne peut parler de testament sans qu'il n'y ait eu au préalable la mort du testateur (Hé. 9:16-17). Certes, des animaux étaient tués sous la torah pour couvrir les péchés. Toutefois, ces sacrifices étaient imparfaits et par conséquent prévus pour ne durer qu'un temps, en attendant le sacrifice parfait de Yéhoshoua ha Mashiah (Jésus-Christ) (Hé. 10:1-14). De plus, il est évident que les animaux sacrifiés ne nous ont rien légué.\\ Ensuite, il est à noter que tous les textes classés dans ce que l'on appelle à tort « Ancien Testament » ne se rapportent pas exclusivement et nécessairement à la loi. Ainsi, des prophètes, en commençant par Moshè en personne, ayant vécu sous la torah, ont prophétisé et écrit sur d'autres sujets que la loi, notamment sur la grâce et la fin des temps. N'oublions pas non plus que Yéhoshoua ha Mashiah est né et a vécu sous la torah (Ga. 4:4). En tant que Juif, il l'a scrupuleusement respectée de telle sorte qu'elle fut totalement accomplie en lui (Mt. 5:17-18 ; Jn. 19:30). En conséquence, la fin de la loi mosaïque eut lieu après la mort du Seigneur, précisément au moment où le Seigneur a dit « Tout est accompli », et lorsque le voile du temple s'est déchiré de haut en bas (Mt. 27:50-51 ; Jn. 19:30). La Nouvelle Alliance ou le Testament de Yéhoshoua débuta avec l'effusion de l'Esprit (Ac. 2). De la mort du Seigneur à la pentecôte, une période de transition de 50 jours s'est écoulée. Yéhoshoua ha Mashiah s'est rendu pendant ce temps dans le sanctuaire céleste pour présenter son sang dans le Saint des saints. Une fois son sacrifice examiné et accepté, le Saint-Esprit qui avait été retiré de l'être humain (Ge. 6:3) put de nouveau revenir habiter le cœur des croyants.\\Mais qu'est-ce que la loi exactement ? Beaucoup de chrétiens sont dans la confusion à ce sujet. En réalité, il n'y avait pas qu'une loi mais trois sortes de lois : Les lois morales et les lois cérémonielles qui préexistaient depuis l'éternité ; et les lois civiles qui ont débuté avec Moshè car elles ne concernaient que son peuple.\\- Les lois civiles régissaient le fonctionnement de la vie en communauté des Hébreux. Elles étaient exclusivement réservées au peuple d'Israël dans le camp puis dans le pays de Canaan (Ex. 21:1-2 ; De. 23).\\- Les lois morales font référence à la nature d'Elohîm : Son amour, sa justice, sa sainteté, etc. Les dix commandements, à l'exception du shabbat tel que prescrit par Moshè (Ex. 16:28-29 ; Lé. 15:32), font partie des lois morales (Ex. 20:1-17). Les dix paroles ne constituent qu'une base, un résumé. Ainsi, d'autres règles morales sont énoncées tout au long des Écritures notamment sur la sexualité (Lé. 18:1-22), l'interdiction des sacrifices humains et de l'occultisme (De. 18:10-13), le respect d'autrui et l'entraide (Lé. 19:10-18, 29-36). Comme il est impossible de consigner dans un livre tous les péchés moraux, le Seigneur a inscrit les lois morales dans le cœur de l'être humain afin qu'il sache instinctivement faire la différence entre le bien et mal (Ro. 2:14-15). Yéhoshoua les a résumées en ces quelques mots : « Tu aimeras le Seigneur ton Elohîm, de tout ton cœur, de toute ton âme et de toute ta pensée. C'est là le premier et le grand commandement. Et voici le deuxième qui lui est semblable : Tu aimeras ton prochain comme toi-même. » (Mt. 22:37-39). Ces lois sont encore en vigueur aujourd'hui et le resteront pour toujours.\\- Les lois cérémonielles étaient relatives au culte et au sanctuaire terrestre, c'est-à-dire le tabernacle puis le temple de Yeroushalaim (Jérusalem) (Hé. 9:1-10). Elles regroupent toutes les ordonnances concernant les sacrifices, les ablutions, les shabbats, les fêtes de YHWH, la dîme des Lévites et des prêtres (voir commentaire en No. 18:21 et Mal. 3:10). Les livres de Lévitique (Vayiqra) et de Nombres (Bamidbar) exposent en détail toutes les ordonnances reçues par Moshè d'après le modèle céleste que YHWH lui avait montré sur le Mont Sinaï (Ex. 26:30). Les lois cérémonielles préexistaient donc depuis l'éternité.\\Les lois cérémonielles représentent la Première Alliance qui avait pour fondement la loi morale. Or cette Alliance a vieilli puis disparu, car elle n'était que l'ombre des choses à venir (Hé. 8:13). En effet, elle était basée sur quatre points principaux : le temple, le culte centralisé, le sacrifice et les prêtres. En Mashiah, nous n'avons plus besoin d'un temple physique puisque nous sommes devenus les temples vivants de YHWH (1 Co. 6:19 ; Ep. 2:22). Nous pouvons désormais adorer le Seigneur en Esprit et en vérité, à tout moment et en tout lieu (Jn. 4:23). La prêtrise lévitique ayant été abolie, chaque enfant d'Elohîm est devenu un prêtre (Ap. 5:10) qui offre en sacrifice sa propre vie consacrée au Seigneur (Ro. 12:1).\\Les lois cérémonielles ont donc trouvé leur parfait accomplissement en Yéhoshoua ha Mashiah : tous les sacrifices sanglants le préfiguraient, toutes les solennités ont été réalisées en Lui (voir note en Lé. 23). Le Mashiah est donc la fin de la loi, non pas morale, mais cérémonielle (Ro. 10:4).\\Un lien étroit existe entre les lois morales et les lois cérémonielles. La loi morale est comme un diagnostic qui révèle une pathologie incurable comme le sida : Le péché (Ro. 5:13-20, 7:7-14). En la découvrant, l'homme se sent condamné, car il réalise qu'il ne peut pas répondre aux exigences de la justice divine. La loi cérémonielle (le sang des animaux – Hé. 9:1-13, 10:11) a donné aux humains une sorte de trithérapie pour les soulager provisoirement de leurs péchés mais sans pour autant les ôter (guérir, délivrer, nettoyer, laver) définitivement. Seul le sang de la Nouvelle Alliance, c'est-à-dire le sang de Yéhoshoua ha Mashiah, a pu nous délivrer une fois pour toutes (Jn. 1:29 ; Hé. 9:11-26, 10:1-23 ; Ap. 1:6).-->.
19:6	Et vous serez pour moi, un royaume de prêtres et une nation sainte. Voilà les discours que tu tiendras aux enfants d'Israël.
19:7	Puis Moshè vint et appela les anciens du peuple et mit devant eux toutes ces paroles que YHWH lui avait ordonnées.
19:8	Et tout le peuple répondit d'un commun accord, en disant : Nous ferons tout ce que YHWH a dit. Et Moshè rapporta à YHWH toutes les paroles du peuple.

### Moshè doit sanctifier le peuple pour qu'il rencontre YHWH

19:9	Et YHWH dit à Moshè : Voici, je viendrai à toi dans une sombre nuée afin que le peuple entende quand je parlerai avec toi et qu'ils aient foi en toi aussi pour toujours. Alors Moshè rapporta à YHWH les paroles du peuple.
19:10	YHWH dit aussi à Moshè : Va-t'en vers le peuple et sanctifie-le aujourd'hui et demain, et qu'ils lavent leurs vêtements.
19:11	Et qu'ils soient tous prêts pour le troisième jour, car au troisième jour, YHWH descendra sur la montagne de Sinaï, à la vue de tout le peuple.
19:12	Tu fixeras au peuple des limites tout autour et tu diras : Gardez-vous de monter sur la montagne et de toucher aucune de ses extrémités. Quiconque touchera la montagne, mourra, il mourra.
19:13	On ne mettra pas la main sur lui, mais il sera lapidé, il sera lapidé ou percé, percé de flèches. Soit bête, soit homme, il ne vivra pas. Quand la corne<!--Ce mot vient de l'hébreu « yowbel » qui signifie « bélier », « corne de bélier » ou « le Jubilé » (voir Lé. 25:10-15,28,30-33,40,50-52,54 ; Lé. 27:17-24 ; No. 27:24, 36:4 ; Jos. 6:4-13).--> de bélier sonnera, ils monteront vers la montagne.
19:14	Et Moshè descendit de la montagne vers le peuple et sanctifia le peuple, et ils lavèrent leurs vêtements.
19:15	Et il dit au peuple : Soyez tous prêts pour le troisième jour et ne vous approchez pas de vos femmes.
19:16	Et le troisième jour au matin, il y eut des tonnerres, et des éclairs, et une grosse nuée sur la montagne, avec un très fort son de shofar, et tout le peuple dans le camp fut effrayé.
19:17	Alors Moshè fit sortir le peuple du camp pour aller au-devant d'Elohîm, et ils s'arrêtèrent au pied de la montagne.
19:18	Or le mont Sinaï était tout couvert de fumée, parce que YHWH y était descendu en feu, et sa fumée montait comme la fumée d'une fournaise. Toute la montagne tremblait beaucoup.
19:19	Et comme le son du shofar se renforçait de plus en plus, Moshè parlait, et Elohîm lui répondait par une voix.
19:20	Ainsi YHWH descendit sur la montagne de Sinaï, au sommet de la montagne. YHWH appela Moshè au sommet de la montagne et Moshè y monta.
19:21	YHWH dit à Moshè : Descends et avertis le peuple de ne pas se précipiter vers YHWH afin de regarder, de peur qu'un grand nombre d'entre eux ne périsse.
19:22	Et que les prêtres qui s'approchent de YHWH se sanctifient aussi, de peur que YHWH ne les brise.
19:23	Et Moshè dit à YHWH : Le peuple ne pourra pas monter sur la montagne de Sinaï, car tu nous as toi–même avertis en disant : Fixe des limites autour de la montagne et sanctifie-la.
19:24	Et YHWH lui dit : Va, descends et tu monteras, toi et Aaron avec toi. Quant aux prêtres et au peuple, qu’ils ne se précipitent pas pour monter vers YHWH, de peur qu’il ne les brise.
19:25	Moshè descendit donc vers le peuple et lui dit ces choses.

## Chapitre 20

### Les dix paroles

20:1	Alors Elohîm prononça toutes ces paroles, en disant :
20:2	Je suis YHWH ton Elohîm, qui t'ai fait sortir du pays d'Égypte, de la maison des esclaves.
20:3	Tu n'auras pas d'autres elohîm devant ma face.
20:4	Tu ne te feras pas d'image taillée, ni aucune ressemblance des choses qui sont là-haut aux cieux, ni ici-bas sur la Terre, ni dans les eaux sous la terre<!--Lé. 26:1.-->.
20:5	Tu ne te prosterneras pas devant elles et tu ne les serviras pas, car je suis YHWH ton Elohîm, le El jaloux, qui punis l'iniquité des pères sur les fils, jusqu'à la troisième et à la quatrième génération<!--Une génération biblique est de 40 ans au minimum (No. 32:13 ; Ps. 95:10 ; 1 Ch. 29:26-28 ; Ac. 7:23,30,36,42 ; Hé. 3:9-11).--> de ceux qui me haïssent,
20:6	et qui fais miséricorde jusqu'à 1 000 générations à ceux qui m'aiment et qui gardent mes commandements.
20:7	Tu ne prendras pas le Nom de YHWH ton Elohîm en vain, car YHWH ne tiendra pas pour innocent celui qui aura pris son Nom en vain<!--Lé. 19:12 ; Mt. 5:33.-->.
20:8	Souviens-toi du jour du repos pour le sanctifier.
20:9	Tu travailleras 6 jours, et tu feras toute ton œuvre.
20:10	Mais le septième jour est le repos de YHWH ton Elohîm. Tu ne feras aucune œuvre en ce jour-là, ni toi, ni ton fils, ni ta fille, ni ton serviteur, ni ta servante, ni ton bétail, ni l'étranger qui est dans tes portes.
20:11	Car en 6 jours YHWH a fait les cieux, la Terre, la mer et tout ce qui est en eux, et s'est reposé le septième jour. C'est pourquoi YHWH a béni le jour du repos et l'a sanctifié<!--Ge. 2:3 ; Ex. 31:14 ; Ez. 20:12.-->.
20:12	Honore ton père et ta mère, afin que tes jours soient prolongés sur la terre que YHWH ton Elohîm te donne<!--Lé. 19:3 ; De. 5:16 ; Mt. 15:4 ; Ep. 6:2.-->.
20:13	Tu n'assassineras pas<!--Mt. 5:21.-->.
20:14	Tu ne commettras pas d'adultère<!--Lé. 20:10 ; De. 5:18 ; Pr. 6:32 ; Mt. 5:32 ; Ro. 7:3.-->.
20:15	Tu ne voleras pas.
20:16	Tu ne diras pas de faux témoignage contre ton prochain.
20:17	Tu ne convoiteras pas la maison de ton prochain. Tu ne convoiteras pas la femme de ton prochain, ni son serviteur, ni sa servante, ni son bœuf, ni son âne, ni aucune chose qui soit à ton prochain.

### Le peuple tout tremblant devant YHWH

20:18	Or tout le peuple apercevait les tonnerres, les éclairs, le son du shofar et la montagne fumante. Et le peuple voyant cela tremblait et se tenait loin.
20:19	Et ils dirent à Moshè : Parle, toi avec nous, et nous écouterons ; mais qu'Elohîm ne parle pas avec nous, de peur que nous ne mourions<!--De. 5:23-24 ; Hé. 12:18-19.-->.
20:20	Et Moshè dit au peuple : N'ayez pas peur ! Car Elohîm est venu pour vous éprouver, afin que sa crainte soit devant vous et que vous ne péchiez pas.
20:21	Le peuple donc se tint loin, mais Moshè s'approcha des ténèbres épaisses où était Elohîm.
20:22	Et YHWH dit à Moshè : Tu diras ainsi aux enfants d'Israël : Vous avez vu que je vous ai parlé depuis les cieux.
20:23	Vous ne ferez pas à côté de moi des elohîm en argent ni des elohîm en or. Vous n'en ferez pas pour vous.
20:24	Tu me feras un autel de terre, sur lequel tu sacrifieras tes holocaustes et tes offrandes de paix<!--Voir commentaire en Lé. 3:1.-->, ton menu et ton gros bétail. En quelque lieu que ce soit où je rappellerai mon Nom, je viendrai à toi et je te bénirai.
20:25	Si tu me fais un autel de pierres, tu ne le bâtiras pas en pierres taillées, car en passant ton outil sur la pierre, tu la profanerais.
20:26	Et tu ne monteras pas à mon autel par des marches, de peur que ta nudité ne soit découverte en y montant.

## Chapitre 21

### Torah sur les maîtres et leurs esclaves

21:1	Voici les ordonnances que tu mettras en face d'eux.
21:2	Si tu achètes un esclave hébreu, il te servira six années, mais la septième, il sortira libre, gratuitement<!--Lé. 25:39-43 ; De. 15:12 ; Jé. 34:14.-->.
21:3	S'il est venu seul, il sortira seul. S'il était le mari d'une femme, sa femme sortira aussi avec lui.
21:4	Si son seigneur lui a donné une femme qui lui ait enfanté des fils ou des filles, sa femme et les enfants qu'il aura seront à son seigneur, et il sortira seul.
21:5	Si l'esclave dit, s'il dit : J'aime mon seigneur, ma femme et mes fils, je ne veux pas sortir libre,
21:6	alors son seigneur le fera venir devant les juges et le fera approcher de la porte ou du poteau, et son seigneur lui percera l'oreille avec un poinçon et il le servira pour toujours.
21:7	Si quelqu'un vend sa fille pour être esclave, elle ne sortira pas comme les esclaves sortent.
21:8	Si elle déplaît aux yeux de son seigneur qui ne l'aura pas fiancée, il la fera racheter. Mais il n'aura pas le pouvoir de la vendre à un peuple étranger, après lui avoir été infidèle.
21:9	Mais s'il l'a fiancée à son fils, il fera pour elle selon le droit des filles.
21:10	S'il en prend une autre pour lui, il ne diminuera en rien la nourriture, les vêtements et le droit conjugal.
21:11	S'il ne fait pas pour elle ces trois choses-là, elle sortira gratuitement, sans argent.

### Torah sur les dommages corporels

21:12	Si quelqu'un frappe un homme et qu'il en meure, il mourra, il mourra<!--Lé. 24:17 ; No. 35:11-16 ; De. 19:2-11 ; Jos. 20:2.-->.
21:13	S'il ne lui a pas dressé d'embûches, mais qu'Elohîm l'ait fait tomber entre ses mains, je t'établirai un lieu où il s'enfuira.
21:14	Mais si un homme agit avec arrogance contre son prochain pour le tuer par ruse, tu l'arracheras de mon autel, afin qu'il meure.
21:15	Celui qui aura frappé son père ou sa mère, il mourra, il mourra<!--Lé. 20:9 ; De. 27:16 ; Mt. 15:4.-->.
21:16	Si quelqu'un enlève un homme et le vend, ou s'il est retrouvé entre ses mains, il mourra, il mourra.
21:17	Celui qui aura maudit son père ou sa mère, il mourra, il mourra.
21:18	Si des hommes ont une querelle, et que l'un d'eux frappe l'autre avec une pierre ou avec le poing, sans causer sa mort, mais qu'il soit obligé de se mettre au lit,
21:19	s'il se lève et marche dehors en s'appuyant sur son bâton, celui qui l'aura frappé sera absous. Toutefois, il le dédommagera de ce qu'il a chômé et il le guérira, il le guérira.
21:20	Si quelqu'un a frappé du bâton son serviteur ou sa servante, et qu'il soit mort sous sa main, on le vengera, on le vengera.
21:21	Mais s'il survit un jour ou deux, il ne sera pas vengé, car c'est son argent.
21:22	Si des hommes se querellent, et que l'un d'eux frappe une femme enceinte, et qu'elle en accouche, s'il n'y a pas cas de mort, il sera condamné, condamné à l'amende que le mari de la femme lui imposera, et il la donnera selon l'estimation des juges.
21:23	Mais si malheur arrive, tu donneras vie pour vie,
21:24	œil pour œil, dent pour dent, main pour main, pied pour pied<!--Lé. 24:20 ; De. 19:21 ; Mt. 5:38.-->,
21:25	brûlure pour brûlure, plaie pour plaie, meurtrissure pour meurtrissure.
21:26	Si un homme frappe l'œil de son esclave ou de sa servante et lui fait perdre son œil, il le renverra libre, pour son œil.
21:27	Et s'il fait tomber une dent à son serviteur ou à sa servante, il le laissera aller libre pour sa dent.
21:28	Si un bœuf heurte de sa corne un homme ou une femme, et que la personne en meure, le bœuf sera lapidé, il sera lapidé et l'on ne mangera pas sa chair, mais le maître du bœuf sera jugé innocent.
21:29	Si le bœuf était d'hier et d'avant-hier sujet à frapper de sa corne, et que son maître en ait été averti avec protestation, et qu'il ne l'ait pas surveillé, s'il tue un homme ou une femme, le bœuf sera lapidé et on fera aussi mourir son maître.
21:30	Si on lui impose un prix pour se racheter, il donnera la rançon de sa vie, selon tout ce qui lui sera imposé.
21:31	Si le bœuf heurte de sa corne un fils ou une fille, on le traitera selon cette ordonnance.
21:32	Si le bœuf heurte de sa corne un esclave, soit homme, soit femme, on donnera 30 sicles d'argent au maître de l'esclave, et le bœuf sera lapidé.
21:33	Si quelqu'un découvre une fosse ou si quelqu'un creuse une fosse, et ne la couvre pas, et qu'il y tombe un bœuf ou un âne,
21:34	le maître de la fosse donnera satisfaction et rendra l'argent au maître du bœuf, mais la bête morte lui appartiendra.
21:35	Et si le bœuf d'un homme blesse le bœuf d'une autre personne et qu'il en meure, ils vendront le bœuf vivant et en partageront l'argent par moitié, ils partageront aussi par moitié le bœuf mort.
21:36	Mais s'il est connu que le bœuf avait d'hier et d'avant-hier l'habitude de heurter avec sa corne et que le maître ne l'ait pas gardé, il rendra, il rendra bœuf pour bœuf et le bœuf mort sera pour lui.

## Chapitre 22

### Torah sur les torts causés à autrui

22:1	Lorsqu'un homme vole un bœuf, ou un agneau, s'il le tue ou le vend, il restituera 5 bœufs pour le bœuf, et 4 brebis pour l'agneau.
22:2	Si le voleur est surpris volant avec effraction et qu'il soit frappé de sorte qu'il en meure, celui qui l'aura frappé ne sera pas coupable de son sang.
22:3	Mais si le soleil est levé, on sera coupable de son sang. Il fera une restitution, il fera une restitution. S'il n'a rien, il sera vendu pour son vol.
22:4	Si ce qui a été dérobé est trouvé vivant entre ses mains, soit bœuf, soit âne, soit brebis ou chèvre, il rendra le double.
22:5	Si un homme fait brouter dans un champ ou dans une vigne, en lâchant son bétail qui aille paître dans le champ d'autrui, il rendra le meilleur de son champ et le meilleur de sa vigne.
22:6	Lorsqu'un feu sort et rencontre des épines, s'il dévore du blé en gerbes ou sur pied, ou le champ, celui qui aura allumé le feu restituera, il restituera ce qui aura été brûlé.
22:7	Lorsqu'un homme donne à son prochain de l'argent ou des vases à garder, et qu'on les vole dans la maison de cet homme, et si l'on découvre le voleur, il restituera le double<!--Lé. 5:20-26.-->.
22:8	Mais si on ne découvre pas le voleur, on fera venir le maître de la maison devant les juges, pour jurer s'il n'a pas mis sa main sur le bien de son prochain.
22:9	Dans toute affaire d'infidélité concernant un bœuf, un âne, une brebis, une chèvre, un vêtement ou tout objet perdu, dont quelqu'un dira lui appartenir, la cause des deux parties viendra devant Elohîm et celui qu'Elohîm aura condamné, restituera le double à son prochain.
22:10	Lorsqu'un homme donne à garder à son prochain un âne, un bœuf, un agneau ou une autre bête, et que la bête meure, s’estropie ou soit emmenée sans que personne ne l'ait vu,
22:11	le serment de YHWH interviendra entre les deux parties<!--Hé. 6:16.-->, pour savoir s'il n'a pas mis sa main sur le bien de son prochain. Le maître de la bête se contentera du serment, et l'autre ne la restituera pas.
22:12	Mais si elle lui a été dérobée, si elle lui a été dérobée, il la restituera à son maître.
22:13	Si elle a été déchirée, déchirée par les bêtes sauvages, il la produira en témoignage et il ne restituera pas ce qui a été déchiré.
22:14	Lorsqu'un homme emprunte à son prochain une bête et qu'elle se casse un membre ou qu'elle meure en l'absence de son maître, il la restituera, il la restituera.
22:15	Si son maître est avec elle, il n'y aura pas de restitution. Si elle a été louée, cela devra rentrer dans son prix de louage.

### Diverses torahs

22:16	Si un homme séduit une vierge qui n'est pas fiancée et couche avec elle, il paiera sa dot. Il paiera sa dot et la prendra pour femme<!--De. 22:28.-->.
22:17	Mais si le père de la fille refuse, s'il refuse de la lui donner, il lui paiera en argent la valeur de la dot des vierges.
22:18	Tu ne laisseras pas vivre la sorcière<!--De. 18:10-11 ; Lé. 20:27.-->.
22:19	Celui qui couche avec une bête, mourra, il mourra<!--Lé. 18:23, 20:15 ; De. 27:21.-->.
22:20	Celui qui sacrifie à d'autres elohîm qu'à YHWH seul, sera dévoué par interdit<!--Lé. 17:7 ; De. 13:6-16, 17:2-5.-->.
22:21	Tu ne fouleras ni n'opprimeras l'étranger, car vous avez été étrangers au pays d'Égypte<!--Lé. 19:34.-->.
22:22	Vous n'affligerez pas la veuve ni l'orphelin<!--De. 24:17-18 ; Za. 7:10.-->.
22:23	Si vous les affligez en quoi que ce soit, et qu'ils crient, qu'ils crient à moi, j'entendrai, j'entendrai leur cri.
22:24	Ma colère s'embrasera et je vous ferai mourir par l'épée. Vos femmes seront veuves et vos fils orphelins.
22:25	Si tu prêtes de l'argent à mon peuple, au pauvre qui est avec toi, tu ne te comporteras pas avec lui en créancier, vous ne lui exigerez pas d'intérêt.
22:26	Si tu prends en gage, si tu prends en gage le vêtement de ton ami, tu le lui rendras avant que le soleil soit couché<!--De. 24:10-13.-->.
22:27	Car c'est sa seule couverture, c'est son vêtement pour couvrir sa peau. Dans quoi coucherait-il ? S'il arrive donc qu'il crie à moi, je l'entendrai, car je suis miséricordieux.
22:28	Tu ne maudiras pas les juges et tu ne maudiras pas le prince de ton peuple<!--Lé. 24:15-16 ; Ac. 23:4.-->.
22:29	Tu ne tarderas pas à m'offrir la plénitude de tes jus. Tu me donneras le premier-né de tes fils<!--Ex. 13:12-15 ; De. 26:2-11.-->.
22:30	Tu feras la même chose de ta vache, de ta brebis et de ta chèvre. Il sera 7 jours avec sa mère, et le huitième jour tu me le donneras.
22:31	Vous serez des hommes saints pour moi, et vous ne mangerez pas de la chair déchirée dans les champs, mais vous la jetterez aux chiens.

## Chapitre 23

### Diverses torahs (suite)

23:1	Tu ne répandras pas de rumeur mensongère. Tu ne prêteras pas la main au méchant pour être témoin d'une violence<!--Ex. 20:16 ; De. 19:16-21.-->.
23:2	Tu ne suivras pas la multitude pour faire le mal et tu ne témoigneras pas dans un procès en te mettant du côté de la multitude pour pervertir la justice.
23:3	Tu ne favoriseras pas le pauvre dans son procès<!--De. 1:17.-->.
23:4	Si tu rencontres le bœuf de ton ennemi ou son âne égaré, tu le lui ramèneras, tu le lui ramèneras.
23:5	Si tu vois l'âne de celui qui te hait, abattu sous sa charge, tu t'arrêteras pour le secourir et tu l'aideras, tu l'aideras.
23:6	Tu ne pervertiras pas le droit de l'indigent, qui est au milieu de toi, dans son procès.
23:7	Tu t'éloigneras de toute parole fausse et tu ne feras pas mourir l'innocent et le juste, car je ne justifierai pas le méchant.
23:8	Tu n'accepteras pas de pot-de-vin<!--Le mot hébreu signifie aussi « présents ».-->, car les pots-de-vin aveuglent les clairvoyants et pervertissent les paroles des justes.
23:9	Tu n'opprimeras pas l'étranger. Vous-mêmes, vous savez ce qu'éprouve l'étranger car vous avez été étrangers au pays d'Égypte.

### Le shabbat, le repos de la terre

23:10	Pendant 6 ans tu ensemenceras ta terre et en recueilleras le revenu.
23:11	Mais la septième année, tu lui donneras du relâche et tu la mettras en jachère, afin que les pauvres de ton peuple en mangent, et que les bêtes des champs mangent ce qui restera. Tu en feras de même de ta vigne et de tes oliviers.
23:12	Tu travailleras 6 jours, mais tu te reposeras au septième jour, afin que ton bœuf et ton âne se reposent, et que le fils de ta servante et l'étranger reprennent courage.
23:13	Vous prendrez garde à toutes les choses que je vous ai ordonnées. Vous ne ferez pas mention du nom des elohîm étrangers, on ne l'entendra pas de ta bouche<!--Jos. 23:7 ; Ps. 16:4.-->.

### Les fêtes

23:14	Trois fois par année, tu me célébreras une fête<!--Lé. 23:4-44.-->.
23:15	Tu observeras la fête des pains sans levain<!--Ex. 29:2.-->. Tu mangeras des pains sans levain pendant 7 jours, comme je t'ai ordonné, au temps fixé<!--Voir Ge. 1:14.--> du mois des épis<!--Nisan (ou Abib), mois de l'exode et de la Pâque (Mars-Avril). Voir Ex. 13:4 ; De. 16:1.-->, car c'est en ce mois-là que tu es sorti d'Égypte. Personne ne se présentera devant ma face à vide.
23:16	Et la fête de la moisson, celle des prémices de ton travail, de ce que tu auras semé dans les champs, ainsi que la fête de la récolte, après la fin de l'année, quand tu auras recueilli des champs les fruits de ton travail<!--Ex. 34:22.-->.
23:17	Trois fois par année, tous les mâles d'entre vous se présenteront devant le Seigneur YHWH.
23:18	Tu ne sacrifieras pas le sang de mon sacrifice avec du pain levé et la graisse de ma fête ne passera pas la nuit jusqu'au matin<!--Ex. 34:25-26.-->.
23:19	Tu apporteras à la maison de YHWH ton Elohîm, les prémices de tes premiers fruits de la terre. Tu ne feras pas cuire le chevreau dans le lait de sa mère.

### Mises en garde et promesses de YHWH

23:20	Voici, j'envoie un Ange devant toi, afin qu'il te garde dans le chemin et qu'il t'introduise dans le lieu que je t'ai préparé.
23:21	Garde-toi de provoquer sa colère, et écoute sa voix, et ne l'irrite pas, car il ne pardonnera pas votre transgression, car mon Nom est en lui.
23:22	Mais si tu écoutes, si tu écoutes sa voix et si tu fais tout ce que je te dirai, je serai l'ennemi de tes ennemis et j'affligerai ceux qui t'affligeront.
23:23	Car mon Ange marchera devant toi et t'introduira au pays des Amoréens, des Héthiens, des Phéréziens, des Cananéens, des Héviens et des Yebousiens, et je les exterminerai.
23:24	Tu ne te prosterneras pas devant leurs elohîm et tu ne les serviras pas. Tu n'imiteras pas leurs œuvres, mais tu les détruiras, tu les détruiras et tu briseras, tu briseras leurs statues<!--Ex. 20:5, 34:13 ; No. 33:52.-->.
23:25	Vous servirez YHWH votre Elohîm. Et il bénira ton pain et tes eaux et j'ôterai les maladies du milieu de toi<!--Ex. 15:26 ; De. 6:13, 7:15-16 ; Mt. 4:10.-->.
23:26	Il n'y aura dans ton pays ni femme qui avorte, ni femme qui soit stérile. Je remplirai le nombre de tes jours.
23:27	J'enverrai la terreur de mon Nom devant toi et j'effrayerai tout peuple vers lequel tu arriveras, et je ferai tourner le dos devant toi à tous tes ennemis<!--De. 7:23.-->.
23:28	Et j'enverrai des frelons devant toi, qui chasseront les Héviens, les Cananéens et les Héthiens, loin de ta face<!--De. 7:20 ; Jos. 24:12.-->.
23:29	Je ne les chasserai pas loin de ta face en une année, de peur que le pays ne devienne un désert, et que les bêtes des champs ne se multiplient contre toi.
23:30	Mais je les chasserai peu à peu loin de ta face, jusqu'à ce que tu te sois accru et que tu possèdes le pays.
23:31	Et je mettrai des bornes depuis la Mer Rouge jusqu'à la mer des Philistins, et depuis le désert jusqu'au fleuve. Car je livrerai entre tes mains les habitants du pays et je les chasserai de devant toi.
23:32	Tu ne traiteras pas d'alliance avec eux ni avec leurs elohîm.
23:33	Ils n'habiteront pas dans ton pays, de peur qu'ils ne te fassent pécher contre moi, car tu servirais leurs elohîm, et ce serait un piège pour toi.

## Chapitre 24

### La torah lue au peuple ; le sang de l'Alliance

24:1	Puis il dit à Moshè : Monte vers YHWH, toi et Aaron, Nadab et Abihou, et 70 des anciens d'Israël, et vous vous prosternerez de loin.
24:2	Et Moshè s'approchera seul de YHWH, mais eux ne s'en approcheront pas, et le peuple ne montera pas avec lui.
24:3	Alors Moshè vint relater au peuple toutes les paroles de YHWH et toutes ses lois. Tout le peuple répondit d'une voix et dit : Nous ferons toutes les paroles que YHWH a dites.
24:4	Or Moshè écrivit toutes les paroles de YHWH et, s'étant levé de bon matin, il bâtit un autel au bas de la montagne et dressa pour monument, 12 pierres pour les 12 tribus d'Israël.
24:5	Et il envoya des jeunes hommes, enfants d'Israël, qui offrirent des holocaustes et qui sacrifièrent des veaux à YHWH, en sacrifice d'offrande de paix.
24:6	Et Moshè prit la moitié du sang et le mit dans des bassins, et avec l'autre moitié il aspergea l'autel.
24:7	Ensuite, il prit le livre de l'Alliance et le lut, et le peuple qui l'écoutait dit : Nous ferons tout ce que YHWH a dit, et nous obéirons.
24:8	Moshè donc prit le sang, et en aspergea le peuple en disant : Voici le sang de l'Alliance que YHWH a traitée avec vous, selon toutes ces paroles<!--Mt. 26:28 ; Mc. 14:24 ; Lu. 22:20 ; 1 Co. 11:25 ; Hé. 9:20.-->.

### YHWH fait monter Moshè sur la montagne

24:9	Puis Moshè, Aaron, Nadab, Abihou, et les 70 anciens d'Israël montèrent.
24:10	Ils virent l'Elohîm d'Israël. Sous ses pieds, c’était comme un ouvrage de saphir blanc, comme le ciel lui-même dans sa pureté.
24:11	Et il n'étendit pas sa main sur les nobles des fils d'Israël. Ils virent<!--« Voir comme un voyant dans un état extatique ».--> Elohîm, puis ils mangèrent et burent.
24:12	Et YHWH dit à Moshè : Monte vers moi sur la montagne et demeure là. Je te donnerai des tablettes de pierre, la torah et les commandements<!--Vient de l'hébreu « mitsvah ». Il est question du code de la sagesse.--> que j'ai écrits pour les enseigner.
24:13	Alors Moshè se leva avec Yéhoshoua qui était à son service, et Moshè monta sur la montagne d'Elohîm.
24:14	Et il dit aux anciens : Attendez-nous ici jusqu'à ce que nous revenions auprès de vous. Voici, Aaron et Hour sont avec vous. Qui sera maître de paroles s'approchera d'eux.
24:15	Moshè donc monta sur la montagne, et une nuée couvrit la montagne<!--Ex. 19:9-16.-->.
24:16	Et la gloire de YHWH demeura sur la montagne de Sinaï, et la nuée la couvrit pendant 6 jours. Et au septième jour, il appela Moshè du milieu de la nuée.
24:17	Et ce qu'on voyait de la gloire de YHWH au sommet de la montagne, était comme un feu dévorant aux yeux des enfants d'Israël<!--De. 4:24, 9:3 ; Hé. 12:29.-->.
24:18	Et Moshè entra dans la nuée et monta sur la montagne. Moshè fut sur la montagne 40 jours et 40 nuits.

## Chapitre 25

### Des offrandes volontaires pour les matériaux du tabernacle

25:1	Et YHWH parla à Moshè, en disant :
25:2	Parle aux enfants d'Israël et qu'on prenne une contribution pour moi. Vous prendrez cette contribution de tout homme dont le cœur me l'offrira volontairement.
25:3	Et voici la contribution que vous prendrez d'eux : de l'or, de l'argent, du cuivre,
25:4	de l'étoffe violette, de la pourpre rouge, de l'écarlate de cochenille<!--La couleur écarlate de cochenille s'obtient grâce à la femelle cochenille aptère qui contient dans son corps et dans ses œufs un pigment rouge à base d'acide carminique qui permet à l'insecte et à ses larves de se protéger des prédateurs. Au moment de la ponte, cette dernière fixe fermement son corps au tronc d'un arbre puis libère ses œufs qui demeurent ainsi protégés en dessous d'elle jusqu'à leur éclosion. Ensuite, l'insecte meurt en libérant cette substance rouge qui se propage sur tout son corps et sur le bois hôte. C'est ce fluide que l'homme récupère pour en faire un colorant à la couleur caractéristique. Une subtile analogie peut être faite entre la cochenille et le Seigneur qui a versé son sang à la croix pour nous donner la vie. « Et moi, je suis un ver et non un homme, l'opprobre des humains et le méprisé du peuple. » (Ps. 22:7).-->, du fin lin, du poil de chèvre,
25:5	des peaux de béliers teintes en rouge, des peaux de taissons<!--Le mot hébreu employé ici est « tachash », il désigne le matériau servant à fabriquer la couverture extérieure de la tente d'assignation. Si tout le monde s'accorde pour dire qu'il s'agissait d'une fourrure ou d'une peau d'animal, un doute subsiste sur la race exacte de l'animal. On hésite entre le marsouin, le dauphin, le blaireau (taisson) ou peut-être le mouton. Dans de nombreuses bibles, le parti a été pris de traduire par « peaux de dauphins ». Cette hypothèse est cependant très peu probable. D'une part parce que le dauphin n'a pas de fourrure ; d'autre part parce que sa peau n'est absolument pas adaptée à la vie terrestre. Elle est donc impossible à conserver et à transformer, en particulier dans le contexte d'un climat propre au désert. Certains pensent qu'il s'agit tout simplement de peaux de béliers. Dans ce cas, comment expliquer qu'on n'ait pas employé le terme « 'ayil » comme cela est mentionné pour les peaux teintes en rouge ? Il reste donc les peaux de taissons, c'est-à-dire de blaireaux, dont la fourrure est utilisée depuis des siècles. On peut objecter qu'il est impossible que le Seigneur puisse accepter la peau d'un animal impur pour construire son sanctuaire. Si tel était le cas, la peau du dauphin, qui est également un animal impur, n'aurait pas non plus été autorisée. Toutefois, d'un point de vue prophétique, le symbole est important : la présence de cet animal impur préfigurait le Mashiah qui a pris une chair semblable à celle du péché (Ro. 8:3) ; mais aussi le levain que l'on peut trouver dans la pâte nouvelle (1 Co. 5:6-7).-->, du bois d'acacia,
25:6	de l'huile pour le luminaire, des aromates pour l'huile d'onction et pour l'encens aromatique,
25:7	des pierres d'onyx, et d'autres pierres pour la garniture de l'éphod et pour le pectoral.
25:8	Et ils me feront un sanctuaire et j'habiterai au milieu d'eux<!--Ex. 29:45-46.-->.
25:9	Vous ferez tout selon le modèle du tabernacle et le modèle de tous ses ustensiles que je vais te montrer.

### L'arche de l'Alliance

25:10	Ils feront une arche en bois d'acacia. Sa longueur sera de 2 coudées et demie, sa largeur d'une coudée et demie et sa hauteur d'une coudée et demie.
25:11	Et tu la couvriras d'or pur, tu la couvriras à l'intérieur et à l'extérieur, et tu lui feras une bordure d'or tout autour<!--Ex. 37:1-9.-->.
25:12	Et tu fondras pour elle 4 anneaux en or que tu mettras à ses 4 coins, 2 anneaux d'un côté et 2 anneaux de l'autre côté.
25:13	Tu feras aussi des barres de bois d'acacia et tu les couvriras d'or.
25:14	Puis tu feras entrer les barres dans les anneaux aux côtés de l'arche pour porter l'arche avec elles.
25:15	Les barres seront dans les anneaux de l'arche et on ne les en tirera pas.
25:16	Et tu mettras dans l'arche le témoignage que je te donnerai<!--Hé. 9:4.-->.
25:17	Tu feras aussi un propitiatoire d'or pur, dont la longueur sera de 2 coudées et demie, et la largeur d'une coudée et demie.
25:18	Et tu feras 2 chérubins en or, tu les feras d'ouvrage étendu au marteau, aux 2 extrémités du propitiatoire.
25:19	Fais donc un chérubin tiré des extrémités et un chérubin tiré de l'autre extrémité. Vous ferez les chérubins tirés du propitiatoire à ses 2 extrémités.
25:20	Et les chérubins étendront les ailes en haut, couvrant de leurs ailes le propitiatoire, et leurs faces seront vis-à-vis l'une de l'autre. Le regard des chérubins sera vers le propitiatoire<!--1 R. 8:6-7 ; Hé. 9:5.-->.
25:21	Et tu poseras le propitiatoire au-dessus de l'arche, et tu mettras dans l'arche le témoignage que je te donnerai.
25:22	Et je me rencontrerai là avec toi, et je te dirai de dessus le propitiatoire, d'entre les deux chérubins qui seront sur l'arche du témoignage, toutes les choses que je t'ordonnerai pour les enfants d'Israël<!--Ex. 29:42-43 ; No. 7:89.-->.

### La table des pains de proposition

25:23	Tu feras aussi une table de bois d'acacia. Sa longueur sera de 2 coudées, et sa largeur d'une coudée, et sa hauteur d'une coudée et demie.
25:24	Tu la couvriras d'or pur et tu lui feras un couronnement d'or tout autour.
25:25	Tu lui feras aussi à l'entour, une clôture d'une largeur de main, et tout autour de sa clôture tu feras un couronnement d'or.
25:26	Tu lui feras aussi 4 anneaux d'or que tu mettras aux 4 coins qui seront à ses 4 pieds.
25:27	Les anneaux seront à l'endroit de la clôture, afin d'y mettre les barres pour porter la table.
25:28	Tu feras les barres de bois d'acacia et tu les couvriras d'or, et on portera la table avec elles.
25:29	Tu feras aussi ses plats, ses tasses, ses gobelets et ses bassins, avec lesquels on fera les aspersions ; tu les feras d'or pur<!--Ex. 37:10-16.-->.
25:30	Et tu mettras sur cette table le pain de proposition continuellement devant moi<!--Lé. 24:5-9.-->.

### Le chandelier d'or pur

25:31	Tu feras aussi un chandelier en or pur<!--Le chandelier avait une double symbolique. D'une part, il préfigurait Yéhoshoua ha Mashiah (Jésus-Christ), notre Lumière (Jn. 1:4-5, 8:12). Les sept lampes évoquaient l'omniscience de l'Esprit de Yéhoshoua ha Mashiah (Za. 3:9 ; Jn. 16:29-30 ; Ap. 1:4, 3:1, 4:5, 5:6). Il est à noter que ce chandelier comportait des calices en forme de fleurs, de pommes et d'amandes (Ex. 25:33) qui symbolisaient le fruit de l'Esprit que nous devons nécessairement porter (Ga. 5:22). D'autre part, il est une image de l'Assemblée (Église) (Ap. 1:20). Voir commentaire en Ex. 37:17-24 ; Es. 8:13-17.-->. Ce chandelier sera fait en ouvrage martelé ; son pied, sa tige et ses branches, ses coupes, ses boutons et ses fleurs sortiront de lui.
25:32	6 branches sortiront de ses côtés : 3 branches d'un côté du chandelier, et 3 autres de l'autre côté du chandelier.
25:33	Il y aura sur l'une des branches, 3 petites coupes en forme d'amande, un bouton et une fleur. Sur l'autre branche, 3 petits plats en forme d'amande, un bouton et une fleur. Il en sera de même des 6 branches sortant du chandelier.
25:34	Il y aura aussi au chandelier, 4 petites coupes en forme d'amande, ses boutons et ses fleurs :
25:35	il y aura un bouton sous ses 2 branches, un bouton sous les 2 autres branches et un bouton sous les 2 autres branches ; il en sera ainsi pour les 6 branches sortant du chandelier.
25:36	Leurs boutons et leurs branches sortiront de lui. Le tout sera d'une seule pièce en ouvrage martelé, en or pur.
25:37	Tu feras aussi ses 7 lampes, et on les allumera afin qu'elles éclairent vis-à-vis du chandelier.
25:38	Et ses mouchettes et ses encensoirs, destinés à recevoir ce qui tombe des lampes, seront en or pur.
25:39	On le fera avec tous ses ustensiles d'un talent d'or pur.
25:40	Regarde donc et fais selon le modèle que tu as vu sur la montagne.

## Chapitre 26

### Les tapis en fin lin retors

26:1	Tu feras aussi le tabernacle avec 10 tapis en fin lin retors, en étoffe violette, en pourpre rouge et en écarlate de cochenille. Et tu y feras des chérubins, ouvrage d'ingénieur<!--Ex. 36:8-38.-->.
26:2	La longueur d'un tapis sera de 28 coudées, et la largeur du même tapis de 4 coudées. Tous les tapis auront une même mesure.
26:3	5 de ces tapis seront joints l'un à l'autre, et les 5 autres seront aussi joints l'un à l'autre.
26:4	Fais aussi des lacets d'étoffe violette sur le bord d'un tapis, au bord du premier assemblage. Tu feras la même chose au bord du dernier tapis dans l'autre assemblage.
26:5	Tu feras donc 50 lacets au premier tapis, et tu feras 50 lacets au bord du tapis qui est dans le second assemblage. Ces lacets se correspondant les uns aux autres<!--Littéralement : « comme une femme à sa sœur ».-->.
26:6	Tu feras aussi 50 crochets en or, et tu assembleras les tapis l'un à l'autre<!--Littéralement : « une femme à sa sœur ».--> par les crochets. Ainsi le tabernacle ne fera qu'un.

### Les tapis de poils de chèvre

26:7	Tu feras aussi des tapis de poils de chèvre pour servir de tente sur le tabernacle. Tu feras 11 de ces tapis.
26:8	La longueur d'un tapis sera de 30 coudées, et la largeur du même tapis sera de 4 coudées. Les 11 tapis auront une même mesure.
26:9	Puis tu joindras séparément 5 de ces tapis, et les 6 tapis à part, mais tu redoubleras le sixième tapis sur le devant du tabernacle.
26:10	Tu feras aussi 50 lacets sur le bord de l'un des tapis, à l'extrémité de l'assemblage, et 50 lacets au bord du tapis de l'autre assemblage.
26:11	Tu feras aussi 50 crochets en cuivre et tu feras entrer les crochets dans les lacets. Tu assembleras<!--Unir, joindre, lier ensemble, être joint. Ep. 2:21.--> ainsi la tente et elle deviendra une.
26:12	Comme il y aura du surplus dans les tapis de la tente, la moitié du tapis de reste retombera sur le derrière du tabernacle.
26:13	La coudée d'une part, et la coudée d'autre part, qui seront de reste sur la longueur des tapis de la tente, retomberont sur les deux côtés du tabernacle, pour le couvrir.

### La couverture en peaux de béliers et la couverture en peaux de taissons

26:14	Tu feras aussi pour ce tabernacle, une couverture en peaux de béliers teintes en rouge, et une couverture en peaux de taissons par-dessus<!--Ex. 35:7,23, 36:19, 39:34.-->.

### Les planches et leurs bases

26:15	Et tu feras pour le tabernacle, des planches en bois d'acacia, qu'on fera tenir debout<!--Ex. 36:20-34.-->.
26:16	La longueur d'une planche sera de 10 coudées, et la largeur d'une même planche d'une coudée et demie.
26:17	Il y aura à chaque planche 2 tenons joints l'un à l'autre. Tu feras de même pour toutes les planches du tabernacle.
26:18	Tu feras les planches pour le tabernacle, 20 planches en direction du midi, au sud.
26:19	Et au-dessous des 20 planches, tu feras 40 bases en argent, 2 bases sous une planche pour ses 2 tenons, et 2 bases sous l'autre planche pour ses 2 tenons.
26:20	Et 20 planches de l'autre côté du tabernacle, du côté nord.
26:21	Et leurs 40 bases seront en argent, 2 bases sous une planche et 2 bases sous l'autre planche.
26:22	Et pour le fond du tabernacle, vers l'occident, tu feras 6 planches.
26:23	Tu feras aussi deux planches pour les angles du tabernacle, aux 2 côtés du fond.
26:24	Elles seront jointes par le bas, et elles seront jointes ensemble jusqu'à leur sommet par un seul anneau. Il en sera de même des 2 planches qui seront aux 2 angles.
26:25	Il y aura donc 8 planches et 16 bases en argent, 2 bases sous une planche et 2 bases sous une autre planche.
26:26	Après cela, tu feras 5 barres en bois d'acacia pour les planches d'un des côtés du tabernacle.
26:27	Pareillement, tu feras 5 barres pour les planches de l'autre côté du tabernacle et 5 barres pour les planches du côté du tabernacle, pour le fond, vers le côté de l'occident.
26:28	Et la barre du milieu sera au milieu des planches d’extrémité en extrémité.
26:29	Tu couvriras aussi d'or les planches, et tu feras d'or leurs anneaux pour mettre les barres, et tu couvriras d'or les barres.
26:30	Tu dresseras le tabernacle selon le modèle qui t'est montré sur la montagne.

### Les voiles intérieur et extérieur

26:31	Et tu feras un voile<!--Le voile intérieur symbolisait la chair de Yéhoshoua ha Mashiah (Jésus-Christ) qui a été brisée à cause de nos péchés (Es. 53:5 ; Hé. 10:20). Ex. 36:35-38 ; Mt. 27:51 ; Hé. 9:3.--> en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors. On y fera des chérubins, ouvrage d'ingénieur.
26:32	Et tu le mettras sur 4 colonnes en bois d'acacia couverts d'or, ayant leurs crochets en or. Et elles seront sur 4 bases en argent.
26:33	Puis tu mettras le voile sous les crochets, et tu feras entrer là-dedans, c'est-à-dire au-dedans du voile, l'arche du témoignage ; et ce voile vous fera la séparation entre le lieu saint et le Saint des saints.
26:34	Et tu poseras le propitiatoire sur l'arche du témoignage, dans le Saint des saints.
26:35	Et tu mettras la table au dehors de ce voile, et le chandelier vis-à-vis de la table, au côté du tabernacle, vers le sud ; et tu placeras la table côté nord.
26:36	Et à l'entrée du tabernacle, tu feras un rideau en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, en ouvrage de broderie.
26:37	Tu feras aussi pour ce rideau 5 colonnes en bois d'acacia, que tu couvriras d'or. Leurs crochets seront en or et tu fondras pour elles 5 bases en cuivre.

## Chapitre 27

### L'autel de cuivre

27:1	Tu feras aussi un autel en bois d'acacia, ayant 5 coudées de long et 5 coudées de large ; l'autel sera carré, et sa hauteur sera de 3 coudées.
27:2	Tu feras à ses 4 coins des cornes. Ses cornes sortiront de lui et tu le couvriras de cuivre<!--C'est sur l'autel de cuivre que les animaux étaient sacrifiés. Il préfigurait la croix et le jugement que Yéhoshoua ha Mashiah a pris sur lui à notre place (Es. 53:5 ; 2 Co. 13:4 ; Ph. 2:8).-->.
27:3	Tu feras ses chaudrons pour recevoir ses cendres, et ses racloirs, ses bassins, ses fourchettes et ses encensoirs ; tu feras tous ses ustensiles en cuivre.
27:4	Tu lui feras une grille en cuivre en forme de treillis, et tu feras au treillis, 4 anneaux de cuivre à ses 4 coins.
27:5	Et tu le mettras au-dessous de l'enceinte de l'autel en bas, et le treillis s'étendra jusqu'au milieu de l'autel.
27:6	Tu feras aussi des barres pour l'autel, des barres en bois d'acacia, et tu les couvriras de cuivre.
27:7	Et on fera passer ses barres dans les anneaux. Les barres seront aux deux côtés de l'autel pour le porter.
27:8	Tu le feras creux, avec des planches. Ils le feront tel qu'il t'a été montré sur la montagne<!--Ex. 38:1-7.-->.

### Le parvis

27:9	Tu feras aussi le parvis du tabernacle. Du côté du midi, au sud, il y aura pour former le parvis, des rideaux en fin lin retors. La longueur de l'un des côtés sera de 100 coudées.
27:10	Il y aura 20 colonnes avec leurs 20 bases en cuivre, mais les crochets des colonnes et leurs filets seront d'argent.
27:11	Ainsi du côté nord, il y aura également des rideaux sur une longueur de 100 coudées, avec 20 colonnes avec leurs 20 bases en cuivre ; mais les crochets des colonnes avec leurs filets seront d'argent.
27:12	La largeur du parvis du côté de l'occident sera de 50 coudées de rideaux, qui auront 10 colonnes, avec leurs 10 bases.
27:13	Et la largeur du parvis du côté de l'orient, directement vers le levant, sera de 50 coudées.
27:14	À l'un des côtés, il y aura 15 coudées de rideaux, avec leurs 3 colonnes et leurs 3 bases.
27:15	Et de l'autre côté, 15 coudées de rideaux, avec leurs 3 colonnes et leurs 3 bases.

### La porte du parvis

27:16	Il y aura aussi pour la porte du parvis, un rideau de 20 coudées, fait en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, ouvrage de broderie, avec 4 colonnes et 4 bases.
27:17	Toutes les colonnes du parvis seront ceintes d'un filet en argent, et leurs crochets seront en argent, mais leurs bases seront en cuivre.
27:18	La longueur du parvis sera de 100 coudées, sa largeur de 50 de chaque côté, et sa hauteur de 5 coudées. Il sera en fin lin retors, et les bases en cuivre.
27:19	Que tous les ustensiles du tabernacle, pour tout son service, et tous ses pieux, avec les pieux du parvis, soient de cuivre<!--Ex. 38:9-20.-->.

### L'huile d'olive pure pour les lampes

27:20	Tu ordonneras aux fils d'Israël qu'ils t'apportent de l'huile pure d'olives concassées pour le luminaire, afin de faire luire les lampes continuellement<!--Ex. 35:8-28 ; Lé. 24:1-4.-->.
27:21	Aaron avec ses fils les prépareront en présence de YHWH, depuis le soir jusqu'au matin, dans la tente d'assignation, hors du voile qui est devant le témoignage. Ce sera un statut perpétuel pour les enfants d'Israël.

## Chapitre 28

### La prêtrise

28:1	Et toi, fais approcher de toi, Aaron ton frère et ses fils avec lui, d'entre les enfants d'Israël, pour exercer la prêtrise pour moi : Aaron, Nadab et Abihou, Èl’azar et Ithamar, fils d'Aaron.
28:2	Et tu feras à Aaron ton frère, des vêtements sacrés en signe de gloire et de beauté.

### Les vêtements sacrés des prêtres

28:3	Et tu parleras à tous les hommes sages de cœur, à chacun de ceux que j'ai remplis de l'Esprit de sagesse, afin qu'ils fassent des vêtements à Aaron pour le consacrer, afin qu'il exerce la prêtrise pour moi.
28:4	Et voici les vêtements qu'ils feront : le pectoral, l'éphod, la robe, la tunique brodée, la tiare et la ceinture. Ils feront donc les vêtements sacrés à Aaron ton frère et à ses fils, pour exercer la prêtrise pour moi.
28:5	Et ils prendront de l'or, de l'étoffe violette, de la pourpre rouge, de l'écarlate de cochenille et du fin lin.

### L'éphod

28:6	Et ils feront l'éphod en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, ouvrage d'ingénieur.
28:7	Il aura 2 épaulettes qui se joindront par les 2 bouts. C'est ainsi qu'il sera joint.
28:8	La ceinture de l'éphod, celle qui est dessus, sera de travail identique : en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors.
28:9	Et tu prendras 2 pierres d'onyx et tu graveras sur elles les noms des enfants d'Israël :
28:10	6 de leurs noms sur une pierre et les 6 noms des autres sur l'autre pierre, selon leur naissance.
28:11	Tu graveras sur les deux pierres en ouvrage de graveur de pierres les noms des enfants d'Israël, comme la gravure d'un sceau, tu les entoureras de montures d'or.
28:12	Et tu mettras les deux pierres sur les épaulettes de l'éphod, afin qu'elles soient des pierres de souvenir pour les enfants d'Israël. Car Aaron portera leurs noms sur ses deux épaules devant YHWH, pour souvenir.
28:13	Tu feras aussi des montures en or,
28:14	et 2 chaînes en or pur que tu tresseras en forme de cordons, et tu fixeras aux montures les chaînes ainsi tressées.

### Le pectoral

28:15	Tu feras aussi le pectoral du jugement d'un ouvrage d'ingénieur, comme l'ouvrage de l'éphod, en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors.
28:16	Il sera carré et double ; et sa longueur sera d'un empan, et sa largeur d'un empan.
28:17	Et tu le rempliras de garniture de pierres, à 4 rangées de pierres précieuses. À la première rangée, on mettra une sardoine, une topaze et une émeraude.
28:18	Et à la seconde rangée, une escarboucle, un saphir et un diamant<!--Certains traduisent par jaspe.-->.
28:19	Et à la troisième rangée, une opale, une agate et une améthyste.
28:20	Et à la quatrième rangée, une chrysolithe, un onyx et un jaspe<!--Certains traduisent par béryl.-->, qui seront enchâssés dans de l'or, selon leur garniture.
28:21	Et ces pierres-là seront selon les noms des enfants d'Israël, 12 selon leurs noms, chacune d'elles gravée comme des sceaux, selon le nom qu'elle doit porter, et elles seront pour les 12 tribus.
28:22	Tu feras donc pour le pectoral des chaînes en or pur, tressées en forme de cordon.
28:23	Et tu feras pour le pectoral deux anneaux en or, et tu mettras les 2 anneaux aux 2 extrémités du pectoral.
28:24	Et tu mettras les 2 cordons en or dans les 2 anneaux à l'extrémité du pectoral.
28:25	Et tu mettras les 2 autres extrémités des 2 chaînettes en cordon sur les deux montures, et tu les mettras sur les épaulettes de l'éphod, sur le devant de l'éphod.
28:26	Tu feras aussi 2 autres anneaux en or, que tu mettras aux 2 autres extrémités du pectoral, sur le bord qui sera du côté de l'éphod à l'intérieur.
28:27	Et tu feras 2 autres anneaux en or, que tu mettras aux 2 épaulettes de l'éphod par le bas, sur le devant, à l'endroit où il se joint, au-dessus de la ceinture de l'éphod.
28:28	On liera le pectoral par ses anneaux aux anneaux de l'éphod avec un cordon d'étoffe violette, afin qu'il tienne au-dessus de la ceinture de l'éphod, et que le pectoral ne puisse pas se séparer de l'éphod.
28:29	Ainsi Aaron portera sur son cœur, les noms des enfants d'Israël gravés sur le pectoral du jugement, quand il entrera dans le lieu saint, pour servir continuellement de souvenir devant YHWH.

### L'ourim et le thoummim

28:30	Et tu mettras sur le pectoral de jugement l'ourim et le thoummim<!--L'ourim (« lumières ») et le thoummim (« perfections ») étaient deux pierres du pectoral que l'on utilisait ensemble pour déterminer la décision d'Elohîm sur certaines questions. Dans ce passage, l'ourim et le thoummim sont précédés par les lettres hébraïques aleph et tav.-->, qui seront sur le cœur d'Aaron, quand il viendra devant YHWH. Et Aaron portera le jugement des enfants d'Israël sur son cœur devant YHWH, continuellement.

### La robe de l'éphod

28:31	Tu feras aussi la robe de l'éphod entièrement d'étoffe violette.
28:32	Il y aura, au milieu, une ouverture pour la tête, et cette ouverture aura tout autour un bord tissé, comme l'ouverture d'une cotte de mailles, afin que la robe ne se déchire pas.
28:33	Tu feras sur ses bords des grenades en étoffe violette, en pourpre rouge et en écarlate de cochenille tout autour, et des clochettes d'or entre elles tout autour.
28:34	Une clochette en or, puis une grenade, une clochette en or, puis une grenade, aux bords de la robe tout autour.
28:35	Et Aaron en sera revêtu quand il fera le service, et on en entendra le son lorsqu'il entrera dans le lieu saint devant YHWH, et quand il en sortira, afin qu'il ne meure pas.

### La lame d'or gravée : « Sainteté à YHWH »

28:36	Et tu feras une lame en or pur, sur laquelle tu graveras ces mots, comme on grave un sceau : Sainteté à YHWH.
28:37	Tu l'attacheras avec un cordon d'étoffe violette sur la tiare, sur le devant de la tiare.
28:38	Elle sera sur le front d'Aaron, ainsi Aaron portera l'iniquité commise par les enfants d'Israël en faisant leurs saintes offrandes : elle sera continuellement sur son front devant YHWH, pour qu'il leur soit favorable.

### Les vêtements de service d'Aaron et ses fils

28:39	Tu tisseras la tunique en fin lin, tu feras une tiare en fin lin et tu feras une ceinture d'ouvrage de broderie<!--Ex. 39:1-32.-->.
28:40	Tu feras aussi pour les fils d'Aaron des tuniques ainsi que des ceintures et des turbans pour leur gloire et leur ornement.
28:41	Et tu en revêtiras Aaron ton frère et ses fils avec lui. Tu les oindras, tu rempliras leur main, tu les sanctifieras et ils exerceront la prêtrise pour moi<!--Lé. 8:12, 16:32 ; No. 3:3.-->.
28:42	Tu leur feras des caleçons de lin, pour couvrir la nudité de leur chair : ils tiendront depuis les reins jusqu'au bas des cuisses.
28:43	Et Aaron et ses fils seront ainsi habillés quand ils entreront dans la tente d'assignation, ou quand ils approcheront de l'autel pour faire le service dans le lieu saint, afin qu'ils ne portent pas d'iniquité et ne meurent pas. Ce sera un statut perpétuel pour lui et pour sa postérité après lui.

## Chapitre 29

### Les prêtres consacrés au service de YHWH

29:1	Et voici ce que tu leur feras pour les consacrer, afin qu'ils exercent la prêtrise pour moi : prends un veau du troupeau et deux béliers sans défaut<!--Lé. 8:2, 9:2 ; Hé. 7:26-28.--> ;
29:2	et des pains sans levain, et des gâteaux sans levain pétris à l'huile, et des galettes sans levain, oints d'huile. Tu les feras de fine farine de froment<!--Lé. 6:13.-->.
29:3	Tu les mettras dans une corbeille et tu les présenteras dans la corbeille et tu présenteras aussi le veau et les deux béliers.
29:4	Puis, tu feras approcher Aaron et ses fils à l'entrée de la tente d'assignation et tu les laveras avec de l'eau<!--Ex. 40:12.-->.
29:5	Ensuite, tu prendras les vêtements et tu feras vêtir à Aaron, la tunique et la robe de l'éphod, l'éphod et le pectoral ; et tu le ceindras par-dessus avec la ceinture de l'éphod.
29:6	Puis, tu mettras sur sa tête la tiare, et la couronne de sainteté sur la tiare.
29:7	Et tu prendras l'huile d'onction et la répandras sur sa tête ; et tu l'oindras ainsi.
29:8	Puis, tu feras approcher ses fils et tu les habilleras des tuniques.
29:9	Tu ceindras Aaron et ses fils d'une ceinture<!--Es. 11:5 ; Ep. 6:14.--> et tu leur attacheras des turbans. Ils posséderont la prêtrise par statut perpétuel. Et tu rempliras la main d'Aaron et la main de ses fils.
29:10	Et tu feras approcher le veau devant la tente d'assignation, et Aaron et ses fils poseront leurs mains sur la tête du veau.
29:11	Et tu égorgeras le veau devant YHWH, à l'entrée de la tente d'assignation.
29:12	Puis, tu prendras du sang du veau et le mettras avec ton doigt sur les cornes de l'autel, et tu répandras tout le reste du sang au pied de l'autel.
29:13	Tu prendras aussi toute la graisse qui couvre les entrailles, et le lobe du foie, les deux rognons, la graisse qui les entoure, et tu les feras fumer sur l'autel.
29:14	Mais tu brûleras au feu la chair du veau, sa peau et ses excréments, hors du camp. C'est un sacrifice pour le péché<!--Lé. 1:3-13 ; Hé. 9:11-22, 13:11.-->.
29:15	Puis, tu prendras l'un des béliers, et Aaron et ses fils poseront leurs mains sur la tête du bélier.
29:16	Puis, tu égorgeras le bélier, et prenant son sang, et tu en aspergeras le pourtour de l'autel.
29:17	Après, tu couperas le bélier par pièces, et ayant lavé ses entrailles et ses jambes, tu les mettras sur ses pièces et sur sa tête.
29:18	Et tu feras fumer tout le bélier sur l'autel. C'est un holocauste pour YHWH, un sacrifice au parfum apaisant, consumé par le feu pour YHWH.
29:19	Puis, tu prendras l'autre bélier, et Aaron et ses fils mettront leurs mains sur sa tête.
29:20	Et tu égorgeras le bélier, et prenant de son sang, tu le mettras sur le lobe de l'oreille droite d'Aaron, et sur le lobe de l'oreille droite de ses fils, sur le pouce de leur main droite, sur le gros orteil de leur pied droit, et tu aspergeras de sang le pourtour de l'autel.
29:21	Et tu prendras du sang qui sera sur l'autel, de l'huile d'onction, et tu en feras l'aspersion sur Aaron, sur ses vêtements, sur ses fils, et sur les vêtements de ses fils avec lui. Ainsi, lui, ses vêtements, ses fils et les vêtements de ses fils, seront sanctifiés avec lui.
29:22	Tu prendras aussi la graisse du bélier, la queue, et la graisse qui couvre les entrailles, le grand lobe du foie, les deux rognons, la graisse qui est dessus et l'épaule droite, car c'est le bélier de consécration.
29:23	Tu prendras encore dans la corbeille des pains sans levain placée devant YHWH, un pain, un gâteau à l'huile et une galette.
29:24	Et tu mettras toutes ces choses sur les mains d'Aaron et sur les mains de ses fils, et tu les agiteras de côté et d'autre devant YHWH<!--No. 6:19.-->.
29:25	Puis, les recevant de leurs mains, tu les feras fumer sur l'autel, sur l'holocauste, pour être un parfum apaisant devant YHWH. C'est un sacrifice consumé par le feu à YHWH.

### La part des prêtres

29:26	Tu prendras aussi la poitrine du bélier des consécrations, qui est pour Aaron, et tu l'agiteras de côté et d'autre en offrande agitée devant YHWH. Ce sera ta part.
29:27	Tu sanctifieras donc la poitrine de l'offrande agitée, et l'épaule de l'offrande élevée, tant ce qui aura été agité que ce qui aura été élevé du bélier de consécration, de ce qui est pour Aaron et de ce qui est pour ses fils<!--Lé. 10:14 ; No. 18:18.-->.
29:28	Et ceci sera une ordonnance perpétuelle pour Aaron et pour ses fils, de ce qui sera offert par les enfants d'Israël, car c'est une offrande élevée. Quand il y aura une offrande élevée de celles qui sont faites par les enfants d'Israël, de leurs offrandes de paix, leur offrande élevée sera pour YHWH.
29:29	Les vêtements sacrés d'Aaron seront pour ses fils après lui, qui les mettront lorsqu'on les oindra et qu'on remplira leurs mains.
29:30	Le prêtre qui succédera à sa place d'entre ses fils, et qui viendra à la tente d'assignation, pour faire le service dans le lieu saint, en sera revêtu durant 7 jours.
29:31	Et tu prendras le bélier des consécrations et tu feras bouillir sa chair dans un lieu saint.
29:32	Aaron et ses fils mangeront à l'entrée de la tente d'assignation la chair du bélier et le pain qui sera dans la corbeille.
29:33	Ils mangeront donc ces choses, par lesquelles la propitiation aura été faite, pour consacrer leurs mains et les sanctifier ; mais l'étranger n'en mangera pas, parce qu'elles sont saintes.
29:34	S'il reste de la chair des consécrations et du pain jusqu'au matin, tu brûleras ce reste au feu. On n'en mangera pas, parce que c'est une chose sainte.
29:35	Tu agiras ainsi à l'égard d'Aaron et de ses fils selon toutes les choses que je t'ai ordonnées. Tu consacreras leurs mains pendant 7 jours<!--Lé. 8:31-35.-->.
29:36	Chaque jour, tu sacrifieras pour le péché un jeune taureau pour l'expiation. Tu offriras pour l'autel un sacrifice pour le péché en faisant la propitiation pour lui, et tu l'oindras pour le sanctifier<!--Ez. 43:19-20.-->.
29:37	Pendant 7 jours, tu feras propitiation pour l'autel et tu le sanctifieras. L'autel sera saint des saints et tout ce qui touchera l'autel sera saint<!--No. 28:3.-->.

### L'holocauste perpétuel

29:38	Et voici ce que tu offriras sur l'autel : 2 agneaux d'un an, chaque jour, à perpétuité.
29:39	Tu sacrifieras l'un des agneaux au matin, et l'autre agneau entre les deux soirs,
29:40	avec un dixième de fine farine pétrie dans un quart de hin d'huile d'olives concassées, et avec une libation d'un quart de hin de vin pour chaque agneau.
29:41	Et tu offriras l'autre agneau entre les deux soirs, tu l'offriras avec une offrande de grain et une libation semblables à celles du matin, en parfum apaisant ; c'est un sacrifice consumé par le feu pour YHWH.
29:42	Ce sera l'holocauste perpétuel qui sera offert dans vos générations, à l'entrée de la tente d'assignation, devant YHWH, où je me trouverai avec vous pour te parler.
29:43	Je me trouverai là pour les enfants d'Israël, et la tente sera sanctifiée par ma gloire.
29:44	Je sanctifierai donc la tente d'assignation et l'autel. Je sanctifierai aussi Aaron et ses fils, afin qu'ils exercent la prêtrise pour moi.
29:45	Et j'habiterai au milieu des enfants d'Israël, et je serai leur Elohîm.
29:46	Et ils sauront que je suis YHWH leur Elohîm, qui les ai fait sortir du pays d'Égypte, pour habiter au milieu d'eux. Je suis YHWH leur Elohîm.

## Chapitre 30

### L'autel des parfums ou de l'encens

30:1	Tu feras aussi un autel pour brûler de l'encens. Tu le feras en bois d'acacia.
30:2	Sa longueur sera d'une coudée et sa largeur d'une coudée. Il sera carré et sa hauteur sera de 2 coudées. Ses cornes sortiront de lui.
30:3	Tu le couvriras d'or pur, tant le dessus que ses côtés tout autour, et ses cornes. Et tu lui feras un couronnement d'or tout autour.
30:4	Tu lui feras aussi 2 anneaux d'or au-dessous de son couronnement, à ses deux côtés, lesquels tu mettras aux 2 coins, pour y faire passer les barres qui serviront à le porter.
30:5	Tu feras les barres en bois d'acacia et tu les couvriras d'or.
30:6	Et tu les mettras devant le voile, qui est au-devant de l'arche du témoignage, à l'endroit du propitiatoire qui est sur le témoignage, où je me trouverai avec toi.
30:7	Et Aaron y fera brûler de l'encens aromatique. Il en fera brûler chaque matin, quand il préparera les lampes.
30:8	Et quand Aaron allumera les lampes entre les deux soirs, il en fera brûler aussi. C'est ainsi que l'on brûlera de l'encens à perpétuité devant YHWH dans vos générations<!--Ex. 37:25-29 ; 2 Ch. 13:11.-->.
30:9	Vous n'offrirez sur cet autel ni encens étranger, ni holocauste, ni offrande, et vous n'y répandrez aucune libation.
30:10	Et Aaron fera une fois par an, la propitiation sur les cornes de cet autel, avec le sang du sacrifice pour le péché, sacrifice offert pour la propitiation, il y fera la propitiation une fois par an, dans toutes vos générations. C'est une chose sainte, sainte pour YHWH.

### L'offrande pour faire la propitiation pour les âmes<!--Ex. 15:1-21 ; Ps. 107:1-2.-->

30:11	YHWH parla aussi à Moshè et lui dit :
30:12	Lorsque tu compteras les têtes des fils d'Israël pour en faire le dénombrement, chaque homme donnera la rançon de son âme à YHWH lors du dénombrement ; ainsi, lors de ce dénombrement<!--No. 1:2.-->, il n'y aura pas de plaie parmi eux.
30:13	Tous ceux qui passeront par le dénombrement donneront un demi-sicle, selon le sicle du lieu saint, qui est de 20 guéras. Le demi-sicle donc sera l'offrande que l'on donnera à YHWH<!--Lé. 27:25 ; No. 3:47 ; Ez. 45:12.-->.
30:14	Tous ceux qui passeront par le dénombrement, depuis l'âge de 20 ans et au-dessus, donneront cette offrande à YHWH.
30:15	Le riche n'augmentera rien, et le pauvre ne diminuera rien du demi-sicle, quand ils donneront à YHWH l'offrande pour faire la propitiation pour vos âmes.
30:16	Tu prendras donc des enfants d'Israël l'argent des propitiations et tu l'appliqueras à l'œuvre de la tente d'assignation. Ce sera pour les fils d'Israël, un souvenir devant YHWH pour faire la propitiation pour vos âmes.

### Purification par l'eau de la cuve en cuivre<!--Jn. 13:3-10 ; Hé. 10:22 ; 1 Jn. 1:9.-->

30:17	YHWH parla encore à Moshè, en disant :
30:18	Fais aussi une cuve en cuivre, avec sa base de cuivre, pour s'y laver. Et tu la mettras entre la tente d'assignation et l'autel, et tu mettras de l'eau dedans ;
30:19	Aaron et ses fils y laveront leurs mains et leurs pieds.
30:20	Quand ils entreront dans la tente d'assignation, ils se laveront avec de l'eau afin qu'ils ne meurent pas. Il en ira de même quand ils approcheront de l'autel pour faire le service afin de faire fumer l'offrande consumée par le feu pour YHWH.
30:21	Ils laveront donc leurs pieds et leurs mains, afin qu'ils ne meurent pas. Ce sera pour eux une ordonnance perpétuelle, tant pour lui que pour ses fils dans leur génération.

### L'huile pour l'onction sainte<!--Jn. 4:23 ; Ep. 2:18, 5:18-19.-->

30:22	YHWH parla aussi à Moshè, en disant :
30:23	Prends des aromates de première qualité : 500 sicles de myrrhe, de celle qui coule d'elle-même ; du cinnamome odoriférant, la moitié, soit 250 sicles et du roseau aromatique, 250 sicles ;
30:24	de la casse, le poids de 500 sicles, selon le sicle du lieu saint, et un hin d'huile d'olive.
30:25	Et tu en feras de l'huile pour l'onction sainte, un onguent composé selon l'art du parfumeur, ce sera l'huile de l'onction sainte.
30:26	Puis tu en oindras la tente d'assignation et l'arche du témoignage,
30:27	la table et tous ses ustensiles, le chandelier et ses ustensiles, et l'autel de l'encens,
30:28	et l'autel des holocaustes et tous ses ustensiles, la cuve et sa base.
30:29	Ainsi, tu les sanctifieras, et ils seront saints des saints. Tout ce qui les touchera sera saint.
30:30	Tu oindras aussi Aaron et ses fils, et les consacreras pour exercer la prêtrise pour moi.
30:31	Tu parleras aussi aux enfants d'Israël, en disant : Ce sera pour moi une huile d'onction sainte dans toutes vos générations.
30:32	On ne la versera pas sur la chair d'un être humain, et vous n'en ferez pas de semblable, de même composition. Elle est sainte, elle sera sainte pour vous.
30:33	Quiconque composera un onguent semblable et qui en mettra sur un étranger, sera retranché de ses peuples.

### L'encens pur parfumé

30:34	YHWH dit aussi à Moshè : Prends des aromates, du stacté, de l'onyx et du galbanum, des aromates et de l'encens pur. Ce sera part pour part.
30:35	Et tu feras avec cela un encens parfumé selon l'art du parfumeur, et tu y mettras du sel. Vous le ferez pur, et ce sera pour vous une chose sainte.
30:36	Après l'avoir réduit en poudre, tu le mettras dans la tente d'assignation devant le témoignage, où je me trouverai avec toi. Il sera pour vous saint des saints.
30:37	Quant à l'encens que tu feras, vous n'en ferez pas pour vous de semblable composition. Ce sera une chose sainte pour YHWH.
30:38	L'homme qui en fera un semblable pour en sentir l'odeur, sera retranché de son peuple.

## Chapitre 31

### YHWH suscite des artisans

31:1	YHWH parla aussi à Moshè, en disant :
31:2	Regarde, j'ai appelé par son nom Betsaleel, fils d'Ouri, fils de Hour, de la tribu de Yéhouda.
31:3	Et je l'ai rempli de l'Esprit d'Elohîm, de sagesse, d'intelligence et de connaissance pour toutes sortes d'ouvrages,
31:4	pour concevoir des inventions, pour travailler l'or, l'argent et le cuivre ;
31:5	pour tailler les pierres à enchâsser, pour tailler le bois afin de faire toutes sortes d'ouvrages.
31:6	Et voici, je lui ai donné pour compagnon Oholiab, fils d'Ahisamac, de la tribu de Dan. Dans le cœur de tout sage de cœur, j'ai mis de la sagesse, afin qu'ils fassent toutes les choses que je t'ai ordonnées :
31:7	la tente d'assignation, l'arche du témoignage, et le propitiatoire qui doit être au-dessus, et tous les ustensiles du tabernacle,
31:8	la table avec tous ses ustensiles, le chandelier pur avec tous ses ustensiles, l'autel de l'encens,
31:9	l'autel de l'holocauste avec tous ses ustensiles, la cuve et sa base,
31:10	les vêtements en ouvrage tressé, les vêtements sacrés pour le prêtre Aaron, et les vêtements de ses fils pour exercer la prêtrise,
31:11	l'huile d'onction et l'encens aromatique pour le lieu saint. Ils feront toutes les choses que je t'ai ordonnées.

### Le shabbat comme signe entre YHWH et Israël

31:12	YHWH parla encore à Moshè, en disant :
31:13	Toi aussi parle aux enfants d'Israël, en disant : Vous garderez absolument mes shabbats, car c'est un signe entre moi et vous, et parmi vos générations, afin que vous sachiez que je suis YHWH-Mékadichéem<!--YHWH qui vous sanctifie.-->.
31:14	Vous garderez le shabbat car il est saint pour vous. Celui qui le profanera mourra, il mourra. Quiconque fera une œuvre en ce jour-là, sera retranché du milieu de son peuple.
31:15	Pendant 6 jours le travail se fera, mais le septième jour est le shabbat, le jour du repos, consacré à YHWH. Quiconque fera un travail le jour du repos mourra, il mourra.
31:16	Ainsi, les enfants d'Israël garderont le shabbat pour célébrer le jour du repos, en leur génération, par une alliance perpétuelle.
31:17	C'est un signe entre moi et les enfants d'Israël à perpétuité. Car YHWH a fait en 6 jours les cieux et la Terre, et il a cessé au septième et s'est reposé<!--Ge. 2:2 ; Ez. 20:12.-->.
31:18	Lorsqu'il eut achevé de parler à Moshè, au mont Sinaï, il lui donna les deux tablettes du témoignage, les tablettes de pierre écrites du doigt d'Elohîm<!--De. 9:10.-->.

## Chapitre 32

### Le culte du veau d'or

32:1	Quand le peuple vit que Moshè tardait à descendre de la montagne, le peuple se rassembla autour d'Aaron et lui dit : Lève-toi ! Fais-nous des elohîm qui marchent devant nous, car quant à ce Moshè, cet homme qui nous a fait monter du pays d'Égypte, nous ne savons ce qui lui est arrivé<!--Ac. 7:40.-->.
32:2	Et Aaron leur répondit : Mettez en pièces les anneaux d'or qui sont aux oreilles de vos femmes, de vos fils et de vos filles, et apportez-les-moi<!--Ex. 35:22.-->.
32:3	Et aussitôt, tout le peuple mit en pièces les anneaux d'or qui étaient à leurs oreilles et ils les apportèrent à Aaron.
32:4	Ayant pris l'or de leurs mains, il le façonna au ciseau pour en faire un veau<!--Selon toute vraisemblance, les Israélites s'étaient inspirés d'une idole égyptienne, le taureau sacré Apis, pour faire le veau d'or. Dieu de la puissance sexuelle, de la fertilité et de la force, il était souvent représenté sous la forme d'un homme avec une tête de taureau, puis avec un disque solaire entre les cornes à partir du Nouvel Empire.--> en métal fondu<!--Lé. 19:4, 26:1.-->. Et ils dirent : Voici tes elohîm, ô Israël, qui t'ont fait monter du pays d'Égypte.
32:5	Lorsque Aaron vit cela, il bâtit un autel devant le veau, et cria, en disant : Demain, il y aura une fête pour YHWH !
32:6	Ainsi, ils se levèrent le lendemain, dès le matin, et ils offrirent des holocaustes et présentèrent des offrandes de paix. Et le peuple s'assit pour manger et pour boire, puis ils se levèrent pour jouer<!--1 Co. 10:7.-->.

### YHWH condamne l'idolâtrie d'Israël

32:7	Alors YHWH dit à Moshè : Va, descends, car ton peuple que tu as fait monter du pays d'Égypte s'est corrompu<!--De. 32:5.-->.
32:8	Ils se sont promptement détournés de la voie que je leur avais ordonnée, et ils se sont fait un veau en métal fondu et se sont prosternés devant lui, ils lui ont offert des sacrifices, puis ont dit : Voici tes elohîm, ô Israël, qui t'ont fait monter du pays d'Égypte<!--1 R. 12:28.-->.
32:9	YHWH dit encore à Moshè : J'ai regardé ce peuple, et voici, c'est un peuple au cou raide.
32:10	Maintenant laisse-moi faire ! Ma colère s'embrasera contre eux et je les consumerai, mais je te ferai devenir une grande nation.

### Moshè implore YHWH pour le peuple

32:11	Alors Moshè supplia YHWH son Elohîm et dit : Ô YHWH, pourquoi ta colère s'embraserait-elle contre ton peuple, que tu as fait sortir du pays d'Égypte par une grande puissance et par une main forte<!--Ps. 106:23.--> ?
32:12	Pourquoi les Égyptiens parleraient-ils, en disant : C'est dans une mauvaise intention qu'il les a fait sortir, pour les tuer dans les montagnes et pour les consumer de la surface de la Terre ? Reviens de l'ardeur de ta colère et repens-toi de ce mal que tu veux faire à ton peuple<!--No. 14:11-15 ; De. 9:28.-->.
32:13	Souviens-toi d'Abraham, de Yitzhak et d'Israël, tes serviteurs, auxquels tu as juré par toi-même en leur disant : Je multiplierai votre postérité comme les étoiles des cieux, et je donnerai à votre postérité tout ce pays dont j'ai parlé, et ils l'hériteront à jamais<!--De. 34:4.-->.
32:14	Et YHWH se repentit du mal qu'il avait dit qu'il ferait à son peuple.

### Jugement sur le peuple

32:15	Moshè regarda et descendit de la montagne, ayant dans sa main les deux tablettes du témoignage. Les tablettes étaient écrites des deux côtés, écrites de l'un et de l'autre côté.
32:16	Les tablettes étaient l'ouvrage d'Elohîm, et l'écriture était l'écriture d'Elohîm, gravée sur les tablettes.
32:17	Et Yéhoshoua, entendant la voix du peuple qui faisait un grand bruit, dit à Moshè : Il y a un cri de guerre dans le camp.
32:18	Il répondit : Ce n'est pas un bruit de cris de bravoure ni un bruit de cris de défaite. J'entends un bruit de gens qui chantent.
32:19	Et il arriva que lorsque Moshè fut approché du camp, il vit le veau et les danses. La colère de Moshè s'embrasa. Il jeta de ses mains les tablettes et les brisa au pied de la montagne.
32:20	Il prit ensuite le veau qu'ils avaient fait, et le brûla au feu, et l'écrasa jusqu'à ce qu'il soit réduit en poudre, puis il répandit cette poudre dans de l'eau et il la fit boire aux enfants d'Israël<!--De. 9:17-21.-->.
32:21	Et Moshè dit à Aaron : Que t'a fait ce peuple pour que tu aies fait venir sur lui un si grand péché ?
32:22	Et Aaron lui répondit : Que la colère de mon seigneur ne s'embrase pas, tu sais que ce peuple est porté au mal.
32:23	Ils m'ont dit : Fais-nous des elohîm qui marchent devant nous ! Car ce Moshè, cet homme qui nous a fait monter du pays d'Égypte, nous ne savons ce qui lui est arrivé !
32:24	Alors je leur ai dit : Que celui qui a de l'or, le mette en pièces ! Et ils me l'ont donné. Je l'ai jeté au feu, et ce veau en est sorti.
32:25	Or Moshè vit que le peuple était dénudé<!--Le mot hébreu signifie aussi « laisser faire », « conduire », « agir comme un meneur », « laisser seul » voir Pr. 29:18.-->, car Aaron l'avait dénudé et exposé à la dérision de ses ennemis.
32:26	Et Moshè se tenant à la porte du camp, dit : Qui est pour YHWH ? Qu'il vienne vers moi ! Et tous les fils de Lévi se rassemblèrent autour de lui.
32:27	Et il leur dit : Ainsi parle YHWH, l'Elohîm d'Israël : Que chacun mette son épée à son côté, passez et repassez de porte en porte par le camp, et que chacun de vous tue son frère, son ami et son voisin.
32:28	Et les fils de Lévi firent selon la parole de Moshè ; et ce jour-là il tomba parmi le peuple environ 3 000 hommes.
32:29	Et Moshè dit : Consacrez aujourd'hui vos mains à YHWH, chacun même contre son fils et contre son frère, afin que vous attiriez aujourd'hui sur vous la bénédiction.

### Moshè intercède pour Israël

32:30	Et le lendemain, Moshè dit au peuple : Vous avez commis un grand péché. Mais je monterai vers YHWH, et peut-être, je ferai la propitiation pour votre péché.
32:31	Moshè donc retourna vers YHWH et dit : Oh ! Je te prie, ce peuple a commis un grand péché, en se faisant des elohîm d'or.
32:32	Maintenant pardonne leur péché ! Sinon, efface-moi maintenant de ton livre que tu as écrit.
32:33	Et YHWH répondit à Moshè : C'est celui qui aura péché contre moi que j'effacerai de mon livre<!--Ap. 3:5, 20:15, 21:27.-->.
32:34	Va maintenant, conduis le peuple au lieu dont je t'ai parlé. Voici, mon ange marchera devant toi, mais le jour où j'exercerai la punition, je ferai venir sur eux la punition pour leur péché.
32:35	Et YHWH frappa le peuple parce qu'il avait fabriqué le veau, celui qu'avait fabriqué Aaron.

## Chapitre 33

### YHWH ne veut plus marcher avec Israël

33:1	YHWH donc dit à Moshè : Va, monte d'ici, toi et le peuple que tu as fait monter du pays d'Égypte, au pays que j'ai juré de donner à Abraham, à Yitzhak et à Yaacov, en disant : Je le donnerai à ta postérité.
33:2	Et j'enverrai un ange devant toi et je chasserai les Cananéens, les Amoréens, les Héthiens, les Phéréziens, les Héviens et les Yebousiens,
33:3	vers un pays où coulent le lait et le miel, mais je ne monterai pas au milieu de toi, parce que tu es un peuple au cou raide, de peur que je ne te consume en chemin.
33:4	Lorsque le peuple entendit cette parole de malheur, il prit le deuil et personne ne se revêtit de ses ornements.
33:5	Alors YHWH dit à Moshè : Dis aux enfants d'Israël : Vous êtes un peuple au cou raide. Je monterai en un moment au milieu de toi et je te consumerai. Maintenant donc, ôte tes ornements de dessus toi et je saurai ce que je te ferai.
33:6	Ainsi, les enfants d'Israël se dépouillèrent de leurs ornements vers la montagne d'Horeb.

### Moshè dresse la tente d'assignation hors du camp

33:7	Et Moshè prit une tente et la dressa pour lui hors du camp, loin du camp. Il l'appela la tente d'assignation. Tous ceux qui cherchaient YHWH sortaient vers la tente d'assignation qui était hors du camp.
33:8	Et il arrivait qu'aussitôt que Moshè sortait vers la tente, tout le peuple se levait, et chacun se tenait à l'entrée de sa tente et regardait Moshè par-derrière, jusqu'à ce qu'il soit entré dans la tente.
33:9	Et il arriva que, comme Moshè entrait dans la tente, la colonne de nuée descendit et se tint à l'entrée de la tente et il parla avec Moshè.
33:10	Tout le peuple voyait la colonne de nuée qui se tenait à l'entrée de la tente, et tout le peuple se levait et se prosternait, chacun à l'entrée de sa tente.
33:11	Et YHWH parlait à Moshè face à face, comme un homme parle avec son ami intime. Puis Moshè retournait dans le camp, mais son serviteur, le jeune Yéhoshoua, fils de Noun, ne quittait pas la tente<!--No. 12:8 ; De. 34:10 ; Jn. 15:14-15.-->.

### Moshè demande que YHWH marche avec Israël

33:12	Moshè donc dit à YHWH : Regarde, tu m'as dit : Fais monter ce peuple, et tu ne m'as pas fait connaître celui que tu dois envoyer avec moi. Tu as même dit : Je te connais par ton nom, et aussi, tu as trouvé grâce à mes yeux.
33:13	Et maintenant, je te prie, si j'ai trouvé grâce à tes yeux, fais-moi connaître ton chemin<!--Jn. 14:6.-->, alors je te connaîtrai et je trouverai ainsi grâce à tes yeux. Considère aussi que cette nation est ton peuple<!--Ps. 25:4.-->.
33:14	Et YHWH dit : Ma face ira avec toi et je te donnerai du repos.
33:15	Et Moshè lui dit : Si ta face ne vient avec nous, ne nous fais pas monter d'ici.
33:16	Car en quoi connaîtra-t-on que nous avons trouvé grâce à tes yeux, moi et ton peuple ? Ne sera-ce pas quand tu marcheras avec nous ? Et alors, moi et ton peuple serons distingués de tous les peuples qui sont sur la Terre.
33:17	Et YHWH dit à Moshè : Je ferai aussi ce que tu dis, car tu as trouvé grâce à mes yeux et je te connais par ton nom.

### Moshè veut voir la gloire de YHWH

33:18	Moshè dit aussi : Je te prie, fais-moi voir ta gloire !
33:19	Et Elohîm dit : Je ferai passer toute ma bonté devant ta face et je crierai le Nom de YHWH devant toi. Je ferai grâce à qui je ferai grâce et j'aurai compassion de celui de qui j'aurai compassion<!--Ro. 9:15.-->.
33:20	Puis il dit : Tu ne pourras pas voir ma face, car aucun être humain ne peut me voir et vivre<!--Jn. 1:18, 14:8-11.-->.
33:21	YHWH dit aussi : Voici, il y a un lieu près de moi, et tu t'arrêteras sur le rocher<!--Le rocher préfigurait Yéhoshoua ha Mashiah (Jésus-Christ), le Roc sur lequel nous devons bâtir nos vies et le fondement de l'Assemblée (Église) (Ps. 18:32 ; Mt. 7:24-25, 16:18 ; 1 Co. 3:11). Voir commentaire en Es. 8:13-17.-->.
33:22	Et quand ma gloire passera, je te mettrai dans le creux du rocher et te couvrirai de ma main, jusqu'à ce que je sois passé.
33:23	Puis je retirerai ma main et tu me verras par-derrière, mais ma face ne peut être vue.

## Chapitre 34

### De nouvelles tables de pierre ; la gloire de YHWH<!--Ex. 33:18-23.-->

34:1	YHWH dit à Moshè : Taille toi-même deux tablettes de pierre comme les premières, et j'écrirai sur ces tablettes les paroles qui étaient sur les premières tablettes que tu as brisées<!--De. 10:1.-->.
34:2	Et sois prêt au matin, et monte au matin sur la montagne de Sinaï, et présente-toi là devant moi sur le haut de la montagne.
34:3	Mais que personne ne monte avec toi. Que personne même ne paraisse sur toute la montagne. Que ni menu ni gros bétail ne paisse sur cette montagne<!--Ex. 19:12-13.-->.
34:4	Moshè donc tailla deux tablettes de pierre comme les premières. Il se leva de bon matin et monta sur la montagne de Sinaï, comme YHWH le lui avait ordonné. Il prit dans sa main les deux tablettes de pierre.
34:5	Et YHWH descendit dans la nuée, et s'arrêta là avec lui, et cria le Nom de YHWH.
34:6	Et YHWH passa devant lui en proclamant : YHWH, YHWH ! le El compatissant, miséricordieux, lent à la colère, grand en bonté et en fidélité<!--No. 14:18 ; 2 Ch. 30:9 ; Né. 9:17 ; Ps. 103:8.-->,
34:7	qui conserve sa bonté jusqu'à 1 000 générations, pardonnant l'iniquité, la transgression et le péché, qui ne tient pas le coupable pour innocent, et qui punit l'iniquité des pères sur les fils, et sur les fils des fils, jusqu'à la troisième et à la quatrième génération<!--Ex. 20:6 ; De. 5:10 ; Jé. 32:18.-->.
34:8	Et Moshè s'empressa de s'incliner jusqu'à terre, en se prosternant.
34:9	Et il dit : Ô Adonaï ! Je te prie, si j'ai trouvé grâce à tes yeux, qu'Adonaï marche maintenant au milieu de nous, car c'est un peuple au cou raide. Pardonne donc nos iniquités et notre péché, et prends-nous pour ta possession.

### YHWH renouvelle ses promesses<!--Ex. 33:18-23.-->

34:10	Et il répondit : Voici, je traite alliance devant tout ton peuple, je ferai des merveilles qui n'ont pas été faites sur toute la Terre ni dans aucune nation. Et tout le peuple au milieu duquel tu es, verra l'œuvre de YHWH, car ce que je m'en vais faire avec toi sera une chose redoutable.
34:11	Garde soigneusement ce que je t'ordonne aujourd'hui. Voici, je vais chasser devant toi les Amoréens, les Cananéens, les Héthiens, les Phéréziens, les Héviens et les Yebousiens.
34:12	Garde-toi de traiter alliance avec les habitants du pays où tu dois entrer, de peur qu'ils ne soient un piège pour toi<!--De. 7:2 ; Jos. 23:12-13 ; 2 Co. 6:14.-->.
34:13	Mais vous démolirez leurs autels, vous briserez leurs statues et vous couperez leurs poteaux d'Asherah<!--Cité au moins 40 fois dans le Tanakh, le terme hébreu « asherah » fait référence à un « arbre sacré, un pieu près d'un autel » ou encore « une idole », mots par lequels il est majoritairement traduit. Il s'agit également de l'objet en bois utilisé dans le culte de la parèdre de Baal. Menashè (Manassé), roi de Yéhouda, introduisit l'emblème d'Asherah dans le temple (2 R. 21:1-7) en dépit de l'interdiction formelle de YHWH (De. 16:21). Il ne fut enlevé que lors des réformes de Yoshiyah (Josias) et de Yehizqiyah (Ézéchias) (2 R. 18:3-4, 23:6-14). Pourtant, YHWH a toujours exigé la destruction d'Asherah, celle qu'il a nommée « l'abomination des Sidoniens », de peur que son peuple y trouve une occasion de chute (Jg. 2:13, 10:6 ; 1 S. 31:10 ; 1 R. 11:5-33 ; 2 R. 23:13). Bien qu'étant majoritairement citée dans les Écritures en tant qu'objet de culte, Asherah est également associée à la divinité Astarté, connue pour être l'Artémis (Diane) des Éphésiens (Ac. 19:23-40), la reine du ciel (Jé. 7:18, 44:15-30), l'Isis des Égyptiens et l'épouse de Baal (voir commentaire en Jg. 2:13).-->.
34:14	Car tu ne te prosterneras pas devant un autre el, parce que YHWH a pour Nom Jaloux, c'est le El qui est jaloux.
34:15	Garde-toi de traiter alliance avec les habitants du pays, de peur que lorsqu'ils se prostitueront après leurs elohîm, et sacrifieront à leurs elohîm, quelqu'un ne t'invite, et que tu ne manges de son sacrifice.
34:16	Et que tu ne prennes de leurs filles pour tes fils, lesquelles se prostituant après leurs elohîm, n'entraînent tes fils à se prostituer après leurs elohîm.
34:17	Tu ne te feras aucun elohîm de métal fondu.

### Les fêtes et le shabbat<!--Lé. 23:4-44.-->

34:18	Tu observeras la fête des pains sans levain. Pendant 7 jours, au temps fixé<!--Ge. 1:14.--> du mois des épis, tu mangeras des pains sans levain, comme je t'en ai donné l'ordre. Car c'est dans le mois des épis que tu es sorti du pays d'Égypte.
34:19	Tout ce qui ouvre le premier la matrice m'appartient. Même le premier-né mâle de toutes les bêtes, tant du gros que du menu bétail.
34:20	Mais tu rachèteras avec un agneau ou un chevreau le premier-né d'un âne. Si tu ne le rachètes pas, tu lui couperas le cou. Tu rachèteras tout premier-né de tes fils. Personne ne se présentera devant ma face à vide.
34:21	Tu travailleras 6 jours, mais au septième tu te reposeras. Tu te reposeras au temps du labourage et de la moisson.
34:22	Tu feras la fête des semaines, des prémices de la moisson du froment et la fête de la récolte à la fin de l'année.
34:23	Trois fois par année, tout mâle d'entre vous comparaîtra devant le Seigneur YHWH, l'Elohîm d'Israël.
34:24	Car je déposséderai les nations de devant toi et j'étendrai tes limites, et personne ne convoitera ton pays lorsque tu monteras pour comparaître trois fois par année devant YHWH, ton Elohîm.
34:25	Tu n'offriras pas le sang de mon sacrifice avec du pain levé et l'on ne gardera rien du sacrifice de la fête de la Pâque jusqu'au matin.
34:26	Tu apporteras les prémices des premiers fruits de la terre dans la maison de YHWH, ton Elohîm. Tu ne feras pas cuire le chevreau dans le lait de sa mère.
34:27	YHWH dit aussi à Moshè : Écris ces paroles, car suivant la teneur de ces paroles, j'ai traité alliance avec toi et avec Israël.
34:28	Et Moshè demeura là avec YHWH 40 jours et 40 nuits, sans manger de pain et sans boire d'eau. YHWH écrivit sur les tablettes les paroles de l'alliance, c'est-à-dire les dix paroles.

### La gloire de YHWH sur le visage de Moshè

34:29	Or il arriva que lorsque Moshè descendait de la montagne de Sinaï, tenant dans sa main les deux tablettes du Témoignage, lorsqu'il descendait de la montagne, il ne s'aperçût pas que la peau de son visage rayonnait<!--« Briller », « avoir des cornes », « être cornu ».--> pendant qu'il parlait avec Elohîm.
34:30	Mais lorsque Aaron et tous les enfants d'Israël virent Moshè, voici, la peau de son visage rayonnait, et ils eurent peur de s'approcher de lui.
34:31	Mais Moshè les appela. Aaron et tous les princes de l'assemblée revinrent vers lui et Moshè parla avec eux.
34:32	Après quoi, tous les enfants d'Israël s'approchèrent, et il leur ordonna toutes les choses que YHWH lui avait dites sur la montagne de Sinaï.
34:33	Lorsque Moshè eut achevé de leur parler, il mit un voile<!--2 Co. 3:13.--> sur son visage.
34:34	Quand Moshè entrait vers YHWH pour parler avec lui, il ôtait le voile jusqu'à ce qu'il sorte. En sortant, il disait aux enfants d'Israël ce qui lui avait été ordonné.
34:35	Or les enfants d'Israël avaient vu que le visage de Moshè, la peau de son visage rayonnait. C'est pourquoi Moshè remettait le voile sur son visage, jusqu'à ce qu'il entre pour parler avec YHWH.

## Chapitre 35

### Rappels sur le shabbat

35:1	Moshè donc réunit toute l'assemblée des fils d'Israël et leur dit : Voici les choses que YHWH a ordonné de faire.
35:2	Pendant 6 jours le travail se fera, mais le septième jour sera saint pour vous, car c'est le shabbat, le jour du repos, consacré à YHWH. Quiconque fera un travail ce jour-là sera puni de mort.
35:3	Vous n'allumerez pas de feu dans aucune de vos demeures le jour du repos.

### Les offrandes pour le tabernacle<!--Ex. 25:1-8.-->

35:4	Puis Moshè parla à toute l'assemblée des fils d'Israël et leur dit : Voici ce que YHWH vous a ordonné, en disant :
35:5	Prenez de ce que vous avez, une offrande pour YHWH. Toute personne dont le cœur est bien disposé apportera cette offrande pour YHWH : de l'or, de l'argent, du cuivre<!--Ex. 25:2 ; 2 Co. 8:12.-->,
35:6	des étoffes teintes en violet, en pourpre rouge, en écarlate de cochenille, du fin lin, du poil de chèvre,
35:7	des peaux de béliers teintes en rouge, des peaux de taissons, du bois d'acacia,
35:8	de l'huile pour le chandelier, des aromates pour l'huile d'onction et pour l'encens aromatique,
35:9	des pierres d'onyx et des pierres pour la garniture de l'éphod et pour le pectoral.
35:10	Et tous les sages de cœur parmi vous, viendront et feront tout ce que YHWH a ordonné :
35:11	le tabernacle, sa tente et sa couverture, ses agrafes, ses planches, ses barres, ses colonnes et ses bases,
35:12	l'arche et ses barres, le propitiatoire et le voile qui sert de rideau,
35:13	la table et ses barres, et tous ses ustensiles, et le pain de proposition ;
35:14	et le chandelier du luminaire, ses ustensiles, ses lampes et l'huile du luminaire,
35:15	l'autel de l'encens et ses barres, l'huile d'onction et l'encens aromatique, le rideau de la porte pour l'entrée du tabernacle,
35:16	l'autel de l'holocauste, sa grille de cuivre, ses barres et tous ses ustensiles, la cuve avec sa base,
35:17	les rideaux du parvis, ses colonnes, ses bases et le rideau de la porte du parvis,
35:18	les pieux du tabernacle, les pieux du parvis et leur cordage,
35:19	les vêtements en ouvrage tressé pour le service dans le lieu saint, les vêtements sacrés d'Aaron le prêtre, et les vêtements de ses fils pour exercer la prêtrise.
35:20	Alors toute l'assemblée des enfants d'Israël sortit de la présence de Moshè.
35:21	Et tous les hommes dont le cœur était bien disposé et l'esprit généreux, apportèrent une offrande à YHWH pour l'ouvrage de la tente d'assignation, pour tout son service et pour les vêtements sacrés.
35:22	Et les hommes vinrent avec les femmes. Tous ceux qui avaient un cœur bien disposé, apportèrent des boucles, des bagues, des anneaux, des bracelets et des joyaux d'or ; et chacun présenta quelque offrande d'or à YHWH.
35:23	Tout homme aussi chez qui se trouvait des étoffes teintes en violet, en pourpre rouge, en écarlate de cochenille, du fin lin, du poil de chèvre, des peaux de béliers teintes en rouge et des peaux de taissons, les apporta.
35:24	Tous ceux qui avaient de quoi faire une offrande d'argent ou de cuivre, apportèrent l'offrande à YHWH. Tous ceux qui avaient du bois d'acacia pour tout l'ouvrage du service, l'apportèrent.
35:25	Et toutes les femmes sages de cœur filèrent de leurs mains et apportèrent ce qu'elles avaient filé : des fils en violet, pourpre rouge, en écarlate de cochenille et du fin lin<!--Pr. 31:19.-->.
35:26	Toutes les femmes aussi dont le cœur les porta à travailler avec habileté, filèrent du poil de chèvre.
35:27	Aussi, les princes apportèrent des pierres d'onyx et d'autres pierres pour la garniture de l'éphod et du pectoral,
35:28	et des aromates, et de l'huile tant pour le chandelier que pour l'huile d'onction, et pour l'encens aromatique.
35:29	Tout homme donc et toute femme que le cœur incita à la libéralité pour apporter de quoi faire l'ouvrage que YHWH avait ordonné par le moyen de Moshè, tous les enfants d'Israël apportèrent volontairement des présents à YHWH.

### Betsaleel et Oholiab oints pour l'œuvre du tabernacle

35:30	Alors Moshè dit aux fils d'Israël : Voyez, YHWH a appelé par son nom Betsaleel, fils d'Ouri, fils de Hour, de la tribu de Yéhouda.
35:31	Et il l'a rempli de l'Esprit d'Elohîm, de sagesse, d'intelligence, de connaissance, pour toutes sortes d'ouvrages :
35:32	afin de concevoir des inventions, pour travailler l'or, l'argent et le cuivre,
35:33	pour tailler les pierres à enchâsser, pour tailler le bois afin de faire toutes sortes d'ouvrages d'ingénieur.
35:34	Et il lui a mis aussi au cœur, tant à lui qu'à Oholiab, fils d'Ahisamac, de la tribu de Dan, de l'enseigner.
35:35	Et il a rempli leur cœur de sagesse pour faire toutes sortes de travaux de gravure et d'artisanat, pour broder et tisser les étoffes teintes en violet, en pourpre rouge, en écarlate de cochenille et le fin lin, pour faire toutes sortes de travaux et pour concevoir toutes sortes d'inventions<!--Es. 28:26.-->.

## Chapitre 36

### Construction du tabernacle d'après le modèle donné par YHWH<!--Ex. 36-39.-->

36:1	Et Betsaleel et Oholiab, et tous les hommes au cœur sage, auxquels YHWH avait donné de la sagesse et de l'intelligence pour savoir faire tout l'ouvrage du service du lieu saint, firent selon toutes les choses que YHWH avait ordonnées.
36:2	Moshè donc appela Betsaleel et Oholiab, et tous les hommes d'esprit, dans le cœur desquels YHWH avait mis de la sagesse, et tous ceux qui furent émus en leur cœur de se présenter pour faire cet ouvrage ;
36:3	lesquels emportèrent de devant Moshè toute l'offrande que les enfants d'Israël avaient apportée pour faire l'ouvrage du service du lieu saint. Or on apportait encore chaque matin quelques offrandes volontaires.
36:4	C'est pourquoi, tous les hommes sages qui faisaient tout l'ouvrage du lieu saint, vinrent chacun de l'ouvrage qu'ils faisaient,
36:5	et parlèrent à Moshè, en disant : Le peuple ne cesse d'apporter plus qu'il ne faut pour le service et pour l'ouvrage que YHWH a ordonné de faire.
36:6	Alors, sur l'ordre de Moshè, on fit dans le camp cette proclamation : Que personne, ni homme ni femme, ne fasse plus d'ouvrage pour l'offrande du lieu saint. On empêcha ainsi le peuple d'en offrir.
36:7	Car ils avaient assez d'affaires pour tout l'ouvrage à faire, et il y en avait même en surplus.

### Les tapis de fin lin retors

36:8	Tous les hommes donc au cœur sage d'entre ceux qui faisaient l'ouvrage, firent le tabernacle avec 10 tapis de fin lin retors, de fil violet, de pourpre rouge et d'écarlate de cochenille ; et ils y firent des chérubins, ouvrage d'ingénieur.
36:9	La longueur d'un tapis était de 28 coudées, et la largeur du même tapis de 4 coudées. Tous les tapis avaient une même mesure<!--Ex. 26:1-6.-->.
36:10	Et ils joignirent 5 tapis l'un à l'autre, et 5 autres tapis l'un à l'autre.
36:11	Et ils firent des lacets d'étoffe violette sur le bord d'un tapis, au bord de celui qui était attaché. Ils en firent ainsi au bord du dernier tapis dans l'assemblage de l'autre.
36:12	Ils firent 50 lacets à un tapis, et 50 lacets au bord du tapis qui était dans l'assemblage de l'autre. Les lacets étant vis-à-vis l'un de l'autre.
36:13	Puis on fit 50 agrafes en or et on attacha les tapis l'un à l'autre avec les agrafes. Cela devint un seul tabernacle.

### Les tapis de poils de chèvre

36:14	Puis on fit des tapis de poils de chèvre, pour servir de tente au-dessus du tabernacle. On fit 11 de ces tapis.
36:15	La longueur d'un tapis était de 30 coudées, et la largeur du même tapis de 4 coudées. Les 11 tapis étaient d'une même mesure.
36:16	Et on assembla 5 de ces tapis à part, et 6 tapis à part.
36:17	On fit 50 lacets au bord du tapis à l'extrémité d'un assemblage, et on fit 50 lacets au bord du tapis du second assemblage.
36:18	On fit aussi 50 agrafes de cuivre pour assembler la tente afin qu'elle forme un tout.

### Les couvertures de peaux de béliers et de taissons

36:19	Puis, on fit pour la tente une couverture de peaux de béliers teintes en rouge, et une couverture de peaux de taissons par-dessus.

### Les planches et leurs bases

36:20	Et on fit pour le tabernacle des planches en bois d'acacia, qu'on fit tenir debout.
36:21	La longueur d'une planche était de 10 coudées, et la largeur de la même planche d'une coudée et demie.
36:22	Il y avait pour chaque planche 2 tenons, joints l'un à l'autre. On fit la même chose pour toutes les planches du tabernacle.
36:23	On fit donc les planches pour le tabernacle : 20 planches pour le côté sud, vers le midi.
36:24	Et au-dessous des 20 planches, on fit 40 bases en argent, 2 bases sous une planche, pour ses deux tenons, et 2 bases sous l'autre planche, pour ses deux tenons.
36:25	On fit aussi 20 planches pour l'autre côté du tabernacle, du côté nord,
36:26	et leurs 40 bases en argent : 2 bases sous une planche et 2 bases sous l'autre planche.
36:27	Et pour le fond du tabernacle, vers l'occident, on fit 6 planches.
36:28	On fit 2 planches pour les angles du tabernacle aux deux côtés du fond.
36:29	Elles étaient jointes par le bas, et elles étaient jointes ensemble jusqu'au sommet par un anneau. On fit la même chose aux deux planches qui étaient aux deux angles.
36:30	Il y avait donc 8 planches et 16 bases en argent, 2 bases sous chaque planche.
36:31	Puis on fit 5 barres en bois d'acacia pour les planches de l'un des côtés du tabernacle,
36:32	et 5 barres pour les planches de l'autre côté du tabernacle, et 5 barres pour les planches du tabernacle pour le fond, vers le côté de l'occident.
36:33	On fit la barre du milieu pour traverser par le milieu les planches d'extrémité en extrémité.
36:34	Et on couvrit d'or les planches, et on fit leurs anneaux d'or pour y faire passer les barres, et on couvrit d'or les barres.

### Le voile et le rideau extérieur

36:35	On fit aussi le voile en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors. On y fit des chérubins, ouvrage d'ingénieur.
36:36	Et on lui fit 4 colonnes en bois d'acacia, qu'on couvrit d'or, ayant leurs crochets d'or. On fondit pour elles 4 bases d'argent.
36:37	On fit aussi à l'entrée de la tente un rideau en étoffe violette, en pourpre rouge, en écarlate de cochenille, en fin lin retors et en ouvrage de broderie ;
36:38	et ses 5 colonnes avec leurs crochets. On couvrit d'or leurs chapiteaux et leurs filets, mais leurs 5 bases étaient en cuivre.

## Chapitre 37

### L'arche de l'Alliance

37:1	Puis Betsaleel fit l'arche en bois d'acacia. Sa longueur était de 2 coudées et demie, et sa largeur d'une coudée et demie, et sa hauteur d'une coudée et demie<!--Ex. 23:10-31.-->.
37:2	Et il la couvrit d'or pur à l'intérieur et à l'extérieur et lui fit un couronnement d'or tout autour.
37:3	Et il fondit pour elle 4 anneaux en or pour les mettre sur ses 4 coins : 2 anneaux à l'un de ses côtés, et 2 autres à l'autre côté.
37:4	Et il fit aussi des barres en bois d'acacia et les couvrit d'or.
37:5	Et il fit entrer les barres dans les anneaux aux côtés de l'arche, pour porter l'arche.

### Le propitiatoire

37:6	Il fit aussi le propitiatoire en or pur ; sa longueur était de 2 coudées et demie, et sa largeur d'une coudée et demie.
37:7	Et il fit 2 chérubins d'or. Il les fit d'ouvrage étendu au marteau, tirés des 2 extrémités du propitiatoire :
37:8	un chérubin à une extrémité, l'autre chérubin à l'autre extrémité. Il fit sortir les chérubins du propitiatoire à ses 2 extrémités.
37:9	Et les chérubins étendaient leurs ailes en haut, couvrant de leurs ailes le propitiatoire. Leurs faces étaient vis-à-vis l'une de l'autre, et les chérubins regardaient vers le propitiatoire.

### La table des pains de proposition

37:10	Il fit aussi la table en bois d'acacia. Sa longueur était de 2 coudées, sa largeur d'une coudée et sa hauteur d'une coudée et demie.
37:11	Et il la couvrit d'or pur et lui fit un couronnement d'or tout autour.
37:12	Il lui fit aussi à l'entour un rebord d'une largeur d'une paume, et à l'entour de sa bordure un couronnement d'or.
37:13	Et il fondit pour elle 4 anneaux d'or et il mit les anneaux aux 4 coins, qui étaient à ses 4 pieds.
37:14	Les anneaux étaient à côté du rebord, pour y mettre les barres afin de porter la table avec elles.
37:15	Et il fit les barres en bois d'acacia et les couvrit d'or, pour porter la table.
37:16	Il fit en or pur les ustensiles qu'on devait mettre sur la table, les plats, les tasses, les bassins et les gobelets, avec lesquels on devait faire les aspersions.

### Le chandelier

37:17	Il fit aussi le chandelier en or pur. Il le fit en ouvrage martelé ; sa tige, ses branches, ses coupes, ses boutons et ses fleurs sortaient de lui.
37:18	Et 6 branches sortaient de ses côtés, 3 branches d'un côté du chandelier, et 3 de l'autre côté du chandelier.
37:19	Il y avait sur l'une des branches 3 coupes en forme d'amande, un bouton et une fleur, et sur l'autre branche 3 plats en forme d'amande, un bouton et une fleur ; il fit la même chose aux 6 branches qui sortaient du chandelier.
37:20	Il y avait sur le chandelier 4 coupes en forme d'amande, ses boutons et ses fleurs :
37:21	Il y avait un bouton sous 2 branches qui sortaient de lui, un bouton sous 2 autres branches et un bouton sous 2 autres branches ; il en allait de même pour les 6 branches sortant du chandelier.
37:22	Ses boutons et ses branches ne faisaient qu'un avec lui. Le tout était en ouvrage martelé d'or pur.
37:23	Il fit aussi ses 7 lampes, ses mouchettes et ses encensoirs d'or pur.
37:24	Et il le fit avec toute sa garniture, d'un talent d'or pur.

### L'autel des parfums ou de l'encens

37:25	Il fit aussi en bois d'acacia l'autel de l'encens. Sa longueur était d'une coudée, et sa largeur d'une coudée. Il était carré, et sa hauteur était de 2 coudées, et ses cornes procédaient de lui<!--Ex. 30:1-10.-->.
37:26	Et il couvrit d'or pur le dessus de l'autel, ses côtés tout autour et ses cornes. Il fit tout autour une bordure en or.
37:27	Il fit aussi au-dessous de sa bordure 2 anneaux d'or à ses 2 côtés, lesquels il mit aux 2 coins, pour y faire passer les barres afin de le porter avec elles.
37:28	Et il fit les barres en bois d'acacia et les couvrit d'or.

### L'huile d'onction et l'encens

37:29	Il fit aussi l'huile pour l'onction sainte et l'encens aromatique et pur ; c'était un ouvrage de parfumeur.

## Chapitre 38

### L'autel des holocaustes

38:1	Il fit aussi en bois d'acacia l'autel des holocaustes. Et sa longueur était de 5 coudées, et sa largeur de 5 coudées. Il était carré, et sa hauteur était de 3 coudées<!--Ex. 27:1-8.-->.
38:2	Et il fit ses cornes à ses 4 coins. Ses cornes sortaient de lui, et il le couvrit de cuivre.
38:3	Il fit aussi tous les ustensiles de l'autel : les chaudrons, les racloirs, les bassins, les fourchettes et les encensoirs. Il fit tous ses ustensiles en cuivre.
38:4	Et il fit pour l'autel une grille de cuivre en forme de treillis, au-dessous de l'enceinte de l'autel, depuis le bas jusqu'au milieu.
38:5	Et il fondit 4 anneaux aux 4 coins de la grille de cuivre, pour mettre les barres.
38:6	Et il fit les barres en bois d'acacia et les couvrit de cuivre.
38:7	Et il fit passer les barres dans les anneaux, aux côtés de l'autel, pour le porter avec elles. Il le fit creux, avec des planches.

### La cuve en cuivre

38:8	Il fit aussi la cuve en cuivre et sa base en cuivre, avec les miroirs des femmes qui se rassemblaient à l'entrée de la tente d'assignation<!--Ex. 30:14-18.-->.

### Le parvis

38:9	Il fit aussi le parvis. Du côté du sud, au midi, les rideaux du parvis, en fin lin retors, avaient 100 coudées.
38:10	Il fit en cuivre leurs 20 colonnes avec leurs 20 bases, mais les crochets des colonnes et leurs filets étaient en argent.
38:11	Et pour le côté nord, il fit des rideaux de 100 coudées, leurs 20 colonnes et leurs 20 bases étaient en cuivre, mais les crochets des colonnes et leurs filets étaient en argent.
38:12	Pour le côté de l'occident, des rideaux de 50 coudées, leurs 10 colonnes et leurs 10 bases. Les crochets des colonnes et leurs filets étaient en argent.
38:13	Pour le côté de l'orient droit vers le levant, des rideaux de 50 coudées.
38:14	Il fit pour l'un des côtés, 15 coudées de rideaux et leurs 3 colonnes avec leurs 3 bases.
38:15	Et pour l'autre côté, 15 coudées de rideaux, afin qu'il y en ait autant de part et d'autre de la porte du parvis, et leurs 3 colonnes avec leurs 3 bases.
38:16	Il fit donc tous les rideaux du parvis qui étaient tout autour en fin lin retors.
38:17	Les bases pour les colonnes étaient en cuivre, les crochets des colonnes et les filets étaient en argent, et leurs chapiteaux furent couverts d'argent. Toutes les colonnes du parvis furent ceintes tout autour d'un filet d'argent.

### La porte du parvis

38:18	Et il fit le rideau de la porte du parvis en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, en ouvrage de broderie, de la longueur de 20 coudées et de la hauteur qui était comme la largeur de 5 coudées, correspondant aux rideaux du parvis ;
38:19	et ses 4 colonnes avec leurs bases de cuivre et leurs crochets d'argent, la couverture aussi de leurs chapiteaux et leurs filets d'argent ;
38:20	et tous les pieux du tabernacle et du parvis tout autour en cuivre.

### Les comptes du tabernacle

38:21	Voici le compte des choses qui furent employées pour le tabernacle, pour le tabernacle du témoignage, compte dressé sur l'ordre de Moshè par les soins des Lévites, sous la conduite d'Ithamar, fils du prêtre Aaron.
38:22	Et Betsaleel, fils d'Ouri, fils de Hour, de la tribu de Yéhouda, fit toutes les choses que YHWH avait ordonnées à Moshè.
38:23	Et avec lui Oholiab, fils d'Ahisamac, de la tribu de Dan. C'était un artisan, un brodeur sur les étoffes en violet, en pourpre rouge, en écarlate de cochenille et sur le fin lin.
38:24	Tout l'or utilisé pour l'ouvrage, pour tout l'ouvrage du lieu saint, l'or des offrandes, fut de 29 talents et 730 sicles, selon le sicle du lieu saint.
38:25	Et l'argent de ceux de l'assemblée qui furent dénombrés fut de 100 talents et 1 775 sicles, selon le sicle du lieu saint.
38:26	Un demi-sicle par tête, la moitié d'un sicle selon le sicle du lieu saint. Tous ceux qui passèrent par le dénombrement depuis l'âge de 20 ans et au-dessus, furent 603 550.
38:27	Il y eut donc 100 talents d'argent pour fondre les bases du lieu saint et les bases du voile : 100 bases de 100 talents, un talent pour chaque base.
38:28	Mais des 1 775 sicles, il fit les crochets pour les colonnes et il couvrit leurs chapiteaux, et en fit des filets tout autour.
38:29	Le cuivre des offrandes fut de 70 talents et 2 400 sicles.
38:30	On en fit les bases de la porte de la tente d'assignation, l'autel de cuivre avec sa grille en cuivre et tous les ustensiles de l'autel,
38:31	et les bases tout autour du parvis, les bases de la porte du parvis, et tous les pieux du tabernacle et tous les pieux du parvis tout autour.

## Chapitre 39

### Les vêtements sacrés d'Aaron

39:1	Ils firent aussi les étoffes teintes en violet, en pourpre rouge et en écarlate de cochenille, les vêtements en ouvrage tressé pour le service dans le lieu saint. Et ils firent les vêtements sacrés pour Aaron, comme YHWH l'avait ordonné à Moshè<!--Ex. 28.-->.
39:2	On fit donc l'éphod en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors.
39:3	Or, on étendit des lames d'or et on les coupa par filets pour les entrelacer dans les étoffes teintes en violet, en pourpre rouge, en écarlate de cochenille et dans le fin lin, ouvrage d'ingénieur.
39:4	On fit à l'éphod des épaulettes<!--Voir annexe « Les vêtements du grand-prêtre ».--> qui s'attachaient, en sorte qu'il était joint par ses deux extrémités.
39:5	Et la ceinture de l'éphod, celle qui est dessus, était de travail identique : en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors, comme YHWH l'avait ordonné à Moshè.
39:6	On enchâssa aussi les pierres d'onyx dans leurs montures d'or, ayant les noms des enfants d'Israël gravés, comme on grave les sceaux.
39:7	Et on les mit sur les épaulettes de l'éphod, afin qu'elles soient des pierres de souvenir pour les enfants d'Israël, comme YHWH l'avait ordonné à Moshè.
39:8	On fit aussi le pectoral<!--Voir annexe « Les vêtements du grand-prêtre ».--> d'ouvrage d'ingénieur, comme l'ouvrage de l'éphod, en or, en étoffe violette, en pourpre rouge, en écarlate de cochenille et en fin lin retors.
39:9	On fit le pectoral carré et double. Sa longueur était d'une paume, et sa largeur d'une paume de part et d'autre.
39:10	Et on le garnit de 4 rangs de pierres : à la première rangée on mit une sardoine, une topaze et une émeraude.
39:11	À la seconde rangée, une escarboucle, un saphir et un diamant<!--Certains traduisent l'hébreu « yahalom » par jaspe.-->.
39:12	À la troisième rangée, une opale, une agate et une améthyste.
39:13	À la quatrième rangée, une chrysolithe, un onyx et un jaspe<!--Certains traduisent par béryl.-->, enchâssés dans leur monture d'or.
39:14	Ainsi, il y avait autant de pierres qu'il y avait de noms des enfants d'Israël, 12 selon leurs noms, chacune d'elles gravée comme des sceaux, selon le nom qu'elle devait porter, et elles étaient pour les 12 tribus.
39:15	Et on fit sur le pectoral des chaînes en or pur, tressées en forme de cordons.
39:16	On fit aussi 2 montures en or et 2 anneaux en or et on mit les 2 anneaux aux 2 extrémités du pectoral.
39:17	Et on mit les 2 chaînettes d'or faites à cordon dans les 2 anneaux à l'extrémité du pectoral.
39:18	Et on mit les 2 autres bouts des 2 chaînettes faites à cordon aux 2 montures, sur les épaulettes de l'éphod, sur le devant de l'éphod.
39:19	On fit aussi 2 autres anneaux en or et on les mit aux 2 autres extrémités du pectoral sur son bord, qui était du côté de l'éphod à l'intérieur.
39:20	On fit aussi 2 autres anneaux d'or et on les mit aux 2 épaulettes de l'éphod par le bas, répondant sur le devant de l'éphod, à l'endroit où il se joignait au-dessus de la ceinture de l'éphod.
39:21	Et on joignit le pectoral élevé par ses anneaux, aux anneaux de l'éphod, avec un cordon d'étoffe violette afin qu'il tienne au-dessus de la ceinture de l'éphod, et que le pectoral ne bouge de dessus l'éphod, comme YHWH l'avait ordonné à Moshè.
39:22	On fit aussi la robe de l'éphod d'ouvrage tissé et entièrement d'étoffe violette :
39:23	Et l'ouverture pour passer la tête était au milieu de la robe, comme l'ouverture d'une cotte de mailles. Il y avait un ourlet à l'ouverture de la robe tout autour afin qu'elle ne se déchire pas.
39:24	Aux bordures de la robe, on fit des grenades en étoffe violette, en pourpre rouge, en écarlate de cochenille, en fil retors.
39:25	On fit aussi des clochettes en or pur. On mit les clochettes entre les grenades aux bordures de la robe tout autour, parmi les grenades :
39:26	une clochette puis une grenade, une clochette puis une grenade, sur la bordure de la robe tout autour, pour faire le service, comme YHWH l'avait ordonné à Moshè.
39:27	On fit aussi à Aaron et à ses fils, des tuniques en fin lin, d'ouvrage tissé.
39:28	La tiare en fin lin, les parures des turbans en fin lin, et les caleçons en lin, en fin lin retors.
39:29	Et la ceinture en fin lin retors, en violet, en pourpre rouge, en écarlate de cochenille, en ouvrage de broderie, comme YHWH l'avait ordonné à Moshè.
39:30	Et la lame du saint diadème en or pur, sur laquelle on écrivit comme on grave un sceau : Sainteté à YHWH.
39:31	Et on mit sur elle un cordon d'étoffe violette pour l'appliquer à la tiare par-dessus, comme YHWH l'avait ordonné à Moshè.

### Le matériel pour exercer la prêtrise est prêt

39:32	Ainsi fut achevé tout l'ouvrage du tabernacle, de la tente d'assignation. Les enfants d'Israël firent selon toutes les choses que YHWH avait ordonnées à Moshè ; ils les firent ainsi.
39:33	Et ils apportèrent à Moshè le tabernacle, la tente et tous ses ustensiles, ses crochets, ses planches, ses barres, ses colonnes et ses bases ;
39:34	la couverture de peaux de béliers teintes en rouge, la couverture de peaux de taissons et le voile qui sert de rideau ;
39:35	l'arche du témoignage et ses barres, et le propitiatoire ;
39:36	la table avec tous ses ustensiles, et les pains de proposition<!--Ex. 31:8-10.--> ;
39:37	et le chandelier d'or pur avec toutes ses lampes arrangées, et tous ses ustensiles, et l'huile du chandelier ;
39:38	et l'autel d'or, l'huile d'onction, l'encens aromatique, et le rideau de l'entrée de la tente ;
39:39	l'autel de cuivre avec sa grille de cuivre, ses barres et tous ses ustensiles, la cuve et sa base,
39:40	les rideaux du parvis, ses colonnes, ses bases, le rideau pour la porte du parvis, son cordage, ses pieux, et tous les ustensiles pour le service du tabernacle, pour la tente d'assignation ;
39:41	les vêtements en ouvrage tressé pour le service du lieu saint, les vêtements sacrés pour le prêtre Aaron, et les vêtements de ses fils pour exercer la prêtrise.
39:42	Les enfants d'Israël donc firent tout l'ouvrage, comme YHWH l'avait ordonné à Moshè.
39:43	Et Moshè vit tout l'ouvrage, et voici, on l'avait fait ainsi que YHWH l'avait ordonné, on l'avait fait ainsi. Et Moshè les bénit.

## Chapitre 40

### Moshè dresse le tabernacle

40:1	Et YHWH parla à Moshè, en disant :
40:2	Au premier jour du premier mois, tu dresseras le tabernacle de la tente d'assignation.
40:3	Et tu y mettras l'arche du témoignage, au-devant de laquelle tu tendras le voile.
40:4	Puis tu apporteras la table et y arrangeras ce qui doit y être arrangé. Tu apporteras aussi le chandelier et allumeras ses lampes.
40:5	Tu mettras aussi l'autel d'or pour l'encens devant l'arche du témoignage, et tu mettras le rideau à l'entrée du tabernacle.
40:6	Tu mettras aussi l'autel de l'holocauste vis-à-vis de l'entrée du tabernacle de la tente d'assignation.
40:7	Tu mettras aussi la cuve entre la tente d'assignation et l'autel, et y mettras de l'eau.
40:8	Tu mettras aussi le parvis tout autour, et tu mettras le rideau à la porte du parvis.
40:9	Tu prendras aussi l'huile de l'onction, et tu en oindras le tabernacle et tout ce qui y est, et tu le sanctifieras avec tous ses ustensiles ; et il sera saint.
40:10	Tu oindras aussi l'autel de l'holocauste et tous ses ustensiles, tu sanctifieras l'autel et l'autel deviendra saint des saints.
40:11	Tu oindras aussi la cuve et sa base, et la sanctifieras.
40:12	Tu feras aussi approcher Aaron et ses fils à l'entrée de la tente d'assignation, et les laveras avec de l'eau.
40:13	Et tu feras vêtir à Aaron les vêtements sacrés, tu l'oindras et le sanctifieras et il exercera la prêtrise pour moi.
40:14	Tu feras aussi approcher ses fils que tu revêtiras de tuniques.
40:15	Et tu les oindras comme tu auras oint leur père. Ils m'exerceront la prêtrise, et leur onction leur sera pour exercer la prêtrise à toujours parmi leur génération.
40:16	Moshè fit exactement ce que YHWH lui avait ordonné. Ainsi fit-il.
40:17	Car au premier jour du premier mois de la seconde année, le tabernacle fut dressé.
40:18	Moshè donc dressa le tabernacle, mit ses bases, posa ses planches, mit ses barres et dressa ses colonnes.
40:19	Et il étendit la tente sur le tabernacle et mit la couverture de la tente au-dessus du tabernacle par le haut, comme YHWH l'avait ordonné à Moshè.
40:20	Puis il prit et posa le témoignage dans l'arche et mit les barres à l'arche. Il mit aussi le propitiatoire au-dessus de l'arche.
40:21	Et il apporta l'arche dans le tabernacle, et posa le voile qui sert de rideau, et le mit au-devant de l'arche du témoignage, comme YHWH l'avait ordonné à Moshè.
40:22	Il mit aussi la table dans la tente d'assignation, au côté du tabernacle vers le nord, en dehors du voile.
40:23	Et il arrangea sur elle les rangées de pains devant YHWH, comme YHWH l'avait ordonné à Moshè.
40:24	Il mit aussi le chandelier dans la tente d'assignation, vis-à-vis de la table, du côté du tabernacle, vers le sud.
40:25	Et il alluma les lampes devant YHWH, comme YHWH l'avait ordonné à Moshè.
40:26	Il posa aussi l'autel d'or dans la tente d'assignation, devant le voile.
40:27	Et il y fit brûler l'encens aromatique, comme YHWH l'avait ordonné à Moshè.
40:28	Il plaça aussi le rideau à l'entrée du tabernacle.
40:29	Et il mit l'autel de l'holocauste à l'entrée du tabernacle, de la tente d'assignation. Il y offrit l'holocauste et l'offrande, comme YHWH l'avait ordonné à Moshè.
40:30	Et il plaça la cuve entre la tente d'assignation et l'autel, et il y mit de l'eau pour se laver.
40:31	Et Moshè et Aaron avec ses fils s'y lavèrent leurs mains et leurs pieds.
40:32	Et quand ils entraient dans la tente d'assignation, et qu'ils approchaient de l'autel, ils se lavaient, selon que YHWH l'avait ordonné à Moshè.
40:33	Il dressa aussi le parvis tout autour du tabernacle et de l'autel, et tendit le rideau de la porte du parvis. Ainsi Moshè acheva l'ouvrage.

### La gloire de YHWH sur le tabernacle

40:34	Et la nuée couvrit la tente d'assignation et la gloire de YHWH remplit le tabernacle<!--No. 9:15 ; 1 R. 8:10.-->,
40:35	de sorte que Moshè ne pouvait pas entrer dans la tente d'assignation, car la nuée se tenait dessus et la gloire de YHWH remplissait le tabernacle.
40:36	Et tout au long de leur voyage, les enfants d'Israël partaient quand la nuée se levait au-dessus du tabernacle.
40:37	Mais si la nuée ne se levait pas, ils ne partaient pas, jusqu'au jour où elle se levait.
40:38	Car la nuée de YHWH était le jour sur le tabernacle, et le feu y était la nuit, devant les yeux de toute la maison d'Israël, tout au long du voyage.
