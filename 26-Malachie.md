# Malakhi (Malachie) (Mal.)

Signification : Mon messager

Auteur : Malakhi (Malachie)

Thème : Message final de la Première Alliance envers Israël, une nation désobéissante

Date de rédaction : 5ème siècle av. J.-C.

Malakhi exerça son service en Yéhouda (Juda) après la reconstruction du temple et la reprise des cultes. Il annonça la venue du Mashiah et du messager qui devait le précéder, le nouvel Eliyah (Élie) que Yéhoshoua ha Mashiah (Jésus-Christ) reconnut en Yohanan le Baptiste (Jean-Baptiste). Ses écrits mettent en évidence l'importance de l'obéissance à la torah de YHWH.

## Chapitre 1

### Israël, le peuple aimé de YHWH

1:1	La prophétie de la parole de YHWH contre Israël, par le moyen de Malakhi.
1:2	Je vous ai aimés, dit YHWH. Et vous dites : En quoi nous as-tu aimés ? Ésav n'était-il pas le frère de Yaacov ? dit YHWH. Or j'ai aimé Yaacov,
1:3	et j'ai haï Ésav : j'ai mis ses montagnes en désolation, et son héritage aux chacals du désert.
1:4	Si Édom dit : Nous sommes détruits, nous rebâtirons les lieux ruinés ! Ainsi parle YHWH Sabaoth : Ils rebâtiront, mais je détruirai, et on les appellera pays de méchanceté et le peuple contre lequel YHWH est irrité pour toujours.
1:5	Vos yeux le verront, et vous direz : Que YHWH soit glorifié sur les frontières d'Israël !

### Transgression des prêtres après le retour d'exil

1:6	Un fils honore son père, et un serviteur son seigneur. Si donc je suis Père, où est l'honneur qui m'appartient ? Et si je suis Seigneur, où est la crainte qu'on a de moi ? dit YHWH Sabaoth, à vous prêtres, qui méprisez mon Nom, et qui dites : En quoi avons-nous méprisé ton Nom ?
1:7	Vous présentez sur mon autel du pain souillé, et vous dites : En quoi t'avons-nous profané ? C'est en disant : La table de YHWH est méprisable !
1:8	Quand vous présentez une bête aveugle pour la sacrifier, n'est-ce pas mal ? Quand vous en présentez une boiteuse ou malade, n'est-ce pas mal ? Présente-la à ton gouverneur ! T'agréera-t-il, te recevra-t-il favorablement ? dit YHWH Sabaoth.
1:9	Maintenant donc suppliez la face de El, pour qu'il ait pitié de nous ! C'est par vos mains que cela a eu lieu, vous recevra-t-il favorablement ? dit YHWH Sabaoth.
1:10	Lequel de vous fermera les portes pour que vous n'allumiez pas en vain le feu sur mon autel ? Je ne prends aucun plaisir en vous, dit YHWH Sabaoth, et je n'agrée pas l'offrande de vos mains.
1:11	Car depuis le soleil levant jusqu'au soleil couchant, mon Nom est grand parmi les nations, et en tous lieux on brûle de l'encens en l'honneur de mon Nom, et des offrandes pures, car mon Nom est grand parmi les nations, dit YHWH Sabaoth.
1:12	Mais vous, vous le profanez, en disant : La table de YHWH est souillée, et ce qu'elle rapporte est un aliment méprisable.
1:13	Vous dites aussi : Quelle fatigue ! Et vous soufflez dessus, dit YHWH Sabaoth. Vous amenez ce qui a été dérobé, ce qui est boiteux, et malade, ce sont là les offrandes que vous faites ! Accepterai-je cela de vos mains ? dit YHWH.
1:14	C'est pourquoi, maudit soit l'homme trompeur, qui a dans son troupeau un mâle, et qui voue et sacrifie à Adonaï ce qui est corrompu ! Car je suis le Grand Roi, dit YHWH Sabaoth, et mon Nom est redoutable parmi les nations.

## Chapitre 2

### Reproches aux prêtres

2:1	Or maintenant, c'est à vous, prêtres, que s'adresse ce commandement :
2:2	Si vous n'écoutez pas, et que vous ne preniez pas à cœur de donner gloire à mon Nom, dit YHWH Sabaoth, j'enverrai sur vous la malédiction, et je maudirai vos bénédictions. Oui, je les maudirai, parce que vous ne prenez pas cela à cœur.
2:3	Voici, je détruirai vos semences, et je répandrai les excréments de vos victimes sur vos visages, les excréments de vos fêtes, et on vous emportera avec eux.
2:4	Alors vous saurez que je vous ai adressé ce commandement, afin que mon alliance avec Lévi subsiste, dit YHWH Sabaoth.
2:5	Mon alliance avec lui était la vie et la paix, c'est ce que je lui accordai pour qu'il me craigne. Il a eu pour moi de la crainte, et il a tremblé devant mon Nom.
2:6	La torah de la vérité était dans sa bouche, et l'injustice n'a pas été trouvée sur ses lèvres. Il a marché avec moi dans la paix et dans la droiture, et il en a détourné beaucoup de l'iniquité.
2:7	Car les lèvres du prêtre doivent garder la connaissance, et c'est de sa bouche qu'on demande la torah, parce qu'il est un messager de YHWH Sabaoth.
2:8	Mais vous, vous vous êtes détournés de ce chemin, vous en avez fait trébucher beaucoup par le moyen de la torah, et vous avez corrompu l'alliance de Lévi, a dit YHWH Sabaoth.

### Infidélités des prêtres

2:9	C'est pourquoi je vous ai rendus méprisables et vils aux yeux de tout le peuple. Parce que vous n'avez pas gardé mes voies, et vous avez égard à l'apparence des personnes quand vous enseignez la torah.
2:10	N'avons-nous pas tous un seul Père ? N'est-ce pas un seul El qui nous a créés ? Pourquoi donc nous trahissons-nous les uns les autres, en profanant l'alliance de nos pères ?
2:11	Yéhouda s'est montré infidèle, et une abomination a été commise en Israël et à Yeroushalaim. Car Yéhouda a profané ce qui est consacré à YHWH, ce qu'il aime, il s'est marié à la fille d'un el étranger.
2:12	YHWH retranchera l'homme qui fait cela, celui qui veille et qui répond, il le retranchera des tentes de Yaacov, et il retranchera celui qui présente une offrande à YHWH Sabaoth.
2:13	Voici une seconde chose que vous faites : Vous couvrez l'autel de YHWH de larmes, de plaintes et de gémissements, de sorte qu'il ne regarde plus aux offrandes et qu'il ne prend plus plaisir à ce qui vient de vos mains.
2:14	Et vous dites : Pourquoi ? C'est parce que YHWH a été témoin entre toi et la femme de ta jeunesse, contre laquelle tu agis avec tromperie. Et pourtant, elle est ta compagne et la femme de ton alliance.
2:15	Or il y en a eu un seul qui ne l'a pas fait, parce qu'il avait le reste de l'Esprit. Un seul, et pourquoi ? Parce qu'il cherchait la postérité d'Elohîm. Prenez donc garde à votre esprit. Que personne ne trahisse la femme de sa jeunesse !
2:16	Car YHWH, l'Elohîm d'Israël a dit qu'il hait la répudiation. Mais on couvre la violence sous sa robe, a dit YHWH Sabaoth. Prenez donc garde à votre esprit et ne commettez pas cette trahison !

### Fausse profession religieuse

2:17	Vous fatiguez YHWH par vos paroles et vous dites : En quoi l'avons-nous fatigué ? C'est quand vous dites : Quiconque fait le mal plaît à YHWH, et il prend plaisir à de tels gens ! Autrement, où est l'Elohîm du jugement ?

## Chapitre 3

### Prophétie sur la venue de Yohanan le Baptiste

3:1	Voici, j'enverrai mon messager<!--Ce messager, ou Eliyah le prophète, est Yohanan le Baptiste (Es. 40:1-3 ; Mt. 3:1-15, 11:14, 17:10-13 ; Mc. 1:1-8, 9:11-13 ; Lu. 1:17, 3:1-5).-->. Il préparera le chemin devant moi. Et soudain entrera dans son temple le Seigneur que vous cherchez, l'Ange de l'Alliance que vous désirez. Voici, il vient, dit YHWH Sabaoth.
3:2	Mais qui pourra soutenir le jour de sa venue ? Qui pourra subsister quand il paraîtra ? Car il sera comme le feu du fondeur et comme la potasse des foulons.
3:3	Il sera assis comme celui qui raffine et purifie l'argent, il purifiera les fils de Lévi, il les épurera comme l'or et l'argent, et ils présenteront à YHWH des offrandes avec justice.
3:4	Alors l'offrande de Yéhouda et de Yeroushalaim sera agréable à YHWH, comme aux anciens jours, comme aux années d'autrefois.
3:5	Je m'approcherai de vous pour le jugement, et je me hâterai de témoigner contre les sorciers et les adultères, contre ceux qui jurent faussement, et contre ceux qui retiennent le salaire du mercenaire, qui oppriment les veuves et les orphelins ou qui détournent l'étranger de son droit, et qui ne me craignent pas, dit YHWH Sabaoth.
3:6	Parce que je suis YHWH et que je n'ai pas changé. À cause de cela, enfants de Yaacov, vous n'avez pas été consumés.

### Hypocrisie d'Israël

3:7	Depuis le temps de vos pères, vous vous êtes détournés de mes statuts, et vous ne les avez pas gardés. Revenez à moi, et je reviendrai à vous, a dit YHWH Sabaoth. Et vous dites : En quoi reviendrons-nous ?
3:8	Un être humain peut-il tromper Elohîm ? Car vous me trompez, et vous dites : En quoi t'avons-nous trompé ? Dans les dîmes et les offrandes.
3:9	Vous êtes maudits, la malédiction est sur vous parce que vous me trompez, vous, toute la nation !
3:10	Apportez toutes les dîmes<!--Il est question ici de la dîme de la dîme que les Lévites donnaient aux prêtres. Cette dîme était rapportée aux magasins ou greniers (Né. 10:35-39), là aussi étaient stockés toutes sortes de trésors. Pour les autres dîmes, voir le commentaire dans De. 14:22-29.--> aux magasins, afin qu'il y ait de la provision dans ma maison. Éprouvez-moi à ce sujet, dit YHWH Sabaoth, si je ne vous ouvre pas les écluses des cieux et si je ne répands pas en votre faveur la bénédiction, jusqu'à ce qu'il n'y ait plus assez de place.
3:11	Et je réprimanderai par amour pour vous celui qui dévore, et il ne vous détruira pas les fruits de la terre, et vos vignes ne seront pas stériles dans la campagne, a dit YHWH Sabaoth.
3:12	Toutes les nations vous diront heureux, car vous serez un pays de délices, dit YHWH Sabaoth.
3:13	Vos paroles sont rudes contre moi, a dit YHWH. Et vous dites : Qu'avons-nous donc dit contre toi ?
3:14	Vous avez dit : C'est en vain que l'on sert Elohîm. Qu'avons-nous gagné à observer ses ordonnances et à marcher dans le deuil devant YHWH Sabaoth ?
3:15	Maintenant nous tenons pour heureux les orgueilleux. Oui, ceux qui pratiquent la méchanceté sont établis. Oui, ils mettent Elohîm à l'épreuve et ils échappent !

### Un reste demeure fidèle à YHWH

3:16	Alors ceux qui craignent YHWH se parlèrent, chacun à son compagnon. YHWH fut attentif et il écouta. Et un livre de souvenir<!--Voir Est. 6:1.--> fut écrit devant lui pour ceux qui craignent YHWH et qui pensent à son Nom.
3:17	Ils seront ma propriété, dit YHWH Sabaoth, au jour que je prépare. Je les épargnerai comme un homme épargne<!--Épargner, pitié, avoir compassion de.--> son fils qui le sert.
3:18	Convertissez-vous donc, et vous verrez la différence qu'il y a entre le juste et le méchant, entre celui qui sert Elohîm et celui qui ne le sert pas.

## Chapitre 4

### Avènement du jour de YHWH

4:1	Car voici, le jour vient, brûlant comme une fournaise. Tous les orgueilleux et tous ceux qui pratiquent la méchanceté deviendront comme du chaume. Ce jour qui vient les embrasera, dit YHWH Sabaoth, il ne leur laissera ni racine ni rameau.
4:2	Mais pour vous qui craignez mon Nom se lèvera le Soleil de justice<!--Lu. 1:78-79.-->, et la guérison sera sous ses ailes. Vous sortirez et vous sauterez comme les veaux d'une étable.
4:3	Et vous foulerez les méchants, car ils seront comme de la cendre sous les plantes de vos pieds, au jour où je ferai mon œuvre, dit YHWH Sabaoth.
4:4	Souvenez-vous de la torah de Moshè, mon serviteur, auquel j'ai prescrit en Horeb, pour tout Israël, des statuts et des ordonnances.

### Venue d'Eliyah avant le jour de YHWH

4:5	Voici, je vous enverrai Eliyah, le prophète<!--Voir commentaire en Mal. 3:1.-->, avant que le jour grand et redoutable de YHWH vienne.
4:6	Il ramènera le cœur des pères à leurs enfants, et le cœur des enfants à leurs pères, de peur que je ne vienne et que je ne frappe la terre, la vouant à une entière destruction.
