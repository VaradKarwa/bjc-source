# Yoel (Joël) (Joë.)

Signification : YHWH est El

Auteur : Yoel (Joël)

Thème : Le jour de YHWH

Date de rédaction : 9ème ou 8ème siècle av. J.-C.

Yoel, fils de Petouël, exerça son service dans le royaume de Yéhouda (Juda). Son message faisait suite à deux fléaux qui s'étaient abattus sur Yéhouda, à savoir une invasion de sauterelles et la sécheresse. Il s'agissait d'un avertissement de YHWH qui appelait le peuple à revenir à lui avec la promesse de le restaurer dans tout ce qu'il avait perdu. Yoel annonça en outre l'effusion de l'Esprit sur toute chair dans un avenir lointain ; prophétie ayant trouvé son accomplissement à la naissance de l'Assemblée lors de la pentecôte.

## Chapitre 1

1:1	La parole de YHWH qui vint à Yoel, fils de Petouël.
1:2	Anciens, écoutez ceci ! Et vous, tous les habitants du pays, prêtez l'oreille ! Est-il arrivé de votre temps ou même du temps de vos pères, une chose comme celle-ci ?
1:3	Faites-en le récit à vos enfants, et que vos enfants le fassent à leurs enfants, et leurs enfants à la génération suivante !

### Désolation après l'invasion des sauterelles

1:4	La sauterelle a dévoré les restes du gazam, le yélek a dévoré les restes de la sauterelle et le hasil a dévoré les restes du yélek.
1:5	Ivrognes, réveillez-vous, et pleurez ! Et vous tous, buveurs de vin, hurlez à cause du vin nouveau, parce qu'il est retranché de votre bouche !
1:6	Car une nation puissante et innombrable est montée contre mon pays. Ses dents sont des dents de lion avec des crocs d'une lionne.
1:7	Elle a fait de mes vignes une dévastation et mis en morceaux mes figuiers. Elle les a dépouillés, elle les a dépouillés et les a jetés par terre. Leurs branches sont devenues toutes blanches.
1:8	Lamente-toi, comme une jeune fille qui se ceint d'un sac pour pleurer le mari de sa jeunesse !
1:9	L'offrande et la libation sont retranchées de la maison de YHWH, et les prêtres qui font le service de YHWH mènent le deuil.
1:10	Les champs sont ravagés, la terre est dans le deuil, parce que le blé est détruit, le vin est tari et l'huile est desséchée.
1:11	Les laboureurs sont confus, les vignerons gémissent à cause du froment et de l'orge, car la moisson des champs est perdue.
1:12	La vigne est desséchée, le figuier languit, le grenadier, le palmier, le pommier et tous les arbres des champs sont desséchés. La joie est desséchée du milieu des fils des humains !
1:13	Prêtres, ceignez-vous et gémissez ! Poussez des cris, vous qui faites le service de l'autel ! Hurlez, vous qui faites le service de mon Elohîm ! Entrez, passez la nuit vêtus de sacs ! Car il est défendu à l'offrande et à la libation d'entrer dans la maison de votre Elohîm.

### Désolation après la sécheresse et la famine

1:14	Sanctifiez le jeûne, proclamez une assemblée solennelle, rassemblez les anciens et tous les habitants du pays dans la maison de YHWH votre Elohîm, et criez à YHWH !
1:15	Hélas ! Quel jour ! Car le jour de YHWH<!--Jour de YHWH : voir commentaire en Za. 14:1.--> est proche : il vient comme un ravage fait par Shaddaï.
1:16	La nourriture n'est-elle pas retranchée sous nos yeux ? Et la joie et l'allégresse de la maison de notre Elohîm ?
1:17	Les semences sont pourries sous leurs mottes, les magasins sont dévastés, les greniers sont renversés parce que le blé a manqué.
1:18	Ô combien ont gémi les bêtes, et dans quelle peine ont été les troupeaux de bœufs, parce qu'ils n'ont pas de pâturage ! Aussi les troupeaux de brebis sont dévastés.
1:19	Ô YHWH, je crierai à toi, car le feu a consumé les pâturages du désert et la flamme a brûlé tous les arbres des champs.
1:20	Même toutes les bêtes des champs crient aussi vers toi, car les canaux d'eau sont à sec et le feu a consumé les pâturages du désert.

## Chapitre 2

### Le jour de YHWH, invasion future

2:1	Sonnez du shofar en Sion et poussez des cris sur la montagne de ma sainteté ! Que tous les habitants de la Terre tremblent ! Car le jour de YHWH<!--Voir commentaire en Za. 14:1.--> vient, car il est proche :
2:2	jour de ténèbres et d'obscurité, jour de nuées et des ténèbres épaisses, il vient comme l'aurore s'étend sur les montagnes. Voici un peuple nombreux et puissant, tel qu'il n'y en a jamais eu, et après lui, il n'y en aura plus, jusqu'aux années d'âge en âge.
2:3	Devant lui est un feu dévorant, et derrière lui une flamme brûlante. Devant lui la Terre était comme le jardin d'Éden, et derrière lui, la désolation d'un désert. Et rien ne lui échappe.
2:4	Leur aspect est comme l'aspect des chevaux, et ils courent comme des cavaliers.
2:5	C'est comme un bruit de chars bondissant sur les sommets des montagnes, comme le bruit d'une flamme de feu qui dévore le chaume. C'est comme un peuple puissant rangé en ordre de bataille.
2:6	Les peuples tremblent en le voyant, tous les visages en deviennent pâles et livides.
2:7	Ils courent comme des hommes vaillants, ils montent sur les murailles comme des gens de guerre. Chacun va son chemin sans se détourner de son sentier.
2:8	Ils ne se pressent pas les uns les autres, chacun va son chemin. Ils se jettent au travers des épées sans être blessés.
2:9	Ils courent çà et là dans la ville, se précipitent sur les murailles, montent sur les maisons, entrent par les fenêtres comme le voleur.
2:10	La Terre tremble devant eux, les cieux sont ébranlés, le soleil et la lune s'obscurcissent<!--Joë. 3:15, 2:31-32 ; Es. 13:10 ; Mt. 24:29 ; Ap. 6:12.-->, et les étoiles retirent leur éclat.
2:11	Aussi YHWH fait entendre sa voix devant son armée, parce que son camp est très grand, car celui qui accomplit sa parole est puissant. Certainement le jour de YHWH est grand et très redoutable. Qui pourra le supporter ?

### Repentance et miséricorde

2:12	Maintenant encore, dit YHWH, revenez à moi de tout votre cœur, avec des jeûnes, avec des pleurs et des lamentations !
2:13	Déchirez vos cœurs et non vos vêtements, et revenez à YHWH, votre Elohîm, car il est compatissant et miséricordieux, lent à la colère et riche en bonté, et il se repent d'avoir affligé.
2:14	Qui sait si YHWH votre Elohîm, ne reviendra pas et ne se repentira pas, et s'il ne laissera pas après lui la bénédiction, des offrandes et des libations ?
2:15	Sonnez du shofar en Sion ! Sanctifiez le jeûne, proclamez une assemblée solennelle !
2:16	Réunissez le peuple, sanctifiez l'assemblée ! Rassemblez les anciens, réunissez les enfants, même les nourrissons à la mamelle ! Que l'époux sorte de sa demeure, et l'épouse de sa chambre nuptiale !
2:17	Que les prêtres qui font le service de YHWH pleurent entre le portique et l'autel, et qu'ils disent : YHWH ! Épargne ton peuple ! Ne livre pas ton héritage à l'insulte, pour qu'il soit dominé par les nations. Pourquoi dirait-on parmi les peuples : Où est leur Elohîm ?

### Promesse de restauration

2:18	Or YHWH est jaloux pour sa terre, et il est ému de compassion envers son peuple.
2:19	YHWH répond et il dit à son peuple : Voici, je vous enverrai du blé, du vin et de l'huile, et vous en serez rassasiés, et je ne vous exposerai plus à l'opprobre parmi les nations.
2:20	J'éloignerai de vous l'armée venue du nord, je la chasserai vers une terre aride et déserte, son avant-garde dans la mer orientale, son arrière-garde dans la mer occidentale. Et sa puanteur montera, et son infection s'élèvera, après avoir fait de grandes choses.
2:21	Terre, n'aie pas peur, sois dans l'allégresse et réjouis-toi, car YHWH fait de grandes choses !
2:22	N'ayez pas peur, bêtes des champs, car les pâturages du désert ont reverdi, et même les arbres portent leur fruit, le figuier et la vigne donnent leurs richesses.
2:23	Et vous, enfants de Sion, soyez dans l'allégresse et réjouissez-vous en YHWH, votre Elohîm, car il vous donnera la pluie selon la justice, et il fera descendre sur vous la pluie, la première pluie<!--Pluie d'automne.--> et la dernière pluie<!--Pluie de printemps, dernière pluie, pluies de mars et avril qui mûrissent les récoltes en orient.-->, comme autrefois.
2:24	Et les aires se rempliront de blé, et les cuves regorgeront de vin et d'huile.
2:25	Ainsi je vous rendrai les années qu'ont dévorées la sauterelle, le yélek, le hasil et le gazam, ma grande armée que j'avais envoyée contre vous.
2:26	Vous mangerez, vous mangerez et vous serez rassasiés, et vous louerez le Nom de YHWH, votre Elohîm, qui aura fait pour vous des choses merveilleuses, et mon peuple ne sera plus jamais dans la confusion.
2:27	Et vous saurez que je suis au milieu d'Israël, que je suis YHWH votre Elohîm, et qu'il n'y en a pas d'autre, et mon peuple ne sera plus jamais dans la confusion.

### La promesse de l'Esprit

2:28	Et il arrivera après cela que je répandrai mon Esprit sur toute chair<!--Cette promesse s'est réalisée en Actes 2 et se réalisera pleinement lors du retour du Mashiah (Christ) en Israël : voir Za. 12:10-14 qui annonce également la repentance nationale d'Israël (Ro. 11:26-27).--> : vos fils et vos filles prophétiseront, vos vieillards rêveront des rêves, et vos jeunes hommes verront des visions.
2:29	Et même en ces jours-là, je répandrai mon Esprit sur les esclaves, hommes et femmes.

### Prodiges précédant le jour de YHWH<!--Es. 13:9-10, 24:21-23 ; Ez. 32:7-10 ; Mt. 24:29-30.-->

2:30	Et je ferai des prodiges dans les cieux et sur la Terre, du sang et du feu, et des colonnes de fumée.
2:31	Le soleil se changera en ténèbres et la lune en sang avant que ne vienne le grand et redoutable jour de YHWH.
2:32	Et il arrivera que quiconque invoquera le Nom de YHWH<!--Cette prophétie fait écho aux propos de Paulos (Paul) en Ro. 10:9-13. Le Nom de YHWH qui nous a été révélé est Yéhoshoua ha Mashiah (Jésus-Christ) (Jn. 17:6,26 ; Ap. 19:13). Ainsi, conformément aux Écrits de la Nouvelle Alliance, quiconque invoquera le Nom du Seigneur Yéhoshoua sera sauvé (Ac. 4:12, 9:21 ; 1 Co. 1:2).--> sera sauvé, car le salut sera sur la montagne de Sion et dans Yeroushalaim, comme l'a dit YHWH, et parmi le reste que YHWH appellera.

## Chapitre 3

### Rétablissement d'Israël et jugements des nations étrangères<!--Es. 11:10-12 ; Jé. 23:5-8 ; Ez. 37:21-28 ; Za. 12:2-3 ; Ac. 15:15-17.-->

3:1	Car voici, en ces jours-là, et en ce temps-là, quand je ramènerai les captifs de Yéhouda et de Yeroushalaim,
3:2	je rassemblerai toutes les nations<!--Elohîm rassemblera les nations dans la vallée de Yehoshaphat (qui signifie « YHWH a jugé ») pour leur jugement. Cette vallée est peut-être celle où le roi Yehoshaphat remporta une grande victoire, avec beaucoup de facilité, sur les Moabites, les Ammonites et les Maonites (2 Ch. 20). Cette vallée s'étend à l'orient de Yeroushalaim, entre la ville et le Mont des Oliviers, et traverse le torrent de Cédron.-->, et je les ferai descendre dans la vallée de Yehoshaphat. Et là, j'entrerai en jugement avec elles à cause de mon peuple, d'Israël, mon héritage qu'elles ont dispersé parmi les nations, et de mon pays qu'elles se sont partagé.
3:3	Elles ont tiré mon peuple au sort, elles ont donné l'enfant pour une prostituée, elles ont vendu la jeune fille pour du vin, et elles ont bu.
3:4	Et vous aussi, Tyr et Sidon, que me voulez-vous ? Et vous tous, districts de Philistie ? Est-ce une récompense que vous me donnez ? Et si vous me récompensez, je ferai retomber votre récompense bien vite et rapidement sur votre tête !
3:5	Car vous avez pris mon argent et mon or, et vous avez emporté dans vos temples mes belles choses désirables.
3:6	Vous avez vendu les enfants de Yéhouda et les enfants de Yeroushalaim aux enfants de Yavan afin de les éloigner de leur territoire.
3:7	Voici, je les réveillerai<!--Le verbe « réveiller » vient de l'hébreu « 'uwr » qui signifie « se réveiller », « éveiller », « être éveillé », « inciter », « veiller », « se lever », « sortir de l'assoupissement », « prendre courage ». YHWH annonce le réveil des Hébreux depuis les nations, d'où ils sont établis. Ce réveil est une prise de conscience qui aboutira au retour à la terre sainte.--> du lieu où vous les avez vendus et je ferai retomber votre récompense sur votre tête.
3:8	Je vendrai donc vos fils et vos filles aux mains des enfants de Yéhouda, et ils les vendront à ceux de Séba qui les transporteront vers une nation éloignée, car YHWH a parlé.
3:9	Publiez ceci parmi les nations : Préparez la guerre ! Réveillez les hommes vaillants ! Qu'ils s'approchent et qu'ils montent, tous les hommes de guerre !
3:10	Forgez des épées de vos hoyaux et des lances de vos serpes ! Et que le faible dise : Je suis fort !
3:11	Hâtez-vous et venez, vous toutes les nations d'alentour, et rassemblez-vous ! Là, ô YHWH, fais descendre tes hommes vaillants !
3:12	Que les nations se réveillent et qu'elles montent à la vallée de Yehoshaphat ! Car là je siégerai pour juger toutes les nations d'alentour.
3:13	Saisissez la faucille, car la moisson est mûre ! Venez et descendez, car le pressoir est plein, les cuves regorgent ! Car leur méchanceté est grande.
3:14	Des multitudes, des multitudes, dans la vallée du jugement, car le jour de YHWH est proche, dans la vallée du jugement.
3:15	Le soleil et la lune s'obscurcissent, et les étoiles retirent leur éclat<!--Voir Joë. 2:10.-->.
3:16	De Sion YHWH rugit, de Yeroushalaim il fait entendre sa voix. Les cieux et la Terre sont ébranlés. Mais YHWH est le refuge pour son peuple et la forteresse<!--Yéhoshoua ha Mashiah (Jésus-Christ) est notre forteresse (commentaire Es. 8:14 ; Ps. 78:35 ; 1 Co. 10:4).--> pour les enfants d'Israël.
3:17	Et vous saurez que je suis YHWH, votre Elohîm, qui habite à Sion, ma sainte montagne. Yeroushalaim sera sainte, et les étrangers n'y passeront plus.

### Restauration finale et pleine bénédiction du royaume

3:18	Et il arrivera en ce jour-là, que le vin ruissellera des montagnes, le lait coulera des collines, il y aura de l'eau dans tous les torrents de Yéhouda. Et une source<!--Yéhoshoua ha Mashiah (Jésus-Christ) est celui qui fait jaillir en nous une source d'eau qui étanche notre soif à jamais et nous donne la vie éternelle (Jé. 2:13, 17:13 ; Ez. 47:1-12 ; Za. 14:8 ; Jn. 4:14 ; Ap. 22:1).--> sortira de la maison de YHWH et arrosera la vallée de Sittim.
3:19	L'Égypte sera dévastée, Édom sera réduit en désert de désolation, à cause de la violence faite aux enfants de Yéhouda, dont ils ont répandu le sang innocent dans leur pays.
3:20	Mais là, Yéhouda sera éternellement habitée, et Yeroushalaim, d'âge en âge.
3:21	Je les purifierai du sang dont je ne les avais pas purifiés. Et YHWH habitera en Sion.
