# 2 Petros (2 Pierre) (2 Pi.)

Signification : Roc, pierre

Auteur : Petros (Pierre)

Thème : Appel à la sainteté et prophétie sur l'apparition de moqueurs et de maîtres corrompus

Date de rédaction : Env. 66 ap. J.-C.

Sans doute écrite à Rome, cette épître, tout comme la première, semble avoir été destinée aux assemblées d'Asie Mineure. Petros (Pierre) y exhorte les chrétiens à être vigilants quant aux faux docteurs et aux nombreuses hérésies. Il insiste sur l'appel et l'élection des chrétiens dont le comportement doit être exemplaire. Il leur rappelle la véracité des écrits prophétiques et les invite à persévérer dans la vie chrétienne.

## Chapitre 1

### Introduction

1:1	Shim’ôn Petros<!--Simon Pierre.-->, esclave et apôtre de Yéhoshoua Mashiah, à vous qui avez reçu en partage une foi du même prix que la nôtre, par la justice de notre Elohîm et Sauveur Yéhoshoua Mashiah<!--Petros (Pierre) affirme ici avec force la divinité de Yéhoshoua Mashiah (Jésus-Christ).--> :
1:2	que la grâce et la paix vous soient multipliées dans la connaissance précise et correcte d'Elohîm et de Yéhoshoua, notre Seigneur !

### Affermir sa vocation et son élection

1:3	Comme sa divine puissance nous a donné tout ce qui regarde la vie et la piété, au moyen de la connaissance précise et correcte de celui qui nous a appelés par sa gloire et par sa vertu,
1:4	par lesquelles nous sont données les grandes et précieuses promesses, afin que par elles vous soyez faits participants de la nature divine, ayant échappé à la corruption qui règne dans le monde par la convoitise ;
1:5	et pour cette même raison, faites tous vos efforts pour fournir à votre foi la vertu, et à la vertu la connaissance,
1:6	et à la connaissance le contrôle de soi, et au contrôle de soi la patience, et à la patience la piété,
1:7	et à la piété l'amour fraternel, et à l'amour fraternel l'amour.
1:8	Car si ces choses sont en vous et y abondent, elles ne vous laisseront pas paresseux ni stériles pour la connaissance précise et correcte de notre Seigneur Yéhoshoua Mashiah.
1:9	Car celui en qui ces choses ne sont pas présentes est aveugle, et ne voit pas de loin, ayant oublié la purification de ses anciens péchés.
1:10	C'est pourquoi, mes frères, efforcez-vous plutôt à affermir votre vocation et votre élection, car en faisant cela, vous ne trébucherez jamais.
1:11	Car c'est ainsi que l'entrée dans le Royaume éternel de notre Seigneur et Sauveur Yéhoshoua Mashiah vous sera richement fournie.

### Autorité du témoignage de Petros (Pierre)

1:12	C'est pourquoi je ne négligerai pas de vous rappeler sans cesse ces choses, quoique vous ayez de la connaissance et que vous soyez fondés dans la vérité présente.
1:13	Mais j'estime qu'il est juste de vous réveiller par des avertissements, aussi longtemps que je suis dans cette tente<!--Tente ou tabernacle. Ces termes sont utilisés comme une belle métaphore du corps humain qui est la demeure de l'esprit humain. Voir Za. 12:1.-->,
1:14	sachant que je quitterai rapidement cette tente, comme notre Seigneur Yéhoshoua Mashiah lui-même me l'a déclaré<!--Voir Jn. 21:19.-->.

### Souvenir de la transfiguration

1:15	Mais je m’efforcerai de faire en sorte qu’après mon départ vous ayez toujours de quoi vous rappeler ces choses.
1:16	Car ce n'est pas en suivant des fables<!--Voir 1 Ti. 1:4 ; 2 Ti. 4:4 ; Tit. 1:14.--> composées avec artifice, que nous vous avons fait connaître la puissance et l'avènement<!--L'avènement du Seigneur. Voir Mt. 24:3.--> de notre Seigneur Yéhoshoua Mashiah, mais comme ayant vu sa grandeur de nos propres yeux.
1:17	Car il a reçu d'Elohîm le Père honneur et gloire, lorsque la voix que voici a été portée jusqu'à lui par la gloire magnifique : Celui-ci est mon Fils bien-aimé, en qui j'ai mis toute mon affection<!--Mt. 3:17.-->.
1:18	Et nous avons entendu cette voix portée depuis le ciel, lorsque nous étions avec lui sur la sainte montagne.

### La parole prophétique

1:19	Nous avons aussi la parole prophétique qui est très ferme, à laquelle vous faites bien d'être attentifs, comme à une lampe qui brille dans un lieu obscur, jusqu'à ce que le jour vienne à paraître et que l'Étoile du matin<!--Yéhoshoua est l'Étoile du matin. C'est cette Étoile qui indiqua aux mages le chemin de la maison où était l'enfant Yéhoshoua (Mt. 2). Balaam a également parlé de cette Étoile (No. 24:17) et Yéhoshoua lui-même s'est présenté comme l'Étoile brillante du matin (Ap. 22:16).--> se lève dans vos cœurs.
1:20	Sachez d'abord ceci : qu'aucune prophétie de l'Écriture ne vient d'une interprétation particulière,
1:21	car la prophétie n'a jamais été autrefois apportée par la volonté humaine, mais c'est portés par le Saint-Esprit que les saints hommes d'Elohîm ont parlé.

## Chapitre 2

### Avertissements contre les faux docteurs

2:1	Mais comme il y a eu de faux prophètes parmi le peuple, il y aura aussi parmi vous de faux docteurs, qui introduiront secrètement des sectes de perdition, et reniant le Seigneur qui les a achetés, attireront sur eux-mêmes une perdition soudaine.
2:2	Et beaucoup les suivront dans leur perdition<!--Le mot grec traduit par perdition signifie aussi destruction.-->, et à cause d'eux, la voie de la vérité sera blasphémée.
2:3	Et, par cupidité, ils trafiqueront de vous au moyen de paroles trompeuses<!--Vient du grec « plastos » qui signifie « moulé, modelé, formé comme dans l'argile, ce qui est feint ou trompeur ».-->, mais la condamnation qui leur est destinée depuis longtemps ne tarde pas, et leur destruction ne sommeille pas.
2:4	Car si Elohîm n'a pas épargné les anges qui avaient péché, mais s'il les a précipités dans le Tartare<!--Tartare. Du grec « tartaroo », ce terme est le nom de la région souterraine, lugubre et sombre, considérée par les Grecs anciens comme la demeure du méchant à sa mort, où il souffre le châtiment pour ses mauvaises actions. « Tartaroo » vient de « tartaros » qui signifie « les plus profonds abîmes du Hadès ».--> où ils sont gardés enchaînés<!--Voir Jud 1:6.--> dans l'obscurité pour le Jugement,
2:5	et s'il n'a pas épargné l'ancien monde, mais s'il a gardé Noah<!--Voir Ge. 7.-->, lui huitième, prédicateur de la justice, lorsqu'il a fait venir le déluge sur un monde d'impies ;
2:6	et s'il a condamné à la destruction totale et réduit en cendres les villes de Sodome et Gomorrhe, les donnant en exemple à ceux qui par la suite vivraient dans l'impiété,
2:7	et s'il a délivré Lot<!--Voir Ge. 18-19.--> le juste, qui était profondément affligé par la luxure sans bride de ces hors-la-loi,
2:8	car ce juste, qui habitait au milieu d'eux, torturait tous les jours son âme juste à cause de ce qu'il voyait et entendait dire de leurs œuvres violeuses de la torah<!--Voir Mc. 15:28.-->.
2:9	Le Seigneur sait comment délivrer de la tentation ceux qui l'honorent et réserver les injustes pour être punis au jour du jugement.
2:10	Mais principalement ceux qui vont après la chair dans un désir d’impureté et qui méprisent la seigneurie. Gens audacieux et arrogants, ils ne craignent pas de blasphémer les gloires,
2:11	alors que des anges, bien qu'ils soient plus grands en force et en puissance, ne prononcent pas contre elles de jugement blasphématoire devant le Seigneur.
2:12	Mais eux, semblables à des bêtes dépourvues de raison, gouvernées par leurs instincts naturels, nées pour être prises et détruites, parlant d'une manière blasphématoire de ce qu'ils ignorent, ils périront par leur propre corruption,
2:13	en recevant la récompense de leur injustice. Trouvant leur plaisir dans une vie de luxe en plein jour. Ce sont des taches et des souillures ; ils vivent dans le luxe et dans leurs propres tromperies quand ils prennent part à vos festins.
2:14	Ayant les yeux pleins d'adultère<!--Ou femme adultère.--> et, incapables d'arrêter de pécher, ils attrapent avec un appât les âmes instables<!--Le mot grec signifie aussi « chancelant, inconstant ». Voir 2 Pi. 3:16.--> ; ils ont le cœur exercé à la cupidité : ce sont des enfants de malédiction.
2:15	Après avoir abandonné le droit chemin, ils se sont égarés en suivant la voie de Balaam<!--Balaam. Voir No. 22.--> de Bosor, qui a aimé le salaire de l'injustice, 
2:16	mais il a reçu une réprimande pour sa transgression : une ânesse muette, parlant d'une voix humaine, a empêché la folie du prophète.
2:17	Ce sont des sources sans eau et des nuées agitées par un tourbillon de vent : l'obscurité des ténèbres leur est réservée pour l'éternité.
2:18	Car en prononçant des discours enflés et dépourvus de vérité et de convenance, ils attrapent avec un appât, par les désirs de la chair<!--Voir Ga. 5:16-21.--> et par leur luxure sans bride, ceux qui ont vraiment échappé aux gens qui vivent dans l'égarement.
2:19	Ils leur promettent la liberté, alors qu'ils sont eux-mêmes esclaves de la corruption, car on est esclave de celui par qui on est vaincu.
2:20	Car, si après avoir échappé aux souillures du monde par la connaissance précise et correcte du Seigneur et Sauveur Yéhoshoua Mashiah, ils s'y engagent de nouveau et sont vaincus par elles, leur dernière condition est pire que la première<!--Mt. 12:43-45.-->.
2:21	Car mieux valait pour eux n'avoir pas connu la voie de la justice, que de l'avoir connue et se détourner du saint commandement qui leur avait été donné.
2:22	Mais ce qu'on dit par un proverbe véritable leur est arrivé : Le chien est retourné à son propre vomissement<!--Pr. 26:11.-->, et la truie lavée est retournée se vautrer dans le bourbier.

## Chapitre 3

3:1	Bien-aimés, voici déjà la seconde lettre que je vous écris. Dans l'une et dans l'autre je réveille par un rappel la pureté de vos pensées,
3:2	afin que vous vous souveniez des paroles qui ont été dites auparavant par les saints prophètes et du commandement que vous avez reçu de nous qui sommes apôtres du Seigneur et Sauveur.

### La promesse de l'avènement du Seigneur

3:3	Sachant avant tout ceci, qu'il viendra, vers la fin des jours<!--Derniers jours. Voir Ge. 49:1-2.-->, des moqueurs se conduisant selon leurs propres désirs,
3:4	et disant : Où est la promesse de son avènement ? Car depuis que les pères sont morts, toutes choses restent permanentes comme depuis le commencement de la création.
3:5	Car ils oublient volontairement que des cieux existèrent autrefois par la parole d'Elohîm, ainsi qu'une terre tirée de l'eau et qui subsistait au moyen de l'eau,
3:6	et que par ces choses le monde d'alors fut détruit, étant submergé par l'eau<!--Voir Ge. 6.-->.
3:7	Mais les cieux et la Terre d'à présent sont gardés par la même parole, étant réservés pour le feu au jour du jugement et de la destruction des humains impies.
3:8	Mais il y a une chose que vous ne devez pas ignorer, bien-aimés, c'est qu'aux yeux du Seigneur un jour est comme 1 000 ans, et 1 000 ans sont comme un jour<!--Ps. 90:4.-->.
3:9	Le Seigneur n'est pas en retard dans l'accomplissement de sa promesse, comme quelques-uns estiment qu'il y a de la lenteur. Mais il est patient envers nous, ne voulant qu'aucun ne périsse, mais que tous arrivent à la repentance.
3:10	Or le jour du Seigneur<!--Le jour du Seigneur. Voir Za. 14:1.--> viendra comme un voleur dans la nuit. Et en ce jour-là, les cieux passeront avec le bruit d'une effroyable tempête, et les éléments seront dissous par l'ardeur du feu, et la Terre avec toutes les œuvres qu'elle renferme sera brûlée entièrement.
3:11	Puisque toutes ces choses seront dissoutes, combien votre conduite et votre piété doivent-elles être saintes,
3:12	en attendant et en hâtant l'avènement du jour d'Elohîm, par lequel les cieux enflammés seront dissous et les éléments se fondront par l'ardeur du feu !
3:13	Mais nous attendons, selon sa promesse, de nouveaux cieux et une nouvelle Terre<!--Es. 66:22.--> où la justice habitera.

### Petros (Pierre) témoigne de Paulos (Paul)

3:14	C'est pourquoi, bien-aimés, en attendant ces choses, efforcez-vous d’être trouvés par lui sans tache et innocents dans la paix.
3:15	Et considérez que la patience du Seigneur est votre salut. Comme Paulos, notre frère bien-aimé vous l'a aussi écrit, selon la sagesse qui lui a été donnée ;
3:16	comme il le fait aussi dans toutes ses lettres, où il parle de ces choses, dans lesquelles il y a des points difficiles à comprendre, que les personnes ignorantes et instables<!--Voir 2 Pi. 2:14.--> tordent<!--Jé. 23:36.-->, comme elles tordent aussi les autres Écritures, pour leur propre destruction.

### Conclusion

3:17	Vous donc mes bien-aimés, puisque vous êtes déjà avertis, prenez garde, de peur qu'entraînés par l'égarement des hors-la-loi, vous ne perdiez votre ferme condition.
3:18	Mais croissez dans la grâce et dans la connaissance de notre Seigneur et Sauveur Yéhoshoua Mashiah. À lui soit la gloire, maintenant et jusqu'au jour d'éternité ! Amen !
