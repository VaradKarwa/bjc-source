# Bamidbar (Nombres) (No.)

Signification : Dans le désert

Auteur : Probablement Moshè (Moïse)

Thème : Pérégrination dans le désert

Date de rédaction : Env. 1450 – 1410 av. J.-C.

Ce livre commence par le recensement des fils d'Israël et relate trente-huit des quarante années qu'ils passèrent dans le désert du Sinaï. Il couvre une période qui s'étend de la deuxième année après la sortie d'Égypte à la veille de l'entrée à Canaan, terre qu'Elohîm avait promis de donner à la descendance d'Abraham. Ce pays où coulaient le lait et le miel s'étendait de Sidon jusqu'à Lesha, en passant par Gaza et Sodome. En plus des Cananéens, il accueillait en son sein des enfants d'Anak, les Amalécites, les Héthiens, les Yebousiens et les Amoréens.

Ces écrits retracent les premières victoires d'Israël et regroupent diverses lois et instructions sur le partage de la terre promise. Ils témoignent également de la révolte et de l'incrédulité de la génération sortie d'Égypte dont la quasi-totalité périt dans le désert.

## Chapitre 1

### Dénombrement des hommes de guerre

1:1	YHWH parla à Moshè dans le désert de Sinaï, dans la tente d'assignation, le premier jour du second mois, la seconde année, après qu'ils furent sortis du pays d'Égypte, en disant :
1:2	Faites le dénombrement de toute l'assemblée des fils d'Israël, selon leurs familles, selon les maisons de leurs pères, en comptant par tête les noms de tous les mâles<!--Ex. 30:12, 38:26.-->.
1:3	Depuis l'âge de 20 ans et au-dessus, tous ceux d'Israël qui peuvent aller à la guerre. Vous les compterez selon leurs armées, toi et Aaron.
1:4	Il y aura avec vous un homme par tribu, celui qui est le chef de la maison de ses pères.
1:5	Voici les noms des hommes qui vous assisteront. Pour la tribu de Reouben : Élitsour, fils de Shedéour ;
1:6	pour celle de Shim’ôn : Sheloumiel, fils de Tsourishaddaï ;
1:7	pour celle de Yéhouda : Nahshôn, fils d'Amminadab ;
1:8	pour celle de Yissakar : Netanél, fils de Tsouar ;
1:9	pour celle de Zebouloun : Éliab, fils de Hélon ;
1:10	pour les fils de Yossef, pour la tribu d'Éphraïm : Éliyshama, fils d'Ammihoud ; pour celle de Menashè : Gamaliel<!--« Récompense de El ». Voir Ac. 5:34, 22:3.-->, fils de Pedahtsour ;
1:11	pour la tribu de Benyamin : Abidan, fils de Guideoni ;
1:12	pour celle de Dan : Ahiézer, fils d'Ammishaddaï ;
1:13	pour celle d'Asher : Paguiel, fils d'Ocran ;
1:14	pour celle de Gad : Éliasaph, fils de Déouel ;
1:15	pour celle de Nephthali : Ahira, fils d'Énan.
1:16	Tels sont ceux qui furent appelés pour tenir l'assemblée, ils étaient les princes des tribus de leurs pères, chefs des milliers d'Israël.
1:17	Alors Moshè et Aaron prirent ces hommes qui avaient été désignés par leurs noms,
1:18	et ils convoquèrent toute l'assemblée, le premier jour du second mois. On les enregistra selon leurs familles et selon la maison de leurs pères, en comptant les noms depuis l'âge de 20 ans et au-dessus, chacun par tête.
1:19	Comme YHWH l'avait commandé à Moshè, il les dénombra dans le désert de Sinaï.
1:20	Les fils donc de Reouben, premier-né d'Israël, selon leurs générations, leurs familles et les maisons de leurs pères, dont on fit le dénombrement par leur nom et par tête, tous les mâles de l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre.
1:21	Ceux de la tribu de Reouben, qui furent dénombrés, furent 46 500.
1:22	Des enfants de Shim’ôn, selon leurs générations, leurs familles et les maisons de leurs pères, ceux qui furent dénombrés par leur nom et par tête, tous les mâles de l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:23	ceux de la tribu de Shim’ôn, qui furent dénombrés, furent 59 300.
1:24	Des fils de Gad, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:25	ceux de la tribu de Gad, qui furent dénombrés, furent 45 650.
1:26	Des enfants de Yéhouda, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:27	ceux de la tribu de Yéhouda, qui furent dénombrés, furent 74 600.
1:28	Des fils de Yissakar, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:29	ceux de la tribu de Yissakar, qui furent dénombrés, furent 54 400.
1:30	Des enfants de Zebouloun, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:31	ceux de la tribu de Zebouloun, qui furent dénombrés, furent 57 400.
1:32	Quant aux fils de Yossef ; les fils d'Éphraïm, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:33	ceux de la tribu d'Éphraïm, qui furent dénombrés, furent 40 500.
1:34	Des fils de Menashè, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:35	ceux de la tribu de Menashè, qui furent dénombrés, furent 32 200.
1:36	Des fils de Benyamin, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:37	ceux de la tribu de Benyamin, qui furent dénombrés, furent 35 400.
1:38	Des fils de Dan, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:39	ceux de la tribu de Dan, qui furent dénombrés, furent 62 700.
1:40	Des fils d'Asher, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:41	ceux de la tribu d'Asher, qui furent dénombrés, furent 41 500.
1:42	Des fils de Nephthali, selon leurs générations, leurs familles et les maisons de leurs pères, dénombrés chacun par leur nom, depuis l'âge de 20 ans et au-dessus, tous ceux qui pouvaient aller à la guerre :
1:43	ceux de la tribu de Nephthali, qui furent dénombrés, furent 53 400.
1:44	Ce sont là ceux dont Moshè et Aaron firent le dénombrement, les douze princes d'entre les enfants d'Israël y étant, un pour chaque maison de leurs pères.
1:45	Ainsi tous ceux des enfants d'Israël dont on fit le dénombrement, selon les maisons de leurs pères, depuis l'âge de 20 ans et au-dessus, tous ceux d'entre les Israélites, qui pouvaient aller à la guerre :
1:46	tous ceux dont on fit le dénombrement furent 603 550.
1:47	Mais les Lévites ne furent pas dénombrés avec eux, selon la tribu de leurs pères.
1:48	Car YHWH avait parlé à Moshè, en disant :
1:49	Tu ne feras aucun dénombrement de la tribu de Lévi et tu ne lèveras pas leurs têtes au milieu des enfants d'Israël.
1:50	Mais tu donneras aux Lévites la charge du tabernacle du témoignage, et de tous ses ustensiles, et de tout ce qui lui appartient. Ils porteront le tabernacle et tous ses ustensiles ; ils y serviront et camperont autour du tabernacle.
1:51	Et quand le tabernacle partira, les Lévites le démonteront ; et quand le tabernacle campera, les Lévites le dresseront. Et l'étranger qui s'en approchera sera puni de mort<!--Ez. 44:8-9.-->.
1:52	Et les enfants d'Israël camperont chacun dans son camp, et chacun sous sa bannière, selon leurs armées.
1:53	Mais les Lévites camperont autour du tabernacle du témoignage afin qu'il n'y ait pas d'indignation sur l'assemblée des enfants d'Israël, et ils prendront en leur charge le tabernacle du témoignage.
1:54	Et les enfants d'Israël firent selon toutes les choses que YHWH avait commandées à Moshè ; ils le firent ainsi.

## Chapitre 2

### Disposition du camp d'Israël par tribu

2:1	Et YHWH parla à Moshè et à Aaron, en disant :
2:2	Les enfants d'Israël camperont chacun sous sa bannière, avec les enseignes des maisons de leurs pères, tout autour de la tente d'assignation, vis-à-vis.
2:3	Ceux qui camperont à l'est, du côté du soleil levant, seront sous la bannière du camp de Yéhouda, selon ses armées. Et Nahshôn, fils d'Amminadab sera le prince des enfants de Yéhouda,
2:4	son armée et ses dénombrés : 74 600.
2:5	Près de lui campera la tribu de Yissakar, et Netanél, fils de Tsouar sera le prince des enfants de Yissakar,
2:6	son armée et ses dénombrés : 54 400.
2:7	Puis la tribu de Zebouloun, et Éliab, fils de Hélon, sera le prince des enfants de Zebouloun,
2:8	son armée et ses dénombrés : 57 400.
2:9	Tous les dénombrés du camp de Yéhouda : 186 400 selon leurs armées. Ils partiront les premiers.
2:10	La bannière du camp de Reouben, selon ses armées sera vers le sud, et Élitsour, fils de Shedéour sera le prince des enfants de Reouben,
2:11	et son armée et ses dénombrés : 46 500.
2:12	Près de lui campera la tribu de Shim’ôn, et Sheloumiel, fils de Tsourishaddaï sera le prince des enfants de Shim’ôn,
2:13	et son armée et ses dénombrés : 59 300.
2:14	Puis la tribu de Gad, et Éliasaph, fils de Reouel, sera le prince des enfants de Gad,
2:15	et son armée et ses dénombrés : 45 650.
2:16	Tous les dénombrés du camp de Reouben : 151 450 selon leurs armées. Ils partiront les seconds.
2:17	Ensuite la tente d'assignation partira avec le camp des Lévites, au milieu des camps qui partiront comme ils auront campé, chacune en sa place, selon leurs bannières.
2:18	La bannière du camp d'Éphraïm, selon ses armées sera vers l'occident, et Éliyshama, fils d'Ammihoud sera le prince des enfants d'Éphraïm,
2:19	et son armée et ses dénombrés : 40 500.
2:20	Et auprès de lui la tribu de Menashè, et Gamaliel, fils de Pedahtsour sera le prince des fils de Menashè,
2:21	et son armée et ses dénombrés : 32 200.
2:22	Puis la tribu de Benyamin, et Abidan, fils de Guideoni sera le prince des enfants de Benyamin,
2:23	et son armée et ses dénombrés : 35 400.
2:24	Tous les dénombrés pour le camp d'Éphraïm : 108 100 selon leurs armées. Ils partiront les troisièmes.
2:25	La bannière du camp de Dan, selon ses armées, sera vers le nord, et Ahiézer, fils d'Ammishaddaï sera le prince des enfants de Dan,
2:26	et son armée et ses dénombrés : 62 700.
2:27	Près de lui campera la tribu d'Asher, et Paguiel, fils d'Ocran sera le prince des enfants d'Asher,
2:28	et son armée et ses dénombrés : 41 500.
2:29	Puis la tribu de Nephthali, et Ahira, fils d'Énan sera le prince des enfants de Nephthali,
2:30	et son armée et ses dénombrés : 53 400.
2:31	Tous les dénombrés du camp de Dan : 157 600. Ils partiront les derniers des bannières.
2:32	Ce sont là ceux des enfants d'Israël dont on fit le dénombrement selon les maisons de leurs pères. Tous les dénombrés des camps selon leurs armées furent 603 550.
2:33	Mais les Lévites ne furent pas dénombrés avec les autres enfants d'Israël, comme YHWH l'avait commandé à Moshè.
2:34	Et les enfants d'Israël agirent en tout comme YHWH l'avait ordonné à Moshè. C'est ainsi qu'ils campaient, selon leurs bannières, et c'est ainsi qu'ils se déplaçaient, chacun selon leurs familles, et selon la maison de leurs pères.

## Chapitre 3

### Organisation des prêtres et des Lévites

3:1	Voici les générations d'Aaron et de Moshè, au temps où YHWH parla à Moshè sur la montagne de Sinaï.
3:2	Voici les noms des fils d'Aaron : Nadab, qui était l'aîné, Abihou, Èl’azar et Ithamar.
3:3	Ce sont là les noms des fils d'Aaron, les prêtres, qui furent oints et consacrés pour exercer la prêtrise<!--Ex. 40:15 ; Lé. 8:30.-->.
3:4	Mais Nadab et Abihou moururent devant YHWH lorsqu'ils offrirent un feu étranger devant YHWH, dans le désert de Sinaï. Ils n'avaient pas eu d'enfants. Mais Èl’azar et Ithamar exercèrent la prêtrise en présence d'Aaron leur père<!--Lé. 10:1-2 ; 1 Ch. 24:2.-->.
3:5	YHWH parla à Moshè, en disant :
3:6	Fais approcher la tribu de Lévi et fais qu'elle se tienne devant Aaron, le prêtre, afin qu'ils le servent
3:7	et qu'ils aient la charge de ce qu'il leur ordonnera de garder et de ce que toute l'assemblée leur ordonnera de garder, devant la tente d'assignation, en faisant le service du tabernacle.
3:8	Et qu'ils gardent tous les ustensiles de la tente d'assignation et ce qui leur sera donné en charge par les enfants d'Israël pour faire le service du tabernacle.
3:9	Ainsi tu donneras les Lévites à Aaron et à ses fils ; ils lui seront donnés, ils lui seront donnés d'entre les enfants d'Israël.
3:10	Tu établiras donc Aaron et ses fils, et ils exerceront leur prêtrise. Mais tout étranger qui s'approchera sera mis à mort.
3:11	Et YHWH parla à Moshè, en disant :
3:12	Voici, j'ai pris les Lévites d'entre les enfants d'Israël, à la place de tout premier-né qui ouvre la matrice parmi les enfants d'Israël : c'est pourquoi les Lévites seront à moi.
3:13	Car tout premier-né m'appartient, depuis le jour où j'ai frappé tous les premiers-nés au pays d'Égypte, je me suis consacré tout premier-né en Israël, depuis les humains jusqu'aux bêtes. Ils seront à moi, je suis YHWH<!--Ex. 13:2, 22:29, 34:19 ; Lé. 27:26.-->.

### Les familles des Lévites

3:14	YHWH parla aussi à Moshè dans le désert de Sinaï, en disant :
3:15	Dénombre les enfants de Lévi, par les maisons de leurs pères et par leurs familles, en comptant tout mâle depuis l'âge d'un mois et au-dessus.
3:16	Et Moshè les dénombra, selon le commandement de YHWH, ainsi qu'il lui avait été ordonné.
3:17	Et voici les fils de Lévi selon leurs noms : Guershon, Kehath et Merari.
3:18	Et voici les noms des fils de Guershon, selon leurs familles, Libni et Shimeï.
3:19	Et les fils de Kehath selon leurs familles, Amram, Yitshar, Hébron et Ouzziel ;
3:20	et les fils de Merari, selon leurs familles, Machli et Moushi. Ce sont là les familles de Lévi, selon les maisons de leurs pères.
3:21	De Guershon est sortie la famille de Libni et la famille de Shimeï ; ce sont les familles des Guershonites.
3:22	Ceux dont on fit le dénombrement, en comptant tous les mâles depuis l'âge d'un mois et au-dessus, furent au nombre de 7 500.
3:23	Les familles des Guershonites camperont derrière le tabernacle à l'occident.
3:24	Et Éliasaph, fils de Laël, sera le prince de la maison des pères des Guershonites.

### Les fonctions des Lévites

3:25	Et les fils de Guershon avaient dans la tente d'assignation la charge du tabernacle, de la tente, de sa couverture et du rideau de l'entrée de la tente d'assignation ;
3:26	et les rideaux du parvis avec le rideau de l'entrée du parvis qui servent pour le tabernacle et pour l'autel tout autour, et son cordage pour tout son service.
3:27	Et de Kehath est sortie la famille des Amramites, la famille des Yitsharites, la famille des Hébronites et la famille des Ouzziélites. Ce furent là les familles des Kehathites,
3:28	dont tous les mâles depuis l'âge d'un mois et au-dessus furent au nombre de 8 600, ayant la charge du lieu saint.
3:29	Les familles des fils de Kehath camperont du côté du tabernacle vers le sud.
3:30	Et Élitsaphan, fils d'Ouzziel, sera le prince de la maison des pères des familles des Kehathites.
3:31	Et ils auront en charge l'arche, la table, le chandelier, les autels et les ustensiles du lieu saint avec lesquels on fait le service, et le rideau, avec tout ce qui y sert.
3:32	Et le prince des princes des Lévites sera Èl’azar, fils d'Aaron, le prêtre ; qui aura la surveillance sur ceux qui auront la charge du lieu saint.
3:33	Et de Merari est sortie la famille des Machlites et la famille des Moushi. Ce furent là les familles de Merari ;
3:34	ceux dont on fit le dénombrement, après le compte qui fut fait de tous les mâles, depuis l'âge d'un mois et au-dessus, furent 6 200.
3:35	Et Touriel, fils d'Abihaïl, sera le prince de la maison des pères des familles des Merarites ; ils camperont du côté du tabernacle vers le nord.
3:36	Et on donnera aux enfants de Merari la surveillance des planches du tabernacle, de ses barres, de ses colonnes, de ses bases et de tous ses ustensiles, avec tout ce qui y sera ;
3:37	et des colonnes du parvis tout autour, avec leurs bases, leurs pieux et leurs cordes.
3:38	Et Moshè, et Aaron et ses fils campaient devant le tabernacle, à l'orient, devant la tente d'assignation, du côté du soleil levant. Ils avaient la garde et le soin du sanctuaire, remis à la garde des enfants d'Israël. Tout étranger qui s'approcherait serait mis à mort.
3:39	Tous les Lévites dénombrés, que Moshè et Aaron dénombrèrent selon leurs familles, selon le commandement de YHWH, tous les mâles de l'âge d'un mois et au-dessus, furent de 22 000.

### Le rachat des premiers-nés

3:40	YHWH dit à Moshè : Fais le dénombrement de tous les premiers-nés mâles des enfants d'Israël, depuis l'âge d'un mois et au-dessus, et relève le nombre de leurs noms.
3:41	Et tu prendras les Lévites pour moi, je suis YHWH, à la place de tous les premiers-nés qui sont parmi les enfants d'Israël ; tu prendras aussi les bêtes des Lévites, à la place de tous les premiers-nés des bêtes des enfants d'Israël.
3:42	Moshè fit le dénombrement, comme YHWH lui avait commandé, de tous les premiers-nés qui étaient parmi les enfants d'Israël.
3:43	Et tous les premiers-nés des mâles, selon le nombre des noms, depuis l'âge d'un mois et au-dessus, selon leur dénombrement, furent 22 273.
3:44	Et YHWH parla à Moshè, en disant :
3:45	Prends les Lévites à la place de tous les premiers-nés qui sont parmi les enfants d'Israël, et les bêtes des Lévites, à la place de leurs bêtes ; et les Lévites seront à moi ; je suis YHWH.
3:46	Et pour le rachat des 273 qui dépassent le nombre des Lévites, parmi les premiers-nés des enfants d'Israël,
3:47	tu prendras 5 sicles par tête. Tu les prendras selon le sicle du lieu saint ; le sicle est de 20 guéras<!--Ex. 30:13 ; Lé. 27:6,25 ; Ez. 45:12.-->.
3:48	Et tu donneras l'argent à Aaron et à ses fils, pour le rachat de ceux qui dépassent le nombre des Lévites.
3:49	Moshè donc prit l'argent pour le rachat de ceux qui dépassaient le nombre des rachetés par les Lévites.
3:50	Et l'argent qu'il reçut des premiers-nés des enfants d'Israël fut de 1 375 sicles, selon le sicle du lieu saint.
3:51	Et Moshè donna l'argent du rachat à Aaron et à ses fils, sur l'ordre de YHWH, comme YHWH l'avait ordonné à Moshè.

## Chapitre 4

### Les fonctions des fils de Kehath

4:1	Et YHWH parla à Moshè et à Aaron, en disant :
4:2	Lève les têtes des fils de Kehath au milieu des enfants de Lévi, selon leurs familles, selon les maisons de leurs pères,
4:3	depuis l'âge de 30 ans et au-dessus, jusqu'à l'âge de 50 ans, tous ceux qui entrent en service pour exercer une fonction dans la tente d'assignation.
4:4	Voici le service des fils de Kehath à la tente d'assignation, c'est-à-dire, le Saint des saints.
4:5	Quand le camp partira, Aaron et ses fils viendront démonter le voile<!--Le voile intérieur est l'image du corps humain du Mashiah (Mt. 26:26). Ce voile se déchira de haut en bas lorsque le Seigneur mourut sur la croix (Mt. 27:50-51). Désormais, le croyant peut pénétrer dans la présence du Père (Hé. 10:19-20).--> qui sert de rideau et en couvriront l'arche du témoignage.
4:6	Puis ils mettront au-dessus une couverture de peaux de taissons, ils étendront par-dessus un drap d'étoffe violette et ils y mettront ses barres.
4:7	Et ils étendront un drap d'étoffe violette sur la table des pains de proposition et mettront sur elle, les plats, les tasses, les bassins et les calices de libations. Le pain continuel sera sur elle.
4:8	Ils étendront au-dessus un drap teint d'écarlate de cochenille, ils le couvriront d'une couverture de peaux de taissons et ils y mettront ses barres.
4:9	Et ils prendront un drap d'étoffe violette, en couvriront le chandelier du luminaire avec ses lampes, ses mouchettes, ses vases à cendre et tous ses vases à huile dont on fait usage pour son service<!--Ex. 25:30-38.-->.
4:10	Ils le mettront avec tous ses ustensiles dans une couverture de peaux de taissons, et le mettront sur une perche.
4:11	Ils étendront sur l'autel d'or un drap d'étoffe violette, ils le couvriront d'une couverture de peaux de taissons, et ils y mettront ses barres.
4:12	Ils prendront aussi tous les ustensiles du service dont on se sert dans le lieu saint, ils les mettront dans un drap d'étoffe violette et ils les couvriront d'une couverture de peaux de taissons, et les mettront sur des perches.
4:13	Ils ôteront les cendres de l'autel et étendront dessus un drap de pourpre.
4:14	Et ils mettront dessus les ustensiles dont on se sert pour l'autel, les brasiers, les fourchettes, les pelles, les bassins et tous les ustensiles de l'autel. Ils étendront dessus une couverture de peaux de taissons, et ils y mettront ses barres.
4:15	Le camp partira après qu'Aaron et ses fils auront achevé de couvrir le lieu saint et tous ses ustensiles, et après cela les fils de Kehath viendront pour le porter. Ils ne toucheront pas les choses saintes, de peur qu'ils ne meurent. C'est là ce que les fils de Kehath porteront de la tente d'assignation.

### Les fonctions d'Èl’azar

4:16	Et Èl’azar fils d'Aaron, le prêtre, aura la surveillance de l'huile du luminaire, de l'encens aromatique, de l'offrande continuelle de grain et de l'huile de l'onction. La charge de tout le tabernacle et de toutes les choses qui sont dans le lieu saint, et de ses ustensiles<!--Ex. 30:23-35.-->.
4:17	YHWH parla à Moshè et à Aaron, en disant :
4:18	Ne retranchez pas la tribu des familles des Kehathites d'entre les Lévites.
4:19	Faites ceci pour eux, afin qu'ils vivent et ne meurent pas quand ils s'approcheront du Saint des saints : Aaron et ses fils viendront et placeront chacun d'eux dans son service et dans sa charge.
4:20	Et ils n'entreront pas pour regarder quand on enveloppera les choses saintes, afin qu'ils ne meurent pas.

### Les fonctions des fils de Guershon

4:21	YHWH parla à Moshè, en disant :
4:22	Fais aussi le compte des fils de Guershon selon les maisons de leurs pères et selon leurs familles.
4:23	Depuis l'âge de 30 ans et au-dessus, jusqu'à l'âge de 50 ans, tu les dénombreras, tous ceux qui entrent en service pour exercer une fonction dans la tente d'assignation.
4:24	Voici le service des familles des Guershonites, le service qu'ils devront faire et ce qu'ils devront porter.
4:25	Ils porteront donc les tapis du tabernacle et la tente d'assignation, sa couverture, la couverture de peaux de taissons qui est sur lui par-dessus, et le rideau de l'entrée de la tente d'assignation ;
4:26	les rideaux du parvis et le rideau de l'entrée de la porte du parvis qui servent pour le tabernacle et pour l'autel tout autour, leurs cordages, et tous les ustensiles de leur service, et tout ce qui est fait pour eux : C'est ce en quoi ils serviront.
4:27	Dans leur service, les fils des Guershonites seront sous les ordres d'Aaron et de ses fils pour tout ce qu'ils doivent porter et pour tout le service qu'ils devront faire. Vous les chargerez d'observer tout ce qu'ils doivent porter.
4:28	C'est là le service des familles des fils des Guershonites dans la tente d'assignation. Leur charge sera sous la conduite d'Ithamar, fils d'Aaron, le prêtre.

### Les fonctions des fils de Merari

4:29	Tu dénombreras aussi les fils de Mérari selon leurs familles et selon les maisons de leurs pères.
4:30	Tu les dénombreras depuis l'âge de 30 ans et au-dessus, jusqu'à l'âge de 50 ans, tous ceux qui entrent en service pour exercer une fonction dans la tente d'assignation.
4:31	Et voici la charge de ce qu'ils auront à porter, selon tout le service qu'ils auront à faire à la tente d'assignation : les planches du tabernacle, ses barres et ses colonnes, avec ses bases<!--Ex. 26:15.-->,
4:32	et les colonnes du parvis tout autour, et leurs bases, leurs pieux, leurs cordages, tous leurs ustensiles, et tout ce dont on se sert en ces choses-là. Et vous compterez par leurs noms les objets confiés à leur garde et qu’ils ont à porter.
4:33	C'est là le service des familles des fils de Merari, pour tout leur service à la tente d'assignation, sous la conduite d'Ithamar, fils d'Aaron, le prêtre.
4:34	Moshè, Aaron et les princes de l'assemblée dénombrèrent les fils des Kéhathites, selon leurs familles et selon les maisons de leurs pères ;
4:35	depuis l'âge de 30 ans et au-dessus, jusqu'à l'âge de 50 ans, tous ceux qui entrent en service pour exercer une fonction dans la tente d'assignation.
4:36	Et ceux dont on fit le dénombrement selon leurs familles, étaient 2 750.
4:37	Ce sont là les dénombrés des familles des Kéhathites, tous servant à la tente d'assignation, que Moshè et Aaron dénombrèrent selon le commandement que YHWH avait fait par le moyen de Moshè.
4:38	Or quant aux dénombrés des fils de Guershon selon leurs familles, et selon les maisons de leurs pères,
4:39	depuis l'âge de 30 ans et au-dessus, jusqu'à l'âge de 50 ans, tous ceux qui entrent en service pour exercer une fonction dans la tente d'assignation,
4:40	ceux qui en furent dénombrés selon leurs familles et selon les maisons de leurs pères, étaient 2630.
4:41	Ce sont là, les dénombrés des familles des fils de Guershon, tous servant dans la tente d'assignation, que Moshè et Aaron dénombrèrent selon le commandement de YHWH.
4:42	Et quant aux dénombrés des familles des fils de Merari, selon leurs familles et selon les maisons de leurs pères,
4:43	depuis l'âge de 30 ans et au-dessus, jusqu'à l'âge de 50 ans, tous ceux qui entrent en service pour exercer une fonction dans la tente d'assignation :
4:44	ceux qui en furent dénombrés selon leurs familles, étaient 3 200.
4:45	Ce sont là, les dénombrés des familles des fils de Merari, que Moshè et Aaron dénombrèrent selon le commandement que YHWH avait fait par le moyen de Moshè.
4:46	Ainsi tous ces dénombrés, que Moshè, Aaron et les princes d'Israël dénombrèrent d'entre les Lévites, selon leurs familles et selon les maisons de leurs pères ;
4:47	depuis l'âge de 30 ans et au-dessus, jusqu'à l'âge de 50 ans, tous ceux qui entrent en service pour exercer une fonction et servir de porteurs dans la tente d'assignation,
4:48	tous ceux qui en furent dénombrés, étaient 8 580.
4:49	On fit leur dénombrement conformément à l'ordre que YHWH avait donné par l'intermédiaire de Moshè, chaque homme selon son service, chaque homme selon sa charge. On fit leur dénombrement conformément à l'ordre que YHWH avait donné à Moshè.

## Chapitre 5

### Mise en garde contre toute impureté ; diverses torahs

5:1	Et YHWH parla à Moshè, en disant :
5:2	Ordonne aux enfants d'Israël de renvoyer du camp tout lépreux, toute personne qui a une gonorrhée et toute personne rendue impure par une âme décédée<!--Lé. 13 et 15.-->.
5:3	Hommes ou femmes, vous les renverrez, vous les renverrez hors du camp afin qu'ils ne rendent pas impur le camp au milieu duquel j'habite.
5:4	Et les enfants d'Israël firent ainsi, et les renvoyèrent hors du camp, comme YHWH l'avait dit à Moshè ; ainsi firent les enfants d'Israël.
5:5	Et YHWH parla à Moshè, en disant :
5:6	Parle aux enfants d'Israël : Quand un homme ou une femme aura commis l'un des péchés par lesquels un être humain commet une infidélité envers YHWH, et que cette âme se sera ainsi rendue coupable,
5:7	alors elle confessera le péché commis et restituera en sa totalité l'objet du délit, en y ajoutant un cinquième. Elle le donnera à celui envers qui elle s'est rendue coupable.
5:8	Et si cet homme n'a pas de rédempteur à qui l'on puisse restituer l'objet du délit, l'objet du délit restitué à YHWH appartiendra au prêtre. Il y aura en outre le bélier expiatoire avec lequel on fera propitiation pour le coupable.
5:9	De même, toute offrande élevée d'entre toutes les choses sanctifiées des enfants d'Israël qu'ils présenteront<!--Ez. 44:30.--> au prêtre lui appartiendra.
5:10	Les choses que chacun aura consacrées lui appartiendront, ce que chacun donnera au prêtre lui appartiendra<!--Lé. 10:12-13.-->.
5:11	YHWH parla à Moshè, en disant :
5:12	Parle aux enfants d'Israël et dis-leur : Si un homme a une femme qui se détourne et lui devient infidèle,
5:13	et qu'un autre homme couche avec elle et a une émission de sperme et que cela soit caché aux yeux de son mari, et qu'elle se soit rendue impure en secret, sans qu'il y ait de témoin contre elle et sans qu'elle soit prise sur le fait,
5:14	et si un esprit de jalousie saisi son mari et qu'il soit jaloux de sa femme parce qu'elle s'est rendue impure, ou bien s'il est saisi d'un esprit de jalousie et qu'il soit jaloux de sa femme sans qu'elle ne se soit rendue impure,
5:15	cet homme amènera sa femme devant le prêtre et apportera en offrande pour elle, la dixième partie d'un epha de farine d'orge. Il ne répandra pas d'huile dessus et il n'y mettra pas d'encens, car c'est une offrande de grain de jalousie, une offrande de grain de souvenir, pour rappeler l'iniquité<!--Lé. 5:11.-->.
5:16	Le prêtre la fera approcher et la fera tenir debout devant YHWH.
5:17	Puis le prêtre prendra de l'eau sainte dans un vase de terre, et il prendra de la poussière qui sera sur le sol du tabernacle et la mettra dans l'eau.
5:18	Ensuite le prêtre fera tenir debout la femme devant YHWH. Il découvrira la tête de cette femme et lui posera sur les paumes des mains l'offrande de grain de souvenir, l'offrande de grain de jalousie. Le prêtre tiendra dans sa main les eaux amères qui apportent la malédiction.
5:19	Et le prêtre fera jurer la femme et lui dira : Si aucun homme n'a couché avec toi, et si étant sous l'autorité de ton mari tu ne t'es pas détournée et souillée, sois préservée<!--Vient de l'hébreu « naqah » qui signifie « être libre », « être innocent », « être purifié », « dégagé ». Ces eaux étaient utilisées pour condamner ou justifier les coupables.--> du mal de ces eaux amères qui apportent la malédiction.
5:20	Mais si, étant sous l'autorité de ton mari, tu t'es détournée et souillée, et si un autre homme que ton mari a couché avec toi...
5:21	Alors le prêtre fera jurer la femme avec un serment d'imprécation et lui dira : Que YHWH te livre à la malédiction et à l'exécration au milieu de ton peuple, en faisant flétrir ta cuisse et enfler ton ventre,
5:22	et que ces eaux qui apportent la malédiction entrent dans tes entrailles pour te faire enfler le ventre et flétrir ta cuisse ! Alors la femme répondra : Amen ! Amen !
5:23	Ensuite le prêtre écrira dans un livre ces imprécations et les effacera avec les eaux amères.
5:24	Et il fera boire à la femme les eaux amères qui apportent la malédiction et les eaux qui apportent la malédiction entreront en elle pour être amères.
5:25	Le prêtre donc prendra des mains de la femme l'offrande de grain de jalousie, et l'agitera de côté et d'autre devant YHWH et l'offrira sur l'autel.
5:26	Le prêtre prendra une poignée de cette offrande de grain comme souvenir<!--Voir commentaire en Lé. 2:2.--> et il la brûlera sur l'autel. C'est après cela qu'il fera boire les eaux à la femme.
5:27	Et après qu'il lui aura fait boire les eaux, s'il est vrai qu'elle se soit souillée et qu'elle a été infidèle à son mari, les eaux qui apportent la malédiction entreront en elle et lui seront amères, et son ventre enflera, sa cuisse se flétrira, et cette femme sera assujettie à l'exécration du serment au milieu de son peuple.
5:28	Mais si la femme ne s'est pas souillée et qu'elle soit pure, elle sera innocentée et ensemencée de semence.
5:29	Telle est la torah sur la jalousie, quand la femme qui est sous l'autorité de son mari se détourne et se souille,
5:30	ou quand un mari saisi d'un esprit de jalousie a des soupçons sur sa femme : Le prêtre la fera tenir debout devant YHWH et fera à l'égard de cette femme tout ce qui est ordonné par cette torah.
5:31	Le mari sera exempt de faute, mais cette femme portera son iniquité.

## Chapitre 6

### Le vœu de naziréat

6:1	YHWH parla à Moshè, en disant :
6:2	Parle aux enfants d'Israël et dis-leur : Lorsqu'un homme ou une femme se consacrera en faisant un vœu de nazaréen pour se consacrer à YHWH,
6:3	il s'abstiendra de vin et de boisson forte. Il ne boira ni vinaigre fait de vin, ni vinaigre fait avec une boisson forte. Il ne boira d'aucune liqueur de raisins et il ne mangera pas de raisins, frais ou secs.
6:4	Durant tous les jours de son naziréat<!--Vient de l'hébreu « nezer » et signifie « consécration », « couronne », « séparation », « Naziréat » et « diadème ».-->, il ne mangera d'aucun fruit de la vigne, depuis les pépins jusqu'à la peau<!--Jg. 13:7 ; Lu. 1:15.-->.
6:5	Le rasoir ne passera pas sur sa tête durant tous les jours du vœu de son naziréat. Il sera saint jusqu'à ce que les jours pour lesquels il s'est consacré à YHWH soient accomplis, et il laissera croître les cheveux de sa tête<!--Jg. 13:5 ; 1 S. 1:11.-->.
6:6	Durant tous les jours pour lesquels il s'est consacré à YHWH, il ne s'approchera d'aucune personne morte<!--Lé. 21:1-4.-->.
6:7	Il ne se rendra pas impur à la mort de son père, ni de sa mère, ni de son frère, ni de sa sœur, car il porte sur sa tête la consécration de son Elohîm.
6:8	Durant tous les jours de son naziréat, il sera consacré à YHWH.
6:9	Et si quelqu'un vient à mourir subitement près de lui, la tête de son naziréat sera impure, et il rasera sa tête au jour de sa purification, il la rasera le septième jour.
6:10	Le huitième jour, il apportera au prêtre 2 tourterelles ou 2 jeunes pigeons, à l'entrée de la tente d'assignation<!--Lé. 1 et 12:6.-->.
6:11	Et le prêtre en offrira un en sacrifice pour le péché et l'autre en holocauste, et il fera la propitiation pour lui, de ce qu'il a péché à l'occasion du mort. Il sanctifiera donc ainsi sa tête en ce jour-là.
6:12	Il consacrera à YHWH les jours de son naziréat et il offrira un agneau d'un an en sacrifice de culpabilité. Et les jours précédents ne compteront pas car son naziréat a été souillé.
6:13	Et voici la torah du naziréen. Lorsque les jours de son naziréat seront accomplis, on le fera venir à la porte de la tente d'assignation.
6:14	Il présentera son offrande à YHWH : Un agneau d'un an et sans défaut pour l'holocauste, une brebis d'un an et sans défaut en sacrifice pour le péché, et un bélier sans défaut pour le sacrifice d'offrande de paix<!--Voir commentaire en Lé. 3:1.--> ;
6:15	une corbeille de pains sans levain, de gâteaux de fine farine pétrie à l'huile et de galettes sans levain oints d'huile, avec leur offrande de grain et leurs libations.
6:16	Le prêtre les offrira devant YHWH et il offrira aussi son sacrifice pour le péché et son holocauste.
6:17	Et il offrira le bélier en sacrifice d'offrande de paix à YHWH, avec la corbeille des pains sans levain ; le prêtre offrira aussi son offrande de grain et sa libation.
6:18	Et le naziréen rasera la tête de son naziréat à l'entrée de la tente d'assignation, et prendra les cheveux de la tête de son naziréat, et les mettra sur le feu qui est sous le sacrifice d'offrande de paix.
6:19	Et le prêtre prendra l'épaule cuite du bélier et un gâteau sans levain de la corbeille, et une galette sans levain, et les mettra sur les paumes des mains du naziréen, après qu'il se sera fait raser son naziréat.
6:20	Et le prêtre les agitera de côté et d'autre devant YHWH : C'est une chose sainte qui appartient au prêtre, avec la poitrine agitée et l'épaule offerte par élévation. Et après cela le naziréen boira du vin<!--Lé. 7:32-34 ; Ex. 29:24-27.-->.
6:21	Telle est la torah du nazaréen qui aura voué à YHWH son offrande pour son naziréat, outre ce que sa main aura pu atteindre. Il accomplira ce qui est ordonné pour le vœu qu'il a fait conformément à la torah de son naziréat.

### Aaron et ses fils bénissent Israël

6:22	YHWH parla à Moshè, en disant :
6:23	Parle à Aaron et à ses fils, et dis-leur : Vous bénirez ainsi les enfants d'Israël, en leur disant :
6:24	Que YHWH te bénisse et te garde !
6:25	Que YHWH fasse luire sa face sur toi et te fasse grâce<!--Ps. 67:2, 119:135.--> !
6:26	Que YHWH tourne sa face vers toi et te donne la paix !
6:27	C'est ainsi qu'ils mettront mon Nom sur les enfants d'Israël et je les bénirai.

## Chapitre 7

### Les offrandes des princes

7:1	Il arriva, le jour où Moshè eut achevé de dresser le tabernacle, et qu'il l'eut oint et sanctifié avec tous ses ustensiles, et l'autel avec tous ses ustensiles, et qu'il les eut oints et sanctifiés,
7:2	que les princes d'Israël et les princes des maisons de leurs pères, qui sont les princes des tribus, et qui avaient assisté aux dénombrements, firent leur offrande.
7:3	Ils amenèrent leur offrande devant YHWH : 6 chariots en forme de litières et 12 bœufs, soit un chariot pour 2 princes et un bœuf pour chacun. Ils les offrirent devant le tabernacle.
7:4	Alors YHWH parla à Moshè, en disant :
7:5	Prends d'eux ces choses, et elles seront employées pour le service de la tente d'assignation. Tu les donneras aux Lévites, à chacun selon ses fonctions.
7:6	Moshè prit donc les chariots et les bœufs, et il les remit aux Lévites.
7:7	Il donna aux fils de Guershon 2 chariots et 4 bœufs, selon leurs fonctions.
7:8	Mais il donna aux fils de Merari 4 chariots et 8 bœufs, selon leurs fonctions, sous la conduite d'Ithamar, fils d'Aaron, le prêtre.
7:9	Or il n'en donna pas aux fils de Kehath, parce qu'ils étaient chargés du service du sanctuaire. Ils portaient ces choses saintes sur les épaules.
7:10	Et les princes présentèrent leur offrande pour la dédicace de l'autel, le jour où on l'oignit. Les princes présentèrent leur offrande devant l'autel.
7:11	Et YHWH dit à Moshè : Que les princes présentent leur offrande, un jour l'un, l'autre jour l'autre, pour la dédicace de l'autel.
7:12	Le premier jour donc, Nahshôn, fils d'Amminadab, présenta son offrande pour la tribu de Yéhouda.
7:13	Il offrit un plat en argent du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:14	Une coupe en or de 10 sicles pleine d'encens,
7:15	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:16	un jeune bouc en sacrifice pour le péché,
7:17	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande de Nahshôn, fils d'Amminadab.
7:18	Le second jour, Netanél, fils de Tsouar, prince de la tribu de Yissakar, présenta son offrande.
7:19	Et il offrit pour son offrande un plat en argent du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:20	Une coupe en or de 10 sicles pleine d'encens,
7:21	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:22	un jeune bouc en sacrifice pour le péché,
7:23	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande de Netanél, fils de Tsouar.
7:24	Le troisième jour, Éliab, fils de Hélon, prince des fils de Zebouloun, présenta son offrande.
7:25	Il offrit un plat en argent du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:26	Une coupe en or de 10 sicles pleine d'encens,
7:27	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:28	un jeune bouc en sacrifice pour le péché,
7:29	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande d'Éliab, fils de Hélon.
7:30	Le quatrième jour, Élitsour, fils de Shedéour, prince des fils de Reouben, présenta son offrande.
7:31	Il offrit un plat en argent du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:32	Une coupe en or de 10 sicles pleine d'encens,
7:33	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:34	un jeune bouc en sacrifice pour le péché,
7:35	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande d'Élitsour, fils de Shedéour.
7:36	Le cinquième jour, Sheloumiel, fils de Tsourishaddaï, prince des fils de Shim’ôn, présenta son offrande.
7:37	Il offrit un plat en argent du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:38	Une coupe en or de 10 sicles pleine d'encens,
7:39	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:40	un jeune bouc en sacrifice pour le péché,
7:41	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs, et 5 agneaux d'un an. Telle fut l'offrande de Sheloumiel, fils de Tsourishaddaï.
7:42	Le sixième jour, Éliasaph, fils de Déouel, prince des fils de Gad, présenta son offrande.
7:43	Il offrit un plat en argent du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:44	Une coupe en or de 10 sicles pleine d'encens,
7:45	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:46	un jeune bouc en sacrifice pour le péché,
7:47	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande d'Éliasaph, fils de Déouel.
7:48	Le septième jour, Éliyshama, fils d'Ammihoud, prince des fils d'Éphraïm, présenta son offrande.
7:49	Il offrit un plat en argent, du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:50	Une coupe en or de 10 sicles pleine d'encens,
7:51	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:52	un jeune bouc en sacrifice pour le péché,
7:53	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande d'Éliyshama, fils d'Ammihoud.
7:54	Le huitième jour, Gamaliel, fils de Pedahtsour, prince des fils de Menashè, présenta son offrande.
7:55	Il offrit un plat en argent, du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:56	Une coupe en or de 10 sicles pleine d'encens,
7:57	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:58	un jeune bouc en sacrifice pour le péché,
7:59	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande de Gamaliel, fils de Pedahtsour.
7:60	Le neuvième jour, Abidan, fils de Guideoni, prince des fils de Benyamin, présenta son offrande.
7:61	Il offrit un plat en argent, du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:62	Une coupe en or de 10 sicles pleine d'encens,
7:63	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:64	un jeune bouc en sacrifice pour le péché,
7:65	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande d'Abidan, fils de Guideoni.
7:66	Le dixième jour, Ahiézer, fils d'Ammishaddaï, prince des fils de Dan, présenta son offrande.
7:67	Il offrit un plat en argent du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:68	Une coupe en or de 10 sicles pleine d'encens,
7:69	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:70	un jeune bouc en sacrifice pour le péché,
7:71	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande d'Ahiézer, fils d'Ammishaddaï.
7:72	Le onzième jour, Paguiel, fils d'Ocran, prince des fils d'Asher, présenta son offrande.
7:73	Il offrit un plat en argent, du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:74	Une coupe en or de 10 sicles pleine d'encens,
7:75	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:76	un jeune bouc en sacrifice pour le péché,
7:77	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande de Paguiel, fils d'Ocran.
7:78	Le douzième jour, Ahira, fils d'Énan, prince des fils de Nephthali, présenta son offrande.
7:79	Il offrit un plat en argent du poids de 130 sicles, un bassin en argent de 70 sicles, selon le sicle du lieu saint, tous les deux pleins de fine farine pétrie à l'huile pour l'offrande de grain.
7:80	Une coupe en or de 10 sicles pleine d'encens,
7:81	un jeune taureau, un bélier, un agneau d'un an pour l'holocauste,
7:82	un jeune bouc en sacrifice pour le péché,
7:83	et pour le sacrifice d'offrande de paix, 2 bœufs, 5 béliers, 5 boucs et 5 agneaux d'un an. Telle fut l'offrande d'Ahira, fils d'Énan.

### Les dons des princes

7:84	Telle fut la dédicace de l'autel qui fut faite par les princes d'Israël, lorsqu'il fut oint : 12 plats en argent, 12 bassins en argent, 12 tasses en or.
7:85	Chaque plat en argent était de 130 sicles, et chaque bassin de 70. Tout l'argent de ces ustensiles montait à 2 400 sicles, selon le sicle du lieu saint.
7:86	12 coupes en or pleines d'encens, chacune de 10 sicles, selon le sicle du lieu saint. Tout l'or des tasses montait à 120 sicles.
7:87	Total des animaux pour l'holocauste : 12 veaux, 12 béliers et 12 agneaux d'un an, avec leurs offrandes de grain, et 12 jeunes boucs en sacrifice pour le péché.
7:88	Tous les animaux du sacrifice d'offrande de paix étaient 24 veaux, avec 60 béliers, 60 boucs et 60 agneaux d'un an. Telle fut donc la dédicace de l'autel, après qu'on l'eut oint.
7:89	Et quand Moshè entrait dans la tente d'assignation pour parler avec YHWH, il entendait une voix qui lui parlait du haut du propitiatoire placé sur l'arche du témoignage, entre les deux chérubins. Et il lui parlait<!--Ex. 25:22.-->.

## Chapitre 8

### Les lampes sur le chandelier

8:1	YHWH parla à Moshè, en disant :
8:2	Parle à Aaron et tu lui diras : Quand tu allumeras les lampes, les sept lampes éclaireront sur le devant du chandelier<!--Ex. 25:37.-->.
8:3	Et Aaron fit ainsi. Il plaça les lampes pour éclairer sur le devant du chandelier, comme YHWH l'avait commandé à Moshè.
8:4	Or le chandelier était fait de telle manière qu'il était d'or battu au marteau, de sa base jusqu'à ses fleurs. Il était fait au marteau. On fit ainsi le chandelier selon le modèle que YHWH avait fait voir à Moshè<!--Ex. 25:31-40.-->.

### Purification des Lévites

8:5	Puis YHWH parla à Moshè, en disant :
8:6	Prends les Lévites du milieu des enfants d'Israël et purifie-les.
8:7	Voici comment tu agiras à leur égard pour les purifier. Fais sur eux l'aspersion de l'eau de purification pour le péché. Ils feront passer le rasoir sur toute leur chair, ils laveront leurs vêtements et ils se purifieront.
8:8	Puis ils prendront un jeune taureau avec son offrande de grain de fine farine pétrie à l'huile. Tu prendras un autre jeune taureau en sacrifice pour le péché.
8:9	Alors tu feras approcher les Lévites devant la tente d'assignation et tu convoqueras toute l'assemblée des enfants d'Israël.
8:10	Tu feras approcher les Lévites devant YHWH et les enfants d'Israël poseront leurs mains sur les Lévites.
8:11	Et Aaron fera tourner de côté et d'autre les Lévites devant YHWH, comme offrande de la part des enfants d'Israël, et ils seront employés au service de YHWH.
8:12	Et les Lévites poseront leurs mains sur la tête des veaux et tu offriras l'un en sacrifice pour le péché, et l'autre en holocauste à YHWH, afin de faire la propitiation pour les Lévites.
8:13	Après tu feras tenir les Lévites devant Aaron et devant ses fils, et tu les présenteras en offrande à YHWH.
8:14	Ainsi tu sépareras les Lévites du milieu des enfants d'Israël et les Lévites m'appartiendront.
8:15	Après cela, les Lévites viendront pour servir dans la tente d'assignation quand tu les auras purifiés et présentés en offrande.
8:16	Car ils me sont donnés, ils me sont donnés du milieu des enfants d'Israël : je les ai pris pour moi à la place des premiers-nés de toute matrice, de tous les premiers-nés des fils d'Israël.
8:17	Car tout premier-né des enfants d'Israël m'appartient, tant des humains que des animaux. Je me les suis consacrés le jour où j'ai frappé tous les premiers-nés dans le pays d'Égypte.
8:18	Or j'ai pris les Lévites au lieu de tous les premiers-nés d'entre les enfants d'Israël.
8:19	Et j'ai donné, j'ai donné à Aaron et à ses fils, du milieu des enfants d'Israël, les Lévites pour faire le service des enfants d'Israël dans la tente d'assignation et pour faire la propitiation pour les enfants d'Israël, afin qu'il n'y ait pas de plaie parmi les enfants d'Israël, quand les enfants d'Israël s'approcheront du lieu saint.
8:20	Moshè, Aaron et toute l'assemblée des enfants d'Israël firent à l'égard des Lévites tout ce que YHWH avait ordonné à Moshè concernant les Lévites. C'est ainsi que firent les enfants d'Israël.
8:21	Les Lévites donc se purifièrent et lavèrent leurs vêtements. Et Aaron les fit tourner de côté et d'autre comme une offrande devant YHWH, et il fit la propitiation pour eux afin de les purifier.
8:22	Cela étant fait, les Lévites vinrent faire leur service dans la tente d'assignation devant Aaron et devant ses fils, selon ce que YHWH avait commandé à Moshè concernant les Lévites. Ainsi fut-il fait à leur égard.
8:23	Puis YHWH parla à Moshè, en disant :
8:24	Voici ce qui concerne les Lévites. Depuis l'âge de 25 ans et au-dessus, tout Lévite entrera en fonction dans la tente d'assignation.
8:25	Dès l'âge de 50 ans, il se retirera de sa fonction, de son service et ne servira plus.
8:26	Cependant il servira ses frères dans la tente d'assignation pour garder ce qui leur a été commis, mais il ne fera plus de service. Tu agiras ainsi à l'égard des Lévites pour ce qui concerne leurs fonctions.

## Chapitre 9

### La Pâque

9:1	YHWH avait aussi parlé à Moshè dans le désert de Sinaï, le premier mois de la seconde année, après qu'ils furent sortis du pays d'Égypte, en disant :
9:2	Que les enfants d'Israël célèbrent la Pâque<!--Ex. 12 ; 1 Co. 5:7.--> au temps fixé<!--Ge. 1:14.-->.
9:3	Vous la ferez au temps fixé<!--Ge. 1:14.-->, le quatorzième jour de ce mois, entre les deux soirs, selon tous ses statuts et selon toutes ses ordonnances.
9:4	Moshè parla donc aux enfants d'Israël afin qu'ils célèbrent la Pâque.
9:5	Et ils firent la Pâque le quatorzième jour du premier mois, entre les deux soirs, dans le désert de Sinaï, selon tout ce que YHWH avait commandé à Moshè ; les enfants d'Israël le firent ainsi.
9:6	Or il y eut quelques-uns qui étaient impurs à cause d'une âme humaine<!--Il est certainement question d'un cadavre.--> et qui ne purent célébrer la Pâque ce jour-là. Ils se présentèrent ce même jour devant Moshè et devant Aaron.
9:7	Et ces hommes leur dirent : Nous sommes impurs à cause d'une âme humaine, pourquoi serions-nous privés de présenter l'offrande à YHWH dans sa saison au milieu des enfants d'Israël ?
9:8	Et Moshè leur dit : Tenez-vous là debout, et j'entendrai ce que YHWH ordonnera à votre sujet.
9:9	Alors YHWH parla à Moshè, en disant :
9:10	Parle aux enfants d'Israël et dis-leur : Si quelqu'un d'entre vous ou de votre postérité est impur à cause d'un mort, ou est en voyage dans un lieu éloigné, il fera cependant la Pâque en l'honneur de YHWH.
9:11	Ils la feront le quatorzième jour du second mois, entre les deux soirs ; et ils la mangeront avec du pain sans levain et des herbes amères<!--Ex. 12:8, 23:18, 34:25 ; De. 16:4 ; Jn. 19:33-36.-->.
9:12	Ils n'en laisseront rien jusqu'au matin et n'en briseront pas les os<!--Voir Ex. 12:46 ; Ps. 34:20-21 et Jn. 19:36.-->. Ils la feront selon tous les statuts de la Pâque.
9:13	Mais si celui qui est pur et qui n'est pas en voyage s'abstient de célébrer la Pâque, il sera retranché d'entre ses peuples parce qu'il n'a pas présenté l'offrande de YHWH en sa saison.
9:14	Et si un étranger en séjour chez vous célèbre la Pâque de YHWH, il la fera selon l'ordonnance de la Pâque. Il y aura un même statut entre vous, pour l'étranger comme pour celui qui est né au pays<!--Ex. 12:49.-->.

### La nuée conduit Israël

9:15	Or le jour où le tabernacle fut dressé, la nuée couvrit le tabernacle, la tente d'assignation. Et le soir jusqu'au matin, elle parut sur le tabernacle avec l'apparence d'un feu<!--Ex. 13:21-22, 40:34-38 ; De. 1:33.-->.
9:16	Il en fut ainsi continuellement : la nuée le couvrait, mais elle paraissait la nuit comme du feu.
9:17	Chaque fois que la nuée s'élevait au-dessus de la tente, les enfants d'Israël partaient ; et au lieu où la nuée s'arrêtait, les enfants d'Israël y campaient.
9:18	Les enfants d'Israël marchaient sur l'ordre de YHWH, et ils campaient sur l'ordre de YHWH. Ils campaient aussi longtemps que la nuée se tenait sur le tabernacle.
9:19	Et quand la nuée restait plusieurs jours sur le tabernacle, les enfants d'Israël observaient l'ordre de YHWH, et ne partaient pas.
9:20	Et pour peu de jours que la nuée fût sur le tabernacle, ils campaient sur l'ordre de YHWH, et ils partaient sur l'ordre de YHWH.
9:21	Et quand la nuée y était depuis le soir jusqu'au matin, et que la nuée se levait au matin, ils partaient. De jour ou de nuit, quand la nuée se levait, ils partaient.
9:22	Si la nuée prolongeait sa demeure sur le tabernacle pendant deux jours, ou un mois, ou plus longtemps, les enfants d'Israël restaient campés et ne partaient pas. Mais quand elle se levait, ils partaient.
9:23	Sur l'ordre de YHWH ils campaient, et sur l'ordre de YHWH ils partaient. Ils observaient les injonctions de YHWH, selon l'ordre que YHWH donnait à Moshè.

## Chapitre 10

### Les trompettes d'argent

10:1	Puis YHWH parla à Moshè, en disant :
10:2	Fais-toi deux trompettes en argent, battues au marteau. Elles te serviront pour convoquer l'assemblée et pour le départ des camps.
10:3	Quand on en sonnera, toute l'assemblée se réunira auprès de toi à l'entrée de la tente d'assignation.
10:4	Et quand on sonnera d'une seule, les princes, qui sont les chefs des milliers d'Israël, se réuniront près de toi.
10:5	Mais quand vous sonnerez l'alarme, ceux qui campent à l'orient partiront.
10:6	Et quand vous sonnerez l'alarme une seconde fois, ceux qui campent au midi partiront. On sonnera l'alarme pour leur départ.
10:7	Lorsque vous convoquerez l'assemblée, vous sonnerez, mais vous ne devrez pas faire entendre une alarme.
10:8	Et les fils d'Aaron, les prêtres, sonneront des trompettes. Ce sera un statut perpétuel pour vous et pour vos descendants.
10:9	Et quand vous partirez en guerre dans votre pays contre l'ennemi qui vous combattra, vous sonnerez des trompettes avec un cri de guerre, et YHWH votre Elohîm, se souviendra de vous, et vous serez délivrés de vos ennemis.
10:10	Aussi dans vos jours de joie, dans vos fêtes, et au commencement de vos mois, vous sonnerez des trompettes en offrant vos holocaustes et vos sacrifices d'offrande de paix, et elles vous serviront de souvenir devant votre Elohîm. Je suis YHWH, votre Elohîm.

### La nuée se lève, reprise de la marche dans le désert

10:11	Or il arriva le vingtième jour du second mois de la seconde année, que la nuée se leva de dessus le tabernacle du témoignage.
10:12	Et les enfants d'Israël partirent du désert de Sinaï, selon l'ordre fixé pour leur marche. La nuée se posa dans le désert de Paran.
10:13	Ils partirent donc pour la première fois, suivant l'ordre de YHWH, par l'intermédiaire de Moshè.
10:14	Et la bannière du camp des fils de Yéhouda partit la première, selon leurs armées. Nahshôn, fils d'Amminadab, commandait l'armée de Yéhouda ;
10:15	Netanél, fils de Tsouar, commandait l'armée de la tribu des fils de Yissakar ;
10:16	Éliab, fils de Hélon, commandait l'armée de la tribu des fils de Zebouloun.
10:17	Le tabernacle fut démonté et les fils de Guershon et les fils de Merari qui portaient le tabernacle partirent.
10:18	Puis la bannière du camp de Reouben partit, selon leurs armées. Élitsour, fils de Shedéour, commandait l'armée de Reouben ;
10:19	Sheloumiel, fils de Tsourishaddaï, commandait l'armée de la tribu des fils de Shim’ôn ;
10:20	Éliasaph, fils de Déouel, commandait l'armée des fils de Gad.
10:21	Alors les Kehathites, qui portaient le sanctuaire, partirent. Or, on dressait le tabernacle en attendant leur arrivée.
10:22	Puis la bannière du camp des fils d'Éphraïm partit, selon leurs armées. Éliyshama, fils d'Ammihoud, commandait l'armée d'Éphraïm ;
10:23	Gamaliel, fils de Pedahtsour, commandait l'armée de la tribu des fils de Menashè ;
10:24	Abidan, fils de Guideoni, commandait l'armée de la tribu des fils de Benyamin.
10:25	Enfin la bannière des camps des fils de Dan qui formait l'arrière-garde, partit selon leurs armées. Ahiézer, fils d'Ammishaddaï, commandait l'armée de Dan.
10:26	Paguiel, fils d'Ocran, commandait l'armée de la tribu des fils d'Asher ;
10:27	Ahira, fils d'Énan, commandait l'armée de la tribu des fils de Nephthali.
10:28	Tel fut l'ordre d'après lequel les enfants d'Israël se mirent en marche selon leurs armées. C'est ainsi qu'ils partirent.
10:29	Moshè dit à Hobab, fils de Reouel, le Madianite, beau-père de Moshè : Nous allons au lieu dont YHWH a dit : Je vous le donnerai. Viens avec nous, et nous te ferons du bien, car YHWH a promis de faire du bien à Israël.
10:30	Hobab lui répondit : Je n'irai pas, mais je m'en irai dans mon pays et vers ma parenté.
10:31	Et Moshè lui dit : Je t'en prie, ne nous abandonne pas. Tu seras nos yeux, parce que tu connais bien les lieux où nous aurons à camper dans le désert.
10:32	Et il arrivera, si tu viens avec nous et que ce bonheur que YHWH doit nous accorder sera arrivé, nous te ferons aussi du bien.
10:33	Et ils partirent de la montagne de YHWH et firent trois jours de route. L'arche de l'alliance de YHWH partit devant eux et fit trois jours de route pour leur chercher un lieu de repos.
10:34	Et la nuée de YHWH était sur eux le jour, quand ils partaient du camp.
10:35	Or il arrivait qu'au départ de l'arche, Moshè disait : Lève-toi, ô YHWH, et tes ennemis seront dispersés, et ceux qui te haïssent s'enfuiront de devant toi<!--Ps. 68:2.--> !
10:36	Et quand on la posait, il disait : Reviens YHWH, vers les dix mille milliers d'Israël !

## Chapitre 11

### Jugement contre les murmures du peuple

11:1	Après, il arriva que le peuple murmura, et cela déplut aux oreilles de YHWH. Lorsque YHWH l'entendit, sa colère s'enflamma, et le feu de YHWH s'alluma parmi eux et en consuma l'extrémité du camp.
11:2	Alors le peuple cria à Moshè. Moshè pria YHWH, et le feu s'éteignit.
11:3	Et on appela ce lieu du nom de Tabeéra, parce que le feu de YHWH s'était allumé parmi eux.

### Le peuple regrette l'Égypte

11:4	Et la multitude d'étrangers qui se trouvait au milieu d'eux s'enflamma de convoitise. Les enfants d'Israël eux-mêmes se mirent à pleurer en disant : Qui nous donnera de la viande à manger<!--Ex. 16:3 ; Ps. 106:14 ; 1 Co. 10:6.--> ?
11:5	Nous nous souvenons des poissons que nous mangions en Égypte et qui ne nous coûtaient rien, des concombres, des melons, des poireaux, des oignons et de l'ail.
11:6	Et maintenant nos âmes sont desséchées : plus rien ! Nos yeux ne voient que de la manne<!--Ps. 78:24.--> !
11:7	Or la manne était comme la graine de coriandre, et avait l'apparence du bdellium<!--Une gomme résineuse. Ex. 16:14-31 ; Jn. 6:31-58.-->.
11:8	Le peuple se dispersait et la ramassait, il la moulait aux meules ou la pilait dans un mortier, il la cuisait au pot et en faisait des gâteaux. Elle avait le goût d'une liqueur d'huile fraîche.
11:9	Et quand la rosée descendait la nuit sur le camp, la manne y descendait aussi.

### Moshè dans l'affliction

11:10	Moshè donc entendit le peuple qui pleurait, chacun dans sa famille et à l'entrée de sa tente. La colère de YHWH s'enflamma extrêmement et cela fut mauvais aux yeux de Moshè.
11:11	Et Moshè dit à YHWH : Pourquoi affliges-tu ton serviteur et pourquoi n'ai-je pas trouvé grâce à tes yeux, que tu aies mis sur moi la charge de tout ce peuple ?
11:12	Est-ce moi qui ai conçu tout ce peuple, ou l'ai-je engendré pour que tu me dises : Porte-le dans ton sein comme le nourricier porte un enfant qui tète, porte-le jusqu'au pays que tu as juré à ses pères ?
11:13	D'où aurais-je de la viande pour en donner à tout ce peuple ? Car il pleure auprès de moi, en disant : Donne-nous de la viande à manger !
11:14	Je ne puis, à moi seul, porter tout ce peuple, car il est trop pesant pour moi<!--De. 1:9-12.-->.
11:15	Si tu agis ainsi à mon égard, tue-moi, je te prie donc, si j'ai trouvé grâce à tes yeux, et que je ne voie pas mon malheur.

### YHWH établit 70 anciens autour de Moshè<!--Ex. 18:19.-->

11:16	Alors YHWH dit à Moshè : Rassemble-moi 70 hommes des anciens d'Israël, de ceux que tu connais comme anciens et officiers du peuple. Amène-les à la tente d'assignation et qu'ils s'y présentent avec toi.
11:17	Et j'y descendrai et je parlerai avec toi. Je mettrai de l'Esprit qui est sur toi sur eux, afin qu'ils portent avec toi la charge du peuple et que tu ne la portes pas toi seul.
11:18	Et tu diras au peuple : Sanctifiez-vous pour demain, et vous mangerez de la viande ; puisque vous avez pleuré aux oreilles de YHWH, en disant : Qui nous fera manger de la viande ? Car nous étions bien en Égypte. Ainsi YHWH vous donnera de la viande, et vous en mangerez.
11:19	Vous n'en mangerez pas un jour, ni 2 jours, ni 5 jours, ni 10 jours, ni 20 jours,
11:20	mais jusqu'à un mois entier, jusqu'à ce qu'elle vous sorte par les narines et que vous en ayez du dégoût, parce que vous avez rejeté YHWH qui est au milieu de vous ; vous avez pleuré devant lui, en disant : Pourquoi sommes-nous sortis d'Égypte ?
11:21	Moshè dit : 600 000 hommes de pied forment ce peuple au milieu duquel je suis, et tu as dit : Je leur donnerai de la viande afin qu'ils en mangent un mois entier !
11:22	Leur tuera-t-on des brebis ou des bœufs, en sorte qu'il y en ait assez pour eux ? Ou leur assemblera-t-on tous les poissons de la mer, en sorte qu'ils en aient assez ?
11:23	YHWH répondit à Moshè : La main de YHWH serait-elle trop courte ? Tu verras maintenant si ce que je t'ai dit arrivera ou non<!--Es. 50:2, 59:1-2.-->.
11:24	Moshè donc sortit et rapporta au peuple les paroles de YHWH. Il rassembla 70 hommes des anciens du peuple et les plaça autour de la tente.
11:25	YHWH descendit dans la nuée et parla à Moshè. Il prit de l'Esprit qui était sur lui et le mit sur les 70 hommes anciens. Et dès que l'Esprit reposa sur eux, ils prophétisèrent, mais ils ne continuèrent pas.

### Prophétie d'Eldad et de Médad

11:26	Or il y eut deux hommes restés dans le camp, l'un s'appelait Eldad, et l'autre Médad, sur lesquels l'Esprit reposa. Ils étaient de ceux qui avaient été inscrits, mais ils n'étaient pas allés à la tente, et ils prophétisaient dans le camp.
11:27	Alors un garçon courut le rapporter à Moshè, en disant : Eldad et Médad prophétisent dans le camp.
11:28	Et Yéhoshoua, fils de Noun, serviteur de Moshè depuis sa jeunesse, répondit en disant : Mon seigneur Moshè, empêche-les !
11:29	Et Moshè lui répondit : Es-tu jaloux pour moi ? Ah ! Si tout le peuple de YHWH était prophète ! Si YHWH leur donnait à tous son Esprit !
11:30	Puis Moshè se retira dans le camp, lui et les anciens d'Israël.

### Les cailles et le jugement de YHWH

11:31	Alors YHWH fit lever un vent de la mer qui amena des cailles et les répandit sur le camp, sur une étendue d'environ une journée de chemin dans un sens et dans l'autre, autour du camp, à peu près de 2 coudées au-dessus de la surface du sol<!--Ex. 16:13-15 ; Ps. 78:26-29, 105:40.-->.
11:32	Le peuple se leva tout ce jour-là et toute la nuit, ainsi que tout le jour du lendemain et ramassa les cailles. Celui qui en avait ramassé le moins en avait 10 homers. Ils les étendirent, ils les étendirent pour eux tout autour du camp.
11:33	Comme la chair était encore entre leurs dents, avant qu'ils l'aient mâchée, la colère de YHWH s'enflamma contre le peuple, et il frappa le peuple d'une très grande plaie<!--Ps. 78:30-31.-->.
11:34	On appela ce lieu du nom de Kibroth-Hattaava (tombes de la convoitise) car c'est là qu'on enterra le peuple qui avait convoité.
11:35	Et de Kibroth-Hattaava le peuple s'en alla pour Hatséroth, et il s'arrêta à Hatséroth.

## Chapitre 12

### Myriam et Aaron murmurent contre Moshè

12:1	Alors Myriam et Aaron parlèrent contre Moshè au sujet de la femme éthiopienne<!--Voir commentaire en Ge. 2:13.--> qu'il avait prise, car il avait pris une femme éthiopienne.
12:2	Et ils dirent : Est-ce seulement par Moshè que YHWH parle ? N'est-ce pas aussi par nous qu'il parle ? Et YHWH entendit cela.
12:3	Moshè était un homme très humble, plus que tout autre humain sur la face de la Terre.
12:4	Et soudain YHWH dit à Moshè, à Aaron et à Myriam : Vous trois, allez à la tente d'assignation ! Et ils y allèrent eux trois.
12:5	Alors YHWH descendit dans la colonne de nuée et se tint à l'entrée de la tente. Puis il appela Aaron et Myriam, qui s'avancèrent tous les deux.
12:6	Et il dit : Écoutez maintenant mes paroles ! Lorsqu'il y aura parmi vous un prophète, moi qui suis YHWH, je me ferai bien connaître à lui en vision, et je lui parlerai en rêve.
12:7	Il n'en est pas ainsi de mon serviteur Moshè, qui est fidèle dans toute ma maison<!--Hé. 3:2.-->.
12:8	Je parle avec lui bouche à bouche, dans une claire apparition et sans énigmes<!--Le mot signifie aussi « parole sombre » et « obscure ».--> et il voit une représentation de YHWH. Pourquoi donc n'avez-vous pas craint de parler contre mon serviteur, contre Moshè ?
12:9	Ainsi la colère de YHWH s'embrasa contre eux. Il s'en alla,
12:10	et la nuée se retira de dessus la tente. Et voici, Myriam était frappée d'une lèpre blanche comme la neige. Aaron se tourna vers Myriam et vit qu'elle avait la lèpre.
12:11	Alors Aaron dit à Moshè : Excuse-nous, mon seigneur ! Je t'en prie, ne mets pas sur nous ce péché, car nous avons agi follement, et nous avons péché !
12:12	Je t'en prie qu'elle ne soit pas comme un enfant mort-né, dont la moitié de la chair est déjà consumée quand il sort du ventre de sa mère !
12:13	Alors Moshè cria à YHWH, en disant : El, s'il te plaît ! Guéris-la, s'il te plaît !
12:14	Et YHWH répondit à Moshè : Si son père lui crachait, lui crachait au visage, ne serait-elle pas couverte de honte pendant 7 jours ? Qu'elle soit enfermée 7 jours hors du camp, après cela, elle y sera reçue<!--Lé. 13:46.-->.
12:15	Ainsi Myriam fut enfermée hors du camp 7 jours, et le peuple ne partit pas de là jusqu'à ce que Myriam fût reçue.
12:16	Après cela le peuple partit de Hatséroth, et il campa dans le désert de Paran.

## Chapitre 13

### 12 espions envoyés pour explorer Canaan

13:1	Et YHWH parla à Moshè, en disant :
13:2	Envoie des hommes pour explorer le pays de Canaan que je donne aux enfants d'Israël. Tu enverras un homme de chaque tribu de leurs pères, tous seront des princes d'entre eux.
13:3	Moshè donc les envoya du désert de Paran, d'après l'ordre de YHWH. Tous ces hommes étaient chefs des enfants d'Israël.
13:4	Et voici leurs noms : De la tribu de Reouben : Shammoua, fils de Zakkour.
13:5	De la tribu de Shim’ôn : Shaphath, fils de Hori.
13:6	De la tribu de Yéhouda : Caleb, fils de Yephounné.
13:7	De la tribu de Yissakar : Yigal, fils de Yossef.
13:8	De la tribu d'Éphraïm : Hoshea<!--Hosée.-->, fils de Noun.
13:9	De la tribu de Benyamin : Palthi, fils de Raphou.
13:10	De la tribu de Zebouloun : Gaddiel, fils de Sodi.
13:11	De l'autre tribu de Yossef : la tribu de Menashè, Gaddi, fils de Sousi.
13:12	De la tribu de Dan : Ammiel, fils de Guemalli.
13:13	De la tribu d'Asher : Setour, fils de Miyka'el.
13:14	De la tribu de Nephthali : Nachbi, fils de Vophsi.
13:15	De la tribu de Gad : Guéouel, fils de Maki.
13:16	Ce sont là les noms des hommes que Moshè envoya pour explorer le pays. Moshè donna à Hoshea, fils de Noun, le nom de Yéhoshoua<!--Moshè (Moïse) changea le nom d'Hoshea (Hosée) en y ajoutant le Nom de YHWH. Hoshea signifie « sauveur » et Yéhoshoua (Josué) « YHWH est salut ». Yéhoshoua (Josué) préfigurait Yéhoshoua ha Mashiah (Jésus-Christ) qui nous a délivrés et transportés dans le Royaume des cieux (Col. 1:12-14). Moshè avait compris prophétiquement que seul Yéhoshoua (Jésus) peut nous faire rentrer dans notre héritage.-->.
13:17	Moshè les envoya pour explorer le pays de Canaan. Il leur dit : Montez de ce côté par le sud, puis vous monterez sur la montagne.
13:18	Et vous verrez quel est ce pays-là, et quel est le peuple qui l'habite, s'il est fort ou faible, s'il est en petit ou en grand nombre.
13:19	Et quel est le pays où il habite, s'il est bon ou mauvais, quelles sont les villes dans lesquelles il habite, si c'est dans des camps ou dans des villes fortifiées.
13:20	Et quelle est la terre, si elle est grasse ou maigre, s'il y a des arbres ou non. Ayez bon courage, et prenez du fruit du pays. Or c'était alors le temps des prémices de raisins.
13:21	Étant donc partis, ils explorèrent le pays, depuis le désert de Tsin jusqu'à Rehob, à l'entrée de Hamath.
13:22	Ils montèrent par le sud et ils allèrent jusqu'à Hébron, où étaient Ahiman, Sheshaï et Talmaï, enfants d'Anak. Hébron avait été bâtie 7 ans avant Tsoan en Égypte.
13:23	Ils vinrent jusqu'au torrent d'Eshcol où ils coupèrent un sarment de vigne avec une grappe de raisins. Ils étaient deux à le porter avec une perche. Ils apportèrent aussi des grenades et des figues.
13:24	Et on donna à ce lieu le nom de vallée d'Eshcol à cause de la grappe que les fils d'Israël y coupèrent.
13:25	Et au bout de 40 jours, ils furent de retour de l'exploration du pays.

### Comptes rendus des envoyés

13:26	Et à leur arrivée, ils se rendirent auprès de Moshè et d'Aaron, et de toute l'assemblée des enfants d'Israël, dans le désert de Padan à Qadesh. Ils leur firent le rapport, ainsi qu'à toute l'assemblée et ils leur montrèrent les fruits du pays.
13:27	Ils firent donc leur rapport à Moshè, et lui dirent : Nous avons été dans le pays où tu nous as envoyés. Effectivement, c'est un pays où coulent le lait et le miel, et en voici les fruits.
13:28	Seulement, le peuple qui habite ce pays est puissant, les villes sont fortifiées, très grandes. Nous y avons vu des enfants d'Anak<!--De. 1:24-28.-->.
13:29	Les Amalécites habitent la contrée du midi, les Héthiens, les Yebousiens et les Amoréens habitent la montagne, les Cananéens habitent le long de la mer et vers le rivage du Yarden.
13:30	Caleb fit taire le peuple devant Moshè, et il dit : Montons, montons et prenons possession de ce pays, car nous serons vainqueurs, nous serons vainqueurs !
13:31	Mais les hommes qui y étaient montés avec lui dirent : Nous ne pouvons pas monter contre ce peuple-là, car il est plus fort que nous.
13:32	Et ils décrièrent devant les enfants d'Israël le pays qu'ils avaient exploré, en disant : Le pays que nous avons parcouru pour l'explorer est un pays qui dévore ses habitants, et tous ceux que nous y avons vus sont des gens de grande taille.
13:33	Et nous y avons vu aussi des géants, des enfants d'Anak, de la race des géants, et nous étions à nos yeux et à leurs yeux comme des sauterelles.

## Chapitre 14

### Rébellion et incrédulité d'Israël<!--1 Co. 10:1-5 ; Hé. 3:7-19.-->

14:1	Alors toute l'assemblée éleva la voix et se mit à pousser des cris, et le peuple pleura cette nuit-là.
14:2	Et tous les enfants d'Israël murmurèrent contre Moshè et Aaron, et toute l'assemblée leur dit : Oh ! Si nous étions morts dans le pays d'Égypte ! Ou si nous étions morts dans ce désert<!--De. 1:26-27.--> !
14:3	Et pourquoi YHWH nous fait-il aller dans ce pays, où nous tomberons par l'épée, où nos femmes et nos petits enfants deviendront une proie ? Ne vaut-il pas mieux retourner en Égypte ?
14:4	Et ils se dirent chacun à son frère : Établissons-nous un chef et retournons en Égypte !
14:5	Alors Moshè et Aaron tombèrent sur leurs visages devant toute l'assemblée des enfants d'Israël.
14:6	Et Yéhoshoua, fils de Noun, et Caleb, fils de Yephounné, qui étaient parmi ceux qui avaient exploré le pays, déchirèrent leurs vêtements,
14:7	et parlèrent à toute l'assemblée des enfants d'Israël, en disant : Le pays que nous avons exploré est un très bon pays.
14:8	Si nous sommes agréables à YHWH, il nous fera entrer dans ce pays et il nous le donnera. C'est un pays où coulent le lait et le miel.
14:9	Seulement, ne vous rebellez pas contre YHWH et n'ayez pas peur du peuple du pays, car ils seront notre pain. Leur ombre protectrice s'est écartée d'eux et YHWH est avec nous. N'ayez pas peur d'eux<!--De. 20:3-4.--> !
14:10	Alors toute l'assemblée parlait de les lapider, mais la gloire de YHWH apparut à tous les enfants d'Israël, devant la tente d'assignation.

### Moshè intercède pour le pardon d'Israël

14:11	Et YHWH dit à Moshè : Jusqu'à quand ce peuple me méprisera-t-il et jusqu'à quand ne croira-t-il pas en moi, malgré tous les signes que j'ai faits au milieu de lui ?
14:12	Je le frapperai par la peste et je le détruirai, mais je ferai de toi une nation plus grande et plus puissante que lui.
14:13	Et Moshè dit à YHWH : Mais les Égyptiens l'entendront, car tu as fait monter par ta puissance ce peuple du milieu d'eux<!--Ex. 32:10-12.-->,
14:14	et ils le diront aux habitants de ce pays. Ils ont entendu que toi, YHWH, tu es au milieu de ce peuple, que c'est toi, YHWH, qui te montres à eux les yeux dans les yeux, que ta nuée se tient sur eux, que tu marches devant eux le jour dans une colonne de nuée, et la nuit dans une colonne de feu.
14:15	Si tu fais mourir ce peuple comme un seul homme, les nations qui ont entendu parler de toi diront :
14:16	YHWH n'avait pas le pouvoir de faire entrer ce peuple dans le pays qu'il avait juré de leur donner, il l'a tué dans le désert.
14:17	Maintenant, je te prie, que la puissance d'Adonaï se montre dans sa grandeur, comme tu l'as déclaré, en disant :
14:18	YHWH est lent à la colère et riche en bonté, il ôte l'iniquité et pardonne la transgression, mais il ne tient pas le coupable pour innocent, et il punit l'iniquité des pères sur les fils, jusqu'à la troisième et à la quatrième génération<!--Ex. 20:5, 34:6-7 ; De. 5:9 ; Jon. 4:2 ; Ps. 86:15, 103:8, 145:8.-->.
14:19	Pardonne, je te prie, l'iniquité de ce peuple, selon la grandeur de ta miséricorde, comme tu as pardonné à ce peuple depuis l'Égypte jusqu'ici.

### Réponse de YHWH à Moshè

14:20	Et YHWH dit : Je pardonne selon ta parole.
14:21	Mais aussi vrai que je suis vivant, et que la gloire de YHWH remplira toute la Terre,
14:22	tous ceux qui ont vu ma gloire et les signes que j'ai faits en Égypte et dans le désert, qui m'ont déjà tenté par 10 fois, et qui n'ont pas écouté ma voix<!--Littéralement : « tous ceux qui ont vu la gloire d'Aleph Tav, les signes qu'Aleph Tav a faits en Égypte et dans le désert, qui ont déjà tenté par 10 fois Aleph Tav, et qui n'ont pas écouté la voix ». Voir commentaire en Ge. 1:1.-->,
14:23	tous ceux-là ne verront pas le pays que j'ai juré à leurs pères de leur donner, tous ceux qui m'ont irrité par mépris, ne le verront pas<!--De. 1:35-38 ; No. 32:11.-->.
14:24	Mais parce que mon serviteur Caleb a été animé d'un autre esprit et qu'il a persévéré à me suivre, je le ferai entrer dans le pays où il a été, et ses descendants le posséderont en héritage.
14:25	Or les Amalécites et les Cananéens habitent la vallée. Demain, tournez-vous et partez pour le désert, dans la direction de la Mer Rouge.
14:26	YHWH parla à Moshè et à Aaron, en disant :
14:27	Jusqu'à quand laisserai-je cette méchante assemblée murmurer contre moi ? J'ai entendu les murmures des enfants d'Israël, qui murmuraient contre moi<!--Ps. 106:25.-->.
14:28	Dis-leur donc : Je suis vivant, dit YHWH, si je ne vous traite comme vous avez parlé à mes oreilles !
14:29	Vos cadavres tomberont dans ce désert, et tous ceux d'entre vous qui ont été dénombrés, selon tout le compte que vous en avez fait, depuis l'âge de 20 ans et au-dessus, vous tous qui avez murmuré contre moi ;
14:30	vous n'entrerez pas dans le pays que j'avais juré de vous faire habiter, excepté Caleb, fils de Yephounné, et Yéhoshoua, fils de Noun.
14:31	Et quant à vos petits enfants, dont vous avez dit : Ils deviendront une proie ! Je les y ferai entrer, et ils connaîtront le pays que vous avez méprisé.
14:32	Mais quant à vous, vos cadavres tomberont dans ce désert.
14:33	Mais vos enfants paîtront dans ce désert 40 ans et ils porteront la peine de vos prostitutions, jusqu'à ce que vos cadavres soient tous consumés dans le désert.
14:34	Selon le nombre des jours que vous avez mis à explorer le pays, 40 jours, un jour pour une année, un jour pour une année, vous porterez la peine de vos iniquités pendant 40 ans, un jour pour une année et vous saurez ce que c'est que d'être en opposition avec moi.
14:35	Je suis YHWH, j'ai parlé ! C'est ainsi que je traiterai cette méchante assemblée, qui s'est assemblée contre moi. Ils seront consumés dans ce désert, et ils y mourront.
14:36	Et les hommes que Moshè avait envoyés pour explorer le pays, et qui, à leur retour avaient fait murmurer contre lui toute l'assemblée, en décriant le pays,
14:37	ces hommes qui avaient débité des rumeurs mauvaises et désagréables sur le pays moururent d’une plaie devant YHWH.
14:38	Mais Yéhoshoua, fils de Noun, et Caleb, fils de Yephounné, restèrent seuls vivants parmi ceux qui étaient allés pour explorer le pays.

### Israël battu par les Amalécites et les Cananéens

14:39	Or Moshè dit ces choses à tous les enfants d'Israël, et le peuple fut dans un grand deuil.
14:40	Puis ils se levèrent de bon matin et montèrent au sommet de la montagne, en disant : Nous voici, et nous monterons au lieu dont YHWH a parlé car nous avons péché.
14:41	Mais Moshè leur dit : Pourquoi transgressez-vous le commandement de YHWH ? Cela ne réussira pas.
14:42	Ne montez pas, car YHWH n'est pas au milieu de vous. Ne vous faites pas battre devant vos ennemis<!--De. 1:41-42.-->.
14:43	Car les Amalécites et les Cananéens sont là devant vous, et vous tomberez par l'épée. Parce que vous vous êtes détournés de YHWH, YHWH ne sera pas avec vous.
14:44	Toutefois ils s'obstinèrent à monter au sommet de la montagne, mais l'arche de l'alliance de YHWH et Moshè ne sortirent pas du milieu du camp.
14:45	Alors les Amalécites et les Cananéens qui habitaient sur cette montagne descendirent, les battirent et les taillèrent en pièces jusqu'à Hormah.

## Chapitre 15

### Consignes pour le pays de Canaan

15:1	Puis YHWH parla à Moshè, en disant :
15:2	Parle aux enfants d'Israël et dis-leur : Quand vous serez entrés au pays que je vous donne, où vous devez demeurer,
15:3	et que vous voudrez faire une offrande consumée par le feu à YHWH, un holocauste, ou un sacrifice en accompagnement d'un vœu ou en offrande volontaire, ou bien dans vos fêtes, pour produire avec votre gros ou votre menu bétail un parfum apaisant pour YHWH<!--Ex. 29:18 ; Lé. 22:21.-->,
15:4	celui qui offrira son offrande à YHWH présentera en offrande de grain un dixième de fleur de farine pétrie dans un quart de hin d'huile<!--Lé. 2:1-2.-->,
15:5	et un quart de hin de vin pour la libation que tu feras sur l'holocauste, ou sur un autre sacrifice pour chaque agneau.
15:6	Si c'est pour un bélier, tu feras en offrande de grain deux dixièmes de fleur de farine pétrie dans un tiers de hin d'huile,
15:7	et un tiers de hin de vin pour la libation, comme offrande dont le parfum est apaisant pour YHWH.
15:8	Et si tu sacrifies un veau, soit comme holocauste, soit comme sacrifice en accompagnement d'un vœu, ou comme sacrifice d'offrande de paix à YHWH,
15:9	on présentera en offrande de grain, avec le veau, trois dixièmes de fleur de farine pétrie dans un demi-hin d'huile.
15:10	Et tu offriras la moitié d'un hin de vin pour la libation, en offrande consumée par le feu dont le parfum est apaisant pour YHWH.
15:11	On fera de même pour chaque bœuf, chaque bélier et chaque petit des brebis ou des chèvres.
15:12	Selon le nombre que vous en sacrifierez, vous ferez ainsi à chacun, d'après leur nombre.
15:13	Tous ceux qui sont nés au pays feront ces choses de cette manière, en offrant un sacrifice consumé par le feu dont le parfum est apaisant pour YHWH.

### Torah sur l'étranger vivant au milieu d'Israël

15:14	Si un étranger séjournant chez vous ou établi au milieu de vous depuis plusieurs générations offre une offrande consumée par le feu dont le parfum est apaisant pour YHWH, il l'offrira de la même manière que vous.
15:15	Il y aura un seul statut pour l’assemblée, pour vous et pour l'étranger séjournant parmi vous. Il y aura un seul statut perpétuel dans vos âges : il en sera de l'étranger comme de vous devant YHWH.
15:16	Il y aura une seule torah et une seule ordonnance pour vous et pour l'étranger séjournant au milieu de vous.

### Diverses torahs

15:17	YHWH parla à Moshè, en disant :
15:18	Parle aux enfants d'Israël et dis-leur : Quand vous serez arrivés dans le pays où je vous ferai entrer,
15:19	et que vous mangerez du pain de ce pays, vous en offrirez à YHWH une offrande élevée.
15:20	Vous offrirez en offrande élevée un gâteau, la première de votre pâte. Vous l'offrirez comme ce qu'on prélève de l'aire.
15:21	Vous donnerez pour YHWH la première offrande de votre pâte, dans tous les âges.
15:22	Et lorsque vous aurez péché involontairement<!--Voir commentaire en Lé. 4:2.--> et que vous n'aurez pas fait tous ces commandements que YHWH a fait connaître à Moshè,
15:23	tout ce que YHWH vous a ordonné par Moshè, depuis le jour où YHWH vous a donné ses commandements, et dans la suite dans vos générations.
15:24	S'il arrive que la chose ait été faite involontairement, sans que l'assemblée s'en soit aperçue, toute l'assemblée sacrifiera un jeune taureau en holocauste dont le parfum est apaisant pour YHWH, avec l'offrande de grain et la libation, d'après les règles établies. Elle offrira encore un jeune bouc en sacrifice pour le péché.
15:25	Ainsi le prêtre fera la propitiation pour toute l'assemblée des enfants d'Israël, et il leur sera pardonné car c'est une chose arrivée involontairement, et ils ont apporté leur offrande, un sacrifice consumé par le feu pour YHWH et le sacrifice pour le péché devant YHWH, à cause de leur péché involontaire.
15:26	Alors il sera pardonné à toute l'assemblée des enfants d'Israël et à l'étranger qui séjourne au milieu d'eux, car c'est involontairement que tout le peuple a péché.
15:27	Si c'est une seule personne qui a péché involontairement, elle offrira une chèvre d'un an en offrande pour le péché<!--Lé. 4:27-28.-->.
15:28	Et le prêtre fera la propitiation pour la personne qui aura péché involontairement, pour le péché qu'elle aura commis involontairement devant YHWH. Quand la propitiation aura été faite pour elle, il lui sera pardonné.
15:29	Il y aura une même torah pour celui qui aura fait quelque chose involontairement, tant pour celui qui est né au pays des enfants d'Israël, que pour l'étranger qui fait son séjour parmi eux.
15:30	Mais quant à celui qui aura péché par fierté, tant celui qui est né au pays, que l'étranger, il a outragé YHWH. Cette personne-là sera retranchée du milieu de son peuple.
15:31	Parce qu'elle a méprisé la parole de YHWH et qu'elle a enfreint son commandement : cette personne donc sera retranchée, elle sera retranchée, son iniquité est sur elle.

### Un homme lapidé selon la torah<!--Ro. 3:19, 7:7-11 ; 2 Co. 3:7-9 ; Ga. 3:10.-->

15:32	Or comme les enfants d'Israël étaient dans le désert, on trouva un homme qui ramassait du bois le jour du shabbat.
15:33	Et ceux qui l'avaient trouvé ramassant du bois, l'amenèrent à Moshè, à Aaron et à toute l'assemblée.
15:34	Et on le mit sous garde, car ce qu'on devait lui faire n'avait pas été déclaré.
15:35	Alors YHWH dit à Moshè : Cet homme mourra, il mourra, et toute l'assemblée le lapidera hors du camp.
15:36	Toute l'assemblée donc le mena hors du camp et le lapida, et il mourut, comme YHWH l'avait ordonné à Moshè.
15:37	Et YHWH parla à Moshè, en disant :
15:38	Parle aux enfants d'Israël et dis-leur : Qu'ils se fassent d'âge en âge, des franges aux bords de leurs vêtements, et qu'ils mettent sur les franges au bords de leurs vêtements un cordon de couleur violette<!--De. 22:12 ; Mt. 23:5.-->.
15:39	Quand vous aurez cette frange, vous la regarderez et vous vous souviendrez de tous les commandements de YHWH, pour les mettre en pratique, et vous ne suivrez pas les désirs de vos cœurs et de vos yeux, pour vous laisser entraîner à la prostitution ;
15:40	afin que vous vous souveniez de tous mes commandements, et que vous les fassiez, et que vous soyez saints à votre Elohîm.
15:41	Je suis YHWH, votre Elohîm, qui vous ai fait sortir du pays d'Égypte, pour être votre Elohîm. Je suis YHWH, votre Elohîm.

## Chapitre 16

### La révolte de Koré<!--Jud. 11.-->

16:1	Or Koré<!--Koré, Dathan et Abiram, s'étaient révoltés contre Aaron et Moshè, car ils voulaient s'attribuer l'honneur d'offrir à Elohîm des sacrifices. Ils voulaient exercer la prêtrise alors que YHWH ne les avait pas établis pour le service du culte. Vouloir servir Elohîm sans avoir reçu un appel divin est dangereux.-->, fils de Yitshar, fils de Kehath, fils de Lévi, prit avec lui Dathan et Abiram, fils d'Éliab, et On, fils de Péleth, tous trois fils de Reouben.
16:2	Et ils s'élevèrent contre Moshè, avec 250 hommes des fils d'Israël, qui étaient des princes de l'assemblée, de ceux que l'on convoquait pour tenir le conseil, et qui étaient des gens de renom.
16:3	Et ils s'assemblèrent contre Moshè et contre Aaron, et leur dirent : C'en est assez ! Puisque tous ceux de l'assemblée sont saints, et que YHWH est au milieu d'eux, pourquoi vous élevez-vous au-dessus de l'assemblée de YHWH ?
16:4	Quand Moshè eut entendu cela, il se jeta sur son visage.
16:5	Et il parla à Koré et à toute son assemblée en disant : Au matin, YHWH fera connaître celui qui lui appartient, et celui qui est saint, et il le fera approcher de lui ; il fera approcher de lui celui qu'il aura choisi.
16:6	Faites ceci, prenez des encensoirs, Koré et toute son assemblée.
16:7	Et demain, mettez-y du feu, et mettez-y de l'encens devant YHWH ; et celui que YHWH choisira, c'est celui-là qui sera saint. C'en est assez, fils de Lévi !
16:8	Moshè dit aussi à Koré : Écoutez maintenant, fils de Lévi :
16:9	Est-ce trop peu de chose pour vous, que l'Elohîm d'Israël vous ait séparés de l'assemblée d'Israël, pour vous faire approcher de lui, afin de faire le service du tabernacle de YHWH, et pour vous tenir devant l'assemblée, afin de la servir ?
16:10	Et qu'il t'ait fait approcher de lui, toi et tous tes frères, les fils de Lévi, et vous exigez aussi la prêtrise !
16:11	C'est pourquoi toi et toute ton assemblée, vous vous êtes rassemblés contre YHWH ! Car qui est Aaron pour que vous murmuriez contre lui ?
16:12	Et Moshè envoya appeler Dathan et Abiram, fils d'Éliab, qui répondirent : Nous n'y monterons pas !
16:13	Est-ce trop peu que tu nous aies fait monter hors d'un pays où coulent le lait et le miel, pour nous faire mourir dans le désert ? Vas-tu encore nous gouverner, oui, nous gouverner ?
16:14	Certes, tu ne nous as pas fait venir dans un pays où coulent le lait et le miel ! Et tu ne nous as pas donné un héritage de champs ni de vignes ! Veux-tu crever les yeux de ces gens ? Nous ne monterons pas !
16:15	Alors Moshè fut très fâché, et il dit à YHWH : N'aie pas égard à leur offrande de grain. Je n'ai pas pris d'eux un seul âne, et je n'ai fait de mal à aucun d'eux.
16:16	Puis Moshè dit à Koré : Toi et tous ceux qui sont assemblés avec toi, trouvez-vous demain devant YHWH, toi et eux avec Aaron.
16:17	Et prenez chacun vos encensoirs, et mettez-y de l'encens ; et que chacun présente devant YHWH son encensoir : Il y aura 250 encensoirs ; toi et Aaron aussi, chacun avec son encensoir.
16:18	Ils prirent donc chacun son encensoir, y mirent du feu et y déposèrent de l'encens, et ils se tinrent à l'entrée de la tente d'assignation avec Moshè et Aaron.
16:19	Et Koré fit rassembler contre eux toute l'assemblée à l'entrée de la tente d'assignation ; et la gloire de YHWH apparut à toute l'assemblée.
16:20	Puis YHWH parla à Moshè et à Aaron, en disant :
16:21	Séparez-vous du milieu de cette assemblée, et je les consumerai en un seul instant<!--Ex. 32:10.--> !
16:22	Mais ils tombèrent sur leur visage, et dirent : El ! Elohîm des esprits de toute chair ! Un seul homme a péché, et tu te mettrais en colère contre toute l'assemblée ?
16:23	Et YHWH parla à Moshè, en disant :
16:24	Parle à l'assemblée, et dis lui : Retirez-vous d'auprès de la demeure de Koré, de Dathan, et d'Abiram.
16:25	Moshè donc se leva, et alla vers Dathan et Abiram ; et les anciens d'Israël le suivirent.
16:26	Et il parla à l'assemblée, en disant : Éloignez-vous, je vous prie, d'auprès des tentes de ces méchants hommes, et ne touchez à rien qui leur appartienne, de peur que vous ne soyez consumés pour tous leurs péchés.
16:27	Ils se retirèrent donc d'auprès des demeures de Koré, de Dathan et d'Abiram. Et Dathan et Abiram sortirent et se tinrent debout à l'entrée de leurs tentes, avec leurs femmes, leurs fils, et leurs petits-enfants.
16:28	Et Moshè dit : À ceci vous connaîtrez que YHWH m'a envoyé pour faire toutes ces choses, et que je n'agis pas de moi-même.
16:29	Si ces gens meurent comme tous les humains meurent, et s'ils subissent le sort commun à tous les humains, YHWH ne m'a pas envoyé ;
16:30	mais si YHWH fait une chose nouvelle, et si la terre ouvre sa bouche pour les engloutir avec tout ce qui leur appartient, et qu'ils descendent vivants dans le shéol, vous saurez alors que ces hommes-là ont irrité par mépris YHWH.
16:31	Et il arriva qu'aussitôt qu'il eut achevé de dire toutes ces paroles, la terre qui était sous eux se fendit.
16:32	Et la terre ouvrit sa bouche et les engloutit, avec leurs tentes et tous les gens qui appartenaient à Koré, et tous leurs biens<!--De. 11:6 ; Ps. 106:17.-->.
16:33	Ils descendirent donc vivants dans le shéol, eux et tout ceux qui leur appartenait. La terre les recouvrit et ils disparurent au milieu de l'assemblée.
16:34	Et tout Israël qui était autour d'eux s'enfuit à leurs cris. Ils disaient en effet : Prenons garde que la terre ne nous engloutisse !
16:35	Un feu sortit de la part de YHWH et consuma les 250 hommes qui offraient l'encens.
16:36	Puis YHWH parla à Moshè, en disant :
16:37	Dis à Èl’azar, fils d'Aaron, le prêtre, qu'il ramasse les encensoirs du milieu de l'embrasement, et d'en répandre au loin le feu, car ils sont sanctifiés.
16:38	Avec les encensoirs de ceux qui ont péché contre leurs âmes, que l'on fasse des lames étendues dont on couvrira l'autel. Puisqu'ils ont été offerts devant YHWH et qu'ils sont sanctifiés, ils serviront de signe aux enfants d'Israël.
16:39	Ainsi, Èl’azar le prêtre, prit les encensoirs de cuivre, que ces hommes qui furent brûlés avaient présentés, et on en fit des lames pour couvrir l'autel.
16:40	C'est un souvenir pour les enfants d'Israël, afin qu'aucun étranger qui n'est pas de la race d'Aaron, ne s'approche pour brûler de l'encens devant YHWH, et ne soit comme Koré, et comme ceux qui ont été assemblés avec lui ; selon ce que YHWH avait déclaré par Moshè.

### Le peuple frappé à cause des murmures

16:41	Or dès le lendemain, toute l'assemblée des enfants d'Israël murmura contre Moshè et contre Aaron, en disant : Vous avez fait mourir le peuple de YHWH.
16:42	Et il arriva comme l'assemblée s'amassait contre Moshè et contre Aaron, et comme ils tournaient les regards vers la tente d'assignation, voici la nuée la couvrit, et la gloire de YHWH apparut.
16:43	Moshè et Aaron vinrent devant la tente d'assignation.
16:44	Et YHWH parla à Moshè, en disant :
16:45	Retirez-vous du milieu de cette assemblée, et je les consumerai en un instant. Alors ils se prosternèrent le visage contre terre.
16:46	Et Moshè dit à Aaron : Prends l'encensoir et mets-y du feu de dessus l'autel, poses-y de l'encens et va promptement à l'assemblée, et fais la propitiation pour eux. En effet, une grande colère est sortie de devant YHWH, la plaie a commencé.
16:47	Et Aaron prit l'encensoir, comme Moshè lui avait dit, et il courut au milieu de l'assemblée, et voici la plaie avait déjà commencé sur le peuple. Alors il mit de l'encens et fit la propitiation pour le peuple.
16:48	Et comme il se tenait entre les morts et les vivants, la plaie fut arrêtée.
16:49	Et il y en eut 14 700 qui moururent de cette plaie, outre ceux qui étaient morts à cause de Koré.
16:50	Et Aaron retourna auprès de Moshè, à l'entrée de la tente d'assignation, et la plaie s'arrêta.

## Chapitre 17

### YHWH confirme l'appel d'Aaron, sa verge fleurit

17:1	Après cela YHWH parla à Moshè, en disant :
17:2	Parle aux enfants d'Israël, et prends une verge de chacun d'eux selon la maison de leur père, de tous ceux qui sont les princes, selon la maison de leurs pères, 12 verges, puis tu écriras le nom de chacun sur sa verge.
17:3	Et tu écriras le nom d'Aaron sur la verge de Lévi<!--La verge d'Aaron est une image du Mashiah (Christ) ressuscité. Elle avait produit la vie tandis que celles des autres princes n'avaient produit aucun fruit. Cette histoire nous parle également de la confirmation de l'appel d'Aaron face aux critiques dont il était l'objet. On reconnaît l'arbre par ses fruits (Mt. 7:16-20 ; Lu. 7:17-22).-->. En effet, il y aura une verge pour chaque chef des maisons de leurs pères.
17:4	Et tu les déposeras dans la tente d'assignation, devant le témoignage, où je me rencontre avec vous.
17:5	Et il arrivera que la verge de l'homme que je choisirai fleurira. C'est ainsi que je ferai cesser devant moi les murmures que les enfants d'Israël murmurent contre vous.
17:6	Quand Moshè parla aux enfants d'Israël, tous leurs princes lui donnèrent une verge, chaque prince une verge, selon les maisons de leurs pères, soit 12 verges ; or la verge d'Aaron était au milieu des leurs.
17:7	Et Moshè mit les verges devant YHWH, dans la tente du témoignage.
17:8	Et le lendemain, lorsque Moshè entra dans la tente du témoignage, voici, la verge d'Aaron, avait fleuri, pour la maison de Lévi, et elle avait poussé des boutons, produit des fleurs et mûri des amandes.
17:9	Alors Moshè ôta de devant YHWH toutes les verges et les porta à tous les fils d'Israël, afin qu'ils les voient et qu'ils prennent chacun leurs verges.
17:10	Et YHWH dit à Moshè : Reporte la verge d'Aaron devant le témoignage, pour être conservée comme un signe pour les fils de rébellion, afin que tu fasses cesser de devant moi leurs murmures et qu'ils ne meurent pas<!--Hé. 9:3-5.-->.
17:11	Et Moshè fit ainsi. Il se conforma à l'ordre que YHWH lui avait donné.
17:12	Les enfants d'Israël parlèrent à Moshè, en disant : Voici, nous expirons, nous périssons, nous périssons tous !
17:13	Quiconque s'approche, s'approche du tabernacle de YHWH, meurt. Faut-il donc que nous expirions entièrement ?

## Chapitre 18

### Droits et devoirs des prêtres et des Lévites

18:1	Alors YHWH dit à Aaron : Toi et tes fils, et la maison de ton père avec toi, vous porterez l'iniquité du sanctuaire. Toi et tes fils avec toi vous porterez l'iniquité de votre prêtrise.
18:2	Fais aussi approcher de toi tes frères, la tribu de Lévi, qui est la tribu de ton père, afin qu'ils te soient attachés et qu'ils te servent, mais toi et tes fils avec toi, vous servirez devant la tente du témoignage.
18:3	Ils garderont ce que tu leur ordonneras de garder, et ce qu'il faut garder de toute la tente, mais ils n'approcheront pas des ustensiles du lieu saint, ni de l'autel de peur qu'ils ne meurent, et que vous ne mouriez avec eux.
18:4	Ils te seront donc attachés, et ils garderont tout ce qu'il faut garder dans la tente d'assignation, selon tout le service du tabernacle et aucun étranger n'approchera de vous.
18:5	Mais vous prendrez garde à ce qu'il faut faire dans le lieu saint, et à ce qu'il faut faire à l'autel, afin qu'il n'y ait plus d'indignation sur les enfants d'Israël.
18:6	Car quant à moi voici, j'ai pris vos frères, les Lévites, du milieu des enfants d'Israël, qui sont donnés en pur don pour YHWH, afin qu'ils soient employés au service de la tente d'assignation.
18:7	Mais toi et tes fils avec toi, vous observerez la fonction de votre prêtrise en tout ce qui concerne l'autel et ce qui est au dedans du voile, et vous y ferez le service. J'établis votre prêtrise en office de pur don. C'est pourquoi si un étranger en approche, on le fera mourir.
18:8	YHWH dit encore à Aaron : Voici, je t'ai donné la garde de mes offrandes prélevées sur toutes les choses consacrées par les enfants d'Israël. Je te les ai données, à toi et à tes enfants, par ordonnance perpétuelle, à cause de l'onction.
18:9	Voici ce qui sera pour toi parmi les saints des saints qui n'auront pas été brûlés : toutes leurs offrandes avec toutes leurs offrandes de grain, tous leurs sacrifices pour le péché et tous leurs sacrifices de culpabilité qu'ils m'apporteront. Ce sont les saints des saints pour toi et pour tes enfants.
18:10	Vous les mangerez dans le saint des saints. Tout mâle en mangera, vous les regarderez comme saintes<!--Lé. 6:17-22, 7:6, 10:13.-->.
18:11	Voici encore ce qui t'appartiendra : tous les présents que les enfants d'Israël présenteront par élévation et en les agitant de côté et d'autre, je te les donne à toi, à tes fils, et à tes filles avec toi, par une loi perpétuelle. Quiconque sera pur dans ta maison en mangera<!--Lé. 7:34, 10:14.-->.
18:12	Je te donne aussi leurs premiers produits qu'ils offriront à YHWH : tout ce qu'il y aura de meilleur en huile, et tout le meilleur du moût et du blé.
18:13	Les prémices de toutes les choses que leur terre produira, et qu'ils apporteront à YHWH t'appartiendront. Quiconque sera pur dans ta maison, en mangera.
18:14	Tout ce qui sera voué à une entière destruction en Israël t'appartiendra<!--Lé. 27:28 ; Ez. 44:29.-->.
18:15	Tout premier-né de toute chair, qu'ils offriront à YHWH, tant des humains que des animaux, t'appartiendra. Mais tu rachèteras, tu rachèteras le premier-né de l'être humain et tu rachèteras le premier-né d'un animal impur.
18:16	Et ceux qui doivent être rachetés, depuis l'âge d'un mois, tu les rachèteras selon ton estimation que tu en feras, au prix de 5 sicles d'argent, selon le sicle du lieu saint, qui est de 20 guéras.
18:17	Mais tu ne feras pas racheter le premier-né du bœuf, ni le premier-né de la brebis, ni le premier-né de la chèvre : ce sont des choses saintes. Tu aspergeras l'autel de leur sang et tu brûleras leur graisse. Ce sera une offrande consumée par le feu dont le parfum est apaisant pour YHWH.
18:18	Mais leur chair t'appartiendra, comme la poitrine qu'on agite de côté et d'autre, et comme l'épaule droite.
18:19	Je t'ai donné, à toi et à tes fils, et à tes filles avec toi, par une loi perpétuelle, toutes les offrandes présentées par élévation des choses sanctifiées, que les enfants d'Israël offriront à YHWH. C'est une alliance de sel<!--Le sel est un aliment pratiquement impérissable et incorruptible. Dans l'Antiquité, il symbolisait l'incorruptibilité (Lé. 2:13).--> et à perpétuité devant YHWH, pour toi et pour ta postérité avec toi.
18:20	Puis YHWH dit à Aaron : Tu ne posséderas rien dans leur pays, et il n'y aura pas de part pour toi au milieu d'eux. C'est moi qui suis ta part et ta possession au milieu des enfants d'Israël<!--De. 10:9, 18:2 ; Ez. 44:28.-->.

### Torah sur les dîmes (De. 14:22-29)

18:21	Je donne comme possession aux fils de Lévi toutes les dîmes<!--Il y avait plusieurs sortes de dîmes dans la loi mosaïque :\\- La 1ère dîme : le peuple devait payer une dîme générale au bénéfice des Lévites (No. 18:21).\\Toutes les tribus d'Israël, à l'exception des Lévites, eurent une possession géographique qu'ils reçurent comme héritage après leur entrée en Canaan. Mais les Lévites devaient accomplir une tâche particulière au sein de la nation. Ils devaient s'occuper du service dans la tente d'assignation. En compensation de ce service, ils devaient percevoir un impôt de 10% des revenus de tous les Israélites.\\- La 2ème dîme : les Lévites devaient payer la « dîme de la dîme », au bénéfice des prêtres (No. 18:25-31).\\Tous les prêtres étaient des Lévites, mais tous les Lévites n'étaient pas des prêtres. Les prêtres descendaient d'Aaron et exerçaient des responsabilités particulières dans la tente d'assignation, puis dans le temple. Cette seconde dîme permettait aux prêtres d'être nourris et assurait donc le bon fonctionnement du service du temple.\\- La 3ème dîme : tous les Israélites devaient conserver une dîme de toute leur production en prévision de leurs pèlerinages annuels à Yeroushalaim (Jérusalem) (De. 14:22-26).\\Trois fois par an, tout le peuple devait s'assembler à Yeroushalaim, l'endroit choisi par le Seigneur, à l'occasion des principales fêtes. Elohîm avait prévu que chacun puisse disposer de ressources suffisantes pour leur permettre de se réjouir pleinement à ces occasions. C'est pour cela qu'ils devaient mettre de côté 10% de leurs productions agricoles annuelles. Il est intéressant de noter que la dîme n'était jamais payée en argent, mais toujours en nature.\\- La 4ème dîme : il fallait payer une dîme spéciale à l'intention des pauvres, des orphelins et des veuves (De. 14:28-29).\\Certains affirment que la dîme existait bien avant la promulgation de la torah. Mais ils ignorent que la Bible parle de plusieurs sortes de lois.\\- Les lois cérémonielles (Hé. 9:1)\\Ces lois étaient relatives au culte et concernaient le tabernacle puis le temple, les sacrifices, les ablutions (Lé. 16 ; Hé. 9:1-10). Les dîmes (la dîme des prêtres) devaient être amenées dans le temple (Mal. 3:10), elles faisaient donc partie des lois cérémonielles. Or les Lévites et les prêtres de la Première Alliance n'existent plus sous la Nouvelle Alliance car les enfants d'Elohîm sont un royaume de rois et de prêtres (Ap. 1:6, 5:10).\\- Les lois morales (Ex. 20:1-17). Elohîm est saint et il veut un peuple saint qui marche dans sa crainte, dans la sainteté et dans l'obéissance. Lé. 18 nous parle des lois morales ; elles n'ont pas été abolies, elles existent toujours. Elles sont inscrites dans la conscience de l'homme, elles sont gravées dans nos cœurs (Hé. 8:10).\\- Les lois sociales (Ex. 21:1-24). Ce sont des lois civiles régissant la vie sociale d'Israël, comme nous pouvons le lire dans Ex. 21 par exemple. Ces lois n'ont rien à voir avec les croyants de la Nouvelle Alliance. Les lois morales témoignent de la nature d'Elohîm, ce sont des lois éternelles qui existaient bien avant Abraham. Les lois cérémonielles ont commencé dès la fondation du monde (Ap. 13:8) car l'Agneau d'Elohîm était immolé avant la fondation du monde (1 Pi. 1:19-20). Seules les lois sociales ont débuté avec Moshè (Moïse) car elles concernaient exclusivement les Israélites. Ces trois sortes de lois ont été institutionnalisées par Moshè, mais les deux premières (morales et cérémonielles) existaient avant ce dernier. Les quatre sortes de dîmes faisaient bel et bien partie des lois sociales et cérémonielles. Or ces lois ne sont plus d'actualité sous la Nouvelle Alliance. En conclusion, nous pouvons dire que Yéhoshoua (Jésus) nous a rachetés en accomplissant les lois cérémonielles afin que nous pratiquions les lois morales (Ep. 2:10). Voir également commentaire en Mal. 3:10.--> d'Israël pour le service auquel ils sont employés, le service de la tente d'assignation.
18:22	Et les enfants d'Israël n'approcheront plus de la tente d'assignation, de peur qu’ils ne se chargent d’un péché et qu’ils ne meurent.
18:23	Mais les Lévites accompliront le service de la tente d'assignation et ils resteront chargés de leurs iniquités. Ce statut sera perpétuel parmi vos descendants, et ils ne posséderont pas d'héritage parmi les enfants d'Israël.
18:24	Car je donne comme possession aux Lévites les dîmes que les enfants d'Israël présenteront à YHWH en offrande élevée. C'est pourquoi je dis d'eux qu'ils n'auront pas d'héritage parmi les enfants d'Israël.
18:25	Puis YHWH parla à Moshè, en disant :
18:26	Tu parleras aussi aux Lévites, et tu leur diras : Quand vous recevrez des enfants d'Israël les dîmes que je vous donne de leur part comme possession, vous en offrirez l'offrande élevée à YHWH, la dîme de la dîme.
18:27	Votre offrande élevée vous sera comptée comme le blé qu'on prélève de l'aire, comme la plénitude qu'on prélève de la cuve.
18:28	C'est ainsi que vous prélèverez une offrande pour YHWH de toutes les dîmes que vous recevrez des enfants d'Israël, et vous donnerez au prêtre Aaron l'offrande que vous en aurez prélevée pour YHWH.
18:29	Sur tous les dons qui vous seront faits, vous prélèverez toute l'offrande élevée pour YHWH. Sur tout ce qu'il y aura de meilleur, vous prélèverez la portion consacrée.
18:30	Et tu leur diras : Quand vous aurez offert en offrande élevée le meilleur de la dîme, elle sera comptée aux Lévites comme le revenu de l'aire, et comme le revenu de la cuve.
18:31	Et vous la mangerez en tout lieu, vous et votre maison, car c'est votre salaire pour le service que vous faites dans la tente d'assignation.
18:32	Vous ne serez pas coupables de péché au sujet de la dîme, quand vous en aurez offert en offrande élevée sur ce qu'il y aura de meilleur et vous ne souillerez pas les choses saintes des enfants d'Israël et vous ne mourrez pas.

## Chapitre 19

### La jeune vache rousse ; l'eau de purification

19:1	YHWH parla à Moshè et à Aaron, en disant :
19:2	Voici le statut de la torah que YHWH a ordonné, en disant : Parle aux enfants d'Israël, et qu'ils t'amènent une jeune vache rousse, entière, sans défaut, et qui n'ait pas porté le joug.
19:3	Puis vous la donnerez à Èl’azar, le prêtre, qui la mènera hors du camp, et on l'égorgera en sa présence<!--Lé. 4:12 ; Hé. 13:11-12.-->.
19:4	Ensuite, Èl’azar, le prêtre, prendra de son sang avec son doigt, et fera 7 fois l'aspersion du sang vers le devant de la tente d'assignation.
19:5	On brûlera la jeune vache en sa présence. On brûlera sa peau, sa chair, son sang et ses excréments<!--Ex. 29:14.-->.
19:6	Le prêtre prendra du bois de cèdre, de l'hysope, et de l'écarlate de cochenille, et les jettera dans le feu où sera brûlée la jeune vache.
19:7	Puis le prêtre lavera ses vêtements et son corps avec de l'eau, après cela, il rentrera dans le camp. Le prêtre sera impur jusqu'au soir.
19:8	Celui qui l'aura brûlée, lavera ses vêtements dans l'eau, il lavera aussi dans l'eau son corps et il sera impur jusqu'au soir.
19:9	Et un homme pur ramassera les cendres de la jeune vache, et les mettra hors du camp, dans un lieu pur. Elles seront gardées pour l'assemblée des enfants d'Israël afin d'en faire l'eau contre l'impureté. C'est une purification pour le péché.
19:10	Celui qui aura ramassé les cendres de la jeune vache, lavera ses vêtements, et sera impur jusqu'au soir. Ce sera un statut perpétuel pour les enfants d'Israël, et pour l'étranger en séjour au milieu d'eux.
19:11	Celui qui touchera un mort, le corps d'un être humain quel qu'il soit, sera impur pendant 7 jours<!--Ag. 2:13.-->.
19:12	Il se purifiera avec cette eau le troisième jour et le septième jour, et il sera pur. Mais s'il ne se purifie pas le troisième jour, il ne sera pas pur le septième jour.
19:13	Toute personne qui touchera un mort, le corps d'un être humain qui sera mort et qui ne se purifiera pas, souille le tabernacle de YHWH. Elle sera retranchée d'Israël. Comme elle n'a pas été aspergée d'eau contre l'impureté, elle est impure et son impureté reste sur elle.
19:14	Voici la torah : lorsqu'un être humain mourra dans une tente, quiconque entrera dans la tente, et quiconque se trouvera dans la tente sera impur pendant 7 jours.
19:15	Aussi tout vase découvert, sur lequel il n'y aura pas de couvercle attaché, sera impur.
19:16	Et quiconque touchera, dans les champs, un homme qui aura été blessé mortellement par l'épée, ou un mort, ou des ossements d'êtres humains, ou un sépulcre, sera impur durant 7 jours.
19:17	Et on prendra pour celui qui est impur de la cendre de la jeune vache brûlée pour le péché, et on la mettra dans un vase avec de l'eau vive par-dessus.
19:18	Puis un homme pur prendra de l'hysope, et la trempera dans l'eau. Il en fera aspersion sur la tente, et sur tous les ustensiles, et sur toutes les personnes qui auront été là, et sur celui qui a touché des ossements ou un homme blessé mortellement, ou un mort, ou un sépulcre.
19:19	Celui qui est pur fera l'aspersion sur celui qui est impur, le troisième jour et le septième jour, et il le purifiera le septième jour. Puis il lavera ses vêtements, et se lavera dans l'eau, et il sera pur le soir.
19:20	Mais un homme qui sera impur et qui ne se purifiera pas sera retranché du milieu de l'assemblée, parce qu'il a souillé le sanctuaire de YHWH. Comme il n'a pas été aspergé d'eau contre l'impureté, il est impur.
19:21	Et ce sera pour eux un statut perpétuel. Celui qui fera l'aspersion de l'eau contre l'impureté lavera ses vêtements et quiconque touchera l'eau contre l'impureté sera impur jusqu'au soir.
19:22	Et tout ce que l'homme impur touchera sera souillé, et la personne qui le touchera sera impure jusqu'au soir.

## Chapitre 20

### Mort de Myriam

20:1	Or toute l'assemblée des enfants d'Israël arriva dans le désert de Tsin au premier mois, et le peuple s'arrêta à Qadesh. C'est là que mourut Myriam, et qu'elle fut ensevelie.

### Murmures du peuple à cause du manque d'eau<!--De. 32:51 ; cp. Ex. 17:1-7.-->

20:2	Il n'y avait pas d'eau pour l'assemblée. Ils se soulevèrent contre Moshè et contre Aaron.
20:3	Le peuple contesta contre Moshè et ils lui dirent : Pourquoi ne sommes-nous pas morts quand nos frères moururent devant YHWH ?
20:4	Pourquoi avez-vous fait venir l'assemblée de YHWH dans ce désert ? Pour nous y faire mourir, nous et notre bétail<!--Ex. 17:3.--> ?
20:5	Pourquoi nous avez-vous fait monter hors d'Égypte et nous avez-vous amenés dans ce méchant lieu ? Ce n'est pas un lieu où l'on puisse semer et il n'y a ni figuier, ni vigne, ni grenadier ni eau à boire
20:6	Alors Moshè et Aaron se retirèrent de devant l'assemblée à l'entrée de la tente d'assignation et ils tombèrent sur leurs faces et la gloire de YHWH leur apparut.

### Incrédulité de Moshè et d'Aaron à Meriba

20:7	YHWH parla à Moshè, en disant :
20:8	Prends la verge, et convoque l'assemblée, toi et Aaron, ton frère. Vous parlerez en leur présence au rocher<!--Le Mashiah, le rocher des âges (Es. 8:13-17 ; 1 Co. 10:1-4).-->, et il donnera son eau. Ainsi tu leur feras sortir de l'eau du rocher, et tu donneras à boire à l'assemblée et à leur bétail.
20:9	Moshè prit la verge qui était devant YHWH, comme il lui avait ordonné.
20:10	Moshè et Aaron convoquèrent l'assemblée devant le rocher. Et il leur dit : Écoutez donc, rebelles ! Est-ce de ce rocher que nous vous ferons sortir de l'eau ?
20:11	Puis Moshè leva sa main, et frappa deux fois le rocher avec sa verge et il en sortit des eaux en abondance. L'assemblée but, et leur bétail aussi.
20:12	Alors YHWH dit à Moshè et à Aaron : Parce que vous n'avez pas cru en moi pour me sanctifier aux yeux des enfants d'Israël, à cause de cela vous ne ferez pas entrer cette assemblée dans le pays que je lui donne.
20:13	Ce sont là les eaux de Meriba, où les enfants d'Israël contestèrent avec YHWH, et il se sanctifia en eux.

### La méchanceté d'Édom<!--Ge. 25:30 ; Ab. 10.-->

20:14	Puis Moshè envoya des messagers de Qadesh au roi d'Édom pour lui dire : Ainsi parle ton frère Israël : Tu sais toutes les détresses qui nous ont atteints.
20:15	Nos pères descendirent en Égypte, et nous avons habité en Égypte longtemps. Mais les Égyptiens nous ont maltraités, nous et nos pères.
20:16	Et nous avons crié à YHWH, et il a entendu nos cris. Il a envoyé l'Ange et nous a fait sortir d'Égypte. Et voici, nous sommes à Qadesh, ville qui est à l'extrémité de ton territoire<!--Ex. 2:23, 23:20 ; Ac. 7:30-38.-->.
20:17	Je te prie, laisse-nous passer par ton pays. Nous ne traverserons ni les champs ni les vignes, et nous ne boirons l'eau d'aucun puits. Nous marcherons par le chemin royal, nous ne nous détournerons ni à droite ni à gauche, jusqu'à ce que nous ayons passé ton territoire.
20:18	Et Édom lui dit : Tu ne passeras pas par mon pays, de peur que je ne sorte en armes à ta rencontre.
20:19	Les enfants d'Israël lui répondirent : Nous monterons par le grand chemin, et si nous buvons de tes eaux, moi et mes bêtes, je t'en payerai le prix. Ce n'est pas une affaire que de me laisser passer avec mes pieds.
20:20	Mais il lui répondit : Tu ne passeras pas ! Et Édom sortit à sa rencontre avec un peuple nombreux, d'une main forte.
20:21	Ainsi Édom ne voulut pas permettre à Israël de passer par ses frontières, c'est pourquoi Israël se détourna de lui.
20:22	Et toute l'assemblée des enfants d'Israël partit de Qadesh et arriva à la montagne de Hor.

### Mort d'Aaron

20:23	Et YHWH parla à Moshè et à Aaron à la montagne de Hor, près des frontières du pays d'Édom, en disant :
20:24	Aaron sera recueilli auprès de son peuple, car il n'entrera pas dans le pays que je donne aux enfants d'Israël, parce que vous avez été rebelles à mon commandement aux eaux de la dispute<!--« Meriba ».-->.
20:25	Prends donc Aaron et Èl’azar, son fils, et fais-les monter sur la montagne de Hor.
20:26	Dépouille Aaron de ses vêtements, et tu en revêtiras Èl’azar, son fils. C'est là qu'Aaron sera recueilli et qu'il mourra.
20:27	Moshè fit ce que YHWH avait ordonné. Ils montèrent sur la montagne de Hor, aux yeux de toute l'assemblée.
20:28	Et Moshè dépouilla Aaron de ses vêtements et en fit revêtir Èl’azar, son fils. Aaron mourut là, au sommet de la montagne. Moshè et Èl’azar descendirent de la montagne<!--De. 10:6.-->.
20:29	Toute l'assemblée, toute la maison d'Israël, voyant qu'Aaron était mort, le pleurèrent 30 jours.

## Chapitre 21

### Les Cananéens livrés à Israël

21:1	Le roi d'Arad, un Cananéen qui habitait le midi, apprit qu'Israël venait par le chemin d'Atharim. Il combattit Israël et emmena des prisonniers.
21:2	Alors Israël fit un vœu à YHWH, en disant : Si tu livres ce peuple entre mes mains, je dévouerai ses villes par interdit.
21:3	Et YHWH écouta la voix d'Israël et livra entre ses mains les Cananéens. On les dévoua par interdit, avec leurs villes, et l'on donna à ce lieu le nom de Hormah.

### Le serpent en cuivre<!--Jn. 3:14-15.-->

21:4	Puis ils partirent de la montagne de Hor, par le chemin de la Mer Rouge, pour faire le tour du pays d'Édom. L'âme du peuple s'impatienta en route.
21:5	Et le peuple parla contre Elohîm et contre Moshè en disant : Pourquoi nous avez-vous fait monter hors d'Égypte pour nous faire mourir dans le désert ? Car il n'y a pas de pain ni d'eau, et notre âme est dégoûtée de cette nourriture misérable.
21:6	Et YHWH envoya contre le peuple des serpents brûlants. Ils mordirent tellement le peuple qu'il en mourut un grand nombre en Israël<!--1 Co. 10:9.-->.
21:7	Alors le peuple vint vers Moshè et dit : Nous avons péché, car nous avons parlé contre YHWH et contre toi. Invoque YHWH afin qu'il éloigne de nous les serpents ! Et Moshè pria pour le peuple.
21:8	Et YHWH dit à Moshè : Fais-toi un serpent brûlant<!--Serpent venimeux (le venin ayant un effet de brûlure).--> et mets-le sur une perche. Quiconque aura été mordu et le regardera conservera la vie.
21:9	Moshè fit un serpent en cuivre<!--Voir Jn. 3:14-16. Ceux qui regardent à Yéhoshoua ha Mashiah (Jésus-Christ), et non aux hommes, obtiennent la délivrance. Le cuivre nous parle du jugement (Job 20:24), le serpent de la malédiction (Ge. 3:14), et la perche parle de la croix (1 Co. 1:18). Yéhoshoua a pris nos malédictions sur la croix de Golgotha (Ga. 3:13).--> et le mit sur une perche. Quiconque avait été mordu par un serpent et regardait le serpent en cuivre conservait la vie.
21:10	Les enfants d'Israël partirent et campèrent à Oboth.
21:11	Et ils partirent d'Oboth et ils campèrent en Iyey-Abariym, dans le désert qui est vis-à-vis de Moab, vers le soleil levant.
21:12	Puis ils partirent de là et campèrent vers le torrent de Zéred.
21:13	Et ils partirent de là et campèrent de l'autre côté de l'Arnon, qui est dans le désert, en sortant du territoire des Amoréens, car l'Arnon est la frontière de Moab, entre les Moabites et les Amoréens<!--Jg. 11:18.-->.
21:14	C'est pourquoi il est dit dans le livre des batailles de YHWH : Vaheb en Soupha, et les torrents de l'Arnon,
21:15	et le cours des torrents qui s'étend du côté d'Ar et touche à la frontière de Moab.
21:16	De là ils allèrent à Beer. C'est le puits au sujet duquel YHWH dit à Moshè : Rassemble le peuple, et je leur donnerai de l'eau.
21:17	Alors Israël chanta ce cantique : Monte, puits ! Chantez-le en vous répondant les uns aux autres.
21:18	Puits que des princes ont creusés, que les grands du peuple ont creusés, avec le législateur, avec leurs bâtons ! Du désert ils vinrent à Matthana,
21:19	de Matthana à Nahaliel, de Nahaliel à Bamoth,
21:20	de Bamoth à la vallée qui est dans le territoire de Moab, au sommet de Pisga, et qui regarde vers Yeshiymown.

### Israël bat le roi des Amoréens et le roi de Bashân

21:21	Puis Israël envoya des messagers à Sihon, roi des Amoréens, pour lui dire :
21:22	Laisse-moi passer par ton pays. Nous ne nous détournerons ni dans les champs, ni dans les vignes, et nous ne boirons l'eau d'aucun des puits. Mais nous marcherons par la route royale, jusqu'à ce que nous ayons passé ton territoire.
21:23	Mais Sihon ne permit pas à Israël de passer sur son territoire. Il rassembla tout son peuple et sortit à la rencontre d'Israël, dans le désert. Il vint à Yahats et combattit Israël<!--De. 2:26-30.-->.
21:24	Israël le fit passer au fil de l'épée et conquit son pays, depuis l'Arnon jusqu'à Yabbok, et jusqu'à la frontière des fils d'Ammon. En effet, la frontière des fils d'Ammon était forte<!--De. 2:30, 29:7 ; Ps. 135:6-12.-->.
21:25	Et Israël prit toutes les villes qui étaient là. Israël habita dans toutes les villes des Amoréens, à Hesbon, et dans toutes les villes de son ressort.
21:26	Or Hesbon était la ville de Sihon, roi des Amoréens, qui avait le premier fait la guerre au roi de Moab, et pris sur lui tout son pays jusqu'à l'Arnon.
21:27	C'est pourquoi les poètes disent : Venez à Hesbon ! Que la ville de Sihon soit rebâtie et fortifiée !
21:28	Car le feu est sorti de Hesbon et la flamme de la cité de Sihon. Elle a consumé Ar-Moab, les habitants des hauteurs de l'Arnon.
21:29	Malheur à toi, Moab ! Peuple de Kemosh, tu es perdu ! Il a livré ses fils qui se sauvaient et ses filles en captivité à Sihon, roi des Amoréens<!--Jé. 48:46.-->.
21:30	Nous les avons défaits à coups de flèches : De Hesbon à Dibon tout est détruit, nous les avons mis en déroute jusqu'à Nophach, jusqu'à Médeba.
21:31	Israël s'établit dans le pays des Amoréens.
21:32	Puis Moshè envoya des gens pour reconnaître Ya`azeyr, ils prirent les villes de son ressort, et chassèrent les Amoréens qui y étaient.
21:33	Ensuite, ils se tournèrent et montèrent par le chemin de Bashân. Og, roi de Bashân, sortit à leur rencontre, avec tout son peuple pour les combattre à Édréi.
21:34	Et YHWH dit à Moshè : N'aie pas peur de lui, car je le livre entre tes mains ainsi que tout son peuple et son pays. Tu le traiteras comme tu as traité Sihon, roi des Amoréens, qui habitait à Hesbon<!--De. 3:1-2.-->.
21:35	Ils le battirent, lui, ses fils et tout son peuple, jusqu'à ce qu'il ne lui reste aucun survivant, et ils s'emparèrent de son pays.

## Chapitre 22

### Balak cherche à maudire Israël ; Balaam<!--2 Pi. 2:15 ; Jud. 11 ; Ap. 2:14.--> séduit par les honneurs.

22:1	Puis les enfants d'Israël partirent, et ils campèrent dans les régions arides de Moab, au-delà du Yarden, vis-à-vis de Yeriycho.
22:2	Balak, fils de Tsippor, vit tout ce qu'Israël avait fait aux Amoréens.
22:3	Et Moab eut une grande frayeur du peuple, parce qu'il était en grand nombre, il fut saisi de terreur en face des enfants d'Israël.
22:4	Et Moab dit aux anciens de Madian : Maintenant cette multitude va brouter tout ce qui nous entoure, comme le bœuf broute l'herbe des champs. Balak, fils de Tsippor, était alors roi de Moab.
22:5	Il envoya des messagers auprès de Balaam, fils de Beor, à Pethor, située sur le fleuve, dans le pays des fils de son peuple, afin de l'appeler et de lui dire : Voici, un peuple est sorti d'Égypte, il couvre la surface de la terre, et il habite vis-à-vis de moi.
22:6	Viens donc maintenant, je t'en prie, maudis ce peuple pour moi, car il est plus puissant que moi. Peut-être ainsi pourrai-je le battre et le chasser du pays car, je le sais, celui que tu bénis est béni et celui que tu maudis est maudit.
22:7	Les anciens de Moab s'en allèrent avec les anciens de Madian, ayant dans leurs mains de quoi payer le devin. Ils arrivèrent auprès de Balaam, et lui rapportèrent les paroles de Balak.
22:8	Il leur répondit : Restez ici cette nuit, et je vous répondrai d'après ce que YHWH me dira. Et les chefs des Moabites restèrent chez Balaam.
22:9	Et Elohîm vint à Balaam, et dit : Qui sont ces hommes que tu as chez toi ?
22:10	Et Balaam répondit à Elohîm : Balak, fils de Tsippor, roi de Moab, les a envoyés pour me dire :
22:11	Voici que le peuple sorti d'Égypte couvre la face de la terre. Viens donc et maudis-le pour moi. Peut-être ainsi pourrai-je le combattre et le chasser.
22:12	Et Elohîm dit à Balaam : Tu n'iras pas avec eux, et tu ne maudiras pas ce peuple, car il est béni.
22:13	Et Balaam se leva le matin, et il dit aux chefs qui avaient été envoyés par Balak : Retournez dans votre pays, car YHWH refuse de me laisser venir avec vous.
22:14	Ainsi les chefs des Moabites se levèrent et retournèrent auprès de Balak, et dirent : Balaam a refusé de venir avec nous.
22:15	Et Balak envoya encore des chefs en plus grand nombre, et plus considérés que les premiers.
22:16	Ils arrivèrent auprès de Balaam, et lui dirent : Ainsi parle Balak, fils de Tsippor : Que l'on ne t'empêche donc pas de venir vers moi,
22:17	car je t'honorerai, je t'honorerai beaucoup et je ferai tout ce que tu me diras. Je te prie donc viens, maudis-moi ce peuple.
22:18	Et Balaam répondit, et dit aux serviteurs de Balak : Quand Balak me donnerait sa maison pleine d'or et d'argent, je ne pourrais pas transgresser l'ordre de YHWH, mon Elohîm ; je ne pourrais faire aucune chose, ni petite ni grande.
22:19	Maintenant, je vous prie, vous aussi restez ici cette nuit, et je saurai ce que YHWH aura de plus à me dire.
22:20	Elohîm vint la nuit à Balaam et lui dit : Puisque ces hommes sont venus t'appeler, lève-toi, et va avec eux. Mais quoi qu'il en soit, tu feras ce que je te dirai.
22:21	Ainsi Balaam se leva le matin, sella son ânesse, et partit avec les chefs de Moab.
22:22	Mais la colère d'Elohîm s'enflamma parce qu'il était parti. L'Ange de YHWH se plaça sur le chemin pour s'opposer à lui. Balaam était monté sur son ânesse, et ses deux serviteurs étaient avec lui.
22:23	L'ânesse vit l'Ange de YHWH qui se tenait sur le chemin, son épée nue dans la main. Elle se détourna du chemin et alla dans les champs. Balaam frappa l'ânesse pour la ramener dans le chemin<!--2 Pi. 2:16 ; Jud. 11.-->.
22:24	L'Ange de YHWH se plaça dans un sentier entre les vignes. Il y avait un mur de chaque côté.
22:25	L'ânesse vit l'Ange de YHWH. Elle se serra contre le mur, et elle serra le pied de Balaam contre le mur. Balaam la frappa de nouveau.
22:26	Et l'Ange de YHWH passa plus loin et s'arrêta dans un lieu étroit où il n'y avait pas d'espace pour se détourner à droite ou à gauche.
22:27	L'ânesse vit l'Ange de YHWH et elle s'abattit sous Balaam. La colère de Balaam s'enflamma et il frappa l'ânesse avec son bâton.
22:28	Alors YHWH fit parler l'ânesse, et elle dit à Balaam : Que t'ai-je fait, pour que tu m'aies déjà frappée trois fois ?
22:29	Et Balaam répondit à l'ânesse : C'est parce que tu t'es moquée de moi. Si j'avais une épée dans la main, je te tuerais sur le champ !
22:30	Et l'ânesse dit à Balaam : Ne suis-je pas ton ânesse, sur laquelle tu montes depuis que je suis à toi, jusqu'à aujourd'hui ? Ai-je l'habitude, ai-je l'habitude d'agir ainsi envers toi ? Et il répondit : Non.
22:31	Alors YHWH ouvrit les yeux de Balaam, et il vit l'Ange de YHWH qui se tenait sur le chemin, et qui avait dans sa main son épée nue. Il s'inclina et se prosterna sur son visage.
22:32	Et l'Ange de YHWH lui dit : Pourquoi as-tu frappé ton ânesse déjà trois fois ? Voici je suis sorti pour m'opposer à toi, car à mes yeux ce voyage est précipité.
22:33	Mais l'ânesse m'a vu et s'est détournée à trois reprises devant moi. Autrement, si elle ne s'était détournée de moi, je t'aurais même déjà tué, et je lui aurais laissé la vie.
22:34	Alors Balaam dit à l'Ange de YHWH : J'ai péché, car je ne savais pas que tu t'étais placé au-devant de moi sur le chemin. Mais maintenant, si cela déplaît à tes yeux, je m'en retournerai.
22:35	L'Ange de YHWH dit à Balaam : Va avec ces hommes, mais tu diras seulement la parole que je te dirai. Et Balaam alla avec les chefs envoyés par Balak.
22:36	Et quand Balak apprit que Balaam arrivait, il sortit à sa rencontre jusqu'à la ville de Moab, qui est sur la limite de l'Arnon, à l'extrême frontière.
22:37	Et Balak dit à Balaam : N'ai-je pas envoyé, envoyé vers toi pour t'appeler ? Pourquoi n'es-tu pas venu vers moi ? Ne puis-je pas vraiment te traiter avec honneur ?
22:38	Et Balaam répondit à Balak : Voici, je suis venu vers toi. Maintenant, pourrai-je, pourrai-je dire quelque chose ? Je ne dirai que les paroles qu'Elohîm m'aura mises dans la bouche.
22:39	Et Balaam alla avec Balak, et ils arrivèrent dans la cité de Qiryath-Houtsoth.
22:40	Et Balak sacrifia des bœufs et des brebis, et il en envoya à Balaam et aux chefs qui étaient venus avec lui.
22:41	Quand le matin fut venu, il prit Balaam et le fit monter à Bamoth-Baal, et de là il vit une partie du peuple.

## Chapitre 23

### Balaam ne maudit pas mais bénit Israël des hauts lieux de Baal

23:1	Et Balaam dit à Balak : Bâtis-moi ici 7 autels, et prépare-moi ici 7 veaux et 7 béliers.
23:2	Et Balak fit ce que Balaam avait dit ; et Balak offrit avec Balaam un veau et un bélier sur chaque autel.
23:3	Balaam dit à Balak : Tiens-toi près de ton holocauste et je m'éloignerai. Peut-être que YHWH viendra à ma rencontre et je te rapporterai tout ce qu'il me révélera. Et il s'en alla sur une hauteur dénudée.
23:4	Et Elohîm vint au-devant de Balaam, et Balaam lui dit : J'ai dressé 7 autels, et j'ai sacrifié un veau et un bélier sur chaque autel.
23:5	Et YHWH mit des paroles dans la bouche de Balaam et lui dit : Retourne vers Balak, et tu parleras ainsi.
23:6	Il retourna donc vers lui ; et voici, il se tenait près de son holocauste avec tous les chefs de Moab.
23:7	Alors Balaam prononça sa parabole<!--Vient de l'hébreu « mashal » et signifie : Proverbe, sentence, poème etc.-->, et dit : Balak, roi de Moab, m'a fait descendre d'Aram<!--De l'hébreu « Aram » traduit par « Aram » ou « Syrie » (1 R. 11:25).-->, des montagnes d'orient, en me disant : Viens, maudis-moi Yaacov ! Viens déteste Israël !
23:8	Mais comment le maudirai-je ? El ne l'a pas maudit. Et comment le détesterai-je ? YHWH ne l'a pas détesté.
23:9	Car je le regarderai du sommet des rochers, et je le contemplerai du haut des collines : Voici, ce peuple habitera à part, et il ne sera pas compté parmi les nations<!--De. 33:28.-->.
23:10	Qui comptera la poussière de Yaacov, et dira le nombre du quart d'Israël ? Que je meure de la mort des justes, et que ma fin soit semblable à la leur !
23:11	Alors Balak dit à Balaam : Que m'as-tu fait ? Je t'ai pris pour maudire mes ennemis, et voici, tu les as bénis, tu les as bénis<!--Le verbe bénir vient de l'hébreu « Barak », il est utilisé deux fois de suite dans ce passage. Voir commentaire en Ge. 2:16-17.--> !
23:12	Et il répondit, et dit : Ne prendrais-je pas garde de dire les paroles que YHWH aura mises dans ma bouche ?

### Balaam bénit Israël au sommet de Pisga

23:13	Alors Balak lui dit : Viens, je te prie, avec moi dans un autre lieu, d'où tu pourras le voir, car tu en voyais seulement une extrémité, et tu ne le voyais pas tout entier ; maudis-le moi de là.
23:14	Puis l'ayant conduit au territoire de Tsophim, sur le sommet de Pisga ; il bâtit 7 autels, et offrit un taureau et un bélier sur chaque autel.
23:15	Alors Balaam dit à Balak : Tiens-toi ici près de ton holocauste, et je m'en irai à la rencontre d'Elohîm, comme j'ai déjà fait.
23:16	YHWH donc vint au-devant de Balaam, il mit des paroles dans sa bouche et lui dit : Retourne vers Balak, et tu parleras ainsi.
23:17	Il retourna vers Balak. Et voici, il se tenait près de son holocauste, et les chefs de Moab avec lui. Et Balak lui dit : Qu'est-ce que YHWH a dit ?
23:18	Alors il prononça sa parabole, et dit : Lève-toi, Balak, écoute ! Fils de Tsippor, prête-moi l'oreille !
23:19	El n'est pas un homme pour mentir, ni fils d'un être humain pour se repentir. Ce qu'il a dit, ne le fera-t-il pas ? Ce qu'il a déclaré, ne l'exécutera-t-il pas<!--Ja. 1:17.--> ?
23:20	Voici, j'ai été pris pour bénir : puisqu'il a béni, je n'y changerai rien.
23:21	Il n'a pas aperçu d'iniquité en Yaacov, il ne voit pas de perversité en Israël. YHWH, son Elohîm, est avec lui, et il y a en lui un cri de triomphe royal<!--Jé. 50:20 ; Ro. 4:7-8.-->.
23:22	El les a fait sortir d'Égypte, il est pour eux comme la vigueur du buffle<!--Le terme « reem » a été traduit par « buffle ». Certains l'ont traduit par « licorne ».-->.
23:23	Car il n'y a pas d'enchantement contre Yaacov, ni la divination contre Israël. Au temps marqué, il sera dit à Yaacov et à Israël : Qu'est-ce que El a fait ?
23:24	Voici, ce peuple se lèvera comme une lionne, et se dressera comme un lion qui est dans sa force. Il ne se couchera pas jusqu'à ce qu'il ait dévoré la proie et bu le sang des blessés à mort.
23:25	Balak dit à Balaam : En effet, ne le maudis pas, ne le maudis pas, mais ne le bénis pas, ne le bénis pas aussi.
23:26	Et Balaam répondit à Balak : Ne t'ai-je pas dit que tout ce que YHWH dira, je le ferai ?

### Balaam bénit Israël de Peor

23:27	Balak dit encore à Balaam : Viens maintenant, je te conduirai dans un autre lieu. Peut-être qu'Elohîm trouvera bon que tu me le maudisses de là.
23:28	Balak conduisit donc Balaam sur le sommet de Peor, qui regarde du côté de Yeshiymown.
23:29	Et Balaam lui dit : Bâtis-moi ici 7 autels, et apprête-moi ici 7 veaux et 7 béliers.
23:30	Et Balak fit donc comme Balaam lui avait dit, puis il offrit un taureau et un bélier sur chaque autel.

## Chapitre 24

24:1	Or Balaam, voyant que YHWH voulait bénir Israël, n'alla plus comme les autres fois à la rencontre des enchantements. Mais il tourna son visage du côté du désert.
24:2	Et Balaam leva les yeux, il vit Israël qui se tenait rangé selon ses tribus. Alors l'Esprit d'Elohîm fut sur lui.
24:3	Il prononça sa parabole, et dit : Balaam, fils de Beor dit, l'homme qui a l'œil ouvert dit,
24:4	celui qui entend les paroles de El, qui voit la vision de Shaddaï, qui tombe à terre, et qui a les yeux ouverts dit :
24:5	Que tes tentes sont belles, ô Yaacov ! Et tes tabernacles, ô Israël !
24:6	Ils sont étendus comme des torrents, comme des jardins près d'un fleuve, comme des arbres d'aloès que YHWH a plantés, comme des cèdres auprès des eaux.
24:7	L'eau coule de ses seaux, et sa semence est parmi d'abondantes eaux. Et son roi s'élève au-dessus d'Agag, et son royaume sera haut élevé.
24:8	El l'a fait sortir d'Égypte, il est pour lui comme la vigueur du buffle. Il dévorera les nations qui sont ses ennemies, il brisera leurs os, et les percera de ses flèches.
24:9	Il s'est courbé, il s'est couché comme un lion qui est dans sa force, et comme une lionne : qui le réveillera ? Quiconque te bénit, sera béni, et quiconque te maudit, sera maudit.
24:10	Alors Balak se mit très en colère contre Balaam, il frappa des mains et Balak parla ainsi à Balaam : Je t'ai appelé pour maudire mes ennemis, et voici, tu les as bénis, tu les a bénis<!--Voir le commentaire en Ge. 2:16-17.--> trois fois déjà.
24:11	Et maintenant, fuis dans ton pays ! J'avais dit que je t'honorerais, que je t'honorerais<!--Le mot hébreu est utilisé deux fois dans ce passage. Voir le commentaire en Ge. 2:17.-->, mais YHWH t'empêche d'être honoré.
24:12	Et Balaam répondit à Balak : N'ai-je pas dit à tes messagers que tu m'as envoyés :
24:13	Quand Balak me donnerait sa maison pleine d'argent et d'or, je ne pourrais transgresser l'ordre de YHWH pour faire de moi-même du bien ou du mal. Mais ce que YHWH dira, je le dirai ?
24:14	Maintenant donc je m'en vais vers mon peuple. Viens, je te donnerai un conseil, et je te dirai ce que ce peuple fera à ton peuple, dans les derniers jours.

### Prophétie sur le Roi qui sort de Yaacov, le Mashiah

24:15	Alors il prononça sa parabole, et dit : Balaam, fils de Beor dit, l'homme qui a l'œil ouvert dit,
24:16	celui qui entend les paroles de El, qui connaît la connaissance d'Elyon, qui voit la vision de Shaddaï, qui tombe à terre, et qui a les yeux ouverts :
24:17	Je le vois, mais non pas maintenant, je le regarde, mais non pas de près : une Étoile est sortie de Yaacov<!--L'Étoile en question est Yéhoshoua ha Mashiah (Jésus-Christ), qui se révéla à Yohanan comme l'Étoile brillante du matin (Ap. 22:16).--> et un Sceptre s'est élevé d'Israël. Il transpercera les côtés de Moab, il détruira tous les enfants de Sheth.
24:18	Édom sera sa possession, Séir sera possédé par ses ennemis, et Israël se portera vaillamment.
24:19	Et il y en aura un de Yaacov qui dominera, il fera périr le reste de la ville.
24:20	Il vit aussi Amalek, il prononça sa parabole et dit : Amalek est le premier des nations, mais à la fin il sera détruit.
24:21	Il vit aussi les Kéniens<!--Il y a plusieurs sens à ce mot :\\Caïn = « possession », « artisan, forgeron », fils d'Adam.\\Kéniens = « forgerons » tribu du beau-père de Moshè (Moïse) qui vivait dans la région du sud de Canaan.-->. Il prononça sa parabole et dit : Ta demeure est dans un lieu solide, et tu as mis ton nid dans le rocher.
24:22	Mais Caïn sera brûlé quand l'Assyrien t'emmènera en captivité.
24:23	Il prononça sa parabole et dit : Malheur ! Qui vivra, quand El fera ces choses ?
24:24	Et des navires viendront de Kittim : ils humilieront l'Assyrien et l'Hébreu et lui aussi sera détruit.
24:25	Puis Balaam se leva, et s'en alla pour retourner chez lui. Balak aussi s'en alla par son chemin.

## Chapitre 25

### Prostitution d'Israël à Baal-Peor<!--No. 31:16 ; Ja. 4:4 ; Ap. 2:14.-->

25:1	Israël demeurait à Sittim. Le peuple commença à commettre la fornication avec les filles de Moab.
25:2	Car elles invitèrent le peuple aux sacrifices de leurs elohîm. Et le peuple mangea et se prosterna devant leurs elohîm.
25:3	Et Israël s'attacha à Baal-Peor, c'est pourquoi la colère de YHWH s'enflamma contre Israël<!--Ps. 106:28 ; Os. 9:10.-->.
25:4	Et YHWH dit à Moshè : Prends tous les chefs du peuple, et fais-les pendre devant YHWH en face du soleil, afin que la colère de YHWH se détourne d'Israël<!--De. 4:3 ; Jos. 22:17.-->.
25:5	Moshè donc dit aux juges d'Israël : Que chacun de vous fasse mourir les hommes qui sont à sa charge, et qui se sont attachés à Baal-Peor.
25:6	Et voici, un homme des enfants d'Israël vint, et amena à ses frères une Madianite, devant Moshè et devant toute l'assemblée des fils d'Israël, tandis qu'ils pleuraient à l'entrée de la tente d'assignation.
25:7	Et Phinées, fils d'Èl’azar, fils d'Aaron le prêtre, le vit, se leva du milieu de l'assemblée et prit une lance dans sa main.
25:8	Et il entra dans la tente de l'homme d'Israël et les transperça tous les deux, l'homme d'Israël puis la femme, par le ventre. Et la plaie s'arrêta parmi les enfants d'Israël<!--Ps. 106:28-30.-->.
25:9	Or il y en eut 24 000 qui moururent de cette plaie.
25:10	Et YHWH parla à Moshè, en disant :
25:11	Phinées, fils d'Èl’azar, fils d'Aaron, le prêtre, a détourné ma colère de dessus les enfants d'Israël, parce qu'il a été animé de mon zèle au milieu d'eux ; et je n'ai pas, dans mon ardeur, consumé les fils d'Israël.
25:12	C'est pourquoi, dis-lui : Voici, je lui donne mon alliance de paix.
25:13	Et l'alliance de prêtrise perpétuelle sera tant pour lui que pour sa postérité après lui, parce qu'il a été animé de zèle pour son Elohîm et qu'il a fait la propitiation pour les enfants d'Israël.
25:14	Et le nom de l'homme d'Israël tué, qui fut tué avec la Madianite était Zimri, fils de Salu, prince d'une maison de père des Shimonites.
25:15	Et le nom de la femme madianite qui fut tuée était Cozbi, fille de Tsour, chef du peuple, et d'une maison de père en Madian.
25:16	YHWH parla à Moshè, en disant :
25:17	Mettez en détresse les Madianites, tuez-les.
25:18	En effet, ils ont agi en ennemis à votre égard en vous séduisant par leurs ruses dans l'affaire de Peor et dans l'affaire de Cozbi, fille d'un prince d'entre les Madianites, leur sœur, qui a été tuée le jour de la plaie, causée par l'affaire de Peor.

## Chapitre 26

### Nouveau dénombrement des hommes de guerre

26:1	Et il arriva après cette plaie, que YHWH parla à Moshè et à Èl’azar, fils d'Aaron, le prêtre, en disant :
26:2	Faites le dénombrement de toute l'assemblée des enfants d'Israël, depuis l'âge de 20 ans et au-dessus, selon les maisons de leurs pères, de tous ceux d'Israël qui peuvent aller à la guerre.
26:3	Moshè donc et Èl’azar, le prêtre, leur parlèrent dans les régions arides de Moab, près du Yarden de Yeriycho, en disant :
26:4	Qu'on fasse le dénombrement depuis l'âge de 20 ans et au-dessus, comme YHWH l'avait ordonné à Moshè et aux enfants d'Israël, quand ils furent sortis du pays d'Égypte.
26:5	Reouben, premier-né d'Israël. Fils de Reouben : Hénoc, de qui descend la famille des Hénokites, Pallou, de qui descend la famille des Pallouites ;
26:6	Hetsron, de qui descend la famille des Hetsronites, Carmi, de qui descend la famille des Carmites.
26:7	Ce sont là les familles des Reoubénites : Ceux qui furent dénombrés étaient 43 730.
26:8	Et les fils de Pallou : Éliab.
26:9	Fils d'Éliab : Nemouel, Dathan et Abiram. Ce Dathan et cet Abiram, qui étaient de ceux qu'on appelait pour tenir l'assemblée, se révoltèrent contre Moshè et contre Aaron dans l'assemblée de Koré, lors de leur révolte contre YHWH.
26:10	Et lorsque la terre ouvrit sa bouche et les engloutit, ainsi que Koré, ceux qui s'étaient assemblés avec lui moururent. Et le feu dévora les 250 hommes qui servirent d'avertissement.
26:11	Mais les fils de Koré ne moururent pas.
26:12	Les fils de Shim’ôn selon leurs familles : De Nemouel descend la famille des Némouélites, de Yamin, la famille des Yaminites, de Yakîn, la famille des Yakinites,
26:13	de Zérach, la famille des Zérachites, de Shaoul, la famille des Shaoulites.
26:14	Ce sont là les familles des Shimonites, qui furent 22 200.
26:15	Fils de Gad selon leurs familles. De Tsephon, descend la famille des Tsephonites, de Haggi, la famille des Haggites, de Shouni, la famille des Shounites,
26:16	d'Ozni, la famille des Oznites, d'Éri, la famille des Érites,
26:17	d'Arod, la famille des Arodites, d'Areéli, la famille des Areélites.
26:18	Ce sont là les familles des fils de Gad, d'après leur dénombrement : 40 500.
26:19	Fils de Yéhouda, Er, et Onan. Mais Er et Onan moururent au pays de Canaan<!--Ge. 38:7-10, 46:12.-->.
26:20	Voici les fils de Yéhouda selon leurs familles : De Shéla descend la famille des Shélanites, de Pérets la famille des Péretsites, de Zérach, la famille des Zérachites.
26:21	Les fils de Pérets furent : Hetsron, de qui descend la famille des Hetsronites, Hamoul, de qui descend la famille des Hamoulites.
26:22	Ce sont là les familles de Yéhouda, selon leur dénombrement : 76 500.
26:23	Fils de Yissakar, selon leurs familles : De Thola descend la famille des Tholaïtes, de Pouva, la famille des Pouvites,
26:24	de Yashoub, la famille des Yashoubites, de Shimron, la famille des Shimronites.
26:25	Ce sont là les familles de Yissakar, d'après leur dénombrement : 64 300.
26:26	Fils de Zebouloun, selon leurs familles : De Séred, descend la famille des Sardites, d'Élon la famille des Élonites, de Yahleel la famille des Yahleélites.
26:27	Ce sont là les familles des Zeboulounites, d'après leur dénombrement : 60 500.
26:28	Fils de Yossef, selon leurs familles : Menashè et Éphraïm.
26:29	Fils de Menashè. De Makir descend la famille des Makirites. Makir engendra Galaad. De Galaad descend la famille des Galaadites.
26:30	Voici les fils de Galaad : Iy`ezer, de qui descend la famille des Iy`ezerites, Hélek la famille des Hélekites.
26:31	Asriel, la famille des Asriélites, Sichem, la famille des Sichémites,
26:32	Shemida, la famille des Shemidaïtes, Hépher, la famille des Héphrites.
26:33	Tselophchad, fils de Hépher, n'eut pas de fils, mais des filles. Voici les noms des filles de Tselophchad : Machlah, No`ah, Hoglah, Milkah et Tirtsah.
26:34	Ce sont là les familles de Menashè, d'après leur dénombrement : 52 700.
26:35	Voici les fils d'Éphraïm, selon leurs familles : De Shoutélah descend la famille des Shoutalhites, de Béker, la famille des Bakrites, de Thachan, la famille des Thachanites.
26:36	Voici les fils de Shoutélah : D'Éran est descendue la famille des Éranites.
26:37	Ce sont là les familles des fils d'Éphraïm, d'après leur dénombrement : 32 500. Ce sont là les fils de Yossef, selon leurs familles.
26:38	Fils de Benyamin, selon leurs familles : De Béla descend la famille des Balites, d'Ashbel, la famille des Ashbélites, d'Achiram, la famille des Achiramites,
26:39	de Shoupham, la famille des Shouphamites, de Houpham, la famille des Houphamites.
26:40	Les fils de Béla furent Ard et Naaman. D'Ard descend la famille des Ardites et de Naaman, la famille des Naamanites.
26:41	Ce sont là les fils de Benyamin, d'après leurs familles ; et leur dénombrement : 45 600.
26:42	Voici les fils de Dan, selon leurs familles : De Shouham descend la famille des Shouhamites. Ce sont là les familles de Dan, selon leurs familles.
26:43	Toutes les familles des Shouhamites, selon leur dénombrement : 64 400.
26:44	Fils d'Asher, selon leurs familles : De Yimnah descend la famille des Yimnites, de Yishviy, la famille des Yishvites, de Beriy`ah la famille des Beriites.
26:45	Des fils de Beriy`ah descendent : De Héber, la famille des Hébrites, de Malkiel, la famille des Malkiélites.
26:46	Et le nom de la fille d'Asher était Sérach.
26:47	Ce sont là les familles des fils d'Asher, d'après leur dénombrement : 53 400.
26:48	Fils de Nephthali, selon leurs familles : De Yahtseel descend la famille des Yahtseélites, de Gouni, la famille des Gounites,
26:49	de Yetser la famille des Yitsrites, de Shillem, la famille des Shillémites.
26:50	Ce sont là les familles de Nephthali, selon leurs familles, et leur dénombrement : 45 400.
26:51	Voici les dénombrés des fils d'Israël, qui furent 601 730.
26:52	YHWH parla à Moshè, en disant :
26:53	Le pays sera partagé entre ceux-ci en héritage, selon le nombre des noms.
26:54	À ceux qui sont en plus grand nombre, tu donneras plus d'héritage, et à ceux qui sont en plus petit nombre tu donneras moins d'héritage. On donnera à chacun son héritage selon le nombre de ses dénombrés.
26:55	Toutefois, que le pays soit partagé par le sort et qu'ils prennent leur héritage selon les noms des tribus de leurs pères<!--Jos. 11:23, 14:2, 18:6-8.-->.
26:56	C'est par le sort que l'héritage sera partagé entre ceux qui sont en grand nombre et ceux qui sont en petit nombre.
26:57	Voici les Lévites dont on fit le dénombrement, selon leurs familles : de Guershon, la famille des Guershonites, de Kehath, la famille des Kehathites, de Merari, la famille des Merarites.
26:58	Voici les familles de Lévi : la famille des Libnites, la famille des Hébronites, la famille des Machlites, la famille des Moushites, la famille des Korites. Kehath engendra Amram.
26:59	Et le nom de la femme d'Amram était Yokebed, fille de Lévi, qui l'enfanta pour Lévi en Égypte. Elle enfanta pour Amram : Aaron, Moshè, et Myriam, leur sœur.
26:60	Aaron engendra Nadab et Abihou, Èl’azar et Ithamar.
26:61	Nadab et Abihou moururent lorsqu'ils apportèrent du feu étranger devant YHWH<!--Lé. 10:1-2 ; 1 Ch. 24:2.-->.
26:62	Et tous les dénombrés des Lévites furent 23 000, tous mâles, depuis l'âge d'un mois et au-dessus. Ils ne furent pas dénombrés avec les autres enfants d'Israël, car on ne leur donna pas d'héritage entre les enfants d'Israël.
26:63	Ce sont là ceux qui furent dénombrés par Moshè et Èl’azar, le prêtre, qui firent le dénombrement des fils d'Israël dans les régions arides de Moab, près du Yarden de Yeriycho,
26:64	entre lesquels il ne s'en trouva aucun de ceux qui avaient été dénombrés par Moshè et Aaron le prêtre, quand ils firent le dénombrement des enfants d'Israël dans le désert de Sinaï.
26:65	Car à leur sujet YHWH avait dit : Ils mourront, ils mourront dans le désert, et il n'en restera pas un, excepté Caleb, fils de Yephounné, et Yéhoshoua, fils de Noun<!--1 Co. 10:5.-->.

## Chapitre 27

### Torah sur les héritages<!--No. 36.-->

27:1	Or les filles de Tselophchad, fils de Hépher, fils de Galaad, fils de Makir, fils de Menashè, d'entre les familles de Menashè, fils de Yossef, s'approchèrent. Voici les noms de ses filles : Machlah, No`ah, Hoglah, Milkah, et Tirtsah.
27:2	Elles se présentèrent devant Moshè, devant Èl’azar, le prêtre, et devant les princes et toute l'assemblée, à l'entrée de la tente d'assignation. Elles dirent :
27:3	Notre père est mort dans le désert. Il n'était pas au milieu de l'assemblée de ceux qui se réunirent contre YHWH, de l'assemblée de Koré, mais il est mort à cause de son péché et il n'avait pas de fils.
27:4	Pourquoi le nom de notre père serait-il retranché de sa famille, parce qu'il n'a pas eu de fils ? Donne-nous une possession parmi les frères de notre père.
27:5	Moshè rapporta leur cause devant YHWH.
27:6	Et YHWH parla à Moshè, en disant :
27:7	Les filles de Tselophchad ont parlé droitement. Tu leur donneras, tu leur donneras en héritage une possession parmi les frères de leur père, c'est à elles que tu feras passer l'héritage de leur père.
27:8	Tu parleras aussi aux enfants d'Israël, et tu leur diras : Lorsqu'un homme mourra sans avoir de fils, vous ferez passer son héritage à sa fille.
27:9	S'il n'a pas de fille, vous donnerez son héritage à ses frères.
27:10	S'il n'a pas de frères, vous donnerez son héritage aux frères de son père.
27:11	Et si son père n'a pas de frère, vous donnerez son héritage à son parent le plus proche de sa famille, et il le possédera. Et ce sera pour les enfants d'Israël un statut de droit, comme YHWH l'a ordonné à Moshè.

### Moshè voit de loin le pays promis aux fils d'Israël

27:12	YHWH dit aussi à Moshè : Monte sur cette montagne d'Abarim, et regarde le pays que je donne aux enfants d'Israël<!--De. 32:48-49.-->.
27:13	Tu le regarderas, puis tu seras toi aussi recueilli auprès de ton peuple comme Aaron ton frère y a été recueilli,
27:14	parce que vous avez été rebelles à mon ordre dans le désert de Tsin lors de la contestation de l'assemblée. Vous ne m'avez pas sanctifié au sujet des eaux devant eux. Ce sont les eaux de Meriba, à Qadesh, dans le désert de Tsin.

### YHWH désigne Yéhoshoua comme successeur de Moshè (Moïse)

27:15	Moshè parla à YHWH, en disant :
27:16	Que YHWH, l'Elohîm des esprits de toute chair, établisse sur l'assemblée un homme<!--Hé. 12:9.-->
27:17	qui sorte devant eux et qui entre devant eux, et qui les fasse sortir et qui les fasse entrer, afin que l'assemblée de YHWH ne soit pas comme des brebis qui n'ont pas de berger<!--1 R. 22:17 ; Mt. 9:36 ; Mc. 6:34 ; Jn. 10.-->.
27:18	Alors YHWH dit à Moshè : Prends Yéhoshoua, fils de Noun, un homme en qui est l'Esprit, et tu poseras ta main sur lui<!--De. 34:9.-->.
27:19	Tu le présenteras devant Èl’azar, le prêtre, et devant toute l'assemblée, et tu lui donneras des instructions sous leurs yeux.
27:20	Et tu lui feras part de ton autorité, afin que toute l'assemblée des enfants d'Israël l'écoute.
27:21	Il se tiendra debout devant Èl’azar, le prêtre, qui consultera pour lui les jugements de l'ourim<!--Lé. 8:8.--> devant YHWH. C'est à sa parole qu'ils sortiront, à sa parole qu'ils entreront, lui et tous les enfants d'Israël avec lui, et toute l'assemblée.
27:22	Moshè donc fit comme YHWH lui avait ordonné. Il prit Yéhoshoua et le présenta devant Èl’azar, le prêtre, et devant toute l'assemblée.
27:23	Puis il posa ses mains sur lui et lui donna des instructions, comme YHWH l'avait dit par Moshè.

## Chapitre 28

### L'holocauste perpétuel

28:1	YHWH parla à Moshè, en disant :
28:2	Donne cet ordre aux enfants d'Israël et dis-leur : Vous aurez soin de m'offrir en leur temps, mon offrande, ma nourriture, pour mes offrandes consumées par le feu dont le parfum est apaisant pour moi<!--Lé. 3:11, 21:6.-->.
28:3	Tu leur diras : Voici le sacrifice consumé par le feu que vous offrirez à YHWH : 2 agneaux d'un an sans défaut, chaque jour, en holocauste perpétuel<!--Ex. 29:38.-->.
28:4	Tu sacrifieras l'un des agneaux le matin, et l'autre agneau entre les deux soirs,
28:5	et la dixième partie d'épha de fine farine pour l'offrande de grain, pétrie avec le quart d'un hin d'huile vierge<!--Lé. 2:1 ; Ex. 29:40, 16:36.-->.
28:6	C'est l'holocauste perpétuel, qui a été offert à la montagne de Sinaï, c'est une offrande consumée par le feu dont le parfum est apaisant pour YHWH.
28:7	Et sa libation sera d'un quart de hin pour chaque agneau. Et tu verseras dans le lieu saint la libation de boisson forte à YHWH.
28:8	Et tu sacrifieras l'autre agneau entre les deux soirs, tu feras la même offrande de grain et la même libation que le matin : c'est une offrande consumée par le feu dont le parfum est apaisant pour YHWH.

### Les sacrifices du shabbat

28:9	Mais le jour du shabbat vous offrirez 2 agneaux d'un an sans défaut, et deux dixièmes de fine farine pétrie à l'huile pour l'offrande de grain, avec sa libation.
28:10	C'est l'holocauste du shabbat, pour chaque shabbat, outre l'holocauste perpétuel avec sa libation.

### Les sacrifices mensuels

28:11	Et au commencement de vos mois, vous offrirez en holocauste à YHWH 2 jeunes taureaux, un bélier, et 7 agneaux d'un an sans défaut,
28:12	et trois dixièmes de fine farine pétrie à l'huile en offrande de grain pour chaque taureau, et deux dixièmes de fine farine pétrie à l'huile en offrande de grain pour le bélier,
28:13	et un dixième de fine farine pétrie à l'huile en offrande de grain pour chaque agneau. C'est un holocauste, une offrande consumée par le feu dont le parfum est apaisant pour YHWH.
28:14	Et leurs libations seront d'un demi-hin de vin pour chaque veau, d'un tiers de hin pour un bélier, et d'un quart de hin pour chaque agneau. C'est l'holocauste du commencement de chaque mois, selon tous les mois de l'année.
28:15	On sacrifiera aussi à YHWH un jeune bouc en sacrifice pour le péché, outre l'holocauste perpétuel, et sa libation.

### Les sacrifices de la Pâque

28:16	Le quatorzième jour du premier mois, ce sera la Pâque pour YHWH.
28:17	Et le quinzième jour du même mois sera un jour de fête. On mangera pendant 7 jours des pains sans levain<!--Ex. 12 ; Lé. 23:5-6.-->.
28:18	Le premier jour, il y aura une sainte convocation : vous ne ferez aucun travail, aucun service.
28:19	Et vous offrirez un sacrifice consumé par le feu en holocauste à YHWH : 2 jeunes taureaux, un bélier, et 7 agneaux d'un an, sans défaut.
28:20	Leur offrande de grain sera de fine farine pétrie à l'huile, vous en offrirez trois dixièmes pour chaque jeune taureau, et deux dixièmes pour un bélier ;
28:21	tu en offriras aussi un dixième pour chacun des 7 agneaux,
28:22	et un bouc en sacrifice pour le péché, afin de faire la propitiation pour vous.
28:23	Vous offrirez ces choses-là, outre l'holocauste du matin qui est l'holocauste perpétuel.
28:24	Vous offrirez ces choses-là chaque jour, pendant 7 jours, comme l'aliment d'une offrande consumée par le feu dont le parfum est apaisant pour YHWH. On offrira cela outre l'holocauste perpétuel et sa libation.
28:25	Et au septième jour, vous aurez une sainte convocation : vous ne ferez aucun travail, aucun service.

### Les sacrifices de la fête des moissons

28:26	Et le jour des prémices, quand vous offrirez à YHWH une nouvelle offrande de grain à votre fête des semaines, vous aurez une sainte convocation : vous ne ferez aucun travail, aucun service.
28:27	Et vous offrirez en holocauste dont le parfum est apaisant pour YHWH, 2 jeunes taureaux, un bélier et 7 agneaux d'un an.
28:28	Et leur offrande de grain sera de fine farine pétrie à l'huile, de trois dixièmes pour chaque jeune taureau, et de deux dixièmes pour le bélier,
28:29	et de la dixième partie d'un dixième pour les agneaux, pour chacun des 7 agneaux,
28:30	et un jeune bouc, afin de faire la propitiation pour vous.
28:31	Vous les offrirez, outre l'holocauste perpétuel et son offrande de grain, lesquels seront sans défaut, avec leurs libations.

## Chapitre 29

### Les sacrifices de la fête des trompettes

29:1	Et le premier jour du septième mois, vous aurez une sainte convocation : vous ne ferez aucun travail, aucun service. Ce jour sera publié parmi vous au son des trompettes<!--Lé. 23:24-25.-->.
29:2	Et vous offrirez en holocauste dont le parfum est apaisant pour YHWH, un jeune taureau, un bélier et 7 agneaux d'un an, sans défaut.
29:3	Et leur offrande de grain sera de fine farine pétrie à l'huile, de trois dixièmes pour le jeune taureau, de deux dixièmes pour le bélier,
29:4	et un dixième pour chacun des 7 agneaux,
29:5	et un jeune bouc, en sacrifice pour le péché, afin de faire la propitiation pour vous,
29:6	outre l'holocauste du commencement du mois et son offrande de grain, et l'holocauste perpétuel et son offrande de grain, et leurs libations selon leur ordonnance. Ce sont des offrandes consumées par le feu dont le parfum est apaisant pour YHWH.

### Les sacrifices du jour des expiations

29:7	Et le dixième jour de ce septième mois, vous aurez une sainte convocation, et vous affligerez vos âmes : vous ne ferez aucun travail<!--Lé. 16:29-31, 23:27.-->.
29:8	Et vous offrirez en holocauste dont le parfum est apaisant pour YHWH, un jeune taureau, un bélier et 7 agneaux d'un an, qui seront sans défaut.
29:9	Et leur offrande de grain sera de fine farine pétrie à l'huile, de trois dixièmes pour le taureau, et de deux dixièmes pour le bélier,
29:10	et d'un dixième pour chacun des 7 agneaux,
29:11	vous offrirez aussi un bouc en sacrifice pour le péché, outre le sacrifice pour le péché qu'on offre le jour des expiations, l'holocauste perpétuel et son offrande de grain avec leurs libations.

### Les sacrifices de la fête des tentes

29:12	Et le quinzième jour du septième mois, vous aurez une sainte convocation : vous ne ferez aucun travail, aucun service. Vous célébrerez une fête à YHWH, pendant 7 jours<!--Lé. 23:34-43.-->.
29:13	Et vous offrirez en holocauste une offrande consumée par le feu dont le parfum est apaisant pour YHWH, 13 jeunes taureaux, 2 béliers et 14 agneaux d'un an, sans défaut.
29:14	Et leur offrande de grain sera de fine farine pétrie à l'huile, de trois dixièmes pour chacun des 13 jeunes taureaux, de deux dixièmes pour chacun des 2 béliers,
29:15	et d'un dixième pour chacun des 14 agneaux.
29:16	Et un jeune bouc en sacrifice pour le péché, outre l'holocauste perpétuel, son offrande de grain et sa libation.
29:17	Et le second jour, vous offrirez 12 jeunes taureaux, 2 béliers, et 14 agneaux d'un an, sans défaut,
29:18	avec l'offrande de grain et les libations pour les jeunes taureaux, pour les béliers, et pour les agneaux, selon leur nombre, d'après les ordonnances.
29:19	Vous offrirez un jeune bouc en sacrifice pour le péché, outre l'holocauste perpétuel, et son offrande de grain, avec leurs libations.
29:20	Et le troisième jour, vous offrirez 11 taureaux, 2 béliers, et 14 agneaux d'un an, sans défaut,
29:21	et l'offrande de grain et les libations pour les jeunes taureaux, les béliers et les agneaux, selon leur nombre, selon leur ordonnance.
29:22	Et un bouc en sacrifice pour le péché, outre l'holocauste continuel, son offrande de grain et sa libation.
29:23	Et le quatrième jour, vous offrirez 10 jeunes taureaux, 2 béliers, et 14 agneaux d'un an, sans défaut,
29:24	l'offrande de grain et les libations pour les taureaux, les béliers, et les agneaux, selon leur nombre et leur ordonnance,
29:25	et un jeune bouc en sacrifice pour le péché, outre l'holocauste perpétuel, son offrande de grain et sa libation.
29:26	Et le cinquième jour, vous offrirez 9 jeunes taureaux, 2 béliers, et 14 agneaux d'un an, sans défaut,
29:27	avec l'offrande de grain et les libations pour les taureaux, les béliers, et les agneaux, selon leur nombre et leur ordonnance,
29:28	et un bouc en sacrifice pour le péché, outre l'holocauste continuel, son offrande de grain et sa libation.
29:29	Et le sixième jour, vous offrirez 8 jeunes taureaux, 2 béliers et 14 agneaux d'un an, sans défaut,
29:30	et l'offrande de grain, les libations pour les taureaux, les béliers, et les agneaux selon leur nombre, leur ordonnance.
29:31	Et un bouc en sacrifice pour le péché, outre l'holocauste continuel, son offrande de grain et sa libation.
29:32	Et le septième jour, vous offrirez 7 jeunes taureaux, 2 béliers, et 14 agneaux d'un an, sans défaut,
29:33	avec l'offrande de grain et les libations pour les jeunes taureaux, les béliers, et les agneaux, selon leur nombre et leur ordonnance,
29:34	et un bouc en sacrifice pour le péché, outre l'holocauste continuel, son offrande de grain et sa libation.
29:35	Et le huitième jour, vous aurez une assemblée solennelle : vous ne ferez aucun travail, aucun service.
29:36	Et vous offrirez en holocauste une offrande consumée par le feu dont le parfum est apaisant pour YHWH : Un jeune taureau, un bélier et 7 agneaux d'un an, sans défaut,
29:37	avec l'offrande de grain et les libations pour le jeune taureau, le bélier, et les agneaux, selon leur nombre et leur ordonnance,
29:38	et un bouc en sacrifice pour le péché, outre l'holocauste perpétuel, son offrande de grain et sa libation.
29:39	Vous offrirez ces choses à YHWH dans vos fêtes, outre vos vœux, et vos offrandes volontaires, selon vos holocaustes, vos offrandes de grain, vos libations et vos sacrifices d'offrande de paix.

## Chapitre 30

### Les vœux

30:1	Et Moshè parla aux enfants d'Israël selon tout ce que YHWH lui avait ordonné.
30:2	Moshè parla aussi aux chefs des tribus des enfants d'Israël, en disant : Voici ce que YHWH ordonne.
30:3	Quand un homme fera un vœu à YHWH, ou aura juré par serment, pour lier son âme par un vœu, il ne violera pas sa parole. Il fera selon tout ce qui est sorti de sa bouche<!--De. 23:21 ; Ec. 5:3-6 ; Ps. 76:12.-->.
30:4	Mais quand une femme, pendant sa jeunesse et dans la maison de son père, fera un vœu à YHWH et se liera par une obligation,
30:5	et que son père aura entendu son vœu et le serment par lequel elle a lié son âme, si son père ne lui dit rien, tous ses vœux seront valables, et tout serment par lequel elle aura lié son âme sera valable.
30:6	Mais si son père la désapprouve le jour où il l'a entendue, aucun de ses vœux ou de ses serments par lesquels elle a lié son âme ne sera valable, et YHWH lui pardonnera, parce que son père l'a désapprouvée.
30:7	Et si elle a un mari, et qu'elle s'est engagée par quelque vœu ou par une parole échappée de ses lèvres par laquelle elle aura lié son âme,
30:8	et que son mari l'aura entendue, et que le jour même où il l'a entendue, il ne lui a rien dit, ses vœux seront valables et ses serments par lesquels elle aura lié son âme seront valables.
30:9	Mais si son mari la désapprouve le jour où il l'a entendue, alors il annulera le vœu par lequel elle s'est engagée et la parole échappée de ses lèvres, par laquelle elle avait lié son âme, et YHWH lui pardonnera.
30:10	Mais le vœu de la veuve ou de la répudiée, tout ce qu’elle a lié sur son âme sera valable pour elle.
30:11	Mais si c'est dans la maison de son mari qu'elle a fait un vœu, ou qu'elle a lié son âme avec une obligation par un serment,
30:12	et que son mari l'ait entendue, et ne lui en ait rien dit, et ne l'ait pas désapprouvée, alors tous ses vœux seront valables, et tout serment par lequel elle a lié son âme sera valable.
30:13	Mais si son mari les a annulés, s'il les a annulés le jour où il les a entendus, alors rien de ce qui est sorti de ses lèvres, soit ses vœux, soit le serment par lequel elle a lié son âme ne seront valables ; parce que son mari les a annulés, et YHWH lui pardonnera.
30:14	Son mari ratifiera ou son mari annulera tout vœu et toute obligation faite par serment, pour affliger l'âme.
30:15	Mais si son mari ne lui dit rien, s'il ne lui dit rien d'un jour à l'autre, il aura ratifié tous ses vœux ou toutes ses obligations dont elle était tenue. Il les aura ratifiés, parce qu'il ne lui en a rien dit le jour où il les a entendus.
30:16	Mais s'il les a annulés, s'il les a annulés quelque temps après les avoir entendus, alors il portera l'iniquité de sa femme.
30:17	Telles sont les ordonnances que YHWH ordonna à Moshè, entre un mari et sa femme, entre un père et sa fille, étant encore dans la maison de son père, dans sa jeunesse.

## Chapitre 31

### Jugements sur Madian<!--No. 25:6-18.-->

31:1	YHWH parla à Moshè, en disant :
31:2	Venge, exécute la vengeance des enfants d'Israël sur les Madianites, puis tu seras recueilli auprès de ton peuple.
31:3	Moshè donc parla au peuple, en disant : Que quelques-uns d'entre vous s'équipent pour aller à la guerre, et qu'ils aillent contre Madian, pour exécuter la vengeance de YHWH sur Madian.
31:4	Vous les enverrez à la guerre, 1 000 par tribu, 1 000 par tribu de toutes les tribus d'Israël.
31:5	On donna d'entre les milliers d'Israël 1 000 hommes de chaque tribu, qui furent 12 000 hommes équipés pour la guerre.
31:6	Moshè les envoya à la guerre, 1 000 de chaque tribu, et avec eux Phinées, fils d'Èl’azar, le prêtre, qui portait les instruments sacrés et les trompettes retentissantes.
31:7	Ils s'avancèrent donc contre Madian, comme YHWH l'avait ordonné à Moshè, et ils en tuèrent tous les mâles.
31:8	Ils tuèrent aussi les rois de Madian, outre les autres qui y furent blessés mortellement, Évi, Rékem, Tsour, Hour, et Réba, cinq rois de Madian. Ils tuèrent aussi par l'épée Balaam, fils de Beor<!--Jos. 13:21-22.-->.
31:9	Et les fils d'Israël emmenèrent prisonniers les femmes de Madian, avec leurs petits enfants, et pillèrent tout leur gros et menu bétail, et tous leurs biens.
31:10	Ils brûlèrent par le feu toutes leurs villes, leurs demeures, et tous leurs châteaux.
31:11	Ils prirent tout le butin et toutes les dépouilles, tant des humains que du bétail<!--De. 20:14.--> ;
31:12	puis ils amenèrent les captifs, les dépouilles et le butin, à Moshè, à Èl’azar le prêtre et à l'assemblée des enfants d'Israël, au camp, dans les régions arides de Moab, qui sont près du Yarden, vis-à-vis de Yeriycho.
31:13	Moshè, Èl’azar, le prêtre, et tous les princes de l'assemblée sortirent au-devant d'eux, hors du camp.
31:14	Et Moshè se mit en grande colère contre les officiers de l'armée, les chefs des milliers, et les chefs des centaines, qui revenaient de cet exploit de guerre.
31:15	Et Moshè leur dit : Avez-vous laissé la vie à toutes les femmes ?
31:16	Voici ce sont elles qui, sur la parole de Balaam, ont donné l'occasion aux fils d'Israël de pécher contre YHWH dans l'affaire de Peor ; ce qui attira la plaie sur l'assemblée de YHWH<!--2 Pi. 2:15 ; Ap. 2:14.-->.
31:17	Et maintenant, tuez tous les mâles d'entre les petits enfants, et tuez toute femme qui a connu un homme en couchant avec lui<!--Jg. 21:11.--> ;
31:18	mais vous garderez en vie toutes les jeunes filles qui n'ont pas connu la couche d'un homme.
31:19	Au reste, restez 7 jours hors du camp. Quiconque aura tué quelqu'un, et quiconque aura touché quelqu'un qui aura été blessé mortellement, se purifiera le troisième et le septième jour, tant vous que vos prisonniers.
31:20	Vous purifierez aussi tous vos vêtements, et tout ce qui sera fait de peau, et tout ouvrage de poil de chèvre, et toute vaisselle de bois.
31:21	Èl’azar, le prêtre, dit aux hommes de guerre qui étaient allés au combat : Voici le statut de la torah que YHWH a prescrite à Moshè.
31:22	Uniquement l'or, l'argent, le cuivre, le fer, l'étain, le plomb,
31:23	tout ce qui peut passer par le feu, vous le ferez passer par le feu pour le rendre pur. Mais on purifiera avec l'eau contre l'impureté toutes les choses qui ne peuvent aller au feu ; vous les ferez passer dans l'eau.
31:24	Vous laverez aussi vos vêtements le septième jour, ensuite vous serez purs. Ensuite vous entrerez dans le camp.

### Partage du butin

31:25	Et YHWH parla à Moshè, en disant :
31:26	Fais le compte du butin et de tout ce qu'on a emmené, tant des humains que des bêtes, toi et Èl’azar, le prêtre, et les chefs des pères de l'assemblée.
31:27	Et partage par moitié le butin entre les combattants qui sont allés à la guerre et toute l'assemblée<!--1 S. 30:24.-->.
31:28	Tu prélèveras aussi pour YHWH une taxe sur les hommes de guerre qui sont allés à la bataille, un sur 500, tant des humains que des bœufs, des ânes et des brebis.
31:29	On le prendra sur leur moitié, et tu le donneras à Èl’azar, le prêtre, en offrande présentée par élévation à YHWH.
31:30	Et sur la moitié qui appartient aux enfants d'Israël, tu prendras un sur 50, tant des humains que des bœufs, des ânes, des brebis et de tous les autres animaux et tu le donneras aux Lévites qui ont la charge de garder le tabernacle de YHWH.
31:31	Moshè et Èl’azar, le prêtre, firent comme YHWH l'avait ordonné à Moshè.
31:32	Or le butin qui était resté du pillage du peuple qui était allé à la guerre, était de 675 000 brebis ;
31:33	de 72 000 bœufs,
31:34	de 61 000 ânes,
31:35	quant aux âmes humaines parmi les femmes qui n'avaient pas connu la couche d'un mâle, toutes les âmes étaient en tout 32 000.
31:36	Et la moitié du butin, la part de ceux qui étaient allés à la guerre, montait à 337 500 brebis,
31:37	dont la taxe pour YHWH fut de 675 brebis,
31:38	36 000 bœufs, dont 72 pour la taxe pour YHWH,
31:39	30 500 ânes, dont 61 pour YHWH,
31:40	et les âmes humaines : 16 000, dont la taxe pour YHWH était de 32 âmes.
31:41	Et Moshè donna à Èl’azar, le prêtre, la taxe de l'offrande présentée par élévation à YHWH, comme YHWH le lui avait ordonné.
31:42	Et de l'autre moitié qui appartenait aux enfants d'Israël, que Moshè avait tirée des hommes qui étaient allés à la guerre,
31:43	cette moitié attribuée à l'assemblée était de 337 500 brebis,
31:44	36 000 bœufs,
31:45	30 500 ânes,
31:46	et 16 000 âmes humaines ;
31:47	de cette moitié qui appartenait aux enfants d'Israël, Moshè prit un sur 50, tant des humains que des bêtes, et les donna aux Lévites qui avaient la charge de garder le tabernacle de YHWH, comme YHWH le lui avait ordonné.
31:48	Les commandants des milliers de l'armée, tant les chefs des milliers que les chefs des centaines, s'approchèrent de Moshè,
31:49	et lui dirent : Tes serviteurs ont fait le compte des hommes de guerre qui étaient sous nos ordres, il ne manque pas un homme d'entre nous.
31:50	C'est pourquoi, nous offrons l'offrande de YHWH, chacun les objets que nous avons trouvés : Des joyaux d'or, des chaînes de cheville, des bracelets, des anneaux, des pendants d'oreilles et des colliers, afin de faire la propitiation pour nos personnes devant YHWH.
31:51	Moshè et Èl’azar le prêtre reçurent d'eux cet or et tous ces objets travaillés.
31:52	Et tout l'or de l'offrande présentée par élévation à YHWH, de la part des chefs de milliers et des chefs de centaines, montait à 16 750 sicles.
31:53	Or les hommes de guerre gardèrent chacun pour soi ce qu'ils avaient pillé.
31:54	Moshè donc et Èl’azar, le prêtre, prirent l'or des chefs des milliers et des chefs de centaines, et l'apportèrent à la tente d'assignation, comme souvenir pour les enfants d'Israël, devant YHWH.

## Chapitre 32

### Reouben et Gad en Galaad

32:1	Les fils de Reouben et les fils de Gad avaient un bétail considérable et en très grande quantité, et ils virent que le pays de Ya`azeyr et le pays de Galaad étaient un lieu propre pour du bétail.
32:2	Ainsi les fils de Gad et les fils de Reouben vinrent, et parlèrent à Moshè et à Èl’azar, le prêtre, et aux princes de l'assemblée, en disant :
32:3	Atharoth, Dibon, Ya`azeyr, Nimrah, Hesbon, et Élealé, Sebam, Nebo, et Beon,
32:4	ce pays-là que YHWH a frappé devant l'assemblée d'Israël, est un pays propre pour le bétail, et tes serviteurs ont des troupeaux.
32:5	Ils dirent donc : Si nous avons trouvé grâce à tes yeux, que ce pays soit donné en possession à tes serviteurs. Ne nous fais pas passer le Yarden.
32:6	Mais Moshè répondit aux fils de Gad, et aux fils de Reouben : Vos frères iront-ils à la guerre, et vous, demeurerez-vous ici ?
32:7	Pourquoi voulez-vous décourager les enfants d'Israël de passer dans le pays que YHWH leur a donné ?
32:8	C'est ainsi que firent vos pères quand je les envoyai de Qadesh-Barnéa pour examiner le pays.
32:9	Car ils montèrent jusqu'à la vallée d'Eshcol, virent le pays, puis découragèrent les enfants d'Israël, afin qu'ils n'entrent pas dans le pays que YHWH leur avait donné.
32:10	C'est pourquoi la colère de YHWH s'enflamma ce jour-là, et il jura en disant :
32:11	Les hommes qui sont montés hors d'Égypte, depuis l'âge de 20 ans et au-dessus, ne verront pas le pays que j'ai juré de donner à Abraham, Yitzhak, et à Yaacov, car ils n'ont pas persévéré à me suivre<!--De. 1:35.-->,
32:12	excepté Caleb, fils de Yephounné, le Kénizien, et Yéhoshoua, fils de Noun, car ils ont persévéré à suivre YHWH.
32:13	Ainsi la colère de YHWH s'enflamma contre Israël et il les fit errer dans le désert pendant 40 ans, jusqu'à ce que toute la génération qui avait fait le mal aux yeux de YHWH, ait été consumée.
32:14	Et voici, vous vous êtes levés à la place de vos pères, comme une race d'hommes pécheurs, pour augmenter encore l'ardeur de la colère de YHWH contre Israël.
32:15	Si vous vous détournez de lui, il continuera encore à vous laisser dans le désert, et vous ferez détruire tout ce peuple.
32:16	Mais ils s'approchèrent de lui et lui dirent : Nous bâtirons ici des cloisons pour nos troupeaux, et des villes pour nos petits enfants,
32:17	et nous nous équiperons pour marcher promptement devant les enfants d'Israël jusqu'à ce que nous les ayons introduits dans leur territoire. Mais nos petits enfants demeureront dans les villes fortes, à cause des habitants du pays.
32:18	Nous ne retournerons pas dans nos maisons avant que chacun des enfants d'Israël n'ait pris possession de son héritage,
32:19	et nous ne posséderons rien en héritage avec eux de l'autre côté du Yarden et au-delà, parce que nous aurons notre héritage de ce côté-ci du Yarden, à l'orient.
32:20	Et Moshè leur dit : Si vous faites cela, si vous vous équipez devant YHWH pour aller à la guerre,
32:21	si chacun de vous étant équipé passe le Yarden devant YHWH, jusqu'à ce qu'il ait dépossédé ses ennemis devant lui,
32:22	et que vous ne reveniez qu'après que le pays aura été assujetti devant YHWH, alors vous serez dégagés de toute responsabilité envers YHWH et envers Israël, et ce pays-ci sera votre propriété devant YHWH.
32:23	Mais si vous ne faites pas cela, vous péchez contre YHWH. Et sachez que votre péché vous atteindra.
32:24	Bâtissez donc des villes pour vos petits enfants, et des cloisons pour vos troupeaux, et faites ce que vous avez dit.
32:25	Alors les fils de Gad et les fils de Reouben parlèrent à Moshè, en disant : Tes serviteurs feront ce que mon seigneur a ordonné.
32:26	Nos petits enfants, nos femmes, nos troupeaux, et tout notre bétail demeureront ici dans les villes de Galaad ;
32:27	et tes serviteurs passeront chacun armé pour aller à la guerre devant YHWH, prêts à combattre, comme mon seigneur a parlé.
32:28	Alors Moshè donna des ordres à leur sujet à Èl’azar, le prêtre, à Yéhoshoua, fils de Noun, et aux chefs des pères des tribus des fils d'Israël.
32:29	Il leur dit : Si les fils de Gad et les fils de Reouben passent avec vous le Yarden tous armés, prêts à combattre devant YHWH, et que le pays vous soit assujetti, vous leur donnerez le pays de Galaad en possession.
32:30	Mais s'ils ne marchent pas en armes avec vous, qu'ils s'établissent au milieu de vous dans le pays de Canaan.
32:31	Les fils de Gad et les fils de Reouben répondirent, en disant : Nous ferons ce que YHWH a dit à tes serviteurs.
32:32	Nous passerons en armes devant YHWH au pays de Canaan. Et nous aurons notre propriété, notre part d'héritage, de ce côté-ci du Yarden.
32:33	Ainsi Moshè donna aux fils de Gad et aux fils de Reouben, et à la demi-tribu de Menashè, fils de Yossef, le royaume de Sihon, roi des Amoréens ; et le royaume de Og, roi de Bashân, le pays avec ses villes, selon les bornes des villes du pays tout autour.
32:34	Alors les fils de Gad rebâtirent Dibon, Atharoth, Aroër,
32:35	Athroth-Shophan, Ya`azeyr, Yogbehah,
32:36	Beth-Nimrah et Beth-Haran, villes fortifiées. Ils firent aussi des cloisons pour les troupeaux.
32:37	Et les fils de Reouben rebâtirent Hesbon, Élealé, Qiryathayim,
32:38	Nébo, Baal-Meon, et Sibma, dont ils changèrent les noms, et ils donnèrent des noms aux villes qu'ils rebâtirent.
32:39	Or les fils de Makir, fils de Menashè, allèrent en Galaad, le prirent et dépossédèrent les Amoréens qui y étaient.
32:40	Moshè donc donna Galaad à Makir, fils de Menashè, qui y habita<!--De. 3:15.-->.
32:41	Yaïr, fils de Menashè, se mit en marche, prit leurs villages, et les appela villages de Yaïr<!--De. 3:14 ; 1 Ch. 2:22.-->.
32:42	Et Nobach se mit en marche, prit Kenath avec les villes de son ressort, et l'appela Nobach d'après son nom.

## Chapitre 33

### Les stations de l'Égypte jusqu'au Yarden (Jourdain)

33:1	Voici les étapes des enfants d'Israël, qui sortirent du pays d'Égypte, selon leurs armées, sous la main de Moshè et d'Aaron.
33:2	Moshè écrivit leurs départs, et leurs étapes, d'après l'ordre de YHWH ! Et voici leurs étapes selon leurs départs.
33:3	Les enfants d'Israël donc partirent de Ramsès le quinzième jour du premier mois, dès le lendemain de la Pâque, et ils sortirent à main levée, à la vue de tous les Égyptiens<!--Ex. 14:8.-->.
33:4	Et les Égyptiens ensevelissaient ceux que YHWH avait frappés parmi eux, tous les premiers-nés. YHWH exerçait aussi ses jugements contre leurs elohîm<!--Ex. 12:12, 18:11.-->.
33:5	Et les enfants d'Israël partirent de Ramsès, et campèrent à Soukkoth<!--Ex. 12:37.-->.
33:6	Et ils partirent de Soukkoth et campèrent à Étham, qui est au bout du désert<!--Ex. 13:20.-->.
33:7	Et ils partirent d'Étham et se détournèrent vers Pi-Hahiroth, qui est vis-à-vis de Baal-Tsephon, et campèrent devant Migdol<!--Ex. 14:2.-->.
33:8	Et ils partirent de devant Pi-Hahiroth et passèrent au travers de la mer vers le désert, et firent trois journées de marche par le désert d'Étham et campèrent à Marah.
33:9	Puis ils partirent de Marah et vinrent à Élim où il y avait 12 fontaines d'eaux et 70 palmiers, et ils y campèrent<!--Ex. 15:27.-->.
33:10	Et ils partirent d'Élim et campèrent près de la Mer Rouge.
33:11	Puis ils partirent de la Mer Rouge et campèrent dans le désert de Sin<!--Ex. 16:1.-->.
33:12	Ils partirent du désert de Sin et campèrent à Dophka.
33:13	Puis ils partirent de Dophka et campèrent à Aloush.
33:14	Et ils partirent d'Aloush et campèrent à Rephidim où il n'y avait pas d'eau à boire pour le peuple<!--Ex. 17:1.-->.
33:15	Puis ils partirent de Rephidim et campèrent dans le désert de Sinaï<!--Ex. 19:2.-->.
33:16	Ils partirent du désert de Sinaï et campèrent à Kibroth-Hattaava.
33:17	Et ils partirent de Kibroth-Hattaava et campèrent à Hatséroth.
33:18	Puis ils partirent de Hatséroth et campèrent à Rithmah.
33:19	Et ils partirent de Rithmah et campèrent à Rimmon-Pérets.
33:20	Ils partirent de Rimmon-Pérets et campèrent à Libnah.
33:21	Et ils partirent de Libnah et campèrent à Rissah.
33:22	Puis ils partirent de Rissah et campèrent vers Qehélatah.
33:23	Et ils partirent de Qehélatah et campèrent à la montagne de Shapher.
33:24	Ils partirent de la montagne de Shapher et campèrent à Haradah.
33:25	Et ils partirent de Haradah et campèrent à Makhéloth.
33:26	Puis ils partirent de Makhéloth et campèrent à Tahath.
33:27	Ils partirent de Tahath et campèrent à Tarach.
33:28	Et ils partirent de Tarach et campèrent à Mithqah.
33:29	Puis ils partirent de Mithqah et campèrent à Hashmonah.
33:30	Ils partirent de Hashmonah et campèrent à Moséroth.
33:31	Et ils partirent de Moséroth et campèrent à Bené Ya`aqan.
33:32	Ils partirent de Bené Ya`aqan et campèrent à Hor-Guidgad.
33:33	Puis ils partirent de Hor-Guidgad et campèrent vers Yotbathah.
33:34	Ils partirent de Yotbathah et campèrent à Abrona.
33:35	Et ils partirent d'Abrona et campèrent à Etsyôn-Guéber.
33:36	Ils partirent d'Etsyôn-Guéber et campèrent dans le désert de Tsin, qui est Qadesh.
33:37	Puis ils partirent de Qadesh et campèrent à la montagne de Hor, qui est au bout du pays d'Édom.
33:38	Et Aaron le prêtre, monta sur la montagne de Hor, suivant l'ordre de YHWH, et mourut là, la quarantième année après que les enfants d'Israël furent sortis du pays d'Égypte, le premier jour du cinquième mois.
33:39	Et Aaron était âgé de 123 ans quand il mourut sur la montagne de Hor.
33:40	Alors le Cananéen, roi d'Arad, qui habitait vers le midi au pays de Canaan, apprit que les enfants d'Israël venaient.
33:41	Et ils partirent de la montagne de Hor et campèrent à Tsalmona.
33:42	Puis ils partirent de Tsalmona et campèrent à Pounôn.
33:43	Et ils partirent de Pounôn et campèrent à Oboth.
33:44	Ils partirent d'Oboth et campèrent à Iyey-Abariym, sur les frontières de Moab.
33:45	Puis ils partirent d'Iyîm<!--Iyey-Abariym.--> et campèrent à Dibon-Gad.
33:46	Et ils partirent de Dibon-Gad, et campèrent à Almon-Diblathaïm.
33:47	Ils partirent d'Almon-Diblathaïm et campèrent aux montagnes d'Abarim devant Nébo.
33:48	Et ils partirent des montagnes d'Abarim et campèrent aux régions arides de Moab, près du Yarden de Yeriycho.
33:49	Puis ils campèrent près du Yarden, depuis Beth-Yeshiymoth jusqu'à Abel-Sittim, dans les régions arides de Moab.

### Consignes pour les possessions attribuées à Israël

33:50	Et YHWH parla à Moshè dans les régions arides de Moab, près du Yarden de Yeriycho, en disant :
33:51	Parle aux enfants d'Israël, et dis-leur : Puisque vous allez passer le Yarden pour entrer au pays de Canaan,
33:52	vous chasserez devant vous tous les habitants du pays, vous détruirez toutes leurs figures, vous détruirez toutes leurs images de métal fondu et vous démolirez tous leurs hauts lieux<!--De. 7:5, 12:2.-->.
33:53	Et vous prendrez possession du pays et vous y habiterez, car je vous ai donné le pays pour qu'il soit votre propriété.
33:54	Or vous recevrez le pays en héritage par le sort, selon vos familles. À ceux qui sont en plus grand nombre, vous donnerez plus d'héritage, et à ceux qui sont en plus petit nombre, vous donnerez moins d'héritage. Chacun aura selon ce qui lui sera échu par le sort, et vous hériterez selon les tribus de vos pères.
33:55	Mais si vous ne chassez pas devant vous les habitants du pays, il arrivera que ceux d'entre eux que vous aurez laissés comme reste, seront comme des épines à vos yeux, et comme des pointes à vos côtés, et ils vous serreront de près dans le pays où vous habiterez<!--Jos. 23:13.-->.
33:56	Et il arrivera que je vous traiterai comme j'avais résolu de les traiter eux.

## Chapitre 34

### Consignes sur les limites de chaque tribu

34:1	YHWH parla aussi à Moshè, en disant :
34:2	Donne l'ordre aux enfants d'Israël, et dis-leur : Parce que vous allez entrer au pays de Canaan, ce pays deviendra votre héritage, le pays de Canaan selon ses limites.
34:3	Votre frontière du côté du sud sera depuis le désert de Tsin, le long d'Édom, et votre frontière du côté du sud commencera au bout de la Mer Salée, vers l'orient ;
34:4	et cette frontière tournera du sud vers la montée d'Akrabbim, et passera jusqu'à Tsin et elle aboutira du côté du sud de Qadesh-Barnéa. Elle sortira aussi par Hatsar-Addar et passera jusqu'à Atsmon.
34:5	Et cette frontière tournera depuis Atsmon jusqu'au torrent d'Égypte et elle aboutira à la mer.
34:6	Quant à la frontière d'occident, vous aurez la grande mer et ses limites. Ce sera votre frontière occidentale.
34:7	Et ce sera ici votre frontière au nord ; depuis la grande mer, vous marquerez pour vos limites la montagne de Hor ;
34:8	et depuis la montagne de Hor, vous marquerez pour vos limites l'entrée de Hamath, et cette frontière aboutira vers Tsedad ;
34:9	cette frontière passera jusqu'à Ziphron, et elle aboutira à Hatsar-Énan. Telle sera votre frontière au nord.
34:10	Puis vous marquerez pour vos limites vers l'orient de Hatsar-Énan à Shepham.
34:11	Et cette frontière descendra de Shepham à Ribla, du côté de l'orient d'Aïn. Cette frontière descendra et s'étendra le long de la Mer de Kinnéreth vers l'orient.
34:12	Cette frontière descendra au Yarden pour aboutir à la Mer Salée ; tel sera le pays que vous aurez avec ses limites tout autour.
34:13	Et Moshè donna l'ordre aux enfants d'Israël, en disant : C'est là le pays que vous hériterez par le sort, et que YHWH a ordonné de donner à neuf tribus, et à la demi-tribu.
34:14	Car la tribu des fils de Reouben selon les familles de leurs pères, et la tribu des fils de Gad, selon les familles de leurs pères, ont pris leur héritage ; et la demi-tribu de Menashè a pris aussi son héritage.
34:15	Deux tribus et la demi-tribu ont pris leur héritage de l'autre côté du Yarden, vis-à-vis de Yeriycho, du côté du levant.
34:16	Et YHWH parla à Moshè, en disant :
34:17	Voici les noms des hommes qui vous partageront le pays : Èl’azar le prêtre, et Yéhoshoua fils de Noun.
34:18	Vous prendrez aussi un prince de chaque tribu pour faire le partage du pays.
34:19	Et voici les noms de ces hommes. Pour la tribu de Yéhouda : Caleb, fils de Yephounné ;
34:20	pour la tribu des fils de Shim’ôn : Shemouél, fils d'Ammihoud ;
34:21	pour la tribu de Benyamin : Élidad, fils de Kislon ;
34:22	pour la tribu des fils de Dan : Celui qui en est le prince, Bouqqi, fils de Yogli ;
34:23	pour les fils de Yossef, pour la tribu des fils de Menashè : Celui qui en est le chef, Hanniel, fils d'Éphod ;
34:24	et pour la tribu des fils d'Éphraïm : Celui qui en est le prince, Kemouel, fils de Shiphtan ;
34:25	pour la tribu des fils de Zebouloun : Celui qui en est le prince, Élitsaphan, fils de Parnac ;
34:26	pour la tribu des fils de Yissakar : Celui qui en est le prince, Paltiel, fils d'Azzan ;
34:27	pour la tribu des fils d'Asher : Celui qui en est le prince, Ahihoud, fils de Shelomi ;
34:28	pour la tribu des fils de Nephthali : Celui qui en est le prince, Pedahel, fils d'Ammihoud.
34:29	Ce sont là, ceux à qui YHWH donna l'ordre de partager l'héritage aux enfants d'Israël dans le pays de Canaan.

## Chapitre 35

### 48 villes pour les Lévites dont 6 villes de refuge

35:1	YHWH parla à Moshè dans les régions arides de Moab, près du Yarden, vis-à-vis de Yeriycho, en disant :
35:2	Donne l'ordre aux enfants d'Israël qu'ils donnent aux Lévites, sur l'héritage qu'ils posséderont, des villes pour y habiter. Vous leur donnerez aussi les faubourgs qui sont autour de ces villes<!--Jos. 21:2.-->.
35:3	Ils auront donc les villes pour y habiter et les faubourgs de ces villes seront pour leurs bétails, pour leurs biens et pour tous leurs animaux.
35:4	Les faubourgs des villes que vous donnerez aux Lévites seront de 1 000 coudées tout autour depuis la muraille de la ville en dehors.
35:5	Vous mesurerez à l'extérieur de la ville du côté de l'orient, 2 000 coudées, du côté du sud, 2 000 coudées, du côté de l'occident, 2 000 coudées du côté du nord, 2 000 coudées. La ville sera au milieu. Tels seront les faubourgs de leurs villes.
35:6	Et des villes que vous donnerez aux Lévites, il y aura 6 villes de refuge que vous donnerez pour que le meurtrier s'y enfuie, et outre celles-là, vous leur donnerez 42 villes.
35:7	Toutes les villes que vous donnerez aux Lévites seront 48 villes, elles et leurs faubourgs.
35:8	Et quant aux villes que vous leur donnerez sur la possession des enfants d'Israël, de ceux qui en auront plus vous en prendrez plus, et de ceux qui en auront moins vous en prendrez moins ; chacun donnera de ses villes aux Lévites, en proportion de l'héritage qu'il possédera.
35:9	Puis YHWH parla à Moshè, en disant :
35:10	Parle aux enfants d'Israël, et dis-leur : Quand vous aurez passé le Yarden, pour entrer au pays de Canaan,
35:11	établissez-vous des villes qui vous soient des villes de refuge, afin que le meurtrier qui aura frappé à mort quelqu'un involontairement s'y enfuie<!--Jos. 20:2-3 ; Ex. 21:13.-->.
35:12	Et ces villes seront pour vous des villes de refuge pour échapper au rédempteur<!--Dans ce passage, il s'agit d'une revendication, d'un rachat au prix du sang du coupable et non d'une simple vengeance.-->, afin que le meurtrier ne meure pas, jusqu'à ce qu'il ait comparu en jugement devant l'assemblée.
35:13	De ces villes que vous donnerez, il y en aura 6 de refuge pour vous.
35:14	Vous donnerez 3 de ces villes au-delà du Yarden, et les 3 autres dans le pays de Canaan, qui seront des villes de refuge<!--De. 19:2, 4:41-42.-->.
35:15	Ces 6 villes serviront de refuge aux enfants d'Israël, à l'étranger et à celui qui séjourne au milieu de vous, afin que quiconque aura frappé à mort quelqu'un involontairement, s'y enfuie.
35:16	Mais si un homme en frappe un autre avec un instrument de fer, et qu'il en meure, il est meurtrier. Le meurtrier sera mis à mort, il sera mis à mort.
35:17	Et s'il le frappe avec une pierre qu'il tenait à la main, dont on puisse mourir, et qu'il en meure, c'est un meurtrier. Le meurtrier sera mis à mort, il sera mis à mort.
35:18	De même s'il le frappe d'un instrument de bois qu'il tenait à la main, dont on puisse mourir, et qu'il en meure, il est un meurtrier. Le meurtrier sera mis à mort, il sera mis à mort.
35:19	Et le rédempteur du sang fera mourir le meurtrier quand il le rencontrera, il pourra le faire mourir.
35:20	Et s'il le pousse par haine, ou s'il jette quelque chose sur lui avec préméditation, et qu'il en meure,
35:21	ou si par inimitié il le frappe de sa main, et qu'il en meure, celui qui a frappé sera mis à mort, il sera mis à mort, c'est un meurtrier. Le rédempteur du sang pourra le faire mourir quand il le rencontrera<!--De. 19:11-12.-->.
35:22	Mais s'il le pousse subitement, sans inimitié, ou s'il jette quelque chose sur lui, sans préméditation,
35:23	ou s'il fait tomber sur lui quelque pierre sans l'avoir vu, et qu'il en meure, n'étant pas son ennemi et ne lui cherchant pas du mal,
35:24	alors l'assemblée jugera entre celui qui a frappé et le rédempteur du sang, selon ces ordonnances.
35:25	L'assemblée délivrera le meurtrier de la main du rédempteur du sang, et le fera retourner dans la ville de refuge où il s'était enfui. Il y demeurera jusqu'à la mort du grand-prêtre, qui aura été oint de la sainte huile.
35:26	Mais si le meurtrier sort de quelque manière que ce soit hors des bornes de la ville de son refuge, où il s'est enfui,
35:27	et si le rédempteur du sang le rencontre hors des bornes de la ville de son refuge, et qu'il tue le meurtrier, il ne sera pas coupable de meurtre.
35:28	Car il doit demeurer dans la ville de son refuge jusqu'à la mort du grand-prêtre. Ce n'est qu'après la mort du grand-prêtre que le meurtrier pourra retourner dans sa possession.
35:29	Et ces choses-ci seront des statuts de jugement pour vous et pour vos générations, dans toutes vos demeures.
35:30	Toutes les fois qu'une âme aura été tuée, c'est sur la parole de témoins qu'on assassinera le meurtrier. Mais un seul témoin ne rendra pas témoignage contre quelqu'un pour le faire mourir<!--De. 17:6, 19:15.-->.
35:31	Et vous ne prendrez pas de rançon pour la vie du meurtrier qui est coupable et digne de mort. Mais il sera mis à mort, il sera mis à mort.
35:32	Vous ne prendrez pas de rançon pour le laisser s'enfuir de sa ville de refuge, pour qu'il retourne habiter dans le pays, jusqu'à la mort du prêtre.
35:33	Et vous ne souillerez pas le pays où vous serez. En effet, le sang souille le pays. Il ne sera fait pour le pays aucune propitiation du sang qui y est répandu, sinon par le sang de celui qui l'a répandu.
35:34	Vous ne souillerez donc pas le pays où vous allez demeurer, et au milieu duquel j'habiterai, car je suis YHWH qui habite au milieu des enfants d'Israël.

## Chapitre 36

### Torah sur les héritages<!--No. 27:1-11.-->

36:1	Or les princes des pères de la famille des fils de Galaad, fils de Makir, fils de Menashè, d'entre les familles des fils de Yossef, s'approchèrent et parlèrent devant Moshè, et devant les princes, les chefs des pères des enfants d'Israël,
36:2	et ils dirent : YHWH a donné l'ordre à mon seigneur de donner aux enfants d'Israël le pays en héritage par le sort. Mon seigneur a reçu l'ordre de YHWH de donner l'héritage de Tselophchad, notre frère, à ses filles.
36:3	Si elles deviennent femmes de quelqu'un des fils des autres tribus d'Israël, leur héritage sera retranché de l'héritage de nos pères et sera ajouté à l'héritage de la tribu de laquelle elles appartiendront. Ainsi l'héritage qui nous est échu par le sort sera diminué.
36:4	Même quand viendra le jubilé pour les enfants d'Israël, on ajoutera leur héritage à l'héritage de la tribu à laquelle elles appartiendront, ainsi leur héritage sera retranché de l'héritage de la tribu de nos pères<!--Lé. 25:10-13.-->.
36:5	Et Moshè donna cet ordre aux enfants d'Israël, suivant l'ordre de la bouche de YHWH, en disant : Ce que la tribu des fils de Yossef dit est juste.
36:6	Voici la parole que YHWH a ordonnée au sujet des filles de Tselophchad, disant : Elles pourront devenir les femmes de qui sera bon à leurs yeux. Toutefois, c'est dans la famille de la tribu de leurs pères qu'elles doivent devenir femmes.
36:7	Ainsi l'héritage ne sera pas transporté entre les enfants d'Israël de tribu en tribu, car chacun des enfants d'Israël se tiendra à l'héritage de la tribu de ses pères.
36:8	Et toute fille qui possédera un héritage dans l'une des tribus des enfants d'Israël devra devenir la femme de quelqu'un de la famille de la tribu de son père, afin que chacun des enfants d'Israël possède l'héritage de ses pères.
36:9	L'héritage donc ne sera pas transporté d'une tribu à une autre, mais chacune des tribus des enfants d'Israël se tiendra à son héritage.
36:10	Les filles de Tselophchad agirent selon l'ordre que YHWH avait donné à Moshè.
36:11	Machlah, Tirtsah, Hoglah, Milkah, et No`ah, filles de Tselophchad, se marièrent aux fils de leurs oncles.
36:12	Elles devinrent femmes dans les familles des fils de Menashè, fils de Yossef, et leur héritage demeura dans la tribu de la famille de leur père.
36:13	Ce sont là les ordonnances et les jugements que YHWH ordonna par Moshè aux enfants d'Israël, dans les régions arides de Moab, près du Yarden, vis-à-vis de Yeriycho.
