# Amowc (Amos) (Am.)

Signification : Fardeau, porteur de fardeau

Auteur : Amos

Thème : Jugement sur le péché

Date de rédaction : 8ème siècle av. J.-C.

Originaire de Tekoa, Amos exerça son service dans le royaume du nord, au temps d'Ouzyah (Ozias), roi de Yéhouda (Juda), et de Yarobam II (Jéroboam II), roi d'Israël. Il fut aussi le contemporain des prophètes Hoshea (Osée), Mikayah (Michée), Yonah (Jonas) et Yesha`yah (Ésaïe).

Alors que le peuple juif jouissait d'une certaine prospérité, l'immoralité et les sacrilèges prirent place dans le royaume. Amos avertit le peuple de son péché et du jugement qu'il encourait. Il lui rappela la bonté d'Elohîm et l'invita à revenir à YHWH et à lui rester fidèle.

## Chapitre 1

1:1	Les paroles d'Amos qui était parmi les bergers de Tekoa, visions qu'il eut concernant Israël, aux temps d'Ouzyah, roi de Yéhouda, et de Yarobam, fils de Yoash, roi d'Israël, deux ans avant le tremblement de terre<!--Za. 14:5.-->.
1:2	Il dit donc : YHWH rugit de Sion, et fait entendre sa voix de Yeroushalaim, et les pâturages des bergers se lamentent, et le sommet du Carmel est desséché<!--Jé. 25:30 ; Joë. 3:16.-->.

### YHWH annonce ses jugements sur les villes et les pays d'alentour

1:3	Ainsi parle YHWH : À cause de trois transgressions de Damas, et même de quatre, je ne le ferai pas revenir<!--Revenir à Elohîm, se repentir. Voir Am. 9:14.-->, parce qu'ils ont foulé Galaad avec des herses de fer<!--Es. 17:1.-->.
1:4	Et j'enverrai le feu dans la maison de Hazaël, et il dévorera le palais de Ben-Hadad.
1:5	Je briserai aussi les verrous de Damas, j'exterminerai de Bikath-Aven ses habitants, et de Beth-Éden celui qui tient le sceptre, et le peuple de Syrie sera emmené en exil à Kir, dit YHWH.
1:6	Ainsi parle YHWH : À cause de trois transgressions de Gaza, et même de quatre, je ne le ferai pas revenir<!--Voir commentaire en Am. 1:3.-->, parce qu'ils ont emmené des captifs en grand nombre pour les livrer à Édom<!--Ez. 25:13-17.-->.
1:7	Et j'enverrai le feu dans les murailles de Gaza, et il dévorera ses palais.
1:8	Et j'exterminerai d'Asdod les habitants et d'Askalon celui qui tient le sceptre, je tournerai ma main contre Ékron, et le reste des Philistins périra, dit Adonaï YHWH.
1:9	Ainsi parle YHWH : À cause de trois transgressions de Tyr, et même de quatre, je ne le ferai pas revenir<!--Voir commentaire en Am. 1:3.-->, parce qu'ils ont livré à Édom beaucoup d'exilés et ne se sont pas souvenus de l'alliance fraternelle<!--Ez. 26:2.-->.
1:10	Et j'enverrai le feu dans les murailles de Tyr, et il dévorera ses palais.
1:11	Ainsi parle YHWH : À cause de trois transgressions d'Édom, et même de quatre, je ne le ferai pas revenir<!--Voir commentaire en Am. 1:3.-->, parce qu'il a poursuivi son frère avec l'épée et qu'il a altéré ses compassions, parce que sa colère déchire continuellement et qu'il garde sa fureur éternellement.
1:12	Et j'enverrai le feu dans Théman, et il dévorera les palais de Botsrah<!--Jé. 49:7 ; Ab. 1:9.-->.
1:13	Ainsi parle YHWH : À cause de trois transgressions des enfants d'Ammon, et même de quatre, je ne le ferai pas revenir<!--Voir commentaire en Am. 1:3.-->, parce qu'ils ont fendu le ventre des femmes enceintes de Galaad pour élargir leurs frontières<!--Ez. 21:33 ; So. 2:8.-->.
1:14	Et j'allumerai le feu dans les murs de Rabba, et il en dévorera les palais au milieu d'alarme de guerre le jour du combat, dans le tourbillon au jour de la tempête.
1:15	Et leur roi ira en captivité, lui et ses chefs ensemble, dit YHWH.

## Chapitre 2

### Suite des jugements prononcés sur les villes et les pays d'alentour

2:1	Ainsi parle YHWH : À cause de trois transgressions de Moab, et même de quatre, je ne le ferai pas revenir, parce qu'il a brûlé les os du roi d'Édom jusqu'à les calciner.
2:2	Et j'enverrai le feu dans Moab, et il dévorera les palais de Qeriyoth, et Moab mourra dans le tumulte, au milieu d'alarme de guerre et du bruit du shofar<!--Ez. 25:8-11.-->.
2:3	Et j'exterminerai les gouverneurs du milieu de son pays et je tuerai ensemble avec lui tous les chefs du pays, dit YHWH.

### Yéhouda et Israël jugés à cause de leurs iniquités

2:4	Ainsi parle YHWH : À cause de trois transgressions de Yéhouda, et même de quatre, je ne le ferai pas revenir, parce qu'ils ont rejeté la torah de YHWH et n'ont pas gardé ses ordonnances ; mais leurs mensonges que leurs pères avaient suivis les ont fait égarer.
2:5	Et j'enverrai le feu dans Yéhouda, et il dévorera les palais de Yeroushalaim.
2:6	Ainsi parle YHWH : À cause de trois transgressions d'Israël, et même de quatre, je ne le ferai pas revenir, parce qu'ils ont vendu le juste pour de l'argent et le pauvre pour une paire de sandales.
2:7	Ils soupirent après la poussière de la terre pour la jeter sur la tête des faibles, et ils pervertissent la voie des pauvres. Le fils et le père vont vers la même jeune fille pour profaner mon saint Nom.
2:8	Et ils se couchent près de chaque autel, sur les vêtements qu'ils ont pris en gage, et boivent dans la maison de leurs elohîm le vin des condamnés.
2:9	J'ai pourtant détruit devant eux les Amoréens qui étaient hauts comme les cèdres et forts comme les chênes, j'ai détruit son fruit au-dessus et leurs racines en dessous<!--No. 21:24-25 ; Jos. 24:8.-->.
2:10	Je vous ai fait monter du pays d'Égypte et je vous ai conduits dans le désert 40 ans pour que vous possédiez le pays des Amoréens.
2:11	J'ai suscité des prophètes parmi vos fils et des nazaréens<!--Le mot « nazaréen » vient de l'hébreu « nâzîr », de la racine « nâzar » qui signifie « séparer ». Il y avait deux types de nazaréens. Premièrement, ceux qui étaient appelés par Elohîm. Par exemple : Shimshôn (Samson) (Jg. 13:2-7), Shemouél (Samuel) (1 S. 1:11) et Yohanan le Baptiste (Jean-Baptiste) (Lu. 1:15). Deuxièmement, les personnes qui voulaient se consacrer à Elohîm (No. 6:1-3).--> parmi vos jeunes hommes. N'en est-il pas ainsi, enfants d'Israël ? dit YHWH.
2:12	Mais vous avez fait boire du vin aux nazaréens, et vous avez donné cet ordre aux prophètes leur disant : Ne prophétisez plus<!--Es. 30:10 ; Jé. 11:21 ; Mi. 2:6.--> !
2:13	Voici, je vais fouler le lieu où vous habitez, comme un chariot plein de gerbes foule tout par où il passe.
2:14	Même l'homme le plus rapide ne trouvera pas de refuge, la force du fort ne s'affermira pas, et le vaillant ne sauvera pas sa vie<!--Jé. 46:6.-->.
2:15	Celui qui manie l'arc ne pourra pas tenir ferme, celui qui a les pieds légers n'échappera pas et celui qui monte le cheval ne sauvera pas sa vie.
2:16	Celui qui a le cœur plein de courage d'entre les hommes vaillants s'enfuira tout nu en ce jour-là, dit YHWH.

## Chapitre 3

### La maison de Yaacov coupable devant YHWH

3:1	Enfants d'Israël écoutez la parole que YHWH a prononcée contre vous, contre toutes les familles que j'ai fait monter du pays d'Égypte, en disant :
3:2	Je vous ai connus, vous seuls parmi toutes les familles de la Terre, c'est pourquoi je vous châtierai pour toutes vos iniquités<!--Ex. 19:5-6 ; Ps. 147:19-20.-->.
3:3	Deux hommes marchent-ils ensemble s'ils ne se sont pas rencontrés ?
3:4	Le lion rugit-il dans la forêt sans avoir une proie ? Le jeune lion fait-il entendre sa voix de sa tanière s'il n'a rien capturé ?
3:5	L'oiseau tombe-t-il dans le filet qui est à terre sans qu'il y ait de piège ? Le filet s'élève-t-il du sol sans avoir pris quelque chose ?
3:6	Sonne-t-on du shofar dans une ville sans que le peuple ait peur ? Arrive-t-il un mal dans une ville sans que YHWH ne l'ait ordonné<!--Es. 45:7 ; La. 3:37-38.--> ?
3:7	Car Adonaï YHWH ne fait rien<!--Vient de l'hébreu « dabar » et signifie « parole », « discours », « chose ». On peut traduire ce verset par : « Car Adonaï YHWH ne fait aucune chose sans avoir révélé son secret à ses serviteurs les prophètes » ou par « Car Adonaï YHWH n'accomplit aucune parole sans avoir révélé son secret à ses serviteurs les prophètes ».--> sans avoir révélé son secret à ses serviteurs les prophètes.
3:8	Le lion rugit, qui ne craindrait ? Adonaï YHWH parle, qui ne prophétiserait<!--Lorsque YHWH parle, des prophètes sont suscités (Jé. 20:7-9 ; Mi. 3:8 ; Ac. 4:20).--> ?
3:9	Faites entendre ceci dans les palais d'Asdod et dans les palais du pays d'Égypte, et dites : Rassemblez-vous sur les montagnes de Samarie et regardez les grands désordres au milieu d'elle, et ceux auxquels on a fait tort dans son sein !
3:10	Car ils ne savent pas faire ce qui est juste, dit YHWH, ils amassent la violence et la rapine dans leurs palais.
3:11	C'est pourquoi ainsi parle Adonaï YHWH : L'ennemi viendra, il cernera le pays, il t'ôtera ta force et tes palais seront pillés.
3:12	Ainsi parle YHWH : Comme un berger sauve de la gueule d'un lion les deux jambes ou le bout d'une oreille, ainsi les enfants d'Israël qui habitent dans Samarie seront arrachés de l'angle d'un lit et de l'asile de Damas.
3:13	Écoutez et protestez contre la maison de Yaacov, dit Adonaï YHWH, Elohîm Sabaoth :
3:14	Le jour où je punirai Israël pour ses transgressions, j'exercerai aussi mon châtiment sur les autels de Béth-El ; les cornes de l'autel seront retranchées et tomberont à terre.
3:15	Je frapperai la maison d'hiver et la maison d'été, les maisons d'ivoire seront détruites et les grandes maisons prendront fin, dit YHWH.

## Chapitre 4

### YHWH condamne les sacrifices du peuple

4:1	Écoutez cette parole, vaches de Bashân qui vous tenez sur la montagne de Samarie, vous qui opprimez les faibles, qui écrasez les pauvres et qui dites à leurs seigneurs : Apportez afin que nous buvions !
4:2	Adonaï YHWH l'a juré par sa sainteté : Voici, les jours viennent sur vous où l'on vous enlèvera avec des crochets, et votre postérité avec des hameçons de pêche<!--Jé. 16:16 ; Ha. 1:14-16.-->.
4:3	Les femmes sortiront par des brèches, chacune de son côté, et elles seront jetées hors de la haute forteresse, dit YHWH.
4:4	Allez à Béth-El et transgressez ! À Guilgal, et transgressez davantage ! Allez avec vos sacrifices dès le matin et vos dîmes tous les trois ans<!--Voir commentaires en No. 18:21 et Mal. 3:10.--> !
4:5	Faites fumer vos offrandes d'action de grâces avec du levain ! Proclamez vos offrandes volontaires, faites-les connaître ! Car c'est là ce que vous aimez, enfants d'Israël, dit Adonaï YHWH<!--Lé. 2:1.-->.

### Endurcissement du peuple malgré les châtiments de YHWH

4:6	C'est pourquoi, je vous ai donné la propreté des dents dans toutes vos villes, le manque de pain dans toutes vos demeures. Mais malgré cela, vous n'êtes pas revenus vers moi, dit YHWH.
4:7	Je vous ai même refusé la pluie quand il restait encore trois mois jusqu'à la moisson, j'ai fait pleuvoir sur une ville et je n'ai pas fait pleuvoir sur une autre ville. Une parcelle a été arrosée par la pluie, et l'autre parcelle sur laquelle il n'a pas plu s’est desséchée<!--1 R. 8:35, 17:1 ; Es. 5:6 ; Ag. 1:10-11.-->.
4:8	Et deux, même trois villes sont allées vers une autre ville pour boire de l'eau et n'ont pas été désaltérées, mais vous n'êtes pas revenus vers moi, dit YHWH.
4:9	Je vous ai frappés par la sécheresse et par la rouille. Vos nombreux jardins, vos vignes, vos figuiers et vos oliviers ont été dévorés par les sauterelles, mais vous n'êtes pas revenus vers moi, dit YHWH<!--De. 28:22-39 ; 1 R. 8:37 ; Ag. 2:17 ; 2 Ch. 6:28.-->.
4:10	J'ai envoyé parmi vous la peste de la même manière que je l'avais envoyée en Égypte, j'ai fait mourir par l'épée vos jeunes hommes et vos chevaux en captivité. J'ai fait monter jusqu’à vos narines la puanteur de vos camps, mais vous n'êtes pas revenus vers moi, dit YHWH<!--Ez. 14:19.-->.
4:11	Je vous ai renversés de la même manière qu'Elohîm renversa Sodome et Gomorrhe, et vous avez été comme un tison arraché du feu, mais vous n'êtes pas revenus vers moi, dit YHWH<!--Ge. 19:24 ; Jé. 49:18 ; Za. 3:2.-->.
4:12	C'est pourquoi je te traiterai de la même manière, Israël ! Et parce que je te traiterai ainsi, prépare-toi à la rencontre de ton Elohîm, ô Israël !
4:13	Car voici celui qui a formé les montagnes et créé le vent, qui déclare à l'être humain quelle est sa pensée, qui fait l'aube et l'obscurité, et qui marche sur les hauteurs de la Terre. Son Nom est YHWH, Elohîm Sabaoth.

## Chapitre 5

### La maison d'Israël invitée à revenir entièrement à YHWH

5:1	Écoutez cette parole, le chant funèbre que j'élève sur vous, ô maison d'Israël :
5:2	Elle est tombée, elle ne se relèvera plus, la vierge d'Israël. Elle est jetée à terre et il n'y a personne pour la relever.
5:3	Car ainsi parle Adonaï YHWH à la maison d'Israël : La ville d'où sortait un millier d'hommes n'en aura plus que 100, et celle d'où sortait une centaine n'en aura plus que 10.
5:4	Car ainsi a parlé YHWH à la maison d'Israël : Cherchez-moi et vous vivrez !
5:5	Ne cherchez pas Béth-El, n'allez pas à Guilgal, et ne passez pas à Beer-Shéba. Car Guilgal ira en exil, elle ira en exil et Béth-El sera détruite<!--Os. 4:15.-->.
5:6	Cherchez YHWH et vous vivrez, de peur qu'il ne saisisse comme un feu la maison de Yossef, et que ce feu ne la consume, sans qu'il y ait personne à Béth-El pour l'éteindre.
5:7	Ils changent le jugement en absinthe et ils renversent la justice<!--Es. 5:26-28 ; Ha. 1:1-3.-->.
5:8	Cherchez celui qui a fait les Pléiades et l'Orion, qui change les profondes ténèbres en aube du matin et qui obscurcit le jour en nuit, qui appelle les eaux de la mer et les répand sur la surface de la Terre. YHWH est son Nom<!--Es. 58:8-10 ; Job 9:9, 38:31.-->.
5:9	Il fait éclater la ruine sur les puissants et la ruine vient sur les villes fortes.
5:10	Ils haïssent celui qui les corrige à la porte et ils ont en abomination celui qui parle avec intégrité.
5:11	C'est pourquoi, puisque vous opprimez le pauvre et que vous prélevez sur lui un tribut de blé, vous avez bâti des maisons en pierres de taille, mais vous n'y habiterez pas ; vous avez planté des vignes délicieuses, mais vous n'en boirez pas le vin.
5:12	Car je connais vos transgressions, elles sont nombreuses, et vos péchés se sont multipliés. Vous êtes des oppresseurs du juste, et des preneurs de rançon, et vous pervertissez le droit des pauvres à la porte.
5:13	C'est pourquoi le perspicace, garde le silence en ce temps-ci, car les temps sont mauvais.
5:14	Recherchez ce qui est bon et non ce qui est mauvais, afin que vous viviez, et qu'ainsi YHWH, Elohîm Sabaoth, soit avec vous, comme vous l'avez dit.
5:15	Haïssez ce qui est mauvais et aimez ce qui est bon, et établissez la justice à la porte. Peut-être YHWH, Elohîm Sabaoth, aura pitié des restes de Yossef.

### Le jour de YHWH

5:16	C'est pourquoi, ainsi parle YHWH, Elohîm Sabaoth, Adonaï : Sur toutes les places on se lamentera, dans toutes les rues on dira : Hélas ! Hélas ! On appellera au deuil le laboureur, et aux lamentations ceux qui s'y connaissent en gémissements.
5:17	Et il y aura des lamentations dans les vignes, car je passerai au milieu de toi, dit YHWH.
5:18	Malheur à vous qui désirez le jour de YHWH<!--L'expression « le jour du Seigneur » ou « le jour de YHWH » est une période durant laquelle Yéhoshoua ha Mashiah interviendra ouvertement dans les affaires des êtres humains. Elle est utilisée 19 fois dans le Tanakh (Es. 2:12, 13:6-9 ; Ez. 13:5, 30:3 ; Joë. 1:15, 2:1,11,31, 3:14 ; Am. 5:18, 20 ; Ab. 1:15 ; So. 1:7,14 ; Za. 14:1 ; Mal. 4:5) et 4 fois dans le Testament de Yéhoshoua (Jésus) (Ac. 2:20 ; 2 Th. 2:2 ; 2 Pi. 3:10). On y fait également allusion dans d'autres passages (Ap. 6:17, 16:14).-->. À quoi vous servira le jour de YHWH ? Il sera ténèbres et non lumière.
5:19	C'est comme si un homme s'enfuit devant un lion et qu'un ours le rencontre : ou qui entre dans sa maison, appuie sa main sur le mur, et un serpent le mord.

### Mépris du droit et de la justice

5:20	Le jour de YHWH n'est-il pas ténèbres et non lumière ? N'est-il pas obscur, sans clarté ?
5:21	Je hais, je rejette vos fêtes, et je ne veux plus sentir l'odeur de vos assemblées solennelles.
5:22	Car si vous m'offrez des holocaustes et des offrandes de grain, je ne les accepterai pas favorablement, et les bêtes grasses de vos offrandes de paix<!--Voir commentaire en Lé. 3:1.-->, je ne les regarderai pas.
5:23	Ôtez de devant moi le bruit de vos chansons ! Je n'écouterai pas la mélodie de vos luths.
5:24	Mais que le jugement coule comme de l'eau, et la justice comme un torrent intarissable.
5:25	Est-ce à moi, maison d'Israël, que vous avez offert des sacrifices et des gâteaux dans le désert pendant 40 ans ?
5:26	Au contraire, vous avez porté Sikkouth<!--« Une divinité babylonienne », « une tente ».-->, votre roi, Kiyoun<!--Kiyoun est une divinité de l'antiquité ainsi que l'un des nombreux noms donnés au soleil par les civilisations antiques.-->, votre idole, l'étoile de votre elohîm que vous vous êtes fabriquée.
5:27	C'est pourquoi je vous transporterai au-delà de Damas, dit YHWH, dont le Nom est Elohîm Sabaoth.

## Chapitre 6

### Ceux qui prospèrent seront emmenés captifs

6:1	Malheur à ceux qui vivent tranquilles dans Sion, à ceux qui mettent leur confiance dans la montagne de Samarie, surnommés : La première des nations ! Vers qui va la maison d'Israël !
6:2	Passez à Calné et regardez, allez de là à Hamath, la grande cité, puis descendez à Gath chez les Philistins. Ces villes sont-elles plus prospères que vos deux royaumes, ou leur pays n'est-il pas plus étendu que votre pays ?
6:3	Vous qui éloignez le jour du malheur et qui approchez le règne de la violence.
6:4	Vous qui vous couchez sur des lits d'ivoire et qui êtes étendus sur vos coussins, vous mangez les agneaux du troupeau et les veaux choisis dans les étables.
6:5	Vous fredonnez au son du luth, vous inventez des instruments de musique comme David.
6:6	Vous buvez le vin dans de grandes coupes, vous vous oignez avec les meilleures huiles, mais vous n'êtes pas affligés de la ruine de Yossef.
6:7	C'est pourquoi maintenant ils vont être exilés en tête des exilés, et l'orgie des voluptueux disparaîtra.
6:8	Adonaï YHWH l'a juré par lui-même, c'est une déclaration de YHWH, Elohîm Sabaoth : Je déteste l'orgueil de Yaacov, et je hais ses palais. C'est pourquoi je livrerai la ville et tout ce qui est en elle.
6:9	Et s'il arrive qu'il reste dix hommes dans une maison, ils mourront.
6:10	Et le parent qui emportera les cadavres hors de la maison pour les brûler dira à celui qui est au fond de la maison : Y a-t-il encore quelqu'un avec toi ? Et il répondra : C'est fini ! Puis on dira : Tais-toi ! Car nous ne pouvons faire mention du Nom de YHWH !
6:11	Car voici, YHWH donne ses ordres : il frappera la grande maison par des gouttes de rosée, et la petite maison par des fissures.
6:12	Les chevaux courent-ils sur les rochers ? Y laboure-t-on avec des bœufs, pour que vous ayez changé la droiture en poison, et le fruit de la justice en absinthe ?
6:13	Vous vous réjouissez de choses qui ne sont que néant, vous dites : N'est-ce pas par notre force que nous avons acquis de la puissance ?
6:14	Mais, j'élèverai contre vous, ô maison d'Israël, dit YHWH, Elohîm Sabaoth, une nation qui vous opprimera depuis l'entrée de Hamath jusqu'au torrent de la région aride.

## Chapitre 7

### Avertissement<!--Am. 8:1-3, 9:10.-->

7:1	Alors Adonaï YHWH me fit voir cette vision : voici, il formait un essaim de criquets au temps où la dernière récolte commençait à pousser. Et voici, c'était la dernière récolte après l'herbe fauchée du roi.
7:2	Et quand elles eurent achevé de dévorer l'herbe de la terre, je dis : Adonaï YHWH, pardonne, je t'en prie ! Comment Yaacov subsistera-t-il ? Car il est faible.
7:3	YHWH se repentit de cela. Cela n'arrivera pas, dit YHWH.
7:4	Alors Adonaï YHWH me fit voir cette vision : Voici, Adonaï YHWH appelait le feu pour combattre. Celui-ci dévora le grand Abîme, puis il dévora le territoire.
7:5	Et je dis : Adonaï YHWH ! Arrête, je t'en prie ! Comment Yaacov se relèvera-t-il ? Car il est petit.
7:6	Et YHWH se repentit de cela. Cela non plus n'arrivera pas, dit Adonaï YHWH.
7:7	Alors il me fit voir cette vision : Voici, Adonaï se tenait debout sur un mur fait au niveau, et il avait un niveau dans la main.
7:8	Et YHWH me dit : Que vois-tu, Amos ? Et je répondis : Un niveau. Et Adonaï me dit : Je mettrai le niveau au milieu de mon peuple d'Israël, je ne lui pardonnerai plus.
7:9	Les hauts lieux de Yitzhak seront ravagés, les sanctuaires d'Israël seront détruits et je me lèverai contre la maison de Yarobam avec l'épée.

### Amatsyah accuse Amos devant Yarobam (Jéroboam)

7:10	Alors Amatsyah, prêtre de Béth-El, envoya dire à Yarobam roi d'Israël : Amos conspire contre toi au milieu de la maison d'Israël. Le pays ne pourrait supporter toutes ses paroles.
7:11	Car ainsi a dit Amos : Yarobam mourra par l'épée et Israël ira en exil, il ira en exil loin de sa terre.
7:12	Amatsyah dit à Amos : Voyant<!--Voyant ou prophète.-->, va-t'en, fuis vers la terre de Yéhouda, manges-y ton pain, et là tu prophétiseras.
7:13	Mais ne continue pas à prophétiser à Béth-El<!--Béth-El, qui signifie « maison d'Elohîm », était devenue le sanctuaire du roi Yarobam.-->, car c'est le sanctuaire du roi, et c'est une maison royale.

### Amos répond

7:14	Amos répondit et dit à Amatsyah : Je n'étais ni prophète ni fils de prophète, j'étais un berger et je recueillais des figues de sycomores.
7:15	Et YHWH m'a pris derrière le troupeau, et YHWH m'a dit : Va, prophétise à mon peuple d'Israël.
7:16	Écoute donc maintenant la parole de YHWH : Tu me dis : Ne prophétise plus contre Israël et ne prêche plus contre la maison de Yitzhak.
7:17	C'est pourquoi ainsi parle YHWH : Ta femme se prostituera dans la ville, tes fils et tes filles tomberont par l'épée, ton champ sera partagé au cordeau, et toi, tu mourras sur une terre souillée, et Israël ira en exil, il ira en exil loin de sa terre.

## Chapitre 8

### Vision du panier de fruit, la fin pour le peuple d'Israël

8:1	Alors Adonaï YHWH me fit voir cette vision : Voici, je vis un panier de fruits d'été.
8:2	Il dit : Que vois-tu, Amos ? Et je répondis : Un panier de fruits. Et YHWH me dit : La fin est venue pour mon peuple d'Israël, je ne passerai plus par-dessus lui.
8:3	Les cantiques du temple seront des hurlements en ce jour-là, dit Adonaï YHWH. On jettera partout en silence une multitude de cadavres.
8:4	Écoutez ceci vous qui dévorez les indigents, même jusqu'à exterminer les pauvres du pays,
8:5	et qui dites : Quand la nouvelle lune sera-t-elle passée, pour que nous vendions le grain, et le shabbat, pour que nous exposions le grain en vente, en faisant l'épha plus petit, en augmentant le sicle, et falsifiant la balance pour tromper ?
8:6	Afin que nous acquérions les faibles pour de l'argent et le pauvre pour une paire de sandales, et que nous vendions la criblure du froment ?
8:7	YHWH l'a juré par la gloire de Yaacov : Jamais je n'oublierai toutes leurs actions !
8:8	La terre ne tremblera–t–elle pas à cause de cela ? Et tous ses habitants ne se lamenteront-ils pas ? Elle montera tout entière comme un fleuve, elle soulèvera ses eaux et s’affaissera comme le fleuve d'Égypte.
8:9	Il arrivera en ce jour-là, dit Adonaï YHWH, que je ferai coucher le soleil à midi et que j'obscurcirai la terre en plein jour.
8:10	Je changerai vos fêtes en deuil et tous vos chants en un chant funèbre. Je couvrirai de sacs tous les reins, et je rendrai chauves toutes les têtes. Je mettrai le pays dans le deuil comme pour un fils unique, et sa fin sera un jour d'amertume.
8:11	Voici, les jours viennent, dit Adonaï YHWH, où j'enverrai la famine sur la terre, non une famine de pain, ni une soif d'eau, mais d'entendre les paroles de YHWH.
8:12	Ils erreront d'une mer à l'autre, du nord à l'est, ils iront çà et là pour chercher la parole de YHWH, et ils ne la trouveront pas.
8:13	En ce jour-là, les belles vierges et les jeunes hommes s'évanouiront de soif.
8:14	Ceux qui jurent par le péché de Samarie disent : Ô Dan ! Ton elohîm est vivant, et vive la voie de Beer-Shéba ! Ils tomberont et ne se relèveront plus.

## Chapitre 9

### Prophétie annonçant la destruction<!--De. 28:63-68.-->

9:1	Je vis Adonaï se tenant debout sur l'autel, et il dit : Frappe le linteau de la porte, et que les poteaux soient ébranlés ! Et blesse-les tous à la tête ! Et je tuerai le dernier d'entre eux avec l'épée : aucun d'eux ne pourra se sauver en fuyant, pas un rescapé d'entre eux ne s'échappera.
9:2	S’ils creusent jusqu'au shéol, de là ma main les prendra, s’ils montent jusqu'aux cieux, je les en ferai descendre.
9:3	S’ils se cachent au sommet du Carmel, je les y chercherai et je les enlèverai de là. S’ils se dissimulent de devant mes yeux au fond de la mer, j’ordonnerai au serpent de les y mordre.
9:4	S’ils vont en captivité devant leurs ennemis, de là j'ordonnerai à l'épée, et elle les tuera. Je fixerai mes yeux sur eux pour leur faire du mal et non pour leur faire du bien.
9:5	Car c'est Adonaï YHWH Sabaoth qui touche la terre, et elle se fond, et tous ceux qui l'habitent mènent deuil, et elle s'écoule toute comme un fleuve, et elle est submergée comme par le fleuve d'Égypte.
9:6	C'est lui qui a bâti ses étages dans les cieux et qui a établi sa voûte sur la Terre ; qui appelle les eaux de la mer et qui les répand sur le dessus de la terre. Son Nom est YHWH.
9:7	N'êtes-vous pas pour moi comme les enfants des Éthiopiens, enfants d'Israël ? dit YHWH. N'ai-je pas fait monter Israël du pays d'Égypte, les Philistins de Caphtor et les Syriens de Kir ?
9:8	Voici, les yeux d'Adonaï YHWH sont sur ce royaume pécheur. Je le détruirai de dessus la surface de la terre. Enfin, je ne détruirai pas, je ne détruirai pas la maison de Yaacov, dit YHWH.
9:9	Car voici, je donnerai mes ordres et je secouerai la maison d'Israël parmi toutes les nations, comme on secoue le blé dans le crible, sans qu'il en tombe un grain à terre.
9:10	Tous les pécheurs de mon peuple mourront par l'épée, ceux qui disent : Le mal n'approchera pas, il ne nous atteindra pas.

### YHWH relève la maison de David

9:11	En ce temps-là, je relèverai le tabernacle de David qui est tombé, j'en réparerai les brèches, j'en redresserai les ruines, et je le rebâtirai comme aux jours d'autrefois<!--Voir Ac. 15:16.--> ;
9:12	afin qu'ils possèdent le reste d'Édom et toutes les nations sur lesquelles mon Nom a été invoqué, dit YHWH, qui accomplira cela.

### Restauration d'Israël

9:13	Voici, les jours viennent, dit YHWH, où le laboureur suivra de près le moissonneur, et celui qui foule les raisins atteindra celui qui répand la semence, et le moût ruissellera des montagnes et découlera de toutes les collines.
9:14	Et je ferai revenir les captifs de mon peuple Israël, et ils rebâtiront les villes dévastées et les habiteront. Ils planteront des vignes et en boiront le vin, ils feront aussi des jardins et en mangeront les fruits.
9:15	Je les planterai sur leur terre et ils ne seront plus arrachés à la terre que je leur ai donnée<!--Cette prophétie annonce la restauration de la maison de David. Personne ne chassera Israël de sa terre, aucune nation n'a le pouvoir de le déloger, car c'est le Seigneur qui l'a établi.-->, dit YHWH, ton Elohîm.
