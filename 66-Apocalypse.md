# Apokalupsis (Apocalypse ou révélation) (Ap.)

Signification : Mettre à nu, révélation d'une vérité, action de révéler

Auteur : Yohanan (Jean)

Thème : L'aboutissement de toutes choses

Date de rédaction : Env. 95 ap. J.-C.

Le terme apocalypse, du grec « apokalupsis », évoque « l'action de révéler ce qui était caché ou inconnu ». Ce mot a pour racine « apokalupto » qui signifie aussi « découvrir, dévoiler ce qui est voilé ou recouvert ».

C'est à Patmos, île grecque de la Mer Égée – où il s'exila en raison de la persécution de l'empereur Domitien (51 – 96 ap. J.-C.) – que Yohanan reçut une révélation de Yéhoshoua ha Mashiah (Jésus-Christ) ainsi qu'un message s'adressant aux « sept assemblées » qui constituaient certainement les villes de l'Asie Mineure où se trouvaient les principales concentrations de chrétiens. Si Éphèse figure dans les écrits de la Nouvelle Alliance et que Thyatire et Laodicée y sont brièvement mentionnées, les quatre autres assemblées – qu'on ne retrouve nulle part ailleurs dans les Écritures – étaient sans doute le fruit du travail missionnaire de Paulos (Paul). Les sept lettres s'adressent à l'ange de chacune de ces assemblées locales, autrement dit aux messagers de celles-ci (probablement un ancien ou un responsable).

Ce livre, qui arrive en conclusion des Écritures, annonce les événements qui doivent précéder la fin de l'histoire de l'humanité.

## Chapitre 1

### Introduction

1:1	Révélation<!--« Apokalupsis » en grec. Voir la présentation du livre de l'Apocalypse.--> de Yéhoshoua Mashiah, qu'Elohîm lui-même<!--Voir Ga. 1:1.--> a donnée pour montrer à ses esclaves les choses qui doivent arriver avec promptitude<!--Vient du grec « tachos » qui signifie « vitesse, rapidité, vivacité et promptitude ».--> et il l'a fait connaître par l'envoi de son ange à Yohanan son esclave.
1:2	Lequel a rendu témoignage de la parole d'Elohîm et du témoignage de Yéhoshoua Mashiah, tout ce qu'il a vu.
1:3	Béni est celui qui lit et ceux qui entendent les paroles de cette prophétie, et qui gardent les choses qui y sont écrites ! Car le temps est proche.

### Yéhoshoua ha Mashiah (Jésus-Christ)

1:4	Yohanan aux sept assemblées qui sont en Asie : que la grâce et la paix vous soient données de la part de celui QUI EST, QUI ÉTAIT et QUI VIENT<!--Les prophètes ont prophétisé la venue de YHWH en personne : Es. 35:4, 40:10-11, 60:1-2 ; Za. 14:1-21 ; Jn. 14:1-3. Yéhoshoua Mashiah (Jésus-Christ) est bien YHWH qui vient.-->, et de la part des sept Esprits qui sont devant son trône,
1:5	et de la part de Yéhoshoua Mashiah, le témoin fidèle, le premier-né d'entre les morts<!--Voir commentaire Col. 1:15.--> et le chef<!--Un prince, un chef, un meneur, un magistrat. Yéhoshoua est le chef des chefs. Il est également le chef du chef de ce monde. Voir Jn. 12:31.--> des rois de la Terre. À celui qui nous a aimés et qui nous a lavés de nos péchés dans son sang,
1:6	et qui a fait de nous des rois et des prêtres pour Elohîm, son Père, à lui soient la gloire et la force d'âge en âge. Amen !

### La venue du Mashiah

1:7	Voici, il vient avec les nuées<!--Es. 19:1 ; Ez. 30:3 ; Na. 1:3 ; Ps. 104:3.-->. Et tout œil le verra, même ceux qui l'ont percé<!--Voir Za. 12:10.-->, et toutes les tribus de la Terre se frapperont la poitrine de chagrin à cause de lui. Oui, amen !
1:8	Moi je suis l'Aleph<!--Dans Ge. 1:1, il y a l'Aleph et le Tav. Dans Za. 12:10, celui qu'on a percé est l'Aleph et le Tav.--> et le Tav<!--« Signe », « marque comme un signe d'exemption du jugement ».-->, le commencement et la fin, dit le Seigneur, QUI EST, QUI ÉTAIT, et QUI VIENT, le Tout-Puissant<!--Celui qui a domination sur toute chose, celui qui gouverne tout, le tout-puissant : Elohîm et Père. Voir 2 Co. 6:18. Il est YHWH Sabaoth que les séraphins appellent « Saint », « Saint », « Saint ». Voir Es. 6:1-8 et Ap. 4:8. Voir aussi Ap. 11:17, 15:3, 16:7,14, 19:6,15, 21:22.-->.

### La vision doit être écrite

1:9	Moi, Yohanan, votre frère, qui participe aussi avec vous à la tribulation, et au règne, et à la patience en Yéhoshoua Mashiah, j'étais sur l'île appelée Patmos à cause de la parole d'Elohîm et du témoignage de Yéhoshoua Mashiah.
1:10	J'apparus<!--Vient d'un mot grec qui signifie « devenir, venir dans l'existence, commencer à être, revenir au passé, arriver, provenir de, apparaître dans l'histoire ».--> au jour du Seigneur en esprit, et j'entendis derrière moi une grande voix comme le son d'une trompette,
1:11	disant : Moi je suis l'Aleph et le Tav, le premier et le dernier. Écris dans un livre ce que tu vois et envoie-le aux sept assemblées qui sont en Asie : à Éphèse, à Smyrne, à Pergame, à Thyatire, à Sardes, à Philadelphie et à Laodicée.
1:12	Et je me retournai pour voir la voix qui me parlait. Et, après m'être retourné, je vis sept chandeliers d'or,
1:13	et au milieu des sept chandeliers d'or, quelqu'un de semblable à un fils d'homme, vêtu d'une longue robe et ayant une ceinture d'or sur la poitrine.
1:14	Sa tête et ses cheveux, blancs comme de la laine blanche<!--Voir Da. 7:9.--> et comme de la neige, et ses yeux, comme une flamme de feu.
1:15	Et ses pieds, semblables à du cuivre ardent, comme embrasés dans une fournaise, et sa voix, comme le bruit des grandes eaux.
1:16	Et dans sa main droite il a sept étoiles et de sa bouche sort une épée aiguë à deux tranchants, et son visage est comme le soleil lorsqu'il brille dans sa force.
1:17	Et quand je le vis, je tombai à ses pieds comme mort, et il posa sa main droite sur moi en me disant : N'aie pas peur !
1:18	Je suis le Premier et le Dernier et je vis. J'étais mort et voici, je suis vivant d'âge en âge. Amen ! Et je détiens les clés de l'Hadès<!--Voir commentaire Mt. 16:18.--> et de la mort.
1:19	Écris les choses que tu as vues, celles qui sont présentement et celles qui doivent arriver ensuite.
1:20	Le mystère des sept étoiles que tu as vues dans ma main droite, et les sept chandeliers d'or : les sept étoiles sont les anges des sept assemblées, et les sept chandeliers que tu as vus sont les sept assemblées.

## Chapitre 2

### Éphèse : L'assemblée qui a abandonné son premier amour

2:1	Écris à l'ange<!--Ange, du grec « aggelos » : « envoyé, messager, un ange, un messager d'Elohîm ». Ce terme sert à désigner aussi bien les créatures spirituelles que les êtres humains.--> de l'assemblée d'Éphèse : Voici ce que dit celui qui tient les sept étoiles dans sa main droite, et qui marche au milieu des sept chandeliers d'or :
2:2	Je connais tes œuvres, et ton travail, et ta patience, et je sais que tu ne peux pas supporter les méchants, et que tu as éprouvé ceux qui se disent apôtres et qui ne le sont pas, et que tu les as trouvés menteurs ;
2:3	et que tu as souffert et que tu as eu de la patience, et que tu as travaillé dur pour mon Nom et que tu ne t'es pas lassé.
2:4	Mais j'ai quelque chose contre toi, c'est que tu as abandonné ton premier amour<!--Il est question ici de l'amour « agape » : « l'amour fraternel, l'amour divin ».-->.
2:5	C'est pourquoi souviens-toi donc d'où tu es tombé, repens-toi et fais tes premières œuvres. Autrement, je viens à toi à toute vitesse, et j'ôterai ton chandelier de sa place si tu ne te repens pas.
2:6	Cependant, tu as ceci, c'est que tu hais les œuvres des Nicolaïtes<!--Nicolaïtes : tiré du nom Nikolaos (Nicolas), qui signifie littéralement « victorieux du peuple ». Il s'agit d'une secte dont les membres furent peut-être des disciples d'un certain Nikolaos, l'un des diacres de l'assemblée d'Antioche qui aurait dévié (Ac. 6:5). Ces membres suivaient la doctrine de Balaam, enseignant aux chrétiens qu'à cause du principe de liberté, ils pouvaient manger des viandes sacrifiées aux idoles et commettre des actes immoraux comme les nations.-->, œuvres que je hais moi aussi.
2:7	Que celui qui a des oreilles entende ce que l'Esprit dit aux assemblées ! À celui qui remporte la victoire, je donnerai à manger de l'arbre de vie qui est au milieu du paradis d'Elohîm.

### Smyrne : L'assemblée sous la persécution

2:8	Écris aussi à l'ange de l'assemblée de Smyrne : Voici ce que dit celui qui est le premier et le dernier, qui était mort et qui est revenu à la vie :
2:9	Je connais tes œuvres, ta tribulation et ta pauvreté, quoique tu sois riche, et le blasphème de ceux qui se disent être Juifs et qui ne le sont pas, mais qui sont la synagogue de Satan.
2:10	N'aie aucune peur de ce que tu vas souffrir. Voici, le diable va jeter quelques-uns d'entre vous en prison, afin que vous soyez éprouvés, et vous aurez une tribulation de dix jours. Sois fidèle jusqu'à la mort et je te donnerai la couronne de vie.
2:11	Que celui qui a des oreilles entende ce que l'Esprit dit aux assemblées ! Celui qui remporte la victoire n'aura rien à souffrir de<!--L'article partitif « de » vient du grec « ek » qui signifie « hors de », « loin de ».--> la seconde mort.

### Pergame : L'assemblée établie dans le monde

2:12	Écris aussi à l'ange de l'assemblée de Pergame : Voici ce que dit celui qui a l'épée aiguë à deux tranchants<!--Hé. 4:12.--> :
2:13	Je connais tes œuvres et le lieu où tu habites : là se trouve le trône de Satan. Mais tu retiens mon Nom, et tu n'as pas renié ma foi, même aux jours d'Antipas, mon fidèle martyr<!--Du grec « martus » qui signifie « témoin ».-->, qui a été mis à mort chez vous, là où Satan habite.
2:14	Mais j'ai quelque chose contre toi, c'est que tu as là des gens qui retiennent la doctrine de Balaam, qui enseignait à Balak à tendre un piège<!--Vient du grec « skandalon » qui signifie aussi « scandale ».--> devant les enfants d'Israël, afin qu'ils mangent des viandes sacrifiées aux idoles et qu'ils se prostituent<!--No. 25:1-2, 31:16.-->.
2:15	De même, toi aussi, tu as des gens qui retiennent la doctrine des Nicolaïtes, ce que je hais !
2:16	Repens-toi donc, autrement je viens à toi à toute vitesse et je les combattrai avec l'épée de ma bouche.
2:17	Que celui qui a des oreilles entende ce que l'Esprit dit aux assemblées ! À celui qui remporte la victoire, je donnerai à manger de la manne cachée, je lui donnerai aussi un caillou blanc<!--Le terme grec « psephos » désigne « une petite pierre usée et lisse », « un vote, un suffrage (du fait de l'utilisation de cailloux pour voter) ». En effet, dans les anciennes cours de justice, l'accusé était condamné par des cailloux noirs ou acquitté par des cailloux blancs.-->, et sur ce caillou sera écrit un nouveau nom<!--Es. 62:2.-->, que personne ne connaît, sinon celui qui le reçoit.

### Thyatire : L'assemblée en période d'idolâtrie

2:18	Écris aussi à l'ange de l'assemblée de Thyatire : Voici ce que dit le Fils d'Elohîm, qui a les yeux comme une flamme de feu et dont les pieds sont semblables à du cuivre ardent.
2:19	Je connais tes œuvres, ton amour, ton service, ta foi, ta patience. Je sais bien que tes dernières œuvres sont plus nombreuses que les premières.
2:20	Mais j'ai quelque peu de chose contre toi, c'est que tu permets que la femme Jézabel<!--1 R. 16:31, 21:25 ; 2 R. 9:7, 9:22.-->, qui se dit elle-même prophétesse, enseigne et égare mes esclaves pour qu'ils se prostituent et mangent des choses sacrifiées aux idoles.
2:21	Et je lui ai donné du temps afin qu'elle se repente de sa relation sexuelle illicite, mais elle ne s'est pas repentie.
2:22	Voici, je la jette sur un lit pour une grande tribulation<!--La grande tribulation. Voir Mt. 24:21 ; Ap. 7:14.--> ainsi que ceux qui commettent l'adultère avec elle, à moins qu'ils ne se repentent de leurs œuvres.
2:23	Et je ferai mourir de mort ses enfants, et toutes les assemblées sauront que je suis celui qui sonde les reins et les cœurs<!--Jé. 17:10. Yéhoshoua (Jésus) est YHWH.-->, et je rendrai à chacun de vous selon ses œuvres.
2:24	Mais je vous dis à vous et aux autres qui sont à Thyatire, à tous ceux qui n'ont pas cette doctrine et qui n'ont pas connu les profondeurs de Satan, comme ils disent, je vous dis : Je ne mettrai pas sur vous d'autre charge.
2:25	Mais retenez ce que vous avez, jusqu'à ce que je vienne.
2:26	Et à celui qui remporte la victoire, à celui qui garde mes œuvres jusqu'à la fin, je donnerai autorité sur les nations.
2:27	Et il les gouvernera avec un sceptre de fer, et elles seront brisées comme les vases d'un potier, ainsi que j'en ai moi-même reçu le pouvoir de mon Père.
2:28	Et je lui donnerai l'étoile du matin.
2:29	Que celui qui a des oreilles entende ce que l'Esprit dit aux assemblées !

## Chapitre 3

### Sardes : L'assemblée morte

3:1	Écris aussi à l'ange de l'assemblée de Sardes : Voici ce que dit celui qui a les sept Esprits d'Elohîm et les sept étoiles : Je connais tes œuvres, puisque tu as la réputation d'être vivant, mais tu es mort.
3:2	Deviens vigilant et affermis le reste qui va mourir, car je n'ai pas trouvé tes œuvres parfaites devant Elohîm.
3:3	Souviens-toi donc des choses que tu as reçues et entendues, garde-les et repens-toi. Si tu ne veilles pas, je viendrai contre toi comme un voleur, et tu ne sauras pas à quelle heure je viendrai contre toi<!--Mt. 24:43 ; Lu. 12:39 ; 1 Th. 5:2 ; 2 Pi. 3:10.-->.
3:4	Toutefois, tu as quelque peu de personnes à Sardes qui n'ont pas souillé leurs vêtements, et qui marcheront avec moi en vêtements blancs, car elles en sont dignes.
3:5	Celui qui remporte la victoire sera revêtu de vêtements blancs, et je n'effacerai pas son nom du livre de vie, mais je confesserai son nom devant mon Père et devant ses anges.
3:6	Que celui qui a des oreilles entende ce que l'Esprit dit aux assemblées !

### Philadelphie : L'assemblée réveillée et fidèle

3:7	Écris aussi à l'ange de l'assemblée de Philadelphie : Voici ce que dit le Saint et le Véritable, celui qui a la clé de David<!--Es. 22:22.-->, celui qui ouvre et personne ne fermera, celui qui ferme et personne n'ouvrira.
3:8	Je connais tes œuvres. Voici, j'ai ouvert une porte devant toi et personne ne peut la fermer, parce que tu as peu de puissance, que tu as gardé ma parole et que tu n'as pas renié mon Nom.
3:9	Voici, je te donne des gens de la synagogue de Satan, qui se disent être Juifs et ne le sont pas, mais qui mentent. Voici, je ferai en sorte qu'ils viennent se prosterner à tes pieds et qu'ils sachent que, moi, je t'ai aimé.
3:10	Parce que tu as gardé la parole de ma persévérance, je te garderai moi-même hors de<!--L'article partitif « de » vient du grec « ek », qui signifie « hors de » ou encore « loin de ».--> l'heure de la tentation qui doit arriver dans le monde entier, pour éprouver les habitants de la Terre.
3:11	Voici, je viens à toute vitesse. Tiens ferme<!--Vient du grec « krateo », qui signifie « avoir le pouvoir », « être puissant », « être un chef », « être le maître ».--> ce que tu as, afin que personne ne prenne ta couronne.
3:12	Celui qui remporte la victoire, je ferai de lui une colonne dans le temple de mon Elohîm, et il n'en sortira plus. Et j'écrirai sur lui le Nom de mon Elohîm, et le nom de la cité de mon Elohîm, qui est la nouvelle Yeroushalaim qui descend du ciel d'auprès de mon Elohîm, et mon nouveau Nom.
3:13	Que celui qui a des oreilles entende ce que l'Esprit dit aux assemblées !

### Laodicée : L'assemblée apostate

3:14	Écris aussi à l'ange de l'assemblée de Laodicée : Voici ce que dit l'Amen, le témoin fidèle et véritable, le commencement de la création d'Elohîm :
3:15	Je connais tes œuvres. Je sais que tu n'es ni froid ni bouillant. Puisses-tu être ou froid ou bouillant !
3:16	Ainsi, parce que tu es tiède et que tu n'es ni froid ni bouillant, je te vomirai de ma bouche.
3:17	Parce que tu dis : Je suis riche, abondant en ressources matérielles et je n'ai besoin de rien, mais tu ne sais pas que tu es malheureux, misérable, pauvre, aveugle et nu.
3:18	Je te conseille d'acheter de moi de l'or éprouvé par le feu afin que tu deviennes riche, et des vêtements blancs afin que tu sois vêtu et que la honte de ta nudité ne paraisse pas, et un collyre pour oindre tes yeux afin que tu voies.
3:19	Moi, je reprends et je châtie<!--De. 8:5 ; 2 S. 7:14 ; Pr. 13:24 ; Hé. 12:7.--> tous ceux que j'aime. Aie donc du zèle et repens-toi.

### Le Mashiah se retrouve hors des assemblées apostates

3:20	Voici, je me tiens à la porte et je frappe. Si quelqu'un entend ma voix et m'ouvre la porte, j'entrerai chez lui, et je souperai avec lui, et lui avec moi.
3:21	Celui qui remporte la victoire, je le ferai asseoir avec moi sur mon trône, ainsi que j'ai remporté la victoire et me suis assis avec mon Père sur son trône.
3:22	Que celui qui a des oreilles entende ce que l'Esprit dit aux assemblées !

## Chapitre 4

### Vision avant l'ouverture des sceaux

4:1	Après ces choses, je regardai et voici une porte ouverte dans le ciel. Et la première voix que j'avais entendue, comme le son d'une trompette et qui parlait avec moi, me dit : Monte ici et je te montrerai les choses qui doivent arriver à l'avenir.
4:2	Immédiatement j'apparus en esprit. Et voici, un trône était dressé dans le ciel, et sur le trône quelqu'un d'assis.
4:3	Et celui qui est assis était d'une apparence semblable à une pierre de jaspe et de sardoine. Et autour du trône, un arc-en-ciel d'une apparence semblable à de l'émeraude.

### Les trônes des vingt-quatre anciens

4:4	Et autour du trône vingt-quatre trônes, et je vis sur ces trônes vingt-quatre anciens assis, vêtus de vêtements blancs et ayant sur leurs têtes des couronnes d'or.
4:5	Et du trône sortent des éclairs, des tonnerres et des voix. Et sept lampes de feu brûlent devant le trône, qui sont les sept Esprits d'Elohîm.

### Le Mashiah est digne de recevoir la louange et la gloire

4:6	Et devant le trône, une mer de verre semblable à du cristal. Et au milieu du trône et autour du trône, quatre êtres vivants, remplis d'yeux devant et derrière.
4:7	Et le premier être vivant ressemble à un lion, le second être vivant ressemble à un veau, le troisième être vivant a la face comme un être humain, et le quatrième être vivant ressemble à un aigle qui vole.
4:8	Et les quatre êtres vivants ont chacun six ailes et ils sont remplis d'yeux tout autour et à l'intérieur. Et ils ne cessent de dire jour et nuit : Saint ! Saint ! Saint est le Seigneur Elohîm Tout-Puissant, QUI ÉTAIT, QUI EST et QUI VIENT.
4:9	Et quand ces êtres vivants donneront gloire, et honneur, et action de grâce à celui qui est assis sur le trône, à celui qui est vivant d'âge en âge,
4:10	les vingt-quatre anciens tomberont devant celui qui est assis sur le trône, et adoreront celui qui est vivant d'âge en âge, et ils jetteront leurs couronnes devant le trône en disant :
4:11	Seigneur, tu es digne de recevoir la gloire, l'honneur et la puissance, car tu as créé toutes choses, et c'est par ta volonté qu'elles existent et qu'elles ont été créées.

## Chapitre 5

### Le Mashiah est le seul digne d'ouvrir le livre

5:1	Et je vis dans la main droite de celui qui est assis sur le trône, un livre écrit en dedans et en dehors, scellé de sept sceaux.
5:2	Et je vis aussi un ange puissant, proclamant à grande voix : Qui est digne d'ouvrir le livre et d’en délier les sceaux ?
5:3	Et nul ne pouvait, ni dans le ciel, ni sur la Terre, ni sous la terre, ouvrir le livre ni le regarder.
5:4	Et je pleurais beaucoup parce que personne n'était trouvé digne d'ouvrir le livre, ni de le lire, ni de le regarder.
5:5	Et l'un des anciens me dit : Ne pleure pas ! Voici, il a remporté la victoire, le Lion qui est de la tribu de Yéhouda, la racine<!--Voir Es. 11:10 ; Ro. 11:17-18, 15:12 ; Ap. 22:16.--> de David, pour ouvrir le livre et pour en délier les sept sceaux.
5:6	Et je regardai, et voici, au milieu du trône et des quatre êtres vivants, et au milieu des anciens, un Agneau qui se tenait debout comme tué, ayant sept cornes et sept yeux, qui sont les sept Esprits d'Elohîm envoyés par toute la Terre.
5:7	Et il vint et prit le livre de la main droite de celui qui est assis sur le trône.

### L'Agneau est adoré<!--Ph. 2:9-11.-->

5:8	Et quand il eut pris le livre, les quatre êtres vivants et les vingt-quatre anciens tombèrent devant l'Agneau, ayant chacun des harpes et des coupes d'or pleines d'encens, qui sont les prières des saints.
5:9	Et ils chantent un cantique nouveau, en disant : Tu es digne de prendre le livre et d'en ouvrir les sceaux, car tu as été tué et tu nous as achetés pour Elohîm par ton sang, de toute tribu, de toute langue, de tout peuple et de toute nation.
5:10	Et tu as fait de nous des rois et des prêtres pour notre Elohîm, et nous régnerons sur la Terre.
5:11	Et je regardai et j'entendis la voix de beaucoup d'anges autour du trône, des êtres vivants et des anciens, et leur nombre était des myriades de myriades et des milliers de milliers.
5:12	Ils disent à grande voix : Digne est l'Agneau qui a été tué, de recevoir la puissance, et la richesse, et la sagesse, et la force, et l'honneur, et la gloire et la louange !
5:13	J'entendis aussi toutes les créatures qui sont dans le ciel, sur la Terre et sous la Terre, et dans la mer et toutes les choses qui y sont, disant : À celui qui est assis sur le trône et à l'Agneau, soient la louange et l'honneur, et la gloire et la force, d'âge en âge !
5:14	Et les quatre êtres vivants disaient : Amen ! Et les vingt-quatre anciens tombèrent et adorèrent celui qui est vivant d'âge en âge.

## Chapitre 6

### Premier sceau : Le cavalier qui part pour vaincre

6:1	Et quand l'Agneau eut ouvert l'un des sceaux, je regardai et j'entendis l'un des quatre êtres vivants dire comme avec une voix de tonnerre : Viens et vois !
6:2	Je regardai et je vis un cheval blanc. Celui qui était monté dessus avait un arc. Et une couronne lui fut donnée, il est sorti remportant la victoire et pour remporter la victoire<!--Contrairement aux apparences, ce cavalier couronné d'un diadème et qui monte un cheval blanc n'est pas Yéhoshoua Mashiah (Jésus-Christ), mais l'Anti-Mashiah (Antichrist) qui singe le retour glorieux du Seigneur : Da. 7:21 ; Mt. 24:4-5 ; 2 Th. 2:9-12 ; Ap. 13:7. Le vrai Mashiah revenant triomphalement avec son Assemblée est décrit en Ap. 19:11-16.-->.

### Deuxième sceau : La guerre

6:3	Et quand il eut ouvert le second sceau, j'entendis le second être vivant dire : Viens et vois !
6:4	Et il sortit un autre cheval qui était roux. Il fut donné à celui qui était monté dessus de pouvoir ôter la paix de la Terre, afin que les hommes se tuent les uns les autres, et une grande épée lui fut donnée.

### Troisième sceau : La famine

6:5	Et quand il eut ouvert le troisième sceau, j'entendis le troisième être vivant dire : Viens et vois ! Je regardai et je vis un cheval noir, et celui qui était monté dessus avait une balance dans sa main.
6:6	Et j'entendis au milieu des quatre animaux une voix qui disait : Un chénice<!--Un chénice est une mesure de capacité qui valait 1,08 litres et dont on se servait pour les corps solides.--> de blé pour un denier, et les trois chénices d'orge pour un denier, mais ne fais pas de mal au vin et à l'huile.

### Quatrième sceau : La mort

6:7	Et quand il eut ouvert le quatrième sceau, j'entendis la voix du quatrième être vivant dire : Viens et vois !
6:8	Et je regardai, et je vis un cheval vert. Et celui qui était monté dessus avait pour nom la Mort, et l'Hadès<!--Voir commentaire Mt. 16:18.--> le suivait de près. Et il leur fut donné le pouvoir sur la quatrième partie de la Terre pour tuer par l'épée, par la famine, par la mort et par les bêtes sauvages de la Terre.

### Cinquième sceau : Les martyrs

6:9	Et quand il eut ouvert le cinquième sceau, je vis sous l'autel les âmes de ceux qui avaient été tués à cause de la parole d'Elohîm et à cause du témoignage qu'ils avaient.
6:10	Et elles criaient à grande voix, en disant : Jusqu'à quand, Maître<!--Yéhoshoua (Jésus) est le seul Maître. Voir Jud. 1:4.--> qui es saint et véritable, ne jugeras-tu pas et ne vengeras-tu pas notre sang sur ceux qui habitent sur la Terre ?
6:11	Et il leur fut donné à chacun une robe blanche, et il leur fut dit de se tenir en repos encore un peu de temps, jusqu'à ce que le nombre de leurs compagnons de service, et de leurs frères qui doivent être mis à mort comme eux, soit au complet.

### Sixième sceau : L'anarchie

6:12	Et je regardai quand il eut ouvert le sixième sceau, et voici, il se fit un grand tremblement de terre, et le soleil devint noir comme un sac de crin, et la lune entière devint comme du sang.
6:13	Et les étoiles du ciel tombèrent sur la Terre<!--Mt. 24:29 ; Mc. 13:25.-->, comme lorsque le figuier est agité par un grand vent et laisse tomber ses figues encore vertes.
6:14	Et le ciel se retira comme un livre qu'on roule<!--Es. 34:4.-->. Et toutes les montagnes et les îles furent remuées de leurs places.
6:15	Et les rois de la Terre, et les princes, et les riches, et les capitaines, et les puissants, et tout esclave, et tout homme libre se cachèrent dans les cavernes et dans les rochers des montagnes<!--Es. 2:19-21.-->.
6:16	Et ils disent aux montagnes et aux rochers : Tombez sur nous<!--Lu. 23:30.--> et cachez-nous devant la face de celui qui est assis sur le trône et devant la colère de l'Agneau.
6:17	Car le grand jour de sa colère est venu, et qui peut tenir debout ?

## Chapitre 7

### Les 144 000 marqués du sceau d'Elohîm

7:1	Et après cela, je vis quatre anges qui se tenaient aux quatre coins de la Terre, retenant les quatre vents de la terre, afin qu'aucun vent ne souffle sur la terre, ni sur la mer, ni sur aucun arbre.
7:2	Et je vis un autre ange qui montait de l'orient, d'où vient le soleil, ayant le sceau d'Elohîm vivant, et il cria d'une grande voix aux quatre anges à qui il avait été donné de faire du mal à la Terre et à la mer,
7:3	en disant : Ne faites pas de mal à la Terre, ni à la mer, ni aux arbres, jusqu'à ce que nous ayons marqué du sceau les esclaves de notre Elohîm sur leurs fronts.
7:4	Et j'entendis que le nombre de ceux qui avaient été marqués du sceau était de 144 000, de toutes les tribus des enfants d'Israël :
7:5	de la tribu de Yéhouda, 12 000 marqués du sceau. De la tribu de Reouben, 12 000 marqués du sceau. De la tribu de Gad, 12 000 marqués du sceau.
7:6	De la tribu d'Asher, 12 000 marqués du sceau. De la tribu de Nephthali, 12 000 marqués du sceau. De la tribu de Menashè, 12 000 marqués du sceau.
7:7	De la tribu de Shim’ôn, 12 000 marqués du sceau. De la tribu de Lévi, 12 000 marqués du sceau. De la tribu de Yissakar, 12 000 marqués du sceau.
7:8	De la tribu de Zebouloun, 12 000 marqués du sceau. De la tribu de Yossef, 12 000 marqués du sceau. De la tribu de Benyamin, 12 000 marqués du sceau.

### Multitude de sauvés pendant la grande tribulation

7:9	Après ces choses, je regardai, et voici une grande foule que personne ne pouvait compter, de toute nation et tribus, et peuples et langues, qui se tenaient devant le trône et devant l'Agneau, vêtus de longues robes blanches, et des palmes dans leurs mains.
7:10	Et ils crient à grande voix, en disant : Le salut est à notre Elohîm, qui est assis sur le trône, et à l'Agneau !
7:11	Et tous les anges se tenaient autour du trône, et des anciens, et des quatre êtres vivants, et ils tombèrent devant le trône sur leurs faces et adorèrent Elohîm,
7:12	en disant : Amen ! La louange, la gloire, la sagesse, l'action de grâces, l'honneur, la puissance et la force soient à notre Elohîm, d'âge en âge. Amen !
7:13	Et l'un des anciens prit la parole et me dit : Ceux qui sont revêtus de longues robes blanches, qui sont-ils et d'où sont-ils venus ?
7:14	Et je lui dis : Seigneur, tu le sais. Et il me dit : Ce sont ceux qui viennent de la grande tribulation<!--Les saints ont toujours été persécutés. Cela a débuté dès la Genèse avec Caïn qui tua son frère Abel (Ge. 4:5-10). La grande tribulation correspond néanmoins à une période de persécutions particulièrement cruelles qui seront orchestrées par l'homme impie (à la tête de plusieurs nations), principalement contre les Juifs (Jé. 30:7 ; Da. 9:24 ; Lu. 21:20-24) et sans doute contre les personnes issues des nations converties au Mashiah (Christ) (Ap. 7:9-17, 12:17). Le Seigneur Yéhoshoua (Jésus) a prédit la grande tribulation à ses disciples (Mt. 24:15-29 ; Mc. 13:14-19) en précisant qu'en ce temps là on verrait « l'abomination qui cause la désolation » établie en lieu saint prophétisée par Daniye'l (Da. 11:31). La grande tribulation durera trois ans et demi, c'est ce que Daniye'l appelle « un temps, des temps et la moitié d'un temps » (Da. 7:25 ; Ap. 11:3.) L'ère de paix factice instaurée par l'impie cédera alors soudainement la place à un temps d'angoisse sans précédent (1 Th. 5:3).-->. Et ils ont lavé leurs robes, et ils ont blanchi leurs robes dans le sang de l'Agneau.
7:15	C'est pourquoi ils sont devant le trône d'Elohîm, et ils lui rendent leur culte jour et nuit dans son temple. Et celui qui est assis sur le trône dressera sa tente sur eux.
7:16	Ils n'auront plus faim ni soif, et le soleil ne tombera plus sur eux ni aucune chaleur.
7:17	Parce que l'Agneau qui est au milieu du trône<!--Voir Ap. 5:6.--> les paîtra et les guidera<!--Jn. 16:13.--> vers les sources d’eaux vivantes<!--Ge. 49:22 ; Jé. 2:13 ; Jn. 4:14-15, 7:37-39 ; Ap. 21:6, 22:16-17.-->, et Elohîm essuiera toutes les larmes de leurs yeux.

## Chapitre 8

### Septième sceau : Annonce des sept trompettes<!--Ap. 4:1.-->

8:1	Et quand il eut ouvert le septième sceau, il y eut un silence dans le ciel d'environ une demi-heure.
8:2	Et je vis les sept anges qui se tiennent devant Elohîm, et sept trompettes leur furent données.
8:3	Et un autre ange vint et se tint devant l'autel, ayant un encensoir d'or, et beaucoup d'encens lui furent donnés pour les offrir, avec les prières de tous les saints, sur l'autel d'or qui est devant le trône.
8:4	Et la fumée de l'encens monta avec les prières des saints de la main de l'ange devant Elohîm.
8:5	Et l'ange prit l'encensoir, et l'ayant rempli du feu de l'autel, il le jeta sur la Terre. Et il y eut des tonnerres, des voix, des éclairs et un tremblement de terre.
8:6	Et les sept anges qui ont les sept trompettes se préparèrent à sonner de la trompette.

### Première trompette : Grêle et feu mêlés de sang

8:7	Et le premier ange sonna de la trompette. Et il y eut de la grêle et du feu mêlés de sang, qui furent jetés sur la Terre. Et le tiers des arbres fut brûlé, et toute herbe verte aussi fut brûlée.

### Deuxième trompette : La montagne embrasée

8:8	Et le second ange sonna de la trompette. Et quelque chose comme une grande montagne brûlante de feu fut jetée dans la mer. Et le tiers de la mer devint du sang,
8:9	et le tiers des créatures dans la mer ayant des âmes mourut, et le tiers des navires périt.

### Troisième trompette : Absinthe, l'étoile tombée du ciel

8:10	Et le troisième ange sonna de la trompette. Et il tomba du ciel une grande étoile, brûlant comme une lampe. Et elle tomba sur le tiers des fleuves et sur les sources des eaux.
8:11	Et le nom de l'étoile est Absinthe. Et le tiers des eaux fut changé en absinthe, et beaucoup d'humains moururent par les eaux, parce qu'elles étaient devenues amères.

### Quatrième trompette : Des signes dans le ciel

8:12	Et le quatrième ange sonna de la trompette, et le tiers du soleil fut frappé, ainsi que le tiers de la lune, et le tiers des étoiles, afin que le tiers en soit obscurci : le jour fut privé d'un tiers de sa clarté et la nuit de même.
8:13	Et je regardai et j'entendis un ange volant au zénith<!--Ap. 19:17.--> en disant à grande voix : Malheur ! Malheur ! Malheur aux habitants de la Terre à cause des autres sons de trompettes que les trois autres anges vont faire retentir.

## Chapitre 9

### Cinquième trompette : Ouverture du puits de l'abîme

9:1	Et le cinquième ange sonna de la trompette, et je vis une étoile qui tomba du ciel sur la Terre, et la clé du puits de l'abîme lui fut donnée.
9:2	Et elle ouvrit le puits de l'abîme, et une fumée monta du puits comme la fumée d'une grande fournaise. Et le soleil et l'air furent obscurcis par la fumée du puits.
9:3	Et des sauterelles sortirent de la fumée du puits et se répandirent sur la Terre, et il leur fut donné un pouvoir comme le pouvoir qu'ont les scorpions de la Terre.
9:4	Et il leur fut dit de ne pas faire de mal à l'herbe de la Terre, ni à aucune verdure, ni à aucun arbre, mais seulement aux humains qui n'avaient pas la marque d'Elohîm sur leurs fronts.
9:5	Et il leur fut donné, non de les tuer, mais de les tourmenter pendant cinq mois. Et le tourment qu'elles causent est comme le tourment que cause le scorpion quand il pique un être humain.
9:6	Et en ces jours-là, les humains chercheront la mort, mais ils ne la trouveront pas. Et ils désireront mourir, mais la mort fuira loin d'eux.
9:7	Et la ressemblance des sauterelles était semblable à des chevaux préparés pour la guerre, et sur leurs têtes, il y avait comme des couronnes semblables à de l'or, et leurs faces étaient comme des faces d'humains.
9:8	Et elles avaient les cheveux comme des cheveux de femmes et leurs dents étaient comme des dents de lions.
9:9	Et elles avaient des cuirasses comme des cuirasses de fer et le bruit de leurs ailes était comme le bruit des chars à beaucoup de chevaux qui courent à la guerre.
9:10	Et elles avaient des queues armées d'aiguillons, comme les scorpions, et c'est dans leurs queues qu'était le pouvoir de faire du mal aux humains pendant cinq mois.
9:11	Et elles ont sur elles comme roi, l'ange de l'abîme, dont le nom en hébreu est Abaddon, mais en grec son nom est Apollyon<!--Abaddon ou Apollyon : le nom de ce démon signifie « le destructeur ».-->.
9:12	Le premier malheur est passé. Et voici que deux malheurs viennent encore après cela.

### Sixième trompette : Les quatre anges de l'Euphrate déliés<!--Ap. 16:12.-->

9:13	Et le sixième ange sonna de sa trompette, et j'entendis une voix sortant des quatre cornes de l'autel d'or qui est devant Elohîm,
9:14	et disant au sixième ange qui avait la trompette : Délie les quatre anges qui sont liés sur le grand fleuve, l'Euphrate.
9:15	On délia donc les quatre anges qui étaient prêts pour l'heure, le jour, le mois et l'année, afin de tuer le tiers des humains.
9:16	Et le nombre des cavaliers de l'armée était de deux myriades de myriades, car j'en entendis le nombre.
9:17	Et je vis aussi dans la vision les chevaux et ceux qui étaient montés dessus, ayant des cuirasses de feu, d'hyacinthe et de soufre. Et les têtes des chevaux étaient comme des têtes de lions, et de leurs bouches sortent du feu, de la fumée et du soufre.
9:18	Le tiers des humains fut tué par ces trois fléaux : par le feu, par la fumée et par le soufre qui sortaient de leurs bouches.
9:19	Car le pouvoir des chevaux est dans leurs bouches et dans leurs queues. Car leurs queues sont semblables à des serpents ayant des têtes et c'est avec elles qu'ils font du mal.
9:20	Et les autres humains qui ne furent pas tués par ces fléaux, ne se repentirent pas des œuvres de leurs mains, ils ne cessèrent pas d'adorer les démons, les idoles d'or, d'argent, de cuivre, de pierre et de bois, qui ne peuvent ni voir, ni entendre, ni marcher.
9:21	Et ils ne se repentirent pas de leurs meurtres, ni de leurs sorcelleries, ni de leur relation sexuelle illicite, ni de leurs vols.

## Chapitre 10

### Un ange puissant descend du ciel

10:1	Et je vis un autre ange puissant descendant du ciel, revêtu d'une nuée et un arc-en-ciel sur la tête. Sa face, comme le soleil, et ses pieds, comme des colonnes de feu.
10:2	Et il avait dans sa main un petit livre ouvert. Et il posa son pied droit sur la mer, et le pied gauche sur la terre.
10:3	Et il cria d'une grande voix comme rugit un lion. Et quand il cria, les sept tonnerres firent entendre leurs voix.
10:4	Et quand les sept tonnerres eurent fait entendre leurs voix, j'allais écrire, et j'entendis une voix du ciel me disant : Scelle les choses que les sept tonnerres ont fait entendre, et ne les écris pas.
10:5	Et l'ange que je voyais debout sur la mer et sur la terre, leva sa main vers le ciel,
10:6	et jura par celui qui est vivant d'âge en âge, qui a créé le ciel avec les choses qui y sont, et la Terre avec les choses qui y sont, et la mer avec les choses qui y sont, qu'il n'y aurait plus de temps,
10:7	mais qu'aux jours de la voix du septième ange, quand il sera sur le point de sonner de la trompette, alors le mystère d'Elohîm s'accomplira, comme il en avait annoncé la bonne nouvelle à ses esclaves les prophètes<!--Lu. 20:37.-->.

### Nouvelle mission de Yohanan

10:8	Et la voix que j'avais entendue du ciel me parla encore et me dit : Va et prends le petit livre ouvert qui est dans la main de l'ange qui se tient sur la mer et sur la terre.
10:9	Et j'allai vers l'ange, en lui disant : Donne-moi le petit livre. Et il me dit : Prends-le et dévore-le. Il remplira ton ventre d'amertume, mais il sera doux dans ta bouche comme du miel<!--Ez. 3:1-3.-->.
10:10	Et je pris le petit livre de la main de l'ange et je le dévorai. Et il fut dans ma bouche doux comme du miel et, quand je l'eus mangé, mon ventre fut rempli d'amertume.
10:11	Et il me dit : Il faut que tu prophétises de nouveau sur beaucoup de peuples, et de nations, et de langues et de rois.

## Chapitre 11

### Le temps des nations

11:1	Et l'on me donna un roseau semblable à une verge, et l'ange se tint debout, disant : Lève-toi et mesure le temple d'Elohîm et l'autel, et ceux qui y adorent.
11:2	Mais le parvis extérieur du temple, laisse-le en dehors et ne le mesure pas, parce qu'il a été donné aux nations, et elles fouleront aux pieds la ville sainte pendant 42 mois<!--C'est le temps que durera la grande tribulation, soit 3 ans et demi. Daniye'l parle d'une semaine, un jour comptant pour une année (Da. 9:27). La grande tribulation débutera à la moitié de cette semaine, ce qui correspond bien à 42 mois (Ap. 13:5) et à 1 260 jours (Ap. 11:3, 12:6). Voir Lu. 21:24.-->.

### Les deux témoins ressuscitent

11:3	Et je donnerai à mes deux témoins de prophétiser pendant 1 260 jours, revêtus de sacs.
11:4	Ce sont les deux oliviers<!--Za. 4:14.--> et les deux chandeliers qui se tiennent devant l'Elohîm de la Terre.
11:5	Et si quelqu'un veut leur faire du mal, du feu sort de leurs bouches et dévore leurs ennemis. Et si quelqu'un veut leur faire du mal, il faut qu'il soit tué de cette manière.
11:6	Ils ont le pouvoir de fermer le ciel afin qu'il ne pleuve pas pendant les jours de leur prophétie. Ils ont aussi le pouvoir de changer les eaux en sang et de frapper la Terre de toutes sortes de plaies, toutes les fois qu'ils le voudront.
11:7	Et quand ils auront achevé leur témoignage, la bête qui monte de l'abîme<!--L'homme impie, l'Anti-Mashiah (Antichrist) ou encore le fils de la perdition dont il est question dans 2 Th. 2:3,8-9.--> leur fera la guerre, remportera la victoire sur eux et les tuera.
11:8	Et leurs cadavres seront sur la place de la grande ville qui est appelée spirituellement Sodome et Égypte, là même où notre Seigneur a été crucifié.
11:9	Et ceux des tribus, des peuples, des langues et des nations verront leurs cadavres pendant trois jours et demi, et ils ne permettront pas que leurs cadavres soient mis dans des sépulcres.
11:10	Et les habitants de la Terre se réjouiront, ils seront dans l'allégresse, ils s'enverront des présents les uns aux autres, parce que ces deux prophètes ont tourmenté les habitants de la Terre.
11:11	Mais après ces trois jours et demi, l'Esprit de vie venant d'Elohîm entra en eux, et ils se tinrent sur leurs pieds, et une grande crainte saisit ceux qui les virent.
11:12	Et ils entendirent une grande voix du ciel qui leur disait : Montez ici ! Et ils montèrent au ciel sur une nuée, et leurs ennemis les virent.
11:13	Et à cette même heure, il y eut un grand tremblement de terre, et la dixième partie de la ville tomba. Et 7 000 noms d'humains furent tués par ce tremblement de terre. Et les autres furent terrifiés et donnèrent gloire à Elohîm du ciel.
11:14	Le second malheur est passé. Voici, le troisième malheur vient à toute vitesse.

### Septième trompette : Le règne du Mashiah annoncé, cantique des vingt-quatre anciens<!--Ap. 8:2.-->

11:15	Et le septième ange sonna de la trompette et il y eut dans le ciel de grandes voix, disant : Les royaumes du monde sont revenus<!--« Devenir, revenir au passé, arriver ».--> à notre Seigneur et à son Mashiah, et il régnera d'âge en âge.
11:16	Et les vingt-quatre anciens qui sont assis devant Elohîm sur leurs trônes, tombèrent sur leurs faces et adorèrent Elohîm,
11:17	en disant : Nous te rendons grâces, Seigneur Elohîm le Tout-Puissant, QUI ES, QUI ÉTAIS et QUI VIENS, de ce que tu as pris en main ta grande puissance et que tu as exercé ton règne.
11:18	Et les nations se sont irritées, mais ta colère est venue. C'est aussi le temps de juger les morts, et de donner la récompense à tes esclaves les prophètes et aux saints, et à ceux qui craignent ton Nom, petits et grands, et de détruire ceux qui détruisent la Terre.
11:19	Et le temple d'Elohîm fut ouvert dans le ciel, et l'arche de son alliance apparut dans son temple. Et il y eut des éclairs, des voix, des tonnerres, un tremblement de terre et une grosse grêle.

## Chapitre 12

### Vision de la femme et du dragon

12:1	Et un grand signe parut dans le ciel : une femme revêtue du soleil, la lune sous ses pieds, et sur sa tête une couronne de douze étoiles<!--En Ge. 37:9-10, Yossef raconte à ses parents et à ses frères un rêve particulier où il voyait le soleil, la lune et onze étoiles se prosterner devant lui. Yaacov comprit que les onze étoiles représentaient ses enfants, la lune sa femme Rachel, qui était la mère de Yossef, et que le soleil c'était lui-même. Il est donc question ici d'Israël, qui a toujours été identifié à une femme (Ez. 16) de qui est issu le Mashiah selon la chair (Ro. 9:5).-->.
12:2	Elle est enceinte et crie, étant en travail et souffrant les douleurs de l'enfantement.
12:3	Il parut aussi un autre signe dans le ciel, et voici un grand dragon rouge feu ayant sept têtes et dix cornes, et sur ses têtes sept diadèmes.
12:4	Et sa queue traîne le tiers des étoiles du ciel et les jeta sur la Terre<!--Da. 8:10.-->. Et le dragon s'arrêta devant la femme qui devait accoucher, afin de dévorer son enfant<!--Cet enfant est évidemment Yéhoshoua Mashiah (Jésus-Christ) (Mt. 2:16).-->, dès qu'elle l'aurait mis au monde.

### La naissance du Mashiah

12:5	Et elle enfanta un fils, un mâle, qui doit paître<!--Voir Mt. 2:6.--> toutes les nations avec un sceptre de fer<!--Ps. 2:8-9.-->. Et son enfant fut enlevé vers Elohîm et vers son trône<!--Lu. 24:51 ; Ac. 1:9-11.-->.
12:6	Et la femme s'enfuit dans un désert, où elle avait un lieu préparé par Elohîm, afin d'y être nourrie pendant 1 260 jours.

### Guerre entre l'archange Miyka'el (Michel) et le dragon

12:7	Et il y eut une guerre dans le ciel. Miyka'el et ses anges combattirent contre le dragon. Et le dragon et ses anges combattirent,
12:8	mais ils ne furent pas les plus forts, et leur place ne fut plus trouvée dans le ciel.
12:9	Et il fut précipité le grand dragon, le serpent ancien, appelé le diable et Satan, celui qui égare toute la terre, il fut précipité sur la terre, et ses anges furent précipités avec lui.
12:10	Et j'entendis une grande voix dans le ciel, disant : Maintenant le salut est arrivé, ainsi que la force, le règne de notre Elohîm et la puissance de son Mashiah. Car l'accusateur de nos frères, qui les accuse devant notre Elohîm jour et nuit, a été précipité.
12:11	Et ils ont remporté la victoire sur lui à cause du sang de l'Agneau et à cause de la parole de leur témoignage, et ils n'ont pas aimé leurs vies jusqu'à la mort.
12:12	C'est pourquoi réjouissez-vous cieux et vous qui y dressez vos tentes. Mais malheur à vous habitants de la Terre et de la mer ! Car le diable est descendu vers vous animé d'une grande colère, sachant qu'il a peu de temps.

### Le dragon persécute la femme, sa postérité et les témoins du Mashiah

12:13	Et quand le dragon vit qu'il avait été précipité sur la Terre, il persécuta la femme qui avait enfanté le mâle.
12:14	Mais les deux ailes d'un grand aigle furent données à la femme, afin qu'elle s'envole de devant le serpent dans le désert, où elle est nourrie un temps, des temps, et la moitié d'un temps.
12:15	Et de sa gueule, le serpent lança de l'eau comme un fleuve derrière la femme, afin de la faire engloutir par le fleuve.
12:16	Mais la Terre secourut la femme, elle ouvrit sa bouche et elle engloutit le fleuve que le dragon avait lancé de sa gueule.
12:17	Et le dragon fut irrité contre la femme et s'en alla faire la guerre au reste de sa semence, à ceux qui gardent les commandements d'Elohîm et qui ont le témoignage de Yéhoshoua Mashiah.

## Chapitre 13

### La bête qui monte de la mer, l'Anti-Mashiah (Antichrist)

13:1	Et je me tins sur le sable de la mer. Et je vis, montant de la mer une bête<!--Cette bête représente deux entités. Tout d'abord l'homme impie, l'Anti-Mashiah (Antichrist), et ensuite un système politique. Les dix cornes sur sa tête symbolisent les dix nations les plus puissantes de la terre avec lesquelles il imposera sa dictature mondiale (Da. 7:16-25). L'alliage des quatre métaux dans la statue de Neboukadnetsar en Da. 2 et la vision des quatre bêtes en Da. 7, annoncent l'instauration d'un quatrième empire ou encore le système politique à la tête duquel sera la bête.--> qui avait sept têtes et dix cornes, et sur ses cornes dix diadèmes, et sur ses têtes des noms de blasphème<!--Voir annexe « La bête d'apocalypse ».-->.
13:2	Et la bête que je vis était semblable à un léopard, ses pieds étaient comme ceux d'un ours, sa gueule était comme la gueule d'un lion<!--Da. 7:7.-->. Et le dragon lui donna sa puissance, son trône et une grande autorité.
13:3	Et je vis l'une de ses têtes comme blessée à mort, mais sa blessure mortelle fut guérie. Et toute la Terre était dans l'admiration derrière la bête.
13:4	Et ils adorèrent le dragon, parce qu'il avait donné l'autorité à la bête, et ils adorèrent aussi la bête, en disant : Qui est semblable à la bête et qui peut combattre contre elle ?
13:5	Et il lui fut donné une bouche<!--Da. 7:7-8,19-21.--> qui proférait des discours pleins d'orgueil et des blasphèmes, et il lui fut aussi donné le pouvoir d'agir pendant 42 mois.
13:6	Et elle ouvrit sa bouche pour blasphémer contre Elohîm, pour blasphémer son Nom et son tabernacle, et ceux qui dressent leurs tentes dans le ciel.
13:7	Et il lui fut donné de faire la guerre aux saints et de remporter la victoire sur eux. Il lui fut aussi donné autorité sur toute tribu, toute langue et toute nation.
13:8	Et tous les habitants de la Terre l'adoreront, ceux dont les noms n'ont pas été écrits dans le livre de vie de l'Agneau tué dès la fondation du monde<!--Voir Ap. 17:8.-->.
13:9	Si quelqu'un a des oreilles qu'il entende.
13:10	Si quelqu'un emmène en captivité, il s'en va en captivité. Si quelqu'un tue par l'épée, il faut qu'il soit lui-même tué par l'épée. C'est ici la persévérance et la foi des saints.

### La bête qui monte de la Terre, le faux prophète

13:11	Et je vis une autre bête<!--Cette bête est identifiée au faux prophète, car son rôle consiste à amener les habitants de la terre à adorer la première bête, tout comme les vrais prophètes invitent les gens à l'adoration de l'Elohîm véritable (Mt. 7:15).--> montant de la Terre. Et elle avait deux cornes semblables à celles de l'Agneau, mais elle parlait comme le dragon.
13:12	Et elle exerce toute l'autorité de la première bête en sa présence, et elle fait que la Terre et ceux qui l'habitent adorent la première bête, dont la blessure mortelle a été guérie<!--Cette bête a existé par le passé sous la forme de l'Empire romain qui s'est écroulé le 4 septembre 476. Ce régime a marqué l'histoire par son caractère universel et brutal. Le fait que cette bête blessée à mort reprenne vie, annonce l'instauration d'un empire universel qui aura les caractéristiques combinées de l'Empire babylonien, médo-perse, gréco-macédonien et romain, ceux-ci correspondant aux quatre bêtes de la vision de Da. 7:1-8 : Le lion, l'ours, le léopard et le quatrième animal.-->.
13:13	Et elle produit de grands signes, même jusqu'à faire descendre le feu du ciel sur la Terre devant les humains.
13:14	Et elle égare les habitants de la Terre par les signes qu'il lui a été donné de produire devant la bête, en disant aux habitants de la terre de faire une image de la bête qui a été blessée par l'épée et qui a repris vie.
13:15	Et il lui fut donné de donner un esprit à l'image de la bête, afin que l'image de la bête parle, et qu'elle fasse que tous ceux qui n'adoreraient pas l'image de la bête soient mis à mort.
13:16	Et elle fait qu'à tous, petits et grands, riches et pauvres, libres et esclaves, il soit donné une marque sur leur main droite ou sur leur front<!--Il s'agit d'une marque qui est avant tout spirituelle. Car de la même façon que nous sommes scellés et marqués par l'Esprit d'Elohîm qui produit en nous la sainteté (Ga. 5:22 ; Ro. 6:20-22 ; Ep. 1:13, 4:30), Satan marque les siens par le péché (1 Ti. 4:1-2 ; 2 Ti. 3:1-5).-->.
13:17	Et que personne ne puisse acheter ni vendre, sans avoir la marque ou le nom de la bête, ou le nombre de son nom.
13:18	Ici est la sagesse : Que celui qui a de l'intelligence compte le nombre de la bête, car c'est un nombre humain. Et son nombre est 666.

## Chapitre 14

### L'Agneau et les 144 000

14:1	Et je regardai, et voici, l'Agneau se tenait debout sur la montagne de Sion, et avec lui 144 000 ayant le Nom de son Père écrit sur leurs fronts.
14:2	Et j'entendis une voix du ciel comme le bruit des grandes eaux, et comme le bruit d'un grand tonnerre. Et j'entendis une voix de joueurs de harpe jouant de leurs harpes.
14:3	Et ils chantent comme un cantique nouveau devant le trône, et devant les quatre êtres vivants et devant les anciens. Et personne ne pouvait apprendre le cantique, si ce n'est les 144 000 qui avaient été achetés de la Terre.
14:4	Ce sont ceux qui ne se sont pas souillés avec les femmes, car ils sont vierges. Ce sont ceux qui suivent l'Agneau partout où il va. Ils ont été achetés d'entre les humains pour être l'offrande du premier fruit<!--Prémices.--> pour Elohîm et pour l'Agneau.
14:5	Et dans leur bouche il ne s'est pas trouvé de fraude, car ils sont sans défaut devant le trône d'Elohîm<!--Ps. 32:2.-->.

### L'Évangile éternel et la chute de Babel

14:6	Et je vis un autre ange volant au zénith<!--Le milieu du ciel, le point le plus haut des cieux, que le soleil atteint à midi, le point d'où tout ce qui est fait peut être vu et entendu de tous. Ap. 19:17.-->, ayant l'Évangile éternel à prêcher à ceux qui demeurent sur la Terre, à toute nation et tribu, et langue et peuple,
14:7	disant à grande voix : Craignez Elohîm et donnez-lui gloire, car l'heure de son jugement est venue. Et adorez celui qui a fait le ciel et la Terre, la mer et les sources des eaux.
14:8	Et un autre ange le suivit, en disant : Elle est tombée, elle est tombée Babel<!--Le nom Babel signifie « la porte de El » ou « confusion par le mélange ». Généralement traduit par Babylone.-->, la grande ville, parce qu'elle a donné à boire à toutes les nations du vin de la colère de sa relation sexuelle illicite !

### Le jugement des adorateurs de la bête

14:9	Et un troisième ange les suivit, disant à grande voix : Si quelqu'un adore la bête et son image, et reçoit la marque sur son front ou sur sa main,
14:10	il boira, lui aussi, du vin de la colère d'Elohîm, du vin pur versé dans la coupe de sa colère, et il sera tourmenté dans le feu et le soufre devant les saints anges et devant l'Agneau.
14:11	Et la fumée de leur tourment monte d'âge en âge et ils n'ont de repos ni jour ni nuit, ceux qui adorent la bête et son image, et quiconque reçoit la marque de son nom.
14:12	Ici est la persévérance des saints, ici, ceux qui gardent les commandements d'Elohîm et la foi de Yéhoshoua.

### Bénédiction de ceux qui meurent en Mashiah

14:13	Et j'entendis une voix du ciel me disant : Écris : Bénis sont dès à présent les morts qui meurent dans le Seigneur ! Oui, c'est vrai ! dit l'Esprit, afin qu'ils se reposent<!--Hé. 4:10.--> de leurs travaux, mais leurs œuvres les suivent.

### Prophétie sur Harmaguédon

14:14	Et je regardai, et voici une nuée blanche, et sur la nuée quelqu'un d'assis, semblable au fils de l'homme<!--Ez. 1:26 ; Da. 7:13 ; Mt. 24:30, 26:64 ; Ap. 1:13.-->, ayant sur sa tête une couronne d'or et dans sa main une faucille tranchante.
14:15	Et un autre ange sortit du temple, criant à grande voix à celui qui est assis sur la nuée : Jette ta faucille et moissonne ! Parce que l'heure de moissonner est venue pour toi, parce que la moisson de la Terre est desséchée<!--Jé. 51:33 ; Mt. 13:30-39.-->.
14:16	Et celui qui est assis sur la nuée jeta sa faucille sur la Terre, et la Terre fut moissonnée.
14:17	Et un autre ange sortit du temple qui est dans le ciel, ayant lui aussi une faucille tranchante.
14:18	Et un autre ange, ayant autorité sur le feu, sortit de l'autel, et il parla à grand cri à celui qui a la faucille tranchante en disant : Jette ta faucille tranchante et vendange les grappes de la vigne de la Terre, car ses raisins sont mûrs.
14:19	Et l'ange jeta sa faucille tranchante sur la Terre et vendangea la vigne de la Terre, et il jeta la vendange dans la grande cuve de la colère d'Elohîm.
14:20	Et la cuve fut foulée aux pieds hors de la ville. Et du sang sortit de la cuve, jusqu'aux mors des chevaux, sur 1 600 stades<!--Es. 63:1-6.-->.

## Chapitre 15

### Une scène glorieuse au ciel

15:1	Et je vis dans le ciel un autre signe, grand et admirable : sept anges, ayant sept fléaux, les derniers, parce que c'est par eux que s'accomplit la colère d'Elohîm.
15:2	Et je vis comme une mer de verre mêlée de feu, et ceux qui avaient remporté la victoire sur la bête et sur son image, et sur sa marque, et sur le nombre de son nom, debout sur la mer de verre, ayant les harpes d'Elohîm.
15:3	Et ils chantent le cantique de Moshè, esclave d'Elohîm et le cantique de l'Agneau, en disant : Tes œuvres sont grandes et merveilleuses, Seigneur, l'Elohîm Tout-Puissant ! Tes voies sont justes et véritables, ô Roi des saints !
15:4	Seigneur, qui ne te craindrait et qui ne glorifierait ton Nom ? Parce que toi seul tu es saint, c'est pourquoi toutes les nations viendront et se prosterneront devant toi, parce que tes jugements ont été manifestés.
15:5	Et après ces choses, je regardai, et voici le temple du tabernacle du témoignage fut ouvert dans le ciel.
15:6	Et les sept anges qui ont en main les sept fléaux sortirent du temple, revêtus d'un lin pur et éclatant, et ceints autour de la poitrine de ceintures d'or.
15:7	Et l'un des quatre êtres vivants donna aux sept anges sept coupes d'or remplies de la colère d'Elohîm qui vit d'âge en âge.
15:8	Et le temple fut rempli de la fumée par la gloire d'Elohîm et par sa puissance, et personne ne pouvait entrer dans le temple jusqu'à ce que les sept fléaux des sept anges soient accomplis.

## Chapitre 16

### Première coupe : Les ulcères

16:1	Et j'entendis du temple une grande voix disant aux sept anges : Allez et versez sur la Terre les coupes de la colère d'Elohîm.
16:2	Et le premier s'en alla et versa sa coupe sur la Terre. Et un ulcère pernicieux et malin vint sur les humains qui avaient la marque de la bête et sur ceux qui adoraient son image.

### Deuxième coupe : La mer changée en sang

16:3	Et le second ange versa sa coupe sur la mer et elle devint du sang, comme celui d'un mort. Et toute âme qui vivait dans la mer mourut.

### Troisième coupe : Les sources changées en sang

16:4	Et le troisième ange versa sa coupe sur les fleuves et sur les sources des eaux, et elles devinrent du sang.
16:5	Et j'entendis l'ange des eaux, disant : Seigneur, QUI ES, QUI ÉTAIS et QUI VIENS, tu es juste, parce que tu as exercé ce jugement.
16:6	Parce qu'ils ont répandu le sang des saints et des prophètes, tu leur as aussi donné du sang à boire, car ils le méritent.
16:7	Et j'en entendis un autre de l'autel, disant : Oui, Seigneur, l'Elohîm Tout-Puissant, tes jugements sont véritables et justes.

### Quatrième coupe : Une chaleur extrême

16:8	Et le quatrième ange versa sa coupe sur le soleil, et il lui fut donné de brûler les humains par le feu.
16:9	Et les humains furent brûlés par de grandes chaleurs, et ils blasphémèrent le Nom d'Elohîm qui a l'autorité sur ces fléaux, et ils ne se repentirent pas pour lui donner gloire.

### Cinquième coupe : Les ténèbres sur le trône de la bête

16:10	Et le cinquième ange versa sa coupe sur le trône de la bête et son royaume devint ténèbres. Et ils mâchaient leurs langues de douleur.
16:11	Et ils blasphémèrent l'Elohîm du ciel à cause de leurs douleurs et de leurs ulcères, et ils ne se repentirent pas de leurs œuvres.

### Sixième coupe : L'Euphrate asséché

16:12	Et le sixième ange versa sa coupe sur le grand fleuve, l'Euphrate. Et son eau tarit, afin que la voie des rois venant du côté où le soleil se lève fût préparée.
16:13	Et je vis sortir de la gueule du dragon, et de la gueule de la bête, et de la bouche du faux prophète, trois esprits impurs semblables à des grenouilles.
16:14	Car ce sont des esprits de démons qui produisent des signes<!--Voir Mt. 24:24.--> et qui vont vers les rois de la Terre et du monde entier, afin de les rassembler pour la guerre du grand jour d'Elohîm Tout-Puissant.
16:15	Voici, je viens comme un voleur. Béni est celui qui veille et qui garde ses vêtements, afin de ne pas marcher nu, et qu'on ne voie pas sa honte !
16:16	Et on les rassembla dans le lieu qui est appelé en hébreu Harmaguédon<!--Le terme Harmaguédon, mentionné uniquement dans ce passage, vient du mot hébreu « har-magidown », ce qui signifie « montagne de Meggido ». Bien qu'il n'existe pas de montagne portant spécifiquement ce nom, l'emplacement probable de cet endroit est la plaine de Meggido se trouvant à proximité de Yeroushalaim (Jérusalem). Par le passé, elle fut le théâtre de la victoire de Barak sur les Cananéens (Jg. 4:15) et de celle de Guid'ôn (Gédéon) sur les Madianites (Jg. 7). C'est aussi à cet endroit que Shaoul (Saül) et ses fils (1 S. 31:8) ainsi que le roi Yoshiyah (Josias) (2 R. 23:29-30 ; 2 Ch. 35:22) trouvèrent la mort. Pour toutes ces raisons, elle devint au fil du temps le symbole de l'affrontement entre Elohîm et la puissance des ténèbres. Selon les prophéties bibliques, la plaine de Meggido et la vallée de Yizre`e'l (Jizréel) constitueront le site de l'ultime guerre mondiale, celle opposant l'Anti-Mashiah (Antichrist) et ses alliés (dirigeants des nations) contre Israël. Le Seigneur interviendra alors ouvertement dans les affaires humaines pour déverser la coupe de sa colère (Ap. 16:1) et anéantir l'homme impie et toute son armée (Ez. 38-39 ; Joë. 3 ; Mi. 4:11 ; So. 1 ; Za. 14:1-15 ; Mt. 24:29-30 ; Ap. 20:1-3,7-10).-->.

### Septième coupe : Une grosse grêle tombe du ciel

16:17	Et le septième ange versa sa coupe dans l'air et, du temple du ciel, du trône, il sortit une grande voix, disant : C'est fait !
16:18	Et il y eut des voix et des tonnerres et des éclairs. Et il y eut un grand tremblement de terre tel qu'il n'y en avait jamais eu depuis que l'être humain est sur la Terre.
16:19	Et la grande ville fut divisée en trois parties, et les villes des nations tombèrent, et Babel la grande vint en mémoire devant Elohîm, pour qu'il lui donnât la coupe du vin de la fureur de sa colère.
16:20	Et toute île s'enfuit et les montagnes ne furent plus trouvées.
16:21	Et une grosse grêle, comme du poids d'un talent descendit du ciel sur les humains. Et les humains blasphémèrent Elohîm à cause du fléau de la grêle, parce que ce fléau est extrêmement grand.

## Chapitre 17

### La prostituée

17:1	Et l'un des sept anges qui tenaient les sept coupes vint, et il m'adressa la parole, en disant : Viens, je te montrerai le jugement de la grande prostituée, qui est assise sur les grandes eaux,
17:2	avec laquelle les rois de la Terre se sont prostitués. Et les habitants de la Terre ont été enivrés du vin de sa relation sexuelle illicite.
17:3	Et il me transporta dans un désert, en esprit. Et je vis une femme assise sur une bête couleur d'écarlate, pleine de noms de blasphème, ayant sept têtes et dix cornes.
17:4	Et la femme était vêtue de pourpre et d'écarlate et parée d'or, de pierres précieuses et de perles. Et elle tenait à la main une coupe d'or pleine d'abominations et des impuretés de sa relation sexuelle illicite.
17:5	Et sur son front, un nom écrit : Mystère, Babel la grande, la mère des prostituées<!--Voir 1 Co. 6:15-16 ; Ap. 17:15-16, 19:2.--> et des abominations de la Terre<!--Symboliquement, Babel la grande incarne l'Assemblée apostate. Elle est soutenue par la bête qu'elle chevauche, c'est-à-dire l'homme impie. Ces deux entités forment un système impie où la politique et la religion se mélangent (Da. 2:43).-->.
17:6	Et je vis cette femme ivre du sang des saints et du sang des martyrs de Yéhoshoua. Et en la voyant, je fus étonné, d'une grande admiration.

### Alliance entre la prostituée et la bête

17:7	Et l'ange me dit : Pourquoi es-tu dans l'admiration ? Je te dirai, moi, le mystère de la femme et de la bête qui la porte, qui a les sept têtes et les dix cornes.
17:8	La bête que tu as vue, était, et elle n'est plus. Elle doit monter de l'abîme et aller à la destruction. Et les habitants de la Terre, ceux dont les noms ne sont pas écrits dans le livre de vie dès la fondation du monde<!--Voir Ap. 13:8.-->, seront dans l'admiration en voyant la bête parce qu'elle était, et qu'elle n'est plus, bien qu'elle soit.
17:9	Ici, l'intelligence<!--L'esprit, c'est-à-dire toutes les facultés de perception et de compréhension et celles de sentiment, jugement, détermination.--> qui a de la sagesse. Les sept têtes sont sept montagnes sur lesquelles la femme est assise.
17:10	Et elles sont sept rois : les cinq sont tombés, l'un est, et l'autre n'est pas encore venu. Et quand il sera venu, il faut qu'il demeure pour un peu de temps.
17:11	Et la bête qui était et qui n'est plus, est elle-même un huitième. Elle est aussi du nombre des sept et s'en va à la destruction.
17:12	Et les dix cornes que tu as vues sont dix rois qui n'ont pas encore reçu le royaume, mais qui reçoivent autorité comme rois pour une heure avec la bête.
17:13	Ceux-ci ont un même dessein, et ils donneront leur puissance et leur autorité à la bête.
17:14	Ceux-ci combattront contre l'Agneau et l'Agneau remportera la victoire sur eux, parce qu'il est le Seigneur des seigneurs et le Roi des rois<!--Voir De. 10:17 ; Ap. 19:16.-->, ainsi que ceux qui sont avec lui, appelés, et élus, et fidèles.
17:15	Et il me dit : Les eaux que tu as vues, et sur lesquelles la prostituée est assise, sont des peuples, des nations et des langues.
17:16	Et les dix cornes que tu as vues sur la bête, sont ceux qui haïront la prostituée, la dépouilleront et la mettront à nu, elles mangeront ses chairs et la consumeront par le feu.
17:17	Car Elohîm a mis dans leurs cœurs d'exécuter son dessein et d'exécuter un même dessein, et de donner leur royaume à la bête, jusqu'à ce que les paroles d'Elohîm soient accomplies.
17:18	Et la femme que tu as vue, c'est la grande ville qui a le pouvoir royal sur les rois de la Terre.

## Chapitre 18

### Babel détruite

18:1	Et après ces choses, je vis un autre ange descendant du ciel, ayant une grande autorité, et la Terre fut illuminée de sa gloire.
18:2	Et il cria avec force d'une grande voix, disant : Elle est tombée, elle est tombée Babel la grande ! et elle est devenue la demeure de démons, et le repaire de tout esprit impur, et le repaire de tout oiseau impur et détestable.
18:3	Parce que toutes les nations ont bu du vin de la colère de sa relation sexuelle illicite, et les rois de la Terre se sont prostitués avec elle, et les marchands de la Terre se sont enrichis par l'excès de son luxe.
18:4	Et j'entendis une autre voix du ciel disant : Sortez d’elle<!--Voir Es. 48:20 et Je. 51:6-8.--> mon peuple, afin que vous ne participiez pas à ses péchés et que vous ne receviez pas une part de ses fléaux.
18:5	Parce que ses péchés sont montés jusqu'au ciel, et Elohîm s'est souvenu de ses iniquités.
18:6	Payez-la comme elle-même vous a payés et doublez le double de ses œuvres. Et dans la même coupe où elle vous a versé à boire, versez-lui au double.
18:7	Autant elle s'est glorifiée et plongée dans le luxe, autant donnez-lui de tourment et de deuil. Car elle dit dans son cœur : Je siège en reine, je ne suis pas veuve et je ne verrai pas de deuil.
18:8	C'est pourquoi, en un seul jour arriveront ses plaies : la mort, le deuil et la famine, et elle sera consumée par le feu. Car il est puissant, le Seigneur Elohîm qui la juge.
18:9	Et les rois de la Terre, qui se sont prostitués avec elle et qui ont vécu dans le luxe, pleureront et se frapperont la poitrine de chagrin à son sujet, quand ils verront la fumée de son embrasement.
18:10	Se tenant à distance par crainte de son tourment, et ils diront : Malheur ! Malheur ! Babel la grande, cette ville si puissante, comment ta condamnation est-elle venue en une seule heure ?
18:11	Et les marchands de la Terre aussi pleurent et sont dans le deuil à son sujet, parce que plus personne n'achète leurs marchandises,
18:12	qui sont des marchandises d'or et d'argent, et de pierres précieuses et de perles, et de fin lin et de pourpre, et de soie et d'écarlate, et de toutes sortes de bois de senteur, et de toutes sortes d'objets en ivoire, et en bois très précieux, et en cuivre, et en fer et en marbre,
18:13	et de cinnamome, et d'encens, et de baumes, et de l'arbre à encens, et de vin, et d'huile, et de fine farine, et de blé, et de bœufs, et de brebis, et de chevaux, et de chars, et de corps et d'âmes humaines<!--Ez. 27:13.-->.
18:14	Le fruit mûr<!--La saison qui succède à l'été, entre le lever de Sirius et celui d'Arcturus, la fin de l'été, le début de l'automne.--> que désirait ton âme s’en est allé loin de toi. Et toutes les choses qui appartiennent à un somptueux et délicat style de vie et les choses magnifiques se sont éloignées de toi, et tu ne les trouveras plus jamais.
18:15	Les marchands de ces choses, qui se sont enrichis par elle, se tiendront à distance par crainte de son tourment. Ils pleureront et seront dans le deuil,
18:16	et disant : Malheur ! Malheur ! La grande ville qui était vêtue de fin lin, de pourpre, d'écarlate, qui était parée d'or, ornée de pierres précieuses et de perles, 
18:17	parce qu'en une heure une richesse si grande a été dévastée ! Et tous les maîtres de navigation aussi, tous ceux qui naviguent vers ce lieu, tous les marins et tous ceux qui exploitent la mer se tenaient à distance,
18:18	et, en voyant la fumée de son embrasement, ils s'écriaient en disant : Quelle ville était semblable à cette grande ville ?
18:19	Et ils se jetaient de la poussière sur la tête, pleurant et étant dans le deuil, et ils disaient : Malheur ! Malheur ! La grande ville où se sont enrichis par son opulence tous ceux qui ont des navires sur la mer, comment a-t-elle été réduite en désert en une seule heure ?
18:20	Ciel, réjouis-toi à cause d'elle ! Et vous aussi saints apôtres et prophètes, réjouissez-vous ! Car Elohîm l'a punie à cause de vous.
18:21	Et un ange puissant souleva une pierre semblable à une grande meule et la jeta dans la mer en disant : Ainsi sera jetée avec violence Babel, la grande ville, et on ne la trouvera plus<!--Jé. 51:63-64.-->.
18:22	Et la voix des joueurs de harpe et des musiciens, et celle des joueurs de flûte et de trompette ne sera plus entendue chez toi. Et aucun artisan, de quelque métier que ce soit, ne s'y trouvera plus. Et le bruit de la meule n’y sera plus entendu.
18:23	Et la lumière de la lampe ne brillera plus chez toi, et la voix de l'époux et de l'épouse n'y sera plus entendue. Parce que tes marchands étaient des princes de la Terre, parce que par tes sorcelleries, toutes les nations ont été égarées.
18:24	Et c'est chez elle qu'a été trouvé le sang des prophètes et des saints, et de tous ceux qui ont été tués sur la Terre.

## Chapitre 19

### Allégresse dans les cieux suite au jugement de la grande prostituée<!--Ap. 17:16-17, 18:8.-->

19:1	Et après ces choses, j'entendis comme la grande voix d'une foule nombreuse dans le ciel, disant : Allélou-Yah ! Le salut, et la gloire, et l'honneur, et la puissance, au Seigneur notre Elohîm,
19:2	parce que ses jugements sont véritables et justes, parce qu'il a jugé la grande prostituée qui a corrompu la Terre par sa relation sexuelle illicite, et qu'il a vengé le sang de ses esclaves versé de la main de la prostituée.
19:3	Et ils dirent encore : Allélou-Yah ! Et sa fumée monte d'âge en âge.
19:4	Et les vingt-quatre anciens et les quatre êtres vivants se prosternèrent sur leurs faces, et adorèrent Elohîm qui est assis sur le trône, en disant : Amen ! Allélou-Yah !
19:5	Et une voix sortit du trône, disant : Louez notre Elohîm, vous tous ses esclaves, et vous qui le craignez, tant les petits que les grands<!--Ps. 134.-->.

### Festin des noces de l'Agneau

19:6	Et j'entendis comme la voix d'une foule nombreuse, et comme la voix de grandes eaux, et comme la voix de puissants tonnerres, disant : Allélou-Yah ! Parce que le Seigneur, l'Elohîm Tout-Puissant a exercé son règne.
19:7	Réjouissons-nous et tressaillons de joie, et donnons-lui gloire, parce qu'elles sont venues les noces de l'Agneau, et que son Épouse s'est préparée.
19:8	Et il lui a été donné de se revêtir d'un fin lin pur et éclatant. Car le fin lin, ce sont les œuvres justes des saints.
19:9	Et il me dit : Écris : Bénis sont ceux qui sont appelés au souper des noces de l'Agneau<!--Mt. 22:1-13 ; Lu. 14:15-24.--> ! Il me dit aussi : Ces paroles sont les véritables paroles d'Elohîm.
19:10	Et je tombai à ses pieds pour l'adorer, mais il me dit : Non, attention, je suis ton compagnon de service et celui de tes frères qui ont le témoignage de Yéhoshoua. Adore Elohîm ! Car le témoignage de Yéhoshoua est l'Esprit de la prophétie.

### Seconde venue du Mashiah dans la gloire<!--Mt. 24:16-30.-->

19:11	Et je vis le ciel ouvert, et voici parut un cheval blanc. Et celui qui est assis sur lui s'appelle FIDÈLE et VÉRITABLE, et il juge et combat avec justice.
19:12	Et ses yeux sont comme une flamme de feu<!--Voir Es. 66:15.--> et il y a sur sa tête beaucoup de diadèmes. Il a un nom écrit que personne ne connaît, si ce n'est lui-même.
19:13	Et il était revêtu d'un vêtement trempé de sang. Et son Nom s'appelle : La parole d'Elohîm.
19:14	Et les armées qui sont dans le ciel le suivaient sur des chevaux blancs, revêtues de fin lin blanc et pur.
19:15	Et de sa bouche sort une épée tranchante<!--Es. 11:4 ; 2 Th. 2:8 ; Hé. 4:12.-->, pour en frapper les nations. Il les gouvernera avec un sceptre de fer<!--Ps. 2:8-9.-->, et il foulera aux pieds la cuve du vin de l'indignation et de la colère d'Elohîm Tout-Puissant.
19:16	Et il a sur son vêtement et sur sa cuisse ce nom écrit : Roi des rois et Seigneur des seigneurs<!--Voir De. 10:17 et 1 Ti. 6:15 ; Ap. 17:14.-->.

### Bataille d'Harmaguédon<!--Ap. 16:16.-->

19:17	Et je vis un ange qui se tenait dans le soleil. Et il cria d'une grande voix, disant à tous les oiseaux qui volent au zénith<!--Le point le plus haut des cieux, que le soleil atteint à midi, le point d'où tout ce qui est fait peut être vu et entendu de tous.--> dans le ciel : Venez et rassemblez-vous pour le grand souper d'Elohîm,
19:18	afin de manger la chair des rois, et la chair des chefs militaires, et la chair des puissants, et la chair des chevaux et de ceux qui les montent, et la chair de toute sorte de personnes, libres, esclaves, petits et grands.
19:19	Et je vis la bête et les rois de la Terre, et leurs armées rassemblées pour faire la guerre<!--Guerre d'Harmaguédon : voir commentaire Ap. 16:14-16.--> contre celui qui était monté sur le cheval et contre son armée.

### Condamnation de la bête et du faux prophète

19:20	Et la bête fut prise, et avec elle, le faux prophète qui avait produit devant elle les signes par lesquels il avait égaré ceux qui avaient pris la marque de la bête et adoré son image. Et ils furent tous les deux jetés vivants dans le lac de feu brûlant, dans le soufre.

### Condamnation des rois et des armées

19:21	Et le reste fut tué par l'épée sortant de la bouche de celui qui est monté sur le cheval, et tous les oiseaux furent rassasiés de leur chair.

## Chapitre 20

### Satan lié pour 1 000 ans et règne du Mashiah

20:1	Et je vis un ange descendant du ciel, ayant la clé de l'abîme et une grande chaîne dans sa main.
20:2	Et il saisit le dragon, le serpent ancien, qui est le diable et Satan, et le lia pour 1 000 ans.
20:3	Et il le jeta dans l'abîme, et il l'enferma et mit le sceau sur lui, afin qu'il n'égare plus les nations, jusqu'à ce que les 1 000 ans soient accomplis. Et après ces choses, il faut qu'il soit délié pour un peu de temps.

### Dernière phase de la première résurrection

20:4	Et je vis des trônes sur lesquels ils s'assirent, et le jugement<!--1 Co. 6:2.--> leur fut donné. Et les âmes de ceux qui avaient été décapités à la hache à cause du témoignage de Yéhoshoua et à cause de la parole d'Elohîm, et de ceux qui n'avaient pas adoré la bête ni son image, et qui n'avaient pas pris sa marque sur leurs fronts ou sur leurs mains, vécurent<!--Jn. 14:19.--> en effet, et ils régnèrent avec le Mashiah 1 000 ans.
20:5	Mais les autres morts ne revinrent pas à la vie jusqu'à ce que les 1 000 ans soient accomplis. C'est la première résurrection.
20:6	Bénis et saints sont ceux qui ont part à la première résurrection ! La seconde mort n'a pas d'autorité sur eux, mais ils seront prêtres d'Elohîm et du Mashiah, et ils régneront avec lui 1 000 ans.

### Satan délié ; sa chute finale

20:7	Et quand les 1 000 ans seront accomplis, Satan sera délié de sa prison.
20:8	Et il sortira pour égarer les nations aux quatre coins de la Terre, Gog et Magog, afin de les rassembler pour la guerre, et leur nombre est comme le sable de la mer.
20:9	Et ils montèrent sur l'étendue de la Terre et ils encerclèrent le camp des saints, et la ville aimée. Et du feu descendit du ciel de la part d'Elohîm et les dévora.

### Satan jeté dans le lac de feu

20:10	Et le diable qui les égare fut jeté dans le lac de feu et de soufre où sont la bête et le faux prophète. Et ils seront tourmentés jour et nuit, d'âge en âge.

### Résurrection des impies et jugement dernier ; l'Hadès (ou l'enfer) et la mort jetés dans le lac de feu

20:11	Et je vis un grand trône blanc, et celui qui est assis dessus. La Terre et le ciel s'enfuirent devant sa face, et il ne fut plus trouvé de place pour eux.
20:12	Et je vis les morts, les grands et les petits, qui se tenaient devant Elohîm. Des livres furent ouverts. Et un autre livre fut ouvert, celui qui est le livre de vie. Et les morts furent jugés d’après ce qui était écrit dans les livres, selon leurs œuvres.
20:13	Et la mer donna les morts qui étaient en elle, et la mort et l'Hadès<!--Voir commentaire en Mt. 16:18.--> donnèrent les morts qui étaient en eux, et ils furent jugés chacun selon ses œuvres.
20:14	Et la mort et l'Hadès furent jetés dans le lac de feu<!--Le lac de feu est aussi appelé « seconde mort », c'est la destination finale de tous les impies, des démons et de Satan. On l'appelle « la seconde mort » parce qu'elle a été précédée de la mort physique. Cette mort n'est pas un anéantissement, mais une condition de souffrances éternelles. C'est la séparation définitive d'avec Elohîm. À l'issue du jugement dernier, Hadès (le séjour des morts ou l'enfer) sera jeté dans le lac de feu (voir commentaire en Mt. 16:18). La Bible utilise également le mot « géhenne » pour décrire l'endroit où les impies passeront l'éternité. Ce terme vient de l'hébreu « ge-hinnom », autrement dit vallée de Ben Hinnom (littéralement « le lieu du feu ») qui se trouve en Israël, en contrebas du mont Sion sur lequel est bâtie la ville de Yeroushalaim (Jérusalem) (Mt. 5:22, 5:29-30, 10:28, 18:9, 23:15, 23:33 ; Mc. 9:47 ; Lu. 12:5 ; Ja. 3:6). Autrefois, on y brûlait des enfants en l'honneur de Moloc, une divinité ammonite (2 R. 23:10 ; Jé. 32:35), puis des immondices. Ce lieu est devenu avec le temps le symbole du péché et de l'affliction et c'est ainsi qu'il finit par désigner le lieu du châtiment éternel.-->. C'est la seconde mort.
20:15	Et quiconque ne fut pas trouvé écrit dans le livre de vie fut jeté dans le lac de feu.

## Chapitre 21

### Nouveaux cieux et une nouvelle Terre ; la nouvelle Yeroushalaim (Jérusalem)

21:1	Et je vis un nouveau ciel et une nouvelle Terre, car le premier ciel et la première Terre ont passé<!--Voir Mt. 24:35 ; Lu. 16:17, 21:33.--> et la mer n'est plus.
21:2	Et moi, Yohanan, je vis la cité sainte, la nouvelle Yeroushalaim, descendant du ciel d'auprès d'Elohîm, préparée<!--Jn. 14:2-3.--> comme une épouse qui s'est ornée pour son époux.
21:3	Et j'entendis une grande voix du ciel disant : Voici le tabernacle d'Elohîm avec les humains ! Et il dressera sa tente avec eux et ils seront ses peuples et Elohîm lui-même sera avec eux, leur Elohîm.
21:4	Et Elohîm essuiera toute larme de leurs yeux et la mort ne sera plus, et il n'y aura plus ni deuil, ni cri, ni douleur, parce que les premières choses sont passées.
21:5	Et celui qui est assis sur le trône dit : Voici, je fais toutes choses nouvelles. Et il me dit : Écris, parce que ces paroles sont véritables et certaines.
21:6	Il me dit aussi : C'est fait ! Moi je suis l’Aleph et le Tav, le commencement et la fin. À celui qui a soif, je donnerai de la source de l’eau de la vie gratuitement<!--Es. 55:1-2 ; Mt. 10:8 ; Ap. 22:17. Voir commentaire Mt. 10:8.-->.
21:7	Celui qui remporte la victoire héritera de toutes choses, et je serai son Elohîm et il sera mon fils.
21:8	Mais pour les timides, les incrédules, les abominables, les meurtriers, les fornicateurs, les sorciers, les idolâtres et tous les menteurs, leur part sera dans le lac brûlant de feu et de soufre, ce qui est la seconde mort.

### L'Épouse de l'Agneau et la nouvelle Yeroushalaim

21:9	Et il vint à moi l’un des sept anges qui a les sept coupes remplies des sept derniers fléaux, et il me parla, disant : Viens et je te montrerai l'Épouse de l'Agneau<!--Yeroushalaim (Jérusalem), femme de l'Agneau ou femme de YHWH. Voir Es. 54:5.-->, la femme.
21:10	Et il me transporta en esprit sur une grande et haute montagne et il me montra la grande cité, la sainte Yeroushalaim, descendant du ciel d'auprès d'Elohîm,
21:11	ayant la gloire d'Elohîm. Et son luminaire<!--Voir Ph. 2:15.--> est semblable à une pierre très précieuse, comme à une pierre de jaspe transparente comme du cristal.
21:12	Et elle a une grande et haute muraille, avec douze portes, et aux portes douze anges, et des noms écrits sur elles, qui sont les noms des douze tribus des fils d'Israël<!--Ez. 48:31-34.-->.
21:13	À l'orient, trois portes, au nord, trois portes, du côté du sud, trois portes et du côté de l'occident, trois portes.
21:14	Et la muraille de la cité a douze fondements, et sur eux les noms des douze apôtres de l'Agneau<!--Lu. 22:29-30 ; Ep. 2:20.-->.
21:15	Et celui qui parlait avec moi avait un roseau d'or pour mesurer la cité, ses portes et sa muraille.
21:16	Et la cité est bâtie en carré, et sa longueur est aussi grande que sa largeur. Il mesura donc la cité avec le roseau, jusqu'à 12 000 stades ; la longueur, la largeur et la hauteur étaient égales.
21:17	Et il mesura la muraille : 144 coudées, d'une mesure humaine qui était celle de l'ange.
21:18	Et la muraille est construite en jaspe et la cité est en or pur, semblable à un verre transparent.
21:19	Et les fondements de la muraille de la cité sont ornés de toutes sortes de pierres précieuses<!--Es. 54:11-12.--> : le premier fondement est de jaspe, le second de saphir, le troisième de calcédoine, le quatrième d'émeraude,
21:20	le cinquième de sardonyx, le sixième de sardoine, le septième de chrysolithe, le huitième de béryl, le neuvième de topaze, le dixième de chrysoprase, le onzième d'hyacinthe, le douzième d'améthyste.
21:21	Et les douze portes sont douze perles. Chacune des portes est d'une seule perle. Et la place de la cité est en or pur, comme du verre transparent.
21:22	Et je ne vis pas de temple, car le Seigneur, l'Elohîm Tout-Puissant, en effet l'Agneau, en est lui-même le Temple.
21:23	Et la cité n'a pas besoin du soleil ni de la lune pour l'éclairer, car la gloire d'Elohîm l'éclaire et l'Agneau est lui-même sa lampe<!--Es. 60:19.-->.
21:24	Et les nations de ceux qui sont sauvés marcheront dans sa lumière, et les rois de la Terre y apporteront leur gloire et leur honneur.
21:25	Et ses portes ne se fermeront pas le jour, car il n'y aura pas de nuit<!--Es. 60:11.-->.
21:26	Et on y apportera la gloire et l'honneur des nations.
21:27	Et il n'entrera en elle rien qui rend impur, ni qui commette l'abomination et le mensonge, mais seulement ceux qui sont écrits dans le livre de vie de l'Agneau.

## Chapitre 22

### Règne éternel des saints avec l'Agneau

22:1	Et il me montra un fleuve d'eau de la vie<!--Ce fleuve représente le Saint-Esprit : Ez. 47:1-12 ; Ps. 46:5 ; Da. 7:9-10 ; Jn. 7:38-39.-->, brillant comme du cristal, sortant du trône d'Elohîm et de l'Agneau.
22:2	Et au milieu de la place de la cité et des deux côtés du fleuve, l'arbre de vie portant douze fruits et rendant son fruit chaque mois, et les feuilles de l'arbre sont pour la guérison des nations<!--Ge. 2:9, 3:22 ; Ez. 47:12.-->.
22:3	Et il n'y aura plus aucune malédiction. Et le trône d'Elohîm et de l'Agneau y sera. Et ses esclaves lui rendront leur culte,
22:4	et ils verront sa face, et son Nom sera sur leurs fronts.
22:5	Et il n'y aura plus de nuit et ils n'auront besoin ni de lumière, ni de lampe, ni du soleil, parce que le Seigneur Elohîm les illuminera<!--Voir Jn. 1:9 ; 1 Co. 4:5.-->. Et ils régneront d'âge en âge.

### Certitude des prophéties de ce livre

22:6	Et il me dit : Ces paroles sont sûres et véritables. Et le Seigneur, l'Elohîm des saints prophètes, a envoyé son ange pour manifester à ses esclaves les choses qui doivent arriver vite<!--Vient du grec « tachos » qui signifie « vitesse, rapidité, vivacité et promptitude ».--> :
22:7	Voici, je viens à toute vitesse<!--Le texte grec utilise le mot « tachu » qui signifie « rapidement, à toute vitesse (sans tarder) ». Beaucoup doutent de cette promesse du Seigneur en faisant la même réflexion évoquée par Petros (Pierre) : « Où est la promesse de son avènement ? Car depuis que les pères sont morts, tout reste permanent comme depuis le commencement de la création » (2 Pi. 3:4). Or le Seigneur ne tarde pas dans l'accomplissement de sa promesse, car il a fixé de sa propre autorité une date pour son retour, que lui seul connaît (Za. 14:7 ; Mt. 24:36 ; Mc. 13:32 ; Ac. 1:6-7). Il sera donc fidèle à son calendrier et ne tardera pas (2 Pi. 3:9 ; Hé. 10:37).-->. Béni est celui qui garde les paroles de la prophétie de ce livre !
22:8	C'est moi, Yohanan, qui ai entendu et vu ces choses. Et après les avoir entendues et vues, je tombai à terre aux pieds de l'ange qui me les montrait pour l'adorer.
22:9	Mais il me dit : Non, attention ! Car je suis ton compagnon de service<!--Hé. 1:14.--> et celui de tes frères les prophètes, et de ceux qui gardent les paroles de ce livre. Adore Elohîm !
22:10	Il me dit aussi : Ne scelle pas les paroles de la prophétie de ce livre, car le temps est proche.
22:11	Que celui qui agit injustement, agisse encore injustement, et que celui qui est sale, se salisse encore, et que celui qui est juste pratique encore la justice et que celui qui est saint se sanctifie encore !
22:12	Et voici, je viens à toute vitesse, et ma rétribution est avec moi<!--Yéhoshoua affirme de nouveau ici sa divinité et confirme les prophéties d'Es. 35:4, 40:10, 62:11, où il est dit que YHWH lui-même viendra avec ses rétributions.--> pour rendre à chacun comme sera son œuvre.
22:13	Moi je suis l’Aleph et le Tav, le premier et le dernier, le commencement et la fin.
22:14	Bénis sont ceux qui observent ces commandements ! Ils auront l'autorité sur l'arbre de vie et ils entreront par les portes dans la cité.
22:15	Mais seront laissés dehors les chiens, les sorciers, les fornicateurs, les meurtriers, les idolâtres et quiconque aime et pratique le mensonge.
22:16	Moi, Yéhoshoua, j'ai envoyé mon ange<!--Cette déclaration de Yéhoshoua fait écho au verset 6, où il est dit que le Seigneur, l'Elohîm des saints prophètes, a envoyé son ange. Yéhoshoua confirme donc qu'il est Seigneur et Elohîm.--> pour vous rendre témoignage de ces choses dans les assemblées. Je suis la Racine<!--Voir Es. 11:10 ; Ro. 11:16-18, 15:18 ; Ap. 5:5.--> et la race<!--Voir 1 Pi. 2:9.--> de David, l'Étoile brillante et matinale.
22:17	Et l'Esprit et l'Épouse disent : Viens ! Et que celui qui entend dise : Viens ! Et que celui qui a soif vienne ! Que celui qui veut de l'eau de la vie la prenne gratuitement !

### Nul ne doit y ajouter ou y retrancher

22:18	Car je rends témoignage à quiconque entend les paroles de la prophétie de ce livre : si quelqu'un y ajoute quelque chose, Elohîm le frappera des fléaux décrits dans ce livre<!--Voir De. 4:2 et Pr. 30:6.-->.
22:19	Et si quelqu'un retranche quelque chose des paroles du livre de cette prophétie, Elohîm retranchera sa part du livre de vie, de la cité sainte et des choses qui sont écrites dans ce livre.
22:20	Celui qui rend témoignage de ces choses, dit : Oui, je viens à toute vitesse. Amen ! Oui, Seigneur Yéhoshoua, viens !
22:21	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous tous ! Amen !
