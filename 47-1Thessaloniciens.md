# 1 Thessaloniciens (1 Th.)

Auteur : Paulos (Paul)

Thème : Le retour du Mashiah (Christ)

Date de rédaction : Env. 51 ap. J.-C.

Autrefois appelée Therme ou Therma, qui signifie « source chaude », Thessalonique reçut son nouveau nom de Cassandre, en l'honneur de sa femme Thessalonike, qui était aussi la sœur d'Alexandre le Grand (356 – 323 av. J.-C.), à qui il succéda. 

Cette ville est située au nord de la Grèce, sur la côte de la Mer Égée. Du temps de Paulos (Paul), ce pays était divisé en deux parties. Dans la région du nord, la Macédoine, se trouvaient les villes de Philippes, Thessalonique et Bérée. Quant à la région du sud, l'Achaïe, elle comportait les villes d'Athènes et de Corinthe. Aujourd'hui, la ville s'appelle Salonique.

En ce temps-là, Thessalonique comptait environ 200 000 habitants (Grecs, Romains et Juifs) et jouissait d'une importante fréquentation puisqu'elle figurait parmi les trois ports principaux de la Méditerranée et se situait sur l'une des plus grandes routes commerciales : La Voie Egnatienne reliant Rome à Byzance.

Sur le plan religieux, les habitants étaient polythéistes et pratiquaient une variété de cultes, dont le culte impérial. Durant trois semaines, Paulos enseigna dans une synagogue à Thessalonique et constitua un groupe de croyants composé de Juifs, des nations, de pauvres et de plusieurs femmes de la haute société. Toutefois, une violente persécution l'obligea à quitter promptement la ville, laissant la communauté nouvellement formée vulnérable et fragile.

La première épître adressée par Paulos aux Thessaloniciens avait pour but d'affermir les Thessaloniciens dans les vérités fondamentales qui leur avaient été enseignées, de les exhorter à vivre une vie de sainteté pour être agréables à Elohîm, de les éclairer quant au devenir des défunts et de les assurer du retour certain du Seigneur.

## Chapitre 1

1:1	Paulos, et Silvanos, et Timotheos, à l'assemblée des Thessaloniciens, en Elohîm le Père et Seigneur Yéhoshoua Mashiah : que la grâce et la paix vous soient données de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !
1:2	Nous rendons toujours grâces à Elohîm pour vous tous, faisant mention de vous dans nos prières,
1:3	en nous rappelant sans cesse l'œuvre de votre foi, le travail de votre amour, et l'immuabilité de votre espérance en notre Seigneur Yéhoshoua Mashiah devant notre Elohîm et Père,
1:4	sachant, frères aimés d'Elohîm, votre élection.

### Annoncer l'Évangile avec puissance et avec l'Esprit Saint

1:5	Parce que notre Évangile ne vous a pas été prêché en paroles seulement, mais aussi avec puissance, avec l'Esprit Saint et dans une grande plénitude d'assurance. Vous savez en effet que c'est ainsi que nous sommes venus parmi vous à cause de vous.
1:6	Et vous êtes devenus nos imitateurs et ceux du Seigneur, ayant reçu avec la joie du Saint-Esprit, la parole au milieu d'une grande tribulation.
1:7	De sorte que vous êtes devenus des modèles pour tous les croyants de la Macédoine<!--La Macédoine était le pays natal d'Alexandre le Grand. Elle fut conquise par les Romains et devint une province romaine, dont la capitale était Thessalonique.--> et de l'Achaïe<!--L'Achaïe était une province romaine placée sous l'autorité d'un proconsul résidant dans la capitale qui était Corinthe (2 Co. 1:1).-->.
1:8	Car la parole du Seigneur a retenti de chez vous, non seulement dans la Macédoine et dans l'Achaïe mais aussi en tous lieux, et votre foi envers Elohîm est si célèbre, que nous n'avons pas besoin d'en parler.
1:9	Car eux-mêmes racontent à notre sujet quelle entrée nous avons eue auprès de vous et comment vous vous êtes convertis à Elohîm, en vous séparant des idoles, pour servir l'Elohîm vivant et véritable,
1:10	et pour attendre des cieux son Fils Yéhoshoua, qu'il a ressuscité des morts et qui nous délivre de la colère à venir<!--La colère à venir. Voir les sept coupes de la colère d'Elohîm (Ap. 15:5-8, 16:1-21).-->.

## Chapitre 2

### Annoncer l'Évangile en recherchant l'approbation d'Elohîm et non celle des humains

2:1	Car vous-mêmes, frères, vous savez que notre entrée au milieu de vous n'a pas été vaine.
2:2	Mais après avoir souffert et subi des outrages à Philippes<!--Philippes était une ville de Macédoine située en Thrace, près de la côte nord de la Mer Égée. Voir Ac. 16:12-40 et l'épître de Paulos (Paul) aux Philippiens.-->, comme vous le savez, nous avons pris de l'assurance en notre Elohîm, pour vous annoncer l'Évangile d'Elohîm au milieu d'un grand combat.
2:3	Car notre exhortation ne provenait ni de l'égarement, ni de l'impureté, ni de la tromperie.
2:4	Mais comme nous avons été éprouvés<!--Voir 1 Ti. 3:10.--> par Elohîm pour que l'Évangile nous fût confié, ainsi nous parlons non comme pour plaire aux humains, mais à Elohîm qui éprouve nos cœurs.
2:5	Car nous ne sommes jamais venus avec une parole flatteuse, comme vous le savez. Jamais nous n'avons eu la cupidité pour mobile<!--Un prétexte (une raison alléguée, une cause prétendue).-->, Elohîm en est témoin.
2:6	Et nous n'avons pas cherché la gloire qui vient des humains, ni de vous, ni des autres. Nous aurions pu nous imposer comme apôtres du Mashiah,
2:7	mais nous avons été doux au milieu de vous, de même qu'une nourrice chérit ses enfants.
2:8	Nous aurions voulu, dans notre affection pour vous, non seulement vous donner l'Évangile d'Elohîm, mais encore notre propre vie, tant vous nous étiez devenus chers.
2:9	Car vous vous souvenez, frères, de notre peine et de notre travail : vu que nous vous avons prêché l'Évangile d'Elohîm, en travaillant nuit et jour, pour n'être à la charge d'aucun d'entre vous.
2:10	Vous êtes témoins et Elohîm aussi, combien notre conduite envers vous qui croyez a été sainte, juste, et irréprochable.
2:11	Et vous savez aussi que nous avons été pour chacun de vous comme un père pour ses enfants,
2:12	vous exhortant, vous encourageant et vous conjurant de vous conduire d'une manière digne d'Elohîm, qui vous appelle à son Royaume et à sa gloire.
2:13	Et c'est la raison pour laquelle nous rendons sans cesse grâces à Elohîm, de ce qu'en recevant la parole d'Elohîm que nous vous avons fait entendre, vous l'avez reçue non comme une parole humaine, mais comme ce qu'elle est vraiment : la parole d'Elohîm qui agit puissamment aussi en vous qui croyez.
2:14	En effet, frères, vous êtes devenus les imitateurs des assemblées d'Elohîm qui sont en Yéhoshoua Mashiah dans la Judée, parce que vous aussi, vous avez souffert de la part de ceux de votre propre nation les mêmes choses qu'elles ont souffertes de la part des Juifs,
2:15	qui ont même mis à mort le Seigneur Yéhoshoua et leurs propres prophètes, qui nous ont persécutés, qui ne plaisent pas à Elohîm et qui sont ennemis de tous les humains,
2:16	nous empêchant de parler aux nations afin qu'elles soient sauvées et mettent ainsi en tout temps le comble à leur péché. Mais à la fin la colère est venue sur eux.
2:17	Mais pour nous, frères, après avoir été quelque temps séparés de vous de corps et non de cœur, nous avons eu d'autant plus d'ardeur et d'empressement de voir votre face.
2:18	Nous avons donc voulu aller chez vous en effet, moi-même, Paulos, une et même deux fois, mais Satan nous en a empêchés.
2:19	Car quelle est notre espérance, ou notre joie, ou notre couronne de gloire ? N'est-ce pas vous qui l'êtes, devant notre Seigneur Yéhoshoua Mashiah, lors de son avènement ?
2:20	Car vous êtes notre gloire et notre joie.

## Chapitre 3

### La persévérance des Thessaloniciens dans l'affliction

3:1	C'est pourquoi, ne pouvant attendre davantage, nous avons trouvé bon de rester seuls à Athènes.
3:2	Et nous avons envoyé Timotheos, notre frère, serviteur d'Elohîm, et notre compagnon d'œuvre dans l'Évangile du Mashiah, pour vous affermir et vous exhorter au sujet de votre foi,
3:3	afin que personne ne soit troublé dans ces afflictions, puisque vous savez vous-mêmes que nous sommes destinés à cela.
3:4	Car aussi, lorsque nous étions avec vous, nous vous prédisions que nous aurions à souffrir des afflictions, comme cela est aussi arrivé, et vous le savez.
3:5	C'est pourquoi, comme je ne pouvais plus supporter cette attente, j'ai envoyé Timotheos pour connaître l'état de votre foi, de peur que le tentateur ne vous ait tentés en quelque sorte, et que notre travail ne soit devenu inutile.
3:6	Mais Timotheos étant revenu depuis peu de chez vous, nous a apporté la bonne nouvelle de votre foi et de votre amour, et nous a dit que vous conservez toujours un bon souvenir de nous, désirant nous voir comme nous désirons aussi vous voir.
3:7	À cause de cela, frères, nous avons été consolés à votre sujet par le moyen de votre foi, dans toute notre tribulation et notre détresse.
3:8	Car maintenant nous vivons, si vous demeurez fermes dans le Seigneur.
3:9	Car quelle action de grâce pourrions-nous rendre à Elohîm à votre sujet, pour toute la joie que nous éprouvons devant notre Elohîm, à cause de vous.
3:10	Nuit et jour nous lui demandons, avec une extrême instance de nous accorder de voir votre visage et de compléter<!--Compléter : du grec « katartizo » qui signifie « redresser », « ajuster », « compléter », « raccommoder » (ce qui a été abîmé), « réparer ». Ce verbe est également utilisé dans Mt. 4:21 lorsque Yaacov et Yohanan (Jacques et Jean) réparaient leurs filets. Le terme « katartismos » traduit par « perfectionnement » dans Ep. 4:11 vient de ce verbe. Ainsi, l'un des rôles de ces services est le perfectionnement des saints et non leur destruction.--> ce qui manque à votre foi.
3:11	Mais qu'Elohîm lui-même, notre Père, et notre Seigneur Yéhoshoua Mashiah, dirige<!--On constate que le verbe « diriger » est conjugué au singulier, y compris dans le texte original grec, ce qui atteste l'unité entre le Père et le Fils. Voir 2 Th. 2:16-17).--> notre chemin vers vous !
3:12	Et que le Seigneur vous fasse croître et abonder en amour les uns envers les autres et envers tous, comme il en est de nous envers vous !
3:13	Qu'il affermisse vos cœurs pour qu'ils soient irréprochables dans la sainteté, devant Elohîm qui est notre Père, lors de l'avènement de notre Seigneur Yéhoshoua Mashiah, accompagné de tous ses saints.

## Chapitre 4

### Exhortation à la sanctification et à l'amour fraternel

4:1	Au reste donc, frères, nous vous le demandons et nous vous y exhortons dans le Seigneur Yéhoshoua, que, comme vous avez appris de nous de quelle manière il vous faut marcher et plaire à Elohîm, vous y abondiez de plus en plus.
4:2	Car vous savez quels commandements nous vous avons donnés de la part du Seigneur Yéhoshoua.
4:3	Car la volonté d'Elohîm, c'est votre sanctification<!--La sanctification personnelle (1 Pi. 1:15-18 ; Hé. 12:14 ; Ap. 22:11). Chaque chrétien doit fournir un effort, en se servant quotidiennement de la parole d'Elohîm et de la prière, pour se maintenir dans la sanctification. Cela implique la séparation d'avec le mal et des mauvaises compagnies (2 Co. 6:14-18). La sanctification se développe au prix de nombreuses souffrances et de multiples sacrifices (Ro. 12:1-3).--> : que vous vous absteniez de relation sexuelle illicite,
4:4	que chacun de vous sache posséder son propre vase<!--« Vase » était une métaphore grecque commune pour « le corps » car les Grecs pensaient que l'âme vivait temporairement dans les corps.--> dans la sanctification et dans l'honneur,
4:5	non pas dans la passion<!--Voir Ro. 1:26.--> du désir, comme les nations qui ne connaissent pas Elohîm.
4:6	Que personne ne trompe ni ne dupe son frère en affaire, parce que le Seigneur punit toutes ces choses, comme nous vous l'avons dit et attesté.
4:7	Car Elohîm ne nous a pas appelés à l'impureté, mais à la sanctification.
4:8	C'est pourquoi celui qui rejette ceci ne rejette pas un être humain, mais Elohîm qui nous a aussi donné son Saint-Esprit.
4:9	Mais concernant l'amour fraternel, vous n'avez pas besoin que je vous en écrive, car vous êtes vous-mêmes enseignés par Elohîm à vous aimer les uns les autres,
4:10	car c'est aussi ce que vous faites à l'égard de tous les frères qui sont dans toute la Macédoine. Mais, mes frères, nous vous prions de vous perfectionner tous les jours davantage,
4:11	et à vous efforcer sérieusement de rester tranquilles, de vous occuper de vos propres affaires, et de travailler de vos propres mains, ainsi que nous vous l'avons ordonné,
4:12	en sorte que vous vous conduisiez d'une manière bienséante envers ceux du dehors et que vous n'ayez besoin de rien.

### L'Assemblée sera enlevée

4:13	Or mes frères, je ne veux pas que vous soyez dans l'ignorance au sujet de ceux qui dorment, afin que vous ne soyez pas attristés comme les autres qui n'ont pas d'espérance.
4:14	Car si nous croyons que Yéhoshoua est mort et qu'il est ressuscité, de même aussi ceux qui dorment en Yéhoshoua, Elohîm les ramènera avec lui.
4:15	Voici en effet ce que nous vous disons par la parole du Seigneur : nous, les vivants qui restons pour l'avènement du Seigneur, nous ne précéderons pas ceux qui dorment.
4:16	Parce que le Seigneur lui-même, avec un cri de commandement<!--L'expression « cri de commandement » vient du grec « keleuma », ce mot signifie un ordre, et en particulier un cri stimulant, comme celui que reçoit un animal pressé par un homme, tels les chevaux par les conducteurs de chariots, les chiens de chasse par les chasseurs, etc. ; ou par lequel un ordre est donné par le capitaine d'un navire, aux soldats par un chef, un appel de trompette. La sagesse d'Elohîm crie (Pr. 8). Yesha`yah (Ésaïe) devait crier à plein gosier (Es. 58:1). Le cri du Seigneur ne sera entendu que par l'Assemblée véritable qui est son épouse (Mt. 25:6).-->, avec une voix d'archange et avec la trompette d'Elohîm, descendra du ciel et les morts en Mashiah ressusciteront premièrement.
4:17	Ensuite, nous les vivants qui restons, nous serons enlevés ensemble avec eux dans les nuées à la rencontre du Seigneur dans les airs, et ainsi nous serons toujours avec le Seigneur.
4:18	C'est pourquoi consolez-vous les uns les autres par ces paroles.

## Chapitre 5

### Veiller en attendant le jour du Seigneur<!--Joë. 1:15.-->

5:1	Mais concernant les temps<!--Chronos est le temps physique. Il permet de segmenter le temps : passé, présent et futur.--> et les époques<!--Kairos est un temps métaphysique. Kairos n'est pas linéaire, il est qualitatif et se ressent. C'est le temps favorable.-->, frères, vous n'avez pas besoin qu'on vous en écrive,
5:2	car vous savez vous-mêmes précisément que le jour du Seigneur vient comme un voleur dans la nuit<!--Mt. 25:6 ; 2 Pi. 3:10 ; Ap. 3:3, 16:15.-->.
5:3	Car quand ils diront : Paix et sûreté ! alors une destruction soudaine les surprendra, comme les douleurs de l'enfantement surprennent la femme enceinte, et ils n'échapperont pas.
5:4	Mais quant à vous, frères, vous n'êtes pas dans les ténèbres pour que ce jour-là vous surprenne comme un voleur.
5:5	Vous êtes tous des enfants de la lumière<!--Les disciples de Yéhoshoua sont les enfants de la lumière (Jn. 12:36 ; Ep. 5:8). Yéhoshoua est la lumière du monde (Jn. 8:12).--> et des enfants du jour. Nous ne sommes pas de la nuit ni des ténèbres.
5:6	Ne dormons donc pas comme les autres, mais veillons et soyons sobres.
5:7	Car ceux qui dorment, dorment la nuit, et ceux qui s'enivrent sont ivres la nuit.
5:8	Mais nous qui sommes enfants du jour, soyons sobres, ayant revêtu la cuirasse de la foi et de l'amour, et ayant pour casque l'espérance du salut<!--Ro. 13:12 ; Ep. 6:14,17.-->.
5:9	Parce qu'Elohîm ne nous a pas destinés à la colère<!--La colère à venir. Voir 1 Th. 1:9-10.-->, mais à l'acquisition du salut par notre Seigneur Yéhoshoua Mashiah,
5:10	qui est mort pour nous, afin que soit que nous veillions, soit que nous dormions, nous vivions avec lui.
5:11	C'est pourquoi exhortez-vous réciproquement, et édifiez-vous tous, les uns les autres, comme aussi vous le faites.

### Le comportement du chrétien envers ses frères

5:12	Mais nous vous prions, frères, d'avoir de la considération pour ceux qui travaillent parmi vous, qui vous dirigent dans le Seigneur, et qui vous exhortent.
5:13	Et ayez pour eux beaucoup d'estime et d'amour<!--Littéralement « agape » : amour fraternel, affection.--> à cause de l'œuvre qu'ils font. Soyez en paix entre vous.
5:14	Mais nous vous en prions, frères, avertissez les désordonnés<!--Mt. 18:15 ; Ga. 6:1.-->, consolez ceux qui ont l'esprit abattu, supportez les faibles, et soyez patients envers tous.
5:15	Veillez à ce que personne ne rende le mal pour le mal<!--Mt. 5:44 ; Ro. 12:21.-->, mais poursuivez toujours ce qui est bon, les uns envers les autres comme envers tout le monde.

### Le comportement du chrétien en tout temps

5:16	Soyez toujours joyeux.
5:17	Priez sans cesse.
5:18	Rendez grâces pour toutes choses, car c'est la volonté d'Elohîm par Yéhoshoua Mashiah.
5:19	N'éteignez pas l'Esprit.
5:20	Ne méprisez pas les prophéties.
5:21	Éprouvez toutes choses<!--Voir 1 Ti. 3:10.-->, retenez ce qui est bon.
5:22	Abstenez-vous de toute forme de mal.
5:23	Mais que l'Elohîm de paix veuille vous sanctifier entièrement, et faire que votre être entier, l'esprit, l'âme et le corps soient conservés sans reproche lors de l'avènement de notre Seigneur Yéhoshoua Mashiah<!--L'avènement du Seigneur. Voir Mt. 24:1-3.--> !
5:24	Celui qui vous appelle est fidèle, c'est pourquoi il fera ces choses en vous.

### Salutations

5:25	Mes frères, priez pour nous.
5:26	Saluez tous les frères par un saint baiser.
5:27	Je vous en conjure par le Seigneur que cette épître soit lue à tous les saints frères.
5:28	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous ! Amen !
