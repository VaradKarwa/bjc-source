# Titos (Tite) (Tit.)

Signification : Nourrice, honorable

Auteur : Paulos (Paul)

Thème : L'ordre dans les assemblées

Date de rédaction : Env. 65 ap. J.-C.

Cette épître pastorale fut écrite après la libération de Paulos (Paul) de sa première captivité romaine, peut-être dans la ville de Philippes. Titos (Tite), disciple d'origine païenne et collaborateur de Paulos, se trouvait alors en Crète où Paulos l'avait laissé, afin qu'il organise les assemblées. Dans cette lettre, Paulos traite des conditions requises pour assumer la charge d'ancien en mettant l'accent sur la saine doctrine. Mentionnant également les obligations morales des jeunes, des personnes âgées, ainsi que des serviteurs, il exhorte Titos à veiller et à s'éloigner des apostats.

## Chapitre 1

### Introduction

1:1	Paulos, esclave d'Elohîm, et apôtre de Yéhoshoua Mashiah, selon la foi des élus d'Elohîm et la connaissance précise et correcte de la vérité qui est selon la piété,
1:2	dans l'espérance de la vie éternelle, qu'Elohîm qui ne peut mentir avait promise avant les temps éternels,
1:3	mais qu'il a manifestée en son propre temps par sa parole, dans la prédication qui m'a été confiée, selon le mandat<!--Voir 1 Ti. 1:1.--> d'Elohîm, notre Sauveur.
1:4	À Titos, mon vrai fils, selon notre commune foi : que la grâce, la miséricorde et la paix te soient données de la part d'Elohîm le Père et du Seigneur Yéhoshoua Mashiah, notre Sauveur !

### Les caractéristiques d'un ancien

1:5	La raison pour laquelle je t'ai laissé en Crète, c'est afin que tu achèves de mettre en ordre les choses qui sont laissées de côté<!--Ou les choses qui traînent, qui manquent.-->, et que tu établisses des anciens de ville en ville, comme je te l'ai ordonné,
1:6	s'il s'y trouve un homme qui soit irréprochable, mari d'une seule femme, ayant des enfants fidèles, qui ne soient ni accusés de libertinage, ni rebelles.
1:7	Car il faut que le surveillant soit irréprochable, comme gestionnaire d'Elohîm. Qu'il ne soit ni arrogant, ni coléreux, ni un ivrogne, ni violent, ni ardent au gain.
1:8	Mais hospitalier, aimant le bien, fléchissant ses désirs et impulsions, juste, saint, maître de lui-même,
1:9	attaché à la parole fidèle telle qu'elle lui a été enseignée, afin qu'il soit capable tant d'exhorter par la saine doctrine, que de réfuter ceux qui la contredisent<!--Lu. 2:34 ; Jn. 19:12 ; Ac. 13:45, 28:19, 28:22 ; Ro. 10:21.-->.
1:10	Car il y en a beaucoup qui ne veulent pas se soumettre, vains discoureurs et séducteurs d'esprits, principalement ceux de la circoncision,
1:11	auxquels il faut fermer la bouche, eux qui renversent<!--Voir 2 Ti. 2:18.--> des maisons tout entières, enseignant ce qu'il ne faut pas, en vue d'un gain obscène.
1:12	Quelqu'un d'entre eux, leur propre prophète, a dit : Les Crétois sont toujours menteurs, de mauvaises bêtes, des ventres paresseux.
1:13	Ce témoignage est véritable. C'est pourquoi reprends-les sévèrement, afin qu'ils deviennent sains dans la foi,
1:14	et qu'ils ne s'attachent pas aux fables<!--Voir 1 Ti. 1:4 ; 2 Ti. 4:4 ; 2 Pi. 1:16.--> judaïques et aux commandements d'humains qui se détournent de la vérité.
1:15	Toutes choses sont en effet pures pour ceux qui sont purs, mais rien n'est pur pour ceux qui sont impurs et incrédules ; mais leur pensée et leur conscience sont souillées.
1:16	Ils font profession de connaître Elohîm, mais ils le renient par leurs œuvres, car ils sont abominables, rebelles, et réprouvés à l'égard de toute bonne œuvre.

## Chapitre 2

### Recommandations de Paulos (Paul)

2:1	Mais toi, annonce les choses qui conviennent à la saine doctrine.
2:2	Que les vieillards soient sobres, honorables, fléchissant leurs désirs et impulsions, sains dans la foi, dans l'amour et dans la persévérance.
2:3	De même, que les femmes âgées règlent leur extérieur d'une manière convenable à la sainteté. Qu'elles ne soient ni calomniatrices<!--Ou diable. Voir Mt. 4:1.-->, ni esclaves des excès de vin, mais qu'elles enseignent ce qui est bon,
2:4	afin qu'elles exhortent sérieusement les jeunes femmes à être modestes, à aimer leurs maris, à aimer leurs enfants,
2:5	fléchissant leurs désirs et impulsions, pures, occupées aux soins domestiques, bonnes, soumises à leurs maris, afin que la parole d'Elohîm ne soit pas blasphémée.
2:6	Exhorte aussi les jeunes hommes à être modérés,
2:7	te montrant toi-même en toutes choses un exemple de bonnes œuvres, par une doctrine incorruptible, par la pureté et la sainteté,
2:8	par une parole saine, qui ne peut être censurée, afin que celui qui vous contredit soit confondu, n'ayant aucun mal à dire de vous.
2:9	Que les esclaves soient soumis à leurs maîtres, qu'ils leurs plaisent en toutes choses, n'étant pas contredisants,
2:10	ne détournant rien, mais faisant toujours paraître une grande fidélité, afin de rendre honorable en toutes choses la doctrine d'Elohîm, notre Sauveur.
2:11	Car la grâce d'Elohîm qui apporte le salut à tous les humains est apparue.
2:12	Et elle nous enseigne à renoncer à l'impiété et aux convoitises mondaines, et à vivre dans l'âge présent d'une manière tempérée, juste et pieuse,
2:13	en attendant l'espérance bénie et l'apparition de la gloire<!--Et quand le Fils de l'homme viendra dans sa gloire et accompagné de tous les saints anges, alors il s'assiéra sur le trône de sa gloire. Mt. 25:31.--> de Yéhoshoua Mashiah notre grand Elohîm et Sauveur,
2:14	qui s'est donné lui-même pour nous, afin de nous racheter de toute violation de la torah et de nous purifier, pour se faire un peuple qui lui appartienne en propre, zélateur des bonnes œuvres.
2:15	Enseigne ces choses, exhorte et reprends avec un mandat<!--Voir 1 Ti. 1:1.--> entier. Et que personne ne te méprise.

## Chapitre 3

### Conseils de Paulos (Paul)

3:1	Rappelle-leur d'être soumis aux chefs et aux autorités, d'obéir, d'être prêts à faire toutes sortes de bonnes actions,
3:2	de ne calomnier personne, d'être non agressifs, doux, et montrant une parfaite douceur envers tous les humains.
3:3	Car nous aussi, nous étions autrefois dénués d'intelligence, rebelles, égarés, esclaves de divers désirs et plaisirs, vivant dans la malice et dans l'envie, dignes d'être haïs, et nous haïssant les uns les autres.
3:4	Mais, quand la bonté d'Elohîm notre Sauveur et son amour pour les humains sont apparus, il nous a sauvés,
3:5	non par des œuvres de justice que nous aurions faites, mais selon la miséricorde, à travers le bain<!--Le mot grec « loutron » signifie également « baignade » ou « action de baigner ». Voir Ep. 5:26.--> de la nouvelle naissance<!--Le mot grec « paliggenesia » signifie « nouvelle naissance, renouvellement, re-création ou régénération ». Voir Mt. 19:28. Ce mot était souvent utilisé pour dénoter la restauration d'une chose à son état d'origine, sa rénovation, comme un renouveau ou une restauration de la vie après la mort. Le renouvellement du monde qui doit avoir lieu après sa destruction par le feu, au dire des Stoïciens.--> et le renouvellement<!--Le mot grec « anakainosis » traduit par renouvellement signifie aussi « renouveau », « rénovation » ou « changement complet vers le meilleur ». Voir Ro. 12:2.--> du Saint-Esprit,
3:6	qu'il a répandu abondamment sur nous par le moyen de Yéhoshoua le Mashiah notre Sauveur,
3:7	afin qu'ayant été justifiés par sa grâce, nous soyons les héritiers de la vie éternelle selon notre espérance.
3:8	Cette parole est certaine, et je veux que tu affirmes ces choses, afin que ceux qui ont cru en Elohîm aient soin principalement de s'appliquer à pratiquer les bonnes œuvres. Voilà les choses qui sont bonnes et utiles aux humains.
3:9	Mais évite les débats insensés<!--Voir 2 Ti. 2:23.-->, les généalogies, les querelles et les disputes au sujet de la torah, car elles sont inutiles et vaines.
3:10	Évite l'homme hérétique, après un premier et un second avertissement,
3:11	sachant qu'un homme de cette espèce est perverti et qu'il pèche en se condamnant lui-même.

### Salutations

3:12	Quand je t'enverrai Artémas ou Tuchikos, hâte-toi de venir vers moi à Nicopolis, car j'ai résolu d'y passer l'hiver.
3:13	Accompagne soigneusement Zénas<!--« Don de Zeus (Jupiter) ».-->, docteur de la torah, et Apollos, afin que rien ne leur manque.
3:14	Que les nôtres aussi apprennent à être les premiers à s'appliquer aux bonnes œuvres, de manière à subvenir aux besoins urgents, afin qu'ils ne soient pas sans fruits.
3:15	Tous ceux qui sont avec moi te saluent. Salue ceux qui nous aiment dans la foi. Grâce soit avec vous tous ! Amen !
