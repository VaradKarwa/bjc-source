# 2 Shemouél (2 Samuel) (2 S.)

Signification : Entendu, exaucé de El

Auteur : Inconnu

Thème : Le règne de David

Date de rédaction : 10ème siècle av. J.-C.

Suite du premier livre de Shemouél (1 Samuel), ce livre commence par le récit de la mort de Shaoul (Saül) et l'accession progressive de David à la royauté. La faveur d'Elohîm dans sa vie lui donna du succès et lui permit d'étendre son royaume jusqu'au nord de Damas. Au détriment de sa piété et de son alliance avec Elohîm, David commit de lourdes erreurs. Il s'en repentit sincèrement, mais il dut en assumer les conséquences.

## Chapitre 1

### Attitude de David à la mort de Shaoul (Saül)

1:1	Et il arriva, après la mort de Shaoul, quand David fut revenu après avoir battu les Amalécites, que David resta deux jours à Tsiklag.
1:2	Le troisième jour, un homme arriva du camp de Shaoul, avec ses vêtements déchirés et de la terre sur sa tête. Il se présenta à David, se jeta par terre et se prosterna.
1:3	David lui dit : D'où viens-tu ? Il lui répondit : Je me suis échappé du camp d'Israël.
1:4	David lui dit : Comment la chose s'est-elle passée ? Je te prie, raconte-moi ! Il répondit : Le peuple s'est enfui de la bataille, et il y a eu beaucoup parmi le peuple qui sont tombés morts ; Shaoul aussi et Yehonathan, son fils, sont morts.
1:5	David dit à ce jeune homme qui lui disait ces nouvelles : Comment sais-tu que Shaoul et Yehonathan, son fils, sont morts ?
1:6	Le jeune homme qui lui disait ces nouvelles lui répondit : Je me trouvais, je me trouvais sur la Montagne de Guilboa, et voici Shaoul était appuyé sur sa lance, et voici les chars et les maîtres cavaliers l'avaient rattrapé.
1:7	S'étant retourné, il m'aperçut et m'appela. Je lui répondis : Me voici !
1:8	Il me dit : Qui es-tu ? Je lui répondis : Je suis Amalécite.
1:9	Et il dit : Approche-toi de moi et tue-moi, car je suis dans une grande angoisse, et ma vie est encore toute en moi.
1:10	Je m'approchai de lui et je lui donnai la mort<!--Cet homme amalécite a peut-être menti afin de gagner la faveur de David (1 S. 31:3-5).-->, sachant bien qu'il ne survivrait après s'être jeté sur sa lance. J'ai pris la couronne qu'il avait sur sa tête, et le bracelet qui était à son bras, et je les apporte ici à mon seigneur.
1:11	Alors David saisit ses vêtements et les déchira, et tous les hommes qui étaient avec lui firent de même.
1:12	Ils furent dans le deuil, ils pleurèrent et ils jeûnèrent jusqu'au soir, à cause de Shaoul, de Yehonathan, son fils, à cause du peuple de YHWH, et de la maison d'Israël, parce qu'ils étaient tombés par l'épée.
1:13	David dit au jeune homme qui lui avait apporté ces nouvelles : D'où es-tu ? Et il répondit : Je suis fils d'un homme étranger, d'un Amalécite.
1:14	David lui dit : Comment n'as-tu pas craint d'avancer ta main pour tuer le mashiah de YHWH ?
1:15	Et David appela l'un de ses serviteurs et lui dit : Approche-toi, jette-toi sur lui ! Ce dernier le frappa et il mourut.
1:16	Et David lui dit : Que ton sang retombe sur ta tête, car ta bouche a témoigné contre toi, en disant : J'ai fait mourir le mashiah de YHWH !

### Chant funèbre de David

1:17	Alors David chanta ce chant funèbre sur Shaoul et sur Yehonathan, son fils,
1:18	qu'il ordonna d'enseigner aux fils de Yéhouda. Arc. C'est écrit dans le livre du Juste.
1:19	L'élite d'Israël a succombé sur tes collines ! Comment des hommes vaillants sont-ils tombés ?
1:20	Ne l'annoncez pas dans Gath et n'en publiez pas la nouvelle dans les rues d'Askalon, de peur que les filles des Philistins ne se réjouissent, de peur que les filles des incirconcis n'en tressaillent de joie.
1:21	Montagnes de Guilboa ! qu'il n'y ait sur vous ni rosée, ni pluie, ni champs qui donnent des prémices pour les offrandes ! Car là ont été jetés les boucliers des hommes vaillants, le bouclier de Shaoul. L'huile a cessé de les oindre.
1:22	L'arc de Yehonathan ne reculait pas devant le sang des blessés et devant la graisse des hommes vaillants, et l'épée de Shaoul ne retournait pas à vide.
1:23	Shaoul et Yehonathan, aimables et agréables pendant leur vie, n'ont pas été séparés dans leur mort. Ils étaient plus légers que les aigles, ils étaient plus forts que des lions.
1:24	Filles d'Israël ! Pleurez sur Shaoul, qui vous revêtait magnifiquement de cramoisi, qui mettait des ornements d'or à vos habits.
1:25	Comment les hommes vaillants sont-ils tombés au milieu du combat ? Comment Yehonathan a-t-il été tué sur tes collines ?
1:26	Yehonathan, mon frère, je suis dans la douleur à cause de toi ! Tu étais pour moi très charmant. L'amour que j'avais pour toi était plus merveilleux que l'amour des femmes.
1:27	Comment sont tombés les hommes vaillants ? Comment se sont perdus les instruments de guerre ?

## Chapitre 2

### David oint roi de Yéhouda

2:1	Et il arriva après cela que David consulta YHWH, en disant : Monterai-je dans l'une des villes de Yéhouda ? YHWH lui répondit : Monte. David dit : Où monterai-je ? YHWH répondit : À Hébron.
2:2	David y monta, avec ses deux femmes, Achinoam de Yizre`e'l, et Abigaïl de Carmel, qui avait été femme de Nabal.
2:3	David fit monter aussi les hommes qui étaient avec lui, chaque homme avec sa famille. Ils habitèrent dans les villes d'Hébron.
2:4	Les hommes de Yéhouda vinrent, et là ils oignirent David pour roi sur la maison de Yéhouda. On fit un rapport à David en disant : Les hommes de Yabesh en Galaad ont enseveli Shaoul.
2:5	Alors David envoya des messagers vers les hommes de Yabesh en Galaad, pour leur dire : Soyez bénis de YHWH, puisque vous avez fait preuve d'une telle bonté envers Shaoul, votre seigneur, et que vous l'avez enseveli.
2:6	Maintenant donc que YHWH use envers vous de bonté et de fidélité. Moi aussi je vous ferai du bien, parce que vous avez agi de la sorte.
2:7	Et maintenant, que vos mains se fortifient ! Soyez des fils talentueux, car Shaoul votre seigneur est mort et la maison de Yéhouda m'a oint pour être roi sur elle.

### Ish-Bosheth établi roi d'Israël

2:8	Cependant Abner, fils de Ner, chef de l'armée de Shaoul, prit Ish-Bosheth, fils de Shaoul, et le fit passer à Mahanaïm.
2:9	Il l'établit roi sur Galaad, sur les Guéshouriens, sur Yizre`e'l, sur Éphraïm, sur Benyamin et sur tout Israël.
2:10	Ish-Bosheth, fils de Shaoul, était âgé de 40 ans quand il commença à régner sur Israël, et il régna 2 ans. Il n'y eut que la maison de Yéhouda qui suivit David.
2:11	Le nombre de jours pendant lesquels David régna à Hébron sur la maison de Yéhouda fut de 7 ans et 6 mois.

### Guerre entre Yéhouda et Israël

2:12	Abner, fils de Ner, et les serviteurs d'Ish-Bosheth, fils de Shaoul, sortirent de Mahanaïm pour marcher vers Gabaon.
2:13	Yoab, fils de Tserouyah, et les serviteurs de David, sortirent aussi. Ils se rencontrèrent ensemble près de l'étang de Gabaon, et les uns se tinrent d'un côté de l'étang, et les autres du côté opposé de l'étang.
2:14	Alors Abner dit à Yoab : Que ces jeunes hommes se lèvent maintenant et qu'ils se battent devant nous ! Et Yoab répondit : Qu'ils se lèvent !
2:15	Ils se levèrent donc, et s'avancèrent en nombre égal, 12 pour Benyamin et pour Ish-Bosheth, fils de Shaoul, et 12 parmi les serviteurs de David.
2:16	Alors, chaque homme saisissant son adversaire par la tête, lui enfonça son épée dans le flanc, et ils tombèrent tous ensemble. Et l'on donna à ce lieu, qui est près de Gabaon, le nom de Helqath-Hatsourim<!--Helqath-Hatsourim signifie « champ des épées ».-->.
2:17	Il y eut ce jour-là un combat très rude, dans lequel Abner et les hommes d'Israël furent battus par les serviteurs de David.
2:18	Les trois fils de Tserouyah, Yoab, Abishaï et Asaël étaient là. Asaël avait les pieds légers comme une gazelle des champs.
2:19	Asaël poursuivit Abner, sans se détourner de lui ni à droite ni à gauche.
2:20	Abner regarda derrière lui et dit : Est-ce toi, Asaël ? Et il répondit : C'est moi.
2:21	Abner lui dit : Détourne-toi à droite ou à gauche ! Saisis-toi de l'un de ces jeunes hommes et prends sa dépouille. Mais Asaël ne voulut pas se détourner de lui.
2:22	Et Abner dit encore à Asaël : Détourne-toi de moi ! Pourquoi te frapperais-je et t'abattrais-je à terre ? Comment ensuite lèverais-je le visage devant ton frère Yoab ?
2:23	Mais Asaël refusa de se détourner. Abner le frappa au ventre avec l'extrémité inférieure de sa lance, qui sortit par-derrière. Il tomba là, raide mort sur place. Tous ceux qui arrivaient au lieu où Asaël était tombé mort, s'y arrêtaient.
2:24	Yoab et Abishaï poursuivirent Abner, et le soleil se couchait quand ils arrivèrent au coteau d'Amma, qui est en face de Guiach, sur le chemin du désert de Gabaon.
2:25	Les fils de Benyamin se rassemblèrent auprès d'Abner et formèrent un corps de troupe et ils s'arrêtèrent sur le sommet d'une colline.
2:26	Alors Abner appela Yoab et dit : L'épée dévorera-t-elle sans cesse ? Ne sais-tu pas qu'il y aura de l'amertume à la fin ? Jusqu'à quand tarderas-tu à dire au peuple qu'il cesse de poursuivre ses frères ?
2:27	Yoab répondit : Elohîm est vivant ! Si tu n'avais parlé ainsi, le peuple n'aurait pas cessé avant le matin de poursuivre ses frères.
2:28	Yoab sonna du shofar et tout le peuple s'arrêta. Ils ne poursuivirent plus Israël et ils ne continuèrent plus à se battre.
2:29	Ainsi, Abner et ses hommes marchèrent toute la nuit dans la région aride. Ils passèrent le Yarden, traversèrent tout le Bithron, et arrivèrent à Mahanaïm.
2:30	Yoab aussi revint de la poursuite d'Abner et rassembla tout le peuple. Il manquait 19 hommes parmi les serviteurs de David, et Asaël.
2:31	Mais les serviteurs de David avaient frappé à mort 360 hommes de Benyamin et des hommes d'Abner.
2:32	Ils emportèrent Asaël et l'ensevelirent dans le sépulcre de son père à Bethléhem. Yoab et ses hommes marchèrent toute la nuit et arrivèrent à Hébron au point du jour.

## Chapitre 3

### L'autorité de David s'accroît<!--1 Ch. 3:1-4.-->

3:1	Or il y eut une longue guerre entre la maison de Shaoul et la maison de David. David devenait de plus en plus fort, et la maison de Shaoul allait en s'affaiblissant.
3:2	Il naquit à David des fils à Hébron. Son premier-né fut Amnon, d'Achinoam de Yizre`e'l ;
3:3	le second, Kileab, d'Abigaïl de Carmel, femme de Nabal ; le troisième, Absalom, fils de Ma'akah, fille de Talmaï, roi de Guéshour ;
3:4	le quatrième, Adoniyah, fils de Haggith ; le cinquième, Shephatyah, fils d'Abithal ;
3:5	et le sixième, Yithream, d'Églah, femme de David. Ce sont là ceux qui naquirent à David à Hébron.

### Abner fait alliance avec David

3:6	Et il arriva que pendant la guerre entre la maison de Shaoul et la maison de David, Abner tint ferme pour la maison de Shaoul.
3:7	Or Shaoul avait eu une concubine, nommée Ritspa, fille d'Ayah. Et Ish-Bosheth dit à Abner : Pourquoi es-tu venu vers la concubine de mon père ?
3:8	Abner fut très irrité à cause du discours d'Ish-Bosheth, et il lui dit : Suis-je une tête de chien, au service de Yéhouda ? Je fais aujourd'hui preuve de bonté envers la maison de Shaoul, ton père, envers ses frères et ses amis, je ne t'ai pas livré entre les mains de David, et c'est aujourd'hui que tu me reproches une faute avec cette femme ?
3:9	Qu'ainsi Elohîm traite Abner et qu'ainsi il y ajoute, si je n'agis pas avec David selon ce que YHWH a juré à David,
3:10	en disant qu'il ferait passer la royauté de la maison de Shaoul à la sienne, et qu'il établirait le trône de David sur Israël et sur Yéhouda depuis Dan jusqu'à Beer-Shéba.
3:11	Ish-Bosheth n'osa pas répondre un seul mot à Abner, parce qu'il le craignait.
3:12	Abner envoya des messagers à David pour lui dire de sa part : À qui est le pays ? Fais alliance avec moi, et voici, ma main sera avec toi, pour tourner vers toi tout Israël.
3:13	David répondit : Je le veux bien ! Je ferai alliance avec toi. Je te demande seulement une chose, c'est que tu ne voies pas ma face, à moins que tu n'amènes d'abord Miykal, fille de Shaoul, quand tu viendras me voir.
3:14	Et David envoya des messagers à Ish-Bosheth, fils de Shaoul, pour lui dire : Rends-moi ma femme Miykal, que j'ai épousée pour 100 prépuces des Philistins.
3:15	Ish-Bosheth l'envoya prendre chez son mari Palthiel, fils de Laïsh.
3:16	Et son mari la suivit, marchant et pleurant continuellement après elle jusqu'à Bahourim. Alors Abner lui dit : Va, retourne-t'en ! Et il s'en retourna.
3:17	Abner parla aux anciens d'Israël et leur dit : Vous désiriez autrefois avoir David pour roi.
3:18	Maintenant agissez ! Car YHWH a parlé de David et a dit : C'est par David, mon serviteur, que je délivrerai mon peuple d'Israël de la main des Philistins et de la main de tous ses ennemis.
3:19	Abner parla aussi aux oreilles de ceux de Benyamin. Puis Abner alla rapporter aux oreilles de David à Hébron, tout ce qui semblait bon aux yeux d'Israël et aux yeux de toute la maison de Benyamin.
3:20	Abner vint donc vers David à Hébron, accompagné de 20 hommes, et David fit un festin à Abner et aux hommes qui étaient avec lui.
3:21	Abner dit à David : Je me lèverai et je partirai pour rassembler tout Israël auprès du roi, mon seigneur. Ils feront alliance avec toi, et tu régneras selon le désir de ton âme. David renvoya Abner, qui s'en alla en paix.
3:22	Voici, les serviteurs de David et Yoab revinrent d'une excursion et amenèrent avec eux un grand butin. Abner n'était plus avec David à Hébron, car David l'avait renvoyé, et il s'en était allé en paix.
3:23	Lorsque Yoab et toute l'armée qui était avec lui revinrent, on fit ce rapport à Yoab en ces mots : Abner, fils de Ner, est venu auprès du roi, qui l'a renvoyé, et il s'en est allé en paix.
3:24	Yoab vint vers le roi, et dit : Qu'as-tu fait ? Voici, Abner est venu vers toi. Pourquoi l'as-tu renvoyé et a-t-il pu s'en aller, s'en aller ?
3:25	Tu connais Abner, fils de Ner ! C'est pour te tromper qu'il est venu, pour épier tes démarches, tes allées et venues, et pour savoir tout ce que tu fais.
3:26	Puis Yoab, après avoir quitté David, envoya sur les traces d'Abner des messagers, qui le ramenèrent de la fosse de Sira, sans que David n'en sache rien.

### Mort d'Abner

3:27	Lorsque Abner revint à Hébron, Yoab le tira à l'écart au milieu de la porte, comme pour lui parler en secret. Là il le frappa au ventre et le tua pour venger la mort de son frère Asaël.
3:28	David apprit ce qui était arrivé, et dit : Je suis à jamais innocent, mon royaume et moi, devant YHWH, du sang d'Abner, fils de Ner.
3:29	Que ce sang retombe sur la tête de Yoab et sur toute la maison de son père ! Que soit retranchée la maison de Yoab, qu'il y ait toujours un homme qui soit atteint d'un flux ou de la lèpre, ou qui s'appuie sur un bâton, ou qui tombe par l'épée, ou qui manque de pain !
3:30	Ainsi Yoab et Abishaï, son frère, tuèrent Abner, parce qu'il avait tué Asaël, leur frère, à Gabaon, dans la bataille.
3:31	David dit à Yoab et à tout le peuple qui était avec lui : Déchirez vos vêtements, ceignez-vous de sacs et menez le deuil en marchant devant Abner ! Et le roi David marcha derrière le cercueil.
3:32	On ensevelit Abner à Hébron. Le roi éleva la voix et pleura sur la tombe d'Abner, et tout le peuple pleura.
3:33	Le roi chanta un chant funèbre sur Abner et dit : Abner devait-il mourir comme meurt un insensé ?
3:34	Tes mains n'étaient pas liées, et tes pieds n'étaient pas mis dans des chaînes ! Tu es tombé comme on tombe devant les injustes. Et tout le peuple recommença à pleurer sur Abner.
3:35	Puis tout le peuple vint pour faire prendre quelque nourriture à David, pendant qu'il était encore jour, mais David jura, en disant : Qu'ainsi me traite Elohîm et qu'ainsi il y ajoute, si je goûte du pain ou quelque chose d'autre avant le coucher du soleil !
3:36	Tout le peuple l'entendit et cela fut bon à ses yeux. Comme tout ce que faisait le roi, cela fut bon aux yeux de tout le peuple.
3:37	En ce jour, tout le peuple et tout Israël surent que ce n'était pas par ordre du roi qu'Abner, fils de Ner, avait été tué.
3:38	Le roi dit à ses serviteurs : Ne savez-vous pas qu'un chef, un grand homme, est tombé aujourd'hui en Israël ?
3:39	Je suis encore faible aujourd'hui, bien que j'aie été oint roi, et ces hommes, les fils de Tserouyah, sont trop puissants pour moi. Que YHWH rende à celui qui fait le mal selon sa méchanceté !

## Chapitre 4

### Mort d'Ish-Bosheth

4:1	Quand le fils de Shaoul apprit qu'Abner était mort à Hébron, ses mains restèrent sans force, et tout Israël fut dans l'épouvante.
4:2	Il y avait deux hommes, chefs de bandes du fils de Shaoul. Le nom de l'un était Baana, et le nom du second Récab : ils étaient fils de Rimmon de Beéroth, d'entre les fils de Benyamin. Car Beéroth était considérée comme appartenant à Benyamin,
4:3	et les Beérothiens s'étaient enfuis à Guitthaïm, où ils y ont habité jusqu'à ce jour.
4:4	Yehonathan, fils de Shaoul, avait un fils perclus des pieds ; il était âgé de 5 ans lorsque la nouvelle de la mort de Shaoul et de Yehonathan arriva de Yizre`e'l ; sa nourrice le prit et s'enfuit, et comme elle se hâtait de fuir, il tomba et devint boiteux ; son nom était Mephibosheth.
4:5	Les fils de Rimmon de Beéroth, Récab et Baana, se rendirent pendant la chaleur du jour à la maison d'Ish-Bosheth, qui était couché pour son repos du midi.
4:6	Ils entrèrent jusqu'au milieu de la maison pour y prendre du froment et le frappèrent à la cinquième côte. Puis Récab et Baana son frère se sauvèrent.
4:7	Ils entrèrent donc dans la maison pendant qu'Ish-Bosheth était couché sur son lit dans sa chambre à coucher. Ils le frappèrent et le firent mourir, et ils lui coupèrent la tête. Ils prirent sa tête et ils marchèrent toute la nuit par le chemin de la région aride.
4:8	Ils apportèrent la tête d'Ish-Bosheth à David dans Hébron, et ils dirent au roi : Voici la tête d'Ish-Bosheth, fils de Shaoul, ton ennemi, qui en voulait à ta vie. YHWH venge aujourd'hui le roi mon seigneur, de Shaoul et de sa race.
4:9	Mais David répondit à Récab et à Baana, son frère, fils de Rimmon de Beéroth, et leur dit : YHWH, qui a délivré mon âme de toute angoisse, est vivant !
4:10	J'ai saisi celui qui est venu m'annoncer et me dire : Voilà, Shaoul est mort, et qui pensait m'apprendre de bonnes nouvelles, je l'ai fait saisir et tuer à Tsiklag, pour lui donner le salaire de ses bonnes nouvelles ;
4:11	combien plus, quand des hommes méchants ont tué un homme juste dans sa maison et sur sa couche, ne redemanderai-je pas maintenant son sang de vos mains et ne vous exterminerai-je pas de la terre ?
4:12	David ordonna à ses jeunes hommes de les tuer. Ils leur coupèrent les mains et les pieds, et les pendirent près de l'étang d'Hébron. Ils prirent la tête d'Ish-Bosheth et l'ensevelirent dans le sépulcre d'Abner à Hébron.

## Chapitre 5

### David oint roi sur tout Israël<!--1 Ch. 11:1-3.-->

5:1	Alors toutes les tribus d'Israël vinrent auprès de David à Hébron et dirent : Voici, nous sommes tes os et ta chair.
5:2	Aussi hier et aussi avant-hier, quand Shaoul était roi sur nous, c'est toi qui conduisais et qui ramenais Israël. YHWH t'a dit : Tu paîtras mon peuple d'Israël, et tu seras le chef d'Israël.
5:3	Tous les anciens d'Israël vinrent donc vers le roi à Hébron, et le roi David fit alliance avec eux à Hébron, devant YHWH. Ils oignirent David pour roi sur Israël.
5:4	David était âgé de 30 ans lorsqu'il commença à régner, il régna 40 ans.
5:5	Il régna sur Yéhouda à Hébron 7 ans et 6 mois, puis il régna 33 ans à Yeroushalaim sur tout Israël et Yéhouda.

### Yeroushalaim (Jérusalem), capitale de tout Israël<!--1 Ch. 11:4-9.-->

5:6	Le roi marcha avec ses gens sur Yeroushalaim contre les Yebousiens qui habitaient ce pays. Ils dirent à David : Tu n'entreras pas ici, car les aveugles mêmes et les boiteux te repousseront ! Ce qui voulait dire : David n'entrera pas ici.
5:7	Mais David s'empara de la forteresse de Sion : c'est la cité de David.
5:8	David avait dit en ce jour-là : Quiconque battra les Yebousiens et atteindra le canal, ces aveugles et ces boiteux, qui sont haïs de l'âme de David, sera récompensé... C'est pourquoi l'on dit : Aucun aveugle ni boiteux n'entrera dans cette maison.
5:9	Et David habita dans la forteresse et l'appela la cité de David. Il bâtit tout autour, depuis Millo jusqu'au-dedans.
5:10	David devenait de plus en plus grand, et YHWH, Elohîm Sabaoth, était avec lui.

### YHWH affermit le règne de David

5:11	Hiram, roi de Tyr, envoya des messagers à David, du bois de cèdre, des charpentiers et des tailleurs de pierres à bâtir, et ils bâtirent la maison de David.
5:12	David reconnut que YHWH l'affermissait comme roi sur Israël, et qu'il élevait son royaume à cause de son peuple d'Israël.

### Fils de David nés à Yeroushalaim (Jérusalem)<!--2 S. 3:2-5 ; 1 Ch. 3:1-4.-->

5:13	David prit encore des concubines et des femmes de Yeroushalaim, après qu'il fut venu d'Hébron, et il lui naquit encore des fils et des filles.
5:14	Voici les noms de ceux qui lui naquirent à Yeroushalaim : Shammoua, Shobad, Nathan, Shelomoh,
5:15	Yibhar, Élishoua, Népheg, Yaphiya,
5:16	Éliyshama, Éliada et Éliphéleth.

### YHWH livre les Philistins à David<!--2 S. 23:13-17 ; 1 Ch. 14:8-17, 11:15-19, 12:8-15.-->

5:17	Or quand les Philistins apprirent qu'on avait oint David pour roi sur Israël, ils montèrent tous pour chercher David. Et David l'ayant appris, il descendit vers la forteresse.
5:18	Les Philistins arrivèrent et se répandirent dans la vallée des géants.
5:19	Alors David consulta YHWH, en disant : Monterai-je contre les Philistins ? Les livreras-tu entre mes mains ? Et YHWH parla à David : Monte, car je livrerai, je livrerai les Philistins entre tes mains.
5:20	Alors David vint à Baal-Peratsim, où il les battit. Puis il dit : YHWH a dispersé mes ennemis devant moi, comme des eaux qui s'écoulent. C'est pourquoi il appela ce lieu du nom de Baal-Peratsim.
5:21	Ils laissèrent là leurs faux elohîm, et David et ses hommes les emportèrent.
5:22	Les Philistins montèrent encore une autre fois et se répandirent dans la vallée des géants.
5:23	David consulta YHWH. Et YHWH dit : Tu ne monteras pas ! Contourne-les par-derrière, et tu les atteindras vis-à-vis des mûriers.
5:24	Et il arrivera que quand tu entendras le bruit d’une marche au sommet des mûriers, alors hâte-toi, car c'est YHWH qui sort devant toi pour battre l'armée des Philistins.
5:25	David fit ce que YHWH lui avait ordonné, et il battit les Philistins depuis Guéba jusqu'à Guézer.

## Chapitre 6

### Désobéissance dans le transport de l'arche<!--1 Ch. 13:1-14.-->

6:1	David rassembla encore tous les hommes sélectionnés d'Israël, au nombre de 30 000 hommes.
6:2	Puis David se leva, ainsi que tout le peuple qui était avec lui, et se mit en marche de Baalé-Yéhouda, pour faire monter de là l'arche d'Elohîm, qui est appelée du Nom, du Nom de YHWH Sabaoth, qui siège entre les chérubins.
6:3	Ils mirent l'arche<!--L'arche ne devait être portée que par les Lévites. Les meilleures intentions pour le service de YHWH ne suffisent pas pour que le Seigneur nous agrée. Nous devons nous conformer à la parole d'Elohîm (1 R. 18:36-39).--> d'Elohîm sur un chariot tout neuf et l'emmenèrent de la maison d'Abinadab qui était sur la colline. Ouzza et Ahyo, fils d'Abinadab, conduisaient le chariot neuf.
6:4	Ils l'emportèrent donc de la maison d'Abinadab sur la colline ; et Ahyo allait devant l'arche.
6:5	David et toute la maison d'Israël jouaient devant YHWH de toutes sortes d'instruments faits de bois de cyprès, de harpes, de luths, de tambourins, de sistres et de cymbales.
6:6	Quand ils furent arrivés à l'aire de Nacon, Ouzza étendit la main vers l'arche d'Elohîm et la saisit parce que les bœufs la faisaient pencher.
6:7	La colère de YHWH s'enflamma contre Ouzza et Elohîm le frappa là à cause de sa faute. Il mourut là, près de l'arche d'Elohîm.
6:8	David fut irrité de ce que YHWH avait fait une brèche en la personne d'Ouzza. C'est pourquoi on a appelé ce lieu jusqu'à ce jour Pérets-Ouzza.
6:9	David eut peur de YHWH en ce jour-là, et il dit : Comment l'arche de YHWH entrerait-elle chez moi ?
6:10	David ne voulut pas déposer l'arche de YHWH chez lui dans la cité de David, mais il la fit conduire dans la maison d'Obed-Édom de Gath.
6:11	L'arche de YHWH resta 3 mois dans la maison d'Obed-Édom de Gath, et YHWH bénit Obed-Édom et toute sa maison.

### Accueil de l'arche à Yeroushalaim (Jérusalem)<!--1 Ch. 15:26-16:1.-->

6:12	Puis on vint dire au roi David : YHWH a béni la maison d'Obed-Édom et tout ce qui lui appartient, à cause de l'arche d'Elohîm. Alors David s'y rendit, et il fit monter l'arche d'Elohîm depuis la maison d'Obed-Édom jusqu'à la cité de David, au milieu des réjouissances.
6:13	Et il arriva que quand ceux qui portaient l'arche d'Elohîm eurent fait six pas, on sacrifia des taureaux et des béliers gras.
6:14	David dansait de toute sa force devant YHWH, et il était ceint d'un éphod de lin.
6:15	Ainsi, David et toute la maison d'Israël firent monter l'arche de YHWH avec des cris de joie et au son du shofar.
6:16	Comme l'arche de YHWH entrait dans la cité de David, Miykal, fille de Shaoul, regardait par la fenêtre, et voyant le roi David sauter et danser devant YHWH, elle le méprisa en son cœur.
6:17	Ils amenèrent l'arche de YHWH et la posèrent au milieu de la tente que David avait dressée pour elle ; et David offrit des holocaustes et des sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.--> devant YHWH.
6:18	Quand David eut achevé d'offrir des holocaustes et des sacrifices d'offrande de paix, il bénit le peuple au Nom de YHWH Sabaoth.
6:19	Et il partagea à tout le peuple, à toute la multitude d'Israël, tant aux hommes qu'aux femmes, à chacun un pain, un morceau de viande, un gâteau aux raisins. Puis tout le peuple s'en alla, chacun dans sa maison.
6:20	David s'en retourna pour bénir aussi sa maison, et Miykal, fille de Shaoul, sortit à sa rencontre. Elle dit : Quel honneur s'est fait aujourd'hui le roi d'Israël, en se découvrant aujourd'hui aux yeux des servantes et de ses serviteurs, comme se découvrirait, se découvrirait un homme sans valeur !
6:21	David répondit à Miykal : C'est devant YHWH, qui m'a choisi plutôt que ton père et toute sa maison pour m'établir chef sur le peuple de YHWH, sur Israël, c'est devant YHWH que je me suis réjoui.
6:22	Je me rendrai encore plus insignifiant que cela et je m'humilierai à mes propres yeux. Toutefois je serai honoré auprès des servantes dont tu parles.
6:23	Or Miykal, fille de Shaoul, n'eut pas d'enfants jusqu'au jour de sa mort.

## Chapitre 7

### David veut construire une maison à YHWH<!--1 Ch. 17:1-2.-->

7:1	Et il arriva, lorsque le roi fut établi dans sa maison, et que YHWH lui eut donné du repos de tous ses ennemis qui l'entouraient,
7:2	qu'il dit à Nathan le prophète : Regarde maintenant ! J'habite dans une maison de cèdres, et l'arche d'Elohîm habite sous des tapis<!--Le terme hébreu est « yeriy'ah », ce qui signifie « rideau », « drap », « tapis ». Voir Ex. 26.-->.
7:3	Alors Nathan répondit au roi : Va, fais tout ce qui est dans ton cœur, car YHWH est avec toi.

### YHWH traite alliance avec David et sa postérité<!--1 Ch. 17:3-15.-->

7:4	Mais il arriva cette nuit-là que la parole de YHWH vint à Nathan, en disant :
7:5	Va, et dis à David, mon serviteur : Ainsi parle YHWH : Me bâtirais-tu une maison afin que j'y habite ?
7:6	Puisque je n'ai pas habité dans une maison depuis le jour où j'ai fait monter les enfants d'Israël hors d'Égypte jusqu'à ce jour ; mais j'ai marché çà et là sous une tente et dans un tabernacle.
7:7	Partout où j'ai marché avec tous les enfants d'Israël, ai-je dit un seul mot à l'une des tribus d'Israël à qui j'avais ordonné de paître mon peuple d'Israël, ai-je dit : Pourquoi ne me bâtissez-vous pas une maison de cèdres ?
7:8	Maintenant tu diras à David, mon serviteur : Ainsi parle YHWH Sabaoth : Je t'ai pris d'une cabane, d'auprès des brebis, afin que tu sois le conducteur de mon peuple, Israël.
7:9	J'ai été avec toi partout où tu as marché, j'ai exterminé tous tes ennemis devant toi et j'ai rendu ton nom grand comme le nom des grands qui sont sur la Terre.
7:10	J'ai établi une demeure à mon peuple, à Israël, et je l'ai planté pour qu'il y habite et ne soit plus agité, pour que les fils de l'injustice ne recommencent plus à l'humilier comme autrefois,
7:11	et comme du temps où j'avais établi des juges sur mon peuple d'Israël. Je t'ai accordé du repos face à tous tes ennemis. Et YHWH t'annonce qu'il te bâtira une maison.
7:12	Quand tu seras endormi avec tes pères, je susciterai après toi, ton fils, qui sera sorti de tes entrailles et j'affermirai son règne.
7:13	Ce sera lui qui bâtira une maison à mon Nom, et j'affermirai pour toujours le trône de son règne<!--Le Royaume millénaire était promis à David et à sa postérité. Il fut proclamé par Yohanan le Baptiste (Mt. 3:2), le Mashiah (Mt. 4:17) et les apôtres (Mt. 10:5-7) comme étant proche. Présentement, le Royaume d'Elohîm se manifeste par la vie sanctifiée des saints en Mashiah (Lu. 17:20-21 ; Jn. 3:1-8 ; Ro. 14:17). Il n'apparaîtra pas de manière visible avant la « moisson », c'est-à-dire le jugement des nations (Mt. 13:39-50). En effet, ce n'est qu'après cette moisson que le Royaume sera installé ici-bas, lorsque le Mashiah rétablira la monarchie et la dynastie de David en sa propre personne. Il rassemblera alors les enfants d'Israël dispersés dans le monde entier et établira sa domination sur toute la terre pendant 1 000 ans. Ce Royaume sera remis au Père par le Mashiah après avoir vaincu le dernier ennemi, c'est-à-dire la mort (1 Co. 15:24-26). De ce fait, personne ne mourra pendant le millénium. Toutes les nations monteront tous les ans à Yeroushalaim (Jérusalem) pour adorer YHWH et célébrer la fête des tabernacles qui sera restaurée (Za. 14:16-21). Le gouvernement théocratique en Israël sera alors restauré (Es. 1:26).-->.
7:14	Je serai pour lui un père et il sera pour moi un fils. S'il fait le mal, je le châtierai avec une verge d'hommes et avec des plaies des fils d'humains ;
7:15	mais ma grâce ne se retirera pas de lui, comme je l'ai retirée de Shaoul, que j'ai ôté de devant toi.
7:16	Ainsi ta maison et ton règne seront assurés à jamais devant toi, et ton trône sera pour toujours affermi.
7:17	Nathan rapporta à David toutes ces paroles et toute cette vision.

### Louange et reconnaissance de David envers YHWH<!--1 Ch. 17:16-27.-->

7:18	Alors le roi David alla se présenter devant YHWH, et dit : Qui suis-je, Adonaï YHWH, et quelle est ma maison, que tu m'aies fait arriver au point où je suis ?
7:19	C'est encore peu de choses à tes yeux, ô Adonaï YHWH ! Car tu as même parlé au sujet de la maison de ton serviteur pour les temps éloignés. Telle est la torah de l'être humain, Seigneur YHWH ?
7:20	Et que pourrait dire de plus David ? Car, Adonaï YHWH, tu connais ton serviteur !
7:21	Tu as fait toutes ces grandes choses pour l'amour de ta parole, et selon ton cœur, pour les révéler à ton serviteur.
7:22	C'est pourquoi tu t'es montré grand, ô YHWH Elohîm ! Car nul n'est semblable à toi, et il n'y a pas d'autre Elohîm que toi, d'après tout ce que nous avons entendu de nos oreilles.
7:23	Et qui est comme ton peuple, comme Israël, la seule nation de la Terre qu'Elohîm est venu racheter pour en faire son peuple, y mettre son Nom et pour accomplir dans ton pays, devant ton peuple que tu t'es racheté d'Égypte, des choses grandes et terribles contre les nations et contre leurs elohîm ?
7:24	Tu as affermi ton peuple d'Israël pour qu'il soit ton peuple pour toujours ; et toi, YHWH, tu es devenu son Elohîm.
7:25	Maintenant donc, ô YHWH Elohîm, confirme pour toujours la parole que tu as prononcée sur ton serviteur et sur sa maison, et agis selon ta parole.
7:26	Que ton Nom soit à jamais glorifié, et que l'on dise : YHWH Sabaoth est l'Elohîm d'Israël ! Et que la maison de David, ton serviteur, demeure stable devant toi !
7:27	Car toi, YHWH Sabaoth, Elohîm d'Israël, tu as découvert l'oreille de ton serviteur en disant : Je te bâtirai une maison ! C'est pourquoi ton serviteur a trouvé le courage de t'adresser cette prière.
7:28	Maintenant, Adonaï YHWH, tu es Elohîm, tes paroles sont vérité, et tu as promis cette grâce à ton serviteur.
7:29	Veuille donc bénir la maison de ton serviteur, afin qu'elle soit éternellement devant toi ! Car c'est toi, Adonaï YHWH, qui a parlé, et par ta bénédiction la maison de ton serviteur sera comblée de bénédictions éternellement.

## Chapitre 8

### YHWH donne à David la victoire sur ses ennemis<!--1 Ch. 18:1-17.-->

8:1	Après cela, il arriva que David battit les Philistins et les humilia, et il prit Métheg-Amma de la main des Philistins.
8:2	Il battit aussi les Moabites et les mesura au cordeau en les faisant coucher par terre. Il en mesura deux cordeaux pour les faire mourir, et un plein cordeau pour leur laisser la vie. Et les Moabites furent assujettis à David et lui payèrent un tribut.
8:3	David battit aussi Hadadézer, fils de Rehob, roi de Tsoba, lorsqu'il alla rétablir sa domination sur le fleuve de l'Euphrate.
8:4	David lui prit 1 700 cavaliers et 20 000 hommes de pied. Il coupa les jarrets aux chevaux de tous les chars et ne conserva que 100 attelages.
8:5	Les Syriens de Damas vinrent au secours d'Hadadézer, roi de Tsoba, et David battit 22 000 Syriens.
8:6	David mit des garnisons dans la Syrie de Damas. Et les Syriens furent assujettis à David et lui payèrent un tribut. YHWH protégeait David partout où il allait.
8:7	Et David prit les boucliers d'or qui étaient aux serviteurs d'Hadadézer, et les apporta à Yeroushalaim.
8:8	Le roi David emporta aussi une grande quantité de cuivre de Béthach et de Bérothaï, villes d'Hadadézer.
8:9	Thoï, roi de Hamath, apprit que David avait battu toute l'armée d'Hadadézer,
8:10	et il envoya Yoram son fils vers le roi David pour le saluer et pour le féliciter d'avoir fait la guerre contre Hadadézer et de l'avoir battu. En effet, Thoï était un homme en guerre contre Hadadézer. Et dans sa main il y avait des vases en argent, des vases en or et des vases en cuivre.
8:11	Le roi David les consacra à YHWH, avec l'argent et l'or qu'il avait déjà consacrés du butin de toutes les nations qu'il s'était assujetties,
8:12	de la Syrie, de Moab, des fils d'Ammon, des Philistins, d'Amalek, et du butin d'Hadadézer, fils de Rehob, roi de Tsoba.
8:13	Au retour de la défaite des Syriens, David se fit encore un nom, en battant dans la vallée du sel 18 000 Édomites.
8:14	Il mit des garnisons dans Édom, il mit des garnisons dans tout Édom. Et tout Édom fut assujetti à David. YHWH protégeait David partout où il allait.
8:15	Ainsi David régna sur tout Israël, et il faisait droit et justice à tout son peuple.
8:16	Yoab, fils de Tserouyah, commandait l'armée ; Yehoshaphat, fils d'Ahiloud, était archiviste ;
8:17	Tsadok, fils d'Ahitoub, et Achimélec, fils d'Abiathar, étaient prêtres ; Serayah était secrétaire ;
8:18	Benayah, fils de Yehoyada, était chef des Kéréthiens et des Péléthiens. Et les fils de David étaient prêtres.

## Chapitre 9

### Mephibosheth à la table de David

9:1	Alors David dit : Ne reste-t-il donc personne de la maison de Shaoul, afin que je lui fasse du bien pour l'amour de Yehonathan ?
9:2	Il y avait dans la maison de Shaoul un serviteur nommé Tsiba, que l'on fit venir auprès de David. Le roi lui dit : Es-tu Tsiba ? Et il répondit : Je suis ton serviteur !
9:3	Le roi dit : N'y a-t-il plus personne de la maison de Shaoul, pour que j'use envers lui de la bonté d'Elohîm ? Tsiba répondit au roi : Il y a encore un des fils de Yehonathan, qui est perclus des pieds.
9:4	Le roi lui dit : Où est-il ? Et Tsiba répondit au roi : Il est dans la maison de Makir, fils d'Ammiel, à Lodebar.
9:5	Alors le roi David l'envoya chercher dans la maison de Makir, fils d'Ammiel, à Lodebar.
9:6	Quand Mephibosheth, fils de Yehonathan, fils de Shaoul, vint auprès de David, il tomba sur sa face et se prosterna. David dit : Mephibosheth ! Et il répondit : Voici ton serviteur.
9:7	David lui dit : N'aie pas peur, car je te traiterai, je te traiterai avec bonté par égard pour ton père Yehonathan. Je te restituerai toutes les terres de Shaoul, ton père, et tu mangeras toujours du pain à ma table.
9:8	Il se prosterna et dit : Qui suis-je, moi ton serviteur, pour que tu regardes un chien mort tel que moi ?
9:9	Le roi appela Tsiba, serviteur de Shaoul, et lui dit : Je donne au fils de ton maître tout ce qui appartenait à Shaoul et à toute sa maison.
9:10	C'est pourquoi tu cultiveras pour lui ces terres, toi, tes fils, et tes serviteurs, et tu en recueilleras les fruits, afin que le fils de ton maître ait du pain à manger ; et Mephibosheth, fils de ton maître, mangera toujours du pain à ma table. Or Tsiba avait 15 fils et 20 serviteurs.
9:11	Tsiba dit au roi : Ton serviteur fera tout ce que le roi, mon seigneur, ordonne à son serviteur. Et Mephibosheth mangea à la table de David comme l'un des fils du roi.
9:12	Mephibosheth avait un jeune fils, nommé Miyka, et tous ceux qui demeuraient dans la maison de Tsiba étaient serviteurs de Mephibosheth.
9:13	Mephibosheth habitait à Yeroushalaim parce qu'il mangeait toujours à la table du roi. Il était boiteux des deux pieds.

## Chapitre 10

### Double bataille contre les Ammonites et les Syriens

10:1	Or il arriva après cela que le roi des fils d'Ammon mourut, et Hanoun, son fils, régna à sa place.
10:2	Et David dit : J'userai de bonté envers Hanoun, fils de Nachash, comme son père en a usé envers moi. Ainsi David lui envoya ses serviteurs pour le consoler au sujet de son père. Lorsque les serviteurs de David arrivèrent dans le pays des fils d'Ammon,
10:3	les chefs des fils d'Ammon dirent à Hanoun, leur maître : Penses-tu que ce soit pour honorer ton père que David t'envoie des consolateurs ? N'est-ce pas pour reconnaître exactement la ville et pour l'épier, afin de la détruire, que David envoie ses serviteurs auprès de toi ?
10:4	Alors Hanoun saisit les serviteurs de David, et fit raser la moitié de leur barbe, et couper la moitié de leurs habits jusqu'aux hanches. Puis il les renvoya.
10:5	David en fut informé et envoya des gens à leur rencontre, car ces hommes étaient très humiliés. Le roi leur fit dire : Restez à Yeriycho jusqu'à ce que votre barbe ait repoussé, et revenez ensuite.
10:6	Les fils d'Ammon voyant qu'ils s'étaient rendus odieux à David, les fils d'Ammon envoyèrent et prirent à leur solde 20 000 hommes de pied chez les Syriens de Beth-Rehob, et chez les Syriens de Tsoba, 1 000 hommes chez le roi de Ma'akah, et 12 000 hommes chez les gens de Tob.
10:7	Et lorsque David l'apprit, il envoya Yoab et toute l'armée, les hommes les plus vaillants.
10:8	Les fils d'Ammon sortirent et se rangèrent en bataille à l'entrée de la porte. Les Syriens de Tsoba de Rehob et les hommes de Tob et de Ma'akah étaient à part dans la campagne.
10:9	Yoab, voyant qu'il avait un front de bataille à la fois devant et derrière lui, choisit alors des jeunes hommes parmi tous ceux d'Israël, et les rangea contre les Syriens.
10:10	Il donna le commandement du reste du peuple à Abishaï, son frère, pour le ranger en bataille contre les fils d'Ammon.
10:11	Il dit : Si les Syriens sont plus forts que moi, tu viendras à mon secours, et si les fils d'Ammon sont plus forts que toi, j'irai te secourir.
10:12	Sois vaillant, et portons-nous vaillamment pour notre peuple et pour les villes de notre Elohîm, et que YHWH fasse ce qui est bon à ses yeux !
10:13	Alors Yoab et le peuple qui était avec lui s'approchèrent pour livrer bataille aux Syriens et ils s'enfuirent devant lui.
10:14	Quand les fils d'Ammon virent que les Syriens avaient pris la fuite, ils s'enfuirent aussi devant Abishaï et rentrèrent dans la ville. Yoab s'éloigna des fils d'Ammon et revint à Yeroushalaim.
10:15	Les Syriens, voyant qu'ils avaient été battus par Israël, se rassemblèrent.
10:16	Hadarézer envoya chercher les Syriens qui étaient de l'autre côté du fleuve. Ils arrivèrent à Hélam, et Shobac, chef de l'armée d'Hadarézer, les conduisait.
10:17	Cela fut rapporté à David, qui rassembla tout Israël, passa le Yarden et vint à Hélam. Les Syriens se rangèrent en bataille contre David et combattirent contre lui.
10:18	Mais les Syriens s'enfuirent devant Israël. Et David défit 700 chars des Syriens et 40 000 cavaliers. Il frappa aussi Shobac, le chef de leur armée, qui mourut sur place.
10:19	Tous les rois soumis à Hadarézer, se voyant battus par Israël, firent la paix avec Israël et lui furent assujettis. Et les Syriens craignirent désormais de secourir les fils d'Ammon.

## Chapitre 11

### Péché de David avec Bath-Shéba

11:1	Il arriva, l'année suivante, au temps où sortent les rois, que David envoya Yoab, avec ses serviteurs et tout Israël, pour détruire les fils d'Ammon et assiéger Rabba. Mais David resta à Yeroushalaim<!--Au lieu d'aller en guerre et de diriger ses troupes, David resta à Yeroushalaim (Jérusalem). Cette négligence l'a conduit à la convoitise, à l'adultère et au meurtre d'Ouriyah. La distraction peut conduire à la mort. Il y a un temps pour toutes choses (Ec. 3:1-8).-->.
11:2	Et il arriva, au temps du soir, que David, s'étant levé de sa couche et se promenant sur le toit de la maison royale, aperçut du toit une femme qui se baignait. Cette femme était très belle de figure.
11:3	David envoya demander qui était cette femme, et on lui dit : N'est-ce pas Bath-Shéba, fille d'Éliam, femme d'Ouriyah, le Héthien ?
11:4	Et David envoya des messagers pour la chercher. Elle vint vers lui, et il coucha avec elle. Après s'être purifiée de sa souillure, elle retourna dans sa maison.
11:5	Cette femme devint enceinte et elle fit dire à David : Je suis enceinte.
11:6	Alors David envoya dire à Yoab : Envoie-moi Ouriyah, le Héthien. Et Yoab envoya Ouriyah à David.
11:7	Ouriyah se rendit auprès de David, qui l'interrogea sur l'état<!--Sur la paix de Yoab.--> de Yoab, sur l'état<!--Sur la paix du peuple.--> du peuple et sur l'état<!--Sur la paix de la guerre.--> de la guerre.
11:8	Puis David dit à Ouriyah : Descends dans ta maison et lave tes pieds. Ouriyah sortit de la maison du roi, et on fit porter après lui un présent royal.
11:9	Mais Ouriyah se coucha à la porte de la maison du roi, avec tous les serviteurs de son maître, et il ne descendit pas dans sa maison.
11:10	On le rapporta à David, et on lui dit : Ouriyah n'est pas descendu dans sa maison. David dit à Ouriyah : N'arrives-tu pas de voyage ? Pourquoi n'es-tu pas descendu dans ta maison ?
11:11	Ouriyah répondit à David : L'arche et Israël et Yéhouda habitent sous des tentes, mon seigneur Yoab et les serviteurs de mon seigneur campent aux champs, et moi j'entrerais dans ma maison pour manger et boire et pour coucher avec ma femme ! Tu es vivant, et ton âme est vivante, je ne ferai pas une telle chose.
11:12	David dit à Ouriyah : Reste ici encore aujourd'hui, et demain je te renverrai. Ouriyah resta donc ce jour-là et le lendemain à Yeroushalaim.
11:13	David l'invita à manger et à boire en sa présence et l'enivra. Mais le soir, il sortit pour se coucher sur son lit avec tous les serviteurs de son maître, et il ne descendit pas dans sa maison.
11:14	Le lendemain matin, David écrivit une lettre à Yoab, et l'envoya par la main d'Ouriyah.
11:15	Il écrivit en disant : Placez Ouriyah face au plus fort de la guerre et éloignez-vous de lui, afin qu'il soit frappé et qu'il meure !
11:16	Yoab, en observant la ville, plaça Ouriyah à l'endroit où il savait qu'il y avait des hommes talentueux.
11:17	Les hommes de la ville sortirent et combattirent contre Yoab. Il en tomba parmi le peuple, parmi les serviteurs de David. Ouriyah le Héthien fut lui aussi tué.
11:18	Alors Yoab envoya un messager à David pour lui faire savoir tout ce qui était arrivé dans ce combat.
11:19	Il donna cet ordre au messager : Quand tu auras achevé de raconter au roi tout ce qui est arrivé au combat.
11:20	S'il arrive que le roi se mette en colère et qu'il te dise : Pourquoi vous êtes-vous approchés de la ville pour combattre ? Ne savez-vous pas bien qu'on tire de dessus la muraille ?
11:21	Qui a tué Abimélec, fils de Yeroubbésheth ? N'est-ce pas une femme qui lança sur lui de dessus la muraille une pièce de meule de moulin, et n'en est-il pas mort à Thébets ? Pourquoi vous êtes-vous approchés de la muraille ? Alors tu lui diras : Ton serviteur Ouriyah, le Héthien, est mort aussi.
11:22	Le messager partit. À son arrivée, il fit savoir à David tout ce pourquoi Yoab l'avait envoyé.
11:23	Le messager dit à David : Ces hommes ont été plus forts que nous. Ils avaient fait une sortie contre nous dans les champs, mais nous les avons repoussés jusqu'à l'entrée de la porte.
11:24	Les archers ont tiré sur tes serviteurs du haut de la muraille et des serviteurs du roi ont été tués. Ton serviteur Ouriyah, le Héthien est mort aussi.
11:25	David dit au messager : Tu diras ainsi à Yoab : Que cette affaire ne paraisse pas mauvaise à tes yeux, car l'épée dévore tantôt l'un, tantôt l'autre. Renforce ta guerre contre la ville et détruis-la. Quant à toi, encourage-le !
11:26	La femme d'Ouriyah apprit qu'Ouriyah son homme était mort et elle pleura son mari.
11:27	Quand le deuil fut passé, David l'envoya chercher et la recueillit dans sa maison. Elle devint sa femme, et lui enfanta un fils. Mais la chose que David avait faite parut mauvaise aux yeux de YHWH.

## Chapitre 12

### Le prophète Nathan envoyé pour reprendre David

12:1	YHWH envoya Nathan vers David. Nathan vint à lui, et lui dit : Il y avait deux hommes dans une ville, l'un riche et l'autre pauvre.
12:2	Le riche avait des brebis et des bœufs en très grand nombre.
12:3	Mais le pauvre n'avait rien du tout, sauf une petite brebis, qu'il avait achetée. Il la nourrissait et elle grandissait chez lui avec ses enfants. Elle mangeait de son pain, buvait dans sa coupe, dormait sur son sein et elle était comme sa fille.
12:4	Et un voyageur arriva chez l'homme riche. Ce riche a épargné ses brebis et ses bœufs, pour préparer un repas au voyageur qui était venu chez lui. Il a pris la brebis du pauvre homme et l'a apprêtée pour l'homme qui était venu chez lui.
12:5	Alors la colère de David s'enflamma violemment contre cet homme, et il dit à Nathan : YHWH est vivant ! L'homme qui fait cela est un fils de la mort.
12:6	Parce qu'il a fait cela et qu'il n'a pas épargné cette brebis, pour une brebis il en rendra 4.
12:7	Alors Nathan dit à David : Tu es cet homme-là ! Ainsi parle YHWH, l'Elohîm d'Israël : Je t'ai oint pour roi sur Israël et je t'ai délivré de la main de Shaoul ;
12:8	je t'ai même donné la maison de ton maître, et les femmes de ton maître dans ton sein, et je t'ai donné la maison d'Israël et de Yéhouda. Et si cela avait été peu, j'y aurais encore ajouté.
12:9	Pourquoi donc as-tu méprisé la parole de YHWH, en faisant ce qui est mal à ses yeux ? Tu as frappé de l'épée Ouriyah, le Héthien, tu as pris sa femme pour en faire ta femme, et tu l'as tué par l'épée des fils d'Ammon.
12:10	Maintenant, l'épée ne s'éloignera jamais de ta maison, parce que tu m'as méprisé, et que tu as pris la femme d'Ouriyah, le Héthien, pour en faire ta femme.
12:11	Ainsi parle YHWH : Voici, je vais faire sortir de ta propre maison le malheur contre toi, et je vais prendre sous tes yeux tes propres femmes pour les donner à un homme de ta maison, qui couchera avec elles à la vue de ce soleil.
12:12	Car tu as fait cette action dans un lieu caché, mais moi, je la ferai à la face de tout Israël et à la face du soleil !

### Repentance de David

12:13	Alors David dit à Nathan : J'ai péché contre YHWH ! Et Nathan dit à David : YHWH passe par-dessus ton péché, tu ne mourras pas.
12:14	Toutefois, parce qu'en commettant cela, tu as donné l'occasion aux ennemis de YHWH de le blasphémer, à cause de cela le fils qui t'est né mourra, il mourra.
12:15	Et Nathan retourna dans sa maison. YHWH frappa l'enfant que la femme d'Ouriyah avait enfanté à David, et il devint gravement malade.
12:16	David pria Elohîm pour le garçon, et David jeûna et, quand il rentra, il passa la nuit couché par terre.
12:17	Les anciens de sa maison se levèrent et vinrent vers lui pour le faire lever de terre, mais il ne voulut pas, et il ne mangea rien avec eux.
12:18	Et il arriva que l'enfant mourut le septième jour. Les serviteurs de David craignaient de lui annoncer que l'enfant était mort. Car ils se disaient : Voici, quand l'enfant vivait encore, nous lui avons parlé, et il n'a pas écouté notre voix. Comment pourrions-nous lui dire : L'enfant est mort ! Il fera quelque chose de mauvais.
12:19	David vit que ses serviteurs parlaient à voix basse, et il comprit que l'enfant était mort. David dit à ses serviteurs : L'enfant est-il mort ? Ils répondirent : Il est mort.
12:20	Alors David se leva de terre. Il se lava, s'oignit, et changea de vêtements. Il alla dans la maison de YHWH et s'y prosterna. De retour chez lui, il demanda qu'on lui serve de la nourriture et il mangea.
12:21	Ses serviteurs lui dirent : Qu'est-ce que tu fais ? Tu jeûnais et pleurais pour l'amour de l'enfant lorsqu'il vivait encore. Maintenant que l'enfant est mort, tu te lèves et tu manges !
12:22	Mais il répondit : Quand l'enfant vivait encore, je jeûnais et pleurais, car je me disais : Qui sait si YHWH n'aura pas pitié de moi et si l'enfant ne vivra pas ?
12:23	Maintenant qu'il est mort, pourquoi jeûnerais-je ? Puis-je le faire revenir ? J'irai vers lui, mais il ne reviendra pas vers moi.

### Naissance de Shelomoh

12:24	David consola sa femme Bath-Shéba, et il alla auprès d'elle et coucha avec elle. Elle lui enfanta un fils qu'il appela du nom de Shelomoh et qui fut aimé de YHWH.
12:25	Il le remit entre les mains de Nathan, le prophète, qui lui donna le nom de Yediydeyah, à cause de YHWH.

### Le pays et le roi de Rabba livrés à Yoab et David<!--1 Ch. 20:1-3.-->

12:26	Yoab combattait contre Rabba qui appartenait aux fils d'Ammon, il s'empara de la ville royale,
12:27	et envoya des messagers à David pour lui dire : J'ai attaqué Rabba, et j'ai pris la ville des eaux.
12:28	C'est pourquoi rassemble maintenant le reste du peuple, campe contre la ville et prends-la, de peur que je ne prenne la ville et qu'on ne l'appelle de mon nom.
12:29	David rassembla tout le peuple et marcha contre Rabba. Il l'attaqua et la prit.
12:30	Il enleva la couronne de dessus la tête de son roi ; elle pesait un talent d'or et était garnie de pierres précieuses. On la mit sur la tête de David, qui emporta de la ville un très grand butin.
12:31	Il fit sortir aussi le peuple qui s'y trouvait, et il les plaça sous des scies, des herses de fer, des haches de fer et le fit passer par un fourneau où l'on cuit les briques. Il traita ainsi toutes les villes des fils d'Ammon. Puis David retourna avec tout le peuple à Yeroushalaim.

## Chapitre 13

### David subit les conséquences de son péché

13:1	Après cela il arriva qu'Absalom, fils de David, ayant une sœur qui était belle et qui se nommait Tamar, Amnon, fils de David, l'aima.

### Inceste au sein de la famille royale

13:2	Amnon fut si tourmenté qu'il tomba malade à cause de Tamar sa sœur, car elle était vierge. Il paraissait trop difficile à Amnon d'obtenir la moindre chose d'elle.
13:3	Amnon avait un ami, nommé Yonadab, fils de Shimea, frère de David, et Yonadab était un homme très rusé.
13:4	Il lui dit : Fils de roi, pourquoi maigris-tu ainsi de matin en matin ? Ne veux-tu pas me le dire ? Amnon lui dit : J'aime Tamar, la sœur de mon frère, Absalom.
13:5	Alors Yehonadab lui dit : Couche-toi dans ton lit et fais le malade. Quand ton père viendra te voir, tu lui diras : Permets à Tamar, ma sœur, de venir pour me donner à manger. Qu'elle prépare un mets sous mes yeux, afin que je le voie et que je le prenne de sa main.
13:6	Amnon se coucha et fit le malade. Le roi vint le voir, et Amnon dit au roi : Je te prie, que ma sœur Tamar vienne faire deux pains sous mes yeux, et que je les mange de sa main.
13:7	David envoya dire à Tamar dans la maison : Va dans la maison de ton frère Amnon, et prépare-lui quelque chose d'appétissant.
13:8	Tamar alla dans la maison de son frère Amnon, qui était couché. Elle prit de la pâte, la pétrit, et en fit devant lui des pains et les fit cuire.
13:9	Puis elle prit la poêle, et elle les versa devant lui. Mais Amnon refusa d'en manger. Il dit : Faites sortir tout le monde de chez moi ! Et tout le monde se retira.
13:10	Alors Amnon dit à Tamar : Apporte-moi le mets dans la chambre, et que je le mange de ta main. Tamar prit les pains qu'elle avait faits, et les apporta à Amnon, son frère dans la chambre.
13:11	Comme elle les lui présentait pour qu'il en mange, il se saisit d'elle et lui dit : Viens, couche avec moi, ma sœur !
13:12	Elle lui répondit : Non, mon frère, ne m'humilie pas, car cela ne se fait pas en Israël. Ne commets pas cette infamie.
13:13	Et moi, où irais-je avec mon opprobre ? Et toi, tu serais comme l'un des infâmes en Israël. Maintenant, je te prie, parle au roi, et il ne s'opposera pas à ce que je sois ta femme.
13:14	Mais il ne voulut pas écouter sa voix. Il fut plus fort qu'elle, l'humilia et coucha avec elle<!--Le viol et l'inceste que commit Amnon, fils de David, sur Tamar, sa demi-sœur, furent les conséquences du péché de David avec Bath-Shéba.-->.
13:15	Après cela, Amnon eut pour elle une très grande haine, en sorte que la haine qu'il lui portait était plus grande que l'amour qu'il avait eu pour elle. Ainsi, Amnon lui dit : Lève-toi, va-t'en !
13:16	Elle lui répondit : Tu n'as aucune raison de me faire cela, car me renvoyer serait un mal plus grand que l'autre, celui que tu m'as déjà fait.
13:17	Mais il ne voulut pas l'écouter, et appelant le garçon qui le servait, il dit : Renvoie donc celle-la loin de moi, dehors ! Et ferme la porte derrière elle !
13:18	Elle était habillée d'une tunique de couleurs, car c'était la robe dont étaient vêtues les filles vierges du roi. Le serviteur d'Amnon la mit dehors, et ferma la porte après elle.
13:19	Alors Tamar répandit de la cendre sur sa tête, et déchira sa tunique de couleurs. Elle mit la main sur sa tête et s'en alla en poussant des cris.
13:20	Et son frère Absalom lui dit : Ton frère, Amnon, a-t-il été avec toi ? Maintenant, ma sœur, tais-toi, c'est ton frère. Ne fixe pas ton cœur sur cette affaire. Et Tamar, dévastée, resta dans la maison d'Absalom son frère.
13:21	Quand le roi David eut appris toutes ces choses, il fut très irrité.
13:22	Absalom ne parla ni en bien ni en mal avec Amnon, mais il éprouva de la haine pour lui parce qu'il avait humilié Tamar, sa sœur.

### Vengeance d'Absalom sur Amnon

13:23	Deux ans après, il arriva comme Absalom avait les tondeurs à Baal-Hatsor, près d'Éphraïm, qu'Absalom invita tous les fils du roi.
13:24	Absalom alla vers le roi et dit : Voici ton serviteur a les tondeurs. Je te prie donc, que le roi et ses serviteurs viennent avec ton serviteur.
13:25	Et le roi dit à Absalom : Non, mon fils, nous n'irons pas tous, de peur que nous ne te soyons à charge. Absalom le pressa, mais le roi ne voulut pas aller et il le bénit.
13:26	Absalom dit : Permets au moins à Amnon, mon frère, de venir avec nous. Le roi lui répondit : Pourquoi irait-il ?
13:27	Absalom le pressa tellement qu'il laissa aller Amnon et tous les fils du roi avec lui.
13:28	Or Absalom avait donné cet ordre à ses serviteurs, en disant : Prenez bien garde, je vous prie, quand le cœur d'Amnon sera égayé par le vin et que je vous dirai : Frappez Amnon ! Vous le tuerez. N'ayez pas peur, n'est-ce pas moi qui vous l'ordonne ? Soyez forts, soyez des fils talentueux !
13:29	Les serviteurs d'Absalom traitèrent Amnon comme Absalom l'avait ordonné. Et tous les fils du roi se levèrent, montèrent chacun sur son mulet et s'enfuirent.
13:30	Et il arriva, comme ils étaient en chemin, que le bruit parvint à David qu'Absalom avait tué tous les fils du roi, et qu'il n'en était pas resté un seul d'entre eux.
13:31	Le roi se leva, déchira ses vêtements et se coucha par terre. Tous ses serviteurs se tenaient là, avec leurs vêtements déchirés.
13:32	Yonadab, fils de Shimea, frère de David, prit la parole et dit : Que mon seigneur ne dise pas que tous les jeunes hommes, fils du roi, ont été tués, car seul Amnon est mort. En effet, c'était chose réglée par la bouche d'Absalom depuis le jour où Amnon a humilié Tamar, sa sœur.
13:33	Maintenant donc, que le roi mon seigneur ne prenne pas la chose à cœur, en disant que tous les fils du roi sont morts, car Amnon seul est mort.
13:34	Absalom prit la fuite. Or le jeune homme placé en sentinelle leva les yeux et regarda. Et voici, un grand peuple venait par le chemin qui était derrière lui, du côté de la montagne.
13:35	Yonadab dit au roi : Voici les fils du roi qui arrivent ! Ainsi se confirme ce que disait ton serviteur.
13:36	Comme il achevait de parler, voici les fils du roi arrivèrent. Ils élevèrent la voix et pleurèrent. Le roi aussi et tous ses serviteurs pleurèrent des pleurs très abondants.

### Absalom s'enfuit loin de son père

13:37	Absalom s'était enfui, et il alla chez Talmaï, fils d'Ammihour, roi de Guéshour<!--Absalom s'était réfugié chez Talmaï, roi de Guéshour (Transjordanie, au nord de la Syrie), qui était le père de Ma'akah, sa mère (2 S. 3:3). Il est donc allé chez son grand-père maternel.-->. Et David pleurait tous les jours son fils.
13:38	Absalom resta 3 ans à Guéshour, où il était allé, après avoir pris la fuite.
13:39	Le roi David cessa de poursuivre Absalom, car il était consolé de la mort d'Amnon.

## Chapitre 14

### Yoab convainc le roi de faire revenir Absalom

14:1	Alors Yoab, fils de Tserouyah, s'aperçut que le cœur du roi était pour Absalom.
14:2	Il envoya chercher à Tekoa une femme habile, et il lui dit : Fais semblant de te lamenter, et revêts des habits de deuil : ne t'oins pas d'huile, mais sois comme une femme qui de longs jours pleure un mort.
14:3	Ensuite va vers le roi, et tu lui parleras de cette manière. Yoab lui mit dans la bouche ce qu'elle devait dire.
14:4	La femme de Tekoa alla parler au roi. Elle tomba la face contre terre, se prosterna, et dit : Ô roi, sauve-moi !
14:5	Le roi lui dit : Qu'as-tu ? Elle répondit : Vraiment, je suis une femme veuve, et mon mari est mort !
14:6	Or ta servante avait deux fils. Ils se sont tous deux querellés dans les champs et il n'y avait personne pour les séparer. L'un a frappé l'autre et l'a tué.
14:7	Et voici, toute la famille s'est élevée contre ta servante, en disant : Donne-nous le meurtrier de son frère ! Nous voulons le faire mourir, pour la vie de son frère qu'il a tué, et que nous exterminions même l'héritier ! Ils veulent ainsi éteindre le charbon vif qui me restait, pour ne laisser à mon mari ni nom ni survivant sur la face de la Terre.
14:8	Le roi dit à la femme : Va-t-en dans ta maison, et je donnerai des ordres en ta faveur.
14:9	Alors la femme de Tekoa dit au roi : Mon seigneur et mon roi ! Que l'iniquité soit sur moi et sur la maison de mon père, et que le roi et son trône en soient innocents.
14:10	Et le roi répondit : Si quelqu'un parle contre toi, amène-le-moi, et jamais il ne lui arrivera de te toucher.
14:11	Et elle dit : Je te prie, que le roi se souvienne de YHWH, son Elohîm, afin que le rédempteur du sang n'augmente pas la ruine et qu'on ne fasse pas périr mon fils. Et il répondit : YHWH est vivant ! Il ne tombera pas à terre un seul des cheveux de ton fils.
14:12	La femme dit : Je te prie que ta servante dise un mot au roi, mon seigneur. Et il répondit : Parle !
14:13	La femme dit : Mais pourquoi as-tu pensé une chose comme celle-ci contre le peuple d'Elohîm ? Puisqu'en tenant ce discours, le roi se déclare coupable en ce qu'il n'a pas fait revenir celui qu'il a banni ?
14:14	Car nous mourrons, nous mourrons, et nous sommes comme l'eau versée sur la terre qu'on ne peut recueillir. Elohîm ne prend pas la vie, mais il forme des projets pour que le banni ne soit plus banni loin de sa présence.
14:15	Maintenant, si je suis venue pour tenir ce discours au roi, mon seigneur, c'est parce que le peuple m'a effrayée. Et ta servante s'est dit : Je veux parler maintenant au roi. Peut-être que le roi fera ce que sa servante lui dira.
14:16	Car le roi écoutera sa servante pour la délivrer de la main de celui qui veut nous exterminer, moi et mon fils, de l'héritage d'Elohîm.
14:17	Ta servante s'est dit : Que la parole du roi, mon seigneur, nous apporte du repos. Car le roi mon seigneur est comme un ange d'Elohîm, pour entendre le bien et le mal. Que YHWH, ton Elohîm, soit avec toi !
14:18	Le roi répondit et dit à la femme : Je te prie, ne me cache rien de ce que je vais te demander. Et la femme dit : Que le roi mon seigneur parle !
14:19	Et le roi dit : La main de Yoab n'est-elle pas avec toi dans tout ceci ? Et la femme répondit et dit : Ton âme vit, ô mon seigneur, qu'on ne saurait se détourner ni à droite ni à gauche de tout ce que dit le roi mon seigneur. C'est en effet ton serviteur Yoab qui m'a donné des ordres et qui a mis dans la bouche de ta servante toutes ces paroles.
14:20	C'est pour donner un autre visage à l'affaire que ton serviteur Yoab a fait cela. Mais mon seigneur est sage comme un ange d'Elohîm, pour savoir tout ce qui se passe sur la Terre.

### Retour d'Absalom à Yeroushalaim (Jérusalem)

14:21	Alors le roi dit à Yoab : Voici, maintenant c'est toi qui as conduit cette affaire. Va donc, et fais revenir le jeune homme Absalom.
14:22	Et Yoab tomba la face contre terre et se prosterna, et il bénit le roi. Puis il dit : Aujourd'hui, ton serviteur sait qu'il a trouvé grâce à tes yeux, ô roi mon seigneur, puisque le roi agit selon ce que son serviteur lui a dit.
14:23	Yoab se leva et partit pour Guéshour, et il ramena Absalom à Yeroushalaim.
14:24	Mais le roi dit : Qu'il se retire dans sa maison, et qu'il ne voie pas ma face. Et Absalom se retira dans sa maison, et ne vit pas la face du roi.
14:25	Il n'y avait pas d'homme dans tout Israël aussi renommé qu'Absalom pour sa beauté. Depuis la plante des pieds jusqu'au sommet de la tête, il n'y avait pas en lui de défaut.
14:26	Lorsqu'il se rasait la tête – c'était à la fin de chaque année qu'il se la rasait, parce que sa chevelure lui pesait trop, le poids de sa chevelure était de 200 sicles, poids du roi.
14:27	Il naquit à Absalom trois fils, et une fille nommée Tamar, qui était une femme belle de figure.
14:28	Et Absalom demeura 2 ans entiers à Yeroushalaim, sans voir la face du roi.
14:29	Absalom fit demander Yoab, pour l'envoyer vers le roi, mais Yoab ne voulut pas venir vers lui. Il le fit demander encore pour la seconde fois mais Yoab ne voulut pas venir.
14:30	Absalom dit alors à ses serviteurs : Vous voyez le champ de Yoab qui est à côté du mien, il y a de l'orge. Allez et mettez-y le feu ! Et les serviteurs d'Absalom mirent le feu au champ.
14:31	Alors Yoab se leva et vint vers Absalom dans sa maison. Il lui dit : Pourquoi tes serviteurs ont-ils mis le feu à mon champ ?
14:32	Et Absalom répondit à Yoab : Voici, j'ai envoyé vers toi en disant : Viens ici, et je t'enverrai vers le roi, afin que tu lui dises : Pourquoi suis-je revenu de Guéshour ? Il vaudrait mieux pour moi que j'y sois encore. Je désire maintenant voir la face du roi et, s'il y a de l'iniquité en moi, qu'il me fasse mourir !
14:33	Yoab alla vers le roi et lui rapporta cela. Et le roi appela Absalom, qui vint vers lui et se prosterna le visage contre terre devant le roi. Le roi embrassa Absalom.

## Chapitre 15

### Mauvaises intentions d'Absalom

15:1	Or il arriva qu'après cela, Absalom se procura des chars et des chevaux, et il avait 50 hommes qui couraient devant lui<!--La révolte d'Absalom était une autre conséquence du péché de David avec Bath-Shéba.-->.
15:2	Absalom se levait de bon matin et se tenait au bord du chemin de la porte. Et chaque fois qu'un homme ayant une contestation se rendait auprès du roi pour obtenir justice, Absalom l'appelait et lui disait : De quelle ville es-tu ? Et il répondait : Ton serviteur est de l'une des tribus d'Israël.
15:3	Absalom lui disait : Vois ! Ta cause est bonne et droite, mais personne de chez le roi ne t'écoutera.
15:4	Absalom disait encore : Qui m'établira juge dans le pays ? Tout homme qui aurait une contestation et un procès viendrait vers moi, et je lui ferais justice.
15:5	Et il arrivait aussi que quand quelqu'un s'approchait de lui pour se prosterner, il lui tendait sa main, le saisissait et l'embrassait.
15:6	Absalom faisait ainsi à tous ceux d'Israël qui venaient vers le roi pour demander justice. Et Absalom gagnait les cœurs des hommes d'Israël.

### Conspiration d'Absalom

15:7	Et il arriva qu'au bout de 40 ans, Absalom dit au roi : Permets que j'aille à Hébron, pour accomplir le vœu que j'ai fait à YHWH.
15:8	Car quand ton serviteur demeurait à Guéshour en Syrie, il fit un vœu, en disant : Si YHWH me ramène, s'il me ramène à Yeroushalaim, je servirai YHWH.
15:9	Et le roi lui répondit : Va en paix. Et Absalom se leva et s'en alla à Hébron.
15:10	Absalom envoya des espions dans toutes les tribus d'Israël, pour dire : Aussitôt que vous entendrez le son du shofar, vous direz : Absalom est établi roi à Hébron !
15:11	200 hommes de Yeroushalaim qui avaient été invités s'en allèrent avec Absalom. Ils y allèrent en toute simplicité de cœur, ne sachant rien de cette affaire.
15:12	Pendant qu'Absalom offrait les sacrifices, il envoya chercher à la ville de Guilo, Achitophel, le Guilonite, conseiller de David. Il se forma une puissante conspiration, parce que le peuple était de plus en plus nombreux auprès d'Absalom.

### David fuit son fils Absalom

15:13	Un messager se rendit auprès de David et lui dit : Le cœur des hommes d'Israël s'est tourné vers Absalom.
15:14	Et David dit à tous ses serviteurs qui étaient avec lui à Yeroushalaim : Levez-vous, fuyons, car nous ne pourrons échapper à Absalom. Hâtez-vous de partir ! Sinon, il ne tarderait pas à nous atteindre, et il nous précipiterait dans le malheur et frapperait la ville du tranchant de l'épée.
15:15	Et les serviteurs du roi répondirent au roi : Tes serviteurs sont prêts à faire tout ce que le roi, notre seigneur, trouvera bon.
15:16	Le roi sortit, et toute sa maison le suivait, mais le roi laissa dix femmes, des concubines, pour garder la maison.
15:17	Le roi sortit, et tout le peuple le suivait, et ils s'arrêtèrent à Beth-Merkhak.
15:18	Tous ses serviteurs marchaient à côté de lui. Tous les Kéréthiens, tous les Péléthiens, et tous les Gathiens, au nombre de 600 hommes venus de Gath, pour être à sa suite, marchaient devant le roi.
15:19	Mais le roi dit à Ittaï de Gath : Pourquoi viendrais-tu aussi avec nous ? Retourne et reste avec le roi, car tu es étranger, et même tu vas retourner bientôt en ton lieu.
15:20	Tu es arrivé hier, et te ferais-je aujourd'hui errer çà et là avec nous ? Quant à moi, je m'en vais où je pourrai ! Retourne et fais retourner tes frères avec toi. Que la bonté et la vérité t'accompagnent !
15:21	Mais Ittaï répondit au roi et dit : YHWH est vivant, et le roi mon seigneur est vivant ! Quel que soit le lieu où le roi mon seigneur sera, soit pour mourir, soit pour vivre, ton serviteur y sera aussi.
15:22	David donc dit à Ittaï : Viens, et marche ! Alors Ittaï de Gath marcha avec tous ses hommes et tous les enfants qui étaient avec lui.
15:23	Et tout le pays pleurait à grands cris au passage du peuple. Puis le roi passa le torrent de Cédron, et tout le peuple passa en face du chemin qui mène au désert.

### L'arche de l'alliance à Yeroushalaim (Jérusalem)

15:24	Tsadok était aussi là, et avec lui, tous les Lévites portant l'arche de l'alliance d'Elohîm. Ils posèrent là l'arche d'Elohîm, et Abiathar montait pendant que tout le peuple achevait de sortir de la ville.
15:25	Le roi dit à Tsadok : Rapporte l'arche d'Elohîm dans la ville. Si je trouve grâce aux yeux de YHWH, il me ramènera, et il me fera voir l'arche et sa demeure.
15:26	Mais s'il dit : Je ne prends pas de plaisir en toi ! Me voici, qu'il fasse de moi ce qui sera bon à ses yeux.
15:27	Le roi dit encore au prêtre Tsadok : N'es-tu pas le voyant ? Retourne en paix dans la ville, avec Achimaats, ton fils, et Yehonathan, fils d'Abiathar, vos deux fils.
15:28	Voyez, j'attendrai dans les régions arides du désert, jusqu'à ce qu'on vienne m'apporter des nouvelles de votre part.
15:29	Ainsi, Tsadok et Abiathar rapportèrent l'arche d'Elohîm à Yeroushalaim, et ils y restèrent.
15:30	David monta par la montée des oliviers. Il montait en pleurant, la tête couverte et il marchait nu-pieds. Tout le peuple aussi qui était avec lui montait, chacun ayant la tête couverte. Et en montant ils pleuraient.
15:31	Alors on vint dire à David : Achitophel est parmi ceux qui ont conspiré avec Absalom. Et David dit : Je te prie, ô YHWH, tourne en folie le conseil d'Achitophel !

### Houshaï, espion pour David dans la cour d'Absalom

15:32	Et il arriva que quand David fut arrivé au sommet, où il se prosterna devant Elohîm, Houshaï, l'Arkien, vint au-devant de lui, la tunique déchirée et de la terre sur sa tête.
15:33	David lui dit : Tu me seras à charge si tu viens avec moi.
15:34	Au contraire, tu feras échouer en ma faveur les conseils d'Achitophel, si tu retournes à la ville et que tu dises à Absalom : Ô roi, je serai ton serviteur, comme je fus autrefois le serviteur de ton père, mais maintenant je serai ton serviteur.
15:35	Les prêtres Tsadok et Abiathar ne seront-ils pas là avec toi ? Tout ce que tu entendras de la maison du roi, tu le rapporteras aux prêtres Tsadok et Abiathar.
15:36	Voici, ils ont là avec eux leurs deux fils, Achimaats, fils de Tsadok, et Yehonathan, fils d'Abiathar, c'est par eux que vous me ferez savoir tout ce que vous aurez entendu.
15:37	Houshaï, l'ami de David, retourna donc dans la ville, et Absalom entra à Yeroushalaim.

## Chapitre 16

### Tsiba retrouve David en fuite

16:1	Quand David eut un peu dépassé le sommet, voici, Tsiba, serviteur de Mephibosheth, vint au-devant de lui avec deux ânes bâtés, sur lesquels il y avait 200 pains, 100 grappes de raisins secs, 100 de fruits d'été, et une outre de vin.
16:2	Le roi dit à Tsiba : Que veux-tu faire de cela ? Et Tsiba répondit : Les ânes serviront de montures pour la maison du roi, le pain et les autres fruits d'été sont pour nourrir les jeunes hommes, et le vin pour désaltérer ceux qui se seront fatigués dans le désert.
16:3	Le roi lui dit : Mais où est le fils de ton maître ? Et Tsiba répondit au roi : Voici, il est resté à Yeroushalaim, car il a dit : Aujourd'hui, la maison d'Israël me rendra le royaume de mon père.
16:4	Alors le roi dit à Tsiba : Voici, tout ce qui est à Mephibosheth est à toi. Et Tsiba dit : Je me prosterne ! Que je trouve grâce à tes yeux, ô roi, mon seigneur !

### Shimeï maudit le roi David

16:5	Le roi David était arrivé jusqu'à Bahourim. Et voici, il sortit de là un homme de la famille et de la maison de Shaoul, nommé Shimeï, fils de Guéra. Il sortit, il sortit en maudissant,
16:6	il jeta des pierres contre David, contre tous ses serviteurs, et contre tout le peuple. Tous les hommes vaillants étaient à la droite et à la gauche du roi.
16:7	Shimeï parlait ainsi en le maudissant : Sors, sors, homme de sang, homme de Bélial<!--Voir commentaire en De. 13:13.--> !
16:8	YHWH fait retomber sur toi tout le sang de la maison de Shaoul, à la place duquel tu régnais, et YHWH a mis le royaume entre les mains de ton fils, Absalom. Et te voilà dans ton malheur, parce que tu es un homme de sang !
16:9	Alors Abishaï, fils de Tserouyah, dit au roi : Pourquoi ce chien mort maudit-il le roi, mon seigneur ? Permets que je m'avance et que je lui ôte la tête.
16:10	Mais le roi répondit : Qu'ai-je à faire avec vous, fils de Tserouyah ? S'il maudit, c'est que YHWH lui a dit : Maudis David ! Qui donc lui dira : Pourquoi agis-tu ainsi ?
16:11	Et David dit à Abishaï et à tous ses serviteurs : Voici, mon propre fils, qui est sorti de mes entrailles, en veut à ma vie. À plus forte raison ce Benyamite ! Laissez-le et qu'il maudisse, car YHWH lui a parlé.
16:12	Peut-être YHWH regardera mon affliction, et que YHWH me rendra le bien au lieu des malédictions d'aujourd'hui.
16:13	David donc et ses hommes continuèrent leur chemin. Et Shimeï marchait sur le flanc de la montagne vis-à-vis de lui, continuant à maudire, jetant des pierres contre lui et de la poussière en l'air.
16:14	Le roi David et tout le peuple qui était avec lui arrivèrent fatigués et là ils se rafraîchirent.

### Abominations d'Absalom à Yeroushalaim (Jérusalem)

16:15	Absalom, et tout le peuple, les hommes d'Israël, étaient entrés dans Yeroushalaim ; et Achitophel était avec lui.
16:16	Et il arriva que quand Houshaï, l'Arkien, l'ami intime de David, fut arrivé auprès d'Absalom, il lui dit : Vive le roi ! Vive le roi !
16:17	Et Absalom dit à Houshaï : Est-ce donc là l'affection que tu as pour ton ami intime ? Pourquoi n'es-tu pas allé avec ton ami ?
16:18	Houshaï répondit à Absalom : Non, mais je serai à celui qui a été choisi par YHWH, par ce peuple et par tous les hommes d'Israël, et je demeurerai avec lui.
16:19	D'ailleurs, qui servirai-je ? Ne sera-ce pas devant son fils ? Comme j'ai servi devant ton père, ainsi je serai devant toi.
16:20	Alors Absalom dit à Achitophel : Donnez un conseil sur ce que nous ferons.
16:21	Et Achitophel dit à Absalom : Va vers les concubines que ton père a laissées pour garder la maison. Ainsi tout Israël saura que tu t'es rendu odieux envers ton père, et les mains de tous ceux qui sont avec toi se fortifieront.
16:22	On dressa une tente pour Absalom sur le toit et Absalom alla vers les concubines de son père, aux yeux de tout Israël<!--2 S. 12:11-12.-->.
16:23	Or les conseils que donnait Achitophel en ce temps-là étaient autant estimés que si un homme avait consulté la parole d'Elohîm. C'est ainsi qu'on considérait tous les conseils qu'Achitophel donnait, tant à David qu'à Absalom.

## Chapitre 17

### Le conseil d'Achitophel dissipé

17:1	Après cela, Achitophel dit à Absalom : Je choisirai maintenant 12 000 hommes, et je me lèverai, et je poursuivrai David cette nuit.
17:2	Je l'atteindrai pendant qu'il est fatigué et que ses mains sont affaiblies. Je le ferai tellement trembler que tout le peuple qui est avec lui s'enfuira. Je frapperai seulement le roi,
17:3	et je ramènerai à toi tout le peuple. L'homme que tu cherches vaut le retour de tous et tout le peuple sera en paix.
17:4	Cette parole fut bonne aux yeux d'Absalom et aux yeux de tous les anciens d'Israël.
17:5	Cependant Absalom dit : Qu'on appelle maintenant aussi Houshaï, l'Arkien, et que nous entendions aussi son avis.
17:6	Houshaï vint vers Absalom et Absalom lui dit : Achitophel a donné un tel avis. Devons-nous faire ce qu'il a dit ou non ? Parle, toi aussi.
17:7	Alors Houshaï dit à Absalom : Cette fois, le conseil qu'Achitophel a donné n'est pas bon.
17:8	Houshaï dit encore : Tu connais ton père et ses hommes, ce sont des hommes vaillants, et ils ont l'amertume dans l'âme comme une ourse des champs privée de ses petits. Ton père est un homme de guerre, il ne passera pas la nuit avec le peuple.
17:9	Voici, il est maintenant caché dans une fosse ou dans un autre endroit. Et il arrivera que, si quelques-uns tombent dès le commencement, quelqu'un l'apprendra, il l'apprendra et dira : Il y a un massacre parmi le peuple qui suit Absalom !
17:10	Alors même un fils talentueux, dont le cœur est comme un cœur de lion se fondra, il se fondra. En effet, tout Israël sait que ton père est un homme de cœur, et que ceux qui sont avec lui sont des fils talentueux.
17:11	Je conseille donc que tout Israël se rassemble auprès de toi, depuis Dan jusqu'à Beer-Shéba, multitude pareille au sable qui est sur le bord de la mer, et qu'en personne tu marches au combat.
17:12	Alors nous l'atteindrons dans un des lieux où il se trouve et nous tomberons sur lui comme la rosée tombe sur la terre. Il ne lui restera aucun de tous les hommes qui sont avec lui.
17:13	S'il se retire dans une ville, tout Israël portera des cordes vers cette ville-là, et nous la traînerons jusqu'au torrent, jusqu'à ce qu'on n'en trouve plus une pierre.
17:14	Alors Absalom et tous les hommes d'Israël dirent : Le conseil de Houshaï, l'Arkien, est meilleur que le conseil d'Achitophel. Car YHWH avait résolu de faire échouer le bon conseil d'Achitophel afin que YHWH fasse venir le malheur sur Absalom.

### Houshaï avertit David du danger

17:15	Alors Houshaï dit aux prêtres Tsadok et Abiathar : Achitophel a donné tel et tel conseil à Absalom, et aux anciens d'Israël, mais moi, j'ai conseillé telle et telle chose.
17:16	Maintenant donc, envoyez tout de suite informer David, en disant : Ne passe pas la nuit dans les régions arides du désert, mais passe, passe de peur que le roi et tout le peuple qui est avec lui ne soient engloutis.
17:17	Yehonathan et Achimaats se tenaient près de la fontaine de Roguel. Une servante vint leur dire d'aller informer le roi David, car ils n'osaient pas se montrer et entrer dans la ville.
17:18	Mais un garçon les aperçut, et le rapporta à Absalom. Et ils partirent tous deux en hâte et ils arrivèrent à Bahourim, à la maison d'un homme qui avait un puits dans sa cour, dans lequel ils descendirent.
17:19	La femme de cet homme prit une couverture, qu'elle étendit sur l'ouverture du puits, et y répandit dessus du grain pilé en sorte qu'on ne s'aperçut de rien.
17:20	Les serviteurs d'Absalom entrèrent dans la maison auprès de cette femme et lui dirent : Où sont Achimaats et Yehonathan ? La femme leur répondit : Ils ont passé le ruisseau. Ils cherchèrent, et ne les trouvant pas, ils retournèrent à Yeroushalaim.
17:21	Après leur départ, ils<!--Achimaats et Yehonathan.--> remontèrent du puits et allèrent informer le roi David. Ils lui dirent : Levez-vous, et hâtez-vous de passer l'eau, car Achitophel a conseillé telle chose contre vous.
17:22	Alors David et tout le peuple qui était avec lui se levèrent et ils passèrent le Yarden. À la lumière du matin, il n'en manqua pas un qui n'eût passé le Yarden.
17:23	Or Achitophel voyant qu'on n'avait pas fait ce qu'il avait conseillé, fit seller son âne, se leva, et s'en alla en sa maison, dans sa ville. Après avoir donné des ordres à sa maison, il s'étrangla et mourut. On l'enterra dans le sépulcre de son père.

### Absalom et Israël en marche contre David

17:24	David arriva à Mahanaïm. Et Absalom passa le Yarden, lui et tous les hommes d'Israël avec lui.
17:25	Absalom établit Amasa sur l'armée, à la place de Yoab. Or Amasa était fils d'un homme nommé Yithra, l'Israélite, qui était allé vers Abigaïl, fille de Nachash, et sœur de Tserouyah, mère de Yoab.
17:26	Israël et Absalom campèrent dans le pays de Galaad.

### Mahanaïm bienveillant envers David

17:27	Or il arriva qu'aussitôt que David fut arrivé à Mahanaïm, Shobi, fils de Nachash de Rabba, des fils d'Ammon, Makir, fils d'Ammiel de Lodebar, et Barzillaï, le Galaadite de Roguelim,
17:28	apportèrent des lits, des bassins, des vases de terre, du froment, de l'orge, de la farine, du grain rôti, des fèves, des lentilles, des pois rôtis,
17:29	du miel, de la crème, des brebis, et des fromages de vache. Ils apportèrent ces choses à David et au peuple qui était avec lui, afin qu'ils mangent, car ils disaient : Ce peuple a dû souffrir de la faim, de la fatigue et de la soif dans le désert.

## Chapitre 18

### Bataille dans la forêt d'Éphraïm ; instructions de David sur Absalom

18:1	David fit le dénombrement du peuple qui était avec lui, et il établit sur eux des chefs de milliers et des chefs de centaines.
18:2	David envoya le peuple, un tiers sous le commandement de Yoab, un tiers sous le commandement d'Abishaï, fils de Tserouyah, frère de Yoab, et un tiers sous le commandement d'Ittaï, de Gath. Et le roi dit au peuple : Moi aussi, je sortirai, je sortirai avec vous.
18:3	Mais le peuple lui dit : Tu ne sortiras pas ! Car si nous nous enfuyons, si nous nous enfuyons, ils ne fixeraient pas le cœur sur nous. Même quand la moitié d'entre nous y serait tuée, ils ne fixeraient pas le cœur sur nous. Mais toi, tu es comme 10 000 d'entre nous, et maintenant il vaut mieux que de la ville tu puisses venir à notre secours.
18:4	Le roi leur répondit : Je ferai ce qui est bon à vos yeux. Le roi s'arrêta donc à la place de la porte, pendant que tout le peuple sortait par centaines et par milliers.
18:5	Le roi donna cet ordre à Yoab, à Abishaï, et à Ittaï, et dit : Par égard pour moi, doucement avec le jeune Absalom ! Et tout le peuple entendit ce que le roi commandait à tous les chefs au sujet d'Absalom.
18:6	Ainsi le peuple sortit dans les champs à la rencontre d'Israël, et la bataille eut lieu dans la forêt d'Éphraïm.
18:7	Là, le peuple d'Israël fut battu par les serviteurs de David, et il y eut en ce jour-là dans ce même lieu, une grande défaite de 20 000 hommes.
18:8	La bataille s'étendit sur toute la contrée, et la forêt dévora ce jour-là beaucoup plus de peuple que l'épée.

### Yoab tue Absalom

18:9	Absalom se retrouva devant les serviteurs de David. Il était monté sur un mulet. Le mulet entra sous les branches entrelacées d'un grand chêne, et la tête d'Absalom fut prise dans le chêne. Il demeura suspendu entre le ciel et la terre, et le mulet qui était sous lui passa outre.
18:10	Un homme ayant vu cela, le rapporta à Yoab et lui dit : Voici, j'ai vu Absalom suspendu à un chêne.
18:11	Et Yoab répondit à l'homme qui lui rapportait cela : Tu l'as vu ! Pourquoi ne l'as-tu pas abattu par terre ? Je t'aurais donné 10 sicles d'argent et une ceinture.
18:12	Mais cet homme dit à Yoab : Quand je pèserais dans ma main 1 000 pièces d'argent, je ne mettrais pas ma main sur le fils du roi. Car nous avons entendu ce que le roi vous a ordonné, à toi, à Abishaï et à Ittaï, en disant : Prenez garde chacun au jeune homme Absalom !
18:13	Et si j’avais agi frauduleusement contre son âme, toute l’affaire ne serait pas restée cachée au roi, et toi, tu te mettrais à l’écart.
18:14	Yoab répondit : Je ne m'attarderai pas auprès de toi ! Et il prit en sa main trois javelots, et les enfonça dans le cœur d'Absalom qui était encore vivant au milieu du chêne.
18:15	Puis dix jeunes hommes, qui portaient les armes de Yoab, entourèrent Absalom, le frappèrent et le firent mourir<!--La mort d'Absalom fut une conséquence du péché de David avec Bath-Shéba. Le péché a donc des conséquences graves et cause beaucoup de souffrances.-->.
18:16	Alors Yoab fit sonner le shofar et le peuple revint de la poursuite d'Israël, parce que Yoab le retint.
18:17	Ils prirent Absalom, le jetèrent dans la forêt dans une grande fosse, et mirent sur lui un très grand monceau de pierres. Tout Israël s'enfuit, chacun dans sa tente.
18:18	Or Absalom s'était fait ériger, de son vivant, un monument dans la vallée du roi. Il se disait en effet : Je n'ai pas de fils pour conserver la mémoire de mon nom. Et il donna son propre nom au monument, qu'on appelle encore aujourd'hui la place d'Absalom.

### David apprend la mort d'Absalom

18:19	Et Achimaats, fils de Tsadok, dit : Laisse-moi courir, et porter au roi la bonne nouvelle que YHWH lui a rendu justice en jugeant ses ennemis.
18:20	Yoab lui répondit : Tu ne seras pas aujourd'hui porteur de bonnes nouvelles. Tu le seras un autre jour, car aujourd'hui tu ne porterais pas de bonnes nouvelles, puisque le fils du roi est mort.
18:21	Et Yoab dit à un Éthiopien : Va, et annonce au roi ce que tu as vu. L'Éthiopien se prosterna devant Yoab, puis il se mit à courir.
18:22	Achimaats, fils de Tsadok, dit encore à Yoab : Quoi qu'il arrive, laisse-moi courir après l'Éthiopien. Yoab lui dit : Pourquoi veux-tu courir, mon fils, puisque tu n'as pas de bonnes nouvelles à apporter ?
18:23	Quoiqu'il arrive, je veux courir, reprit Achimaats. Et Yoab lui dit : Cours ! Achimaats courut par le chemin de la plaine, et il devança l'Éthiopien.
18:24	David était assis entre les deux portes. La sentinelle alla sur le toit de la porte vers la muraille. Elle leva les yeux et elle regarda. Et voici un homme qui courait tout seul.
18:25	Alors la sentinelle cria et avertit le roi. Le roi dit : S'il est seul, c'est qu'il y a des nouvelles dans sa bouche. Et cet homme marchait, marchait et se rapprochait.
18:26	Puis la sentinelle vit un autre homme qui courait. Elle cria au portier : Voici un homme qui court tout seul. Le roi dit : Il apporte aussi de bonnes nouvelles.
18:27	La sentinelle dit : La manière de courir du premier me paraît celle d'Achimaats, fils de Tsadok. Et le roi dit : C'est un homme bon, il vient quand il y a de bonnes nouvelles.
18:28	Alors Achimaats cria, et il dit au roi : Tout va bien ! Et il se prosterna devant le roi, le visage contre terre, et dit : Béni soit YHWH, ton Elohîm, qui a livré les hommes qui levaient leurs mains contre le roi, mon seigneur !
18:29	Le roi dit : Le jeune homme Absalom se porte-t-il bien ? Achimaats lui répondit : J'ai vu s'élever un grand tumulte au moment où Yoab envoya le serviteur du roi et moi ton serviteur, mais je ne sais pas exactement ce que c'était.
18:30	Et le roi lui dit : Mets-toi là de côté. Et Achimaats se tint de côté.
18:31	Aussitôt arriva l'Éthiopien. Et l'Éthiopien dit : Que le roi, mon seigneur apprenne, ces bonnes nouvelles ! Aujourd'hui, YHWH t'a rendu justice en jugeant tous ceux qui s'élevaient contre toi.
18:32	Le roi dit à l'Éthiopien : Le jeune homme Absalom se porte-t-il bien ? Et l'Éthiopien lui répondit : Que les ennemis du roi, mon seigneur, et tous ceux qui s'élèvent contre toi pour te faire du mal soient comme ce jeune homme !

## Chapitre 19

### Souffrance de David ; indignation de Yoab

19:1	Alors le roi, perturbé, monta à la chambre haute de la porte et alla pleurer. Il disait ainsi en marchant : Mon fils Absalom ! Mon fils, mon fils Absalom ! Pourquoi ne suis-je pas mort à ta place ? Absalom, mon fils, mon fils !
19:2	Et on fit ce rapport à Yoab : Voici, le roi pleure et se lamente à cause d'Absalom.
19:3	Ainsi, la victoire fut en ce jour-là changée en deuil pour tout le peuple, car en ce jour-là le peuple entendait dire : Le roi est affligé à cause de son fils.
19:4	Et ce même jour le peuple rentra dans la ville à la dérobée, comme l'auraient fait un peuple honteux d'avoir pris la fuite dans la bataille.
19:5	Le roi s'était couvert le visage, et il criait à grande voix : Mon fils Absalom ! Absalom, mon fils, mon fils !
19:6	Yoab entra dans la chambre où était le roi, et lui dit : Tu couvres aujourd'hui de confusion les faces de tous tes serviteurs, qui ont en ce jour sauvé ta vie, la vie de tes fils et de tes filles, la vie de tes femmes et de tes concubines.
19:7	Tu aimes ceux qui te haïssent et tu hais ceux qui t'aiment, car tu as proclamé aujourd’hui que chefs et serviteurs ne sont rien pour toi. Car je sais maintenant que, si Absalom vivait et si nous étions tous morts aujourd’hui, cela serait agréable à tes yeux.
19:8	Maintenant donc lève-toi, sors, et parle au cœur de tes serviteurs ! Car je jure par YHWH que si tu ne sors pas, il ne restera pas un seul homme avec toi cette nuit. Et ce mal sera pire que tous ceux qui te sont arrivés depuis ta jeunesse jusqu'à présent.

### Retour du roi David à Yeroushalaim (Jérusalem)

19:9	Alors le roi se leva et s'assit à la porte. On fit dire à tout le peuple : Voici, le roi est assis à la porte. Et tout le peuple vint devant le roi. Cependant, Israël s'était enfui, chaque homme dans sa tente.
19:10	Et dans toutes les tribus d'Israël, tout le peuple était en contestation, disant : Le roi nous a délivrés de la main de nos ennemis, c'est lui qui nous a sauvés de la main des Philistins, et maintenant il a dû fuir du pays devant Absalom.
19:11	Or Absalom, que nous avions oint pour roi sur nous, est mort dans la bataille. Maintenant donc, pourquoi ne parlez-vous pas de faire revenir le roi ?
19:12	Le roi David envoya dire aux prêtres Tsadok et Abiathar : Parlez aux anciens de Yéhouda et dites-leur : Pourquoi seriez-vous les derniers à ramener le roi dans sa maison ? Car les discours que tout Israël avait tenus étaient parvenus jusqu'au roi dans sa maison.
19:13	Vous êtes mes frères, vous êtes mes os et ma chair. Pourquoi seriez-vous les derniers à ramener le roi ?
19:14	Dites même à Amasa : N'es-tu pas mon os et ma chair ? Qu'ainsi me traite Elohîm et qu'ainsi il y ajoute si tu ne deviens pas devant moi pour toujours chef de l'armée à la place de Yoab !
19:15	Ainsi David fléchit le cœur de tous les hommes de Yéhouda comme s'ils n'avaient été qu'un seul homme, et ils envoyèrent dire au roi : Reviens, toi, et tous tes serviteurs.
19:16	Le roi revint et arriva jusqu'au Yarden. Yéhouda se rendit jusqu'à Guilgal, pour aller à la rencontre du roi afin de lui faire repasser le Yarden.
19:17	Et Shimeï, fils de Guéra, Benyamite, qui était de Bahourim, se hâta de descendre avec les hommes de Yéhouda à la rencontre du roi David.
19:18	Il avait avec lui 1 000 hommes de Benyamin, et Tsiba, serviteur de la maison de Shaoul, ses 15 enfants, et ses 20 serviteurs étaient aussi avec lui. Ils passèrent le Yarden en présence du roi.
19:19	Et ils traversèrent à gué pour faire passer la maison du roi et pour faire ce qui était bon à ses yeux. Au moment où le roi allait passer le Yarden, Shimeï, fils de Guéra, se prosterna devant lui.
19:20	Et il dit au roi : Que mon seigneur ne tienne pas compte de mon iniquité. Qu'il ne se souvienne pas du tort que ton serviteur a fait le jour où le roi mon seigneur sortait de Yeroushalaim, et que le roi ne le prenne pas à cœur !
19:21	Car ton serviteur sait qu'il a péché. Et voici, je suis venu aujourd'hui le premier de toute la maison de Yossef pour descendre à la rencontre du roi, mon seigneur.
19:22	Mais Abishaï, fils de Tserouyah, répondit et dit : À cause de cela, ne fera-t-on pas mourir Shimeï, puisqu'il a maudit le mashiah de YHWH ?
19:23	Et David dit : Qu'ai-je à faire avec vous, fils de Tserouyah ? Et pourquoi vous montrez-vous aujourd'hui mes adversaires ? Ferait-on mourir aujourd'hui quelqu'un en Israël ? Ne sais-je donc pas que je règne aujourd'hui sur Israël ?
19:24	Et le roi dit à Shimeï : Tu ne mourras pas ! Et le roi le lui jura.
19:25	Après cela, Mephibosheth, fils de Shaoul, descendit aussi à la rencontre du roi. Il n'avait pas lavé ses pieds, ni fait sa barbe, ni lavé ses vêtements, depuis que le roi s'en était allé, jusqu'au jour où il revenait en paix.
19:26	Il se trouva donc au-devant du roi comme il entrait dans Yeroushalaim, et le roi lui dit : Pourquoi n'es-tu pas venu avec moi, Mephibosheth ?
19:27	Et il lui répondit : Ô roi, mon seigneur, mon serviteur m'a trompé, car ton serviteur qui est boiteux avait dit : Je ferai seller mon âne, je monterai dessus et j'irai avec le roi.
19:28	Et il a calomnié ton serviteur auprès du roi, mon seigneur. Mais le roi mon seigneur est comme un ange d'Elohîm. Fais donc ce qui semblera bon à tes yeux.
19:29	En effet, toute la maison de mon père n’était que des hommes morts devant le roi, mon seigneur. Tu as en effet mis ton serviteur parmi ceux qui mangent à ta table. Quel droit puis-je encore avoir, pour me plaindre encore au roi ?
19:30	Et le roi lui dit : Pourquoi toutes ces paroles ? Je l'ai dit : Toi et Tsiba, vous partagerez les terres.
19:31	Et Mephibosheth dit au roi : Qu'il prenne même tout, puisque le roi mon seigneur rentre en paix dans sa maison.
19:32	Barzillaï, le Galaadite, descendit de Roguelim, et passa le Yarden avec le roi, pour l'accompagner jusqu'au-delà du Yarden.
19:33	Barzillaï était très vieux, âgé de 80 ans. Il avait nourri le roi pendant qu'il avait séjourné à Mahanaïm, car c'était un homme fort riche.
19:34	Le roi dit à Barzillaï : Viens avec moi, je te nourrirai chez moi à Yeroushalaim.
19:35	Mais Barzillaï répondit au roi : Combien d'années vivrai-je pour que je monte avec le roi à Yeroushalaim ?
19:36	Je suis aujourd'hui âgé de 80 ans. Puis-je encore discerner ce qui est bon de ce qui est mauvais ? Ton serviteur peut-il savourer ce qu'il mange et ce qu'il boit ? Puis-je encore entendre la voix des chanteurs et des chanteuses ? Et pourquoi ton serviteur serait-il encore à charge à mon seigneur, le roi ?
19:37	Ton serviteur ira un peu au-delà du Yarden avec le roi. Pourquoi le roi voudrait-il me donner une telle récompense ?
19:38	Je te prie que ton serviteur s'en retourne ! Que je meure dans ma ville, près du sépulcre de mon père et de ma mère ! Mais voici ton serviteur Kimham : il passera avec le roi mon seigneur. Fais pour lui ce qui sera bon à tes yeux.
19:39	Le roi dit : Que Kimham passe avec moi, et je ferai pour lui ce qui sera bon à tes yeux. Tout ce que tu voudras de moi, je te l'accorderai.
19:40	Tout le peuple passa donc le Yarden avec le roi. Puis le roi embrassa Barzillaï et le bénit. Et Barzillaï retourna dans sa demeure.

### Yéhouda et Israël se disputent le roi

19:41	Le roi passa à Guilgal et Kimham passa avec lui. Ainsi, tout le peuple de Yéhouda et même la moitié du peuple d'Israël firent passer le roi.
19:42	Mais voici, tous les hommes d'Israël vinrent vers le roi et lui dirent : Pourquoi nos frères, les hommes de Yéhouda, t'ont-ils enlevé, et ont-ils fait passer le Yarden au roi et à sa maison, et à tous les hommes de David ?
19:43	Alors tous les hommes de Yéhouda répondirent aux hommes d'Israël : Parce que nous sommes plus proches du roi. Pourquoi vous fâchez-vous à propos de cette chose ? Avons-nous mangé, mangé de ce qui est au roi ? Nous a-t-il fait des présents ?
19:44	Les hommes d'Israël répondirent aux hommes de Yéhouda et dirent : Nous avons dix parts au roi et même David nous appartient plus qu'à vous. Pourquoi nous avez-vous méprisés ? Notre parole n'a-t-elle pas été la première pour ramener notre roi ? Mais la parole des hommes de Yéhouda fut plus dure que la parole des hommes d'Israël.

## Chapitre 20

### Yéhouda reste fidèle au roi David

20:1	Et il se rencontra là un homme de Bélial<!--Voir commentaire en De. 13:13.-->, son nom était Shéba, fils de Bicri, Benyamite. Il sonna du shofar et dit : Nous n'avons pas de part avec David ni d'héritage avec le fils d'Isaï ! Israël, chaque homme à ses tentes !
20:2	Ainsi tous les hommes d'Israël se séparèrent de David et suivirent Shéba, fils de Bicri. Mais les hommes de Yéhouda s'attachèrent à leur roi et l'accompagnèrent depuis le Yarden jusqu'à Yeroushalaim.
20:3	Et quand David rentra dans sa maison à Yeroushalaim, il prit les dix femmes, les concubines qu'il avait laissées pour garder sa maison, et les mit dans un lieu où elles étaient gardées. Il pourvut à leur entretien, mais il n'alla pas vers elles. Ainsi, elles furent enfermées jusqu'au jour de leur mort, vivant dans le veuvage.

### Bataille contre Shéba ; Yoab tue Amasa

20:4	Puis le roi dit à Amasa : Rassemble-moi dans 3 jours les hommes de Yéhouda et toi, sois présent ici.
20:5	Amasa donc s'en alla pour rassembler Yéhouda, mais il tarda au-delà du temps que le roi lui avait fixé.
20:6	Alors David dit à Abishaï : Maintenant Shéba, fils de Bicri, nous fera plus de mal qu'Absalom. Prends toi-même les serviteurs de ton maître et poursuis-le, de peur qu'il ne trouve pour lui des villes fortifiées et n'échappe à nos yeux.
20:7	Et Abishaï partit, suivi des hommes de Yoab, des Kéréthiens et des Péléthiens, et de tous les hommes vaillants. Ils sortirent de Yeroushalaim, pour poursuivre Shéba, fils de Bicri.
20:8	Et comme ils furent près de la grande pierre qui est à Gabaon, Amasa vint au-devant d'eux. Yoab était ceint d'une épée par-dessus les habits dont il était revêtu. Elle était attachée à ses reins dans le fourreau, et comme il s'avançait, elle tomba.
20:9	Yoab dit à Amasa : Te portes-tu bien, mon frère ? Puis Yoab prit de sa main droite la barbe d'Amasa pour l'embrasser.
20:10	Amasa ne prit pas garde à l'épée qui était dans la main de Yoab, et Yoab l'en frappa au ventre et répandit ses entrailles à terre, sans le frapper une seconde fois. Et il mourut. Après cela, Yoab et Abishaï, son frère, poursuivirent Shéba, fils de Bicri.
20:11	Un des serviteurs de Yoab resta près d'Amasa, et il disait : Qui aime Yoab et qui est pour David ? Qu'il suive Yoab !
20:12	Et Amasa se roulait dans son sang au milieu de la route. Voyant que tout le peuple s'arrêtait, cet homme le poussa hors de la route dans un champ et jeta sur lui un vêtement. C'est ce qu'il fit quand il vit que tous ceux qui arrivaient près de lui s'arrêtaient.
20:13	Quand il fut ôté de la route, tous les hommes qui suivaient Yoab passaient au-delà, afin de poursuivre Shéba, fils de Bicri.

### La mort de Shéba

20:14	Yoab passa par toutes les tribus d'Israël jusqu'à Abel-Beth-Ma'akah, avec tous les Bériens, qui s'étaient rassemblés et qui l'avaient suivi.
20:15	Ils vinrent assiéger Shéba dans Abel-Beth-Ma'akah et ils élevèrent contre la ville un tertre qui atteignait le rempart. Tout le peuple qui était avec Yoab sapait la muraille pour la faire tomber.
20:16	Lorsqu'une femme sage de la ville se mit à crier : Écoutez, écoutez ! Dites, je vous prie, à Yoab : Approche jusqu'ici, je veux te parler !
20:17	Il s'approcha d'elle, et la femme dit : Es-tu Yoab ? Il répondit : Je le suis. Elle lui dit : Écoute les paroles de ta servante. Il répondit : J'écoute.
20:18	Et elle dit : Autrefois on avait coutume de dire : Que l'on consulte Abel ! Et tout se terminait ainsi.
20:19	Je suis une des cités paisibles et fidèles en Israël, et tu cherches à détruire une ville qui est une mère en Israël ! Pourquoi détruirais-tu l'héritage de YHWH ?
20:20	Yoab lui répondit : Loin de moi ! Loin de moi de vouloir engloutir et détruire !
20:21	La chose n'est pas ainsi. Mais un homme de la Montagne d'Éphraïm, nommé Shéba, fils de Bicri, a levé sa main contre le roi David. Livrez-le, lui seul et je m'éloignerai de la ville. La femme dit à Yoab : Voici, sa tête te sera jetée par-dessus la muraille.
20:22	Et la femme alla vers tout le peuple, et leur parla sagement, et ils coupèrent la tête de Shéba, fils de Bicri, et la jetèrent à Yoab. Alors il sonna du shofar. On se dispersa loin de la ville, et chacun s'en alla dans sa tente. Puis Yoab retourna vers le roi à Yeroushalaim.
20:23	Yoab était le chef de toute l'armée d'Israël ; Benayah, fils de Yehoyada, était à la tête des Kéréthiens et des Péléthiens.
20:24	Adoram était préposé aux impôts. Yehoshaphat, fils d'Ahiloud, était archiviste.
20:25	Sheva était le secrétaire ; Tsadok et Abiathar étaient les prêtres,
20:26	et Ira de Yaïr aussi était prêtre de David.

## Chapitre 21

### Vengeance des Gabaonites sur la maison de Shaoul (Saül)

21:1	Or il y eut du temps de David, une famine qui dura 3 ans, année après année. David chercha la face de YHWH, et YHWH lui répondit : C'est à cause de Shaoul et de sa maison sanguinaire, parce qu'il a fait mourir les Gabaonites.
21:2	Alors le roi appela les Gabaonites pour leur parler. Or les Gabaonites n'étaient pas des enfants d'Israël, mais un reste des Amoréens. Les enfants d'Israël leur avaient juré de les laisser vivre<!--Jos. 9.-->, mais Shaoul dans son zèle pour les enfants d'Israël et de Yéhouda, avait cherché à les faire mourir.
21:3	Et David dit aux Gabaonites : Que ferais-je pour vous, et par quel moyen vous apaiserai-je, afin que vous bénissiez l'héritage de YHWH ?
21:4	Les Gabaonites lui répondirent : Il ne s'agit pas pour nous d'argent ou d'or avec Shaoul et avec sa maison, et ce n'est pas à nous de faire mourir un homme en Israël. Le roi leur dit : Que voulez-vous donc que je fasse pour vous ?
21:5	Ils répondirent au roi : Puisque cet homme nous a consumés, et qu'il avait résolu de nous exterminer pour nous faire disparaître de tout le territoire d'Israël,
21:6	qu'on nous livre sept hommes d'entre ses fils, et nous les pendrons devant YHWH à Guibea de Shaoul, l'élu de YHWH. Et le roi dit : Je vous les livrerai.
21:7	Le roi épargna Mephibosheth, fils de Yehonathan, fils de Shaoul, à cause du serment que David et Yehonathan, fils de Shaoul, avaient fait entre eux, devant YHWH.
21:8	Mais le roi prit les deux fils que Ritspa, fille d'Ayah, avait enfantés à Shaoul, Armoni et Mephibosheth, et les cinq fils que Mérab, fille de Shaoul, avait enfantés à Adriel de Mehola, fils de Barzillaï,
21:9	et il les livra entre les mains des Gabaonites, qui les pendirent sur la montagne, devant YHWH. Tous les sept furent tués ensemble. On les fit mourir dans les premiers jours de la moisson, au commencement de la moisson des orges.
21:10	Alors Ritspa, fille d'Ayah, prit un sac et l'étendit sous elle au-dessus d'un rocher, depuis le commencement de la moisson jusqu'à ce que l'eau du ciel tombe sur eux. Elle ne permit pas aux oiseaux du ciel de s'approcher d'eux pendant le jour, ni aux bêtes des champs pendant la nuit.
21:11	On informa David de ce qu'avait fait Ritspa, fille d'Ayah, concubine de Shaoul.
21:12	Et David alla prendre les os de Shaoul et les os de Yehonathan, son fils, chez les habitants de Yabesh en Galaad, qui les avaient enlevés de la place de Beth-Shan, où les Philistins les avaient pendus lorsqu'ils tuèrent Shaoul à Guilboa.
21:13	Il emporta de là les os de Shaoul et les os de Yehonathan, son fils. On recueillit aussi les os de ceux qui avaient été pendus.
21:14	On les enterra avec les os de Shaoul et de Yehonathan, son fils, au pays de Benyamin, à Tséla, dans le sépulcre de Kis, père de Shaoul. Et l'on fit tout ce que le roi avait ordonné. Après cela, Elohîm fut apaisé envers le pays.

### Nouvelles batailles contre les Philistins

21:15	Il y eut encore une guerre entre les Philistins et Israël. David descendit avec ses serviteurs et ils combattirent tellement contre les Philistins que David fut épuisé.
21:16	Et Yishbo-Benob, qui était un des enfants de géant, eut l'intention de tuer David. Il avait une lance dont le fer pesait 300 sicles de cuivre, et il était ceint d'une armure neuve.
21:17	Mais Abishaï, fils de Tserouyah, vint au secours de David, frappa le Philistin, et le tua. Alors les gens de David jurèrent, en disant : Tu ne sortiras plus avec nous à la bataille, de peur que tu n'éteignes la lampe d'Israël.
21:18	Après cela, il y eut encore une autre guerre à Gob avec les Philistins. Sibbecaï, le Houshatite, tua Saph, qui était un des enfants de géant.
21:19	Il y eut encore une autre guerre à Gob avec les Philistins. Et Elchanan, fils de Ya`arey Oregiym, de Bethléhem, tua le frère de Goliath de Gath, qui avait une lance dont le bois était comme une ensouple de tisserand.
21:20	Il y eut encore une guerre à Gath. Il s'y trouva un homme de haute taille, qui avait 6 doigts à chaque main, et 6 orteils à chaque pied, en tout 24, lequel était aussi issu de géant.
21:21	Il jeta un défi à Israël et Yehonathan, fils de Shimea, frère de David, le tua.
21:22	Ces quatre-là étaient nés à Gath, de la race de géant. Ils moururent par les mains de David et par les mains de ses serviteurs.

## Chapitre 22

### Louange à YHWH, l'Elohîm qui délivre

22:1	Après cela, David adressa à YHWH les paroles de ce cantique, le jour où YHWH l'eut délivré de la main de tous ses ennemis, et de la main de Shaoul.
22:2	Il dit : YHWH est mon Rocher, ma forteresse, mon libérateur.
22:3	Elohîm est mon Rocher où je me réfugie, mon bouclier et la corne de mon salut<!--Voir Ps. 18:3 ; Lu. 1:69.-->, ma haute retraite et mon refuge. Ô mon Sauveur ! Tu me délivres de la violence.
22:4	Je m'écrie : Loué soit YHWH ! Et je suis délivré de mes ennemis<!--Ps. 18:4.-->.
22:5	Car les flots de la mort m'avaient environné, les torrents de Bélial<!--Voir De. 13:13.--> m'avaient terrifié.
22:6	Les cordes du shéol m'avaient entouré, les filets de la mort m'avaient surpris.
22:7	Dans ma détresse, j'ai invoqué YHWH, j'ai crié à mon Elohîm. De son temple, il a entendu ma voix, et mon cri est parvenu à ses oreilles.
22:8	Alors la Terre fut ébranlée et trembla, les fondements des cieux s'agitèrent, et ils furent ébranlés, parce qu'il était irrité.
22:9	Une fumée montait de ses narines, et de sa bouche sortait un feu dévorant : il en jaillissait des charbons embrasés.
22:10	Il abaissa les cieux, et descendit : il y avait des ténèbres épaisses sous ses pieds.
22:11	Il était monté sur un chérubin, et il volait, il paraissait sur les ailes du vent.
22:12	Il mit autour de lui les ténèbres pour tabernacle, des amas d'eaux, des nuées épaisses.
22:13	Des charbons ardents étaient allumés par l'éclat qui le précédait.
22:14	YHWH tonna des cieux, Elyon fit retentir sa voix.
22:15	Il lança des flèches et dispersa mes ennemis, il lança des éclairs et les mit en déroute.
22:16	Alors le fond de la mer apparut, et les fondements de la terre habitable furent mis à découvert, par la menace de YHWH, par le souffle du vent de sa colère.
22:17	Il étendit sa main d'en haut, il me saisit, il me retira des grandes eaux ;
22:18	il me délivra de mon ennemi puissant, de ceux qui me haïssaient, car ils étaient plus forts que moi.
22:19	Ils m'avaient surpris au jour de ma détresse, mais YHWH fut mon appui.
22:20	Il m'a mis au large, il m'a sauvé, parce qu'il a pris son plaisir en moi.
22:21	YHWH m'a traité selon ma droiture, il m'a rendu selon la pureté de mes mains ;
22:22	parce que j'ai gardé les voies de YHWH, et que je ne me suis pas détourné de mon Elohîm.
22:23	Toutes ses ordonnances ont été devant moi, et je ne me suis pas écarté de ses statuts.
22:24	J'ai été intègre envers lui, et je me suis gardé de mon iniquité.
22:25	YHWH donc m'a rendu selon ma droiture, selon ma pureté devant ses yeux.
22:26	Avec celui qui est bon tu es bon, avec l'homme intègre tu es intègre,
22:27	avec celui qui est pur tu te montres pur, mais avec le pervers tu agis selon sa perversité.
22:28	Tu sauves le peuple qui s'humilie, et de ton regard, tu abaisses les orgueilleux.
22:29	Tu es ma lampe, ô YHWH ! Et YHWH éclaire mes ténèbres.
22:30	Avec toi je me précipite sur une troupe en armes, avec mon Elohîm je franchis une muraille.
22:31	La voie de El est parfaite, la parole de YHWH est éprouvée ; il est le bouclier de tous ceux qui se confient en lui.
22:32	Car qui est El, si ce n'est YHWH ? Qui est le rocher, si ce n'est notre Elohîm ?
22:33	C'est El qui est ma puissante forteresse et qui me conduit dans la voie droite.
22:34	Il a rendu mes pieds semblables à ceux des biches, et il me fait tenir debout sur mes lieux élevés.
22:35	Il exerce mes mains au combat, et mes bras tendent l'arc de cuivre.
22:36	Tu me donnes le bouclier de ton salut, et ton humilité me fait devenir plus grand.
22:37	Tu élargis le chemin sous mes pas, et mes pieds ne chancellent pas.
22:38	Je poursuis mes ennemis et je les détruis, je ne reviens qu'après les avoir exterminés.
22:39	Je les anéantis, je les transperce, ils ne se relèvent plus, et ils tombent sous mes pieds.
22:40	Tu me ceins de force pour le combat, tu fais plier sous moi mes adversaires.
22:41	Tu fais tourner le dos à mes ennemis devant moi et j'extermine ceux qui me haïssent.
22:42	Ils regardent autour d'eux, et il n'y a pas de sauveur ! Ils crient à YHWH, mais il ne leur répond pas !
22:43	Je les broie comme la poussière de la terre, je les écrase, je les foule, comme la boue des rues.
22:44	Tu me délivres des dissensions de mon peuple. Tu me gardes pour être chef des nations. Un peuple que je ne connaissais pas m'est asservi.
22:45	Les fils de l'étranger me flattent, dès qu'ils ont entendu parler de moi, ils m'ont obéi.
22:46	Les fils de l'étranger défaillent et sortent tremblants de leurs forteresses.
22:47	YHWH est vivant, et béni soit mon rocher ! Qu'Elohîm, le rocher de mon salut, soit exalté,
22:48	le El qui me donne vengeance, qui m'assujettit les peuples,
22:49	et qui me fait échapper à mes ennemis ! Tu m'élèves au-dessus de mes adversaires, tu me délivres de l'homme violent.
22:50	C'est pourquoi, ô YHWH, je te louerai parmi les nations et je chanterai des psaumes à ton Nom.
22:51	C'est lui qui est la Tour de délivrance de son roi, et qui fait miséricorde à son mashiah, à David, et à sa postérité, à jamais.

## Chapitre 23

### Paroles prophétiques de David

23:1	Voici les dernières paroles de David. Parole de David, fils d'Isaï, parole de l'homme qui a été élevé, du mashiah d'Elohîm de Yaacov, du chantre agréable d'Israël :
23:2	L'Esprit de YHWH parle par moi, et sa parole est sur ma langue.
23:3	L'Elohîm d'Israël a parlé, le Rocher<!--Voir commentaire Es. 8:13-17.--> d'Israël m'a dit : Le juste dominateur<!--Il est question ici du Mashiah voir Mi. 5:1 et 2 Pi. 1:19.--> des humains, le dominateur ayant la crainte d'Elohîm
23:4	est comme la lumière du matin quand le soleil se lève, un matin sans nuage : par son éclat l'herbe verte germe de la terre après la pluie.
23:5	N'en est-il pas ainsi de ma maison devant El, puisqu'il a traité avec moi une alliance éternelle, bien ordonnée et gardée ? Tout mon salut et tout mon plaisir, ne les fera-t-il pas germer ?
23:6	Mais les Bélials<!--Voir De. 13:13.--> sont tous comme des épines que l'on jette au loin parce qu'on ne les prend pas avec la main.
23:7	Celui qui les touche, s'arme du fer ou du bois d'une lance, et on les brûle au feu sur place.

### Les vaillants hommes de David<!--1 Ch. 11:10-47.-->

23:8	Voici les noms des vaillants hommes qui étaient au service de David. Yosheb-Basshébeth, le Tachkemonite, était l'un des principaux chefs. C'était Hadino le Hetsnite, qui eut le dessus sur 800 hommes qu'il tua en une seule fois.
23:9	Après lui, Èl’azar, fils de Dodo, fils d'Achochi. Il était l'un des trois vaillants hommes qui étaient avec David lorsqu'ils défièrent les Philistins rassemblés pour combattre, tandis que les hommes d'Israël se retiraient.
23:10	Il se leva et frappa les Philistins jusqu'à ce que sa main fut lasse et qu'elle restât attachée à l'épée. Ce jour-là, YHWH opéra une grande délivrance. Le peuple revint après Èl’azar, seulement pour prendre les dépouilles.
23:11	Après lui, Shammah, fils d'Agué d'Harar. Les Philistins s'étaient rassemblés en troupe. Il y avait là une parcelle de champ pleine de lentilles et le peuple fuyait devant les Philistins.
23:12	Shammah se mit au milieu de cette parcelle, la défendit, et frappa les Philistins. Et YHWH opéra une grande délivrance.
23:13	Trois des 30 chefs descendirent au temps de la moisson et vinrent vers David, dans la caverne d'Adoullam, lorsqu'une troupe de Philistins était campée dans la vallée des géants.
23:14	David était alors dans la forteresse, et la garnison des Philistins était en ce temps-là à Bethléhem.
23:15	Et David eut un désir et dit : Qui est-ce qui me fera boire de l'eau de la citerne qui est à la porte de Bethléhem ?
23:16	Alors ces trois vaillants hommes passèrent au travers du camp des Philistins et puisèrent de l'eau de la citerne qui est à la porte de Bethléhem. Ils l'apportèrent, et ils la présentèrent à David ; mais il ne voulut pas la boire, et il la répandit devant YHWH.
23:17	Car il dit : Loin de moi, ô YHWH, de faire une telle chose ! N'est-ce pas le sang de ces hommes qui sont allés au péril de leur vie ? Il ne voulut pas la boire. Voilà ce que firent ces trois vaillants hommes.
23:18	Il y avait aussi Abishaï, frère de Yoab, fils de Tserouyah, qui était le chef des trois. Il brandit sa lance sur 300 hommes, les blessa à mort et il se fit un nom parmi les trois.
23:19	Il était le plus considéré des trois, et il fut leur chef. Cependant il n'égala pas les trois premiers.
23:20	Benayah, fils de Yehoyada, fils d'un vaillant homme, qui avait fait de nombreuses actions, originaire de Kabtseel. C'est lui qui frappa les deux héros de Moab. Il descendit au milieu d'une fosse, où il frappa un lion, un jour de neige.
23:21	C'est encore lui qui tua un Égyptien, un homme d'un aspect redoutable. Cet Égyptien avait en main une lance. Il descendit vers lui, armé d'un bâton, arracha la lance de la main de l'Égyptien et le tua avec sa propre lance.
23:22	Voilà ce que fit Benayah, fils de Yehoyada. Il se fit un nom parmi les trois vaillants hommes.
23:23	Il était le plus honoré des trente, mais il n'égala pas les trois premiers. C'est pourquoi David l'établit dans son conseil secret.
23:24	Asaël, frère de Yoab, était des trente. Elchanan, fils de Dodo, de Bethléhem.
23:25	Shammah, de Harod. Élika, de Harod.
23:26	Hélets, de Péleth. Ira, fils d'Ikkesh, de Tekoa.
23:27	Abiézer, d'Anathoth. Mebounnaï, de Housha.
23:28	Tsalmon, d'Achoach. Maharaï, de Nethopha.
23:29	Héleb, fils de Baana, de Nethopha. Ittaï, fils de Ribaï, de Guibea des fils de Benyamin.
23:30	Benayah, de Pirathon. Hiddaï, de Nachalé-Gaash.
23:31	Abi-Albon, d'Araba. Azmaveth, de Barhoum.
23:32	Éliachba, de Shaalbon. Ben-Yashen. Yehonathan.
23:33	Shammah, d'Harar. Achiam, fils de Sharar, d'Arar.
23:34	Éliphéleth, fils d'Achasbaï, fils d'un Maakathien. Éliam, fils d'Achitophel, de Guilo.
23:35	Hetsro, de Carmel. Paaraï, d'Arab.
23:36	Yigal, fils de Nathan, de Tsoba. Bani, de Gad.
23:37	Tsélek, l'Ammonite. Naharaï, de Beéroth, qui portait les armes de guerre de Yoab, fils de Tserouyah.
23:38	Ira, de Yithriy. Gareb, de Yithriy.
23:39	Ouriyah, le Héthien. En tout, 37.

## Chapitre 24

### Péché de David ; plaie mortelle sur Israël<!--1 Ch. 21:1-17.-->

24:1	La colère de YHWH s'enflamma encore contre Israël et il incita David contre eux, en disant : Va, fais le dénombrement d'Israël et de Yéhouda<!--1 Ch. 21.--> !
24:2	Le roi dit donc à Yoab, chef de l'armée qui se trouvait près de lui : Parcours toutes les tribus d'Israël, depuis Dan jusqu'à Beer-Shéba ; et dénombre le peuple, afin que je sache le nombre du peuple.
24:3	Yoab dit au roi : Que YHWH, ton Elohîm, veuille augmenter ton peuple 100 fois plus, et que les yeux du roi mon seigneur le voient ! Mais pourquoi le roi mon seigneur prend-il plaisir à cela ?
24:4	Néanmoins, la parole du roi l'emporta sur Yoab, et sur les chefs de l'armée. Yoab et les chefs de l'armée sortirent de la présence du roi pour dénombrer le peuple d'Israël.
24:5	Ils passèrent le Yarden et ils campèrent à Aroër, au sud de la ville qui est au milieu de la vallée du torrent de Gad, et vers Ya`azeyr.
24:6	Ils allèrent en Galaad et dans le territoire de ceux qui habitent vers le bas du pays de Thachthim-Hodshi. Ils allèrent à Dan Ya`an, et aux environs de Sidon.
24:7	Ils vinrent jusqu'à la forteresse de Tyr, et dans toutes les villes des Héviens et des Cananéens. Ils sortirent vers le midi de Yéhouda à Beer-Shéba.
24:8	Ainsi ils parcoururent tout le pays et arrivèrent à Yeroushalaim au bout de 9 mois et 20 jours.
24:9	Et Yoab donna au roi le rôle du dénombrement du peuple : il y avait en Israël 800 000 hommes talentueux tirant l'épée, et les hommes de Yéhouda étaient 500 000 hommes.
24:10	Alors David sentit battre son cœur, après qu'il eut fait ainsi dénombrer le peuple. Et David dit à YHWH : J'ai commis un grand péché en faisant cette chose ! Et maintenant, fais passer, je te prie, ô YHWH, l'iniquité de ton serviteur, car j'ai agi très follement !
24:11	Après cela, David se leva dès le matin, et la parole de YHWH vint à Gad le prophète, qui était le voyant de David :
24:12	Va dire à David : Ainsi parle YHWH : J'apporte trois choses contre toi. Choisis l'une d'elles et je l'exécuterai contre toi.
24:13	Gad alla vers David et lui rapporta cela en disant : Que veux-tu qu'il t'arrive : 7 ans de famine sur ton pays, ou que durant 3 mois tu fuies devant tes ennemis qui te poursuivront, ou que durant 3 jours la peste soit dans ton pays ? Sache maintenant et vois quelle parole je rapporterai à celui qui m'a envoyé.
24:14	David répondit à Gad : Je suis dans une très grande détresse ! Tombons entre les mains de YHWH, car sa miséricorde est grande. Mais que je ne tombe pas entre les mains des humains !
24:15	YHWH envoya donc la peste en Israël, depuis le matin jusqu'au temps fixé. Depuis Dan jusqu'à Beer-Shéba, il mourut 70 000 hommes parmi le peuple.
24:16	Mais quand l'ange étendait sa main sur Yeroushalaim pour la ravager, YHWH se repentit de ce mal, et dit à l'ange qui ravageait le peuple : C'est assez ! Retire maintenant ta main ! Or l'Ange de YHWH était près de l'aire d'Aravna, le Yebousien.
24:17	Car David voyant l'ange qui frappait le peuple, parla à YHWH, et dit : Voici, c'est moi qui ai péché ! C'est moi qui ai commis l'iniquité ! Mais ces brebis, qu'ont-elles fait ? Je te prie que ta main soit contre moi et contre la maison de mon père !

### Sacrifice de David ; YHWH met fin à la plaie<!--1 Ch. 21:18-30.-->

24:18	Ce jour-là, Gad vint vers David et lui dit : Monte et dresse un autel à YHWH dans l'aire d'Aravna, le Yebousien.
24:19	Et David monta, selon la parole de Gad, comme YHWH l'avait ordonné.
24:20	Aravna regarda, et vit le roi et ses serviteurs qui venaient vers lui ; et Aravna sortit, et se prosterna devant le roi, le visage contre terre.
24:21	Aravna dit : Pourquoi le roi mon seigneur vient-il vers son serviteur ? Et David répondit : Pour acheter ton aire et y bâtir un autel à YHWH, afin que cette plaie se retire de dessus le peuple.
24:22	Aravna dit à David : Que le roi mon seigneur prenne et offre ce qu'il lui plaira ! Regarde, les bœufs seront pour l'holocauste, et les chars avec l'attelage de bœufs serviront de bois.
24:23	Aravna donna tout cela au roi. Et Aravna dit au roi : Que YHWH, ton Elohîm, te soit favorable !
24:24	Mais le roi répondit à Aravna : Non ! Car je te l'achèterai, je te l'achèterai pour son prix. Je n'offrirai pas à YHWH, mon Elohîm, des holocaustes qui ne me coûtent rien. Ainsi, David acheta l'aire et les bœufs pour 50 sicles d'argent.
24:25	David bâtit là un autel à YHWH, et offrit des holocaustes et des sacrifices d'offrande de paix. Alors YHWH fut apaisé envers le pays, et la plaie se retira d'Israël.
