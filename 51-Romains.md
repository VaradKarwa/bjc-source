# Romains (Ro.)

Auteur : Paulos (Paul)

Thème : L'Évangile d'Elohîm

Date de rédaction : Env. 56 ap. J.-C.

Rome est une ville située dans la région du Latium, au centre de l'Italie, à la confluence de l'Aniene et du Tibre. Centre de l'Empire romain, elle domina l'Europe, l'Afrique du Nord et le Moyen-Orient du 1er siècle av. J.-C. au 5ème siècle ap. J.-C.

La lettre était destinée à l'assemblée de Rome, fondée sans doute par des chrétiens convertis grâce au service de Paulos (Paul) et d'autres apôtres itinérants. Cette assemblée comptait quelques Juifs, mais était surtout constituée des nations. L'épître de Paulos aux Romains fut rédigée au cours du troisième voyage missionnaire de Paulos, pendant les trois mois que l'apôtre passa à Corinthe.

En attendant de leur rendre visite, Paulos avait le désir de communiquer aux chrétiens de Rome les grandes lignes du principe de la grâce, dont il avait eu la révélation. Il aborde donc dans cette épître plusieurs doctrines majeures, comme le salut par la foi et la grâce, ainsi que des enseignements pratiques sur l'amour, le devoir du chrétien et la sainteté.

## Chapitre 1

### Introduction : L'Évangile du Mashiah (Christ), puissance d'Elohîm pour le salut de tous

1:1	Paulos, esclave de Yéhoshoua Mashiah, apôtre appelé, mis à part pour l'Évangile d'Elohîm,
1:2	qu'il avait auparavant promis par le moyen de ses prophètes dans les saintes Écritures,
1:3	concernant son Fils qui, selon la chair, est provenu de la semence de David,
1:4	déterminé Fils d'Elohîm avec puissance, selon l'Esprit de sainteté, par sa résurrection d'entre les morts : Yéhoshoua Mashiah, notre Seigneur.
1:5	Au moyen duquel, nous avons reçu la grâce et l'apostolat pour amener en son Nom toutes les nations à l'obéissance de la foi,
1:6	parmi lesquelles vous êtes aussi, vous qui êtes appelés par Yéhoshoua Mashiah.
1:7	À tous les bien-aimés d'Elohîm, appelés et saints<!--En hébreu, plusieurs termes sont employés pour parler de la sanctification. Il y a par exemple le verbe « qadash » qui signifie « consacrer », « sanctifier » et le nom commun « qodesh » qui signifie « mise à part », « sainteté », « consécration », « séparation ». Ces termes sont appliqués aux humains et aux objets, quand ceux-ci sont réservés pour le service d'Elohîm. Le terme grec « hagios » signifie « consacré à Elohîm », « saint », « sacré », « pieux ». Contrairement à ce qui est enseigné par certains, on ne devient pas saint après avoir été canonisé ou béatifié, mais par le sang de Yéhoshoua ha Mashiah, lorsqu'une personne l'accepte comme son Seigneur et Sauveur (Hé. 10:10, 13:12). Ainsi, sous la Nouvelle Alliance, tous les disciples de Yéhoshoua sont saints ou consacrés (Hé. 10:10-14 ; Ph. 1:1). Cet état de sainteté se maintient par l'action du Saint-Esprit, par le moyen de la parole d'Elohîm (Jn. 15:3, 17:17 ; 1 Th. 5:23-24 ; Ep. 5:25-26 ; 2 Co. 3:18). Ainsi, au moment de la conversion, le chrétien s'engage dans un processus de sanctification qui durera toute sa vie. La sainteté parfaite ne sera atteinte qu'au retour du Seigneur. Alors, tous ceux qui lui appartiennent, connaîtront la rédemption totale : corps, âme et esprit (1 Co. 15:29-50 ; Ep. 5:27 ; Ph. 3:20-21 ; 1 Jn. 3:2 ; Ap. 22:12).-->, qui sont à Rome : que la grâce et la paix vous soient données de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !
1:8	Premièrement, je rends vraiment grâce à mon Elohîm par Yéhoshoua Mashiah, au sujet de vous tous, parce qu’on publie votre foi dans le monde entier.
1:9	Car Elohîm, à qui je rends un culte en mon esprit, par l'Évangile de son Fils, m'est témoin que je fais sans cesse mention de vous.
1:10	Demandant toujours dans mes prières que je puisse enfin trouver, par la volonté d'Elohîm, quelque moyen favorable pour venir chez vous.
1:11	Car je désire ardemment vous voir pour vous communiquer quelque don de grâce spirituel, afin que vous soyez affermis,
1:12	c'est-à-dire, afin qu'étant parmi vous, je sois consolé avec vous, par le moyen de la foi qui nous est commune à vous et à moi.
1:13	Or mes frères, je ne veux pas que vous ignoriez que je me suis souvent proposé d'aller chez vous, afin de recueillir quelque fruit aussi bien parmi vous, que parmi les autres nations, mais j'en ai été empêché jusqu'à présent.
1:14	Je suis débiteur tant aux Grecs qu'aux Barbares, tant aux sages qu'aux dénués d'intelligence.
1:15	Ainsi, autant qu'il dépend de moi, je suis prêt à vous annoncer aussi l'Évangile, à vous qui êtes à Rome.
1:16	Car je n'ai pas honte de l'Évangile du Mashiah, vu qu'il est la puissance d'Elohîm pour le salut de tous ceux qui croient, du Juif premièrement, mais aussi du Grec,
1:17	parce qu'en lui est révélée la justice d'Elohîm de foi en foi, selon qu'il est écrit : Le juste vivra par la foi<!--Ha. 2:4.-->.

### Jugement sur ceux qui retiennent la vérité

1:18	Car la colère d'Elohîm se révèle du ciel contre toute impiété et toute injustice des humains qui retiennent la vérité dans l'injustice,
1:19	parce que ce qu'on peut connaître d'Elohîm est manifeste parmi eux, car Elohîm le leur a manifesté.
1:20	Car ses choses invisibles, mais aussi sa puissance éternelle et sa divinité, se voient clairement depuis la création du monde, quand on les considère dans ses ouvrages, pour qu'ils soient inexcusables,
1:21	parce qu'ayant connu Elohîm, ils ne l'ont pas glorifié comme Elohîm et ils ne lui ont pas rendu grâces, mais ils sont devenus vains dans leurs discours, et leur cœur destitué d'intelligence a été couvert par les ténèbres<!--Ep. 4:18.-->.
1:22	Prétendant être sages, ils sont devenus fous,
1:23	et ils ont changé la gloire d'Elohîm incorruptible en images<!--Ex. 20:4-5, 32:1-35 ; Mt. 22:20 ; Mc. 12:16 ; Lu. 20:24 ; Ap. 13:14-15, 14:9-11, 15:2, 16:2, 19:20, 20:4.--> représentant l'être humain corruptible, et des oiseaux, et des quadrupèdes, et des reptiles.

### Les conséquences de l'endurcissement des humains

1:24	C'est pourquoi aussi Elohîm les a livrés, par les désirs de leurs propres cœurs, à l'impureté pour déshonorer entre eux leurs propres corps<!--Elohîm les a livrés à l'esprit d'égarement (1 R. 22:21-24 ; 2 Th. 2:11-12).-->,
1:25	eux qui ont échangé la vérité d'Elohîm contre le mensonge et qui ont adoré la créature, en lui rendant un culte, au lieu du Créateur, qui est béni pour les âges. Amen !
1:26	C'est pourquoi Elohîm les a livrés à leurs passions<!--Vient du grec « pathos » qui signifie « tout ce qui peut arriver à quelqu'un, que ce soit triste ou joyeux », « affliction de l'esprit », « émotion », « passion ». Voir Mt. 19:4 ; Mc. 10:6.--> déshonorantes, car même les femelles parmi eux ont échangé les rapports sexuels naturels pour des relations contre nature.
1:27	De même aussi les mâles<!--Vient du grec « arrhen » qui fait allusion au sexe masculin. Voir Mt. 19:4 ; Mc. 10:6.-->, abandonnant les rapports sexuels naturels avec le sexe féminin<!--Vient du grec « thelus » qui signifie aussi « femelle ». Voir Mt. 19:4 ; Mc. 10:6.-->, se sont embrasés dans leurs désirs<!--Désir, désir ardent, besoin impérieux, convoitise. Le mot grec « orexis » est utilisé dans un bon et un mauvais sens, aussi bien naturel et licite et même de certains désirs (appétit pour la nourriture), que pour les désirs corrompus et illicites.--> les uns pour les autres, commettant mâle avec mâle des choses honteuses, et recevant en eux-mêmes la juste et correcte récompense de leur égarement.
1:28	Et comme ils n'ont pas jugé bon d'avoir la connaissance précise et correcte d'Elohîm, aussi Elohîm les a livrés<!--De. 28:28 ; 1 R. 22 ; 2 Th. 2:11.--> à un esprit désapprouvé<!--Vient du grec « adokimos » qui signifie « refusé à l'épreuve », « non approuvé », « celui qui ne prouve pas ce qu'il devrait », « faux », « réprouvé ». Ce terme est utilisé pour les monnaies et les poids. Voir 2 Ti. 2:15.-->, pour pratiquer des choses qui ne sont pas convenables,
1:29	étant remplis de toute espèce d'injustice, d'impureté, de malice, d'avarice, de malignité, pleins d'envie, de meurtre, de querelle, de tromperie et de mauvais caractère,
1:30	rapporteurs, médisants, haïssant Elohîm, outrageux, orgueilleux, vains, inventeurs de choses mauvaises, rebelles à leurs parents,
1:31	sans intelligence, ne tenant pas ce qu'ils ont promis, sans affection naturelle, sans traité ou alliance, sans miséricorde<!--Impitoyable.-->.
1:32	Lesquels ayant connu le jugement d'Elohîm qui déclare dignes de mort ceux qui pratiquent de telles choses, non seulement ils les font eux-mêmes, mais encore ils approuvent ceux qui les pratiquent.

## Chapitre 2

### Condamnation du moralisme

2:1	C'est pourquoi, ô humain, qui que tu sois, toi qui juges, tu es inexcusable. Car, en jugeant les autres, tu te condamnes toi-même, puisque toi qui juges, tu commets les mêmes choses.
2:2	Mais nous savons que le jugement d'Elohîm est selon la vérité, contre ceux qui pratiquent de telles choses.
2:3	Et penses-tu, ô humain, qui juges ceux qui pratiquent de telles choses et qui les fais toi-même, échapper au jugement d'Elohîm ?
2:4	Ou méprises-tu la richesse de sa bonté, de sa tolérance et de sa patience, ne reconnaissant pas que la douceur d'Elohîm te conduit à la repentance ?
2:5	Mais, par ta dureté et par ton cœur qui n'admet aucun changement de l'esprit, tu t'amasses la colère pour le jour de la colère et de la révélation du juste jugement d'Elohîm,
2:6	qui rendra à chacun selon ses œuvres :
2:7	la vie éternelle en effet à ceux qui, par leur persévérance dans les bonnes œuvres, cherchent la gloire, et l'honneur et l'immortalité.
2:8	Mais il y aura de l'indignation en effet et de la colère contre ceux qui ont un esprit de parti<!--Voir Ph. 2:3.-->, et qui sont rebelles à la vérité et obéissent à l'injustice.
2:9	Il y aura tribulation et angoisse sur toute âme humaine qui fait le mal, du Juif premièrement, mais aussi du Grec.
2:10	Mais gloire, honneur et paix à tout homme qui fait ce qui est bon, au Juif premièrement, mais aussi au Grec.
2:11	Car auprès d'Elohîm il n'y a pas d'égard à l'apparence des personnes<!--Ou partialité.-->.
2:12	Car tous ceux qui auront péché sans la connaissance de la torah, périront aussi sans la connaissance de la torah, et tous ceux qui auront péché ayant la torah, seront jugés par le moyen de la torah.
2:13	Car ce ne sont pas les auditeurs de la torah qui sont justes devant Elohîm, mais ce sont les observateurs de la torah qui seront justifiés.
2:14	Car, quand les nations qui n'ont pas la torah pratiquent naturellement les choses de la torah, n'ayant pas la torah, elles sont une torah pour elles-mêmes.
2:15	Et elles montrent par là, que l'œuvre de la torah est écrite dans leurs cœurs, car leur conscience en rend témoignage, et leurs pensées les accusent ou les défendent tour à tour,
2:16	au jour où Elohîm jugera les secrets des humains par Yéhoshoua Mashiah, selon mon Évangile.

### Condamnation du formalisme

2:17	Voici, tu portes le nom de Juif, tu te reposes entièrement sur la torah et tu te glorifies en Elohîm,
2:18	tu connais sa volonté et tu fais la différence entre le bien et le mal, étant instruit par la torah.
2:19	Et tu te persuades d'être le guide des aveugles, la lumière de ceux qui sont dans les ténèbres,
2:20	le professeur des insensés, le docteur des ignorants, ayant le modèle de la connaissance et de la vérité dans la torah.
2:21	Toi donc, qui enseignes les autres, tu ne t'enseignes pas toi-même ! Toi qui prêches qu'on ne doit pas voler, tu voles !
2:22	Toi qui dis qu'on ne doit pas commettre d'adultère, tu commets l'adultère ! Toi qui as en abomination les idoles, tu commets des sacrilèges !
2:23	Toi qui te glorifies de la torah, tu déshonores Elohîm par la transgression de la torah !
2:24	Car le Nom d'Elohîm est blasphémé parmi les nations à cause de vous, comme cela est écrit<!--Voir Es. 52:5.-->.
2:25	Car la circoncision est en effet utile si tu gardes la torah. Mais si tu es un transgresseur de la torah, ta circoncision devient incirconcision.
2:26	Si donc l'incirconcis observe les ordonnances de la torah, son incirconcision ne sera-t-elle pas tenue pour circoncision ?
2:27	Et l'incirconcis de nature qui accomplit la torah ne te jugera-t-il pas, toi qui la transgresses tout en ayant la lettre de la torah et la circoncision ?
2:28	Car le Juif, ce n'est pas celui qui en a les apparences<!--Paulos (Paul) dénonce ici le formalisme (2 Ti. 3:5). L'apparence de la piété correspond aux vêtements des brebis : « Gardez-vous des faux prophètes qui viennent à vous en habits de brebis, mais au-dedans, ce sont des loups ravisseurs » (Mt. 7:15). « Et je vis une autre bête montant de la Terre. Et elle avait deux cornes semblables à celles de l'Agneau, mais elle parlait comme le dragon » (Ap. 13:11). Cette bête a l'apparence d'un agneau, mais sa voix est celle du dragon, il s'agit de Satan.-->, et la circoncision, ce n'est pas celle qui est visible dans la chair.
2:29	Mais le Juif est celui qui l'est dans le secret, et la circoncision, c'est celle du cœur, selon l'Esprit et non selon la lettre. La louange de ce Juif ne vient pas des humains, mais d'Elohîm.

## Chapitre 3

### L'avantage du Juif

3:1	Quel est donc l'avantage du Juif, ou quelle est l'utilité de la circoncision ?
3:2	Grande de toute manière ! Et premièrement parce que les oracles d'Elohîm leur ont été confiés en effet.
3:3	Quoi ? Car si quelques-uns n'ont pas cru, leur incrédulité anéantira-t-elle la fidélité d'Elohîm ?
3:4	Non, sans doute ! qu'Elohîm soit vrai et tout être humain menteur, selon qu'il est écrit : Pour que tu sois justifié dans tes paroles et que tu sois victorieux lorsqu’on te juge<!--Ps. 51:6.-->.
3:5	Or si notre injustice établit la justice d'Elohîm, que dirons-nous ? Elohîm est-il injuste quand il déchaîne sa colère ? (Je parle à la manière des humains.)
3:6	Non, sans doute ! Autrement, comment Elohîm jugerait-il le monde ?
3:7	Et si, par mon mensonge, la vérité d'Elohîm a éclaté davantage pour sa gloire, pourquoi suis-je encore condamné comme pécheur ?
3:8	Et pourquoi ne ferions-nous pas le mal afin qu'il en arrive des bonnes choses, comme quelques-uns, qui nous calomnient, déclarent que nous le disons ? La condamnation de ces gens est juste !

### Juifs et Grecs, tous coupables devant Elohîm

3:9	Quoi donc ! Avons-nous la prééminence<!--« Avoir de l'avance sur un autre, avoir la prééminence sur un autre », « exceller », « surpasser », « surpasser dans l'excellence qui peut être passée à son crédit ».--> sur les autres ? Nullement ! Car nous avons déjà prouvé que tous, tant Juifs que Grecs, sont assujettis au péché,
3:10	selon qu'il est écrit : Il n'y a pas de juste, pas même un seul<!--Ps. 14:3.-->.
3:11	Il n'y a personne qui comprenne, personne qui recherche Elohîm.
3:12	Tous ont dévié, ils se sont tous ensemble rendus inutiles. Il n'y en a pas un qui pratique la bonté, non il n'y en a pas un seul.
3:13	Leur gosier est une tombe ouverte, ils se sont servis de leurs langues pour tromper, un venin d'aspic est sous leurs lèvres,
3:14	leur bouche est pleine de malédictions et d'amertume,
3:15	leurs pieds sont légers pour répandre le sang,
3:16	destruction et malheur sont sur leurs voies,
3:17	ils n'ont pas connu la voie de la paix,
3:18	et la crainte d'Elohîm n'est pas devant leurs yeux<!--Ps. 14.-->.
3:19	Or nous savons que tout ce que la torah dit, elle l'adresse à ceux qui sont sous la torah, afin que toute bouche soit fermée, et que tout le monde devienne sujet au châtiment<!--Dans un jugement, quelqu'un qui perd son procès, le débiteur de quelqu'un.--> venant d'Elohîm.
3:20	À cause de ceci, aucune chair ne sera justifiée devant lui depuis les œuvres de la torah, car c'est par le moyen de la torah que vient la connaissance précise et correcte du péché.

### La justification par la foi

3:21	Mais maintenant, sans la torah, est manifestée la justice d'Elohîm, la torah et les prophètes lui rendant témoignage :
3:22	c'est la justice d'Elohîm par le moyen de la foi en Yéhoshoua Mashiah pour tous ceux et sur tous ceux qui croient. Car il n'y a pas de différence :
3:23	car tous ont péché<!--Le mot « péché » vient du terme grec « hamartano » : « manquer la marque, manquer le chemin de la droiture et de l'honneur, s'éloigner de la loi d'Elohîm ». Le péché est la violation délibérée de la loi divine et l'absence de droiture.--> et n'atteignent<!--Le mot grec ici habituellement traduit par « privé » signifie plutôt : « venir en retard ou trop tardivement », « être laissé derrière dans la course et ainsi ne pas pouvoir atteindre le but » ou encore « tomber près du but ».--> pas la gloire d'Elohîm,
3:24	étant justifiés gratuitement par sa grâce, par le moyen de la rédemption<!--La rédemption est la délivrance par le paiement d'un prix. Trois termes grecs sont utilisés pour parler de la rédemption : 1 - Agorázo : acheter un objet au marché (agora signifiant marché). Les pécheurs sont considérés comme des esclaves vendus au marché (Ro. 7:14). 2 - Exagorázo : acheter et amener un objet hors du marché (Ga. 3:13, 4:5). L'esclave acheté et amené hors du marché, est définitivement délivré. 3 - Lutróō : détacher, rendre libre (Lu. 24:21 ; Tit. 2:14 ; 1 Pi. 1:18). Yéhoshoua ha Mashiah nous a délivrés du péché, de la puissance de Satan et de la loi mosaïque (Col. 1:12-14, 2:14-17 ; 1 Jn. 3:5).--> qui est en Yéhoshoua Mashiah,
3:25	qu'Elohîm a exposé aux regards pour être la propitiation<!--Le terme propitiation vient du grec « hilasterion » qui signifie « ce qui est expié, ce qui rend propice ou le don qui assure la propitiation ». C'est aussi le lieu où s'accomplit la propitiation (Hé. 9:5), c'est-à-dire, le couvercle de l'arche. Lors du grand jour des expiations (Yom Kippour en hébreu), l'aspersion du sang était faite sur le propitiatoire (Lé. 16:14). Le Seigneur Yéhoshoua ha Mashiah est notre victime propitiatoire (1 Jn. 2:2, 4:10).--> au moyen de la foi, par son sang, afin de démontrer sa justice, parce qu'il avait laissé impunis les péchés commis auparavant,
3:26	dans sa tolérance, Elohîm a démontré sa justice dans le temps présent, de manière à être juste, tout en justifiant celui qui a la foi en Yéhoshoua.
3:27	Où est donc le sujet de se glorifier ? Il est exclu. Par le moyen de quelle torah ? Est-ce par la torah des œuvres ? Non, mais par le moyen de la loi de la foi.
3:28	Nous estimons donc que l'être humain est justifié par la foi, sans les œuvres de la torah.
3:29	Mais Elohîm est-il seulement celui des Juifs ? N'est-il pas aussi celui des nations ? Oui, il est aussi celui des nations,
3:30	puisqu'il n'y a qu'un seul Elohîm<!--1 Co. 8:5-6.--> qui justifiera la circoncision par la foi et l'incirconcision par le moyen de la foi.
3:31	Anéantissons-nous donc la torah par le moyen de la foi ? Non, sans doute ! Mais au contraire, nous établissons la torah.

## Chapitre 4

### Abraham et David justifiés par la foi

4:1	Que dirons-nous donc qu'Abraham, notre père, a obtenu selon la chair ?
4:2	Car si Abraham a été justifié par les œuvres, il a de quoi se glorifier, mais non pas envers Elohîm.
4:3	Car que dit l'Écriture ? Abraham a cru en Elohîm et cela lui a été compté comme justice<!--Voir Ge. 15:6.-->.
4:4	Or à celui qui fait un travail, le salaire ne lui est pas compté comme une grâce, mais comme une chose due.
4:5	Mais à celui qui ne fait pas un travail, mais qui croit en celui qui justifie l'impie, sa foi lui est comptée comme justice.
4:6	De même aussi, David exprime la bénédiction de l'être humain à qui Elohîm compte la justice sans les œuvres, en disant :
4:7	Heureux ceux à qui les violations de la torah sont pardonnées, et dont les péchés sont couverts !
4:8	Heureux l'homme à qui le Seigneur ne tient aucun compte de son péché<!--Ps. 32:2.--> !
4:9	Cette déclaration de bénédiction vient-elle sur la circoncision ou aussi sur l'incirconcision ? Car nous disons que la foi d'Abraham lui fut comptée comme justice.
4:10	Comment donc lui a-t-elle été comptée ? Quand il était dans la circoncision ou dans l'incirconcision ? Non dans la circoncision, mais dans l'incirconcision.
4:11	Et il a reçu le signe de la circoncision comme sceau de la justice de la foi qu'il avait eue dans l'incirconcision, pour qu'il soit le père de tous ceux qui croient quoique dans l'incirconcision, pour que la justice leur soit aussi comptée,
4:12	et le père de la circoncision pour ceux qui, non seulement sont de la circoncision, mais aussi qui marchent sur les traces de la foi que notre père Abraham avait eue dans l'incirconcision.
4:13	Car ce n'est pas par le moyen de la torah que la promesse d'être héritier du monde a été faite à Abraham ou à sa postérité, mais par le moyen de la justice de la foi.
4:14	Car si ceux qui sont de la torah sont les héritiers, la foi est rendue inutile et la promesse est abolie.
4:15	Car la torah produit la colère, et là où il n'y a pas de torah, il n'y a pas non plus de transgression.
4:16	C'est donc par la foi, afin que ce soit par la grâce, et afin que la promesse soit assurée à toute la postérité, non seulement à celle qui est de la torah, mais aussi à celle qui est de la foi d'Abraham, qui est le père de nous tous,
4:17	selon qu'il est écrit : Je t'ai établi père de beaucoup de nations. Il l'est devant l'Elohîm en qui il a cru, celui qui donne la vie aux morts et qui appelle les choses qui ne sont pas tout comme celles qui sont.
4:18	Espérant contre toute espérance, il a cru qu'il deviendrait le père de beaucoup de nations, selon ce qui lui avait été dit<!--Ge. 15:1-5.--> : Ainsi sera ta postérité.
4:19	Et n'étant pas faible dans la foi, il n'a pas considéré que son corps était déjà atteint par la mort<!--Voir Hé. 11:12.-->, vu qu'il avait environ 100 ans, et que la matrice de Sarah était morte.
4:20	Et il n'a pas douté à l'égard de la promesse d'Elohîm, par incrédulité, mais il a été fortifié par la foi et il a donné gloire à Elohîm,
4:21	et étant pleinement convaincu que ce qu'il a promis, il est puissant aussi pour l'accomplir.
4:22	C'est pourquoi aussi cela lui a été compté comme justice.
4:23	Mais ce n'est pas à cause de lui seul qu'il est écrit<!--Voir Ge. 15:6.--> : Cela lui a été compté,
4:24	mais c'est aussi à cause de nous, à qui cela sera compté, à nous qui croyons en celui qui a ressuscité des morts, Yéhoshoua notre Seigneur,
4:25	lequel a été livré à cause de nos fautes, et qui est ressuscité à cause de notre justification.

## Chapitre 5

### Le sacrifice de Yéhoshoua permet la réconciliation avec Elohîm

5:1	Étant donc justifiés<!--La justification est l'œuvre par laquelle Elohîm déclare juste un pécheur (Ro. 4:3, 5:1-9 ; Ga. 2:16, 3:11). Cette justice ne s'obtient pas par les œuvres ou les efforts qu'une personne peut faire. Elle n'est donc pas un dû ou une récompense, mais une grâce (Ep. 2:5-9) qui repose uniquement sur le sacrifice de Yéhoshoua ha Mashiah (1 Pi. 2:24).--> à partir de<!--Vient de la préposition « ek » dénotant une origine (point d'où l'action ou le mouvement procède), depuis, de, hors (d'un lieu, du temps, d'une cause).--> la foi, nous avons la paix avec Elohîm, par le moyen de notre Seigneur Yéhoshoua Mashiah.
5:2	C'est aussi par son moyen que nous avons été amenés par la foi à cette grâce, dans laquelle nous tenons ferme, et nous nous glorifions dans l'espérance de la gloire d'Elohîm.
5:3	Et non seulement cela, mais nous nous glorifions même dans les tribulations, sachant que la tribulation produit la persévérance,
5:4	et la persévérance l'épreuve, et l'épreuve l'espérance.
5:5	Or l'espérance ne rend pas honteux, parce que l'amour d'Elohîm est répandu dans nos cœurs par le moyen de l'Esprit Saint qui nous a été donné.
5:6	Car, lorsque nous étions sans force, Mashiah est mort en son temps pour les impies.
5:7	Car à peine quelqu'un mourra-t-il pour un juste, mais pour ce qui est bon, peut-être quelqu’un ose même mourir.
5:8	Mais Elohîm montre son amour envers nous, puisque, alors que nous étions encore des pécheurs, Mashiah est mort pour nous.
5:9	À plus forte raison donc, étant maintenant justifiés par son sang, serons-nous sauvés de la colère<!--Voir 1 Th. 1:9-10, 5:9 ; Ap. 15:5-8, 16:1-21.--> par son moyen.
5:10	Car si, étant ennemis, nous avons été réconciliés avec Elohîm au moyen de la mort de son Fils, à plus forte raison, étant réconciliés, serons-nous sauvés par sa vie.
5:11	Et non seulement cela, mais nous nous glorifions même en Elohîm par notre Seigneur Yéhoshoua Mashiah, par le moyen duquel nous avons maintenant obtenu la réconciliation.

### L'œuvre de Yéhoshoua ha Mashiah et l'œuvre d'Adam

5:12	C'est pourquoi, de même que par le moyen d'un seul être humain<!--Ge. 1:26.--> le péché est entré dans le monde, et par le moyen du péché la mort, et qu’ainsi la mort s'est étendue sur tous les humains, par lequel tous péchèrent.
5:13	Car jusqu'à la torah, le péché était dans le monde. Or le péché n'est pas mis en compte, quand il n'y a pas de torah.
5:14	Mais la mort a régné depuis Adam jusqu'à Moshè, même sur ceux qui n'avaient pas péché par une transgression semblable à celle d'Adam, lequel est la figure de celui qui devait venir.
5:15	Mais, il n'en est pas du don de la grâce comme de la faute. Car, si par la faute d'un seul, il en est beaucoup qui sont morts, à bien plus forte raison la grâce d'Elohîm et le don de grâce d'un seul être humain, Yéhoshoua Mashiah, ont-ils été abondamment répandus sur beaucoup.
5:16	Et il n'en est pas du don comme de ce qui est arrivé par le moyen d'un seul qui a péché. Car, à partir d'une seule faute, le jugement aboutit en effet à la condamnation, tandis qu'à partir de beaucoup de fautes, le don de grâce aboutit à la justification.
5:17	Car si par la faute d'un seul, la mort a régné par le moyen de ce seul, à plus forte raison ceux qui reçoivent l'abondance de la grâce et du don de la justice régneront-ils dans la vie par le moyen du seul Yéhoshoua Mashiah.
5:18	Ainsi donc, comme par la faute d'un seul ce fut pour tous les humains la condamnation, de même, par la justice d'un seul, c'est pour tous les humains la justification qui donne la vie.
5:19	Car de même que par le moyen de la désobéissance d'un seul être humain, beaucoup ont été rendus pécheurs, de même aussi par le moyen de l'obéissance d'un seul, beaucoup seront rendus justes.
5:20	Or la torah est entrée en complément afin que la faute se multipliât, mais là où le péché s'est multiplié, la grâce a surabondé,
5:21	afin que, comme le péché a régné par la mort, ainsi la grâce règne par la justice pour conduire à la vie éternelle, par Yéhoshoua Mashiah notre Seigneur.

## Chapitre 6

### La mort au péché

6:1	Que dirons-nous donc ? Resterons-nous dans le péché afin que la grâce se multiplie ?
6:2	Que cela n'arrive jamais ! Car nous qui sommes morts au péché, comment vivrons-nous encore en lui ?
6:3	Ignorez-vous que nous tous qui avons été baptisés en Mashiah Yéhoshoua, avons été baptisés en sa mort ?
6:4	Nous avons donc été ensevelis avec lui par le moyen du baptême en sa mort, afin que, comme Mashiah est ressuscité des morts par la gloire du Père, de même nous aussi nous marchions en nouveauté de vie.
6:5	Car, si nous sommes devenus une même plante avec lui par la ressemblance de sa mort, nous le serons aussi par celle de sa résurrection.
6:6	Sachant que notre vieil homme a été crucifié avec lui, afin que le corps du péché soit inactif et que nous ne soyons plus esclaves du péché.
6:7	Car celui qui est mort est justifié<!--Vient du grec « dikaioo » qui signifie « rendre juste ou comme il doit être », « montrer », « exposer », « démontrer sa justice », « comme on est et qu'on souhaite être considéré », « déclarer », « prononcer la justice de quelqu'un, le justifier ».--> du péché.
6:8	Or si nous sommes morts avec Mashiah, nous croyons que nous vivrons aussi avec lui.
6:9	Sachant que Mashiah ressuscité des morts ne meurt plus, la mort n'a plus de pouvoir sur lui.
6:10	Car il est mort, et c'est à cause du péché qu'il est mort une fois pour toutes. Mais en vivant, il vit pour Elohîm.
6:11	De même, vous aussi, considérez-vous vraiment comme morts au péché, mais vivants pour Elohîm en Yéhoshoua Mashiah notre Seigneur.
6:12	Que le péché ne règne donc pas dans votre corps mortel pour lui obéir dans ses désirs<!--Voir Ga. 5:16,24.-->.
6:13	Et n'offrez pas vos membres au péché pour être des instruments de l'injustice, mais offrez-vous vous-mêmes à Elohîm, comme de morts étant devenus vivants, et offrez vos membres à Elohîm pour être des instruments de justice.
6:14	Car le péché ne dominera pas sur vous, parce que vous n'êtes plus sous la torah, mais sous la grâce.
6:15	Quoi donc ! Pécherons-nous, parce que nous ne sommes plus sous la torah, mais sous la grâce ? Que cela n'arrive jamais !
6:16	Ne savez-vous pas qu'en vous offrant à quelqu'un comme esclaves pour lui obéir, vous êtes esclaves de celui à qui vous obéissez, soit du péché pour la mort, soit de l'obéissance pour la justice ?
6:17	Mais grâce à Elohîm de ce qu'ayant été les esclaves du péché, vous avez obéi de cœur à cette forme de doctrine qui vous a été transmise !
6:18	Mais ayant été rendus libres du péché, vous êtes devenus esclaves de la justice.
6:19	Je parle à la façon des hommes, à cause de la faiblesse de votre chair. De même que vous avez offert vos membres comme esclaves à l'impureté et à la violation de la torah, pour la violation de la torah, de même offrez maintenant vos membres comme esclaves à la justice pour la sanctification.
6:20	Car lorsque vous étiez esclaves du péché, vous étiez libres à l'égard de la justice.
6:21	Quel fruit donc aviez-vous alors ? Des fruits dont maintenant vous avez honte. Car la fin de ces choses, c'est la mort.
6:22	Mais maintenant, rendus libres<!--Voir Jn. 8:32,36 et Ro. 6:18, 8:2,21.--> du péché et devenus esclaves d'Elohîm, vous avez votre fruit dans la sanctification et pour fin la vie éternelle.
6:23	Car le salaire du péché, c'est la mort, mais le don gratuit d'Elohîm, c'est la vie éternelle par Yéhoshoua Mashiah notre Seigneur.

## Chapitre 7

### La mort à la torah

7:1	Ignorez-vous, frères, car je parle à ceux qui connaissent la torah, que la torah exerce son pouvoir sur l'être humain aussi longtemps qu'il est vivant ?
7:2	Car la femme mariée est liée par la torah à son mari tant qu'il est vivant, mais si son mari meurt, elle est délivrée de la torah du mari.
7:3	Si donc elle devient la femme d'un autre mari du vivant de son mari, elle sera appelée adultère. Mais si son mari meurt, elle est délivrée de la torah, de sorte qu'elle ne sera pas adultère en devenant la femme d'un autre mari.
7:4	Ainsi donc, vous aussi, mes frères, vous avez été, au moyen du corps du Mashiah, mis à mort en ce qui concerne la torah, pour être à un autre, à celui qui est ressuscité des morts, afin que nous portions des fruits pour Elohîm.
7:5	Car, lorsque nous étions dans la chair, les passions des péchés, à cause de la torah, agissaient dans nos membres de manière à produire des fruits pour la mort.
7:6	Mais maintenant, nous sommes déliés de la torah, étant morts à celle sous laquelle nous étions retenus, c'est ainsi que nous servons Elohîm dans un esprit nouveau et non selon la lettre qui a vieilli.
7:7	Que dirons-nous donc ? La torah est-elle péché ? Que cela n'arrive jamais ! Mais je n'ai connu le péché que par le moyen de la torah. Car je n'aurais pas connu la convoitise, si la torah n'avait pas dit : Tu ne convoiteras pas<!--Voir Ex. 20:17.-->.
7:8	Mais, saisissant l'occasion, le péché produisit en moi, par le moyen du commandement, toutes sortes de convoitises, parce que sans la torah, le péché est mort.
7:9	Mais, autrefois sans torah, je vivais. Mais le commandement étant venu, le péché a pris vie, et moi je suis mort.
7:10	Et le commandement qui était pour la vie, j’ai trouvé qu’il était pour la mort.
7:11	Car le péché saisissant l'occasion, m'a trompé par le moyen du commandement, et m'a tué par son moyen.
7:12	La torah donc est sainte, et le commandement est vraiment saint, et juste et bon.
7:13	Ce qui est bon est-il donc devenu pour moi la mort ? Nullement ! Mais le péché, afin de se manifester en tant que péché, a produit en moi la mort par ce qui est bon, afin que, par le moyen du commandement, le péché devienne excessivement pécheur.
7:14	Car nous savons que la torah est spirituelle, mais moi, je suis charnel, vendu au péché.
7:15	Car je n'approuve pas ce que je fais, puisque je ne fais pas ce que je veux, mais je fais ce que je hais.
7:16	Or, si je fais ce que je ne veux pas, je reconnais par là que la torah est bonne.
7:17	Mais maintenant ce n'est plus moi qui accomplis cela, mais le péché qui habite en moi.
7:18	Car je sais qu'il n'y a rien de bon en moi, c'est-à-dire, dans ma chair, parce que le vouloir est à ma portée, mais je ne trouve pas le moyen d'accomplir ce qui est bon.
7:19	Car je ne fais pas le bien que je veux, mais je fais le mal que je ne veux pas.
7:20	Si je fais ce que je ne veux pas, ce n'est plus moi qui accomplis cela, mais le péché qui habite en moi.
7:21	Je trouve donc cette torah au-dedans de moi : quand je veux faire ce qui est bon, le mal est attaché à moi.
7:22	Car je prends bien plaisir à la torah d'Elohîm, quant à l'homme intérieur,
7:23	mais je vois dans mes membres une autre torah qui lutte contre la torah de mon esprit<!--Esprit : du grec « nous », c'est-à-dire l'esprit, l'intelligence, la pensée, le bon sens, la raison.--> et qui me rend captif de la torah du péché qui est dans mes membres.
7:24	Misérable être humain que je suis ! Qui me délivrera du corps de cette mort ?
7:25	Je rends grâce à Elohîm par Yéhoshoua Mashiah notre Seigneur ! Ainsi donc moi-même, par l'esprit, je suis en effet l'esclave de la torah d'Elohîm, mais par la chair, de la torah du péché.

## Chapitre 8

### La torah de l'Esprit de vie

8:1	Il n'y a donc maintenant aucune condamnation pour ceux qui sont en Yéhoshoua Mashiah, qui marchent, non selon la chair, mais selon l'Esprit.
8:2	Car la torah de l'Esprit de vie en Yéhoshoua Mashiah m'a rendu libre<!--Voir Jn. 8:32,36.--> de la torah du péché et de la mort.
8:3	Car ce qui était impossible à la torah parce qu'elle était affaiblie par la chair, Elohîm, en envoyant son propre Fils dans une chair semblable à celle du péché et à cause du péché, a condamné le péché dans la chair,
8:4	afin que la justice de la torah soit accomplie en nous, qui ne marchons pas selon la chair, mais selon l'Esprit.

### La vie selon l'Esprit et la vie selon la chair<!--Cp. Ga. 5:16-18.-->

8:5	Car, ceux qui sont selon la chair pensent<!--Voir Col. 3:2.--> aux choses de la chair, mais ceux qui sont selon l'Esprit aux choses de l'Esprit.
8:6	Car la pensée et le but de la chair, c'est la mort, mais la pensée et le but de l'Esprit, c'est la vie et la paix.
8:7	Parce que la pensée de la chair est inimitié contre Elohîm, car elle ne se soumet pas à la torah d'Elohîm et qu'elle n'en est même pas capable.
8:8	Or ceux qui sont dans la chair ne peuvent plaire à Elohîm.
8:9	Mais vous, vous n'êtes pas dans la chair, mais dans l'Esprit, puisque l'Esprit d'Elohîm habite en vous. Mais si quelqu'un n'a pas l'Esprit du Mashiah<!--Notez que le Saint-Esprit est aussi appelé l'Esprit de Yéhoshoua (Ac. 16:7).-->, il ne lui appartient pas.
8:10	Et si Mashiah est en vous, le corps est vraiment mort à cause du péché, mais l'Esprit est vie à cause de la justice.
8:11	Et si l'Esprit de celui qui a ressuscité Yéhoshoua d'entre les morts habite en vous, celui qui a ressuscité Mashiah d'entre les morts, rendra aussi la vie à vos corps mortels, à cause de son Esprit qui habite en vous.
8:12	Ainsi donc, mes frères, nous sommes débiteurs, mais non pas envers la chair, pour devoir vivre selon la chair.
8:13	Car si vous vivez selon la chair, vous mourrez. Mais si par l'Esprit vous faites mourir les actions du corps, vous vivrez.
8:14	Car tous ceux qui sont conduits par l'Esprit d'Elohîm sont fils d'Elohîm.

### L'Esprit d'adoption<!--Ga. 4:5.-->

8:15	Car vous n'avez pas reçu un esprit d'esclavage<!--Voir Ga. 2:4 ; 2 Co. 11:20.--> pour être encore dans la crainte, mais vous avez reçu l'Esprit d'adoption par lequel nous crions : Abba ! Père !
8:16	L'Esprit lui-même rend témoignage à notre esprit, que nous sommes enfants d'Elohîm.
8:17	Or, si nous sommes enfants, nous sommes aussi héritiers : héritiers d'Elohîm en effet et cohéritiers du Mashiah, si en effet nous souffrons avec lui, afin que nous soyons aussi glorifiés avec lui.

### La gloire à venir<!--Ge. 3:18-19.-->

8:18	Car j'estime que les souffrances du temps présent ne sont pas dignes d'être comparées à la gloire à venir<!--Mt. 13:43 ; Ph. 3:21 ; Col. 3:4 ; Ap. 22:5.--> qui doit être révélée en nous.
8:19	Car la création attend<!--Voir Ro. 8:23-25 ; 1 Co. 1:7 ; Ga. 5:5 ; Ph. 3:20 ; Hé. 9:28.--> assidûment et patiemment, avec une ferme attente la révélation des fils d'Elohîm.
8:20	Car la création a été soumise à la vanité, non de son gré, mais à cause de celui qui l'y a soumise,
8:21	avec l'espérance qu'elle aussi sera rendue libre de l'esclavage de la corruption, pour avoir part à la liberté de la gloire des enfants d'Elohîm.
8:22	Car nous savons que, jusqu'à ce jour, toute la création soupire et souffre les douleurs de l'enfantement.
8:23	Et non seulement elle, mais nous aussi qui avons l'offrande du premier fruit<!--Prémices. Voir 1 Co. 15:20-23. Le Mashiah est le premier fruit.--> de l'Esprit, nous aussi, nous soupirons en nous-mêmes en attendant assidûment et patiemment l'adoption, la rédemption de notre corps<!--1 Co. 15:35-43,51-54.-->.
8:24	Car c'est en espérance que nous sommes sauvés. Or, l'espérance qu'on voit n'est plus espérance, car ce que l’on voit, pourquoi l’espérer encore ?
8:25	Mais si nous espérons ce que nous ne voyons pas, nous l'attendons assidûment et patiemment, avec persévérance.

### Le Saint-Esprit, l'intercesseur

8:26	Et de même aussi l’Esprit vient en aide à notre faiblesse, car nous ne savons pas prier comme il faut. Mais l'Esprit lui-même intercède pour nous par des soupirs inexprimables.
8:27	Mais celui qui sonde les cœurs connaît quelle est la pensée de l'Esprit, parce que c'est selon Elohîm qu'il intercède pour les saints. 

### L'amour du Mashiah

8:28	Mais nous savons aussi que toutes choses concourent au bien de ceux qui aiment Elohîm, de ceux qui sont appelés selon son dessein.
8:29	Parce que ceux qu'il a connus d'avance, il les a aussi prédestinés à être conformes à l'image de son Fils, afin qu'il soit le premier-né de beaucoup de frères.
8:30	Et ceux qu'il a prédestinés, il les a aussi appelés. Et ceux qu'il a appelés, il les a aussi justifiés, et ceux qu'il a justifiés, il les a aussi glorifiés.
8:31	Que dirons-nous donc à ces choses ? Si Elohîm est pour nous, qui sera contre nous ?
8:32	Lui qui n’a même pas épargné son propre Fils, mais qui l'a livré pour nous tous, comment ne nous donnera-t-il pas aussi gracieusement toutes choses avec lui ?
8:33	Qui s'avancera en accusateur contre les élus d'Elohîm ? Elohîm est celui qui justifie !
8:34	Qui les condamnera ? Mashiah est mort, et bien plus, il est ressuscité, il est à la droite d'Elohîm et il intercède pour nous !
8:35	Qui nous séparera de l'amour du Mashiah ? Sera-ce la tribulation, ou l'angoisse, ou la persécution, ou la famine, ou la nudité, ou le péril, ou l'épée ?
8:36	Selon qu'il est écrit : Nous sommes mis à mort tous les jours, à cause de toi, et nous sommes estimés comme des brebis de la boucherie<!--Ps. 44:23.-->.
8:37	Mais au contraire, dans toutes ces choses, nous sommes plus que vainqueurs par le moyen de celui qui nous a aimés.
8:38	Car je suis persuadé que ni la mort, ni la vie, ni les anges, ni les principautés, ni les puissances, ni les choses présentes, ni les choses à venir,
8:39	ni la hauteur, ni la profondeur, ni aucune autre créature ne pourra nous séparer de l'amour d'Elohîm manifesté en Yéhoshoua Mashiah notre Seigneur.

## Chapitre 9

### La tristesse de Paulos (Paul) pour Israël

9:1	Je dis la vérité en Mashiah, je ne mens pas, ma conscience me rendant témoignage par le Saint-Esprit,
9:2	que j'ai une grande tristesse et une continuelle douleur dans mon cœur.
9:3	Car moi-même je souhaiterais être anathème et séparé du Mashiah pour mes frères, mes parents selon la chair,
9:4	qui sont israélites, à qui appartiennent l'adoption, la gloire, les alliances, l'ordonnance de la loi, le service divin,
9:5	les promesses et les pères, et de qui est sorti selon la chair, le Mashiah, qui est Elohîm au-dessus de toutes choses, béni éternellement. Amen !

### Les enfants de la chair et les enfants de la promesse

9:6	Or ce n'est pas que la parole d'Elohîm ait échoué. Car tous ceux qui descendent d'Israël, ne sont pas Israël,
9:7	et bien qu’ils soient la postérité d'Abraham, ils ne sont pas tous ses enfants. Car il est dit<!--Ge. 21:12.--> : C'est en Yitzhak que tu auras une postérité appelée de ton nom.
9:8	C'est-à-dire, que ce ne sont pas ceux qui sont enfants de la chair qui sont enfants d'Elohîm, mais que ce sont les enfants de la promesse qui sont comptés pour semence.
9:9	Car voici, la parole de la promesse : En ce temps, je viendrai et Sarah aura un fils<!--Ge. 18:10.-->.
9:10	Et non seulement cela, mais il y a aussi Ribqah<!--Rébecca.-->. C’est du seul Yitzhak, notre père, qu’elle avait conçu.
9:11	Car ils n’étaient pas encore nés et n’avaient rien pratiqué de bon ou de mauvais, afin que le dessein arrêté selon l'élection d'Elohîm demeure, non d'après les œuvres, mais d'après celui qui appelle,
9:12	il lui fut dit : L'aîné sera l'esclave du plus petit<!--Ge. 25:23.-->, ainsi qu'il est écrit :
9:13	J'ai aimé Yaacov et j'ai haï Ésav<!--Mal. 1:2-3.-->.

### La miséricorde et la volonté souveraine d'Elohîm

9:14	Que dirons-nous donc ? Y a-t-il de l'injustice en Elohîm ? Que cela n'arrive jamais !
9:15	Car il dit à Moshè : J'aurai compassion de celui de qui j'aurai compassion et je ferai miséricorde à celui à qui je ferai miséricorde<!--Ex. 33:19.-->.
9:16	Ainsi donc, cela ne dépend ni de celui qui veut, ni de celui qui court, mais d'Elohîm qui fait miséricorde.
9:17	Car l'Écriture dit à pharaon : C'est pour cela même que je t'ai suscité, afin de démontrer en toi ma puissance, et afin que mon Nom soit publié par toute la Terre<!--Ex. 9:16.-->.
9:18	Ainsi donc, il fait miséricorde à qui il veut, et il endurcit qui il veut.
9:19	Tu me diras : Pourquoi se plaint-il encore ? Car qui est celui qui peut résister à sa volonté ?
9:20	Mais toi, ô humain, qui es-tu pour contester avec Elohîm ? Le vase en terre dira-t-il à celui qui l'a formé : Pourquoi m'as-tu ainsi fait ?
9:21	Le potier<!--Job 10:8-9.--> n'a-t-il pas autorité sur l'argile, pour faire en effet de la même masse un vase pour l'honneur et un autre pour le déshonneur ?
9:22	Mais que dire si Elohîm, en voulant montrer sa colère et faire connaître sa puissance, a supporté avec beaucoup de patience les vases de colère équipés pour la destruction,
9:23	afin de faire connaître aussi la richesse de sa gloire sur les vases de miséricorde qu'il a préparés d'avance pour la gloire ?
9:24	Et il nous a appelés, non seulement hors des Juifs, mais aussi hors des nations,
9:25	selon ce qu'il dit dans Hoshea<!--Généralement traduit par « Hosée » ou « Osée ».--> : J'appellerai mon peuple, celui qui n'était pas mon peuple, et la bien-aimée, celle qui n'était pas la bien-aimée.
9:26	Et il arrivera que, dans le lieu même où il leur avait été dit : Vous n'êtes pas mon peuple ! Là ils seront appelés les fils d'Elohîm vivant.
9:27	Mais Yesha`yah<!--Ésaïe.--> s'écrie au sujet d'Israël : Si le nombre des fils d'Israël était comme le sable de la mer<!--Os. 2:1.-->, un reste<!--Es. 10:21-23.--> seulement sera sauvé.
9:28	Car le Seigneur accomplit et exécute la parole avec droiture. Parce qu'il fera exécuter la parole sur la Terre.
9:29	Et comme Yesha`yah avait dit auparavant : Si le Seigneur Sabaoth ne nous avait laissé une postérité, nous serions devenus comme Sodome et nous aurions été semblables à Gomorrhe<!--Es. 1:9.-->.
9:30	Que dirons-nous donc ? Que les nations qui ne cherchaient pas la justice ont obtenu la justice, la justice qui vient de la foi,
9:31	mais Israël, poursuivant la torah de la justice, n'est pas parvenu à cette torah.
9:32	Pourquoi ? Parce que cela venait non de la foi, mais comme venant des œuvres de la torah. Car ils se sont heurtés contre la pierre d'achoppement,
9:33	selon qu'il est écrit : Voici, je mets en Sion la pierre d'achoppement et un rocher de scandale, et quiconque croit en lui, ne sera pas confus<!--Es. 28:16.-->.

## Chapitre 10

### La foi en Mashiah, condition du salut

10:1	Frères, le désir de mon cœur et ma supplication à Elohîm pour Israël est vraiment en vue du salut.
10:2	Car je leur rends témoignage qu'ils ont du zèle pour Elohîm, mais non pas selon la connaissance précise et correcte.
10:3	Car, ne connaissant pas la justice d'Elohîm et cherchant à établir leur propre justice, ils ne se sont pas soumis à la justice d'Elohîm.
10:4	Car Mashiah est la fin<!--Le mot « fin » vient du grec « telos » qui signifie « la limite à laquelle cesse une chose » ou encore « le but ». En effet, les lois cérémonielles avaient pour but d'amener les Israélites au sacrifice du Mashiah (Mt. 24:14 ; 1 Co. 10:11 ; 1 Ti. 1:5).--> de la torah<!--Il est question de la loi cérémonielle relative au culte mosaïque. Avant sa mort, Yéhoshoua, qui était né sous la torah (Ga. 4:4), demandait aux gens de l'appliquer. Ainsi, il demanda au lépreux qu'il avait guéri de présenter une offrande pour sa purification au temple (Mt. 8:1-4) et à ses disciples d'observer l'enseignement des scribes (Mt. 23:1-2). En effet, il fallait que les lois cérémonielles soient respectées jusqu'à sa mort. Quand Yéhoshoua a dit : « Tout est accompli » (Jn. 19:30), toutes ces lois n'avaient plus aucune raison d'être (Col. 2:14-17 ; Hé. 7:11-22, 10:1-2).--> pour la justice de tout croyant.
10:5	Car Moshè décrit la justice qui vient de la torah : L'être humain qui aura pratiqué ces choses vivra par elles<!--Lé. 18:5.-->.
10:6	Mais ainsi parle la justice qui vient de la foi : Ne dis pas en ton cœur : Qui montera au ciel ? C'est en faire descendre Mashiah<!--De. 30:11-14.--> ;
10:7	ou : Qui descendra dans l'abîme ? C'est faire remonter Mashiah d'entre les morts.
10:8	Mais que dit-elle ? La parole est près de toi, dans ta bouche et dans ton cœur. Or voilà la parole de foi que nous prêchons.
10:9	Parce que si tu confesses de ta bouche le Seigneur Yéhoshoua, et si tu crois dans ton cœur qu'Elohîm l'a ressuscité des morts, tu seras sauvé.
10:10	Car c'est du cœur que l'on croit à la justice, et c'est de la bouche que l'on fait profession pour le salut, 
10:11	car l'Écriture dit : Quiconque croit en lui ne sera pas confus<!--Es. 49:23.-->.
10:12	Car il n'y a pas de différence entre le Juif et le Grec, puisqu'ils ont le même Seigneur, qui est riche pour tous ceux qui l'invoquent<!--Voir 1 Pi. 1:17.-->.
10:13	Car quiconque invoquera le Nom du Seigneur sera sauvé<!--Paulos (Paul) se réfère ici à Joë. 2:32 : « Et il arrivera que quiconque invoquera le Nom de YHWH sera sauvé ». Comme à d'autres endroits, le nom propre YHWH a été remplacé par le générique « Seigneur ». Voir commentaire en Lu. 4:18-19.-->.
10:14	Mais comment invoqueront-ils celui en qui ils n'ont pas cru ? Et comment croiront-ils en celui dont ils n'ont pas entendu parler ? Et comment en entendront-ils parler, sans quelqu'un qui prêche ?
10:15	Mais comment prêchera-t-on, si l’on n’est pas envoyé ? Selon qu'il est écrit : Qu'ils sont beaux les pieds de ceux qui annoncent la paix, de ceux qui annoncent de bonnes choses<!--Es. 52:7.--> !
10:16	Mais tous n'ont pas obéi à l'Évangile. Car Yesha`yah dit : Seigneur, qui est-ce qui a cru à ce qu’il a entendu de nous<!--Es. 53:1.--> ?
10:17	Ainsi la foi vient de ce qu'on entend, et l'on entend par le moyen de la parole d'Elohîm.
10:18	Mais je demande : Ne l'ont-ils pas entendue ? Au contraire ! Leur voix est allée par toute la Terre, et leur parole jusqu'aux extrémités du monde<!--Ps. 19:5.-->.
10:19	Mais je demande : Israël ne l'a-t-il pas su ? Moshè le premier dit : J'exciterai votre jalousie par ce qui n'est pas une nation, je provoquerai votre colère par une nation sans intelligence<!--De. 32:21.-->.
10:20	Et Yesha`yah assume avec hardiesse et dit : J'ai été trouvé par ceux qui ne me cherchaient pas, et je me suis clairement manifesté à ceux qui ne me demandaient pas<!--Es. 65:1.-->.
10:21	Mais à Israël il dit : Tout le jour j'ai tendu mes mains vers un peuple rebelle et contredisant<!--Es. 65:2.-->.

## Chapitre 11

### Elohîm n'a pas rejeté Israël

11:1	Je demande donc : Elohîm a-t-il rejeté son peuple ? Que cela n'arrive jamais ! Car je suis aussi Israélite, de la postérité d'Abraham, de la tribu de Benyamin.
11:2	Elohîm n'a pas rejeté son peuple, qu'il a connu d'avance. Et ne savez-vous pas ce que l'Écriture dit par Eliyah, comment il prie Elohîm contre Israël en disant :
11:3	Seigneur, ils ont tué tes prophètes et ils ont démoli tes autels, et je suis resté moi seul, et ils cherchent mon âme<!--1 R. 19:10.--> ?
11:4	Mais quelle réponse Elohîm lui donna-t-il ? Je me suis réservé 7 000 hommes, qui n'ont pas fléchi le genou devant Baal<!--1 R. 19:18.-->.
11:5	Ainsi donc, il y a aussi dans le temps présent, un reste, selon l'élection de la grâce.
11:6	Or si c'est par la grâce, ce n'est plus d’après les œuvres, autrement la grâce n'est plus la grâce. Mais si c'est d’après les œuvres, ce n'est plus par une grâce, autrement l'œuvre n'est plus une œuvre.
11:7	Quoi donc ? Ce qu'Israël cherche, il ne l'a pas obtenu, mais les élus l'ont obtenu, tandis que les autres ont été endurcis.
11:8	Ainsi qu'il est écrit : Elohîm leur a donné un esprit d'assoupissement, des yeux pour ne pas voir et des oreilles pour ne pas entendre<!--Es. 29:10.-->, jusqu'à ce jour. 
11:9	Et David dit : Que leur table soit pour eux un filet et un piège, et une occasion de chute, et cela pour leur récompense !
11:10	Que leurs yeux soient obscurcis pour ne pas voir<!--Ps. 69:23-24.-->, et courbe continuellement leur dos !
11:11	Je dis donc : Ont-ils trébuché afin de tomber ? Nullement ! Mais, par leur chute<!--Le mot grec signifie aussi « tomber à côté de », « une faute » ou « une déviation par rapport à la vérité et la droiture », « un méfait ». Voir Mt. 6:14 ; Ro. 11:12.-->, le salut est accordé aux nations, pour les exciter à la jalousie.
11:12	Or, si leur chute est la richesse du monde et leur amoindrissement la richesse des nations, combien plus en sera-t-il quand ils se convertiront tous ?

### Exhortation à considérer la grâce d'Elohîm

11:13	Car je vous parle à vous, nations : en tant qu'apôtre des nations, je glorifie vraiment mon service,
11:14	si de quelque manière j'excite en effet la jalousie de ceux de ma chair et en sauve quelques-uns.
11:15	Car si leur rejet a été la réconciliation du monde, quelle sera leur réception, sinon une vie d'entre les morts ?
11:16	Or si l'offrande du premier fruit<!--Voir 1 Co. 15:20-23.--> est sainte, la masse l'est aussi. Et si la racine est sainte, les branches le sont aussi.
11:17	Mais si quelques-unes des branches ont été retranchées, et si toi, qui étais un olivier sauvage, tu as été greffé parmi elles<!--Voir Ro. 11:24.--> et tu es devenu participant de la racine et de la sève de l'olivier,
11:18	ne te glorifie pas contre ces branches. Car si tu te glorifies, ce n'est pas toi qui portes la racine, mais c'est la racine qui te porte.
11:19	Mais tu diras : Des branches ont été retranchées, afin que moi je sois greffé.
11:20	C'est vrai. Elles ont été retranchées à cause de leur incrédulité, et tu es debout par la foi. Ne t'élève donc pas par orgueil, mais crains.
11:21	Car si Elohîm n'a pas épargné les branches naturelles, il ne t'épargnera pas non plus.
11:22	Considère donc la bonté et la sévérité d'Elohîm. En effet, la sévérité envers ceux qui sont tombés et la bonté envers toi, si tu persévères dans cette bonté. Autrement, tu seras aussi coupé.
11:23	Et eux de même, s'ils ne persistent pas dans leur incrédulité, ils seront greffés, car Elohîm est puissant pour les greffer de nouveau.
11:24	Car si toi, tu as été coupé de l'olivier sauvage selon sa nature, et greffé contrairement à ta nature sur l'olivier cultivé, à plus forte raison eux, seront-ils greffés selon leur nature sur leur propre olivier.
11:25	Car je ne veux pas, frères, que vous ignoriez ce mystère, afin que vous ne soyez pas sages à vos propres yeux : c’est qu’un endurcissement est arrivé en partie à Israël jusqu’à ce que la plénitude des nations soit entrée.
11:26	Et ainsi tout Israël sera sauvé, selon qu'il est écrit : Le Libérateur viendra de Sion et il détournera de Yaacov les infidélités.
11:27	Et c'est là l'alliance que je ferai avec eux, lorsque j'ôterai leurs péchés<!--Es. 59:20-21.-->.
11:28	En effet, ils sont ennemis selon l'Évangile à cause de vous, mais selon l'élection, ils sont aimés à cause de leurs pères.
11:29	Car les dons de grâce et l'appel d'Elohîm sont sans regrets.
11:30	Car comme vous avez été vous-mêmes autrefois rebelles à Elohîm, et que maintenant vous avez obtenu miséricorde à cause de leur obstination,
11:31	de même ils sont maintenant devenus rebelles, afin qu'ils obtiennent aussi miséricorde, par la miséricorde qui vous a été faite.
11:32	Car Elohîm a enfermé tous les humains dans l'obstination, afin de faire miséricorde à tous.

### Les voies incompréhensibles d'Elohîm

11:33	Ô profondeur de la richesse, et de la sagesse et de la connaissance d'Elohîm ! Que ses jugements sont insondables et ses voies incompréhensibles !
11:34	Car qui a connu la pensée du Seigneur, ou qui a été son conseiller ?
11:35	Qui lui a donné le premier<!--Jo. 41:2.-->, et il lui sera rendu ?
11:36	Parce que c’est de lui et par lui et pour lui que sont toutes choses. À lui soit la gloire pour l'éternité ! Amen !

## Chapitre 12

### Un culte raisonnable

12:1	Je vous exhorte donc, frères, par les compassions d'Elohîm, à offrir vos corps en sacrifice vivant, saint, agréable à Elohîm. C'est votre culte raisonnable.
12:2	Et ne vous conformez<!--Se conformer (c'est-à-dire son esprit et son caractère) au modèle d'un autre, (se façonner selon).--> pas à cet âge-ci, mais soyez transformés<!--Le verbe « transformer » est la traduction du terme grec « metamorphoo » qui a donné en français « transfigurer ». C'est le même terme qui a été utilisé en Mt. 17:2 pour parler de la transfiguration du Seigneur. Si Paulos (Paul) recommandait cela à des personnes déjà converties, c'est parce qu'Elohîm les appelait à aller plus loin. La transformation d'une chenille en papillon est un très bel exemple pour illustrer le changement qui doit s'opérer en nous. Pour atteindre ce stade, cet insecte passe par plusieurs étapes. La transformation nous permet de croître spirituellement. En effet, tout enfant d'Elohîm est appelé à devenir mature, à passer du stade de petit enfant à celui de jeune homme, et de celui de jeune homme à celui de père (1 Jn. 2:12-14).--> par le renouvellement<!--Le mot grec « anakainosis » traduit par renouvellement signifie aussi « renouveau », « rénovation » ou « changement complet vers le meilleur ». Voir Tit. 3:5.--> de votre esprit, afin que vous éprouviez quelle est la volonté d'Elohîm, ce qui est bon, agréable et parfait.

### L'exercice des dons

12:3	Car à travers la grâce qui m'a été donnée, je dis à chacun de vous qu'il ne faut pas penser plus de bien de soi qu'il n'est convenable, mais de penser à exercer un contrôle sur soi-même, selon la mesure de foi qu'Elohîm a départie<!--« Diviser », « séparer en parts », « couper en morceaux », « être fendu en fractions ».--> à chacun.
12:4	Car, comme nous avons beaucoup de membres dans un seul corps, et que tous les membres n'ont pas la même fonction,
12:5	ainsi, nous qui sommes beaucoup, nous formons un seul corps en Mashiah et nous sommes tous membres<!--Voir 1 Co. 12.--> les uns des autres.
12:6	Mais nous avons des dons différents selon la grâce qui nous a été donnée. Si c'est la prophétie, prophétisons selon la proportion de la foi.
12:7	Si c'est le service, en servant. Si c'est l'enseignement, en enseignant.
12:8	Si c'est l'exhortation, en exhortant. Que celui qui donne le fasse dans la simplicité, celui qui dirige, avec zèle, celui qui exerce la miséricorde, avec joie.

### Exhortation à l'amour

12:9	Que l'amour soit sincère. Ayez en horreur le mal, attachez-vous à ce qui est bon.
12:10	Quant à l'amour fraternel, ayez de la tendresse<!--Vient du grec « philostorgos » qui signifie « l'amour mutuel des parents et enfants, des maris et épouses », « amour affectueux, promptitude à aimer, aimer tendrement », « se dit surtout de la tendresse réciproque des parents et enfants ».--> les uns pour les autres. Quant à l'honneur, soyez les premiers à le rendre aux autres.
12:11	Ne soyez pas paresseux à vous employer pour autrui. Soyez bouillants de chaleur de l'esprit, étant les esclaves<!--Être un esclave, servir, faire le service.--> du Seigneur.
12:12	Soyez joyeux dans l'espérance, patients dans la tribulation, persévérants dans la prière.
12:13	Prenez part aux besoins des saints. Empressez-vous à exercer l'hospitalité.
12:14	Bénissez ceux qui vous persécutent, bénissez et ne maudissez pas.
12:15	Réjouissez-vous avec ceux qui se réjouissent et pleurez avec ceux qui pleurent.
12:16	Ayez une même pensée les uns envers les autres. Ne pensez pas à ce qui est élevé, mais laissez-vous entraîner avec ce qui est humble. Ne soyez pas sages à votre propre jugement.
12:17	Ne rendez à personne le mal pour le mal. Recherchez les choses honnêtes devant tous les humains.
12:18	S'il est possible, autant que cela dépend de vous, soyez en paix avec tous les humains.
12:19	Ne vous vengez pas vous-mêmes, mes bien-aimés, mais laissez agir la colère d'Elohîm, car il est écrit : À moi la vengeance, à moi la rétribution, dit le Seigneur<!--De. 32:35.-->.
12:20	Si donc ton ennemi a faim, donne-lui à manger, s'il a soif, donne-lui à boire, car en faisant cela, tu amasseras<!--Voir Pr. 25:21-22.--> des charbons de feu sur sa tête.
12:21	Ne sois pas surmonté par le mal, mais surmonte<!--Voir Jn. 16:33 ; 1 Jn. 5:4-5, 2:13-14, 4:4, 5:4-5 ; Ap. 2:7, 2:11, 2:17, 2:26, 3:5, 3:12, 3:21, 5:5, 12:11, 15:2, 17:14, 21:7.--> le mal par le bien.

## Chapitre 13

### Le chrétien et les autorités

13:1	Que toute âme se soumette aux autorités qui sont au-dessus d'elle, car il n'y a pas d'autorité qui ne vienne d'Elohîm<!--Voir Jn. 19:10-11. Il exite aussi l'autorité démoniaque. Voir Lu. 22:53 ; Ac. 26:18 ; Co. 1:13 et Ap. 13:2-4.--> et les autorités qui existent ont été instituées par<!--Vient du grec « hupo » qui signifie aussi « au-dessous de ».--> Elohîm.
13:2	C'est pourquoi celui qui résiste à l'autorité s'oppose à l'ordre d'Elohîm. Et ceux qui s'y opposent attireront la condamnation sur eux-mêmes.
13:3	Car ce n'est pas pour une bonne action, mais pour une mauvaise, que les magistrats sont à craindre. Or veux-tu ne pas craindre l'autorité ? Fais-le bien et tu recevras d'elle de la louange.
13:4	Car le magistrat est un serviteur d'Elohîm pour ton bien. Mais si tu fais le mal, crains. Car ce n'est pas en vain qu'il porte l'épée, étant serviteur d'Elohîm, ordonné pour faire justice en punissant celui qui fait le mal.
13:5	C'est pourquoi il faut être soumis, non seulement à cause de la punition, mais aussi à cause de la conscience.
13:6	Car c'est aussi pour cela que vous payez les impôts<!--Le tribut, spécialement la taxe levée sur les maisons, les terres et les personnes. Voir Mt. 17:24-25, 22:17-19 ; Mc. 12:14-17 ; Lu. 20:21-25, 23:2.-->, car ils sont ministres d'Elohîm, ceux qui s’appliquent constamment à cela.
13:7	Rendez donc à tous ce qui leur est dû : l'impôt à qui vous devez l'impôt, le tribut à qui vous devez le tribut, le péage à qui vous devez le péage, la crainte à qui vous devez la crainte, l'honneur à qui vous devez l'honneur.

### Aimer son prochain<!--Cp. Lu. 10:29-37.-->

13:8	Ne devez rien à personne, si ce n'est de vous aimer les uns les autres. Car celui qui aime les autres a accompli la torah.
13:9	En effet : Tu ne commettras pas d'adultère, tu n'assassineras pas, tu ne voleras pas, tu ne diras pas de faux témoignage, tu ne convoiteras pas, et tout autre commandement se résument dans cette parole : Tu aimeras ton prochain comme toi-même<!--Ex. 20:12-17 ; Lé. 19:18 ; Mt. 22:39 ; Mc. 12:31 ; Ga. 5:14.-->.
13:10	L'amour ne fait pas de mal au prochain. L'amour est donc l'accomplissement de la torah.
13:11	Et même, vu le temps, c'est déjà l'heure de nous réveiller du sommeil. Car maintenant le salut est plus près de nous que lorsque nous avons cru.
13:12	La nuit est avancée<!--Mt. 25:1-13.--> et le jour approche. Alors mettons de côté<!--Voir commentaire en Hé. 12:1.--> les œuvres des ténèbres et soyons revêtus des armes de lumière.
13:13	Marchons d'une manière bienséante, comme en plein jour, non dans les orgies<!--Orgies : du grec « komos ». Ce terme désigne la procession nocturne et rituelle, qui avait lieu après un souper, de gens à moitié ivres, à l'esprit folâtre, qui défilaient à travers les rues avec torches et musique en l'honneur de Bacchus ou quelque autre divinité, et chantaient et jouaient devant les maisons de leurs amis, hommes ou femmes. Ce mot est aussi utilisé pour les fêtes et beuveries de nuit qui se terminaient en orgies.--> et les ivrogneries, non dans le concubinage et la luxure sans bride<!--Voir commentaire en Mc. 7:22.-->, non dans la querelle et la jalousie.
13:14	Mais, soyez revêtus du Seigneur Yéhoshoua Mashiah et ne prenez pas soin de la chair pour accomplir ses désirs<!--Ga. 5:16-19.-->.

## Chapitre 14

### Les faibles dans la foi<!--Cp. 1 Co. 8:1-10:33.-->

14:1	Mais quant à celui qui est faible dans la foi, recevez-le sans jugement, ni discussion.
14:2	En effet, tel a la foi pour manger de toutes choses, mais tel autre, qui est faible, ne mange que des légumes.
14:3	Que celui qui mange ne méprise pas celui qui ne mange pas. Et que celui qui ne mange pas, ne juge pas celui qui mange, car Elohîm l'a accueilli.
14:4	Qui es-tu, toi qui juges le domestique d'autrui ? S'il se tient debout, ou s'il tombe, cela regarde son seigneur. Mais il tiendra, car Elohîm a le pouvoir de le faire tenir.
14:5	En effet, tel juge un jour supérieur à un autre, mais tel autre les juge tous égaux. Que chacun soit pleinement persuadé en son esprit.
14:6	Celui qui pense au jour, c'est pour le Seigneur qu'il y pense. Et celui qui ne pense pas au jour, c'est pour le Seigneur qu'il n'y pense pas. Celui qui mange, mange à cause du Seigneur, et il rend grâce à Elohîm. Et celui qui ne mange pas, ne mange pas aussi à cause du Seigneur, et il rend grâce à Elohîm.
14:7	Car aucun de nous ne vit pour lui-même, et aucun ne meurt pour lui-même.
14:8	Car si nous vivons, nous vivons pour le Seigneur, et si nous mourons, nous mourons pour le Seigneur. Soit donc que nous vivions, soit que nous mourions, nous sommes au Seigneur.
14:9	Car c'est pour cela que Mashiah est mort, qu'il est ressuscité, et qu'il a repris une nouvelle vie, afin de dominer sur les morts et sur les vivants.
14:10	Mais toi, pourquoi juges-tu ton frère ? Ou toi, aussi, pourquoi méprises-tu ton frère ? Car nous nous présenterons tous au tribunal<!--Siège officiel d'un juge ou trône du jugement. 2 Co. 5:10.--> du Mashiah.
14:11	Car il est écrit : Je suis vivant, dit le Seigneur, tout genou fléchira devant moi et toute langue donnera gloire à Elohîm<!--Es. 45:23 ; Ph. 2:10-11.-->.
14:12	Ainsi donc, chacun de nous rendra compte à Elohîm pour lui-même.
14:13	Ne nous jugeons donc plus les uns les autres, mais usez plutôt de votre jugement pour ne pas mettre devant votre frère, une pierre d'achoppement ou une occasion de chute<!--Ou scandale.-->.
14:14	Je sais et je suis persuadé par le Seigneur Yéhoshoua, que rien n'est souillé par soi-même, mais cependant si quelqu'un croit qu'une chose est souillée, elle est souillée pour lui.
14:15	Mais si ton frère est attristé à cause d'un aliment, tu ne marches plus selon l'amour. Ne détruis pas par ton aliment celui pour lequel Mashiah est mort.
14:16	Que ce qui est bon pour vous ne soit donc pas calomnié.
14:17	Car le royaume d'Elohîm n'est ni aliment ni boisson, mais justice et paix et joie par l'Esprit Saint.
14:18	Car celui qui sert le Mashiah de cette manière est agréable à Elohîm et approuvé des humains.
14:19	Recherchons donc les choses qui tendent à la paix et à la construction mutuelle.
14:20	Ne détruis pas l'œuvre d'Elohîm pour un aliment. En effet, toutes choses sont pures, mais il est mal à un homme de devenir en mangeant une pierre d'achoppement.
14:21	Il est bon de ne pas manger de viande, de ne pas boire de vin et de s'abstenir de ce qui peut être pour ton frère une occasion de chute, de scandale ou de faiblesse.
14:22	As-tu la foi ? Garde-la devant Elohîm. Béni est celui qui ne se condamne pas lui-même dans ce qu'il approuve !
14:23	Mais celui qui doute est condamné s'il mange, parce que cela ne vient pas de la foi. Or tout ce qui ne vient pas de la foi est péché.

## Chapitre 15

15:1	Mais nous devons, nous qui sommes forts, supporter les infirmités des faibles et ne pas nous plaire en nous-mêmes.
15:2	Que chacun de nous donc plaise à son prochain, dans ce qui est bon, en vue de sa construction.
15:3	Car même le Mashiah n'a pas cherché à se satisfaire lui-même. Mais, selon qu'il est écrit : Les outrages de ceux qui t'insultent sont tombés sur moi<!--Ps. 69:10.-->.
15:4	Car toutes les choses qui ont été écrites auparavant, ont été écrites pour notre instruction, afin que, par la persévérance et la consolation des Écritures, nous ayons espérance.
15:5	Mais que l'Elohîm de persévérance et de consolation vous donne d'avoir une même pensée les uns envers les autres selon Yéhoshoua Mashiah,
15:6	afin que tous, d'un même cœur et d'une même bouche, vous glorifiiez l'Elohîm et Père de notre Seigneur Yéhoshoua Mashiah.
15:7	C'est pourquoi, accueillez-vous les uns les autres, comme aussi le Mashiah nous a accueillis, pour la gloire d'Elohîm.
15:8	Mais je dis que Yéhoshoua Mashiah a été serviteur de la circoncision pour la vérité d'Elohîm, afin de confirmer les promesses faites aux pères,
15:9	et pour que les nations glorifient Elohîm pour sa miséricorde, selon ce qui est écrit : À cause de cela, je te confesserai parmi les nations et je chanterai ton Nom<!--Ps. 18:50.-->. 
15:10	Et il est dit encore : Nations, réjouissez-vous avec son peuple<!--De. 32:43.--> !
15:11	Et encore : Louez le Seigneur, vous toutes les nations, et célébrez-le, vous tous les peuples<!--Ps. 117:1.--> ! 
15:12	Et Yesha`yah dit encore : Et ce sera la racine<!--Yéhoshoua ha Mashiah est la racine d'Isaï qui nous porte. Voir Es. 11:10 ; Ap. 5:5, 22:16.--> d'Isaï qui se lèvera pour être le chef<!--Voir Mc. 10:42. Les chefs de ce monde seront remplacés par Yéhoshoua (Jésus), le véritable chef.--> des nations. Les nations espéreront en elle<!--Es. 11:1,10.-->.
15:13	Mais que l'Elohîm de l'espérance vous remplisse de toute joie et de toute paix, dans la foi, afin que vous abondiez en espérance, par la puissance du Saint-Esprit !

### Voyage de Paulos (Paul) à Yeroushalaim (Jérusalem), à Rome et en Espagne

15:14	Or mes frères, je suis aussi moi-même persuadé, que vous êtes aussi pleins de bonté, remplis de toute connaissance, et que vous pouvez même vous exhorter les uns les autres.
15:15	Mais je vous ai écrit en quelque sorte plus hardiment, frères, comme pour vous rappeler la grâce qui m'a été donnée par Elohîm,
15:16	pour que je sois le ministre de Yéhoshoua Mashiah parmi les nations, administrant selon la manière d'un prêtre l'Évangile d'Elohîm, afin que l'offrande des nations lui soit agréable, étant sanctifiée par le Saint-Esprit.
15:17	J'ai donc de quoi me glorifier en Yéhoshoua Mashiah, dans les choses qui concernent Elohîm.
15:18	Car je n’oserai rien dire que Mashiah n’ait accompli par mon moyen pour l'obéissance des nations, par la parole et par les œuvres,
15:19	par la puissance de signes et de prodiges, par la puissance de l'Esprit d'Elohîm, de sorte que depuis Yeroushalaim et les pays voisins, jusqu'en Illyrie, j'ai tout rempli de l'Évangile du Mashiah.
15:20	Mais je m'efforce sérieusement<!--Voir 2 Co. 5:9.--> de prêcher l'Évangile là où Mashiah n'avait pas encore été prêché, afin de ne pas bâtir sur le fondement étranger<!--« Appartenant à d'autres », « pas de notre famille », « un ennemi ».-->.
15:21	Mais selon qu'il est écrit : Ceux à qui il n'a pas été annoncé, le verront, et ceux qui n'en avaient pas entendu parler, l'entendront<!--Es. 52:15.-->.
15:22	Et c'est ce qui m'a souvent empêché d'aller vous voir.
15:23	Mais maintenant, comme je n'ai plus rien à faire dans ces régions et que, depuis de nombreuses années, j'ai un grand désir d'aller vers vous,
15:24	j'irai chez vous lorsque je partirai pour aller en Espagne, et j'espère que je vous verrai en passant par votre pays, et y être accompagné par vous, après que j'aurai premièrement satisfait en partie mon désir d'être avec vous.
15:25	Mais maintenant je vais à Yeroushalaim pour servir les saints.
15:26	Car la Macédoine et l’Achaïe ont décidé de faire une contribution pour les pauvres parmi les saints de Yeroushalaim.
15:27	Car elles l’ont décidé et elles sont leurs débitrices. Car si les nations ont eu part à leurs biens spirituels, elles doivent aussi leur faire part des choses charnelles.
15:28	Après donc que j'aurai achevé cela et que je leur aurai consigné<!--Ce mot vient du grec « sphragizo » et signifie aussi « confirmer l'authenticité », « mettre un sceau ». Il était question des offrandes qu'il devait remettre aux saints de la Judée.--> ce fruit, je partirai pour l'Espagne, en passant par vos quartiers.
15:29	Et je sais qu'en allant chez vous, c’est avec la pleine bénédiction de l'Évangile du Mashiah que je viendrai.
15:30	Mais je vous exhorte, frères, par notre Seigneur Yéhoshoua Mashiah et par l'amour de l'Esprit, à combattre avec moi dans vos prières à Elohîm pour moi,
15:31	afin que je sois délivré des rebelles qui sont en Judée et que mon service<!--Service : du grec « diakonia », terme qui désigne le service ou le ministère de ceux qui répondent aux besoins des autres. Ce vocable fait aussi allusion à l'office des diacres.--> à l'égard de Yeroushalaim soit bien reçu par les saints.
15:32	Afin que, arrivé avec joie auprès de vous par la volonté d'Elohîm, je me repose avec vous.
15:33	Mais que l'Elohîm de paix soit avec vous tous ! Amen !

## Chapitre 16

### Salutations

16:1	Mais je vous recommande notre sœur Phœbé, qui est diaconesse de l'assemblée de Cenchrées,
16:2	afin que vous la receviez selon le Seigneur, comme il faut recevoir les saints, et que vous l'assistiez dans tout ce dont elle aura besoin, car elle est venue en aide<!--« Une femme responsable des autres », « une gardienne », « une protectrice », « une patronne », « prenant soin des affaires des autres et les aidant par ses ressources ».--> à beaucoup, et aussi à moi-même.
16:3	Saluez Priscilla et Aquilas, mes compagnons d'œuvre en Yéhoshoua Mashiah,
16:4	qui ont exposé leur cou pour ma vie. Ce n'est pas moi seul qui leur rends grâce, mais aussi toutes les assemblées des nations.
16:5	Saluez aussi l'assemblée qui est dans leur maison. Saluez Épaïnetos, mon bien-aimé, qui a été pour Mashiah l'offrande du premier fruit d'Achaïe.
16:6	Saluez Myriam, qui a beaucoup travaillé pour nous.
16:7	Saluez Andronicos et Iounias, mes parents et mes compagnons de captivité, qui sont distingués parmi les apôtres, qui même ont été en Mashiah avant moi.
16:8	Saluez Amplias, mon bien-aimé dans le Seigneur.
16:9	Saluez Ourbanos, notre compagnon d'œuvre en Mashiah, et Stakys, mon bien-aimé.
16:10	Saluez Apellès, qui est éprouvé dans le Mashiah. Saluez ceux de chez Aristoboulos.
16:11	Saluez Hérodion, mon parent. Saluez ceux de chez Narkissos<!--Narcisse.--> qui sont dans le Seigneur.
16:12	Saluez Truphaina et Tryphose, qui travaillent pour le Seigneur. Saluez Persis, la bien-aimée qui a beaucoup travaillé pour le Seigneur.
16:13	Saluez Rhouphos, l'élu du Seigneur, et sa mère, qui est aussi la mienne.
16:14	Saluez Asugkritos, Phlégon, Hermas, Patrobas, Hermès<!--Mercure.-->, et les frères qui sont avec eux.
16:15	Saluez Philologos et Ioulia, Néreus et sa sœur, et Olympas et tous les saints qui sont avec eux.
16:16	Saluez-vous les uns les autres par un saint baiser. Les assemblées du Mashiah vous saluent.
16:17	Mais je vous exhorte mes frères, à prendre garde à ceux qui causent des divisions et des scandales contre la doctrine que vous avez apprise, et éloignez-vous d'eux.
16:18	Car ces sortes de gens ne servent pas notre Seigneur Yéhoshoua Mashiah, mais leur propre ventre. Et, par de douces paroles et des flatteries, ils trompent les cœurs des simples.
16:19	Car votre obéissance est venue à la connaissance de tous. Je me réjouis donc de vous, mais je désire que vous soyez vraiment prudents quant à ce qui est bon, et sans mélange<!--Voir Mt. 10:16.--> quant au mal.
16:20	Mais l'Elohîm de paix brisera bien vite<!--Vient du grec « tachos » qui signifie « vitesse, rapidité, vivacité et promptitude ».--> Satan sous vos pieds. Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous ! Amen !
16:21	Timotheos, mon compagnon d'œuvre, vous salue, ainsi que Loukios, et Iason et Sosipatros, mes parents.
16:22	Moi Tertios, qui ai écrit cette épître, je vous salue en notre Seigneur.
16:23	Gaïos, mon hôte et celui de toute l'assemblée, vous salue. Erastos, le gestionnaire de la ville, vous salue, et Kouartos, notre frère.
16:24	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous tous ! Amen !
16:25	Or à celui qui est puissant pour vous affermir selon mon Évangile et selon la prédication de Yéhoshoua Mashiah, conformément à la révélation du mystère qui a été caché durant des temps éternels,
16:26	mais manifesté maintenant à travers les écritures prophétiques, selon le mandat<!--L'ordre. Voir 1 Ti. 1:1 ; Tit. 1:3.--> d'Elohîm éternel qui l’a fait connaître ainsi à toutes les nations pour l’obéissance de la foi.
16:27	À Elohîm seul sage, soit la gloire éternellement, par Yéhoshoua Mashiah ! Amen !
