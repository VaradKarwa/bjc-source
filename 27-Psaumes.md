# Tehilim (Psaumes) (Ps.)

Signification : Louanges

Auteurs : David essentiellement et d'autres écrivains

Thème : La louange et l'adoration

Date de rédaction : À compter du 10ème siècle av. J.-C. et au-delà

Le terme « psaume » désigne un poème chanté avec l'accompagnement d'un instrument. C'est ainsi que furent initialement contés les récits de la création divine, la captivité ou encore la gloire de Yeroushalaim (Jérusalem). Expressions de joie, de reconnaissance, de repentance, d'angoisse ou de vulnérabilité de l'être humain, ces hymnes étaient des prières adressées à Elohîm.

Prophétiques, certains psaumes annoncent les événements de l'achèvement des âges (fin des temps), notamment les souffrances du Mashiah (Christ). Utilisé comme recueil de chants, le livre des Psaumes exalte la grandeur d'Elohîm, sa souveraineté, sa miséricorde et son omniscience. Il est le fruit d'une grande variété d'expériences spirituelles du fait de la diversité de ses auteurs. De plus, il contient une richesse de styles considérable, ce qui en fait le chef-d'œuvre de la poésie hébraïque.

## Chapitre 1

### La voie du juste et celle du pécheur

1:1	Béni est l'homme qui ne marche pas selon le conseil des méchants, qui ne s'arrête pas sur la voie des pécheurs et qui ne s'assied pas dans l'assemblée des moqueurs<!--Jé. 15:17 ; 1 Co. 15:33 ; Ep. 5:11.-->,
1:2	mais qui prend plaisir dans la torah de YHWH, et qui médite sa torah jour et nuit<!--De. 6:6, 17:19 ; Jos. 1:8.--> !
1:3	Il est comme un arbre planté près des ruisseaux d'eaux, qui rend son fruit en sa saison, et dont le feuillage ne se flétrit pas<!--Jé. 17:7-8 ; Ez. 47:12 ; Jn. 15:8 ; Ap. 22:2.-->. Et ainsi tout ce qu'il fera réussira.
1:4	Il n'en est pas ainsi des méchants : ils sont comme la balle que le vent chasse au loin<!--Job 21:17-18 ; Os. 13:3.-->.
1:5	C'est pourquoi les méchants ne résistent pas dans le jugement, ni les pécheurs dans l'assemblée des justes.
1:6	Car YHWH connaît la voie des justes, mais la voie des méchants périra.

## Chapitre 2

### Complot des nations contre le Mashiah

2:1	Pourquoi cette agitation parmi les nations, ces vains complots parmi les peuples ?
2:2	Les rois de la Terre se présentent et les princes se concertent ensemble contre YHWH et contre son Mashiah<!--Cette prophétie concerne le complot des Juifs, de Pilate et d'Hérode contre Yéhoshoua ha Mashiah (Jésus-Christ). Il est également question du gouvernement mondial dirigé par Satan. Mt. 12:14, 26:3-4, 26:59-66, 27:1-2 ; Mc. 3:6, 11:18 ; Ac. 4:23-29.--> :
2:3	Brisons leurs liens, jetons loin de nous leurs chaînes !
2:4	Celui qui habite dans les cieux se rit d'eux, Adonaï se moque d'eux.
2:5	Il leur parle dans sa colère, et il les remplira de terreur par la grandeur de son courroux<!--Pr. 1:26.--> :
2:6	C'est moi qui ai oint mon Roi sur Sion, la montagne de ma sainteté<!--Mi. 4:7.--> !
2:7	Je vous réciterai cette ordonnance. YHWH m'a dit : Tu es mon Fils ! Je t'ai engendré aujourd'hui<!--Ac. 13:33 ; Hé. 1:5, 5:5.-->.
2:8	Demande-moi, et je te donnerai les nations pour héritage, et les extrémités de la Terre pour possession.
2:9	Tu les briseras avec un sceptre de fer, et tu les mettras en pièces comme un vase de potier<!--Da. 2:44 ; Ap. 2:27.-->.
2:10	Maintenant donc, rois, ayez de l'intelligence ! Juges de la Terre, recevez instruction !
2:11	Servez YHWH avec crainte, et réjouissez-vous avec tremblement<!--Ps. 19:10.-->.
2:12	Donnez un baiser au Fils, de peur qu'il ne s'irrite et que vous ne périssiez dans cette conduite, quand sa colère s'embrasera promptement. Bénis sont tous ceux qui se confient en lui !

## Chapitre 3

### YHWH, le véritable secours

3:1	Psaume de David au sujet de sa fuite devant Absalom, son fils.
3:2	YHWH, que mes adversaires sont nombreux ! Beaucoup de gens se lèvent contre moi !
3:3	Beaucoup disent à mon âme : Plus de salut pour lui auprès d'Elohîm ! Sélah<!--Le mot hébreu « sélah » signifie « élever, exalter ». Il peut aussi traduire une pause dans le cantique ou le texte. C'est sûrement un terme technique musical montrant probablement une accentuation, une pause, une interruption.-->.
3:4	Mais toi, YHWH, tu es un bouclier autour de moi ! Tu es ma gloire, et tu relèves ma tête.
3:5	De ma voix je crie à YHWH, et il me répond de la montagne de sa sainteté. Sélah.
3:6	Je me couche, je m'endors, je me réveille, car YHWH me soutient<!--Lé. 26:6.-->.
3:7	Je n'ai pas peur des myriades de peuples quand ils se rangent contre moi de toutes parts.
3:8	Lève-toi, YHWH, mon Elohîm ! Délivre-moi ! Car tu frappes à la joue tous mes ennemis, tu brises les dents des méchants.
3:9	Le salut vient de YHWH<!--Es. 43:11 ; Jé. 3:23 ; Pr. 21:31 ; Ap. 7:10.--> ! Que ta bénédiction soit sur ton peuple ! Sélah.

## Chapitre 4

### YHWH, la joie et la paix du juste

4:1	Psaume de David. Au chef de musique. Sur Negiynah.
4:2	Elohîm de ma justice, puisque je crie, réponds-moi ! Quand j'étais à l'étroit, tu m'as mis au large ! Aie pitié de moi, et exauce ma prière<!--Ps. 28:1-2.--> !
4:3	Fils des hommes, jusqu'à quand ma gloire sera-t-elle diffamée ? Jusqu'à quand aimerez-vous la vanité et chercherez-vous le mensonge ? Sélah.
4:4	Sachez que YHWH distingue celui qui lui est fidèle. YHWH m'exauce quand je crie à lui<!--1 Jn. 5:14.-->.
4:5	Tremblez et ne péchez pas ! Parlez dans vos cœurs sur votre couche, et taisez-vous. Sélah.
4:6	Offrez des sacrifices de justice<!--Ps. 51:19.-->, et confiez-vous en YHWH.
4:7	Beaucoup disent : Qui nous fera voir le bonheur ? Lève sur nous la lumière de ta face, YHWH !
4:8	Tu mets plus de joie dans mon cœur qu'ils n'en ont, quand abondent leur froment et leur vin.
4:9	Je me couche et je m'endors en paix, car toi seul, YHWH, me fais reposer en sécurité<!--Pr. 3:24.-->.

## Chapitre 5

### Recours à la protection de YHWH

5:1	Psaume de David. Au chef de musique. Sur Nehiloth.
5:2	Prête l'oreille à mes paroles, YHWH ! Écoute ma méditation !
5:3	Mon Roi et mon Elohîm ! Sois attentif à la voix de mon cri, car c'est à toi que j'adresse ma requête.
5:4	YHWH, le matin tu entends ma voix, dès le matin je me tourne vers toi et je veille,
5:5	car tu n'es pas un El qui prend plaisir à la méchanceté. Le mal n'a pas sa demeure auprès de toi,
5:6	les orgueilleux ne subsistent pas devant tes yeux. Tu hais tous ceux qui commettent l'iniquité<!--Ha. 1:13 ; Ps. 1:5.-->,
5:7	tu fais périr ceux qui disent le mensonge ; YHWH a en abomination l'homme sanguinaire et le trompeur.
5:8	Mais moi, comblé de tes bienfaits, j'entrerai dans ta maison, je me prosternerai dans le palais de ta sainteté avec les sentiments d'une crainte respectueuse.
5:9	YHWH, conduis-moi dans ta justice, à cause de mes ennemis, aplanis ta voie sous mes pas<!--Ps. 25:4-5, 27:11.-->,
5:10	car il n'y a rien de droit dans leur bouche ; leur cœur est rempli de malice, leur gosier est un sépulcre ouvert, ils flattent de leur langue<!--Ps. 10:7, 12:3 ; Ro. 3:13.-->.
5:11	Elohîm ! fais-leur leur procès, qu'ils échouent dans leurs projets ! Chasse-les au loin, à cause du grand nombre de leurs transgressions ! Car ils se sont rebellés contre toi.
5:12	Mais tous ceux qui t'ont pour refuge se réjouiront, ils seront pour toujours dans l'allégresse et tu les protégeras ; et ceux qui aiment ton Nom exulteront en toi !
5:13	Car tu bénis le juste, YHWH ! Et tu l'environnes de ta faveur comme d'un bouclier.

## Chapitre 6

### La miséricorde de YHWH

6:1	Psaume de David. Au chef de musique. Sur Negiynah, sur Sheminith.
6:2	YHWH, ne me punis pas dans ta colère, et ne me châtie pas dans ta fureur<!--Jé. 10:24.-->.
6:3	Aie pitié de moi, YHWH ! car je suis sans aucune force. Guéris-moi, YHWH ! car mes os sont épouvantés.
6:4	Même mon âme est extrêmement troublée. Et toi, YHWH ! jusqu'à quand ?
6:5	Reviens, YHWH ! Délivre mon âme. Sauve-moi, à cause de ta miséricorde.
6:6	Car dans la mort on ne se souvient pas de toi : qui te célébrera dans le shéol<!--Es. 38:18 ; Ps. 88:11, 115:17.--> ?
6:7	Je m'épuise à force de gémir ; chaque nuit ma couche est baignée de mes larmes<!--Job 7:3-4.-->, mon lit est arrosé de mes pleurs.
6:8	J'ai le visage usé par le chagrin<!--Ps. 31:10.--> ; il vieillit à cause de tous ceux qui m'oppriment.
6:9	Retirez-vous loin de moi, vous tous qui pratiquez la méchanceté<!--Mt. 7:23, 25:41 ; Lu. 13:27.--> ! Car YHWH a entendu la voix de mes pleurs.
6:10	YHWH a entendu ma supplication, YHWH a reçu ma prière.
6:11	Tous mes ennemis sont honteux, extrêmement troublés ; ils reculent soudain, honteux.

## Chapitre 7

### La délivrance se trouve auprès de YHWH

7:1	Shiggaïon<!--Le terme « shiggaïon » veut certainement dire « chant lyrique ».--> de David, chanté à YHWH, au sujet de Koush, fils de la tribu de Benyamin.
7:2	YHWH, mon Elohîm ! je cherche en toi mon refuge. Sauve-moi de tous mes persécuteurs, et délivre-moi,
7:3	de peur qu'ils ne déchirent mon âme comme un lion qui dévore sans qu'il n'y ait personne qui me secoure.
7:4	YHWH, mon Elohîm ! si j'ai commis une telle action, s'il y a de l'injustice dans mes mains,
7:5	si j'ai rendu le mal à celui qui était en paix avec moi, si j'ai dépouillé celui qui m'opprimait sans cause,
7:6	que l'ennemi me poursuive et m'atteigne, qu'il foule à terre ma vie, et qu'il couche ma gloire dans la poussière ! Sélah.
7:7	Lève-toi, YHWH ! Dans ta colère, lève-toi contre la fureur de mes adversaires. Réveille-toi pour me secourir, ordonne un jugement !
7:8	Que l'assemblée des peuples t'environne ! Monte au-dessus d'elle vers les lieux élevés !
7:9	YHWH juge les peuples : rends-moi justice, YHWH<!--Ps. 9:5.-->, selon ma droiture et selon mon intégrité !
7:10	Que la malice des méchants prenne fin, et affermis le juste, toi qui sondes les cœurs et les reins<!--Jé. 11:20, 17:10.-->, Elohîm juste !
7:11	Mon bouclier est en Elohîm, qui délivre ceux qui sont droits de cœur.
7:12	Elohîm est un juste Juge, El s'irrite en tout temps contre les méchants.
7:13	Si le méchant ne se convertit pas, Elohîm aiguise son épée<!--De. 32:41.-->, il bande son arc, et vise.
7:14	Il dirige sur lui des traits meurtriers, il rend ses flèches brûlantes.
7:15	Voici, le méchant prépare le mal, il conçoit l'iniquité, et il enfante le mensonge<!--Ja. 1:15.-->.
7:16	Il fait une fosse, il la creuse, et il tombe dans la fosse qu'il a faite<!--Ps. 9:16.-->.
7:17	Son travail retourne sur sa tête, et sa violence redescend sur son front.
7:18	Je célébrerai YHWH selon sa justice, je chanterai le Nom de YHWH, d'Elyon.

## Chapitre 8

### Magnificence d'Elohîm et vanité de l'homme

8:1	Psaume de David. Au chef de musique. Sur Guitthith.
8:2	YHWH, notre Seigneur, que ton Nom est magnifique sur toute la Terre ! Tu as établi ta majesté au-dessus des cieux<!--Es. 6:3.-->.
8:3	Par la bouche des petits enfants et de ceux qui tètent<!--Mt. 21:16.-->, tu as fondé ta puissance, à cause de tes adversaires, afin de faire cesser l'ennemi et le vindicatif.
8:4	Quand je regarde tes cieux, l'ouvrage de tes doigts, la lune et les étoiles que tu as fixées :
8:5	Qu'est-ce que l'homme, pour que tu te souviennes de lui ? Et le fils d'humain, pour que tu le visites<!--Dans ce passage, il est question de l'incarnation de YHWH afin de nous sauver (1 Co. 15:45-49 ; 1 Ti. 3:16 ; Hé. 2:14). Yéhoshoua ha Mashiah (Jésus-Christ) s'est lui-même nommé « fils de l'homme » (Lu. 9:22-26), littéralement « fils d'Adam », d'ailleurs cette expression apparaît dans les évangiles plus de 80 fois.--> ?
8:6	Tu l'as fait de peu inférieur aux anges<!--Le mot hébreu est « elohîm » qui signifie aussi « juges », « divinités ». Voir Hé. 2:7.-->, et tu l'as couronné de gloire et d'honneur.
8:7	Tu l'as fait dominer sur les œuvres de tes mains, tu as tout mis sous ses pieds<!--1 Co. 15:27.-->,
8:8	les brebis comme les bœufs, les animaux des champs,
8:9	les oiseaux du ciel et les poissons de la mer, tout ce qui parcourt les sentiers des mers.
8:10	YHWH, notre Seigneur ! Que ton Nom est magnifique sur toute la Terre !

## Chapitre 9

### Louange à YHWH, l'auteur de nos victoires

9:1	Psaume de David. Au chef de musique. Sur Mouth-Labben.
9:2	[Aleph.] Je célébrerai de tout mon cœur YHWH, je raconterai toutes tes merveilles.
9:3	Je me réjouirai et je m'égaierai en toi, je chanterai ton Nom, Elyon !
9:4	[Beth.] Parce que mes ennemis retournent en arrière, ils trébuchent, ils périssent devant ta face.
9:5	Car tu soutiens mon droit et ma cause, tu sièges sur ton trône en juste Juge.
9:6	[Guimel.] Tu châties les nations, tu détruis le méchant, tu effaces leur nom pour toujours, et à perpétuité.
9:7	Plus d'ennemis ! Les désolations sont arrivées à leur fin pour toujours ! Tu as déraciné leurs villes, et leur mémoire a péri avec elles.
9:8	[He.] Mais YHWH sera assis éternellement, il a établi son trône pour juger.
9:9	Il juge le monde avec justice, il juge les peuples avec droiture<!--Ps. 96:13, 98:9.-->.
9:10	[Vav.] YHWH est le refuge pour l'opprimé, son refuge au temps où il sera dans la détresse<!--Ps. 37:39, 46:2, 91:2.-->.
9:11	Ceux qui connaissent ton Nom se confient en toi<!--Pr. 3:5.-->, car tu n'abandonnes pas ceux qui te cherchent, YHWH !
9:12	[Zayin.] Chantez à YHWH qui habite en Sion, annoncez ses œuvres parmi les peuples !
9:13	Lorsqu'il recherche le sang versé, il se souvient des pauvres, il n'oublie pas le cri des affligés.
9:14	[Heth.] Aie pitié de moi, YHWH ! Vois la misère où me réduisent mes ennemis, enlève-moi des portes de la mort,
9:15	afin que je raconte toutes tes louanges, dans les portes de la fille de Sion. Je me réjouirai de la délivrance<!--Voir commentaire en Es. 26:1.--> que tu m'auras donnée.
9:16	[Teth.] Les nations tombent dans la fosse qu'elles ont faite<!--Ps. 10:2, 35:7.-->, leur pied se prend au filet qu'elles ont caché.
9:17	YHWH se fait connaître, il fait justice, le méchant est enlacé dans l'ouvrage de ses mains. (Higgaïon. Sélah).
9:18	[Yod.] Les méchants retournent dans le shéol, toutes les nations qui oublient Elohîm.
9:19	Car l'indigent n'est pas oublié à jamais, l'espérance des affligés ne périt pas à toujours.
9:20	[Kaf.] Lève-toi, YHWH ! Que l'homme mortel ne triomphe pas ! Que les nations soient jugées devant ta face !
9:21	Frappe-les de terreur, YHWH ! Que les peuples sachent qu'ils ne sont que des hommes mortels<!--Es. 51:12.--> ! Sélah.

## Chapitre 10

### Appel au jugement d'Elohîm sur les méchants

10:1	[Lamed.] Pourquoi, YHWH, restes-tu loin, te caches-tu aux temps de détresse<!--Ps. 13:2, 44:24.--> ?
10:2	Le méchant par son orgueil poursuit ardemment les affligés, mais ils seront pris par les méchants desseins qu'ils ont prémédités<!--Ps. 7:15-16, 9:16, 35:8.-->.
10:3	Car le méchant se glorifie du désir de son âme, il estime heureux l'avare et il méprise YHWH.
10:4	Le méchant, par la grandeur de sa face, ne cherche pas Elohîm<!--Ps. 14:1, 53:2.--> ! Voilà toute sa pensée.
10:5	Ses voies réussissent en tout temps ; tes jugements sont éloignés de lui, il souffle contre tous ses adversaires.
10:6	Il dit en son cœur : Je ne serai pas ébranlé, car d'âge en âge je n'aurai pas de malheur !
10:7	Sa bouche est pleine de malédictions, de tromperies et de fraudes ; il n'y a sous sa langue qu'oppression et outrage<!--Ps. 59:7-8, 64:3-4 ; Job 20:12.-->.
10:8	Il se tient aux embûches dans des villages, il tue l'innocent dans des lieux cachés, ses yeux épient le malheureux.
10:9	Il se tient aux aguets dans un lieu caché, comme un lion dans sa tanière, il se tient aux aguets pour attraper l'affligé ; il attrape l'affligé, l'attirant dans son filet.
10:10	Il se courbe, il se baisse, et les malheureux tombent sous sa puissance.
10:11	Il dit en son cœur : El oublie ! Il cache sa face, il ne le verra jamais<!--Ps. 94:7.--> !
10:12	[Qof.] Lève-toi, YHWH ! El, lève ta main ! N'oublie pas les malheureux !
10:13	Pourquoi le méchant méprise-t-il Elohîm ? Il dit en son cœur que tu ne le rechercheras pas.
10:14	[Resh.] Tu l'as vu, car lorsqu'on afflige ou qu'on maltraite quelqu'un, tu regardes pour le mettre entre tes mains. C'est auprès de toi que se réfugie le malheureux, tu es le secours de l'orphelin.
10:15	[Shin.] Brise le bras du méchant, du mauvais, recherche sa méchanceté jusqu'à ce que tu n'en trouves plus !
10:16	YHWH est Roi à toujours et à perpétuité<!--Ps. 29:10, 145:13, 146:10 ; La. 5:19.-->, les nations sont exterminées de sa terre.
10:17	[Tav.] Tu entends les vœux de ceux qui souffrent, YHWH ! Tu affermis leur cœur, ton oreille les écoute attentivement
10:18	pour rendre justice à l'orphelin et à l'opprimé, afin que l'homme mortel tiré de la terre ne fasse plus trembler.

## Chapitre 11

### YHWH, le refuge des hommes droits

11:1	Psaume de David. Au chef de musique. C'est en YHWH que je cherche un refuge. Comment pouvez-vous dire à mon âme : Oiseau, va–t’en dans la montagne !
11:2	En effet, les méchants bandent l'arc<!--Ps. 37:14.-->, ils ajustent leur flèche sur la corde, pour tirer dans l'ombre sur ceux dont le cœur est droit.
11:3	Puisque les fondements sont renversés, que fera le juste ?
11:4	YHWH est dans son saint temple, YHWH a son trône dans les cieux. Ses yeux voient, ses paupières examinent les fils des humains.
11:5	YHWH examine le juste et le méchant, son âme hait celui qui aime la violence.
11:6	Il fait pleuvoir sur les méchants des pièges, du feu, du soufre<!--Ez. 38:22.--> et un vent de chaleur brûlante : la portion de leur coupe.
11:7	Car YHWH est juste, il aime la justice. Les hommes droits voient sa face.

## Chapitre 12

### Le langage des lèvres arrogantes

12:1	Psaume de David. Au chef de musique. Sur Sheminith.
12:2	Sauve, YHWH ! car les hommes pieux n'existent plus, les fidèles disparaissent parmi les fils d'humains.
12:3	Chacun dit des faussetés à son compagnon avec des lèvres flatteuses et ils parlent avec un cœur double.
12:4	Que YHWH retranche toutes les lèvres flatteuses, la langue qui parle fièrement<!--Ps. 17:10.-->,
12:5	parce qu'ils disent : Nous sommes puissants par nos langues, nous avons nos lèvres avec nous. Qui serait notre maître ?
12:6	À cause du mauvais traitement que l'on fait aux malheureux, à cause du gémissement des pauvres, je me lèverai maintenant, dit YHWH, je mettrai en sûreté celui à qui l'on tend des pièges.
12:7	Les paroles de YHWH sont des paroles pures, c'est un argent éprouvé sur terre au creuset<!--Ps. 19:10, 119:140 ; Pr. 30:5.-->, et sept fois épuré.
12:8	Toi, YHWH ! garde-les, et préserve à jamais chacun d'eux de cette race de gens.
12:9	Les méchants se promènent de toutes parts quand la bassesse s’élève parmi les fils d'humains.

## Chapitre 13

### Savoir attendre le secours d'Elohîm

13:1	Psaume de David. Au chef de musique.
13:2	YHWH, jusqu'à quand m'oublieras-tu ? Sera-ce pour toujours ? Jusqu'à quand me cacheras-tu ta face<!--Ps. 10:1, 27:9.--> ?
13:3	Jusqu'à quand imposerai-je des conseils en mon âme, et affligerai-je mon cœur tous les jours ? Jusqu'à quand mon ennemi s'élèvera-t-il contre moi ?
13:4	YHWH, mon Elohîm ! regarde, exauce-moi, illumine mes yeux, de peur que je ne dorme du sommeil de la mort,
13:5	de peur que mon ennemi ne dise : J'ai eu le dessus ! et que mes adversaires ne se réjouissent, si je venais à tomber<!--Ps. 25:2.-->.
13:6	Mais moi, je me confie en ta bonté, mon cœur se réjouira de la délivrance que tu m'auras donnée. Je chanterai à YHWH, parce qu'il m'a fait du bien.

## Chapitre 14

### L'insensé ne cherche pas Elohîm

14:1	Psaume de David. Au chef de musique. L'insensé dit en son cœur : Il n'y a pas d'Elohîm<!--Ceux qui ne croient pas en l'existence d'Elohîm sont appelés insensés. En effet, la création révèle l'existence du Créateur (Ro. 1:19-20).--> ! Ils se sont corrompus, ils se sont rendus abominables dans leurs actions ; il n'y a personne qui fasse le bien.
14:2	YHWH regarde des cieux les fils d'humain, pour voir s'il y a quelqu'un qui soit intelligent, qui cherche Elohîm<!--Ps. 33:13 ; Job 28:24.-->.
14:3	Ils se sont tous égarés, ils se sont tous ensemble rendus odieux, il n'y a personne qui fasse le bien, pas même un seul<!--Tous les humains naissent pécheurs (Ro. 3:10-23).-->.
14:4	Tous ceux qui pratiquent la méchanceté n'ont-ils pas de connaissance ? Ils dévorent mon peuple comme s'ils dévoraient du pain ; ils n'invoquent pas YHWH.
14:5	Là, ils seront saisis d'une grande frayeur, car Elohîm est avec la race des justes.
14:6	Vous jetez l'opprobre sur le conseil du malheureux parce que YHWH est son refuge.
14:7	Oh ! Qui fera partir de Sion la délivrance d'Israël<!--C'est le Mashiah qui délivrera Israël (Ro. 11:25-27).--> ? Quand YHWH ramènera son peuple captif, Yaacov se réjouira, Israël se réjouira.

## Chapitre 15

### L'homme que YHWH agrée

15:1	Psaume de David. YHWH ! Qui séjournera dans ta tente ? Qui habitera sur la montagne de ta sainteté<!--Ps. 24:3-4.--> ?
15:2	C'est celui qui marche dans l'intégrité, qui fait ce qui est juste, et qui profère la vérité telle qu'elle est dans son cœur :
15:3	qui ne calomnie pas avec sa langue, qui ne fait pas de mal à son ami, et qui ne diffame pas son prochain,
15:4	qui regarde avec dédain celui qui est méprisable, mais qui honore ceux qui craignent YHWH, qui ne se rétracte pas s'il fait un serment à son préjudice,
15:5	qui ne prête pas son argent à intérêt et qui n'accepte pas de pot-de-vin contre l'innocent<!--Lé. 25:36 ; De. 16:19, 27:25.-->. Celui qui fait ces choses ne sera jamais ébranlé.

## Chapitre 16

### YHWH, la source de la vie

16:1	Mikhtam<!--Le mot hébreu « miktam » est généralement traduit par « hymne ».--> de David. Garde-moi, El ! car je cherche en toi mon refuge.
16:2	Je dis à YHWH : Tu es Adonaï, ce que j'ai de bon n'est rien sans toi !
16:3	Les saints qui sont dans le pays, les hommes pieux sont l'objet de toute mon affection.
16:4	Les angoisses de ceux qui courent après un autre elohîm seront multipliées : je ne répands pas leurs libations de sang et je ne mets pas leurs noms sur mes lèvres.
16:5	YHWH est la part de mon héritage et ma coupe, tu maintiens mon lot.
16:6	Un héritage agréable m'est attribué, une belle possession m'est accordée.
16:7	Je bénirai YHWH qui me donne conseil. Je le bénirai même durant les nuits dans lesquelles mes reins m'enseignent.
16:8	J'ai constamment YHWH devant moi. Parce qu'il est à ma droite, je ne serai pas ébranlé<!--Ps. 109:31, 110:5 ; Ac. 2:25.-->.
16:9	C'est pourquoi mon cœur se réjouit, mon esprit se réjouit et mon corps repose en sécurité.
16:10	Car tu n'abandonneras pas mon âme au shéol, tu ne permettras pas que ton bien-aimé voie la corruption<!--Le roi David prophétise ici la résurrection du Mashiah.-->.
16:11	Tu me feras connaître le chemin de la vie. Il y a d'abondantes joies devant ta face, des délices éternels à ta droite.

## Chapitre 17

### L'assurance en Elohîm

17:1	Prière de David. YHWH, écoute la droiture, sois attentif à mon cri, prête l'oreille à ma prière faite avec des lèvres sans tromperie !
17:2	Que ma justice paraisse devant ta face, que tes yeux contemplent mon intégrité !
17:3	Tu as sondé mon cœur<!--Ps. 139:1 ; Jé. 12:3.-->, tu l'as visité de nuit, tu m'as examiné, tu n'as rien trouvé : ma pensée ne va pas au-delà de ma parole.
17:4	Quant aux actions des humains, selon la parole de tes lèvres, je me tiens en garde contre la voie du violent.
17:5	Mes pas sont fermes dans tes sentiers, mes pieds ne chancellent pas.
17:6	Je t'invoque, car tu m'exauces, El ! Incline ton oreille vers moi, écoute mes paroles !
17:7	Signale ta bonté, toi qui sauves ceux qui cherchent un refuge, et qui par ta droite les délivres de leurs adversaires !
17:8	Garde-moi comme la prunelle, la fille de l'œil, cache-moi à l'ombre de tes ailes<!--Mt. 23:37 ; Lu. 13:34-35.-->
17:9	contre les méchants qui me traitent violemment, contre mes ennemis qui entourent mon âme.
17:10	Ils sont enfermés dans leur propre graisse, leur bouche parle avec orgueil.
17:11	Maintenant, ils nous environnent à chaque pas que nous faisons, ils fixent de leurs yeux pour nous étendre à terre.
17:12	Ils ressemblent au lion qui ne demande qu'à déchirer, et au lionceau qui se tient dans les lieux cachés.
17:13	Lève-toi, YHWH, marche à leur rencontre, fais-les plier ! Délivre mon âme du méchant par ton épée !
17:14	des hommes par ta main, YHWH, des hommes de ce monde ! Leur part est dans cette vie et tu remplis leur ventre de tes biens ; leurs enfants sont rassasiés, et ils laissent leurs restes à leurs petits-enfants.
17:15	Mais moi, dans mon innocence, je verrai ta face<!--Ps. 16:10-11 ; Job 19:26-27.--> et je me rassasierai de ton image, dès mon réveil.

## Chapitre 18

### Louange à Elohîm, le bouclier des saints

18:1	Psaume de David, serviteur de YHWH, qui adressa à YHWH les paroles de ce cantique, le jour où YHWH l'eut délivré de la main de Shaoul. Au chef des chantres.
18:2	Il dit : YHWH qui es ma force, je t'aimerai profondément.
18:3	YHWH est mon Rocher<!--YHWH est le Rocher sur lequel s'appuyait David. Paulos (Paul) enseigne que ce Rocher était Yéhoshoua ha Mashiah (Jésus-Christ) (1 Co. 10:1-4). Voir commentaire en Es. 8:13-17.-->, ma forteresse et mon libérateur ! Mon El est mon Rocher où je trouve un refuge ! Il est mon bouclier, la corne de mon salut<!--Voir 2 S. 22:3 ; Lu. 1:69.--> et ma haute retraite !
18:4	Je crie : Loué soit YHWH ! Et je suis délivré de mes ennemis.
18:5	Les liens de la mort m'avaient environné et les torrents de Bélial<!--Voir commentaire en De. 13:13.--> m'avaient terrifié.
18:6	Les liens du shéol m'avaient entouré, les filets de la mort m'avaient surpris<!--Ps. 116:3.-->.
18:7	Dans ma détresse, j'ai invoqué YHWH, j'ai crié au secours à mon Elohîm ; il a entendu ma voix de son palais, mon cri est parvenu devant lui à ses oreilles.
18:8	La Terre fut ébranlée et trembla, les fondements des montagnes s'agitèrent et s'ébranlèrent<!--Es. 5:25, 64:1-3 ; Jé. 4:24 ; Ps. 104:32.-->, parce qu'il était irrité.
18:9	Une fumée montait de ses narines, et de sa bouche sortait un feu dévorant, des charbons embrasés.
18:10	Il abaissa les cieux et descendit : il y avait des ténèbres épaisses sous ses pieds.
18:11	Il était monté sur un chérubin, et il volait, il était porté sur les ailes du vent<!--Ps. 104:3.-->.
18:12	Il faisait des ténèbres sa couverture<!--Voir Ps. 91:1.-->, autour de lui était sa tente, il était enveloppé des eaux obscures et de sombres nuages.
18:13	De la splendeur qui le précédait, s'échappaient les nuées, lançant de la grêle et des charbons de feu.
18:14	YHWH tonna dans les cieux, Elyon fit retentir sa voix avec de la grêle et des charbons de feu.
18:15	Il envoya ses flèches pour les disperser, il lança des éclairs et les mit en déroute<!--Ps. 77:18.-->.
18:16	Le fond des eaux parut, les fondements du monde furent découverts, par ta menace, YHWH ! par le souffle du vent de tes narines.
18:17	Il étendit la main d'en haut, il m'enleva et me retira des grandes eaux<!--2 S. 22:17.-->.
18:18	Il me délivra de mon puissant ennemi, et de ceux qui me haïssaient, car ils étaient plus forts que moi.
18:19	Ils m'avaient surpris au jour de ma détresse, mais YHWH fut mon appui.
18:20	Il m'a mis au large, il m'a délivré, parce qu'il m'aime.
18:21	YHWH m'a traité selon ma justice, il m'a récompensé selon la pureté de mes mains<!--Ps. 7:9, 18:25.-->,
18:22	car j'ai observé les voies de YHWH et je ne me suis pas détourné de mon Elohîm.
18:23	Car j'ai eu devant moi toutes ses ordonnances et je ne me suis pas écarté de ses statuts.
18:24	J'ai été intègre envers lui, et je me suis tenu en garde contre mon iniquité.
18:25	Aussi YHWH m'a rendu selon ma justice, selon la pureté de mes mains devant ses yeux.
18:26	Avec celui qui est bon, tu te montres bon, avec l'homme droit tu agis selon la droiture.
18:27	Avec celui qui est pur, tu te montres pur, et avec le pervers tu agis selon sa perversité.
18:28	Car tu sauves le peuple affligé, et tu abaisses les yeux hautains<!--Es. 2:11, 5:15.-->.
18:29	Même c'est toi qui fais briller ma lumière. YHWH, mon Elohîm, éclaire mes ténèbres.
18:30	Même par ton moyen, je me précipite sur une troupe, avec mon Elohîm je franchis la muraille.
18:31	Les voies de El sont sans défaut, la parole de YHWH est éprouvée<!--De. 32:4 ; Ps. 19:8-9 ; Da. 4:37.--> ; il est un bouclier pour tous ceux qui se confient en lui.
18:32	Car qui est Éloah, si ce n'est YHWH ? Et qui est un rocher, si ce n'est notre Elohîm<!--1 S. 2:2 ; 2 S. 22:32.--> ?
18:33	C'est le El qui me ceint de force, et qui me conduit dans la voie droite.
18:34	Il rend mes pieds semblables à ceux des biches<!--2 S. 2:18.-->, et il me place sur mes lieux élevés.
18:35	Il exerce tellement mes mains au combat que mes bras ont plié un arc de cuivre<!--Job 20:24.-->.
18:36	Tu me donnes le bouclier de ton salut, ta droite me soutient, et je deviens puissant par ta bonté.
18:37	Tu élargis le chemin sous mes pas, et mes pieds ne chancellent pas.
18:38	Je poursuis mes ennemis, je les atteints, et je ne reviens pas avant de les avoir anéantis.
18:39	Je les brise, et ils ne se relèvent plus, ils tombent sous mes pieds.
18:40	Car tu m'as ceint de force pour le combat, tu fais plier sous moi ceux qui s'élevaient contre moi.
18:41	Tu fais tourner le dos à mes ennemis devant moi, et j'extermine ceux qui me haïssaient.
18:42	Ils crient au secours, mais il n'y a pas de libérateur ! Ils crient à YHWH, mais il ne leur répond pas !
18:43	Je les brise comme la poussière qui est dispersée par le vent et je les foule comme la boue des rues.
18:44	Tu me délivres des querelles du peuple, tu m'établis chef des nations. Un peuple que je ne connais pas m'est asservi.
18:45	Ils m'obéissent au premier ordre, les fils de l'étranger me flattent.
18:46	Les fils de l’étranger se fanent, ils tremblent de peur dans leurs forteresses.
18:47	YHWH est vivant, et béni soit mon rocher ! Que l'Elohîm de mon salut soit exalté !
18:48	C'est le El qui me donne la vengeance et qui m'assujettit les peuples,
18:49	c'est lui qui me délivre de mes ennemis ! Tu m'élèves au-dessus de mes adversaires, tu me sauves de l'homme de violence.
18:50	C'est pourquoi, YHWH, je te confesserai parmi les nations ! Et je chanterai des louanges à ton Nom.
18:51	Qui accorde de grandes délivrances à son roi, qui fait miséricorde à son mashiah, à David, et à sa postérité, pour toujours.

## Chapitre 19

### La création exalte la grandeur d'Elohîm

19:1	Psaume de David. Au chef de musique.
19:2	Les cieux racontent la gloire de El, et le firmament met en évidence l'œuvre de ses mains.
19:3	Le jour fait jaillir la parole au jour, et la nuit montre sa connaissance la nuit.
19:4	Ce n'est pas un langage, ce ne sont pas des paroles dont le cri ne soit pas entendu :
19:5	leur retentissement couvre toute la Terre, et leur voix est allée jusqu'aux extrémités du monde<!--Ro. 10:18.-->. Il a dressé une tente pour le soleil.
19:6	Et lui, il est semblable à un époux sortant de sa chambre nuptiale, il s'élance sur le sentier avec la joie d'un homme vaillant.
19:7	Il se lève à l'extrémité des cieux et achève sa course à l'autre extrémité<!--Ec. 1:5.--> : rien ne se dérobe à sa chaleur.
19:8	La torah de YHWH est parfaite, elle restaure l'âme. Le témoignage de YHWH est fidèle, il donne la sagesse au stupide<!--2 S. 22:31 ; Ps. 18:31, 119:130.-->.
19:9	Les préceptes de YHWH sont droits, ils réjouissent le cœur. Les commandements de YHWH sont purs, ils éclairent les yeux.
19:10	La crainte de YHWH est pure, elle subsiste à toujours. Les jugements de YHWH sont vrais, et ils sont tous justes.
19:11	Ils sont plus précieux que l'or, que beaucoup d'or fin, et plus doux que le miel, que celui qui coule des rayons de miel<!--Ps. 119:103.-->.
19:12	Ton serviteur aussi en reçoit l'éclairage ; pour qui les observe, la récompense est grande.
19:13	Qui discerne ses erreurs ? Purifie-moi de celles qui me sont cachées.
19:14	Garde aussi ton serviteur des péchés d'orgueil : qu'ils ne dominent pas sur moi ! Alors je serai parfait, je serai pur de grandes transgressions !
19:15	Que les paroles de ma bouche et la méditation de mon cœur soient des délices devant toi, YHWH ! mon Rocher et mon Rédempteur<!--Voir commentaire en Es. 60:16.--> !

## Chapitre 20

### Recours à l'intervention d'Elohîm

20:1	Psaume de David. Au chef de musique.
20:2	Que YHWH te réponde au jour de la détresse, que le Nom d'Elohîm de Yaacov te protège !
20:3	Qu'il envoie ton secours du saint lieu, et qu'il te soutienne de Sion !
20:4	Qu'il se souvienne de toutes tes offrandes, qu'il réduise en cendres ton holocauste ! Sélah.
20:5	Qu'il te donne ce que ton cœur désire, et qu'il fasse réussir tes desseins !
20:6	Nous triompherons dans ton salut, nous lèverons la bannière au Nom de notre Elohîm. YHWH exaucera tous tes vœux.
20:7	Je sais déjà que YHWH sauve son mashiah, il l'exaucera des cieux, de sa sainte demeure, par le secours puissant de sa droite.
20:8	Les uns se vantent de leurs chars, et les autres de leurs chevaux. Mais nous, nous glorifierons le Nom de YHWH, notre Elohîm.
20:9	Eux, ils plient, et ils tombent, nous, nous tenons ferme, et restons debout.
20:10	YHWH sauve-nous ! Que le roi nous réponde quand nous crions à lui !

## Chapitre 21

### La protection d'Elohîm sur le roi

21:1	Psaume de David. Au chef de musique.
21:2	YHWH, le roi se réjouit de ta puissance, ton salut le remplit d'allégresse !
21:3	Tu lui as donné ce que désirait son cœur et tu n'as pas refusé ce que demandaient ses lèvres. Sélah.
21:4	Car tu es venu au-devant de lui avec des bénédictions de ta bonté, et tu as mis sur sa tête une couronne d'or pur.
21:5	Il te demandait la vie, et tu la lui as donnée, la longueur des jours, pour toujours, à jamais.
21:6	Sa gloire est grande à cause de ton salut, tu l'as couvert de majesté et d'honneur.
21:7	Car tu le rends à jamais un objet de bénédictions, tu le combles de joie devant ta face<!--Ps. 16:11.-->.
21:8	Parce que le roi se confie en YHWH, et par la bonté d'Elyon, il ne sera pas ébranlé<!--Ps. 16:8.-->.
21:9	Ta main trouvera tous tes ennemis, ta droite trouvera tous ceux qui te haïssent.
21:10	Tu les rendras tels qu'une fournaise ardente le jour où l'on verra ta face. YHWH les engloutira dans sa colère, et le feu les consumera.
21:11	Tu feras périr leur fruit de la terre et leur race du milieu des fils d'humains.
21:12	Car ils ont projeté du mal contre toi, ils ont conçu de mauvais desseins, mais ils ne prévaudront pas.
21:13	Parce que tu les mettras sur le dos, tu dirigeras les cordes de ton arc contre leurs faces.
21:14	Lève-toi, YHWH, par ta force ! Nous chanterons et célébrerons ta puissance.

## Chapitre 22

### Les souffrances du Mashiah

22:1	Psaume de David. Au chef de musique. Sur Ayeleth-Hashachar<!--Biche de l'Aurore.-->.
22:2	Mon El ! Mon El ! Pourquoi m'as-tu abandonné<!--Le Psaume 22 est une description détaillée de la mort par crucifixion du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) (Mt. 27:45-46).-->, t'éloignant de ma délivrance et des paroles de mon rugissement ?
22:3	Mon Elohîm ! Je crie le jour, mais tu ne réponds pas, la nuit, et je n'ai pas de repos.
22:4	Pourtant tu es le Saint, tu habites au milieu des louanges d'Israël.
22:5	Nos pères se sont confiés en toi ; ils se sont confiés, et tu les as délivrés.
22:6	Ils ont crié vers toi, et ils ont été délivrés ; ils se sont appuyés sur toi, et ils n'ont pas été confus<!--Es. 49:23 ; Ps. 25:3, 31:2.-->.
22:7	Et moi, je suis un ver et non un homme, l'opprobre des humains et le méprisé du peuple<!--Es. 53:2-3.-->.
22:8	Tous ceux qui me voient se moquent de moi, ils ouvrent les lèvres, secouent la tête<!--Ps. 109:25 ; Mt. 27:39.--> :
22:9	Il s'abandonne, disent-ils, à YHWH ! Qu'il te délivre, et qu'il te sauve, puisqu'il prend son bon plaisir en toi<!--Mt. 27:43.--> !
22:10	Cependant, c'est toi qui m'as tiré hors du ventre de ma mère, qui m'as mis en sûreté lorsque j'étais sur les mamelles de ma mère.
22:11	J'ai été sous ta garde, dès le sein maternel, tu as été mon El dès le ventre de ma mère<!--Es. 49:1.-->.
22:12	Ne t'éloigne pas de moi, car la détresse est près de moi, et il n'y a personne qui me secoure<!--Ps. 69:21.--> !
22:13	Beaucoup de taureaux sont autour de moi, de puissants taureaux de Bashân m'entourent.
22:14	Ils ouvrent leur gueule contre moi, comme un lion qui déchire et rugit.
22:15	Je suis comme de l'eau qui s'écoule, et tous mes os se séparent ; mon cœur est comme de la cire, il se fond dans mes entrailles.
22:16	Ma force se dessèche comme l'argile, et ma langue s'attache à mon palais ; tu me réduis à la poussière de la mort.
22:17	Car des chiens m'environnent, une assemblée de méchants m'entoure, ils ont percé mes mains et mes pieds.
22:18	Je pourrais compter tous mes os un par un. Eux, ils m'examinent, ils me regardent.
22:19	Ils se partagent mes vêtements et tirent au sort ma tunique<!--Mt. 27:35 ; Mc. 15:24 ; Lu. 23:33.-->.
22:20	Et toi, YHWH, ne t'éloigne pas ! Ma force, hâte-toi de me secourir !
22:21	Délivre ma vie de l'épée, mon unique du pouvoir des chiens !
22:22	Sauve-moi de la gueule du lion, délivre-moi des cornes du buffle !
22:23	Je déclarerai ton Nom à mes frères, je te louerai au milieu de l'assemblée<!--Hé. 2:12.-->.
22:24	Vous qui craignez YHWH, louez-le ! Toute la race de Yaacov, glorifiez-le ! Toute la race d'Israël, redoutez-le !
22:25	Car il n'a ni mépris ni dédain pour les peines du misérable, et il ne lui cache pas sa face, mais il l'écoute quand il crie au secours à lui.
22:26	Ta louange commencera par moi dans la grande assemblée ; j'accomplirai mes vœux en présence de ceux qui te craignent<!--Ps. 56:13.-->.
22:27	Les malheureux mangeront et seront rassasiés, ceux qui cherchent YHWH le loueront. Votre cœur vivra à perpétuité !
22:28	Toutes les extrémités de la Terre s'en souviendront, ils se convertiront à YHWH, et toutes les familles des nations se prosterneront devant toi<!--Ps. 72:8-11, 86:9.-->.
22:29	Car le règne appartient à YHWH : il domine sur les nations.
22:30	Tous les puissants de la Terre mangeront et se prosterneront devant lui, tous ceux qui descendent dans la poussière s'inclineront, même celui qui ne peut conserver sa vie.
22:31	La postérité le servira, on parlera d'Adonaï de génération en génération<!--Es. 59:21, 65:23 ; Ps. 110:3.-->.
22:32	Ils viendront et ils publieront sa justice au peuple qui naîtra, parce qu'il aura fait ces choses.

## Chapitre 23

### Le Bon Berger

23:1	Psaume de David. YHWH est mon Berger<!--YHWH, le Bon Berger, est notre Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) (Es. 40:11 ; Jé. 23:4 ; Jn. 10:11). Voir aussi Ps. 28:9.--> : je ne manquerai de rien.
23:2	Il me fait reposer dans de verts pâturages, il me dirige près des eaux de repos.
23:3	Il restaure mon âme, il me conduit dans les sentiers de la justice pour l'amour de son Nom.
23:4	Même quand je marcherais dans la vallée de l'ombre de la mort, je ne craindrais aucun mal<!--Ps. 118:6.-->, car tu es avec moi : ton bâton et ta houlette me consolent.
23:5	Tu dresses devant moi une table, en face de mes adversaires ; tu oins d'huile ma tête et ma coupe déborde.
23:6	En effet, la bonté et la miséricorde m'accompagneront tous les jours de ma vie, et j'habiterai dans la maison de YHWH pour toujours.

## Chapitre 24

### Accueil de YHWH, le Roi de gloire

24:1	Psaume de David. La Terre appartient à YHWH, avec tout ce qui est en elle<!--Ex. 19:5 ; De. 10:14 ; Ps. 50:12 ; Job 41:2 ; 1 Co. 10:26.-->, la terre habitable et ceux qui y habitent !
24:2	Car il l'a fondée sur les mers, et l'a établie sur les fleuves.
24:3	Qui pourra monter à la montagne de YHWH ? Et qui pourra se lever dans son lieu saint<!--Ps. 15:1-2, 118:19.--> ?
24:4	Celui qui a les mains pures et le cœur pur, qui ne livre pas son âme à la fausseté, et qui ne jure pas pour tromper.
24:5	Il obtiendra la bénédiction de YHWH et la justice d'Elohîm, son Sauveur.
24:6	Tels sont ceux qui l'invoquent, ceux qui cherchent ta face en Yaacov. Sélah.
24:7	Portes, levez la tête ! Levez-vous portes éternelles ! Et le Roi de gloire entrera !
24:8	Qui est ce Roi de gloire ? C'est YHWH, le Fort et le Puissant, YHWH, le Puissant à la guerre.
24:9	Portes, levez la tête ! Levez-la, portes éternelles ! Et le Roi de gloire entrera !
24:10	Qui est ce Roi de gloire ? YHWH Sabaoth : c'est lui qui est le Roi de gloire ! Sélah.

## Chapitre 25

### La crainte d'Elohîm mène à la voie de YHWH

25:1	Psaume de David. [Aleph.] YHWH, j'élève mon âme à toi.
25:2	[Beth.] Mon Elohîm ! je me confie en toi : fais que je ne sois pas honteux<!--Ps. 22:5, 31:2.--> ! Et que mes ennemis ne triomphent pas de moi !
25:3	[Guimel.] En effet, tous ceux qui espèrent en toi ne seront pas confus<!--Ro. 10:11.--> ; ceux qui agissent avec tromperie sans cause seront confus.
25:4	[Daleth.] YHWH ! Fais-moi connaître tes voies, enseigne-moi tes sentiers<!--Ps. 27:11, 86:11, 143:10.-->.
25:5	[He. Vav.] Fais-moi marcher selon la vérité, et instruis-moi, car tu es l'Elohîm de ma délivrance, je m'attends à toi tous les jours.
25:6	[Zayin.] YHWH ! Souviens-toi de tes compassions et de ta bonté, car elles sont éternelles<!--Jé. 33:11 ; Ps. 103:17, 106:1, 107:1, 117:2, 136:1-2.-->.
25:7	[Heth.] Ne te souviens pas des péchés de ma jeunesse ni de mes transgressions ! Souviens-toi de moi selon ta miséricorde, à cause de ta bonté, YHWH !
25:8	[Teth.] YHWH est bon et droit : C'est pourquoi il enseigne aux pécheurs le chemin qu'ils doivent tenir.
25:9	[Yod.] Il conduit les humbles dans la justice, et il leur enseigne sa voie.
25:10	[Kaf.] Tous les sentiers de YHWH sont miséricorde et vérité, pour ceux qui gardent son alliance et ses témoignages.
25:11	[Lamed.] Pour l'amour de ton Nom, YHWH ! tu me pardonneras mon iniquité, quoiqu'elle soit grande<!--2 S. 24:10.-->.
25:12	[Mem.] Qui est l'homme qui craint YHWH ? YHWH lui enseignera le chemin qu'il doit choisir.
25:13	[Noun.] Son âme demeurera dans le bonheur, et sa postérité possédera la Terre en héritage.
25:14	[Samech.] Les secrets de YHWH sont pour ceux qui le craignent, et son alliance leur donne le savoir.
25:15	[Ayin.] Mes yeux sont continuellement sur YHWH, car c'est lui qui sortira mes pieds du filet.
25:16	[Pe.] Tourne ta face vers moi, et aie pitié de moi, car je suis seul et affligé.
25:17	[Tsade.] Les détresses de mon cœur se sont agrandies : fais-moi sortir de mes angoisses !
25:18	[Resh.] Vois ma misère et ma peine, et pardonne tous mes péchés.
25:19	[Resh.] Vois combien mes ennemis sont nombreux, et me haïssent d'une haine pleine de violence<!--Jn. 15:25.-->.
25:20	[Shin.] Garde mon âme et délivre-moi ! Que je ne sois pas confus, car je me suis réfugié en toi !
25:21	[Tav.] Que l'innocence et la droiture me protègent, car je m'attends à toi !
25:22	[Pe.] Elohîm ! rachète Israël de toutes ses détresses !

## Chapitre 26

### Demeurer dans l'intégrité

26:1	Psaume de David. YHWH, rends-moi justice<!--Ps. 43:1, 54:3.--> ! Car je marche dans mon intégrité, je me confie en YHWH, je ne chancelle pas.
26:2	Sonde-moi et éprouve-moi<!--Ps. 11:4-5, 17:3, 139:23.-->, YHWH ! Fais passer au creuset mes reins et mon cœur,
26:3	car ta grâce est devant mes yeux, et je marche dans ta vérité.
26:4	Je ne m'assieds pas avec les hommes faux<!--Ps. 1:1 ; 1 Co. 5:9-11, 15:33.-->, et je ne vais pas avec les gens dissimulés.
26:5	Je hais la compagnie de ceux qui font le mal<!--Ps. 101:2-5, 119:113.-->, et je ne m'assieds pas avec les méchants.
26:6	Je lave mes mains dans l'innocence et je fais le tour de ton autel<!--Ps. 73:13.-->, YHWH !
26:7	pour faire entendre le cri des actions de grâces, et pour raconter toutes tes merveilles.
26:8	YHWH, j'aime la demeure de ta maison, le lieu dans lequel est le tabernacle de ta gloire.
26:9	N'enlève pas mon âme avec les pécheurs, ma vie avec les hommes de sang,
26:10	dans les mains desquels il y a de la méchanceté préméditée, et dont la main droite est pleine de pot-de-vin.
26:11	Mais moi, je marche dans mon intégrité. Délivre-moi et aie pitié de moi !
26:12	Mon pied se tient dans la droiture. Je bénirai YHWH dans les assemblées.

## Chapitre 27

### La foi qui triomphe des épreuves

27:1	Psaume de David. YHWH est ma lumière<!--Es. 60:19-20 ; Mi. 7:8 ; Ps. 118:6 ; Jn. 8:12 ; Ap. 21:23.--> et mon salut : de qui aurai-je peur ? YHWH est le soutien de ma vie : de qui aurai-je peur ?
27:2	Lorsque les méchants s'avancent contre moi pour dévorer ma chair, ce sont mes adversaires et mes ennemis qui chancellent et tombent.
27:3	Si toute une armée campait contre moi, mon cœur ne craindrait pas. Si une guerre s'élevait contre moi, je serais plein de confiance.
27:4	Je demande une chose à YHWH, que je désire ardemment : c'est d'habiter dans la maison de YHWH tous les jours de ma vie, pour contempler la beauté de YHWH et pour admirer son temple.
27:5	Car il me cachera dans son tabernacle au jour du malheur, il me cachera sous la couverture<!--Voir Ps. 91:1.--> de sa tente, il m'élèvera sur un rocher.
27:6	Maintenant même, ma tête s'élève par-dessus mes ennemis qui m'entourent. J'offrirai des sacrifices dans sa tente, au son de la trompette, je chanterai et célèbrerai YHWH.
27:7	YHWH ! Écoute ma voix, je t'invoque : Aie pitié de moi et exauce-moi !
27:8	Mon cœur dit de ta part : Cherche ma face ! Je chercherai ta face, YHWH !
27:9	Ne me cache pas ta face, ne rejette pas avec colère ton serviteur ! Tu es mon secours, ne me laisse pas, ne m'abandonne pas, Elohîm de mon salut !
27:10	Car mon père et ma mère m'abandonnent, mais YHWH me recueillera<!--Es. 49:15.-->.
27:11	YHWH, enseigne-moi ta voie, et conduis-moi dans le sentier de la droiture, à cause de mes ennemis<!--Ps. 5:9, 25:4-5.-->.
27:12	Ne me livre pas au désir de mes adversaires, car contre moi se sont levés de faux témoins et ceux qui ne respirent que la violence.
27:13	Oh ! Si je n'étais pas sûr de voir la bonté de YHWH sur la Terre des vivants...
27:14	Attends-toi à YHWH ! Demeure ferme et que ton cœur<!--Es. 33:2 ; Ps. 31:25.--> se fortifie ! Attends-toi à YHWH.

## Chapitre 28

### Louange à YHWH, le Rocher de son peuple

28:1	De David. Je crie à toi, YHWH ! Mon rocher ! Ne reste pas sourd envers moi, de peur que si tu ne me réponds pas, je ne sois semblable à ceux qui descendent dans la fosse<!--Ps. 4:2, 143:7. Voir commentaire en Es. 8:13-17.-->.
28:2	Écoute la voix de mes supplications quand je crie au secours vers toi, quand j'élève mes mains vers ta sainteté, ton lieu très-saint.
28:3	Ne m'emporte pas avec les méchants ni avec ceux qui pratiquent la méchanceté, qui parlent de paix avec leur prochain pendant que la malice est dans leur cœur<!--Jé. 9:8 ; Ps. 26:9.-->.
28:4	Donne-leur selon leurs œuvres et selon la méchanceté de leurs actions, donne-leur selon l'ouvrage de leurs mains, rends-leur ce qu'ils ont mérité<!--2 Ti. 4:14.-->.
28:5	Parce qu'ils ne prennent pas garde aux œuvres de YHWH, à l'œuvre de ses mains. Qu'il les renverse et ne les édifie pas !
28:6	Béni soit YHWH ! Car il exauce la voix de mes supplications.
28:7	YHWH est ma force et mon bouclier. Mon cœur se confie en lui, et je suis secouru. Mon cœur se réjouit, c'est pourquoi je le loue par mes chants.
28:8	YHWH est la force de son peuple, il est le refuge des délivrances de son mashiah.
28:9	Sauve ton peuple et bénis ton héritage ! Sois leur berger<!--Ps. 23:1.--> et porte-les pour toujours !

## Chapitre 29

### La suprématie d'Elohîm

29:1	Psaume de David. Fils de El, rendez à YHWH, rendez à YHWH la gloire et la force<!--Ps. 96:7-8.--> !
29:2	Rendez à YHWH la gloire due à son Nom ! Prosternez-vous devant YHWH avec des ornements sacrés !
29:3	La voix de YHWH est sur les eaux, le El de gloire<!--Le El kabowd.--> fait tonner, YHWH est sur les grandes eaux.
29:4	La voix de YHWH est forte, la voix de YHWH est majestueuse.
29:5	La voix de YHWH brise les cèdres, YHWH brise les cèdres du Liban,
29:6	il les fait sauter comme un veau, le Liban et le Shiryown comme de jeunes buffles.
29:7	La voix de YHWH fait jaillir des flammes de feu.
29:8	La voix de YHWH fait trembler le désert, YHWH fait trembler le désert de Qadesh.
29:9	La voix de YHWH fait naître les biches, et dépouille les forêts. Dans son palais tout s'écrie : Gloire<!--Kabowd.--> !
29:10	YHWH était assis lors du déluge, YHWH est assis en Roi pour toujours<!--Ps. 146:10.-->.
29:11	YHWH donne de la force à son peuple, YHWH bénit son peuple en paix.

## Chapitre 30

### De la délivrance découle la louange

30:1	Psaume. Cantique pour la dédicace de la maison de David.
30:2	YHWH, je t'exalte parce que tu m'as relevé, tu n'as pas voulu que mes ennemis se réjouissent à mon sujet.
30:3	YHWH, mon Elohîm ! j'ai crié au secours à toi, et tu m'as guéri.
30:4	YHWH ! tu as fait monter mon âme du shéol, tu m'as rendu la vie, afin que je ne descende pas dans la fosse.
30:5	Chantez à YHWH, vous ses bien-aimés, et célébrez la mémoire de sa sainteté<!--Ps. 97:12.--> !
30:6	Car sa colère dure un instant, mais sa grâce toute la vie. Le soir arrivent les pleurs, et le matin les cris de louange.
30:7	Dans ma sécurité, je disais : Je ne serai jamais ébranlé<!--Ps. 10:6.--> !
30:8	YHWH ! Ta faveur m'a établi sur de fortes montagnes... Tu caches ta face, et je suis terrifié<!--Ps. 13:2, 88:15, 102:3, 143:7.-->.
30:9	YHWH ! J'ai crié à toi, j'ai présenté ma supplication à YHWH :
30:10	Que gagnes-tu à verser mon sang si je descends dans la fosse ? La poussière te célébrera-t-elle ? Racontera-t-elle ta fidélité<!--Es. 38:18.--> ?
30:11	YHWH, écoute, et aie pitié de moi ! YHWH, secours-moi !
30:12	Tu as changé mon deuil en allégresse, tu as détaché mon sac, et tu m'as ceint de joie,
30:13	afin que ma langue te loue<!--Ps. 57:10.--> et ne se taise pas. YHWH, mon Elohîm ! je te célébrerai toujours.

## Chapitre 31

### Recherche de la protection divine

31:1	Psaume de David. Au chef de musique.
31:2	YHWH ! Tu es mon refuge : fais que je ne sois jamais confus ! Délivre-moi par ta justice<!--Ps. 25:2-20, 71:1-2.--> !
31:3	Incline ton oreille vers moi, hâte-toi de me délivrer ! Sois pour moi un rocher de protection, une forteresse, afin que je puisse m'y sauver !
31:4	Car tu es mon rocher et ma forteresse. Tu me dirigeras et tu me donneras du repos, pour l'amour de ton Nom.
31:5	Fais-moi sortir du filet qu'ils m'ont tendu en secret, car tu es ma protection.
31:6	Je remets mon esprit entre tes mains<!--Lu. 23:46.-->. Tu me rachèteras, YHWH, le El de vérité !
31:7	Je hais ceux qui s'adonnent aux vanités trompeuses, et je me confie en YHWH.
31:8	Je serai par ta bonté dans l'allégresse et dans la joie, car tu vois mon affliction, tu sais les angoisses de mon âme,
31:9	et parce que tu ne m'as pas livré entre les mains de l'ennemi, mais tu feras tenir mes pieds au large.
31:10	YHWH, aie pitié de moi ! Car je suis dans la détresse. Mes yeux, mon âme et mon corps dépérissent de chagrin<!--Ps. 6:8, 88:10.-->.
31:11	Car ma vie se consume dans la douleur, et mes années dans les soupirs ; ma force chancelle à cause de mon iniquité, et mes os sont consumés.
31:12	J'ai été un objet d'opprobre à cause de tous mes adversaires, je l'ai même été extrêmement pour mes voisins, et de terreur pour ceux qui me connaissent ; ceux qui me voient dehors s'enfuient loin de moi<!--Ps. 38:12 ; Job 19:13-14.-->.
31:13	Je suis oublié de leurs cœurs comme un mort, je suis comme un vase brisé.
31:14	Car j'entends les calomnies de plusieurs, la crainte m'environne, quand ils se concertent unis contre moi : ils projettent de m'ôter la vie<!--Jé. 20:10.-->.
31:15	Toutefois, je me confie en toi, YHWH ! Je dis : Tu es mon Elohîm !
31:16	Ma destinée est entre tes mains : délivre-moi de la main de mes ennemis et de ceux qui me poursuivent !
31:17	Fais briller ta face sur ton serviteur<!--Ps. 4:7, 67:2.-->, délivre-moi par ta bonté !
31:18	YHWH, que je ne sois pas confus puisque je t'ai invoqué. Que les méchants soient confus, qu'ils soient couchés dans le shéol !
31:19	Que les lèvres menteuses soient muettes, elles profèrent des paroles dures contre le juste, avec orgueil et avec mépris !
31:20	Que ta bonté est grande<!--Ps. 36:6.--> ! Toi qui la réserves pour ceux qui te craignent, tu leur fais un refuge à la vue des fils d'humains !
31:21	Tu les caches sous la couverture<!--Ps. 91:1.--> de ta face, loin du complot des hommes, tu les caches sous ton abri contre les langues querelleuses.
31:22	Béni soit YHWH ! Car il a rendu merveilleuse sa bonté envers moi, comme si j'avais été dans une ville retranchée.
31:23	Je disais dans ma précipitation : Je suis retranché loin de ton regard ! Mais tu as entendu la voix de mes supplications quand j'ai crié au secours vers toi.
31:24	Aimez YHWH, vous tous, ses bien-aimés ! YHWH garde les fidèles, et il punit sévèrement celui qui agit avec orgueil.
31:25	Vous tous qui espérez en YHWH<!--Ps. 27:14.-->, demeurez fermes et il fortifiera votre esprit !

## Chapitre 32

### La puissance du pardon

32:1	Cantique de David. Heureux celui à qui la transgression est pardonnée, et dont le péché est couvert !
32:2	Heureux l'homme à qui YHWH ne tient pas compte de son iniquité<!--Ro. 4:6-8.-->, et dans l'esprit duquel il n'y a pas de tromperie !
32:3	Quand je me suis tu, mes os se sont consumés, je n'ai fait que gémir tout le jour,
32:4	parce que jour et nuit ta main s'appesantissait sur moi<!--Ps. 38:3.-->, ma vigueur s'est changée en une sécheresse d'été. Sélah.
32:5	Je t'ai fait connaître mon péché et je n'ai pas caché mon iniquité. J'ai dit : J'avouerai mes transgressions à YHWH<!--Pr. 28:13 ; 1 Jn. 1:9.--> ! Et tu as ôté la peine de mon péché. Sélah.
32:6	C'est pourquoi tout fidèle te prie au temps où tu peux être trouvé<!--Es. 55:6 ; So. 2:3 ; Ps. 69:14.-->. Même si les grandes eaux débordent, elles ne l'atteindront pas.
32:7	Tu es ma couverture<!--Ps. 91:1.-->, tu me gardes de la détresse, tu m'environnes de chants de triomphe à cause de ta délivrance. Sélah.
32:8	Je te rendrai intelligent, je t'enseignerai la voie dans laquelle tu dois marcher, je te guiderai de mon œil.
32:9	Ne devenez pas comme le cheval ni comme le mulet qui sont sans intelligence, dont l'ornement est la bride et le mors, pour les freiner afin qu'ils ne s'approchent pas de toi<!--Ja. 3:3.-->.
32:10	Beaucoup de douleurs atteindront le méchant<!--Pr. 19:29.-->, mais la bonté environne l'homme qui se confie en YHWH.
32:11	Vous justes, réjouissez-vous en YHWH, soyez dans l'allégresse ! Criez de joie, vous tous qui êtes droits de cœur<!--Ps. 33:1, 64:11.--> !

## Chapitre 33

### Louanges à YHWH, l'Elohîm fidèle

33:1	Vous justes, poussez un cri de joie à cause de YHWH<!--Ps. 32:11, 97:12, 147:1.--> ! La louange convient aux gens droits.
33:2	Célébrez YHWH avec la harpe, chantez-lui des psaumes sur le luth à dix cordes !
33:3	Chantez-lui un cantique nouveau<!--Ps. 40:4, 96:1, 98:1, 144:9 ; Ap. 5:9, 14:3.--> ! Jouez bien de vos instruments avec un cri de joie !
33:4	Car la parole de YHWH est droite et toutes ses œuvres s'accomplissent avec fidélité !
33:5	Il aime la justice et la droiture<!--Ps. 45:8 ; Hé. 1:9.-->. La Terre est remplie de la bonté de YHWH.
33:6	Les cieux ont été faits par la parole de YHWH, et toute leur armée par le souffle de sa bouche<!--Ge. 2:1-2.-->.
33:7	Il amoncelle en un tas les eaux de la mer, il met les abîmes dans des réservoirs.
33:8	Que toute la Terre craigne YHWH ! Que tous les habitants du monde le redoutent !
33:9	Car il dit, et la chose arrive, il ordonne, et la chose se présente.
33:10	YHWH rompt le conseil des nations, il anéantit les desseins des peuples,
33:11	mais le conseil de YHWH subsiste à toujours, les desseins de son cœur subsistent d'âge en âge<!--Pr. 19:21.-->.
33:12	Bénie est la nation dont YHWH est l'Elohîm<!--Ps. 144:15.--> et le peuple qu'il s'est choisi pour héritage !
33:13	YHWH regarde des cieux, il voit tous les fils d'humains<!--Job 28:24.-->.
33:14	Du lieu de sa demeure, il observe tous les habitants de la Terre.
33:15	C'est lui qui forme également leur cœur et qui prend garde à toutes leurs actions.
33:16	Le roi n'est pas sauvé par une grande armée, l'homme puissant n'échappe pas par sa grande force.
33:17	Le cheval est impuissant pour sauver, et ne délivre pas par la grandeur de sa force<!--Ps. 147:10.-->.
33:18	Voici, l'œil de YHWH est sur ceux qui le craignent<!--Ps. 34:16 ; 1 Pi. 3:12.-->, sur ceux qui s'attendent à sa bonté,
33:19	pour délivrer leur âme de la mort et pour les faire vivre durant la famine.
33:20	Notre âme espère en YHWH : il est notre aide et notre bouclier.
33:21	Certainement notre cœur se réjouit en lui, parce que nous avons mis notre confiance en son saint Nom.
33:22	Que ta bonté soit sur nous, YHWH ! Selon l'espérance que nous avons eue en toi !

## Chapitre 34

### YHWH libère les siens

34:1	Psaume de David, lorsqu'il changea son comportement<!--Littéralement : Il changea son goût. 1 S. 21:13.--> en présence d'Abimélec, qui s'en alla, chassé par lui.
34:2	[Aleph.] Je bénirai YHWH en tout temps ; sa louange sera continuellement dans ma bouche.
34:3	[Beth.] Mon âme se glorifie en YHWH ! Que les pauvres écoutent et se réjouissent !
34:4	[Guimel.] Glorifiez YHWH avec moi ! Élevons son Nom tous ensemble !
34:5	[Daleth.] J'ai cherché YHWH et il m'a répondu, il m'a délivré de toutes mes frayeurs.
34:6	[He. Vav.] Quand on le regarde, on est illuminé, et la face n'est pas confuse.
34:7	[Zayin.] Cet affligé a crié, et YHWH l'a exaucé, et l'a délivré de toutes ses détresses.
34:8	[Heth.] L'Ange de YHWH campe autour de ceux qui le craignent, et les délivre.
34:9	[Teth.] Goûtez et voyez combien YHWH est bon<!--1 Pi. 2:3.--> ! Béni est l'homme qui se confie en lui !
34:10	[Yod.] Craignez YHWH, vous ses saints ! Car rien ne manque à ceux qui le craignent.
34:11	[Kaf.] Les lionceaux éprouvent la disette et la faim, mais ceux qui cherchent YHWH ne manquent d'aucun bien.
34:12	[Lamed.] Venez, mes fils, écoutez-moi ! Je vous enseignerai la crainte de YHWH.
34:13	[Mem.] Qui est l'homme qui prend plaisir à la vie, qui aime la prolonger pour jouir du bonheur ?
34:14	[Noun.] Garde ta langue du mal, et tes lèvres des paroles trompeuses<!--1 Pi. 3:10.--> ;
34:15	[Samech.] détourne-toi du mal et fais-le bien, cherche la paix et poursuis-la<!--Hé. 12:14.-->.
34:16	[Ayin.] Les yeux<!--Voir Ps. 33:18 et 1 Pi. 3:12.--> de YHWH sont sur les justes, et ses oreilles sont attentives à leur cri.
34:17	[Pe.] La face de YHWH est contre ceux qui font le mal, pour retrancher de la Terre leur mémoire<!--Jé. 44:11 ; Lé. 17:10.-->.
34:18	[Tsade.] Quand les justes crient, YHWH les exauce et il les délivre de toutes leurs détresses.
34:19	[Qof.] YHWH est près de ceux qui ont le cœur déchiré par la douleur, et il délivre ceux qui ont l'esprit abattu.
34:20	[Resh.] Le juste passe par beaucoup de souffrances, mais YHWH le délivre de toutes<!--2 Ti. 3:11.-->.
34:21	[Shin.] Il garde tous ses os, aucun d'eux n'est brisé.
34:22	[Tav.] Le mal tue le méchant, et ceux qui haïssent le juste sont détruits.
34:23	[Pe.] YHWH rachète l'âme de ses serviteurs, et aucun de ceux qui se confient en lui ne sera détruit.

## Chapitre 35

### Prière au juste Juge

35:1	Psaume de David. YHWH ! Défends-moi contre mes adversaires, combats ceux qui me combattent !
35:2	Prends le petit et le grand bouclier, et lève-toi pour me secourir !
35:3	Brandis la lance et le javelot contre mes persécuteurs ! Dis à mon âme : Je suis ton salut<!--Yeshuw`ah.--> !
35:4	Que ceux qui en veulent à ma vie soient honteux et confus<!--Jé. 17:18 ; Ps. 40:15, 70:3.--> ! Que ceux qui méditent ma perte se retirent en arrière et rougissent !
35:5	Qu'ils soient comme la balle emportée par le vent<!--Es. 29:5 ; Os. 13:3.-->, et que l'Ange de YHWH les chasse !
35:6	Que leur chemin soit ténébreux et glissant, et que l'Ange de YHWH les poursuive !
35:7	Car sans cause ils m'ont caché la fosse où étaient tendus leurs filets, sans cause ils l'ont creusée pour surprendre mon âme<!--Jé. 18:20 ; Ps. 57:7, 140:5, 141:9.-->.
35:8	Que la ruine les atteigne sans qu'ils le sachent, qu'ils soient capturés dans le filet qu'ils ont caché ! Qu'ils y tombent et soient ravagés !
35:9	Et mon âme aura de la joie en YHWH, de l'allégresse en sa délivrance.
35:10	Tous mes os diront : YHWH, qui est semblable à toi ? Qui délivre l'affligé de la main de celui qui est plus fort que lui, l'affligé et le pauvre de celui qui le pille ?
35:11	De faux témoins s'élèvent contre moi : on m'interroge sur ce que j'ignore.
35:12	Ils me rendent le mal pour le bien, tâchant de m'ôter la vie<!--Ps. 38:21, 109:5.-->.
35:13	Mais moi, quand ils étaient malades, je me couvrais d'un sac, j'affligeais mon âme par le jeûne, je priais dans mon sein.
35:14	Comme pour un ami intime, pour un frère, j'étais abattu, en pleurs, comme pour le deuil d'une mère.
35:15	Mais quand je chancelle, ils se réjouissent et s'assemblent, ils s'assemblent contre moi sans que je le sache pour me frapper, ils me déchirent pour que je sois silencieux ;
35:16	avec les athées d'entre les railleurs qui suivent les bonnes tables, et ils ont grincé les dents contre moi.
35:17	Adonaï ! jusqu'à quand le verras-tu ? Détourne mon âme de leurs tempêtes, mon unique des lionceaux.
35:18	Je te célébrerai dans la grande assemblée, je te louerai parmi un peuple nombreux<!--Ps. 111:1.-->.
35:19	Que ceux qui sont mes ennemis par leurs mensonges ne se réjouissent pas de moi, que ceux qui me haïssent sans cause ne clignent pas l'œil<!--Jn. 15:25.--> !
35:20	Car ils ne parlent pas de paix, mais ils préméditent des choses pleines de tromperies contre les gens tranquilles de la Terre.
35:21	Ils ont ouvert leur bouche autant qu'ils ont pu contre moi, et ont dit : Ah ! Ah ! Nos yeux l'ont vu !
35:22	YHWH, tu le vois ! Ne te tais pas<!--Ps. 83:2.--> ! Adonaï, ne t'éloigne pas de moi !
35:23	Réveille-toi, réveille-toi pour me rendre justice<!--Ps. 44:24.--> ! Adonaï mon Elohîm, défends ma cause !
35:24	Juge-moi selon ta justice, YHWH, mon Elohîm ! Et qu'ils ne se réjouissent pas de moi !
35:25	Qu'ils ne disent pas dans leur cœur : Ah ! Notre âme ! Et qu'ils ne disent pas : Nous l'avons englouti !
35:26	Que ceux qui se réjouissent de mon mal soient honteux et rougissent tous ensemble ! Que ceux qui s'élèvent contre moi soient couverts de honte et de confusion !
35:27	Mais que ceux qui prennent plaisir à ma justice se réjouissent avec des chants de triomphe, qu'ils disent sans cesse : Grand est YHWH qui désire la paix de son serviteur !
35:28	Alors ma langue publiera ta justice et ta louange tous les jours.

## Chapitre 36

### Opposition : les justes et les méchants

36:1	Psaume de David, serviteur de YHWH. Au chef de musique.
36:2	La transgression du méchant me dit, au dedans de mon cœur, qu'il n'y a pas de crainte d'Elohîm devant ses yeux.
36:3	Car il se flatte à ses propres yeux pour consumer, pour assouvir sa haine.
36:4	Les paroles de sa bouche ne sont que méchanceté et tromperie, il cesse d'être sage et de faire le bien.
36:5	Il projette le malheur sur sa couche, il se tient sur un chemin qui n'est pas bon, il ne rejette pas le mal.
36:6	YHWH ! ta bonté atteint jusqu'aux cieux, ta fidélité jusqu'aux nuages<!--Ps. 57:11, 108:5.-->.
36:7	Ta justice est comme les montagnes de El, tes jugements sont un grand abîme. YHWH, tu sauves les humains et les bêtes.
36:8	Elohîm ! Combien est précieuse ta bonté ! Aussi les fils d'humains se retirent à l'ombre de tes ailes<!--Ps. 17:8, 57:2.-->.
36:9	Ils seront abondamment rassasiés de la graisse de ta maison, et tu les abreuveras au fleuve de tes délices.
36:10	Car la source de la vie est auprès de toi, et par ta lumière nous voyons la lumière.
36:11	Étends ta bonté sur ceux qui te connaissent, et ta justice sur ceux qui ont le cœur droit !
36:12	Que le pied de l'orgueilleux ne s'avance pas sur moi, et que la main des méchants ne m'ébranle pas !
36:13	Là sont tombés ceux qui pratiquent la méchanceté, ils sont renversés et ne peuvent se relever.

## Chapitre 37

### Se confier en la justice de YHWH

37:1	Psaume de David. [Aleph.] Ne t'irrite pas contre les méchants, ne jalouse pas ceux qui s'adonnent à l'injustice<!--Pr. 23:17, 24:19.-->.
37:2	Car ils seront soudainement retranchés comme le foin, et ils se faneront comme l'herbe verte.
37:3	[Beth.] Confie-toi en YHWH, et fais ce qui est bon, aie le pays pour demeure et la fidélité pour pâture.
37:4	Fais de YHWH tes délices, et il t'accordera ce que ton cœur désire.
37:5	[Guimel.] Recommande tes voies à YHWH, confie-toi en lui et il agira<!--Ps. 22:9, 55:23 ; Pr. 16:3.-->.
37:6	Il manifestera ta justice comme la lumière, et ton droit comme le soleil à son midi<!--Pr. 4:18.-->.
37:7	[Daleth.] Garde le silence devant YHWH, et tremble devant lui. Ne t'irrite pas contre celui qui réussit dans ses voies, contre celui qui vient à bout de ses mauvais desseins.
37:8	[He.] Laisse la colère, et abandonne la rage<!--Ep. 4:26.--> ; ne t'irrite pas pour faire le mal.
37:9	Car les méchants seront retranchés, mais ceux qui se confient en YHWH hériteront la Terre.
37:10	[Vav.] Encore un peu de temps, et le méchant ne sera plus ; tu regardes le lieu où il était et il n'y est plus<!--Job 7:10, 20:9.-->.
37:11	Les pauvres prennent possession de la Terre et jouissent abondamment de la paix.
37:12	[Zayin.] Le méchant complote contre le juste, et grince ses dents contre lui.
37:13	Adonaï se rit de lui, car il voit que son jour approche.
37:14	[Heth.] Les méchants tirent leur épée et bandent leur arc, pour faire tomber le malheureux et le pauvre, pour massacrer ceux qui marchent dans la droiture<!--Ps. 11:2.-->.
37:15	Mais leur épée entre dans leur propre cœur, et leurs arcs se brisent.
37:16	[Teth.] Mieux vaut au juste le peu qu'il a, que l'abondance de beaucoup de méchants<!--Pr. 15:16-17 ; Ec. 4:6.-->,
37:17	car les bras des méchants seront brisés, mais YHWH soutient les justes.
37:18	[Yod.] YHWH connaît les jours de ceux qui sont intègres, et leur héritage demeure à jamais.
37:19	Ils ne sont pas honteux au jour du malheur, mais ils sont rassasiés au jour de la famine.
37:20	[Kaf.] Mais les méchants périssent, et les ennemis de YHWH, comme les beaux pâturages, s'évanouissent ; ils s'évanouissent en fumée.
37:21	[Lamed.] Le méchant emprunte et ne rend pas, mais le juste a compassion et donne.
37:22	Car les bénis de YHWH hériteront la Terre, mais ceux qu'il a maudits seront retranchés.
37:23	[Mem.] YHWH affermit les pas de l'homme, et il prend plaisir à ses voies.
37:24	S'il tombe, il ne sera pas entièrement abattu, car YHWH le soutient de sa main.
37:25	[Noun.] J'ai été jeune et j'ai vieilli, et je n'ai pas vu le juste abandonné ni sa postérité mendiant son pain.
37:26	Il est compatissant tout le temps, il prête, et sa postérité est bénie.
37:27	[Samech.] Détourne-toi de ce qui est mauvais et fais ce qui est bon, et tu demeureras éternellement.
37:28	Car YHWH aime ce qui est juste, et il n'abandonne pas ses fidèles. C'est pourquoi ils sont sous sa garde pour toujours, mais la postérité des méchants est retranchée.
37:29	[Ayin.] Les justes hériteront la Terre et y habiteront à perpétuité.
37:30	[Pe.] La bouche du juste prononce la sagesse, et sa langue déclare la justice.
37:31	La torah de son Elohîm est dans son cœur<!--Ps. 40:8-9.-->, aucun de ses pas ne chancellera.
37:32	[Tsade.] Le méchant épie le juste, et cherche à le faire mourir.
37:33	YHWH ne l'abandonne pas entre ses mains et ne le laisse pas condamner quand on le juge.
37:34	[Qof.] Espère en YHWH, et garde sa voie, et il t'élèvera pour que tu hérites la Terre, tu verras les méchants retranchés.
37:35	[Resh.] J'ai vu le méchant dans toute sa puissance, il s'étendait comme un arbre verdoyant.
37:36	Il a passé, et voici, il n'est plus. Je le cherche, et il ne se trouve plus.
37:37	[Shin.] Observe l'homme intègre, et considère l'homme droit, car il y a une issue pour l'homme de paix.
37:38	Mais les rebelles seront tous détruits, et ce qui sera resté des méchants sera retranché.
37:39	[Tav.] Mais la délivrance des justes vient de YHWH, il est leur protection au temps de la détresse.
37:40	YHWH les secourt et les délivre, il les délivre des méchants et les sauve, parce qu'ils se confient en lui.

## Chapitre 38

### La tristesse du péché mène à la repentance

38:1	Psaume de David. Pour souvenir.
38:2	YHWH ! ne me juge pas dans ta colère et ne me châtie pas dans ta fureur.
38:3	Car tes flèches m'ont atteint, et ta main s'est appesantie sur moi.
38:4	Il n'y a rien de sain dans ma chair à cause de ta colère, ni de paix dans mes os à cause de mon péché.
38:5	Car mes iniquités s'élèvent au-dessus de ma tête, elles se sont appesanties comme un pesant fardeau, au-delà de mes forces<!--Ps. 40:13.-->.
38:6	Mes plaies ont une mauvaise odeur et sont purulentes, à cause de ma folie.
38:7	Je suis courbé et abattu outre mesure, je marche en pleurs tout le jour.
38:8	Car un mal brûlant remplit mes reins, et dans ma chair il n'y a rien de sain.
38:9	Je suis affaibli et brisé, je rugis le cœur troublé.
38:10	Adonaï, tout mon désir est devant toi, et mon soupir ne t'es pas caché.
38:11	Mon cœur est agité çà et là, ma force m'abandonne, et la lumière de mes yeux n'est plus avec moi.
38:12	Ceux qui m'aiment, et même mes amis intimes, se tiennent loin de ma plaie, et mes proches se tiennent loin de moi<!--Job 19:13-14.-->.
38:13	Ceux qui en veulent à ma vie me tendent des pièges. Ceux qui cherchent ma perte parlent de calamités et méditent des tromperies tous les jours.
38:14	Mais moi, je suis comme un sourd, comme un muet qui n'ouvre pas sa bouche.
38:15	Je suis comme un homme qui n'entend pas, et qui n'a pas de réplique dans sa bouche.
38:16	Car je m'attends à toi, YHWH ! tu me répondras, Adonaï mon Elohîm !
38:17	Car j'ai dit : Qu'ils ne se réjouissent pas à mon sujet ! Dès que mon pied chancelle, ils s'élèvent orgueilleusement contre moi<!--Ps. 94:18.--> !
38:18	Car je suis près de tomber et ma douleur est continuellement devant moi.
38:19	Car je reconnais mon iniquité, et je suis dans la crainte à cause de mon péché.
38:20	Cependant mes ennemis, qui sont vivants, se renforcent, et ceux qui me haïssent à tort se multiplient.
38:21	Ceux qui me rendent le mal pour le bien sont mes adversaires, parce que je recherche le bien<!--Jé. 18:20 ; Ps. 109:5.-->.
38:22	Ne m'abandonne pas YHWH ! Mon Elohîm, ne t'éloigne pas de moi !
38:23	Hâte-toi de venir à mon secours ! Adonaï, tu es ma délivrance !

## Chapitre 39

### La faiblesse de l'homme

39:1	Psaume de David. Au chef de musique. À Yedoutoun.
39:2	J'ai dit : Je prends garde à mes voies, de peur de pécher par ma langue. Je garderai sur ma bouche une muselière, tant que le méchant sera devant moi.
39:3	Je suis resté muet, dans le silence, je me suis tu à propos de ce qui est bon, et ma douleur a été agitée.
39:4	Mon cœur s'enflammait au-dedans de moi, un feu intérieur me consumait, j’ai parlé avec ma langue :
39:5	YHWH, fais-moi connaître ma fin, quelle est la mesure de mes jours<!--Ps. 119:84.-->, afin que je sache à quel point je suis éphémère<!--« Rejeté », « abstention », « transitoire », « fugitif ».--> !
39:6	Voici, tu as donné à mes jours la largeur de la main, la durée de ma vie est comme un rien devant toi. Certainement, tout être humain quoiqu'il soit debout n'est qu'une vapeur<!--Ja. 4:14.-->. Sélah.
39:7	Certainement, l'homme se promène comme une ombre ! Certainement on s'agite inutilement ! On amasse des biens et on ne sait pas qui les recueillera.
39:8	Maintenant, que puis-je espérer, Adonaï ? Mon espérance est en toi.
39:9	Délivre-moi de toutes mes transgressions ! Ne m'expose pas aux insultes de l'insensé !
39:10	Je me suis tu et je n'ai pas ouvert ma bouche, parce que c'est toi qui agis.
39:11	Détourne de moi tes coups ! Je suis consumé par les attaques de ta main.
39:12	Aussitôt que tu châties quelqu'un, en le punissant à cause de son iniquité, tu détruis comme la teigne ce qu'il a de plus cher. Certainement, tout être humain n'est qu'une vapeur. Sélah.
39:13	YHWH, écoute ma prière et prête l'oreille à mon cri ! Ne sois pas sourd à mes larmes ! Car je suis un voyageur et un étranger chez toi, comme tous mes pères<!--Lé. 25:23 ; Ps. 119:19 ; 1 Pi. 2:11 ; Hé. 11:13.-->.
39:14	Détourne ton regard de moi, afin que je reprenne mes forces, avant que je m'en aille et que je ne sois plus.

## Chapitre 40

### Un cantique nouveau à YHWH

40:1	Psaume de David. Au chef de musique.
40:2	J'ai espéré, j'ai espéré en YHWH, et il s'est tourné vers moi et a entendu mon cri.
40:3	Il m'a retiré de la fosse de destruction, du fond de la boue, il a dressé mes pieds sur le roc, il a affermi mes pas.
40:4	Il a mis dans ma bouche un cantique nouveau, qui est la louange de notre Elohîm. Beaucoup verront cela et ils craindront et se confieront en YHWH.
40:5	Béni est l'homme qui place sa confiance en YHWH et qui ne se tourne pas vers les orgueilleux, vers ceux qui s'adonnent au mensonge !
40:6	YHWH, mon Elohîm ! Qu'ils sont grands les merveilles et les desseins que tu as faits pour nous ! Personne n'est comparable à toi. Je voudrais les raconter et les proclamer, mais leur nombre est trop grand pour en faire le compte<!--Yéhoshoua ha Mashiah (Jésus-Christ) est YHWH dont les œuvres ne pouvaient être racontées en détails ni par David, ni par Yohanan, tant elles sont nombreuses. Voir Jn. 21:25.-->.
40:7	Tu ne désires ni sacrifice ni offrande, tu m'as creusé les oreilles, tu ne demandes ni holocauste ni sacrifice pour le péché<!--Hé. 10:5.-->.
40:8	Alors je dis : Voici, je viens. Il est écrit de moi dans le rouleau du livre.
40:9	Mon Elohîm, je prends plaisir à faire ta volonté, et ta torah est au fond de mes entrailles<!--Es. 51:7 ; Ps. 37:31.-->.
40:10	Je prêche ta justice dans la grande assemblée. Vois, je ne ferme pas mes lèvres, YHWH, tu le sais !
40:11	Je ne cache pas ta justice, qui est dans mon cœur, je déclare ta fidélité et ta délivrance. Je ne cache pas ta bonté ni ta vérité dans la grande assemblée.
40:12	Toi, YHWH ! ne m'épargne pas tes compassions. Que ta bonté et ta vérité me gardent continuellement !
40:13	Car des maux sans nombre m'environnent : mes iniquités m'atteignent, et je ne supporte pas leur vue ; elles surpassent en nombre les cheveux de ma tête, et mon cœur m'abandonne.
40:14	YHWH, veuille me délivrer ! YHWH, hâte-toi de venir à mon secours !
40:15	Que tous ensemble ils soient honteux et confus, ceux qui cherchent mon âme pour la perdre ! Et que ceux qui prennent plaisir à mon malheur retournent en arrière et rougissent !
40:16	Que ceux qui disent de moi : Ah ! Ah ! soient consumés en récompense de la honte qu'ils m'ont faite.
40:17	Que tous ceux qui te cherchent soient dans l'allégresse et se réjouissent en toi<!--Ps. 70:5.--> ! Que ceux qui aiment ta délivrance disent continuellement : Grand est YHWH !
40:18	Moi, je suis affligé et misérable, mais Adonaï prend soin de moi. Tu es mon secours et mon libérateur : mon Elohîm, ne tarde pas<!--Ps. 70:6.--> !

## Chapitre 41

### Secours de YHWH dans le malheur

41:1	Psaume de David. Au chef de musique.
41:2	Béni est celui qui s'intéresse au pauvre ! YHWH le délivrera au jour du malheur,
41:3	YHWH le garde et lui conserve la vie. Il est heureux sur la Terre, et tu ne le livres pas au bon plaisir de ses ennemis.
41:4	YHWH le soutient sur son lit de douleur, tu retournes toute sa couche dans sa maladie.
41:5	Je dis : YHWH, aie pitié de moi ! Guéris mon âme ! car j'ai péché contre toi.
41:6	Mes ennemis disent du mal de moi : Quand mourra-t-il ? Et quand périra son nom ?
41:7	Si quelqu'un vient me voir, c'est pour dire des mensonges : son cœur fait provision de méchancetés<!--Ps. 5:10, 10:7, 12:3.-->. Il sort et parle dehors.
41:8	Tous ceux qui me haïssent chuchotent ensemble contre moi, ils projettent du mal contre moi :
41:9	Quelque chose de Bélial<!--Voir commentaire en De. 13:13.--> est attachée à lui. Le voilà couché, disent-ils, il ne se relèvera plus !
41:10	Même l'homme avec qui j'étais en paix, qui avait ma confiance et qui mangeait mon pain, a levé le talon contre moi<!--Il est question ici de la trahison du Mashiah par Yéhouda Iscariot (Judas) (Jn. 13:18-19).-->.
41:11	Mais toi, YHWH ! aie pitié de moi et relève-moi ! Et je leur rendrai ce qui leur est dû.
41:12	Je saurai que tu prends plaisir en moi, si mon ennemi ne triomphe pas de moi.
41:13	Pour moi, tu m'as soutenu à cause de mon intégrité, et tu m'as établi pour toujours en ta présence.
41:14	Béni soit YHWH, l'Elohîm d'Israël, d'éternité en éternité ! Amen ! Amen !

## Chapitre 42

### Avoir soif d'Elohîm

42:1	Cantique des fils de Koré. Au chef de musique.
42:2	Comme une biche soupire après des courants d'eau, ainsi mon âme soupire ardemment après toi, Elohîm !
42:3	Mon âme a soif d'Elohîm, du El vivant<!--Ps. 63:2, 84:3.--> : quand entrerai-je et me présenterai-je devant la face d'Elohîm ?
42:4	Mes larmes sont ma nourriture jour et nuit, quand on me dit chaque jour : Où est ton Elohîm<!--Ps. 80:6, 115:2.--> ?
42:5	Je me souviens de ces choses, en répandant mon âme au-dedans de moi, quand je marchais avec la foule, et que je m'en allais tout doucement en leur compagnie, avec une voix de triomphe et des actions de grâces, jusqu'à la maison d'Elohîm, et qu'une grande multitude de gens sautait alors de joie.
42:6	Mon âme, pourquoi t'abats-tu, et murmures-tu au-dedans de moi ? Attends-toi à Elohîm, car je le célébrerai encore ; sa face est la délivrance même.
42:7	Mon Elohîm ! mon âme est abattue au-dedans de moi, aussi je me souviens de toi, depuis la terre du Yarden<!--Jourdain.-->, depuis l'Hermon, et depuis la montagne de Mitsear.
42:8	Un abîme appelle un autre abîme au bruit de tes ondées, toutes tes vagues et tes flots passent sur moi.
42:9	YHWH enverra de jour sa bonté, et de nuit son cantique sera avec moi, et ma prière sera au El qui est ma vie.
42:10	Je dis à El, mon rocher : Pourquoi m'oublies-tu ? Pourquoi marcherai-je dans la tristesse à cause de l'oppression de l'ennemi ?
42:11	Comme avec une épée dans mes os, mes ennemis m'outragent, tandis qu'ils me disent chaque jour : Où est ton Elohîm ?
42:12	Mon âme, pourquoi t'abats-tu, et pourquoi murmures-tu au-dedans de moi ? Attends-toi à Elohîm ! car je le célébrerai encore, il est ma délivrance et mon Elohîm.

## Chapitre 43

### Espérer dans la délivrance d'Elohîm

43:1	Fais-moi justice, Elohîm ! Et défends ma cause contre une nation infidèle<!--Ps. 26:1, 35:1.--> ! Délivre-moi de l'homme trompeur et injuste.
43:2	Car c'est toi, Elohîm, qui es ma protection. Pourquoi me repousses-tu ? Pourquoi marcherai-je dans la tristesse à cause de l'oppression de l'ennemi ?
43:3	Envoie ta lumière et ta vérité, afin qu'elles me conduisent et m'introduisent dans la montagne de ta sainteté, et dans tes demeures !
43:4	Alors je viendrai à l'autel d'Elohîm, au El de ma joie et mon allégresse, et je te célébrerai sur la harpe, Elohîm, mon Elohîm !
43:5	Mon âme, pourquoi t'abats-tu, et pourquoi murmures-tu au-dedans de moi ? Attends-toi à Elohîm, car je le célébrerai encore : il est le salut de ma face et mon Elohîm<!--Ps. 42:6.-->.

## Chapitre 44

### Prière des affligés

44:1	Cantique des fils de Koré. Au chef de musique.
44:2	Elohîm, nous avons entendu de nos oreilles, nos pères nous ont raconté l'œuvre que tu as faite de leur temps, aux jours d'autrefois<!--Jg. 6:13 ; Ps. 77:12.-->.
44:3	Tu as de ta main chassé des nations pour les établir, tu as brisé des peuples pour les étendre.
44:4	Car ce n'est pas par leur épée qu'ils ont conquis le pays, et ce n'est pas leur bras qui les a délivrés, mais c'est ta droite, c'est ton bras, c'est la lumière de ta face, parce que tu les aimais.
44:5	Elohîm, tu es mon Roi : ordonne la délivrance de Yaacov !
44:6	Avec toi nous battrons nos adversaires, par ton Nom nous foulerons ceux qui s'élèvent contre nous.
44:7	Car je ne me confie pas en mon arc, et ce n'est pas mon épée qui me délivrera.
44:8	Mais tu nous délivreras de nos adversaires, et tu rendras confus ceux qui nous haïssent.
44:9	Nous nous glorifierons en Elohîm chaque jour et nous célébrerons à jamais ton Nom. Sélah.
44:10	Mais tu nous rejettes, tu nous confonds, et tu ne sors plus avec nos armées.
44:11	Tu nous fais reculer devant l'adversaire, et ceux qui nous haïssent enlèvent nos dépouilles.
44:12	Tu nous livres comme des brebis destinées à être dévorées, et tu nous as dispersés parmi les nations.
44:13	Tu as vendu ton peuple pour rien, et tu ne l'estimes pas d'une grande valeur<!--Es. 52:3 ; Jé. 15:13.-->.
44:14	Tu fais de nous un objet d'opprobre chez nos voisins, de dérision et de raillerie auprès de ceux qui habitent autour de nous<!--Jé. 24:9 ; Ps. 79:4.-->.
44:15	Tu fais de nous le proverbe<!--Ou parabole.--> des nations, et de hochement de tête parmi les peuples.
44:16	Ma confusion est tout le jour devant moi, et la honte couvre ma face,
44:17	à cause des discours de celui qui nous fait des reproches et qui nous injurie, et à cause de l'ennemi et du vindicatif.
44:18	Tout cela nous est arrivé, et cependant nous ne t'avons pas oublié, et nous n'avons pas violé ton alliance.
44:19	Notre cœur ne s'est pas détourné, nos pas ne se sont pas éloignés de tes sentiers,
44:20	pour que tu nous écrases dans le territoire des serpents, et que tu nous couvres de l'ombre de la mort<!--Ps. 23:4.-->.
44:21	Si nous avions oublié le Nom de notre Elohîm et étendu nos mains vers un el étranger,
44:22	Elohîm ne le saurait-il pas, lui qui connaît les secrets du cœur ?
44:23	Mais nous sommes tous les jours mis à mort pour l'amour de toi, nous sommes regardés comme des brebis destinées à la boucherie<!--Es. 53:7.-->.
44:24	Lève-toi, pourquoi dors-tu Adonaï ? Réveille-toi ! Ne nous rejette pas à jamais !
44:25	Pourquoi caches-tu ta face ? Pourquoi oublies-tu notre affliction et notre oppression ?
44:26	Car notre âme est abattue dans la poussière et notre ventre est attaché à la terre.
44:27	Lève-toi, pour nous secourir ! Délivre-nous à cause de ta bonté !

## Chapitre 45

### La beauté du Roi

45:1	Cantique des fils de Koré. Chant du bien-aimé. Au chef de musique. Sur Shoshannim.
45:2	Une parole agréable bouillonne dans mon cœur. Et j'ai dit : Mon œuvre est pour le roi ! Ma langue sera comme la plume d'un scribe habile !
45:3	Tu es le plus beau des fils de l'homme, la grâce est répandue sur tes lèvres : c'est pourquoi Elohîm t'a béni éternellement.
45:4	Homme vaillant, ceins ton épée sur ta hanche, ta majesté et ta magnificence,
45:5	et prospère dans ta magnificence ! Sois porté sur la parole de vérité, de douceur, et de justice, et ta droite lancera des choses terribles !
45:6	Tes flèches sont aiguës, les peuples tomberont sous toi, elles perceront le cœur des ennemis du roi.
45:7	Ton trône Elohîm, subsiste d'éternité en éternité<!--Voir Hé. 1:8.--> ! Le sceptre de ton règne est un sceptre d'équité.
45:8	Tu aimes la justice et tu hais le crime envers la torah. C'est pourquoi, Elohîm, ton Elohîm t'a oint d'une huile d'allégresse plus que tes associés<!--Voir Hé. 1:8-9.-->.
45:9	Tous tes vêtements sont parfumés de myrrhe, d'aloès et de casse. Dans les palais d'ivoire les instruments à cordes te réjouissent.
45:10	Des filles de rois sont parmi tes bien-aimées ; la reine est à ta droite, parée d'or d'Ophir.
45:11	Écoute, jeune fille ! regarde et tends l'oreille : oublie ton peuple et la maison de ton père.
45:12	Le roi désire ta beauté ; puisqu'il est ton Seigneur, prosterne-toi devant lui.
45:13	La fille de Tyr et les plus riches des peuples te supplieront avec des présents.
45:14	La fille du roi est intérieurement pleine de gloire. Elle porte un vêtement tissé d'or.
45:15	Elle sera présentée au roi en vêtements de broderie, et les filles qui viennent après elle, et qui sont ses compagnes, seront amenées vers toi.
45:16	Elles te seront présentées avec réjouissance et allégresse, et elles entreront au palais du roi.
45:17	Tes fils seront à la place de tes pères, tu les établiras pour princes sur toute la Terre.
45:18	Je rendrai ton Nom mémorable dans tous les âges, et à cause de cela les peuples te célébreront pour toujours et à perpétuité<!--Ps. 67:3-5.-->.

## Chapitre 46

### L'assurance du peuple d'Elohîm

46:1	Cantique des fils de Koré. Au chef de musique. Sur Alamoth.
46:2	Elohîm est notre retraite, notre force, et notre secours qui ne manque jamais dans les détresses<!--Ps. 9:10.-->.
46:3	C'est pourquoi nous ne craindrons pas quand la Terre est bouleversée, et que les montagnes chancellent au cœur des mers<!--Es. 54:10.-->,
46:4	quand ses eaux mugissent, écument et se soulèvent jusqu'à faire trembler les montagnes. Sélah.
46:5	Le fleuve et ses ruisseaux réjouissent la cité d'Elohîm, qui est le lieu saint où demeure Elyon<!--Ez. 47:1-2 ; Za. 14:8-9 ; Jn. 7:38 ; Ap. 22:1-2.-->.
46:6	Elohîm est au milieu d'elle : Elle n'est pas ébranlée. Elohîm la secourt au tournant du matin<!--So. 3:16-17.-->.
46:7	Les nations murmurent, les royaumes s'ébranlent, il a fait entendre sa voix et la terre se fond.
46:8	YHWH Sabaoth est avec nous, l'Elohîm de Yaacov est pour nous une haute retraite. Sélah.
46:9	Venez, contemplez les œuvres de YHWH, et voyez quels ravages il a fait sur la Terre.
46:10	Il a fait cesser les guerres jusqu'au bout de la Terre, il a brisé l'arc et rompu la lance, il a consumé par le feu les chariots de guerre<!--Es. 2:4.-->.
46:11	Arrêtez, et sachez que je suis Elohîm ! Je suis élevé parmi les nations, je suis élevé sur toute la Terre !
46:12	YHWH Sabaoth est avec nous, l'Elohîm de Yaacov est pour nous une haute retraite. Sélah.

## Chapitre 47

### YHWH, l'Elohîm élevé

47:1	Psaume des fils de Koré. Au chef de musique.
47:2	Peuples battez tous des mains ! Poussez vers Elohîm des cris de joie avec une voix de triomphe !
47:3	car YHWH, Elyon, est redoutable. Il est le Grand Roi sur toute la Terre.
47:4	Il nous assujettit des peuples et des nations sous nos pieds.
47:5	Il nous choisit notre héritage, la gloire de Yaacov qu'il aime. Sélah.
47:6	Elohîm est monté avec un cri de joie, YHWH, avec le son du shofar.
47:7	Chantez à Elohîm, chantez ! Chantez à notre Roi, chantez !
47:8	Car Elohîm est le Roi de toute la Terre : chantez un cantique !
47:9	Elohîm règne sur les nations, Elohîm est assis sur son saint trône.
47:10	Les princes des peuples se rassemblent vers le peuple d'Elohîm d'Abraham, car les boucliers de la Terre sont à Elohîm : il est puissamment élevé.

## Chapitre 48

### Sion, splendeur du grand Roi

48:1	Cantique. Psaume des fils de Koré.
48:2	YHWH est grand, il est l'objet de toutes les louanges, dans la ville de notre Elohîm, sur la montagne de sa sainteté.
48:3	Belle est la colline, joie de toute la Terre, la montagne de Sion, le côté nord, c'est la ville du grand Roi !
48:4	Elohîm est connu dans ses palais pour une haute retraite.
48:5	Ils l'ont vue, et aussitôt ils ont été émerveillés ; ils ont été troublés et se sont enfuis à la hâte.
48:6	Ils ont regardé tout stupéfaits, ils ont eu peur et ont pris la fuite.
48:7	Là un tremblement les a saisis, une douleur comme celle de l'enfantement<!--Es. 13:8.-->.
48:8	Ils ont été chassés comme par le vent d'orient qui brise les navires de Tarsis.
48:9	Comme nous l'avions entendu, ainsi l'avons-nous vu dans la ville de YHWH Sabaoth, dans la ville de notre Elohîm : Elohîm l'établira à toujours. Sélah.
48:10	Elohîm ! nous pensons à ta bonté au milieu de ton temple.
48:11	Elohîm ! comme ton Nom, ta louange retentit jusqu'aux extrémités de la Terre, ta droite est pleine de justice.
48:12	La montagne de Sion se réjouit, et les filles de Yéhouda sont dans la joie, à cause de tes jugements.
48:13	Entourez Sion, faites-en le tour, comptez ses tours.
48:14	Fixez votre cœur sur son rempart, examinez ses palais pour le raconter à la génération future.
48:15	Car ce Elohîm-là est notre Elohîm éternellement et à jamais, il nous accompagnera jusqu'à la mort.

## Chapitre 49

### Vanité des richesses terrestres

49:1	Psaume des fils de Koré. Au chef de musique.
49:2	Vous tous peuples, entendez ceci, vous habitants du monde, prêtez l'oreille,
49:3	fils d'Adam et fils de l'homme, riches et pauvres ensemble !
49:4	Ma bouche prononcera des discours pleins de sagesse, et les pensées de mon cœur sont pleines de sens.
49:5	Je prêterai l'oreille à la parabole, j'expliquerai mes énigmes au son de la harpe.
49:6	Pourquoi craindrai-je au jour du malheur, quand l'iniquité de mes adversaires m'entoure ?
49:7	Ils mettent leur confiance dans leurs biens et se glorifient de l'abondance de leurs richesses.
49:8	Un homme ne peut racheter, racheter son frère ni donner à Elohîm sa rançon<!--Mt. 16:26 ; Mc. 8:36-37 ; Lu. 12:15-21.-->.
49:9	Car le rachat de leur âme est trop coûteux, et il ne se fera jamais.
49:10	Ils ne vivront pas toujours et n'éviteront pas la vue de la fosse.
49:11	Car on voit que les sages meurent, l'insensé et le stupide périssent également, et ils laissent à d'autres leurs biens<!--Ec. 2:21, 6:2.-->.
49:12	Leur pensée est que leurs maisons durent éternellement, et leurs habitations d'âge en âge, ils ont donné leurs noms à leurs terres.
49:13	Mais l'humain ne demeure pas dans ses honneurs, il est semblable aux bêtes qui périssent.
49:14	Tel est leur chemin, leur folie, et ceux qui les suivent se plaisent à leurs discours. Sélah.
49:15	Ils seront mis dans le shéol comme des brebis, la mort se nourrira d'eux, et au matin les hommes droits les fouleront aux pieds, leur beau rocher s'usera, le shéol sera leur résidence<!--Job 24:19.-->.
49:16	Mais Elohîm rachètera mon âme de la main du shéol, car il me prendra<!--Ps. 68:19 ; Ep. 4:8-9.-->. Sélah.
49:17	N'aie pas peur si tu vois un homme s'enrichir et quand les trésors de sa maison se multiplient.
49:18	Car lorsqu'il mourra, il n'emportera rien, ses trésors ne descendront pas après lui<!--Job 27:16-19 ; 1 Ti. 6:7.-->.
49:19	Il aura beau s'estimer heureux pendant sa vie, on aura beau te louer des jouissances que tu te donnes,
49:20	tu iras néanmoins au séjour de tes pères, qui jamais ne reverront la lumière.
49:21	L'humain qui est en honneur, qui n'a pas d'intelligence, est semblable aux bêtes qui périssent.

## Chapitre 50

### YHWH, le juste Juge

50:1	Psaume d'Asaph. El Elohîm, Elohîm, YHWH a parlé et il a appelé toute la Terre, depuis le soleil levant jusqu'au soleil couchant.
50:2	De Sion, Elohîm a fait luire sa splendeur qui est d'une beauté parfaite.
50:3	Notre Elohîm viendra, il ne se taira pas. Il y aura devant lui un feu dévorant, et tout autour de lui une grosse tempête.
50:4	Il appellera les cieux d'en haut, et la Terre pour juger son peuple, en disant :
50:5	Rassemblez-moi mes bien-aimés, qui ont traité alliance avec moi par le sacrifice<!--Mt. 24:29-31.--> !
50:6	Les cieux aussi annonceront sa justice parce qu'Elohîm est le juge. Sélah.
50:7	Écoute, mon peuple ! et je parlerai, Israël, et je t'avertirai, moi, Elohîm, ton Elohîm.
50:8	Je ne te réprimande pas pour tes sacrifices, tes holocaustes sont continuellement devant moi.
50:9	Je ne prendrai pas de taureaux de ta maison ni de boucs de tes enclos<!--Ps. 40:7.-->.
50:10	Car tous les animaux des forêts sont à moi, toutes les bêtes qui paissent sur 1 000 montagnes.
50:11	Je connais tous les oiseaux des montagnes, et tout ce qui se meut dans les champs m'appartient.
50:12	Si j'avais faim, je ne t'en dirais rien, car le monde est à moi et tout ce qu'il renferme<!--1 Co. 10:26.-->.
50:13	Mangerais-je la chair des gros taureaux ? Et boirais-je le sang des boucs ?
50:14	Offre à Elohîm le sacrifice d'actions de grâces et accomplis tes vœux envers Elyon !
50:15	Invoque-moi au jour de ta détresse, je te délivrerai, et tu me glorifieras<!--Ps. 37:5.-->.
50:16	Elohîm dit au méchant : Quoi donc ? Tu énumères mes lois et tu as mon alliance dans ta bouche !
50:17	Toi qui hais la correction, et qui jettes mes paroles derrière toi !
50:18	Si tu vois un voleur, tu te plais avec lui, et ta part est avec les adultères.
50:19	Tu livres ta bouche au mal, et ta langue est un tissu de tromperies.
50:20	Tu t'assieds et tu parles contre ton frère, tu couvres d'opprobre le fils de ta mère.
50:21	Tu as fait ces choses-là, et je me suis tu. Tu as estimé que j'étais vraiment comme toi, mais je vais te reprendre et tout mettre sous tes yeux.
50:22	Comprenez cela maintenant, vous qui oubliez Éloah, de peur que je ne déchire sans que personne ne vous délivre.
50:23	Celui qui offre pour sacrifice des actions de grâces me glorifie, et à celui qui veille sur sa voie, je lui ferai voir le salut d'Elohîm.

## Chapitre 51

### Le cœur repentant, sacrifice agréable à Elohîm

51:1	Psaume de David. Au chef de musique.
51:2	Lorsque Nathan, le prophète, vint à lui, après que David fut allé vers Bath-Shéba<!--2 S. 11 ; 2 S. 12.-->.
51:3	Elohîm ! Aie pitié de moi dans ta bonté ! Selon ta grande miséricorde, efface mes transgressions !
51:4	Lave-moi complètement de mon iniquité et purifie-moi de mon péché.
51:5	Car je connais mes transgressions, et mon péché est continuellement devant moi<!--Es. 59:12.-->.
51:6	J'ai péché contre toi, contre toi seul, et j'ai fait ce qui est mal à tes yeux, en sorte que tu seras juste dans ta sentence, sans reproche dans ton jugement.
51:7	Voici, je suis né dans l'iniquité, et ma mère m'a conçu dans le péché.
51:8	Mais tu prends plaisir à la vérité au fond du cœur, et tu me fais connaître la sagesse au-dedans de moi.
51:9	Purifie-moi de mon péché avec de l'hysope, et je serai pur ; lave-moi, et je serai plus blanc que la neige.
51:10	Fais-moi entendre la joie et l'allégresse, et les os que tu as brisés se réjouiront.
51:11	Détourne ta face de mes péchés, et efface toutes mes iniquités.
51:12	Elohîm ! Crée en moi un cœur pur, et renouvelle en moi un esprit ferme<!--Mt. 5:8.-->.
51:13	Ne me chasse pas loin de ta face, et ne m'enlève pas ton Esprit Saint.
51:14	Rends-moi la joie de ton salut et qu'un esprit bien disposé me soutienne.
51:15	J'enseignerai tes voies aux transgresseurs et les pécheurs reviendront à toi.
51:16	Elohîm, Elohîm de mon salut, délivre-moi du sang ! Ma langue chantera hautement ta justice.
51:17	Adonaï, ouvre mes lèvres, et ma bouche annoncera ta louange.
51:18	Car tu ne prends pas plaisir aux sacrifices, autrement je t'en donnerais. L'holocauste ne t'est pas agréable.
51:19	Les sacrifices à Elohîm, c'est un esprit brisé. Elohîm ! tu ne méprises pas un cœur brisé et contrit.
51:20	Répands par ta grâce tes bienfaits sur Sion, édifie les murs de Yeroushalaim !
51:21	Alors tu prendras plaisir aux sacrifices de justice, à l'holocauste, et aux sacrifices qui se consument entièrement par le feu. Alors on offrira des taureaux sur ton autel.

## Chapitre 52

### Le sort de l'homme qui se confie en ses richesses

52:1	Cantique de David. Au chef de musique.
52:2	À l'occasion du rapport que Doëg, l'Édomite, vint faire à Shaoul, en lui disant : David s'est rendu dans la maison d'Achimélec.
52:3	Pourquoi te vantes-tu du mal, vaillant homme ? La bonté de El dure à toujours.
52:4	Ta langue n'invente que des méchancetés, elle est aiguisée comme un rasoir, elle agit avec tromperie !
52:5	Tu aimes plus le mal que le bien, le mensonge plutôt que de dire la vérité. Sélah.
52:6	Tu aimes tous les discours pernicieux, le langage trompeur !
52:7	Aussi El te détruira pour toujours, il t'enlèvera et t'arrachera de ta tente, il te déracinera de la Terre des vivants. Sélah.
52:8	Les justes le verront, et auront de la crainte, et ils se riront d'un tel homme, disant :
52:9	Voilà cet homme qui n'a pas pris Elohîm pour sa protection, mais qui se confiait en ses grandes richesses et qui mettait sa force dans ses mauvais désirs<!--Es. 47:10 ; Lu. 12:15-21.-->.
52:10	Mais moi, je serai dans la maison d'Elohîm comme un olivier verdoyant. Je me confie dans la bonté d'Elohîm, pour toujours et à jamais.
52:11	Je te célébrerai à jamais, car tu agis. Je mettrai mon espérance en ton Nom, parce qu'il est bon envers tes fidèles.

## Chapitre 53

### Égarement des impies

53:1	Cantique de David. Au chef de musique. Sur la flûte.
53:2	L'insensé dit en son cœur : Il n'y a pas d'Elohîm ! Ils se sont corrompus, ils ont commis des injustices abominables ; il n'y a personne qui fasse ce qui est bon<!--Ps. 10:4 ; Ro. 1:20-21, 3:12.-->.
53:3	Elohîm a regardé des cieux les fils d'humains, pour voir s'il y a quelqu'un qui soit intelligent, qui cherche Elohîm.
53:4	Ils se sont tous détournés et se sont tous rendus odieux. Il n'y a personne qui fasse ce qui est bon, pas même un seul.
53:5	Ceux qui pratiquent la méchanceté n'ont-ils pas de connaissance ? Ils dévorent mon peuple comme s'ils dévoraient du pain. Ils n'invoquent pas Elohîm.
53:6	Ils trembleront de terreur là où il n'y a pas sujet de terreur, car Elohîm a dispersé les os de celui qui campe contre toi. Tu les confondras, car Elohîm les a rejetés.
53:7	Oh ! Qui fera partir de Sion le salut<!--Voir commentaire en Ge. 49:18.--> d'Israël ? Quand Elohîm aura ramené son peuple captif, Yaacov sera dans l'allégresse, Israël se réjouira.

## Chapitre 54

### La délivrance vient de YHWH

54:1	Cantique de David. Au chef de musique. Sur Negiynah.
54:2	Lorsque les Ziphiens vinrent dire à Shaoul : David n'est-il pas caché parmi nous<!--1 S. 23:19, 26:1.--> ?
54:3	Elohîm ! Délivre-moi par ton Nom et fais-moi justice par ta puissance.
54:4	Elohîm ! Écoute ma prière, prête l'oreille aux paroles de ma bouche !
54:5	Car des étrangers se sont levés contre moi, des gens impitoyables, qui ne mettent pas Elohîm devant eux, cherchent mon âme. Sélah.
54:6	Voici, Elohîm m'accorde son secours, Adonaï est avec ceux qui soutiennent mon âme.
54:7	Il fera retourner le mal sur mes ennemis. Détruis-les selon ta vérité !
54:8	Je t'offrirai de bon cœur des sacrifices. YHWH, je célébrerai ton Nom parce qu'il est bon !
54:9	Car il m'a délivré de toute détresse, et mes yeux voient mes ennemis.

## Chapitre 55

### Se garder des méchants

55:1	Cantique de David. Au chef de musique. Sur Negiynah.
55:2	Elohîm ! Prête l'oreille à ma prière, et ne te cache pas loin de mes supplications !
55:3	Écoute-moi, et réponds-moi ! J'erre çà et là dans ma méditation et je suis agité
55:4	à cause du bruit que fait l'ennemi, à cause de l'oppression du méchant, car ils font tomber sur moi les outrages, et ils me haïssent jusqu'à la fureur.
55:5	Mon cœur tremble au-dedans de moi, et les terreurs de la mort tombent sur moi.
55:6	La crainte et l'épouvante m'atteignent, et le frisson m'habille.
55:7	Je dis : Qui me donnera des ailes de colombe ? Je m'envolerais, et je trouverais ma demeure.
55:8	Voici, je m'enfuirais bien loin, et je me tiendrais dans le désert. Sélah.
55:9	Je m'échapperais en toute hâte, plus rapide que le vent impétueux, que la tempête.
55:10	Adonaï, réduis à néant, divise leurs langues ! Car j'ai vu la violence et les querelles dans la ville ;
55:11	elles font jour et nuit le tour sur les murailles ; l'iniquité et la malice sont dans son sein.
55:12	Les calamités sont au milieu d'elle, et la tromperie et la fraude ne partent pas de ses places.
55:13	Car ce n'est pas mon ennemi qui m'a diffamé, je le supporterais. Ce n'est pas celui qui me hait qui s'élève contre moi, je me cacherais de lui.
55:14	Mais c'est toi, homme ! que j'estimais mon égal, mon ami, toi que je connais bien<!--Ps. 41:10.--> !
55:15	Nous prenions plaisir à communiquer nos secrets ensemble, nous allions avec la multitude à la maison d'Elohîm.
55:16	Que la mort les séduise ! Qu'ils descendent vivants dans le shéol ! Car le mal est dans leur demeure, parmi eux dans leur assemblée.
55:17	Mais moi je crie à Elohîm, et YHWH me délivrera.
55:18	Le soir, le matin, et à midi je me plains et je gémis, et il entendra ma voix.
55:19	Il délivrera mon âme de la guerre et me rendra la paix, car ils sont nombreux contre moi.
55:20	El entendra et témoignera en ma faveur, lui qui de toute éternité est assis sur son trône. Sélah. Car il n'y a pas de changement en eux, et ils ne craignent pas Elohîm.
55:21	Chacun d'eux porte la main sur ceux qui vivaient en paix avec lui, et viole son alliance.
55:22	Les paroles de sa bouche sont plus douces que la crème, mais la guerre est dans son cœur ; ses paroles sont plus douces que l'huile, néanmoins elles sont tout autant d'épées nues.
55:23	Remets ton sort à YHWH, et il te soulagera. Il ne permettra jamais que le juste tombe.
55:24	Mais toi, Elohîm ! tu les feras descendre dans la fosse de la destruction. Les hommes sanguinaires et trompeurs ne parviendront pas à la moitié de leurs jours. C'est en toi que je me confie.

## Chapitre 56

### Se glorifier en la parole de YHWH

56:1	Mikhtam de David. Au chef de musique. Sur Yonathelem-Rehoqiym<!--Signifie « Colombe des térébinthes lointains ».-->. Lorsque les Philistins le saisirent à Gath<!--1 S. 21:10-14.-->.
56:2	Elohîm, aie pitié de moi ! Car des hommes m'écrasent et m'oppriment, me faisant tout le jour la guerre, ils m'oppressent.
56:3	Mes adversaires me piétinent tout le jour ! Car ils sont nombreux ceux qui me font la guerre là-haut.
56:4	Le jour où j'aurai peur, je me confierai en toi.
56:5	Je me glorifierai en Elohîm, en sa parole. Je me confie en Elohîm, je ne craindrai rien : que peuvent me faire les hommes<!--Ps. 118:6 ; Hé. 13:6.--> ?
56:6	Tous les jours ils déforment mes paroles, ils ne pensent qu'à me faire du mal.
56:7	Ils complotent, ils se tiennent cachés, ils observent mes pas, car ils cherchent ardemment mon âme.
56:8	C'est par la méchanceté qu'ils échapperaient ! Dans ta colère, Elohîm, fais tomber les peuples !
56:9	Tu comptes mes allées et venues, recueille mes larmes dans tes outres : ne sont-elles pas écrites dans ton livre ?
56:10	Le jour où je crierai à toi, mes ennemis retourneront en arrière ; je sais qu'Elohîm est pour moi.
56:11	Je me glorifierai en Elohîm, en sa parole, je me glorifierai en YHWH, en sa parole.
56:12	Je me confie en Elohîm, je ne craindrai rien : que peut me faire l'être humain ?
56:13	Elohîm ! J'accomplirai les vœux que je t'ai faits, je te rendrai des actions de grâces.
56:14	Car tu as délivré mon âme de la mort, et mes pieds de la chute, afin que je marche devant Elohîm, à la lumière des vivants.

## Chapitre 57

### Avoir confiance en Elohîm dans les difficultés

57:1	Mikhtam de David. Au chef de musique. Sur Al-Thasheth<!--Al-Thasheth signifie « ne détruis pas ». Voir Ps. 58:1, 59:1, 75:1.-->. Lorsqu'il s'enfuit de devant Shaoul dans la caverne<!--1 S. 22:1.-->.
57:2	Aie pitié de moi, Elohîm ! Aie pitié de moi ! Car mon âme cherche un refuge ; je cherche un refuge à l'ombre de tes ailes, jusqu'à ce que les calamités soient passées<!--Ps. 17:8.-->.
57:3	J’appelle Elohîm Elyon, le El qui accomplit tout pour moi.
57:4	Il m'enverra des cieux la délivrance, il rendra honteux celui qui veut me dévorer. Sélah. Elohîm enverra sa bonté et sa vérité.
57:5	Mon âme est au milieu des lions, je suis couché au milieu des gens qui sont enflammés, parmi les fils d'Adam dont les dents sont des lances et des flèches, et dont la langue est une épée aiguë<!--Ps. 59:8, 64:4 ; Ja. 3:5-12.-->.
57:6	Elohîm, élève-toi sur les cieux ! Que ta gloire soit sur toute la Terre !
57:7	Ils avaient tendu un filet sous mes pas : mon âme se courbait. Ils avaient creusé une fosse devant moi, mais ils y sont tombés. Sélah.
57:8	Mon cœur est affermi, Elohîm ! Mon cœur est affermi, je chanterai et je ferai retentir mes instruments.
57:9	Réveille-toi, ma gloire ! Réveillez-vous, mon luth et ma harpe ! Je me réveillerai à l'aube du jour.
57:10	Adonaï, je te célébrerai parmi les peuples, je te chanterai parmi les nations.
57:11	Car ta bonté est grande jusqu'aux cieux, et ta vérité jusqu'aux nuages<!--Ps. 118:4-5.-->.
57:12	Elohîm ! Élève-toi sur les cieux ! Que ta gloire soit sur toute la Terre !

## Chapitre 58

### YHWH rend justice sur la Terre

58:1	Mikhtam de David. Au chef de musique. Sur Al-Thasheth.
58:2	En effet, quand vous parlez, la justice est silencieuse. Vous, fils d'humains, jugez-vous avec droiture ?
58:3	Au contraire, vous pratiquez l’injustice dans votre cœur. Sur la Terre, c'est la violence de vos mains que vous pesez.
58:4	Les méchants se sont aliénés dès le sein maternel, ils se sont égarés dès le ventre, en parlant faussement.
58:5	Ils ont un venin semblable au venin du serpent, ils sont comme le cobra sourd, qui ferme son oreille,
58:6	qui n'entend pas la voix des enchanteurs, du charmeur le plus habile aux incantations.
58:7	Elohîm, brise-leur les dents dans leur bouche ! YHWH, brise les mâchoires des lionceaux !
58:8	Qu'ils s'écoulent comme de l'eau, et qu'ils se fondent ! Que chacun d'eux bande son arc, mais que ses flèches soient comme si elles étaient rompues !
58:9	Qu'ils s'en aillent comme un limaçon qui se fond ! Qu'ils ne voient pas le soleil comme l'avorton d'une femme !
58:10	Avant que vos chaudières aient senti le feu des épines, l'ardeur de la colère, semblable à un tourbillon, enlèvera chacun d'eux comme de la chair crue.
58:11	Le juste se réjouira quand il aura vu la vengeance, il lavera ses pieds avec le sang du méchant.
58:12	Et les humains diront : Oui, il est un fruit pour le juste, Oui, il y a un Elohîm qui juge sur la Terre.

## Chapitre 59

### Intervention divine

59:1	Mikhtam de David. Au chef de musique. Sur Al-Thasheth. Lorsque Shaoul envoya des gens qui épièrent sa maison, afin de le tuer<!--1 S. 19:11.-->.
59:2	Mon Elohîm ! Délivre-moi de mes ennemis, protège-moi de ceux qui s'élèvent contre moi !
59:3	Délivre-moi de ceux qui pratiquent la méchanceté et garde-moi des hommes sanguinaires !
59:4	Car voici, ils m'ont dressé des embûches, et des gens puissants complotent contre moi, bien qu'il n'y ait pas en moi de transgression ni de péché, YHWH !
59:5	Ils courent çà et là, et se mettent en ordre, bien qu'il n'y ait pas d'iniquité en moi. Réveille-toi pour venir au-devant de moi, et regarde !
59:6	Toi donc, YHWH ! Elohîm Sabaoth, Elohîm d'Israël, réveille-toi pour visiter toutes les nations ! Ne fais grâce à aucun de ceux qui me trahissent ! Sélah.
59:7	Ils reviennent chaque soir, ils hurlent comme des chiens, ils font le tour de la ville.
59:8	Voici, de leur bouche ils font jaillir le mal, il y a des épées sur leurs lèvres<!--Ja. 3:5-12.-->. Car, disent-ils : Qui nous entend ?
59:9	Mais toi, YHWH, tu te riras d'eux, tu te moqueras de toutes les nations<!--Ps. 2:4.-->.
59:10	Quelle que soit leur force, je m'attends à toi, car Elohîm est ma haute retraite.
59:11	Elohîm qui me favorise me préviendra, Elohîm me fera voir mes adversaires<!--Ps. 118:7.-->.
59:12	Ne les tue pas, de peur que mon peuple ne l'oublie, fais-les errer par ta puissance et abats-les, Adonaï, notre bouclier !
59:13	Leur bouche pèche à chaque parole de leurs lèvres, qu'ils soient pris par leur orgueil ! Ils ne tiennent que des discours de malédiction et de mensonge.
59:14	Consume-les avec fureur, consume-les de sorte qu'ils ne soient plus ! Qu'on sache qu'Elohîm domine sur Yaacov et jusqu'aux extrémités de la Terre ! Sélah.
59:15	Qu'ils reviennent le soir, et qu'ils hurlent comme des chiens, et qu'ils fassent le tour de la ville !
59:16	Qu'ils errent çà et là, cherchant leur nourriture, et qu'ils passent la nuit sans être rassasiés !
59:17	Mais moi je chanterai ta force, je célébrerai dès le matin ta bonté<!--Ps. 88:14.-->. Car tu es pour moi une haute retraite, et mon asile au jour de ma détresse.
59:18	Ma force ! je te chanterai, car Elohîm est ma haute retraite, l'Elohîm qui me favorise.

## Chapitre 60

### YHWH, le meilleur secours

60:1	Mikhtam de David, pour enseigner. Au chef de musique. Sur le lis lyrique.
60:2	Lorsqu'il fit la guerre contre les Syriens de Mésopotamie, et contre les Syriens de Tsoba, et que Yoab revint et défit 12 000 Édomites dans la vallée du sel<!--2 S. 8:3-13 ; 1 Ch. 18:3-12.-->.
60:3	Elohîm ! tu nous as rejetés, tu nous as dispersés, tu t'es irrité : reviens vers nous !
60:4	Tu as ébranlé la Terre et l'as mise en pièces : répare ses brèches, car elle chancelle !
60:5	Tu as fait voir à ton peuple des choses dures, tu nous as abreuvés d'un vin d'étourdissement<!--Es. 51:17-21 ; Ap. 14:10.-->.
60:6	Tu as donné une bannière à ceux qui te craignent afin de l'élever bien haut en face de la vérité<!--Ou « en face de l'arc ».-->. Sélah.
60:7	Afin que ceux que tu aimes soient délivrés. Sauve-moi par ta droite et exauce-moi<!--Ps. 108:6.--> !
60:8	Elohîm a parlé dans son lieu saint : Je me réjouirai, je partagerai Sichem, je mesurerai la vallée de Soukkoth.
60:9	Galaad est à moi, Menashè aussi est à moi. Éphraïm est la protection de ma tête, et Yéhouda, mon sceptre.
60:10	Moab est le bassin où je me lave, je jette ma sandale sur Édom. Pays des Philistins, pousse des cris de guerre à mon sujet<!--2 S. 8:2 ; 1 Ch. 18:2.--> !
60:11	Qui me conduira dans la ville forte ? Qui me conduira jusqu'en Édom ?
60:12	Ne sera-ce pas toi, Elohîm, qui nous avais rejetés, et qui ne sortais plus, Elohîm, avec nos armées ?
60:13	Donne-nous du secours contre la détresse ! Car le salut de l'humain est vain<!--Jé. 17:5 ; Ps. 108:13, 118:8.-->.
60:14	Avec Elohîm, nous agirons avec force, et c'est lui qui foule aux pieds nos ennemis<!--Ps. 108:14.-->.

## Chapitre 61

### Elohîm, le parfait refuge

61:1	Psaume de David. Au chef de musique. Sur Negiynah.
61:2	Elohîm, je crie à toi, sois attentif à ma prière !
61:3	Je crie à toi du bout de la Terre, le cœur abattu ; conduis-moi sur le rocher qui est trop haut pour moi !
61:4	Car tu es mon refuge<!--Voir Ps. 32:7, 91:1.-->, une tour forte au-devant de l'ennemi.
61:5	Je séjournerai éternellement dans ta tente, je me réfugierai sous la couverture<!--Voir Ps. 32:7, 91:1, 119:114.--> de tes ailes. Sélah.
61:6	Car, Elohîm ! tu exauces mes vœux, et tu me donnes l'héritage de ceux qui craignent ton Nom.
61:7	Tu ajoutes des jours aux jours du roi, que ses années se prolongent à jamais !
61:8	Qu'il demeure toujours dans la présence d'Elohîm ! Que la bonté et la vérité le gardent !
61:9	Ainsi je chanterai ton Nom à perpétuité, en accomplissant mes vœux chaque jour.

## Chapitre 62

### La confiance en Elohîm

62:1	Psaume de David. Au chef de musique. D'après Yedoutoun.
62:2	Quoiqu'il en soit, mon âme se repose en Elohîm. C'est de lui que vient ma délivrance.
62:3	Quoiqu'il en soit, il est mon rocher, ma délivrance, et ma haute retraite, je ne serai pas entièrement ébranlé.
62:4	Jusqu'à quand accablerez-vous de maux un homme ? Vous serez tous mis à mort, et vous serez comme le mur qui penche, comme une cloison qui a été ébranlée.
62:5	Ils ne font que consulter pour le faire déchoir de son élévation. Ils prennent plaisir au mensonge, ils bénissent de leur bouche, mais au-dedans ils maudissent. Sélah.
62:6	Mais toi mon âme, demeure tranquille, regarde à Elohîm, car mon espérance est en lui.
62:7	Quoiqu'il en soit, il est mon rocher, ma délivrance, et ma haute retraite, je ne serai pas ébranlé.
62:8	En Elohîm se trouvent mon salut et ma gloire, en Elohîm sont le rocher de ma force et mon refuge.
62:9	Peuples, confiez-vous en lui en tout temps, déchargez votre cœur sur lui ! Elohîm est notre retraite. Sélah.
62:10	Oui, vanité, les fils de l'homme ! Mensonge, les fils de l'homme ! Dans une balance, ils monteraient tous ensemble, plus légers qu'un souffle.
62:11	Ne mettez pas votre confiance dans l'oppression et ne placez pas un vain espoir dans le vol. Quand les richesses s'accroissent, n'y attachez pas votre cœur.
62:12	Elohîm a parlé une fois, j'ai entendu cela deux fois : c'est que la force est à Elohîm.
62:13	Et c'est à toi, Adonaï, qu'appartient la bonté. Certainement tu rendras à chacun selon son œuvre<!--Jé. 32:19 ; Pr. 24:12 ; Job 34:11 ; Mt. 16:27 ; Ro. 2:6.-->.

## Chapitre 63

### Soif de la présence d'Elohîm

63:1	Psaume de David, lorsqu'il était dans le désert de Yéhouda<!--1 S. 22:5, 23:14-15.-->.
63:2	Elohîm ! tu es mon El, je te cherche au point du jour. Mon âme a soif de toi, ma chair soupire après toi sur cette terre aride, desséchée, et sans eau<!--Ps. 42:2, 84:3, 143:6.-->.
63:3	Ainsi je te contemple dans ton lieu saint pour voir ta force et ta gloire.
63:4	Car ta bonté vaut mieux que la vie, mes lèvres te louent.
63:5	Ainsi, je te bénirai toute ma vie<!--Ps. 104:33.-->, j'élèverai mes mains en ton Nom.
63:6	Mon âme sera rassasiée comme de graisse et de moelle, et ma bouche te louera avec un chant de réjouissance.
63:7	Quand je me souviens de toi sur mon lit, je médite sur toi durant les veilles de la nuit<!--Ps. 16:7, 119:55.-->.
63:8	Car tu m'as secouru, je me réjouirai à l'ombre de tes ailes.
63:9	Mon âme s'est attachée à toi pour te suivre, ta droite me soutient.
63:10	Mais ceux qui cherchent à ruiner mon âme iront dans les parties inférieures de la Terre.
63:11	Ils seront livrés au pouvoir de l'épée, ils seront la portion des renards.
63:12	Mais le roi se réjouira en Elohîm. Quiconque jure par lui s'en glorifiera, car la bouche de ceux qui mentent sera fermée<!--Ps. 107:42 ; Job 5:16.-->.

## Chapitre 64

### YHWH, le seul abri

64:1	Psaume de David. Au chef de musique.
64:2	Elohîm ! écoute ma voix quand je m'écrie ! Protège ma vie contre l'ennemi que je crains !
64:3	Cache-moi des complots des méchants, de l'assemblée tumultueuse de ceux qui pratiquent la méchanceté !
64:4	Ils aiguisent leur langue comme une épée<!--Jé. 9:3 ; Ps. 11:2, 59:8.-->, ils tirent comme des flèches leurs paroles amères,
64:5	afin de tirer sur l'innocent dans sa cachette. Ils tirent soudainement sur lui et n'ont aucune crainte.
64:6	Ils se fortifient dans des paroles mauvaises, ils ne parlent que de cacher des pièges et disent : Qui les verra<!--Job 24:15.--> ?
64:7	Ils combinent des crimes : ils achèvent les complots qu'ils combinent. La pensée intérieure, le cœur de l'homme est insondable.
64:8	Mais Elohîm lance contre eux ses traits, soudain les voilà frappés.
64:9	Leur langue a causé leur chute, tous ceux qui les voient secouent leur tête.
64:10	Et tous les humains craindront et raconteront l'œuvre d'Elohîm, et considéreront ce qu'il aura fait.
64:11	Le juste se réjouira en YHWH, et se retirera vers lui, et tous ceux qui sont droits de cœur s'en glorifieront<!--Ps. 63:12, 97:12.-->.

## Chapitre 65

### Le règne de YHWH sur la nature

65:1	Psaume de David. Cantique. Au chef de musique.
65:2	Elohîm ! dans le calme, on te louera dans Sion, et l'on accomplira nos vœux<!--Ps. 50:14, 66:13.-->.
65:3	Tu entends nos prières. Toute chair viendra jusqu'à toi.
65:4	Les actions d'iniquités prévalent sur moi, mais tu feras la propitiation de nos transgressions.
65:5	Béni est celui que tu choisis et que tu fais approcher de toi pour qu'il habite dans tes parvis ! Nous serons rassasiés des biens de ta maison, des biens du saint lieu de ton temple.
65:6	Dans ta justice, tu nous réponds par des choses terribles, Elohîm de notre salut, espoir de toutes les extrémités lointaines de la Terre et de la mer.
65:7	Il affermit les montagnes par sa force, il est ceint de puissance.
65:8	Il apaise le mugissement de la mer, le mugissement de leurs flots, et le tumulte des peuples.
65:9	Ceux qui habitent aux extrémités de la Terre ont peur de tes prodiges. Tu réjouis l'orient et l'occident.
65:10	Tu visites la Terre et tu lui donnes l'abondance, tu la combles de richesses. Le ruisseau d'Elohîm est plein d'eau, tu prépares le blé. Ainsi tu la prépares.
65:11	Tu arroses ses sillons, et tu aplanis ses mottes. Tu l'amollis par de grosses averses, et tu bénis son germe<!--Es. 55:10 ; Ps. 104:13-14.-->.
65:12	Tu couronnes l'année de tes biens, et tes voies versent l'abondance.
65:13	Les plaines du désert sont abreuvées, et les collines sont ceintes de joie.
65:14	Les pâturages se couvrent de brebis, et les vallées se revêtent de froment. Les cris de joie et les chants retentissent.

## Chapitre 66

### Louange à Elohîm de grâces

66:1	Cantique. Psaume. Au chef de musique. Vous tous habitants de toute la Terre, poussez des cris de triomphe à Elohîm,
66:2	chantez la gloire de son Nom, faites éclater sa gloire par vos louanges !
66:3	Dites à Elohîm : Que tes œuvres sont redoutables ! Tes ennemis te mentiront à cause de la grandeur de ta force.
66:4	Toute la Terre se prosterne devant toi et te chante, elle chante ton Nom. Sélah.
66:5	Venez et voyez les œuvres d'Elohîm ! Il est redoutable quand il agit sur les fils d'humains.
66:6	Il a fait de la mer une terre sèche, on a passé le fleuve à pied sec. Là, nous nous sommes réjouis en lui<!--Ex. 14:21 ; Jos. 3:14-17.-->.
66:7	Il domine par sa puissance éternellement, ses yeux prennent garde sur les nations<!--Ps. 14:2, 33:13 ; Job 28:24.-->. Que les rebelles ne s'élèvent pas ! Sélah.
66:8	Peuples, bénissez notre Elohîm, et faites retentir le son de sa louange.
66:9	C'est lui qui a remis notre âme en vie, et qui n'a pas permis que nos pieds chancellent.
66:10	Car tu nous as éprouvés, Elohîm, tu nous fonds comme l'argent se fond.
66:11	Tu nous as amenés dans le filet, tu as mis sur nos reins un pesant fardeau.
66:12	Tu as fait monter des hommes sur notre tête, et nous sommes entrés dans le feu et dans l'eau. Mais tu nous as fait sortir dans l'abondance.
66:13	J'entrerai dans ta maison avec des holocaustes, j'accomplirai mes vœux envers toi<!--Ps. 22:26, 76:12, 116:14.-->.
66:14	Pour eux, mes lèvres se sont ouvertes, et ma bouche les a prononcés dans ma détresse.
66:15	Je t'offrirai en holocauste des brebis grasses, avec l'encens des béliers, je te sacrifierai des taureaux et des boucs. Sélah.
66:16	Vous tous qui craignez Elohîm, venez, écoutez, et je raconterai ce qu'il a fait pour mon âme.
66:17	Je l'ai invoqué de ma bouche, et ma langue a célébré sa louange.
66:18	Si j'avais conçu l'iniquité dans mon cœur, Adonaï ne m'aurait pas écouté<!--Jn. 9:31.-->.
66:19	Mais certainement Elohîm m'a écouté, il a été attentif à la voix de ma prière.
66:20	Béni soit Elohîm, qui n'a pas rejeté ma prière, et qui n'a pas éloigné de moi sa bonté.

## Chapitre 67

### Louange des peuples

67:1	Psaume. Cantique. Au chef de musique. Sur Negiynah.
67:2	Qu'Elohîm ait pitié de nous et qu'il nous bénisse, qu'il fasse luire sa face sur nous<!--No. 6:25 ; Ps. 4:7, 31:17, 119:135.-->. Sélah.
67:3	Afin que ta voie soit connue sur la Terre et ta délivrance parmi toutes les nations.
67:4	Les peuples te célébreront, Elohîm ! Tous les peuples te célébreront<!--Ps. 22:27, 68:33.--> !
67:5	Les peuples se réjouiront et chanteront de joie, car tu juges les peuples avec droiture et tu conduis les nations sur la Terre<!--Ps. 96:10.-->. Sélah.
67:6	Les peuples te célébreront, Elohîm ! Tous les peuples te célébreront !
67:7	La Terre produira son fruit. Elohîm, notre Elohîm, nous bénira.
67:8	Elohîm nous bénira, et toutes les extrémités de la terre le craindront.

## Chapitre 68

### YHWH, l'Elohîm glorieux

68:1	Psaume. Cantique de David. Au chef de musique.
68:2	Qu'Elohîm se lève, et ses ennemis seront dispersés, et ceux qui le haïssent s'enfuiront devant lui<!--No. 10:35.-->.
68:3	Tu les chasseras comme la fumée est chassée par le vent, comme la cire se fond devant le feu ! Ainsi les méchants périront devant Elohîm<!--Ps. 37:20, 97:5.-->.
68:4	Mais les justes se réjouiront et s'égayeront devant Elohîm, et tressailliront de joie<!--Ps. 67:4-5.-->.
68:5	Chantez à Elohîm, célébrez son Nom ! Exaltez celui qui est monté à cheval par les régions arides ! Son Nom est Yah ! Réjouissez-vous dans sa présence !
68:6	Il est le père des orphelins et le juge des veuves. Elohîm est dans sa demeure sainte<!--Ps. 146:9.-->.
68:7	Elohîm donne une famille à ceux qui étaient abandonnés, il fait sortir les captifs pour leur prospérité, mais les rebelles habitent sur une terre déserte.
68:8	Elohîm, tu es sorti devant ton peuple, tu as marché dans le désert ! Sélah.
68:9	La Terre a tremblé et les cieux ont distillé leurs eaux devant Elohîm, le mont Sinaï a tremblé devant Elohîm, l'Elohîm d'Israël<!--Ex. 19:18 ; Jg. 5:5.-->.
68:10	Elohîm ! tu as fait tomber une pluie abondante sur ton héritage, et quand il était épuisé, tu l'as rétabli.
68:11	Ton troupeau a habité dans le pays que dans ta bonté, Elohîm, tu avais préparé pour les malheureux.
68:12	Adonaï donne une parole, et les messagères de bonnes nouvelles sont une grande armée.
68:13	Les rois des armées se sont enfuis, ils se sont enfuis, et celle qui se tenait à la maison a partagé le butin<!--1 S. 30:16.-->.
68:14	Tandis que vous vous couchez dans les étables, les ailes de la colombe sont couvertes d'argent, et son plumage est d'un jaune d'or.
68:15	Quand Shaddaï dispersa les rois dans le pays, il devint blanc comme la neige du Tsalmon.
68:16	La montagne d'Elohîm est une montagne de Bashân<!--« Fertile », « large ». District à l'est du Yarden (Jourdain) connu pour sa fertilité et attribué à la demi-tribu de Menashè (Manassé).-->, une montagne élevée, une montagne de Bashân.
68:17	Pourquoi l'insultez-vous, montagnes dont le sommet est élevé ? Elohîm a désiré cette montagne pour y habiter, et YHWH y demeurera à jamais.
68:18	Les chars d'Elohîm se comptent par 20 000, par milliers et par milliers. Adonaï est au milieu d'eux, le Sinaï est dans le sanctuaire.
68:19	Tu es monté dans la hauteur, tu as emmené des captifs, tu as pris des dons pour les distribuer parmi les humains, et même parmi les rebelles, afin qu'ils habitent dans le lieu de Yah Elohîm<!--Ep. 4:8-10. Cette prophétie concerne la résurrection du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ).-->.
68:20	Béni soit Adonaï, qui tous les jours nous comble de ses biens ! El est notre délivrance. Sélah.
68:21	El est pour nous le El de délivrance ! C'est à YHWH Adonaï de faire sortir de la mort.
68:22	En effet, Elohîm brisera la tête de ses ennemis<!--Ge. 3:15.-->, le sommet de la tête chevelue de celui qui vit dans sa culpabilité.
68:23	Adonaï dit : Je les ramènerai de Bashân<!--No. 21:33-35.-->, je les ramènerai du fond de la mer,
68:24	afin que tu plonges ton pied dans le sang<!--Ps. 58:11.-->, et que la langue de tes chiens ait sa part de tes ennemis.
68:25	Ils voient ta marche, Elohîm ! Ils ont vu ta marche dans le lieu saint, la marche de mon El, mon Roi.
68:26	Les chanteurs allaient devant, ensuite les joueurs d'instruments, et au milieu les jeunes filles jouant du tambour<!--Ex. 15:20 ; 1 S. 18:6.-->.
68:27	Bénissez Elohîm dans les assemblées, bénissez Adonaï, vous qui êtes descendants d'Israël !
68:28	Là sont Benyamin, le plus jeune, qui domine sur eux, les chefs de Yéhouda et leur multitude, les chefs de Zebouloun, et les chefs de Nephthali.
68:29	Ton Elohîm ordonne que tu sois puissant. Affermis, Elohîm, ce que tu as fait pour nous.
68:30	Dans ton temple, à Yeroushalaim, les rois t'amèneront des présents<!--1 R. 10:10 ; Ps. 72:10 ; 2 Ch. 32:23.-->.
68:31	Épouvante les bêtes sauvages des roseaux, la troupe des taureaux, et les veaux des peuples, et ceux qui se prosternent avec des pièces d'argent ! Disperse les peuples qui prennent plaisir à la guerre !
68:32	Des princes viendront de l'Égypte ; l'Éthiopie se hâtera d'étendre ses mains vers Elohîm.
68:33	Royaumes de la Terre, chantez à Elohîm, célébrez Adonaï ! Sélah.
68:34	Chantez à celui qui est monté dans les cieux des cieux, les cieux éternels ! Voici, il fait retentir de sa voix un son puissant.
68:35	Attribuez la force à Elohîm ! Sa majesté est sur Israël, et sa force est dans les nuées.
68:36	Elohîm, tu es redouté à cause de ton sanctuaire. Le El d'Israël est celui qui donne la force et la puissance à son peuple. Béni soit Elohîm !

## Chapitre 69

### Elohîm, attentif à la prière de ceux qui s'humilient

69:1	Psaume de David. Au chef de musique. Sur les lis.
69:2	Délivre-moi, Elohîm ! car les eaux menacent ma vie<!--Ps. 124:4, 144:7.-->.
69:3	Je suis enfoncé dans un bourbier profond, sans appui. Je suis entré au plus profond des eaux, et les courants d'eau me submergent.
69:4	Je suis las de crier, mon gosier se dessèche, mes yeux se consument pendant que je m'attends à Elohîm.
69:5	Ceux qui me haïssent sans cause<!--Jn. 15:25.-->, dépassent en nombre les cheveux de ma tête. Ils sont puissants, ceux qui veulent me détruire, qui sont à tort mes ennemis. Je dois rendre ce que je n'avais pas ravi.
69:6	Elohîm ! tu connais ma folie, et mes délits ne te sont pas cachés.
69:7	Adonaï YHWH Sabaoth ! que ceux qui se confient en toi ne soient pas honteux à cause de moi ! Et que ceux qui te cherchent ne soient pas humiliés à cause de moi, Elohîm d'Israël !
69:8	Car c'est pour toi que je porte l'opprobre, que la honte couvre mon visage.
69:9	Je suis devenu un étranger pour mes frères, et un homme de dehors pour les fils de ma mère<!--Ge. 31:14-15 ; Jn. 7:3-5.-->.
69:10	Car le zèle de ta maison me dévore<!--Jn. 2:17 ; Ro. 15:3.-->, et les outrages de ceux qui t'insultaient sont tombés sur moi.
69:11	Je pleure et je jeûne, c'est ce qui m'attire l'opprobre.
69:12	Je prends un sac pour vêtement, et je suis devenu le sujet de leurs proverbes.
69:13	Ceux qui sont assis à la porte parlent de moi, et je suis la chanson<!--Job 30:9 ; La. 3:14.--> des buveurs de boissons fortes.
69:14	Mais je t'adresse ma prière, YHWH<!--Ps. 102:2.--> ! Que ce soit le temps favorable, Elohîm ! Par ta grande bonté, réponds-moi en m'assurant ta délivrance.
69:15	Délivre-moi de la boue, que je ne m'y enfonce pas<!--Ps. 40:3.-->, que je sois délivré de ceux qui me haïssent et des eaux profondes.
69:16	Que les courants d'eau ne me submergent plus, que les profondeurs ne m'engloutissent pas, et que le puits ne ferme pas sa bouche sur moi !
69:17	YHWH, exauce-moi, car ta bonté est agréable ! Dans tes grandes compassions, tourne ta face vers moi !
69:18	Ne cache pas ta face à ton serviteur, car je suis en détresse. Hâte-toi, exauce-moi !
69:19	Approche-toi de mon âme, rachète-la ! Délivre-moi à cause de mes ennemis !
69:20	Tu connais toi-même mon opprobre, ma honte et mon ignominie. Tous mes ennemis sont devant toi.
69:21	L'opprobre m'a brisé le cœur, et je suis languissant ; j'ai attendu que quelqu'un ait compassion de moi, mais il n'y en a pas eu. J'ai attendu des consolateurs, mais je n'en ai pas trouvé.
69:22	Ils m'ont au contraire donné du fiel<!--Mt. 27:34,48.--> pour mon repas ; et dans ma soif, ils m'ont abreuvé de vinaigre.
69:23	Que leur table soit pour eux un piège et un appât au sein de leur perfection !
69:24	Que leurs yeux soient tellement obscurcis, qu'ils ne puissent pas voir ; et fais continuellement chanceler leurs reins !
69:25	Répands ton indignation sur eux, et que l'ardeur de ta colère les saisisse !
69:26	Que leur campement soit désolé, et qu'il n'y ait personne qui habite dans leurs tentes !
69:27	Car ils persécutent celui que tu avais frappé, et racontent les souffrances de ceux que tu blesses.
69:28	Mets des iniquités à leurs iniquités, qu'ils n'entrent pas dans ta justice !
69:29	Qu'ils soient effacés du livre de vie, et qu'ils ne soient pas inscrits avec les justes !
69:30	Mais pour moi, qui suis affligé et dans la douleur, ta délivrance, Elohîm, m'élèvera en une haute retraite.
69:31	Je louerai le Nom d'Elohîm par des cantiques et je le glorifierai par des actions de grâces.
69:32	Cela est agréable à YHWH, plus qu'un taureau avec des cornes et des sabots fendus.
69:33	Les humbles le voient et ils se réjouissent, que votre cœur vive, vous qui cherchez Elohîm !
69:34	Car YHWH exauce les misérables et ne méprise pas ses prisonniers.
69:35	Que les cieux et la Terre le louent, la mer et tout ce qui y rampe<!--Ps. 96:11.--> !
69:36	Car Elohîm délivrera Sion, et bâtira les villes de Yéhouda. On y habitera et on la possèdera.
69:37	Et la postérité de ses serviteurs en fera son héritage, et ceux qui aiment son Nom y auront leur demeure.

## Chapitre 70

### Le pauvre et l'indigent

70:1	Psaume de David, pour souvenir. Au chef de musique.
70:2	Elohîm, hâte-toi de me délivrer ! YHWH, hâte-toi de venir à mon secours<!--Ps. 40:14, 71:12.--> !
70:3	Que ceux qui cherchent mon âme soient honteux et rougissent<!--Ps. 35:4, 71:13.--> ! Et que ceux qui prennent plaisir à mon mal soient repoussés en arrière et soient confus !
70:4	Que ceux qui disent : Ah ! Ah ! retournent en arrière par l'effet de leur honte.
70:5	Que tous ceux qui te cherchent, exultent et se réjouissent en toi ! Et que ceux qui aiment ta délivrance disent toujours : Glorifié soit Elohîm !
70:6	Moi, je suis affligé et misérable. Elohîm ! hâte-toi de venir vers moi ! Tu es mon secours et mon libérateur. YHWH ! ne tarde pas !

## Chapitre 71

### Demeurer en Elohîm jusqu'au bout

71:1	YHWH, je cherche en toi mon refuge : que je ne sois jamais confus !
71:2	Délivre-moi par ta justice et sauve-moi ! Incline ton oreille vers moi, mets-moi en sûreté !
71:3	Sois pour moi le rocher de mon refuge, afin que je puisse toujours m'y retirer ! Tu as donné l'ordre de me mettre en sûreté, car tu es mon rocher et ma forteresse.
71:4	Mon Elohîm, délivre-moi de la main du méchant, de la main du pervers et de l'oppresseur !
71:5	Car tu es mon espérance, Adonaï YHWH ! ma confiance dès ma jeunesse.
71:6	Je m'appuie sur toi dès le ventre de ma mère : c'est toi qui m'as fait sortir des entrailles de ma mère<!--Ps. 22:10-11.-->. Tu es le sujet continuel de mes louanges.
71:7	Je suis pour beaucoup comme un miracle, mais tu es mon puissant refuge.
71:8	Que ma bouche soit remplie de ta louange et de ta gloire chaque jour !
71:9	Ne me rejette pas au temps de ma vieillesse ! Ne m'abandonne pas maintenant que ma force est consumée !
71:10	Car mes ennemis ont parlé de moi, et ceux qui épient mon âme ont pris conseil ensemble,
71:11	disant : Elohîm l'a abandonné. Poursuivez-le et saisissez-le, car il n'y a personne qui le délivre.
71:12	Elohîm, ne t'éloigne pas de moi ! Mon Elohîm, hâte-toi de venir à mon secours !
71:13	Que ceux qui sont les ennemis de mon âme soient honteux et défaits ! Et que ceux qui cherchent mon malheur soient enveloppés d'opprobre et de honte !
71:14	Mais moi, j'espérerai toujours et je te louerai tous les jours davantage.
71:15	Ma bouche racontera chaque jour ta justice et ta délivrance, bien que je n'en sache pas le nombre.
71:16	Je marcherai par la force d'Adonaï YHWH, je raconterai ta justice, la tienne seule.
71:17	Elohîm ! tu m'as enseigné dès ma jeunesse, et j'ai annoncé jusqu'à présent tes merveilles.
71:18	Elohîm ! ne m'abandonne pas, même dans la blanche vieillesse, afin que j'annonce ta force à la génération présente, ta puissance à la génération à venir.
71:19	Car ta justice, Elohîm, est haut élevée, car tu as fait de grandes choses ! Elohîm, qui est semblable à toi ?
71:20	Tu m'as fait éprouver bien des détresses et des malheurs, mais tu me redonneras la vie et tu me feras monter hors des abîmes de la Terre.
71:21	Relève ma grandeur et console-moi encore !
71:22	Je te louerai au son du luth, je chanterai ta fidélité, mon Elohîm, je te célèbrerai avec la harpe, Saint d'Israël !
71:23	Mes lèvres et mon âme, que tu as rachetée, pousseront des cris de joie quand je te chanterai ;
71:24	ma langue aussi publiera chaque jour ta justice, car ceux qui cherchent mon malheur seront honteux et rougiront.

## Chapitre 72

### Le Royaume messianique

72:1	De Shelomoh. Elohîm, donne tes jugements au roi et ta justice au fils du roi.
72:2	Qu'il juge avec justice ton peuple, et tes malheureux avec équité !
72:3	Que les montagnes portent la paix pour le peuple, et que les collines la portent en justice !
72:4	Qu'il fasse droit aux malheureux du peuple, qu'il délivre les fils du misérable, et qu'il écrase l'oppresseur !
72:5	Ils te craindront tant que le soleil et la lune dureront, d'âge en âge.
72:6	Il descendra comme la pluie sur l'herbe fauchée, comme les grosses averses qui arrosent la Terre.
72:7	En son temps, le juste fleurira, et il y aura abondance de paix jusqu'à ce qu'il n'y ait plus de lune.
72:8	Il dominera de la mer à la mer, et du fleuve jusqu'aux extrémités de la Terre.
72:9	Les habitants des déserts se courberont devant lui, et ses ennemis lécheront la poussière.
72:10	Les rois de Tarsis et des îles lui rapporteront des dons, les rois de Saba et de Séba lui apporteront des présents.
72:11	Tous les rois aussi se prosterneront devant lui, toutes les nations le serviront.
72:12	Car il délivrera le pauvre qui crie au secours vers lui, l'affligé et celui qui n'a personne pour l'aider<!--Ps. 34:18 ; Job 29:12.-->.
72:13	Il aura compassion du pauvre et du misérable, et il sauvera les âmes des misérables,
72:14	il garantira leur âme de la fraude et de la violence, et leur sang sera précieux devant ses yeux.
72:15	Il vivra donc, et on lui donnera de l'or de Séba, et on fera des prières pour lui continuellement. On le bénira chaque jour.
72:16	Les blés abonderont dans le pays, au sommet des montagnes, et leurs épis s'agiteront comme les arbres du Liban. Les hommes fleuriront dans les villes comme l'herbe de la terre.
72:17	Son Nom est éternel, son Nom se propagera devant le soleil, et on se bénira en lui ! Toutes les nations le diront béni.
72:18	Béni soit YHWH Elohîm, l'Elohîm d'Israël, qui seul fait des choses merveilleuses !
72:19	Béni soit éternellement son Nom glorieux ! Et que toute la Terre soit remplie de sa gloire ! Amen ! Oui, amen !
72:20	Fin des prières de David, fils d'Isaï.

## Chapitre 73

### L'orgueil des méchants

73:1	Psaume d'Asaph. Quoi qu'il en soit, Elohîm est bon pour Israël, pour ceux qui ont le cœur pur<!--Mt. 5:8.-->.
73:2	Et quant à moi, pour un peu mes pieds allaient se détourner, il s'en est fallu d'un rien que mes pas ne glissent,
73:3	car j'étais envieux des vantards en voyant la prospérité des méchants.
73:4	Parce que rien ne les tourmente jusqu'à leur mort, et leur corps est gras,
73:5	ils n'ont pas de part aux peines des humains, et ils ne sont pas frappés comme les humains.
73:6	C'est pourquoi l'orgueil leur sert de collier, et la violence les couvre comme un vêtement.
73:7	Les yeux leur sortent dehors à force de graisse, ils dépassent les imaginations du cœur.
73:8	Ils sont moqueurs et parlent méchamment d'opprimer, ils parlent avec hauteur.
73:9	Ils élèvent leur bouche jusqu'aux cieux, et leur langue parcourt la Terre.
73:10	C'est pourquoi son peuple se tourne de leur côté, et on draine vers eux un plein d'eaux.
73:11	Ils disent : Comment El saurait-il ? Comment Elyon connaîtrait-il<!--Es. 29:15 ; Ez. 8:12 ; Ps. 94:7 ; Job 22:12-13.--> ?
73:12	Voilà, ceux-ci sont méchants, ils prospèrent toujours dans ce monde et acquièrent de plus en plus de richesses.
73:13	Quoi qu'il en soit, c'est donc en vain que j'ai purifié mon cœur et que j'ai lavé mes mains dans l'innocence<!--Mal. 3:14 ; Job 35:3.--> :
73:14	je suis frappé tous les jours, et tous les matins mon châtiment est là.
73:15	Si je disais : Je veux parler comme eux. Voici, je trahirais la génération de tes enfants.
73:16	Alors j'ai médité pour comprendre ces choses, mais cela m'a paru extrêmement difficile,
73:17	jusqu'à ce que je sois entré dans le sanctuaire de El, et que j'aie considéré la fin de telles gens.
73:18	Quoi qu'il en soit, tu les as mis sur des voies glissantes, tu les fais tomber dans des précipices.
73:19	Comment ! En un instant les voilà ruinés, ils sont finis, consumés par la terreur.
73:20	Ils sont comme un rêve lorsqu'on s'est réveillé. Adonaï, tu méprises leur image à ton réveil.
73:21	Quand mon cœur s'aigrissait et que je me sentais percé dans les entrailles,
73:22	j'étais alors stupide et je n'avais aucune connaissance, j'étais comme une bête dans ta présence.
73:23	Je serai donc toujours avec toi : tu m'as pris par la main droite.
73:24	Tu me conduiras par ton conseil, et tu me recevras dans la gloire.
73:25	Qui d'autre ai-je au ciel ? Avec toi, je ne désire rien sur la Terre.
73:26	Ma chair et mon cœur étaient consumés, mais Elohîm est le rocher de mon cœur et ma part pour l'éternité.
73:27	Car voilà, ceux qui s'éloignent de toi périront, tu retrancheras tous ceux qui se détournent de toi.
73:28	Mais pour moi, m'approcher d'Elohîm, c'est mon bonheur : j'ai mis toute mon espérance dans Adonaï YHWH, afin de raconter toutes tes œuvres.

## Chapitre 74

### Appel au secours du peuple d'Elohîm

74:1	Cantique d'Asaph. Elohîm, pourquoi nous as-tu rejetés pour toujours ? Et pourquoi ta colère fume-t-elle contre le troupeau de ton pâturage<!--Ps. 79:5.--> ?
74:2	Souviens-toi de ton assemblée que tu as acquise autrefois. Tu t'es approprié cette montagne de Sion, sur laquelle tu habitais, afin qu'elle soit la portion de ton héritage.
74:3	Élève tes pas vers les lieux constamment dévastés ! L'ennemi a tout renversé dans le lieu saint.
74:4	Tes adversaires ont rugi au milieu de ton assemblée ; ils ont mis leurs signes pour signes.
74:5	On les a vus pareils à celui qui lève la cognée dans une épaisse forêt.
74:6	Et maintenant, avec des haches et des marteaux, ils brisent les sculptures.
74:7	Ils ont mis le feu à ton sanctuaire, ils ont profané par terre la demeure de ton Nom<!--2 R. 25:9.-->.
74:8	Ils disaient dans leur cœur : Traitons-les tous avec violence ! Ils ont brûlé dans le pays tous les lieux de El.
74:9	Nous ne voyons plus nos signes, il n'y a plus de prophètes, et personne parmi nous qui sache jusqu'à quand<!--La. 2:9-10.-->...
74:10	Elohîm ! jusqu'à quand l'adversaire te couvrira-t-il d'opprobres ? Et l'ennemi méprisera-t-il ton Nom à jamais ?
74:11	Pourquoi retires-tu ta main, ta droite ? Tire-la du milieu de ton sein ! Détruis !
74:12	Elohîm est mon Roi dès les temps anciens, faisant des délivrances au milieu de la Terre.
74:13	Tu as fendu la mer par ta force, tu as brisé les têtes des monstres marins sur les eaux,
74:14	tu as brisé les têtes du léviathan, tu l'as donné pour nourriture au peuple du désert.
74:15	Tu as ouvert la fontaine et le torrent, tu as desséché les grosses rivières.
74:16	À toi est le jour, à toi aussi est la nuit ! Tu as établi la lumière et le soleil.
74:17	Tu as posé toutes les limites de la Terre, tu as formé l'été et l'hiver.
74:18	Souviens-toi de ceci : que l'ennemi a blasphémé YHWH, et qu'un peuple insensé a méprisé ton Nom !
74:19	Ne livre pas aux vivants l'âme de la tourterelle, n'oublie pas à toujours la vie de tes affligés !
74:20	Regarde à ton alliance, car les lieux ténébreux de la Terre sont remplis d'habitations de violence.
74:21	Ne permets pas que celui qui est foulé s'en retourne tout confus. Que l'affligé et le pauvre louent ton Nom !
74:22	Elohîm, lève-toi, défends ta cause ! Souviens-toi de l'opprobre qui t'est fait tous les jours par l'insensé !
74:23	N'oublie pas le cri de tes adversaires, le bruit de ceux qui s'élèvent contre toi monte continuellement !

## Chapitre 75

### L'élévation vient de YHWH

75:1	Psaume d'Asaph. Cantique. Au chef de musique. Sur Al-Thasheth<!--Voir Ps. 57:1.-->.
75:2	Nous te louons, Elohîm, nous te louons. Ton Nom est près de nous, nous racontons tes merveilles.
75:3	Au temps que j'aurai fixé, je jugerai avec droiture.
75:4	La Terre se dissout avec tous ceux qui y habitent, mais j'affermis ses colonnes. Sélah.
75:5	Je dis à ceux qui se vantent : Ne vous vantez pas ! Et aux méchants : N'élevez pas la corne !
75:6	N'élevez pas si haut votre corne, et ne parlez pas, le cou arrogant.
75:7	Car l'élévation ne vient ni de l'orient, ni de l'occident, ni du désert.
75:8	Mais c'est Elohîm qui gouverne : il abaisse l'un, et il élève l'autre<!--1 S. 2:7.-->.
75:9	Mais il y a dans la main de YHWH<!--Es. 51:17-22 ; Jé. 25:27-28 ; Ap. 14:10, 16:19.--> une coupe où fermente un vin plein de mélange. Il en verse en effet, et tous les méchants de la Terre en suceront, ils en boiront jusqu'à la lie.
75:10	Mais moi, je raconterai ces choses à jamais, je chanterai à Elohîm de Yaacov.
75:11	J'humilierai tous les méchants, mais les justes seront élevés.

## Chapitre 76

### La puissance d'Elohîm redoutable

76:1	Au chef de musique. Sur Negiynah. Psaume d’Asaph. Cantique.
76:2	Elohîm est connu en Yéhouda, son Nom est grand en Israël !
76:3	Sa tente est à Salem, et sa demeure à Sion.
76:4	Là il a brisé les arcs étincelants, le bouclier, l'épée et les armes de guerre. Sélah.
76:5	Tu es resplendissant, plus magnifique que les montagnes des ravisseurs.
76:6	Les hommes au cœur puissant ont été dépouillés, ils se sont endormis de leur sommeil, et ils n'ont plus trouvé leurs mains, tous ces hommes talentueux.
76:7	Elohîm de Yaacov, les cavaliers et les chevaux se sont endormis quand tu les as menacés.
76:8	Tu es redoutable, toi ! Qui peut se tenir devant toi quand ta colère éclate ?
76:9	Des cieux tu fais entendre ton jugement, la Terre a peur et se tient tranquille,
76:10	quand tu te lèves, Elohîm ! pour faire justice, pour délivrer tous les affligés de la Terre ! Sélah.
76:11	L'être humain te célèbre, même dans sa fureur, quand tu te ceins de toute ta colère.
76:12	Faites vos vœux à YHWH votre Elohîm et accomplissez-les ! Que tous ceux qui l'environnent apportent des dons à Elohîm redoutable !
76:13	Il coupe le souffle des princes, il est redoutable pour les rois de la Terre !

## Chapitre 77

### Se souvenir des prodiges de YHWH

77:1	Psaume d'Asaph. Au chef de musique. D'après Yedoutoun.
77:2	Ma voix s'élève vers Elohîm, et je crie ; ma voix s'adresse à Elohîm, et il m'écoutera.
77:3	Je cherche Adonaï au jour de ma détresse. Sans cesse mes mains s'étendent durant la nuit, mon âme refuse d'être consolée.
77:4	Je me souviens d'Elohîm, et je gémis ; je médite, et mon esprit est affaibli. Sélah.
77:5	Tu empêches mes yeux de dormir ; je suis troublé, et je ne puis parler.
77:6	Je pense aux jours d'autrefois et aux années des siècles passés<!--Ps. 143:5.-->.
77:7	Je me souviens de mes chants pendant la nuit, je médite en mon cœur, et mon esprit fait des recherches.
77:8	Adonaï m'a-t-il rejeté pour toujours ? Ne me sera-t-il plus favorable ?
77:9	Sa bonté est-elle disparue pour toujours ? Sa parole a-t-elle pris fin d'âge en âge ?
77:10	El a-t-il oublié d'avoir compassion ? A-t-il dans sa colère retiré sa miséricorde ? Sélah.
77:11	Puis je dis : C'est bien ce qui m'affaiblit, la droite d'Elyon a changé.
77:12	Je me souviens des exploits de Yah. Oui, je me souviens de tes merveilles d'autrefois.
77:13	Je méditerai toutes tes œuvres, et je parlerai de tes œuvres.
77:14	Elohîm ! tes voies sont saintes. Quel el est grand comme Elohîm ?
77:15	Tu es le El qui fait des merveilles ! Tu as fait connaître ta force parmi les peuples.
77:16	Tu as délivré par ton bras ton peuple, les fils de Yaacov et de Yossef. Sélah.
77:17	Les eaux t'ont vu, Elohîm ! Les eaux t'ont vu et ont tremblé ; même les abîmes en ont été émus.
77:18	Les nuées ont versé un déluge d'eau, les nuées ont fait retentir leur son, tes flèches ont volé de toutes parts.
77:19	La voix de ton tonnerre était dans le tourbillon, les éclairs ont éclairé le monde, la Terre en a été émue et en a tremblé.
77:20	Tu te frayas un chemin par la mer, un sentier par les grosses eaux, et tes traces ne furent plus reconnues.
77:21	Tu as mené ton peuple, comme un troupeau, sous la conduite de Moshè et d'Aaron<!--Mi. 6:4.-->.

## Chapitre 78

### Les œuvres d'Elohîm dans l'histoire d'Israël

78:1	Cantique d'Asaph. Mon peuple, écoute ma torah ! Prêtez vos oreilles aux paroles de ma bouche !
78:2	J'ouvrirai ma bouche en paraboles, je ferai jaillir les énigmes cachées des temps anciens<!--Mt. 13:35.-->.
78:3	Ce que nous avons entendu et connu, et que nos pères nous ont raconté<!--Ps. 44:2.-->,
78:4	nous ne le cacherons pas à leurs fils. Ils raconteront à la génération à venir les louanges de YHWH, sa puissance et ses merveilles qu'il a faites.
78:5	Car il a établi le témoignage en Yaacov, et il a mis la torah en Israël ; il a donné cet ordre à nos pères de la faire connaître à leurs fils<!--De. 4:9.-->,
78:6	pour qu'elle soit connue de la génération future, des fils qui naîtraient, et pour que lorsqu'ils seront grands, ils la relatent à leurs fils,
78:7	afin qu'ils mettent leur confiance en Elohîm, et qu'ils n'oublient pas les œuvres d'Elohîm, et qu'ils gardent ses commandements.
78:8	Afin qu'ils ne soient pas comme leurs pères, une génération réfractaire et rebelle, une génération dont le cœur n'est pas ferme et dont l'esprit n'est pas fidèle à El<!--Ex. 32:9 ; Ac. 7:51.-->.
78:9	Les fils d'Éphraïm, armés et tirant de l'arc, tournèrent le dos le jour de la bataille.
78:10	Ils ne gardèrent pas l'alliance d'Elohîm et refusèrent de marcher selon sa torah.
78:11	Ils oublièrent ses œuvres et ses merveilles qu'il leur avait fait voir.
78:12	Il avait fait des miracles en présence de leurs pères, dans le pays d'Égypte, dans le champ de Tsoan.
78:13	Il fendit la mer et les fit passer au travers, il fit arrêter les eaux comme un monceau de pierres.
78:14	Il les conduisit de jour par la nuée, et toute la nuit par une lumière de feu<!--Ex. 13:21.-->.
78:15	Il fendit les rochers dans le désert, et leur donna à boire d'abondantes eaux, comme s'il eût puisé des abîmes.
78:16	Il fit sortir des ruisseaux de la roche<!--Ex. 17:6 ; No. 20:11 ; 1 Co. 10:4.--> et fit couler des eaux comme des rivières.
78:17	Toutefois, ils continuèrent à pécher contre lui, irritant Elyon dans le désert.
78:18	Ils tentèrent El dans leurs cœurs, en demandant de la viande selon leur désir.
78:19	Ils parlèrent contre Elohîm, en disant : El pourrait-il dresser une table dans ce désert<!--No. 11:4.--> ?
78:20	Voici, il a frappé le rocher, et les eaux ont coulé et des torrents ont débordé. Pourrait-il aussi nous donner du pain ? Fournirait-il de la viande à son peuple ?
78:21	C'est pourquoi, YHWH l'entendit et s'irrita grandement. Le feu s'embrasa contre Yaacov, et sa colère s'excita contre Israël.
78:22	Parce qu'ils n'avaient pas cru en Elohîm et ne s'étaient pas confiés en sa délivrance.
78:23	Il ordonna aux nuées d'en haut et il ouvrit les portes des cieux ;
78:24	il fit pleuvoir la manne sur eux pour leur nourriture et il leur donna le blé du ciel<!--Ex. 16:14 ; Jn. 6:31.-->.
78:25	Les hommes mangèrent le pain des grands. Il leur envoya de la viande pour s'en rassasier.
78:26	Il excita dans les cieux le vent d'orient et il amena par sa puissance le vent du sud.
78:27	Il fit pleuvoir sur eux de la chair comme de la poussière, et des oiseaux ailés, en une quantité pareille au sable de la mer.
78:28	Il les fit tomber au milieu du camp, autour de leurs demeures.
78:29	Ils en mangèrent et en furent pleinement rassasiés, car il leur donna selon leur désir.
78:30	Mais ils ne furent pas encore dégoûtés de leur désir, et leur viande était encore dans leur bouche
78:31	quand la colère d'Elohîm s'excita contre eux, et qu'il mit à mort les plus gras d'entre eux, et abattit les jeunes hommes d'Israël<!--1 Co. 10:5.-->.
78:32	Malgré cela, ils péchèrent encore et ne crurent pas à ses prodiges<!--No. 14:2.-->.
78:33	C'est pourquoi il consuma leurs jours par la vanité et leurs années par une fin soudaine.
78:34	Quand il les mettait à mort, alors ils le recherchaient, ils se repentaient et ils cherchaient El dès le matin.
78:35	Ils se souvenaient qu'Elohîm était leur rocher<!--1 Co. 10:4.-->, que El Elyon était leur rédempteur.
78:36	Mais ils le trompaient de leur bouche et ils lui mentaient de leur langue<!--Es. 29:13 ; Jé. 12:2 ; Mt. 15:8.--> ;
78:37	car leur cœur n'était pas droit envers lui, et ils ne furent pas fidèles à son alliance.
78:38	Toutefois, comme il est compatissant, il pardonna leur iniquité, au point qu'il ne les détruisit pas ; mais il détourna souvent sa colère et ne réveilla pas toute sa fureur.
78:39	Il se souvenait qu'ils n'étaient que chair, un souffle<!--Le mot hébreu signifie aussi « esprit ».--> qui s'en va et ne revient pas.
78:40	Combien de fois l'ont-ils irrité dans le désert, et combien de fois l'ont-ils attristé dans ce lieu inhabitable ?
78:41	Ils ne cessèrent de tenter El et de provoquer le Saint<!--Voir commentaire en Ac. 3:14.--> d'Israël.
78:42	Ils ne se souvinrent pas de sa puissance, du jour où il les délivra de la main de l'ennemi,
78:43	des miracles qu'il accomplit en Égypte, et de ses merveilles dans les champs de Tsoan.
78:44	Il changea en sang leurs fleuves et leurs ruisseaux, et ils ne purent en boire les eaux<!--Ex. 7:20.-->.
78:45	Il envoya contre eux des mouches qui les dévorèrent, et des grenouilles qui les détruisirent<!--Ex. 8:6-24.-->.
78:46	Il livra leurs récoltes aux sauterelles, le produit de leur travail aux sauterelles<!--Ex. 10:13.-->.
78:47	Il détruisit leurs vignes par la grêle, et leurs sycomores par les orages<!--Ex. 9:23.-->.
78:48	Il livra leur bétail à la grêle, et leurs troupeaux aux foudres étincelantes.
78:49	Il envoya sur eux l'ardeur de sa colère, la fureur, la rage et la détresse, une troupe de messagers de malheur.
78:50	Il donna libre cours à sa colère, et ne retira pas leur âme de la mort, il livra leur vie à la peste<!--Ex. 9:6.-->.
78:51	Il frappa tout premier-né en Égypte, les premiers de la vigueur sous les tentes de Cham<!--Ex. 12:29.-->.
78:52	Il fit partir son peuple comme des brebis, il les mena comme un troupeau dans le désert.
78:53	Il les dirigea sûrement, pour qu'ils soient sans crainte, et la mer couvrit leurs ennemis.
78:54	Il les amena vers sa frontière sainte, vers cette montagne que sa droite a acquise<!--Ex. 15:17.-->.
78:55	Il chassa devant eux les nations, leur assigna par le sort leur part d'héritage, et fit habiter les tribus d'Israël dans les tentes de ces nations.
78:56	Mais ils tentèrent et irritèrent Elohîm Elyon, et ne gardèrent pas ses témoignages.
78:57	Et ils se retirèrent en arrière et furent infidèles comme leurs pères. Ils tournèrent comme un arc trompeur.
78:58	Ils l'irritèrent par leurs hauts lieux, et ils provoquèrent sa jalousie par leurs images taillées<!--De. 32:16-21.-->.
78:59	Elohîm l'entendit et se mit dans une grande colère, et il méprisa fortement Israël.
78:60	Il abandonna la demeure de Shiyloh, la tente où il habitait parmi les humains.
78:61	Il livra en captivité sa force et son ornement entre les mains de l'ennemi.
78:62	Il livra son peuple à l'épée et se mit dans une grande colère contre son héritage.
78:63	Le feu dévora leurs jeunes hommes, et leurs vierges ne furent pas vantées<!--Voir Ge. 12:15. Le mot hébreu signifie aussi « briller », « louer ».-->.
78:64	Leurs prêtres tombèrent par l'épée, et leurs veuves ne les pleurèrent pas.
78:65	Puis Adonaï se réveilla comme un homme qui se serait endormi, comme un puissant homme vaincu par le vin.
78:66	Il frappa ses adversaires par derrière, et les mit en opprobre perpétuel.
78:67	Mais il dédaigna la tente de Yossef, et ne choisit pas la tribu d'Éphraïm.
78:68	Mais il choisit la tribu de Yéhouda, la montagne de Sion, celle qu'il aime.
78:69	Il bâtit son sanctuaire dans les lieux élevés, et l'établit comme la Terre qu'il a fondée pour toujours.
78:70	Il choisit David, son serviteur, et le prit de l'enclos des moutons<!--1 S. 16:11 ; 2 S. 7:8.-->.
78:71	Il le prit derrière les brebis qui allaitent et l'amena pour paître Yaacov, son peuple, et Israël, son héritage.
78:72	Et David les fit paître selon l'intégrité de son cœur et les conduisit par l'intelligence de ses mains.

## Chapitre 79

### Appel au jugement d'Elohîm

79:1	Psaume d'Asaph. Elohîm, les nations sont entrées dans ton héritage. On a rendu impur ton saint temple, on a mis Yeroushalaim en monceaux de pierres.
79:2	On a livré les cadavres de tes serviteurs pour viande aux oiseaux du ciel, et la chair de tes fidèles aux bêtes de la Terre.
79:3	On a répandu leur sang comme de l'eau autour de Yeroushalaim, et il n'y a eu personne pour les enterrer.
79:4	Nous sommes devenus un objet de mépris pour nos voisins, de moquerie et de risée pour ceux qui habitent autour de nous<!--Ps. 44:14, 80:7.-->.
79:5	Jusqu'à quand, YHWH, t'irriteras-tu sans cesse et ta jalousie s'embrasera-t-elle comme un feu<!--Ps. 89:47.--> ?
79:6	Répands ta fureur sur les nations qui ne te connaissent pas et sur les royaumes qui n'invoquent pas ton Nom<!--Jé. 10:25.--> !
79:7	Car on a dévoré Yaacov, et on a ravagé ses demeures.
79:8	Ne rappelle pas devant nous les iniquités passées. Que tes compassions viennent en hâte au-devant de nous ! Car nous sommes dans une extrême détresse.
79:9	Elohîm de notre délivrance ! aide-nous pour l'amour de la gloire de ton Nom et délivre-nous ! Fais la propitiation pour nos péchés pour l'amour de ton Nom !
79:10	Pourquoi les nations diraient-elles : Où est leur Elohîm ? Que la vengeance du sang de tes serviteurs, qui a été répandu, soit manifestée parmi les nations en notre présence !
79:11	Que le gémissement des captifs parvienne jusqu'à toi ! Par ton bras puissant, sauve tes fils, ceux qui vont périr !
79:12	Et rends à nos voisins, dans leur sein, sept fois les insultes par lesquelles ils t'ont insulté, Adonaï !
79:13	Mais nous, ton peuple, et le troupeau de ton pâturage, nous te louerons pour toujours, et de génération en génération nous publierons tes louanges.

## Chapitre 80

### Implorer YHWH

80:1	Psaume d'Asaph. Au chef de musique. Sur Shoushannim-Edouth.
80:2	Toi qui pais Israël, prête l'oreille ! Toi qui mènes Yossef comme un troupeau, toi qui es assis entre les chérubins<!--2 S. 6:2 ; Es. 37:16 ; Ps. 99:1.-->, fais briller ta splendeur !
80:3	Réveille ta puissance au-devant d'Éphraïm, de Benyamin et de Menashè et viens pour notre délivrance !
80:4	Elohîm, ramène-nous et fais briller ta face ! Et nous serons délivrés !
80:5	YHWH Elohîm Sabaoth, jusqu'à quand seras-tu irrité contre la prière de ton peuple ?
80:6	Tu les nourris de pain de larmes, et tu les abreuves de larmes à pleine mesure.
80:7	Tu fais de nous un sujet de dispute entre nos voisins, et nos ennemis se moquent de nous.
80:8	Elohîm Sabaoth ! ramène-nous et fais briller ta face, et nous serons délivrés.
80:9	Tu avais retiré une vigne hors d'Égypte, tu as chassé les nations, et tu l'as plantée<!--Es. 5:1-7 ; Os. 10:1 ; Mt. 20:1, 21:28-33.-->.
80:10	Tu as préparé une place devant elle, tu lui as fait prendre racine, et elle a rempli la Terre.
80:11	Les montagnes étaient couvertes de son ombre, et ses rameaux étaient comme de hauts cèdres de El.
80:12	Elle étendait ses branches jusqu'à la mer, et ses rejetons jusqu'au fleuve.
80:13	Pourquoi as-tu rompu ses clôtures, de sorte que tous les passants sur la route cueillent ses raisins ?
80:14	Les sangliers de la forêt l'ont détruite, et toutes les bêtes des champs en font leur pâture.
80:15	Elohîm Sabaoth, reviens je t'en prie ! Regarde des cieux et vois, visite cette vigne,
80:16	la racine que ta droite avait plantée, le fils que tu t'es choisi.
80:17	Elle est brûlée par le feu, elle est coupée ! Ils périssent devant ta face menaçante.
80:18	Que ta main soit sur l'homme de ta droite, sur le fils de l'homme que tu t'es choisi.
80:19	Et nous ne nous éloignerons plus de toi. Rends-nous la vie, et nous invoquerons ton Nom.
80:20	YHWH Elohîm Sabaoth, ramène-nous ! Fais briller ta face et nous serons délivrés !

## Chapitre 81

### Interdiction des elohîm étrangers

81:1	Psaume d'Asaph. Au chef de musique. Sur la Guitthith.
81:2	Chantez avec allégresse à notre Elohîm, notre force ! Poussez des cris de joie en l'honneur d'Elohîm de Yaacov !
81:3	Sonnez du shofar, prenez le tambour, la harpe mélodieuse et le luth !
81:4	Sonnez du shofar à la nouvelle lune, à la pleine lune, au jour de notre fête<!--No. 10:10.--> !
81:5	Car c'est une loi pour Israël, une ordonnance d'Elohîm de Yaacov.
81:6	Il établit un statut à Yossef, lorsqu'il marcha contre le pays d'Égypte, où j'entendis un langage que je ne connaissais pas.
81:7	J'ai déchargé son épaule du fardeau, et ses mains ont lâché les corbeilles.
81:8	Tu as crié dans la détresse, et je t'ai sauvé. Je t'ai répondu dans le lieu caché du tonnerre, je t'ai éprouvé auprès des eaux de Meriba. Sélah.
81:9	Écoute, mon peuple ! Je te relèverai. Israël, si tu m'écoutais !
81:10	Il n'y aura pas au milieu de toi de el étranger, et tu ne te prosterneras pas devant le el des étrangers.
81:11	Je suis YHWH, ton Elohîm, qui t'ai fait monter hors du pays d'Égypte. Ouvre ta bouche et je la remplirai.
81:12	Mais mon peuple n'a pas écouté ma voix, et Israël ne m'a pas obéi.
81:13	C'est pourquoi je les ai abandonnés à la dureté<!--Mt. 19:8 ; Mc. 10:5.--> de leur cœur, et ils ont suivi leurs propres conseils<!--Es. 63:17, 65:2 ; Ro. 1:24-26 ; 2 Pi. 3:3.-->.
81:14	Si mon peuple m'écoutait ! Si Israël marchait dans mes voies !
81:15	J'abattrais en un instant leurs ennemis et je tournerais ma main contre leurs adversaires.
81:16	Ceux qui haïssent YHWH le flatteraient, et le bonheur de mon peuple durerait toujours.
81:17	Elohîm le nourrirait du meilleur froment, et je le rassasierais du miel du rocher.

## Chapitre 82

### Elohîm dénonce l'injustice des hommes

82:1	Psaume d'Asaph. Elohîm se tient dans l'assemblée de El, il juge au milieu des elohîm<!--Ou des juges.-->.
82:2	Jusqu'à quand jugerez-vous avec injustice et aurez-vous égard à l'apparence de la personne des méchants<!--Ps. 58:2.--> ? Sélah.
82:3	Défendez le faible et l'orphelin, rendez justice à l'affligé et au pauvre,
82:4	délivrez celui qu'on maltraite et le misérable, délivrez-les de la main des méchants.
82:5	Ils ne connaissent ni n'entendent rien, ils marchent dans les ténèbres, tous les fondements de la Terre sont ébranlés.
82:6	J'ai dit : Vous êtes des elohîm<!--Jn. 10:34.-->, et vous êtes tous fils d'Elyon<!--Très Haut.-->.
82:7	Toutefois, vous mourrez comme les humains<!--Ou Adam.-->, et vous les princes vous tomberez comme les autres.
82:8	Elohîm, lève-toi, juge la Terre, car tu auras en héritage toutes les nations<!--Ps. 2:8 ; Hé. 1:2.--> !

## Chapitre 83

### Dessein et confusion des ennemis d'Israël

83:1	Cantique et psaume d'Asaph.
83:2	Elohîm ! ne garde pas le silence, ne te tais pas, et ne te tiens pas en repos, El<!--Ps. 35:22.--> !
83:3	Car voici, tes ennemis s'agitent, et ceux qui te haïssent lèvent la tête.
83:4	En secret, ils prennent des conseils rusés contre ton peuple, ils se consultent ensemble contre ceux qui se retirent vers toi pour se cacher<!--Ps. 2:2.-->.
83:5	Venez, disent-ils, détruisons-les, de sorte qu'ils ne soient plus une nation, et qu'on ne fasse plus mention du nom d'Israël<!--Ce passage fait allusion aux désirs qu'ont certaines nations de voir Israël détruite. Mi. 4:11 ; Ap. 11:1-2.--> !
83:6	Car ils se consultent ensemble d'un même cœur, ils font alliance contre toi.
83:7	Les tentes d'Édom et des Yishmaelites, des Moabites et des Hagaréniens,
83:8	de Guebal, d'Ammon, d'Amalek, les Philistins avec les habitants de Tyr.
83:9	L'Assyrie aussi se joint à eux, elle sert de bras aux fils de Lot. Sélah.
83:10	Traites-les comme Madian<!--Jg. 7:15.-->, comme Sisera<!--Jg. 4:15.-->, et comme Yabiyn, au torrent de Kison !
83:11	Ils furent détruits à En-Dor et servirent de fumier à la Terre.
83:12	Que leurs chefs soient traités comme Oreb et comme Zeeb, et que tous leurs princes soient comme Zébach et Tsalmounna<!--Jg. 7:25.--> !
83:13	Parce qu'ils disent : Prenons possession des habitations agréables d'Elohîm !
83:14	Mon Elohîm ! rends-les semblables au tourbillon et au chaume chassé par le vent,
83:15	comme le feu brûle une forêt, et comme la flamme embrase les montagnes.
83:16	Poursuis-les ainsi par ta tempête et épouvante-les par ton tourbillon !
83:17	Couvre leurs visages d'ignominie afin qu'on cherche ton Nom, YHWH !
83:18	Qu'ils soient honteux et épouvantés à jamais, qu'ils rougissent, et qu'ils périssent !
83:19	Afin qu'on sache que toi seul, dont le Nom est YHWH, tu es Elyon<!--Très-Haut.--> sur toute la Terre.

## Chapitre 84

### Délices pour ceux qui ont YHWH comme appui

84:1	Psaume des fils de Koré. Au chef de musique. Sur la Guitthith.
84:2	YHWH Sabaoth, que tes demeures sont aimables !
84:3	Mon âme soupire et languit après les parvis de YHWH, mon cœur et ma chair poussent des cris de joie vers le El vivant.
84:4	Le passereau même trouve sa maison, et l'hirondelle son nid où elle a mis ses petits... Tes autels, YHWH Sabaoth ! Mon Roi et mon Elohîm !
84:5	Bénis sont ceux qui habitent ta maison, et qui te louent sans cesse ! Sélah.
84:6	Béni est l'homme dont la force est en toi, et ceux dans le cœur desquels sont les chemins tout tracés !
84:7	En passant par la vallée de Baca, ils en font une source, et la pluie la couvre de bénédictions.
84:8	Ils marchent avec force pour se présenter devant Elohîm à Sion.
84:9	YHWH Elohîm Sabaoth, écoute ma prière, Elohîm de Yaacov, prête l'oreille. Sélah.
84:10	Elohîm, notre bouclier, vois et regarde la face de ton mashiah !
84:11	Car mieux vaut un jour dans tes parvis, que mille ailleurs. J'aimerais mieux me tenir à la porte dans la maison de mon Elohîm, que de demeurer dans les tentes des méchants.
84:12	Car YHWH Elohîm est un soleil<!--YHWH, notre soleil, se révèle en la personne de Yéhoshoua. Voir Lu. 1:78.--> et un bouclier<!--Ge. 15:1 ; Ps. 89:19, 144:2.--> ; YHWH donne la grâce et la gloire, et il ne refuse pas le bonheur à ceux qui marchent dans l'intégrité.
84:13	YHWH Sabaoth, béni est l'homme qui se confie en toi<!--Ps. 2:12.--> !

## Chapitre 85

### Supplication des rescapés de l'exil

85:1	Psaume des fils de Koré. Au chef de musique.
85:2	YHWH, tu as été favorable à ta terre, tu as ramené et mis en repos les prisonniers de Yaacov.
85:3	Tu as pardonné l'iniquité de ton peuple, tu as couvert tous leurs péchés. Sélah.
85:4	Tu as retiré toute ta colère, tu es revenu de l'ardeur de ton indignation.
85:5	Elohîm de notre délivrance, rétablis-nous et fais cesser la colère que tu as contre nous.
85:6	Seras-tu irrité à jamais contre nous ? Feras-tu durer ta colère de génération en génération ?
85:7	Ne reviendras-tu pas nous rendre la vie<!--Ps. 71:20.-->, afin que ton peuple se réjouisse en toi ?
85:8	YHWH, fais-nous voir ta miséricorde et accorde-nous ta délivrance !
85:9	J'écouterai ce que dira El, YHWH, car il parlera de paix à son peuple et à ses bien-aimés, pourvu que jamais ils ne retournent à leur folie.
85:10	Certainement, sa délivrance est proche de ceux qui le craignent, la gloire habite dans notre pays.
85:11	La grâce et la vérité<!--Jn. 1:17.--> se rencontrent, la justice et la paix s'embrassent<!--Hé. 7:2.-->.
85:12	La vérité germe de la Terre, et la justice regarde des cieux.
85:13	YHWH donnera ce qui est bon, et notre terre rendra son fruit.
85:14	La justice marchera devant lui, et il la mettra sur le chemin où il passera.

## Chapitre 86

### Cœur disposé à la crainte d'Elohîm

86:1	Prière de David. YHWH, écoute, réponds-moi ! Car je suis affligé et misérable.
86:2	Garde mon âme, car je suis un de tes bien-aimés ! Toi, mon Elohîm, délivre ton serviteur qui se confie en toi !
86:3	Adonaï, aie pitié de moi, car je crie à toi tout le jour.
86:4	Réjouis l'âme de ton serviteur, car j'élève mon âme à toi, Adonaï.
86:5	YHWH, tu es bon et clément, et d'une grande bonté envers tous ceux qui t'invoquent<!--Joë. 2:13.-->.
86:6	YHWH, prête l'oreille à ma prière, et sois attentif à la voix de mes supplications !
86:7	Je t'invoque au jour de ma détresse, car tu m'exauces<!--Ps. 50:15.-->.
86:8	Adonaï, nul n'est comme toi parmi les dieux, et rien ne ressemble à tes œuvres<!--De. 3:24 ; Ps. 95:3.-->.
86:9	Adonaï, toutes les nations que tu as faites viendront et se prosterneront devant toi, et glorifieront ton Nom.
86:10	Car tu es grand, et tu fais des choses merveilleuses. Tu es Elohîm, toi seul.
86:11	YHWH, enseigne-moi tes voies et je marcherai dans ta vérité<!--Ps. 25:4, 27:11.-->. Unifie mon cœur pour que je craigne ton Nom !
86:12	Adonaï, mon Elohîm, je te célébrerai de tout mon cœur, et je glorifierai ton Nom à toujours !
86:13	Car ta bonté est grande envers moi, et tu as délivré mon âme du shéol, de ses parties inférieures.
86:14	Elohîm, des gens orgueilleux se sont élevés contre moi, et une troupe de méchants en veut à ma vie ; ils ne portent pas leurs pensées sur toi.
86:15	Mais toi, Adonaï, tu es le El compatissant, miséricordieux, lent à la colère, riche en bonté et en vérité.
86:16	Tourne-toi vers moi, et aie pitié de moi ! Donne ta force à ton serviteur, délivre le fils de ta servante !
86:17	Accorde-moi un signe de ta faveur, et que ceux qui me haïssent le voient et soient honteux ! Parce que tu m'aideras, YHWH ! Tu me consoleras !

## Chapitre 87

### Sion, la cité d'Elohîm

87:1	Psaume. Cantique des fils de Koré. Sa fondation est sur les montagnes saintes.
87:2	YHWH aime les portes de Sion plus que toutes les demeures de Yaacov.
87:3	Ce qui se dit de toi, cité d'Elohîm, sont des choses glorieuses ! Sélah.
87:4	Je ferai mention de Rahab et de Babel<!--Babylone.--> parmi ceux qui me connaissent ; voici le pays des Philistins, et Tyr avec l'Éthiopie : c'est dans Sion qu'ils sont nés.
87:5	Et de Sion il est dit : Un homme, un homme y est né, Elyon lui-même l'établira.
87:6	Quand YHWH enregistrera les peuples, il écrira : C'est là qu'ils sont nés. Sélah.
87:7	Et les chanteurs, de même que les joueurs de flûte s'écrient : Toutes mes sources sont en toi.

## Chapitre 88

### Lamentation dans l'affliction

88:1	Cantique. Psaume des fils de Koré. Au chef de musique. Sur la flûte. Cantique d'Héman, l'Ezrachite.
88:2	YHWH, Elohîm de ma délivrance ! je crie jour et nuit devant toi<!--Lu. 18:7.-->.
88:3	Que ma prière parvienne en ta présence ! Étends ton oreille à mon cri !
88:4	Car mon âme est rassasiée de maux, et ma vie atteint le shéol<!--Lu. 16:23.-->.
88:5	On m'a mis au rang de ceux qui descendent dans la fosse<!--Ps. 28:1, 31:13.-->. Je suis devenu comme un homme qui n'a plus de vigueur.
88:6	Je suis étendu parmi les morts, semblable à ceux qui sont tués et couchés dans la tombe, à ceux dont tu n'as plus le souvenir, et qui sont séparés par ta main.
88:7	Tu m'as mis dans la fosse inférieure, dans les ténèbres, dans les profondeurs.
88:8	Ta fureur se pose sur moi, et tu m'as accablé de tous tes flots. Sélah.
88:9	Tu as éloigné de moi ceux de qui j'étais connu, tu m'as mis en abomination devant eux ; je suis enfermé et je ne peux sortir.
88:10	Mes yeux se consument dans la souffrance. YHWH ! Je crie à toi tout le jour ! J'étends mes mains vers toi<!--Ex. 9:29 ; 1 R. 8:22 ; Job 17:7.--> !
88:11	Est-ce pour les morts que tu fais des miracles ? Les morts<!--Vient d'un mot hébreu qui signifie « fantômes de morts, ombres, revenants » ou encore « esprits ».--> se relèveront-ils pour te célébrer<!--1 Co. 15:12-13 ; 1 Th. 4:16.--> ? Sélah.
88:12	Parle-t-on de ta bonté dans le sépulcre, de ta fidélité dans le tombeau<!--Ep. 4:9-10 ; 1 Pi. 3:18-20.--> ?
88:13	Connaîtra-t-on tes merveilles dans les ténèbres, et ta justice dans la terre de l'oubli ?
88:14	Mais moi, YHWH, j'implore ton secours, ma prière s'élève dès le matin.
88:15	YHWH, pourquoi rejettes-tu mon âme ? Pourquoi me caches-tu ta face<!--Mt. 27:46 ; Mc. 15:34.--> ?
88:16	Je suis malheureux et moribond dès ma jeunesse, j'ai été exposé à tes terreurs, et je ne sais pas où j'en suis.
88:17	Les ardeurs de ta colère sont passées sur moi, et tes terreurs m'anéantissent<!--Es. 53:5.-->.
88:18	Elles m'environnent tout le jour comme des eaux, elles m'enveloppent toutes à la fois.
88:19	Tu as éloigné de moi mon ami intime et mon compagnon ; mes connaissances ont disparu<!--Mt. 26:56.-->.

## Chapitre 89

### Béni est le peuple qui connaît le son de la trompette

89:1	Cantique d'Éthan, l'Ezrachite.
89:2	Je chanterai toujours les bontés de YHWH, je ferai connaître de ma bouche ta fidélité de génération en génération.
89:3	Car je dis : Ta bonté a des fondements éternels ; tu établis ta fidélité dans les cieux quand tu dis :
89:4	J'ai traité alliance avec mon élu, j'ai fait serment à David, mon serviteur :
89:5	j'affermirai ta postérité pour toujours, et j'établirai ton trône de génération en génération<!--2 S. 7:8-16.-->. Sélah.
89:6	Les cieux célèbrent tes merveilles, YHWH ! Ta fidélité aussi est célébrée dans l'assemblée des saints.
89:7	Car qui, dans le nuage, peut se comparer à YHWH ? Qui est semblable à YHWH parmi les fils de El ?
89:8	El fait trembler dans la grande assemblée des saints, il est plus redoutable que tous ceux qui sont autour de lui.
89:9	Yah, Elohîm Sabaoth ! qui est semblable à toi, puissant YHWH ? Aussi ta fidélité t'environne.
89:10	Tu domines l'élévation des flots de la mer ; quand ses vagues s'élèvent, tu les calmes<!--Job 26:12, 38:8-12.-->.
89:11	Tu écrasas Rahab<!--Ce terme hébreu fait référence au nom emblématique de l'Égypte et signifie « largeur », « arrogance ».--> comme un homme blessé à mort ; tu dispersas tes ennemis par le bras de ta force.
89:12	À toi sont les cieux, à toi aussi est la Terre. Tu as fondé le monde, et tout ce qui est en lui.
89:13	Tu as créé le nord et le sud, le Thabor et l'Hermon se réjouissent en ton Nom.
89:14	Ton bras est puissant, ta main est forte, ta droite est haut élevée.
89:15	La justice et l'équité sont la base de ton trône ; la bonté et la vérité marchent devant ta face.
89:16	Béni est le peuple qui connaît le son de la trompette<!--1 Co. 15:52 ; Ap. 10:7.--> ! Il marche, YHWH ! à la clarté de ta face !
89:17	Il se réjouit chaque jour en ton Nom, et il se glorifie de ta justice.
89:18	Parce que tu es la gloire de leur puissance, c'est par ta faveur que s'élève notre corne.
89:19	Car notre bouclier est YHWH, et notre Roi est le Saint d'Israël.
89:20	Tu as autrefois parlé en vision concernant ton bien-aimé, et tu as dit : J'ai ordonné mon secours en faveur d'un homme vaillant ; j'ai élevé l'élu du milieu du peuple.
89:21	J'ai trouvé David, mon serviteur, je l'ai oint de ma sainte huile<!--1 S. 16:13 ; Ac. 13:22.-->.
89:22	Ma main sera ferme avec lui et mon bras le rendra fort.
89:23	L'ennemi ne le surprendra pas, et le fils de l'injustice ne l'affligera pas ;
89:24	mais j'écraserai devant lui ses adversaires, et je détruirai ceux qui le haïssent.
89:25	Ma fidélité et ma bonté seront avec lui, et sa gloire sera élevée en mon Nom.
89:26	Je mettrai sa main sur la mer, et sa droite sur les fleuves.
89:27	Il m'invoquera : Tu es mon Père, mon El, le Rocher<!--Voir commentaire en Es. 8:14.--> de mon salut.
89:28	Aussi je ferai de lui le premier-né<!--Col. 1:15.-->, le plus élevé des rois de la Terre.
89:29	Je lui garderai ma bonté à toujours, et mon alliance lui sera assurée.
89:30	Je rendrai éternelle sa postérité, et son trône comme les jours des cieux.
89:31	Mais si ses fils abandonnent ma torah et ne marchent pas dans mes statuts,
89:32	s'ils violent mes statuts et qu'ils ne gardent pas mes commandements,
89:33	je punirai de la verge leur transgression, et de plaie leur iniquité.
89:34	Mais je ne retirerai pas de lui ma bonté, et je ne lui trahirai pas ma fidélité.
89:35	Je ne violerai pas mon alliance, et je ne changerai pas ce qui est sorti de mes lèvres.
89:36	J'ai une fois juré par ma sainteté : mentirai-je à David<!--Hé. 6:13.--> ?
89:37	Sa postérité sera à toujours, et son trône sera devant moi comme le soleil,
89:38	il aura une durée éternelle comme la lune. Le témoin qui est dans le nuage est fidèle. Sélah.
89:39	Néanmoins, tu l'as rejeté et dédaigné<!--Es. 53:3.--> ! Tu t'es mis en grande colère contre ton mashiah !
89:40	Tu as rejeté l'alliance faite avec ton serviteur ; tu as souillé sa couronne en la jetant par terre.
89:41	Tu as rompu toutes ses murailles ; tu as mis en ruine ses forteresses.
89:42	Tous ceux qui passaient par le chemin l'ont pillé ; il a été mis en opprobre à ses voisins.
89:43	Tu as élevé la droite de ses adversaires, tu as réjoui tous ses ennemis.
89:44	Tu as fait reculer le tranchant de son épée, et tu ne l'as pas élevé dans le combat.
89:45	Tu as fait cesser sa splendeur, et tu as jeté par terre son trône.
89:46	Tu as abrégé les jours de sa jeunesse, et l'as couvert de honte. Sélah.
89:47	Jusqu'à quand, YHWH ? Te cacheras-tu à jamais ? Ta fureur s'embrasera-t-elle comme un feu ?
89:48	Souviens-toi quelle est la durée de ma vie. Pourquoi aurais-tu créé en vain tous les fils d'humains ?
89:49	Qui est l'homme fort qui vivra sans voir la mort ? Qui peut délivrer son âme de la main du shéol<!--1 Co. 15:54-57.--> ? Sélah.
89:50	Adonaï, où sont tes bontés premières que tu juras à David dans ta fidélité ?
89:51	Adonaï, souviens-toi de tes serviteurs qu'on insulte, je porte sur mon sein tous ces grands peuples.
89:52	Souviens-toi des outrages de tes ennemis, YHWH ! des outrages contre les pas de ton mashiah.
89:53	Béni soit à toujours YHWH ! Amen ! Oui, amen !

## Chapitre 90

### Mortalité de l'homme

90:1	Prière de Moshè, homme d'Elohîm<!--De. 33:1.-->. Adonaï ! Tu as été pour nous un refuge de génération en génération.
90:2	Avant que les montagnes soient nées et que tu aies formé la Terre et le monde, d'éternité en éternité, tu es El<!--Ge. 17:1 ; Es. 40:28.-->.
90:3	Tu fais revenir l'homme à la poussière, et tu dis : Fils des hommes, retournez<!--Ge. 3:19 ; Ec. 12:7.--> !
90:4	Car mille ans sont, à tes yeux, comme le jour d'hier qui est passé, et comme une veille de la nuit<!--Ps. 39:5 ; 2 Pi. 3:8.-->.
90:5	Tu les emportes semblables à un sommeil qui, le matin, passe comme l'herbe :
90:6	elle fleurit au matin et reverdit ; le soir on la coupe, et elle se fane<!--1 Pi. 1:24.-->.
90:7	Car nous sommes consumés par ta colère, et nous sommes troublés par ta fureur.
90:8	Tu as mis devant toi nos iniquités, et à la lumière de ta face nos fautes cachées.
90:9	Car tous nos jours s'en vont par ta grande colère, et nos années se consument dans un soupir.
90:10	Les jours de nos années s'élèvent à 70 ans, et pour les plus forts, à 80 ans, et l'orgueil qu'ils en tirent n'est que peine et misère, car le temps passe vite et nous nous envolons.
90:11	Qui connaît, selon ta crainte, la force de ton indignation et de ta grande colère ?
90:12	Enseigne-nous à compter nos jours, afin que notre cœur vienne à la sagesse !
90:13	YHWH, reviens ! Jusqu'à quand ? Sois apaisé envers tes serviteurs !
90:14	Rassasie-nous chaque matin de ta bonté, afin que nous nous réjouissions et que nous soyons joyeux tout le long de nos jours.
90:15	Réjouis-nous autant de jours que tu nous as affligés, autant d'années que nous avons vu le malheur.
90:16	Que ton œuvre se voie sur tes serviteurs, et ta gloire sur leurs fils !
90:17	Que la grâce de YHWH, notre Elohîm, soit sur nous ! Et affermis l'œuvre de nos mains, oui, affermis l'œuvre de nos mains !

## Chapitre 91

### La sécurité et la fidélité de YHWH

91:1	Celui qui demeure sous la couverture<!--Elohîm seul est notre couverture spirituelle. Voir Ps. 18:12, 27:5, 31:21, 32:7, 61:4-5.--> d'Elyon, repose à l'ombre de Shaddaï.
91:2	Je dis à YHWH : Tu es ma retraite et ma forteresse, tu es mon Elohîm en qui je me confie !
91:3	Car il te délivrera du filet de l'oiseleur, de la peste et de ses ravages.
91:4	Il te couvrira de ses plumes, et tu trouveras un refuge sous ses ailes. Sa fidélité est un bouclier et une cuirasse.
91:5	Tu ne craindras ni les terreurs de la nuit, ni la flèche qui vole le jour<!--Pr. 3:23-24.-->,
91:6	ni la peste qui marche dans les ténèbres, ni la destruction qui frappe en plein midi.
91:7	Que 1 000 tombent à ton côté, et 10 000 à ta droite, tu ne seras pas atteint.
91:8	De tes yeux tu regarderas, et tu verras la rétribution des méchants.
91:9	Car tu es mon refuge, YHWH ! Tu fais d'Elyon ta demeure.
91:10	Aucun malheur ne s'approchera de toi, aucun fléau n'approchera de ta tente<!--Ex. 8:18-19 ; Ps. 121:6-8.-->.
91:11	Car il ordonnera à ses anges de te garder dans toutes tes voies.
91:12	Ils te porteront sur les mains, de peur que ton pied ne heurte contre une pierre<!--Mt. 4:5-6 ; Lu. 4:9-11.-->.
91:13	Tu marcheras sur le lion et sur le cobra, tu piétineras le lionceau et le dragon.
91:14	Puisqu'il s'est attaché à moi, je le délivrerai, je le mettrai sur les hauteurs, parce qu'il connaît mon Nom.
91:15	Il m'invoquera et je l'exaucerai. Je serai avec lui dans la détresse, je le délivrerai et le glorifierai.
91:16	Je le rassasierai de jours, et je lui ferai voir mon salut<!--Yeshuw`ah.-->.

## Chapitre 92

### Proclamer la louange d'Elohîm

92:1	Psaume. Cantique pour le jour du shabbat.
92:2	C'est une belle chose que de célébrer YHWH, et de chanter ton Nom, Elyon<!--Ps. 147:1.--> !
92:3	Afin d'annoncer chaque matin ta bonté, et ta fidélité toutes les nuits<!--Ps. 59:17, 88:14, 89:2.-->,
92:4	sur l'instrument à dix cordes, sur le luth, et par un cantique médité sur la harpe.
92:5	Car YHWH, tu me réjouis par tes œuvres, je me réjouis des œuvres de tes mains !
92:6	YHWH, que tes œuvres sont magnifiques ! Tes pensées sont merveilleusement profondes<!--Es. 55:8-9 ; Job 5:9.-->.
92:7	L'homme stupide n'y connaît rien, et le fou n'y prend pas garde<!--Es. 5:12 ; Ro. 1:21.-->.
92:8	Les méchants poussent comme l'herbe, et tous ceux qui pratiquent la méchanceté fleurissent pour être exterminés éternellement<!--Jé. 12:1-2 ; Mal. 3:15 ; Ps. 37:2, 73:1-20.-->.
92:9	Mais toi, YHWH, tu es élevé à toujours !
92:10	Car voici, tes ennemis, YHWH ! Car voici, tes ennemis périssent ; tous ceux qui pratiquent la méchanceté sont dispersés.
92:11	Mais tu élèves ma corne comme celle d'un buffle, je suis pétri d'huile fraîche<!--Ps. 23:5 ; Hé. 1:9.-->.
92:12	Mes yeux se plaisent à regarder ceux qui m'épient, et mes oreilles à entendre les méchants qui s'élèvent contre moi.
92:13	Le juste fleurit comme le palmier, il croît comme le cèdre au Liban.
92:14	Étant plantés dans la maison de YHWH, ils fleurissent dans les parvis de notre Elohîm.
92:15	Ils portent encore des fruits à l'âge des cheveux blancs, ils sont gras et verdoyants<!--Os. 14:6 ; Ps. 1:3.-->,
92:16	afin d'annoncer que YHWH est droit. C'est mon Rocher, et il n'y a pas d'injustice en lui.

## Chapitre 93

### Majesté et puissance de YHWH

93:1	YHWH règne, il est revêtu de majesté, YHWH est revêtu de force, il s'en est ceint. Aussi le monde est tellement ferme, qu'il ne sera pas ébranlé.
93:2	Ton trône est établi dès lors, tu es de toute éternité<!--Ps. 9:8 ; Hé. 1:8.-->.
93:3	Les fleuves ont élevé, YHWH ! Les fleuves augmentent leur bruit, les fleuves élèvent leurs flots<!--Ps. 46:4, 65:7-8.-->.
93:4	YHWH, qui est dans les lieux élevés, est plus puissant que le bruit des grandes eaux, et que les fortes vagues de la mer<!--Es. 57:15 ; Ac. 7:49.-->.
93:5	Tes témoignages sont entièrement fidèles. YHWH ! La sainteté orne ta maison pour de longs jours.

## Chapitre 94

### À El seul la vengeance

94:1	El des vengeances, YHWH, El des vengeances, fais briller ta splendeur !
94:2	Toi, juge de la Terre, lève-toi ! Rends aux orgueilleux selon leurs œuvres !
94:3	Jusqu'à quand les méchants, YHWH, jusqu'à quand les méchants se réjouiront-ils ?
94:4	Jusqu'à quand tous ceux qui pratiquent la méchanceté discourront-ils, et diront-ils des paroles rudes et se vanteront-ils ?
94:5	YHWH ! Ils écrasent ton peuple, et affligent ton héritage.
94:6	Ils tuent la veuve et l'étranger, et ils mettent à mort les orphelins.
94:7	Ils disent : Yah ne le voit pas, l'Elohîm de Yaacov n'entend rien !
94:8	Vous les plus abrutis d'entre les peuples, prenez garde à ceci ! Et vous insensés, quand serez-vous intelligents ?
94:9	Celui qui a planté l'oreille, n'entendrait-il pas ? Celui qui a formé l'œil, ne verrait-il pas<!--Ex. 4:11 ; Pr. 20:12.--> ?
94:10	Celui qui châtie les nations ne punirait-il pas, lui qui apprend la connaissance aux humains<!--Ap. 19:15.--> ?
94:11	YHWH sait que les pensées des humains sont une vapeur.
94:12	Béni est l'homme que tu châties, Yah<!--Hé. 12:6.--> ! que tu instruis par ta torah,
94:13	afin qu'il soit dans la paix aux jours du malheur, jusqu'à ce que la fosse soit creusée pour le méchant !
94:14	Car YHWH ne délaisse pas son peuple, et n'abandonne pas son héritage<!--Es. 49:15 ; Ro. 11:2.-->.
94:15	C'est pourquoi le jugement s'unira à la justice, et tous ceux qui sont droits de cœur le suivront.
94:16	Qui se lèvera pour moi contre les méchants<!--Job 19:25 ; Ro. 8:31.--> ? Qui m'assistera contre ceux qui pratiquent la méchanceté ?
94:17	Si YHWH n'était pas mon secours, mon âme serait bien vite dans la demeure du silence.
94:18	Quand je dis : Mon pied chancelle ! Ta bonté me soutient, YHWH !
94:19	Quand j'ai beaucoup de pensées au-dedans de moi, tes consolations font les délices de mon âme.
94:20	Serais-tu l'allié du trône de méchanceté, qui forge des injustices contre les règles de la justice ?
94:21	Ils se rassemblent contre l'âme du juste, et condamnent le sang innocent<!--Mt. 27:1-4,24.-->.
94:22	Or YHWH est pour moi une haute retraite, mon Elohîm est le Rocher de mon refuge.
94:23	Il fera retourner sur eux leur iniquité, et les détruira par leur propre méchanceté. YHWH, notre Elohîm, les détruira<!--Mt. 13:30 ; Ap. 20:14-15.-->.

## Chapitre 95

### Adoration à YHWH

95:1	Venez, chantons à YHWH ! Poussons des cris de réjouissance au Rocher de notre salut.
95:2	Allons au-devant de lui en lui présentant nos actions de grâces, poussons devant lui des cris de réjouissance en chantant des psaumes !
95:3	Car YHWH est le Grand El, et il est le Grand Roi au-dessus de tous les elohîm.
95:4	Les lieux les plus profonds de la Terre sont dans sa main, et les sommets des montagnes sont à lui.
95:5	C'est à lui qu'appartient la mer, car lui-même l'a faite, et ses mains ont formé la Terre.
95:6	Venez, prosternons-nous, inclinons-nous, et mettons-nous à genoux devant YHWH qui nous a faits<!--Ps. 96:9 ; Ph. 2:10-11.--> !
95:7	Car il est notre Elohîm, et nous sommes le peuple de son pâturage, et les brebis que sa main conduit<!--Ps. 23:1, 100:3 ; Jn. 10:11.-->. Si vous entendez aujourd'hui sa voix,
95:8	n'endurcissez pas votre cœur<!--Hé. 3:8, 4:7.-->, comme à Meriba, comme au jour de Massa<!--Voir Ex. 17:7.--> dans le désert,
95:9	là où vos pères m'ont tenté et éprouvé bien qu'ils virent mes œuvres<!--Ex. 17:7.-->.
95:10	J'ai eu cette génération en dégoût durant 40 ans et j'ai dit : C'est un peuple dont le cœur s'égare, ils n'ont pas connu mes voies.
95:11	C'est pourquoi j'ai juré dans ma colère : ils n'entreront pas dans mon repos<!--No. 14:22-23 ; Hé. 3:15-19, 4:3.--> !

## Chapitre 96

### La grandeur et la gloire d'Elohîm

96:1	Chantez à YHWH un cantique nouveau<!--Es. 42:10 ; Ps. 98:1 ; Ap. 5:9, 14:3.--> ! Vous tous habitants de la Terre chantez à YHWH !
96:2	Chantez à YHWH, bénissez son Nom ! Prêchez de jour en jour sa délivrance !
96:3	Racontez sa gloire parmi les nations, ses merveilles parmi tous les peuples<!--Ps. 67:5.--> !
96:4	Car YHWH est grand et digne d'être loué, il est redoutable au-dessus de tous les elohîm<!--Ph. 2:9 ; Ap. 5:9.-->.
96:5	Car tous les elohîm des peuples ne sont que des faux elohîm, mais YHWH a fait les cieux.
96:6	La splendeur et la magnificence marchent devant lui, la force et la beauté sont dans son sanctuaire.
96:7	Familles des peuples, rendez à YHWH, rendez à YHWH la gloire et la puissance !
96:8	Rendez à YHWH la gloire due à son Nom ! Apportez des offrandes, et entrez dans ses parvis !
96:9	Prosternez-vous devant YHWH avec des ornements sacrés. Tremblez devant lui, vous toute la Terre !
96:10	Dites parmi les nations : YHWH est roi. Oui, le monde est affermi, il ne sera pas ébranlé. Il jugera les peuples avec équité.
96:11	Que les cieux se réjouissent, et que la Terre soit dans l'allégresse ! Que la mer tonne avec tout ce qui la remplit !
96:12	Que les champs s'égayent avec tout ce qui est en eux ! Alors tous les arbres de la forêt chanteront de joie,
96:13	devant YHWH. Car il vient, car il vient pour juger la Terre. Il jugera avec justice le monde, et les peuples selon sa fidélité.

## Chapitre 97

### Aimer Elohîm, c'est haïr le mal

97:1	YHWH règne ! Que la Terre soit dans l'allégresse, et que les îles nombreuses s'en réjouissent<!--Es. 42:10 ; Ps. 86:9, 93:1, 99:1.--> !
97:2	La nuée et les ténèbres épaisses sont autour de lui, la justice et le jugement sont la base de son trône.
97:3	Le feu marche devant lui, et embrase tout autour ses adversaires.
97:4	Ses éclairs illuminent le monde, et la Terre le voit et tremble tout étonnée<!--Job 38:35 ; Ap. 4:5.-->.
97:5	Les montagnes se fondent comme de la cire<!--Mi. 1:4.-->, à cause de la présence de YHWH, à cause de la présence du Seigneur de toute la Terre.
97:6	Les cieux annoncent sa justice, et tous les peuples voient sa gloire.
97:7	Que tous ceux qui servent les images taillées, et qui se glorifient des faux elohîm soient confus<!--De. 4:25-26 ; 1 S. 5:1-5.--> ! Vous elohîm, prosternez-vous tous devant lui !
97:8	Sion l'a entendu, et s'en est réjouie, les filles de Yéhouda se sont égayées pour l'amour de tes jugements, YHWH !
97:9	Car toi, YHWH, tu es Elyon<!--Très-Haut.--> sur toute la Terre, tu es infiniment élevé au-dessus de tous les elohîm.
97:10	Vous qui aimez YHWH, haïssez le mal<!--Am. 5:14-15 ; Ro. 12:9.--> ! Il garde les âmes de ses bien-aimés, et les délivre de la main des méchants<!--Ps. 34:8 ; Jn. 10:28-29.-->.
97:11	La lumière est faite pour le juste<!--Mt. 5:15-16.-->, et la joie pour ceux qui sont droits de cœur.
97:12	Justes, réjouissez-vous en YHWH, et célébrez la mémoire de sa sainteté !

## Chapitre 98

### Invitation à la louange

98:1	Psaume. Chantez à YHWH un cantique nouveau ! Car il a fait des choses merveilleuses. Sa droite et le bras de sa sainteté l'ont délivré<!--Es. 52:10, 53:1, 63:3-5.-->.
98:2	YHWH a fait connaître son salut<!--Il est question de la révélation de Yéhoshoua. Voir commentaire en Es. 26:1.-->, il a révélé sa justice devant les yeux des nations.
98:3	Il s'est souvenu de sa bonté et de sa fidélité envers la maison d'Israël. Toutes les extrémités de la Terre ont vu le salut<!--La translitération du mot hébreu est « Yeshuw`ah ». Voir commentaire en Ge. 49:18.--> de notre Elohîm<!--Es. 49:6 ; Lu. 1:72 ; Ac. 13:47.-->.
98:4	Vous tous, habitants de la Terre, poussez des cris de réjouissance à YHWH ! Faites retentir vos cris, et chantez de joie !
98:5	Chantez à YHWH avec la harpe, avec la harpe et avec une voix mélodieuse !
98:6	Poussez des cris de réjouissance avec le shofar et le son du cor devant le Roi, YHWH !
98:7	Que la mer tonne avec tout ce qu'elle contient, que la Terre et ceux qui y habitent fassent éclater leurs cris !
98:8	Que les fleuves frappent des mains, et que les montagnes chantent de joie
98:9	devant YHWH ! Car il vient pour juger la Terre<!--YHWH, qui vient pour juger la Terre, est Yéhoshoua ha Mashiah (Jésus-Christ) (Za. 14:1-7 ; 2 Ti. 4:1 ; Ap. 19:15).-->. Il jugera le monde avec justice, et les peuples avec équité.

## Chapitre 99

### Grandeur, justice, et sainteté d'Elohîm

99:1	YHWH règne : que les peuples tremblent ! Il est assis entre les chérubins : que la Terre soit ébranlée<!--Ex. 25:22 ; Es. 37:16.--> !
99:2	YHWH est grand en Sion, et il est élevé au-dessus de tous les peuples.
99:3	Qu'on célèbre ton Nom grand et redoutable ! Car il est saint !
99:4	et la force du Roi, car il aime la justice. Tu as ordonné l'équité, tu as prononcé des jugements justes en Yaacov.
99:5	Exaltez YHWH, notre Elohîm, et prosternez-vous devant le marchepied de ses pieds ! Il est saint !
99:6	Moshè et Aaron étaient parmi ses prêtres<!--Ex. 31:10 ; Lé. 2:2.-->, et Shemouél parmi ceux qui invoquaient son Nom, ils invoquaient YHWH et il leur répondait<!--1 S. 12:18-19.-->.
99:7	Il leur parlait de la colonne de nuée, ils ont gardé ses témoignages et l'ordonnance qu'il leur avait donnée.
99:8	YHWH, mon Elohîm ! tu les as exaucés, tu as été pour eux le El qui pardonne<!--Hé. 10:16-17.-->, et qui fait vengeance de leurs actes.
99:9	Exaltez YHWH, notre Elohîm, prosternez-vous sur la montagne de sa sainteté ! Car YHWH, notre Elohîm, est saint !

## Chapitre 100

### Célébrez et bénissez le Nom de YHWH

100:1	Psaume d'actions de grâces. Vous tous habitants de la Terre, poussez des cris de réjouissance à YHWH !
100:2	Servez YHWH avec allégresse, venez devant lui avec un chant de joie !
100:3	Sachez que YHWH est Elohîm ! C'est lui qui nous a faits, ce n'est pas nous qui nous sommes faits : nous sommes son peuple et le troupeau de son pâturage<!--Ps. 79:13, 80:2, 95:6, 119:73.-->.
100:4	Entrez dans ses portes avec des actions de grâces, dans ses parvis avec des cantiques ! Célébrez-le, bénissez son Nom !
100:5	Car YHWH est bon : sa bonté demeure à toujours, et sa fidélité de génération en génération.

## Chapitre 101

### Appel à l'intégrité

101:1	Psaume de David. Je chanterai la miséricorde et la justice. YHWH, je te chanterai !
101:2	Je me rendrai attentif à une conduite pure jusqu'à ce que tu viennes à moi. Je marcherai dans l'intégrité de mon cœur au milieu de ma maison.
101:3	Je ne mettrai pas devant mes yeux des choses de Bélial<!--Voir commentaire en De. 13:13 et Ps. 41:9.-->. Je hais les actions de ceux qui se détournent : elles ne s'attacheront pas à moi !
101:4	Le cœur mauvais s'éloignera de moi, je ne connaîtrai pas le méchant.
101:5	J'exterminerai celui qui calomnie en secret son prochain, je ne supporterai pas celui qui a les yeux élevés et le cœur enflé<!--Pr. 6:16-17.-->.
101:6	J'aurai les yeux sur les fidèles du pays afin qu'ils demeurent avec moi. Celui qui marche dans la voie de l'intégrité, me servira.
101:7	Celui qui usera de tromperie ne demeurera pas dans ma maison. Celui qui proférera des mensonges ne sera pas affermi devant mes yeux.
101:8	J'exterminerai chaque matin tous les méchants du pays, afin d'exterminer de la cité de YHWH tous ceux qui pratiquent la méchanceté.

## Chapitre 102

### YHWH, l'Elohîm immuable

102:1	Prière de l'affligé étant dans l'angoisse et répandant sa plainte devant YHWH.
102:2	YHWH, écoute ma prière, et que mon cri parvienne jusqu'à toi<!--Ps. 69:14.--> !
102:3	Ne me cache pas ta face le jour où je suis dans la détresse, prête l'oreille à ma prière ! Au jour où je t'invoque, hâte-toi de me répondre !
102:4	Car mes jours se sont évanouis comme la fumée et mes os brûlent comme dans un foyer.
102:5	Mon cœur est frappé et se dessèche comme l'herbe, car j'ai oublié de manger mon pain<!--Mt. 4:4 ; Lu. 4:4.-->.
102:6	Le gémissement de ma voix est tel que mes os s'attachent à ma chair<!--Job 19:20.-->.
102:7	Je ressemble au pélican du désert. Je suis devenu comme la chouette des lieux sauvages.
102:8	Je veille, et je suis semblable au passereau solitaire sur le toit.
102:9	Mes ennemis m'outragent tous les jours, et ceux qui sont furieux contre moi, jurent contre moi.
102:10	Car j'ai mangé la cendre comme le pain et j'ai mêlé des larmes à ma boisson,
102:11	à cause de ta colère et de ta fureur, car après m'avoir élevé bien haut, tu m'as jeté par terre.
102:12	Mes jours sont comme l'ombre qui décline, et je deviens sec comme l'herbe.
102:13	Mais toi, YHWH ! tu demeures éternellement, et ta mémoire est de génération en génération.
102:14	Tu te lèveras, et tu auras compassion de Sion car il est temps d'en avoir pitié, parce que le temps assigné est échu.
102:15	Car tes serviteurs aiment ses pierres et chérissent sa poussière.
102:16	Alors les nations redouteront le Nom de YHWH, et tous les rois de la Terre, ta gloire.
102:17	Quand YHWH aura édifié Sion, quand il aura été vu dans sa gloire,
102:18	quand il aura eu égard à la prière de ceux qu'on a dépouillés, et qu'il n'aura pas méprisé leur supplication.
102:19	Cela sera enregistré pour la génération à venir, le peuple qui sera créé louera Yah.
102:20	Car il regarde du lieu élevé de sa sainteté. Du haut des cieux, YHWH regarde la Terre,
102:21	pour entendre le gémissement des prisonniers, pour délier ceux qui étaient voués à la mort<!--Es. 42:6-7, 61:1 ; Lu. 4:18-19.-->,
102:22	afin qu'on annonce le Nom de YHWH dans Sion, et sa louange dans Yeroushalaim,
102:23	quand les peuples seront réunis tous ensemble, et les royaumes aussi, pour servir YHWH.
102:24	Il a abattu ma force en chemin, il a abrégé mes jours.
102:25	J'ai dit : Mon El, ne m'enlève pas au milieu de mes jours, toi dont les années durent d'âge en âge !
102:26	Tu as jadis fondé la Terre, et les cieux sont l'ouvrage de tes mains.
102:27	Ils périront, mais tu subsisteras. Ils s'useront tous comme un vêtement ; tu les changeras comme un habit, et ils seront changés.
102:28	Mais toi, tu es toujours le même<!--Voir Hé. 13:8. Yéhoshoua est l'Elohîm qui ne change pas.-->, et tes années ne seront jamais achevées.
102:29	Les fils de tes serviteurs habiteront près de toi, et leur postérité sera établie devant toi.

## Chapitre 103

### YHWH, l'Elohîm miséricordieux et compatissant

103:1	Psaume de David. Mon âme, bénis YHWH ! Et que tout ce qui est en moi bénisse son saint Nom !
103:2	Mon âme, bénis YHWH, et n'oublie aucun de ses bienfaits<!--De. 6:12.--> !
103:3	C'est lui qui pardonne toutes tes iniquités, qui guérit toutes tes maladies<!--Es. 33:24, 53:5 ; Jé. 17:14 ; Ps. 130:3-4 ; Mt. 9:6 ; Lu. 7:47.--> ;
103:4	qui rachète ta vie de la fosse<!--Es. 59:20 ; Ps. 106:10.-->, qui te couronne de bonté et de compassions.
103:5	C'est lui qui rassasie ta bouche de ce qui est bon, et ta jeunesse est renouvelée comme celle de l'aigle<!--Es. 40:31.-->.
103:6	YHWH fait justice et droit à tous les opprimés<!--Ps. 146:7.-->.
103:7	Il a fait connaître ses voies à Moshè, et ses œuvres aux enfants d'Israël<!--Ex. 33:12-17.-->.
103:8	YHWH est compatissant, miséricordieux, lent à la colère, et riche en bonté.
103:9	Il ne conteste pas éternellement, et il ne garde pas à toujours sa colère<!--Es. 57:16 ; Jé. 3:5 ; Mi. 7:18.-->.
103:10	Il ne nous traite pas selon nos péchés, et ne nous rend pas selon nos iniquités<!--Esd. 9:13.-->.
103:11	Car autant les cieux sont élevés au-dessus de la Terre, autant sa bonté est grande sur ceux qui le craignent.
103:12	Il éloigne de nous nos transgressions, autant que l'orient est éloigné de l'occident<!--Es. 38:17.-->.
103:13	Comme un père a compassion de ses fils, YHWH a compassion de ceux qui le craignent<!--Mal. 3:17 ; Lu. 11:11-13.-->.
103:14	Car il sait bien de quoi nous sommes faits, il se souvient que nous ne sommes que poussière.
103:15	L'homme ! Ses jours sont comme l'herbe<!--Es. 40:6-8 ; Job 14:1-2 ; 1 Pi. 1:24.-->, il fleurit comme la fleur d'un champ.
103:16	Lorsque le vent passe dessus, elle n'est plus, et son lieu ne la reconnaît plus.
103:17	Mais la miséricorde de YHWH dure d'éternité en éternité pour ceux qui le craignent, et sa justice pour les fils de leurs fils ;
103:18	pour ceux qui gardent son alliance, et qui se souviennent de ses préceptes pour les accomplir<!--De. 7:9.-->.
103:19	YHWH a établi son trône dans les cieux, et son règne domine sur tout.
103:20	Bénissez YHWH, vous, ses anges puissants en force, qui accomplissez sa parole, en obéissant à la voix de sa parole !
103:21	Bénissez YHWH, vous toutes ses armées, qui êtes ses serviteurs, qui accomplissez sa volonté !
103:22	Bénissez YHWH, vous toutes ses œuvres, par tous les lieux de sa domination ! Mon âme, bénis YHWH !

## Chapitre 104

### YHWH, l'Elohîm de toute la création

104:1	Mon âme, bénis YHWH ! YHWH, mon Elohîm, tu es merveilleusement grand, tu es revêtu de majesté et de splendeur.
104:2	Il s'enveloppe de lumière comme d'un vêtement. Il étend les cieux comme un voile<!--Es. 40:22 ; Job 9:8 ; 1 Ti. 6:16.-->.
104:3	Avec les eaux, il pose la charpente de sa chambre haute, faisant des nuées<!--Ap. 1:7.--> son char, marchant sur les ailes du vent<!--Es. 19:1 ; Ps. 18:10 ; Ap. 14:14.-->.
104:4	Il fait des vents ses messagers, et des flammes de feu ses serviteurs<!--Ps. 148:8 ; Jn. 3:8 ; Hé. 1:7.-->.
104:5	Il a fondé la Terre sur ses bases, elle ne sera jamais ébranlée<!--Ps. 24:1-2, 78:69, 93:1 ; Job 26:7, 38:4-6.-->.
104:6	Tu l'avais couverte de l'abîme comme d'un vêtement, les eaux se tenaient sur les montagnes<!--Ge. 1:2.-->.
104:7	Elles s'enfuirent à ta menace, et se mirent promptement en fuite au son de ton tonnerre.
104:8	Les montagnes s'élevèrent, et les vallées s'abaissèrent au même lieu que tu leur avais fixé.
104:9	Tu as posé une limite que les eaux ne doivent pas franchir, afin qu'elles ne reviennent plus couvrir la Terre<!--Ge. 1:9 ; Jé. 5:22 ; Pr. 8:29 ; Job 26:10.-->.
104:10	C'est lui qui conduit les sources par les vallées, elles se promènent entre les monts.
104:11	Elles abreuvent toutes les bêtes des champs, les ânes sauvages y étanchent leur soif.
104:12	Les oiseaux des cieux se tiennent auprès d'elles, et font résonner leur voix parmi les rameaux.
104:13	Il abreuve les montagnes de ses chambres hautes. La Terre est rassasiée du fruit de tes œuvres.
104:14	Il fait germer l'herbe pour le bétail, et les plantes pour le besoin de l'homme, faisant sortir le pain de la terre,
104:15	et le vin qui réjouit le cœur de l'homme<!--Jg. 9:13 ; Pr. 31:6-7.-->, qui fait resplendir son visage avec l'huile, et qui soutient le cœur de l'homme avec le pain.
104:16	Les hauts arbres de YHWH en sont rassasiés, ainsi que les cèdres du Liban qu'il a plantés,
104:17	afin que les oiseaux y fassent leurs nids. Quant à la cigogne, les sapins sont sa demeure.
104:18	Les hautes montagnes sont pour les chèvres de montagne, et les rochers sont la retraite des damans<!--Blaireaux des rochers ou lapins.-->.
104:19	Il a fait la lune pour les temps fixés<!--Ge. 1:14.-->, et le soleil sait quand il doit se coucher<!--Ge. 1:16.-->.
104:20	Tu amènes les ténèbres, et c'est la nuit où tous les animaux de la forêt se mettent à grouiller.
104:21	Les lionceaux rugissent après la proie, pour demander à El leur nourriture.
104:22	Le soleil se lève-t-il ? Ils se retirent et se couchent dans leurs tanières.
104:23	L'être humain sort pour son ouvrage, pour son travail jusqu’au soir.
104:24	YHWH, que tes œuvres sont en grand nombre ! Tu les as toutes faites avec sagesse. La Terre est pleine de tes richesses.
104:25	Cette mer, grande et vaste de tous côtés, là où des animaux sans nombre se meuvent, des petites bêtes avec des grandes !
104:26	Là se promènent les navires, et ce léviathan que tu as formé pour y jouer.
104:27	Ils s'attendent tous à toi, afin que tu leur donnes la nourriture en leur temps.
104:28	Quand tu la leur donnes, ils la recueillent, et quand tu ouvres ta main, ils sont rassasiés de biens.
104:29	Caches-tu ta face ? Ils sont troublés. Retires-tu leur esprit ? Ils défaillent et retournent dans leur poussière.
104:30	Tu envoies ton Esprit, ils sont créés, et tu renouvelles la face de la Terre.
104:31	Que la gloire de YHWH subsiste à toujours ! Que YHWH se réjouisse dans ses œuvres !
104:32	Il jette son regard sur la Terre, et elle tremble, il touche les montagnes et elles fument !
104:33	Je chanterai à YHWH durant ma vie, je chanterai à mon Elohîm tant que j'existerai.
104:34	Ma méditation lui sera agréable, et je me réjouirai en YHWH.
104:35	Que les pécheurs soient consumés de dessus la terre et qu'il n'y ait plus de méchants ! Mon âme, bénis YHWH ! Allélou-Yah !

## Chapitre 105

### YHWH, l'Elohîm fidèle

105:1	Célébrez YHWH, invoquez son Nom ! Faites connaître parmi les peuples ses œuvres !
105:2	Chantez-lui, chantez-lui des psaumes, parlez de toutes ses merveilles !
105:3	Glorifiez-vous de son saint Nom, et que le cœur de ceux qui cherchent YHWH se réjouisse !
105:4	Recherchez YHWH et sa puissance, cherchez continuellement sa face !
105:5	Souvenez-vous de ses merveilles qu'il a faites, de ses miracles, et des jugements de sa bouche !
105:6	Postérité d'Abraham, son serviteur, enfants de Yaacov, ses élus !
105:7	Il est YHWH, notre Elohîm, ses jugements sont sur toute la Terre.
105:8	Il s'est souvenu pour toujours de son alliance, de la parole qu'il a ordonnée en mille générations,
105:9	de l’alliance qu’il a traitée avec Abraham, et de son serment à Yitzhak<!--Ge. 17:2, 22:16, 26:3, 28:13, 33:11 ; Lu. 1:73.-->.
105:10	Il l'a établi en ordonnance pour Yaacov, pour Israël en alliance éternelle,
105:11	en disant : Je te donnerai le pays de Canaan, comme héritage qui vous est échu<!--Ge. 13:15, 15:18.-->.
105:12	Ils étaient alors un petit nombre de gens, très peu nombreux, et étrangers dans le pays.
105:13	Car ils allaient de nation en nation, et d'un royaume vers un autre peuple.
105:14	Il ne permit à personne de les opprimer, et il châtia des rois à cause d'eux<!--Ge. 35:5.-->,
105:15	disant : Ne touchez pas à mes mashiah<!--Oints.-->, et ne faites pas de mal à mes prophètes<!--1 Ch. 16:22.--> !
105:16	Il appela aussi la famine sur la Terre, et rompit le bâton du pain<!--Lé. 26:26 ; Es. 3:1 ; Ez. 4:16.-->.
105:17	Il envoya un homme devant eux : Yossef fut vendu comme esclave<!--Ge. 37:28-36.-->.
105:18	On serra ses pieds dans des ceps, sa personne fut mise aux fers,
105:19	jusqu'au temps où vint sa parole, où la parole de YHWH l'éprouva.
105:20	Le roi le relâcha et le laissa aller, le dominateur des peuples le délivra.
105:21	Il l'établit seigneur sur sa maison et gouverneur de tous ses biens<!--Ge. 41:40.-->,
105:22	pour lier ses princes à son âme et pour enseigner la sagesse à ses anciens.
105:23	Puis Israël entra en Égypte, et Yaacov séjourna dans le pays de Cham<!--Ge. 46:6 ; Ps. 78:51.-->.
105:24	YHWH rendit son peuple très fécond et le rendit plus puissant que ceux qui l'opprimaient.
105:25	Il changea leur cœur, de sorte qu'ils se mirent à haïr son peuple et traitèrent ses serviteurs avec perfidie<!--Ex. 1:7-12.-->.
105:26	Il envoya Moshè, son serviteur, et Aaron, qu'il avait élu<!--Ex. 4:14.-->.
105:27	Ils accomplirent au milieu d'eux des prodiges et des miracles qu'ils avaient eu la charge de faire dans le pays de Cham.
105:28	Il envoya les ténèbres et fit venir l'obscurité, et ils ne furent pas rebelles à sa parole.
105:29	Il changea leurs eaux en sang et fit mourir leurs poissons.
105:30	Leur terre grouilla de grenouilles, jusque dans les chambres de leurs rois.
105:31	Il parla, et des mouches vinrent, des poux sur tout leur pays.
105:32	Il leur donna pour pluie de la grêle, et un feu flamboyant sur la Terre.
105:33	Il frappa leurs vignes et leurs figuiers, et il brisa les arbres de leur pays.
105:34	Il ordonna et les sauterelles vinrent, des jeunes sauterelles sans nombre
105:35	qui dévorèrent toute l'herbe du pays, et qui dévorèrent le fruit de leur terroir.
105:36	Il frappa tous les premiers-nés de leur pays, les premiers de toute leur vigueur<!--Lire Ex. 7-12.-->.
105:37	Puis il les fit sortir avec de l'or et de l'argent, et personne ne chancela parmi ses tribus.
105:38	L'Égypte se réjouit à leur départ, car la peur qu'ils avaient d'eux les avait saisis.
105:39	Il étendit la nuée pour couverture, et le feu pour éclairer la nuit.
105:40	Le peuple demanda et il fit venir des cailles, et il les rassasia du pain des cieux<!--Ex. 16:12-13.-->.
105:41	Il ouvrit le rocher et les eaux en coulèrent, elles se répandirent comme un fleuve dans les lieux arides<!--Ex. 17:6.-->.
105:42	Car il se souvint de sa parole sainte qu'il avait donnée à Abraham, son serviteur<!--Ge. 15:13-16.-->.
105:43	Il fit sortir son peuple dans l'allégresse, ses élus au milieu des cris retentissants<!--Ex. 15:1.-->.
105:44	Il leur donna les terres des nations et ils possédèrent le fruit du travail des peuples,
105:45	afin qu'ils gardent ses statuts et qu'ils observent sa torah. Allélou-Yah !

## Chapitre 106

### L'infidélité d'Israël

106:1	Allélou-Yah ! Célébrez YHWH car il est bon, car sa bonté demeure à toujours !
106:2	Qui pourrait réciter les exploits de YHWH ? Qui pourrait faire retentir toute sa louange ?
106:3	Bénis sont ceux qui observent la justice, qui font en tout temps ce qui est juste !
106:4	YHWH, souviens-toi de moi, dans ta faveur pour ton peuple ! Aie soin de moi selon ta délivrance !
106:5	Afin que je voie le bien de tes élus, que je me réjouisse dans la joie de ta nation, que je me glorifie avec ton héritage.
106:6	Nous avons péché avec nos pères, nous avons agi dans l'iniquité, nous avons fait le mal<!--Da. 9:16 ; Esd. 9:7 ; Né. 1:6.-->.
106:7	Nos pères n'ont pas été attentifs à tes merveilles en Égypte. Ils ne se sont pas souvenus de la multitude de tes faveurs ; mais ils furent rebelles près de la mer, vers la Mer Rouge<!--Ex. 14:11.-->.
106:8	Toutefois, il les délivra pour l'amour de son Nom, afin de faire connaître sa puissance.
106:9	Car il menaça la Mer Rouge et elle se dessécha. Il les conduisit à travers les profondeurs de la mer comme dans un désert ;
106:10	il les délivra de la main de ceux qui les haïssaient, et les racheta de la main de l'ennemi.
106:11	Les eaux couvrirent leurs oppresseurs, il n'en resta pas un seul<!--Ex. 14:27.-->.
106:12	Alors ils crurent à ses paroles, et ils chantèrent sa louange.
106:13	Mais ils oublièrent vite ses œuvres, et ne s'attendirent pas à son conseil.
106:14	Ils furent épris de convoitise dans le désert et ils tentèrent El dans le lieu de désolation.
106:15	Alors il leur donna ce qu'ils avaient demandé, toutefois il leur envoya le dépérissement dans leur corps.
106:16	Ils jalousèrent dans le camp Moshè et Aaron, le saint de YHWH.
106:17	La Terre s'ouvrit et engloutit Dathan, elle recouvrit l'assemblée d'Abiram<!--No. 16.-->.
106:18	Le feu s'alluma au milieu de leur assemblée, et la flamme brûla les méchants.
106:19	Ils firent un veau en Horeb, et se prosternèrent devant une image de métal fondu<!--Ex. 32.-->.
106:20	Ils échangèrent leur gloire contre la figure d'un bœuf<!--Voir Ro. 1:22.--> qui mange l'herbe.
106:21	Ils oublièrent El, leur Sauveur, qui avait fait de grandes choses en Égypte,
106:22	des choses merveilleuses dans le pays de Cham, et des choses terribles sur la Mer Rouge.
106:23	C'est pourquoi il dit qu'il les détruirait, mais Moshè, son élu, se tint à la brèche devant lui pour détourner sa fureur, afin qu'il ne les détruisît pas<!--Ex. 32:11.-->.
106:24	Ils méprisèrent le pays désirable, et ne crurent pas à sa parole.
106:25	Et ils murmurèrent dans leurs tentes, et n'obéirent pas à la voix de YHWH.
106:26	C'est pourquoi il leur jura la main levée de les faire tomber dans le désert,
106:27	d'accabler leur postérité parmi les nations, et de les disperser au milieu des pays<!--No. 14:22.-->.
106:28	Ils s'attachèrent à Baal-Peor, et mangèrent les sacrifices des morts.
106:29	Et ils irritèrent Elohîm par leurs actions, au point qu'une plaie fit une brèche parmi eux.
106:30	Mais Phinées se présenta et fit justice, et la plaie fut arrêtée.
106:31	Et cela lui fut imputé à justice de génération en génération, pour toujours<!--No. 25:3-8.-->.
106:32	Ils excitèrent aussi sa colère près des eaux de Meriba, et Moshè fut puni à cause d'eux.
106:33	Car ils résistèrent à son esprit, et il parla avec légèreté de ses lèvres<!--No. 20:12.-->.
106:34	Ils ne détruisirent pas les peuples que YHWH leur avait dit de détruire,
106:35	mais ils se mêlèrent avec ces nations et apprirent leurs œuvres.
106:36	Ils servirent leurs idoles qui furent un piège pour eux.
106:37	Car ils sacrifièrent leurs fils et leurs filles aux démons<!--Lé. 18:21 ; De. 12:31, 32:17 ; 2 R. 16:3 ; Ez. 20:26.-->.
106:38	Ils répandirent le sang innocent, le sang de leurs fils et de leurs filles, qu'ils sacrifièrent aux idoles de Canaan, et le pays fut souillé par le sang<!--No. 35:33.-->.
106:39	Ils se rendirent impurs par leurs œuvres, et se prostituèrent par leurs actions.
106:40	C'est pourquoi la colère de YHWH s'embrasa contre son peuple, et il eut en abomination son héritage.
106:41	Il les livra entre les mains des nations, et ceux qui les haïssaient dominèrent sur eux.
106:42	Leurs ennemis les opprimèrent, et ils furent humiliés sous leur main.
106:43	Il les délivra souvent, mais ils se montrèrent rebelles dans leurs desseins et furent humiliés par leur iniquité.
106:44	Toutefois, il vit leur détresse, lorsqu'il entendit leurs supplications.
106:45	Et il se souvint en leur faveur de son alliance, et se repentit selon la grandeur de ses compassions.
106:46	Et il leur fit trouver compassion auprès de tous ceux qui les avaient emmenés captifs.
106:47	YHWH, notre Elohîm, délivre-nous et rassemble-nous du milieu des nations, afin que nous célébrions ton saint Nom et que nous mettions notre gloire à te louer !
106:48	Béni soit YHWH, l'Elohîm d'Israël, d'éternité en éternité ! Et que tout le peuple dise : Amen ! Allélou-Yah !

## Chapitre 107

### La grâce de YHWH pour ses rachetés

107:1	Célébrez YHWH car il est bon, parce que sa bonté demeure à toujours !
107:2	Qu'ainsi disent les rachetés de YHWH, ceux qu'il a rachetés de la main de l'oppresseur,
107:3	et qu'il a rassemblés de tous les pays, de l'orient et de l'occident, du nord et du sud.
107:4	Ils étaient errants dans le désert, dans un chemin solitaire, et ils ne trouvaient aucune ville habitée.
107:5	Ils étaient affamés et assoiffés, leur âme était languissante.
107:6	Alors ils ont crié vers YHWH dans leur détresse et il les a délivrés de leurs angoisses ;
107:7	il les a conduits sur le droit chemin pour aller dans une ville habitée.
107:8	Qu'ils célèbrent YHWH pour sa bonté et ses merveilles envers les fils d'humains !
107:9	Parce qu'il a désaltéré l'âme altérée, et rassasié de ses biens l'âme affamée<!--Ps. 146:7 ; Lu. 1:53.-->.
107:10	Ceux qui habitaient dans les ténèbres et dans l'ombre de la mort, vivaient captifs dans l'affliction et dans les chaînes,
107:11	parce qu'ils étaient rebelles aux paroles de El, et parce qu'ils avaient rejeté le conseil d'Elyon<!--De. 31:20 ; La. 3:42.-->.
107:12	Il humilia leur cœur par le travail : ils trébuchaient, et personne ne les secourait.
107:13	Alors ils ont crié vers YHWH dans leur détresse, et il les a délivrés de leurs angoisses.
107:14	Il les a fait sortir hors des ténèbres et de l'ombre de la mort, et il a rompu leurs liens<!--Ps. 68:19 ; Ep. 4:8 ; Col. 1:12-13.-->.
107:15	Qu'ils célèbrent YHWH pour sa bonté et ses merveilles envers les fils d'humains !
107:16	Parce qu'il a brisé les portes de cuivre et cassé les barreaux de fer.
107:17	Les fous sont affligés à cause de leur transgression et à cause de leurs iniquités.
107:18	Leur âme avait en horreur toute nourriture, et ils touchaient aux portes de la mort.
107:19	Alors ils ont crié vers YHWH dans leur détresse, et il les a délivrés de leurs angoisses<!--Ps. 50:15 ; Os. 5:15.-->.
107:20	Il a envoyé sa parole et les a guéris, et il les a fait échapper de leurs tombeaux.
107:21	Qu'ils célèbrent YHWH pour sa bonté et ses merveilles envers les fils d'humains !
107:22	Qu'ils offrent des sacrifices d'actions de grâces, et qu'ils racontent ses œuvres avec des cris de joie !
107:23	Ceux qui descendaient sur la mer dans des navires, faisant des affaires sur les grandes eaux,
107:24	ceux-là ont vu les œuvres de YHWH et ses merveilles dans les profondeurs.
107:25	Il parla et fit lever un vent de tempête qui souleva des grosses vagues.
107:26	Ils montaient vers les cieux, ils descendaient dans l'abîme, sous le mal leur âme fondait ;
107:27	saisis de vertiges, ils chancelaient comme un homme ivre et toute leur sagesse était anéantie<!--Es. 51:17-21 ; Jé. 13:13.-->.
107:28	Alors ils ont crié vers YHWH dans leur détresse, et il les a fait sortir de leurs angoisses.
107:29	Il a arrêté la tempête, ramené le calme, et les vagues se sont tues.
107:30	Puis ils se sont réjouis de ce qu'elles s'étaient apaisées, et il les a conduits au port qu'ils désiraient.
107:31	Qu'ils célèbrent YHWH pour sa bonté et ses merveilles envers les fils d'humains !
107:32	Et qu'ils l'exaltent dans l'assemblée du peuple, et le louent dans l'assemblée des anciens !
107:33	Il transforme les fleuves en désert et les sources d'eaux en sol assoiffé,
107:34	la terre fertile en terre salée, à cause de la méchanceté de ses habitants<!--Jé. 12:4, 17:6.-->.
107:35	Il transforme le désert en étangs d'eaux, et la terre sèche en des sources d'eaux<!--Es. 41:18.-->.
107:36	Il y établit ceux qui sont affamés, ils bâtissent des villes pour l'habiter.
107:37	Ils ensemencent des champs, plantent des vignes, ils en récoltent les fruits.
107:38	Il les bénit et ils se multiplient extrêmement, et il ne laisse pas diminuer leur bétail.
107:39	Puis ils sont amoindris et humiliés par l'oppression, le malheur et la souffrance.
107:40	Il répand le mépris sur les princes et les fait errer dans des lieux déserts sans chemin.
107:41	Mais il relève le pauvre et le délivre de la misère, il établit les familles comme des troupeaux<!--1 S. 2:8 ; Ps. 113:7.-->.
107:42	Les hommes droits le voient et se réjouissent, mais toute injustice a la bouche fermée.
107:43	Que celui qui est sage prenne garde à ces choses, et considère les bontés de YHWH.

## Chapitre 108

### YHWH, le secours

108:1	Cantique. Psaume de David. Mon cœur est ferme, Elohîm ! Ma gloire l'est aussi, je chanterai et je jouerai des psaumes !
108:2	Réveillez-vous, mon luth et ma harpe ! Je me réveillerai à l'aube du jour.
108:3	YHWH, je te célébrerai parmi les peuples et je te chanterai parmi les nations.
108:4	Car ta bonté est grande par-dessus les cieux, et ta vérité atteint jusqu'aux nuages.
108:5	Elohîm ! élève-toi sur les nuages, et que ta gloire soit sur toute la Terre !
108:6	Afin que ceux que tu aimes soient délivrés. Sauve-moi par ta droite et exauce-moi !
108:7	Elohîm a dit dans sa sainteté : Je me réjouirai, je partagerai Sichem et mesurerai la vallée de Soukkoth.
108:8	Galaad sera à moi, Menashè sera à moi, et Éphraïm sera le sommet de ma forteresse, Yéhouda, mon législateur.
108:9	Moab sera le bassin où je me laverai, je jetterai ma sandale sur Édom, je triompherai des Philistins !
108:10	Qui me conduira dans la ville forte ? Qui me conduira jusqu'en Édom ?
108:11	N'est-ce pas toi, Elohîm, qui nous avais rejetés, et qui ne sortais plus, Elohîm, avec nos armées ?
108:12	Donne-nous du secours contre la détresse ! Car le salut de l'humain est vain.
108:13	Avec Elohîm, nous agirons avec force, et c'est lui qui foule aux pieds nos ennemis<!--Ps. 60:5-14.-->.

## Chapitre 109

### La méchanceté de l'homme

109:1	Psaume de David. Au chef de musique<!--Les Psaumes d'imprécations (Ps. chapitres 35, 52, 55, 58, 59, 79, 109 et 137) sont des demandes faites à Elohîm pour qu'il punisse les méchants. Le Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) nous demande aujourd'hui de bénir nos ennemis (Lu. 6:27-37).-->. Elohîm de ma louange, ne te tais pas !
109:2	Car la bouche du méchant et la bouche remplie de fraude se sont ouvertes contre moi, ils parlent contre moi avec une langue mensongère,
109:3	ils m'entourent de paroles pleines de haine et ils me font la guerre sans cause !
109:4	En échange de mon amour, ils sont mes adversaires, mais moi, je m'adonne à la prière.
109:5	Ils me rendent le mal pour le bien, et la haine pour l'amour que je leur porte.
109:6	Établis le méchant sur lui, et que Satan se tienne à sa droite !
109:7	Quand il sera jugé, fais qu'il soit déclaré méchant, et que sa prière soit regardée comme un péché !
109:8	Que sa vie soit courte, et qu'un autre prenne sa fonction<!--Ce passage fait allusion à Yéhouda (Judas) Iscariot.--> !
109:9	Que ses enfants soient orphelins, et sa femme veuve !
109:10	Que ses enfants soient errants, qu'ils soient errants et mendiants, qu'ils aillent consulter loin de leur maison en ruine<!--Job 20:10.--> !
109:11	Que le créancier usant d'exaction jette le filet sur tout ce qui est à lui, et que les étrangers butinent tout son travail !
109:12	Que personne n'étende sa compassion sur lui, et que personne n'ait pitié de ses orphelins !
109:13	Que sa postérité soit retranchée, que leur nom soit effacé dans la génération qui le suivra !
109:14	Que l'iniquité de ses pères revienne en mémoire à YHWH, et que le péché de sa mère ne soit pas effacé !
109:15	Qu'ils soient continuellement devant YHWH, et qu'il retranche leur mémoire de la Terre<!--Ps. 34:17.-->,
109:16	parce qu'il ne s'est pas souvenu d'user de miséricorde, mais il a persécuté l'homme affligé et misérable, dont le cœur est brisé, et cela pour le faire mourir !
109:17	Puisqu'il aime la malédiction, que la malédiction tombe sur lui ! Puisqu'il ne prend pas plaisir à la bénédiction, que la bénédiction aussi s'éloigne de lui !
109:18	Et qu'il soit revêtu de la malédiction comme de sa robe, qu'elle entre dans son corps comme de l'eau, et dans ses os comme de l'huile !
109:19	Qu'elle lui soit comme un vêtement dont il se couvre, et comme une ceinture dont il se ceigne continuellement !
109:20	Tel sera, de la part de YHWH, le salaire de mes adversaires, et de ceux qui parlent mal de moi !
109:21	Mais toi, YHWH Adonaï, agis avec moi pour l'amour de ton Nom ! Et parce que ta miséricorde est grande, délivre-moi !
109:22	Car je suis affligé et misérable, et mon cœur est blessé au-dedans de moi.
109:23	Je m'en vais comme l'ombre quand elle décline, et je suis chassé comme une sauterelle.
109:24	Mes genoux sont affaiblis par le jeûne, et mon corps est épuisé de maigreur au lieu d'être gras.
109:25	Je suis pour eux un objet d'opprobre. Quand ils me voient, ils secouent la tête.
109:26	YHWH, mon Elohîm ! aide-moi, délivre-moi selon ta miséricorde.
109:27	Afin qu'on sache que c'est ta main, que c'est toi, YHWH ! qui l'as fait.
109:28	Ils maudiront, mais tu béniras. Ils s'élèveront, mais ils seront confus, et ton serviteur se réjouira.
109:29	Que mes adversaires soient revêtus de confusion, et couverts de leur honte comme d'une robe !
109:30	Je célébrerai hautement de ma bouche YHWH, et je le louerai au milieu de plusieurs nations,
109:31	de ce qu'il se tient à la droite du misérable pour le délivrer de ceux qui condamnent son âme.

## Chapitre 110

### YHWH, le Roi et le prêtre

110:1	Psaume de David. YHWH a dit à mon Seigneur : Assieds-toi à ma droite, jusqu'à ce que je fasse de tes ennemis le marchepied de tes pieds<!--Ce psaume affirme la divinité de Yéhoshoua ha Mashiah (Jésus-Christ) (Mt. 22:41-46 ; Mc. 12:35-37 ; Lu. 20:41-44 ; Ac. 2:34-35 ; Hé. 1:13, 10:12-13).-->.
110:2	YHWH étendra de Sion le sceptre de ta puissance : domine au milieu de tes ennemis<!--Es. 2:2-3 ; Da. 7:14.--> !
110:3	Ton peuple est plein d'ardeur quand tu rassembles ton armée. Avec des ornements sacrés, du sein de l'aurore, ta jeunesse vient à toi comme une rosée.
110:4	YHWH l'a juré et il ne s'en repentira pas : Tu es prêtre éternellement, à la manière de Melchisédek<!--Ge. 14:18 ; Hé. 5:6, 6:20, 7:17.-->.
110:5	Adonaï est à ta droite, il brisera les rois au jour de sa colère.
110:6	Il exercera le jugement sur les nations, il les remplira de cadavres, il brisera le chef d'une vaste terre<!--Ap. 14 et 16.-->.
110:7	Il boit au torrent pendant la marche : c'est pourquoi il lève haut la tête.

## Chapitre 111

### Les œuvres magnifiques d'Elohîm

111:1	Allélou-Yah. [Aleph.] Je célébrerai YHWH de tout mon cœur, [Beth.] dans le conseil secret des hommes droits et dans l'assemblée.
111:2	[Guimel.] Les œuvres de YHWH sont grandes, [Daleth.] elles sont recherchées par tous ceux qui y prennent plaisir.
111:3	[He.] Son œuvre n'est que majesté et magnificence, [Vav.] et sa justice demeure à perpétuité.
111:4	[Zayin.] Il a fait un mémorial pour ses merveilles. [Heth.] YHWH est miséricordieux et compatissant.
111:5	[Teth.] Il a donné de la nourriture à ceux qui le craignent, [Yod.] il s'est souvenu pour toujours de son alliance.
111:6	[Kaf.] Il a manifesté à son peuple la puissance de ses œuvres, [Lamed.] en leur donnant l'héritage des nations.
111:7	[Mem.] Les œuvres de ses mains ne sont que vérité et équité. [Noun.] Tous ses préceptes sont véritables,
111:8	[Samech.] appuyés à perpétuité, éternellement, [Ayin.] faits avec fidélité et droiture.
111:9	[Pe.] Il a envoyé la rédemption à son peuple<!--Ex. 6:6 ; Jn. 3:16.-->, [Tsade.] il lui a donné une alliance éternelle ; [Qof.] son Nom est saint et redoutable.
111:10	[Resh.] Ce qu'il y a de capital dans la sagesse c'est la crainte de YHWH : [Shin.] tous ceux qui s'adonnent à faire ce qu'elle prescrit sont bien sages<!--De. 4:6 ; Pr. 1:7, 9:10, 8:13.-->. [Tav.] Sa louange demeure à perpétuité.

## Chapitre 112

### La crainte de YHWH enrichit et donne de l'assurance

112:1	Allélou-Yah ! [Aleph.] Béni est l'homme qui craint YHWH [Beth.] et qui prend un grand plaisir à ses commandements !
112:2	[Guimel.] Sa postérité sera puissante sur la Terre, [Daleth.] la génération des hommes droits sera bénie<!--Pr. 20:7.-->.
112:3	[He.] Il y aura des biens et des richesses dans sa maison, [Vav.] et sa justice demeure à perpétuité.
112:4	[Zayin.] La lumière s'est levée dans les ténèbres sur ceux qui sont justes<!--Ps. 37:6 ; Pr. 4:18.-->, [Heth.] il est compatissant, miséricordieux et juste.
112:5	[Teth.] Il est bon l'homme qui exerce la miséricorde et qui prête, [Yod.] qui règle ses actions avec justice !
112:6	[Kaf.] Il ne chancelle jamais. [Lamed.] La mémoire du juste dure toujours<!--Pr. 10:7.-->.
112:7	[Mem.] Il ne craint pas les mauvaises nouvelles, [Noun.] son cœur est ferme, confiant en YHWH.
112:8	[Samech.] Son cœur est bien affermi, il ne craint pas, [Ayin.] jusqu'à ce qu'il mette son plaisir à regarder ses adversaires.
112:9	[Pe.] Il a répandu, il a donné aux pauvres, [Tsade.] sa justice demeure à perpétuité<!--Voir 2 Cor. 9:9.-->, [Qof.] sa corne s'élève en gloire.
112:10	[Resh.] Le méchant le voit et s'irrite, [Shin.] il grince des dents et fond. [Tav.] Les désirs des méchants périssent.

## Chapitre 113

### YHWH, l'Elohîm élevé au-dessus de tout

113:1	Allélou-Yah<!--Les psaumes disant « Allélou-Yah » sont les Ps. 104 à 106, 111 à 113, 115 à 117, 135 à 136, 146 à 150. Parmi eux, les Ps. 135 et 146 à 150, étaient chantés durant le service quotidien d'adoration dans la synagogue. Les psaumes 115 à 118, appelés « le grand Hallel », étaient chantés lors des fêtes de Pâque. Allélou-Yah veut dire « Louez Yah » (Ap. 19:1).--> ! Louez, vous serviteurs de YHWH, louez le Nom de YHWH !
113:2	Que le Nom de YHWH soit béni dès maintenant et à toujours !
113:3	Le Nom de YHWH est digne de louanges depuis le soleil levant jusqu'au soleil couchant.
113:4	YHWH est élevé par-dessus toutes les nations, sa gloire est au-dessus des cieux.
113:5	Qui est semblable à YHWH notre Elohîm, qui habite dans les lieux très hauts ?
113:6	Il s'abaisse pour regarder dans les cieux et sur la Terre.
113:7	Il relève le pauvre de la poussière, et retire l'indigent<!--1 S. 2:8 ; Ps. 107:41.--> du fumier,
113:8	pour les faire asseoir avec les nobles, avec les nobles de son peuple<!--Job 36:7.-->.
113:9	Il donne une maison à la femme stérile, il en fait une mère joyeuse au milieu de ses enfants<!--Ge. 17:17-21 ; 1 S. 2:5 ; Ps. 68:6.-->. Allélou-Yah !

## Chapitre 114

### La création tremble devant le Tout-Puissant

114:1	Quand Israël sortit d'Égypte, quand la maison de Yaacov s'éloigna d'un peuple barbare,
114:2	Yéhouda devint son lieu saint, Israël son domaine<!--Jé. 2:2-3.-->.
114:3	La mer le vit et s'enfuit, le Yarden retourna en arrière<!--Jos. 3:13-16 ; Ps. 77:17.-->.
114:4	Les montagnes sautèrent comme des béliers, les collines comme des jeunes moutons<!--Jg. 5:5 ; Ha. 3:10 ; Ps. 68:9.-->.
114:5	Mer ! qu'avais-tu pour t'enfuir ? Et toi, Yarden, pour retourner en arrière ?
114:6	Et vous montagnes, pour sauter comme des béliers ? Et vous collines, comme des agneaux ?
114:7	Terre ! tremble devant la présence du Seigneur, devant la face de l'Éloah de Yaacov,
114:8	qui a changé le rocher en un étang d'eaux, la pierre très dure en une source d'eaux.

## Chapitre 115

### Louange à Elohîm de gloire

115:1	Non pas à nous, YHWH ! non pas à nous, mais à ton Nom donne gloire, à cause de ta bonté, à cause de ta fidélité !
115:2	Pourquoi les nations diraient-elles : Où est maintenant leur Elohîm ?
115:3	Notre Elohîm est au ciel, il fait tout ce qu'il veut<!--Ps. 135:6 ; Job 23:13.-->.
115:4	Leurs idoles sont de l'argent et de l'or, ouvrage de mains humaines.
115:5	Elles ont une bouche et ne parlent pas, elles ont des yeux et ne voient pas,
115:6	elles ont des oreilles et n'entendent pas, elles ont un nez et ne sentent pas,
115:7	elles ont des mains et elles ne touchent pas, elles ont des pieds et elles ne marchent pas, et elles ne rendent aucun son de leur gosier<!--Ex. 32:2-8 ; 1 R. 18:25-26 ; Es. 44:9 ; Ez. 8:8-12.-->.
115:8	Ils leur ressemblent, ceux qui les fabriquent, tous ceux qui se confient en elles.
115:9	Israël, confie-toi en YHWH ! Il est le secours et le bouclier de ceux qui se confient en lui.
115:10	Maison d'Aaron, confie-toi en YHWH ! Il est leur secours et leur bouclier.
115:11	Vous qui craignez YHWH, confiez-vous en YHWH ! Il est leur secours et leur bouclier.
115:12	YHWH s'est souvenu de nous, il bénira, il bénira la maison d'Israël, il bénira la maison d'Aaron.
115:13	Il bénira ceux qui craignent YHWH, tant les petits que les grands.
115:14	YHWH vous multipliera ses bénédictions, à vous et à vos fils.
115:15	Vous êtes bénis de YHWH, qui a fait les cieux et la Terre.
115:16	Les cieux sont les cieux de YHWH, mais il a donné la Terre aux enfants d'Adam.
115:17	Ce ne sont pas les morts qui célèbrent Yah, ce n'est aucun de ceux qui descendent dans le lieu du silence<!--Es. 38:18-19 ; Ps. 6:6, 88:11.-->.
115:18	Mais nous, nous bénirons Yah dès maintenant et pour toujours. Allélou-Yah !

## Chapitre 116

### Psaume des rachetés

116:1	J'aime YHWH, car il a entendu ma voix et mes supplications ;
116:2	car il a incliné son oreille vers moi, c'est pourquoi je l'invoquerai durant mes jours.
116:3	Les liens de la mort m'avaient environné, et les angoisses du shéol m'avaient trouvé<!--2 S. 22:5 ; Ps. 18:5.--> ; j'avais trouvé la détresse et la douleur.
116:4	Mais j'invoquai le Nom de YHWH en disant : Oh ! Je te prie YHWH, délivre mon âme !
116:5	YHWH est compatissant et juste, et notre Elohîm fait miséricorde.
116:6	YHWH garde les stupides ; j'étais devenu misérable et il m'a sauvé.
116:7	Mon âme, retourne dans ton repos, car YHWH t'a fait du bien.
116:8	Car tu as retiré mon âme de la mort, mes yeux des larmes et mes pieds de la chute.
116:9	Je marcherai dans la présence de YHWH, sur la Terre des vivants.
116:10	J'ai cru, c'est pourquoi j'ai parlé<!--2 Co. 4:13.-->. J'ai été extrêmement affligé.
116:11	Je disais dans ma précipitation : Tout homme est menteur<!--Ro. 3:4.--> !
116:12	Que rendrai-je à YHWH pour tous ses bienfaits envers moi ?
116:13	J'élèverai la coupe des délivrances et j'invoquerai le Nom de YHWH.
116:14	J'accomplirai maintenant mes vœux envers YHWH, devant tout son peuple.
116:15	La mort des bien-aimés de YHWH est précieuse à ses yeux.
116:16	Oh ! Je te prie YHWH ! car je suis ton serviteur, je suis ton serviteur, fils de ta servante. Tu as délié mes liens.
116:17	Je t'offrirai le sacrifice d'actions de grâces et j'invoquerai le Nom de YHWH.
116:18	J'accomplirai maintenant mes vœux envers YHWH, devant tout son peuple,
116:19	dans les parvis de la maison de YHWH, au milieu de toi, Yeroushalaim ! Allélou-Yah !

## Chapitre 117

### Toutes les nations louent YHWH

117:1	Toutes les nations, louez YHWH ! Tous les peuples, célébrez-le !
117:2	Car sa miséricorde est grande envers nous, et sa fidélité dure à toujours. Louez Yah !

## Chapitre 118

### YHWH, l'Elohîm de mon secours

118:1	Célébrez YHWH, car il est bon, parce que sa bonté dure à toujours !
118:2	Qu'Israël dise maintenant : Car sa bonté dure à toujours !
118:3	Que la maison d'Aaron dise maintenant : Car sa bonté dure à toujours !
118:4	Que ceux qui craignent YHWH disent maintenant : Car sa bonté dure à toujours !
118:5	Dans ma détresse j'ai invoqué Yah<!--Ps. 120:1.--> : Yah m'a répondu et m'a mis au large.
118:6	YHWH est pour moi, je n'aurai pas peur. Que peut me faire un être humain<!--Hé. 13:6.--> ?
118:7	YHWH est pour moi, il vient à mon secours. C'est pourquoi je regarde fixement ceux qui me haïssent.
118:8	Mieux vaut mettre sa confiance en YHWH que de se confier aux humains<!--Es. 2:22 ; Jé. 17:5 ; Ps. 62:9.-->.
118:9	Mieux vaut mettre sa confiance en YHWH que de se confier aux nobles.
118:10	Toutes les nations m'avaient environné, mais au Nom de YHWH je les taille en pièces.
118:11	Elles m'avaient environné, elles m'avaient environné, mais au Nom de YHWH je les taille en pièces.
118:12	Elles m'avaient environné comme des abeilles, elles s'éteignent comme un feu d'épines<!--De. 1:44.-->, car au Nom de YHWH je les taille en pièces.
118:13	Tu me poussais, tu me poussais pour me faire tomber, mais YHWH m'a secouru.
118:14	Yah est ma force et le sujet de mes louanges, et il a été ma délivrance<!--Ex. 15:2 ; Es. 12:2.-->.
118:15	Une voix de chant de triomphe et de délivrance retentit dans les tentes des justes : la droite de YHWH agit avec puissance !
118:16	La droite de YHWH est élevée ! La droite de YHWH agit avec puissance !
118:17	Je ne mourrai pas, je vivrai et je raconterai les œuvres de Yah.
118:18	Yah m'a châtié, il m'a châtié mais il ne m'a pas livré à la mort.
118:19	Ouvrez-moi les portes de la justice, j'y entrerai et je célébrerai Yah !
118:20	C'est ici la porte de YHWH, les justes y entreront.
118:21	Je te célébrerai parce que tu m'as exaucé et tu as été mon libérateur.
118:22	La Pierre que les bâtisseurs avaient rejetée est devenue la principale de l'angle<!--Le Mashiah est présenté comme la pierre ou le rocher (cp. Es. 8:13-17 ; 1 Pi. 2:7).-->.
118:23	Ceci a été fait par YHWH, c'est une chose merveilleuse à nos yeux.
118:24	C'est le jour que YHWH a fait, nous nous réjouirons et nous serons dans l'allégresse en lui.
118:25	Oh ! Je te prie YHWH, délivre maintenant ! YHWH, je te prie, donne maintenant la prospérité !
118:26	Béni soit celui qui vient<!--Mt. 11:3, 21:9, 23:39 ; Lu. 13:35 ; Jn. 12:13.--> au Nom de YHWH ! Nous vous bénissons de la maison de YHWH.
118:27	YHWH El est notre lumière<!--Jn. 8:12.--> ! Attachez la victime de la fête avec des cordes, jusqu'aux cornes de l'autel !
118:28	Tu es mon El, c'est pourquoi je te célébrerai. Tu es mon Elohîm, je t'exalterai.
118:29	Célébrez YHWH car il est bon, car sa bonté est éternelle !

## Chapitre 119

### La parole de YHWH éclaire

119:1	[Aleph.] Bénis sont ceux qui sont intègres dans leur voie, qui marchent selon la torah de YHWH.
119:2	Bénis sont ceux qui gardent ses témoignages et qui le cherchent de tout leur cœur<!--Jos. 1:8.--> ;
119:3	qui ne commettent pas d'injustice, qui marchent dans ses voies<!--1 Jn. 3:9, 5:18.-->.
119:4	Tu as donné tes préceptes afin qu'on les garde soigneusement.
119:5	Oh ! Que mes voies soient bien établies pour garder tes statuts !
119:6	Et je ne rougirai pas de honte quand je regarderai à tous tes commandements.
119:7	Je te célébrerai avec droiture de cœur quand j'aurai appris les ordonnances de ta justice.
119:8	Je veux garder tes statuts, ne m'abandonne pas entièrement !
119:9	[Beth.] Par quel moyen le jeune homme rendra-t-il pure sa voie ? En observant ta parole.
119:10	Je te recherche de tout mon cœur, ne me laisse pas m'égarer loin de tes commandements !
119:11	Je garde ta parole cachée dans mon cœur afin de ne pas pécher contre toi.
119:12	YHWH ! tu es béni, enseigne-moi tes statuts !
119:13	De mes lèvres je raconte toutes les ordonnances de ta bouche.
119:14	Je me réjouis dans le chemin de tes préceptes comme si je possédais toutes les richesses du monde.
119:15	Je médite tes préceptes et j'observe tes voies.
119:16	Je prends plaisir à tes statuts et je n'oublie pas tes paroles.
119:17	[Guimel.] Fais du bien à ton serviteur afin que je vive, et je garderai ta parole<!--Ps. 116:7.-->.
119:18	Ouvre mes yeux afin que je regarde aux merveilles de ta torah<!--Ep. 1:18.--> !
119:19	Je suis un étranger sur la Terre, ne me cache pas tes commandements.
119:20	Mon âme est brisée par le désir qui me porte toujours vers tes ordonnances.
119:21	Tu réprimandes les orgueilleux, ces maudits, qui se détournent de tes commandements.
119:22	Décharge-moi de l'opprobre et du mépris, car j'ai gardé tes témoignages<!--Ps. 3:9.-->.
119:23	Même les princes s'assoient et parlent contre moi pendant que ton serviteur médite tes statuts.
119:24	Tes témoignages font mes délices, ce sont les hommes de mon conseil.
119:25	[Daleth.] Mon âme est attachée à la poussière, fais-moi revivre selon ta parole<!--Ps. 44:26, 143:11.-->.
119:26	Je te raconte mes voies et tu me réponds. Enseigne-moi tes statuts !
119:27	Fais-moi entendre la voie de tes préceptes, et je parlerai de tes merveilles<!--Ps. 145:6.-->.
119:28	Mon âme pleure de chagrin, relève-moi selon tes paroles.
119:29	Éloigne de moi la voie du mensonge et accorde-moi la grâce d'observer ta torah.
119:30	Je choisis la voie de la vérité et je place tes ordonnances sous mes yeux.
119:31	Je m'attache à tes préceptes, YHWH ! Ne me rends pas honteux !
119:32	Je courrai dans la voie de tes commandements, quand tu auras mis mon cœur au large.
119:33	[He.] YHWH, enseigne-moi la voie de tes statuts, et je la garderai jusqu'au bout.
119:34	Donne-moi de l'intelligence, je garderai ta torah et je l'observerai de tout mon cœur<!--Pr. 2:6 ; Ja. 1:5.--> !
119:35	Fais-moi marcher sur le sentier de tes commandements car j'y prends plaisir.
119:36	Incline mon cœur à tes préceptes et non pas au profit<!--Ez. 33:31 ; Mc. 7:21-22 ; Hé. 13:5.-->.
119:37	Détourne mes yeux de la vue des choses vaines, fais-moi vivre dans ta voie !
119:38	Accomplis ta parole envers ton serviteur, afin qu'on te craigne.
119:39	Éloigne de moi l'opprobre que je redoute, car tes ordonnances sont bonnes.
119:40	Voici, je désire pratiquer tes préceptes, fais-moi vivre dans ta justice.
119:41	[Vav.] Que ta miséricorde vienne sur moi, YHWH ! et ta délivrance aussi, selon ta promesse !
119:42	Et je pourrai répondre à celui qui m'outrage, car je me confie en ta parole.
119:43	N'arrache pas de ma bouche la parole de vérité, car j'espère en tes jugements.
119:44	Je garderai continuellement ta torah, à toujours et à perpétuité.
119:45	Je marcherai au large parce que je recherche tes préceptes.
119:46	Je parlerai de tes témoignages devant les rois et je ne rougirai pas de honte<!--Ps. 138:1-4 ; Mt. 10:18-19 ; Ac. 26.-->.
119:47	Je fais mes délices de tes commandements que j'aime.
119:48	J'étends mes mains vers tes commandements que j'aime, et je médite tes statuts.
119:49	[Zayin.] Souviens-toi de la parole donnée à ton serviteur, sur laquelle tu m'as fait espérer.
119:50	C'est ici ma consolation dans mon affliction, car ta parole me rend la vie.
119:51	Les orgueilleux se sont fort moqués de moi, mais je ne me suis pas détourné de ta torah.
119:52	YHWH, je me souviens de tes jugements anciens et je me suis consolé en eux.
119:53	L'horreur me saisit à cause des méchants qui abandonnent ta torah.
119:54	Tes statuts sont le sujet de mes cantiques dans la maison où je suis étranger.
119:55	YHWH, je me souviens de ton Nom pendant la nuit et je garde ta torah.
119:56	Cela m'arrive parce que je garde tes préceptes.
119:57	[Heth.] YHWH ! J'en conclus que ma part est de garder tes paroles.
119:58	Je supplie ta face de tout mon cœur : Aie pitié de moi selon ta parole.
119:59	Je fais le compte de mes voies et je rebrousse chemin vers tes témoignages<!--Os. 6:3 ; La. 3:40.-->.
119:60	Je me hâte, je ne diffère pas de garder tes commandements.
119:61	Une compagnie de méchants me pille, mais je n'oublie pas ta torah.
119:62	Je me lève au milieu de la nuit pour te célébrer à cause des ordonnances de ta justice.
119:63	Je suis l'ami de tous ceux qui te craignent et qui gardent tes préceptes.
119:64	YHWH, la Terre est pleine de ta bonté, enseigne-moi tes statuts.
119:65	[Teth.] YHWH, tu fais du bien à ton serviteur selon ta parole.
119:66	Enseigne-moi le bon sens et la connaissance car je crois à tes commandements.
119:67	Avant d'avoir été humilié, je m'égarais, mais maintenant j'observe ta parole.
119:68	Tu es bon et bienfaisant, enseigne-moi tes statuts.
119:69	Les orgueilleux imaginent des faussetés contre moi, mais je garde de tout mon cœur tes préceptes.
119:70	Leur cœur est insensible comme la graisse, mais moi, je prends plaisir dans ta torah<!--De. 32:15 ; Jé. 5:28.-->.
119:71	Il est bon que je sois humilié afin que j'apprenne tes statuts.
119:72	La torah que tu as prononcée de ta bouche m'est plus précieuse que mille pièces d'or ou d'argent<!--Ps. 19:10-11 ; Job 22:2.-->.
119:73	[Yod.] Tes mains m'ont façonné, elles m'ont formé<!--Jé. 1:5 ; Job 10:9.-->. Donne-moi l'intelligence afin que j'apprenne tes commandements.
119:74	Ceux qui te craignent me verront et se réjouiront, parce que j'espère en tes promesses.
119:75	Je reconnais, YHWH, que tes jugements sont justes, et que tu m'as humilié par ta fidélité<!--Hé. 12:10.-->.
119:76	Que ta bonté soit ma consolation, comme tu l'as promis à ton serviteur !
119:77	Que tes compassions viennent sur moi et je vivrai, car ta torah fait mes délices !
119:78	Que les orgueilleux rougissent de honte, de ce qu'ils m'oppriment sans cause ! Mais moi, je médite sur tes préceptes.
119:79	Que ceux qui te craignent et ceux qui connaissent tes témoignages reviennent vers moi !
119:80	Que mon cœur soit intègre dans tes statuts afin que je ne sois pas couvert de honte !
119:81	[Kaf.] Mon âme se consume en attendant ta délivrance ; j'espère en ta promesse.
119:82	Mes yeux s'épuisent en attendant ta promesse, lorsque je dis : Quand me consoleras-tu ?
119:83	Car je suis comme une outre dans la fumée, je n'oublie pas tes statuts.
119:84	Quel est le nombre de jours de ton serviteur ? Quand jugeras-tu ceux qui me poursuivent<!--Ap. 6:10.--> ?
119:85	Les orgueilleux me creusent des fosses, ils n'agissent pas selon ta torah.
119:86	Tous tes commandements ne sont que fidélité. On me persécute sans cause, aide-moi<!--Mt. 5:10.--> !
119:87	Encore un peu, ils m'auraient exterminé sur la Terre, mais je n'ai pas abandonné tes préceptes.
119:88	Fais-moi revivre selon ta miséricorde et je garderai les préceptes de ta bouche.
119:89	[Lamed.] YHWH ! ta parole subsiste à toujours dans les cieux.
119:90	Ta fidélité dure d'âge en âge : tu as établi la Terre, et elle demeure ferme<!--Ec. 1:4.-->.
119:91	Ces choses subsistent aujourd'hui selon tes ordonnances, car toutes choses te servent.
119:92	Si ta torah n'avait pas fait mes délices, j'aurais déjà péri dans mon affliction.
119:93	Je n'oublierai jamais tes préceptes car c'est par eux que tu m'as fait revivre.
119:94	Je suis à toi : sauve-moi ! Car je recherche tes préceptes.
119:95	Les méchants m'attendent pour me faire périr, mais je suis attentif à tes témoignages.
119:96	J'ai vu une fin à toute perfection, mais ton commandement est extrêmement vaste.
119:97	[Mem.] Combien j'aime ta torah<!--Ps. 1:2.--> ! Elle est tout le jour l'objet de ma méditation.
119:98	Par tes commandements, tu m'as rendu plus sage que mes ennemis, parce que tes commandements sont toujours avec moi.
119:99	J'ai surpassé en prudence tous ceux qui m'avaient enseigné parce que tes préceptes sont l'objet de ma méditation.
119:100	Je suis devenu plus intelligent que les vieillards parce que j'observe tes préceptes.
119:101	Je garde mes pieds de toute mauvaise voie afin d'observer ta parole.
119:102	Je ne me suis pas détourné de tes ordonnances parce que tu me les enseignes.
119:103	Que ta parole est douce à mon palais ! plus douce que le miel à ma bouche.
119:104	Je suis devenu intelligent par tes préceptes, c'est pourquoi je hais toute voie de mensonge.
119:105	[Noun.] Ta parole est une lampe à mes pieds et une lumière sur mon sentier<!--Pr. 6:23 ; 2 Pi. 1:19.-->.
119:106	J'ai juré et je l'accomplirai : j'observerai les lois de ta justice<!--Né. 10:29.-->.
119:107	YHWH, je suis extrêmement affligé, fais-moi revivre selon ta parole.
119:108	YHWH, je te prie, agrée les offrandes volontaires de ma bouche, et enseigne-moi tes ordonnances<!--Os. 14:2 ; Hé. 13:15.-->.
119:109	Ma vie est continuellement en danger, toutefois je n'oublie pas ta torah.
119:110	Les méchants m'ont tendu des pièges, toutefois je ne me suis pas égaré de tes préceptes.
119:111	J'ai pris pour héritage perpétuel tes préceptes car ils sont la joie de mon cœur.
119:112	J'ai incliné mon cœur à accomplir toujours tes statuts jusqu'au bout.
119:113	[Samech.] Je hais les hommes indécis<!--1 R. 18:21 ; Ja. 1:6, 4:8.-->, mais j'aime ta torah.
119:114	Tu es ma couverture<!--Ps. 32:7, 91:1.--> et mon bouclier, je m'attends à ta parole.
119:115	Méchants, retirez-vous de moi<!--Ps. 6:9 ; Mt. 7:23.--> ! et je garderai les commandements de mon Elohîm.
119:116	Soutiens-moi selon ta parole et je vivrai ; ne me rends pas honteux dans mon espérance !
119:117	Soutiens-moi et je serai sauvé, j'aurai continuellement les yeux sur tes statuts.
119:118	Tu as foulé aux pieds tous ceux qui se détournent de tes statuts, car le mensonge est le moyen dont ils se servent pour tromper.
119:119	Tu enlèves comme des scories tous les méchants de la Terre, c'est pourquoi j'aime tes témoignages.
119:120	Ma chair frissonne de la terreur que tu m'inspires et je crains tes jugements<!--Ha. 3:16.-->.
119:121	[Ayin.] J'ai exercé le jugement et la justice. Ne m'abandonne pas à ceux qui m'oppriment !
119:122	Garantis le bonheur de ton serviteur ! Que les orgueilleux ne m'oppriment pas.
119:123	Mes yeux s'épuisent en attendant ta délivrance et la parole de ta justice.
119:124	Agis envers ton serviteur suivant ta miséricorde et enseigne-moi tes statuts.
119:125	Je suis ton serviteur, donne-moi l'intelligence, et je connaîtrai tes témoignages<!--Pr. 1:4, 6:23.-->.
119:126	Il est temps que YHWH opère : ils ont aboli ta torah.
119:127	C'est pourquoi j'aime tes commandements, plus que l'or et l'or fin.
119:128	C'est pourquoi je trouve justes tous tes préceptes, je hais toute voie de mensonge.
119:129	[Pe.] Tes préceptes sont merveilleux, c'est pourquoi mon âme les garde.
119:130	L'ouverture<!--Vient de l'hébreu « pethach » qui signifie aussi « entrée », « encadrement d'une porte ».--> de tes paroles apporte la lumière, elle donne de l'intelligence aux stupides.
119:131	J'ouvre grandement ma bouche et je soupire, car je désire tes commandements.
119:132	Regarde-moi, et aie pitié de moi, selon tes jugements à l'égard de ceux qui aiment ton Nom.
119:133	Affermis mes pas sur ta parole, et que l'iniquité n'ait pas d'emprise sur moi.
119:134	Délivre-moi de l'oppression des hommes afin que je garde tes préceptes.
119:135	Fais luire ta face sur ton serviteur et enseigne-moi tes statuts.
119:136	Mes yeux répandent des torrents d'eau parce qu'on n'observe pas ta torah.
119:137	[Tsade.] Tu es juste, YHWH, et droit dans tes jugements.
119:138	Tu as prescrit tes témoignages avec justice et grande fidélité.
119:139	Mon zèle me consume parce que mes adversaires oublient tes paroles.
119:140	Ta parole est entièrement éprouvée, c'est pourquoi ton serviteur l'aime.
119:141	Je suis petit et méprisé, toutefois je n'oublie pas tes préceptes.
119:142	Ta justice est une justice éternelle, et ta torah est la vérité.
119:143	La détresse et l'angoisse m'atteignent, mais tes commandements font mes délices.
119:144	Tes préceptes ne sont que justice éternelle, donne-moi l'intelligence afin que je vive !
119:145	[Qof.] Je crie de tout mon cœur, réponds-moi, YHWH ! Je garde tes statuts.
119:146	Je crie vers toi, sauve-moi afin que j'observe tes témoignages.
119:147	Je devance l'aurore et je crie au secours, je m'attends à ta parole.
119:148	Mes yeux ont devancé les veilles de la nuit pour méditer ta parole.
119:149	Écoute ma voix selon ta miséricorde, YHWH ! Fais-moi revivre selon ton ordonnance.
119:150	Ceux qui poursuivent la méchanceté s'approchent de moi, et ils s'éloignent de ta torah.
119:151	YHWH, tu es près de moi, et tous tes commandements ne sont que vérité.
119:152	Je sais depuis longtemps que tu as établi tes témoignages pour toujours.
119:153	[Resh.] Regarde mon affliction et sauve-moi, car je n'oublie pas ta torah.
119:154	Soutiens ma cause et rachète-moi, fais-moi revivre selon ta parole !
119:155	La délivrance est loin des méchants parce qu'ils ne recherchent pas tes statuts.
119:156	Tes compassions sont en grand nombre, YHWH ! Fais-moi revivre selon tes ordonnances.
119:157	Ceux qui me persécutent et qui me pressent sont en grand nombre, toutefois je ne me détourne pas de tes préceptes.
119:158	Je vois avec dégoût les traîtres et je suis rempli de tristesse car ils n'observent pas ta parole.
119:159	Regarde combien j'aime tes préceptes, YHWH ! Fais-moi revivre selon ta miséricorde !
119:160	Le fondement de ta parole est la vérité, et toutes les lois de ta justice sont éternelles.
119:161	[Shin.] Les princes du peuple me persécutent sans cause, mais mon cœur tremble à cause de ta parole.
119:162	Je me réjouis de ta parole comme ferait celui qui aurait trouvé un grand butin.
119:163	Je hais le mensonge, je l'ai en abomination, mais j'aime ta torah.
119:164	Sept fois le jour je te loue à cause des ordonnances de ta justice.
119:165	Il y a une grande paix pour ceux qui aiment ta torah, et rien ne peut les renverser<!--Es. 32:17 ; Ph. 4:7.-->.
119:166	YHWH, j'espère en ta délivrance et je pratique tes commandements.
119:167	Mon âme observe tes témoignages, je les aime beaucoup.
119:168	J'observe tes préceptes et tes témoignages, car toutes mes voies sont devant toi.
119:169	[Tav.] YHWH, que mon cri parvienne jusqu'à toi, donne-moi l'intelligence selon ta parole.
119:170	Que ma supplication vienne devant toi, délivre-moi selon ta parole.
119:171	Mes lèvres publieront ta louange quand tu m'auras enseigné tes statuts.
119:172	Ma langue ne parlera que de ta parole, parce que tous tes commandements ne sont que justice !
119:173	Que ta main vienne me secourir, parce que j'ai choisi tes préceptes.
119:174	YHWH, je souhaite ta délivrance, et ta torah fait mes délices.
119:175	Que mon âme vive afin qu'elle te loue, et que tes ordonnances me soient en aide !
119:176	Je suis errant comme une brebis perdue<!--Es. 53:6 ; Lu. 15:4 ; 1 Pi. 2:25.--> ; cherche ton serviteur, car je n'oublie pas tes commandements.

## Chapitre 120

### Cri de détresse

120:1	Cantique des degrés<!--Les psaumes 120 à 134 sont appelés « psaumes des degrés » ou de « l'ascension ». Ces psaumes furent chantés par les Israélites montant à Yeroushalaim au retour de la captivité de Babel (Babylone).-->. J'ai invoqué YHWH dans ma grande détresse, et il m'a exaucé.
120:2	YHWH, délivre mon âme des lèvres mensongères et de la langue trompeuse.
120:3	Que te donne, que te rapporte la langue trompeuse ?
120:4	Ce sont des flèches aiguës tirées par un homme puissant et des charbons ardents du genêt<!--Jé. 9:3 ; Ja. 3:5-6.-->.
120:5	Malheureux que je suis de séjourner en Méshec et de demeurer aux tentes de Kédar !
120:6	Assez longtemps mon âme a demeuré auprès de ceux qui haïssent la paix !
120:7	Je ne cherche que la paix ; mais lorsque j'en parle, ils sont pour la guerre.

## Chapitre 121

### YHWH ne dort ni ne sommeille

121:1	Cantique pour les degrés. Je lève mes yeux vers les montagnes... D'où me viendra le secours ?
121:2	Mon secours vient de YHWH qui a fait les cieux et la Terre<!--Ps. 124:8.-->.
121:3	Il ne permettra pas que ton pied chancelle, celui qui te garde ne sommeillera pas<!--Es. 27:3 ; Pr. 3:23.-->.
121:4	Voici, il ne sommeille ni ne dort celui qui garde Israël.
121:5	YHWH est celui qui te garde, YHWH est ton ombre à ta droite<!--Es. 25:4.-->.
121:6	Pendant le jour, le soleil ne te frappera pas, ni la lune pendant la nuit<!--Es. 49:10 ; Ap. 7:16.-->.
121:7	YHWH te gardera de tout mal, il gardera ton âme.
121:8	YHWH gardera ton départ et ton arrivée, dès maintenant et à jamais<!--De. 28:6.-->.

## Chapitre 122

### Yeroushalaim, la ville de YHWH

122:1	Cantique des degrés de David. Je me réjouis à cause de ceux qui me disent : Allons à la maison de YHWH<!--Ps. 84:1-5.--> !
122:2	Nos pieds s'arrêtent dans tes portes, Yeroushalaim !
122:3	Yeroushalaim est bâtie comme une ville qui forme un ensemble bien uni,
122:4	à laquelle montent les tribus, les tribus de Yah, selon le témoignage d'Israël, pour célébrer le Nom de YHWH.
122:5	Car c'est là que sont placés les trônes pour le jugement<!--Mt. 19:28.-->, les trônes de la maison de David.
122:6	Priez pour la paix de Yeroushalaim. Que ceux qui t'aiment jouissent du repos !
122:7	Que la paix soit dans tes murs, et la tranquillité dans tes palais !
122:8	Pour l'amour de mes frères et de mes amis, je prie maintenant pour ta paix.
122:9	À cause de la maison de YHWH, notre Elohîm, je fais une requête pour ton bonheur.

## Chapitre 123

### Les yeux fixés sur YHWH

123:1	Cantique des degrés. Je lève mes yeux vers toi qui habites dans les cieux.
123:2	Voici, comme les yeux des serviteurs regardent la main de leurs maîtres, comme les yeux de la servante regardent la main de sa maîtresse, ainsi nos yeux regardent à YHWH, notre Elohîm, jusqu'à ce qu'il ait pitié de nous<!--Ps. 25:15.-->.
123:3	Aie pitié de nous, YHWH ! aie pitié de nous ! car nous sommes assez rassasiés de mépris !
123:4	Notre âme est assez rassasiée des moqueries des orgueilleux, du mépris des hautains.

## Chapitre 124

### YHWH, l'Elohîm qui secourt et protège son peuple

124:1	Cantique des degrés de David. Sans YHWH qui était de notre côté, qu'Israël le dise !
124:2	Sans YHWH qui était de notre côté quand les hommes s'élevèrent contre nous,
124:3	ils nous auraient engloutis tout vivants, quand leur colère s'enflamma contre nous.
124:4	Alors les eaux nous auraient submergés, les torrents auraient passé sur notre âme,
124:5	alors les flots impétueux auraient passé sur notre âme.
124:6	Béni soit YHWH qui ne nous a pas livrés en proie à leurs dents !
124:7	Notre âme s'est échappée comme l'oiseau du filet des oiseleurs ; le filet a été rompu, et nous nous sommes échappés<!--Pr. 6:5.-->.
124:8	Notre secours est dans le Nom de YHWH<!--Ac. 4:11-12.-->, qui a fait les cieux et la Terre.

## Chapitre 125

### YHWH entoure tous ceux qui se confient en lui

125:1	Cantique des degrés. Ceux qui se confient en YHWH sont comme la montagne de Sion : elle ne chancelle pas et est affermie pour toujours.
125:2	Comme Yeroushalaim est entourée de montagnes ; de même YHWH entoure son peuple, dès maintenant et à jamais.
125:3	Car la verge de la méchanceté ne restera pas sur le lot des justes, de peur que les justes ne tendent leurs mains vers l'injustice<!--Es. 14:5.-->.
125:4	YHWH, sois bon avec ceux qui sont bons, ceux dont le cœur est droit.
125:5	Mais quant à ceux qui s'engagent dans des voies détournées, que YHWH les fasse marcher avec ceux qui pratiquent la méchanceté<!--Mt. 7:23.--> ! La paix sera sur Israël.

## Chapitre 126

### YHWH, le libérateur

126:1	Cantique des degrés. Quand YHWH ramena les captifs de Sion, nous étions comme ceux qui font un rêve.
126:2	Alors notre bouche était remplie de joie, et notre langue de chants de triomphe, alors on disait parmi les nations : YHWH a fait de grandes choses pour eux !
126:3	YHWH a fait de grandes choses pour nous ; nous sommes dans la joie.
126:4	YHWH ! ramène nos captifs, comme des ruisseaux dans le midi<!--Os. 6:11 ; Joë. 3:11.--> !
126:5	Ceux qui sèment avec larmes, moissonneront avec chants d'allégresse<!--Ga. 6:9.-->.
126:6	Celui qui marche en pleurant, quand il porte la semence pour la mettre en terre, revient avec des chants d'allégresse quand il porte ses gerbes.

## Chapitre 127

### YHWH, le plus grand architecte

127:1	Cantique des degrés de Shelomoh. Si YHWH ne bâtit la maison, ceux qui la bâtissent travaillent en vain. Si YHWH ne garde la ville, celui qui la garde fait le guet en vain.
127:2	C'est en vain que vous vous levez de grand matin, que vous vous couchez tard, et que vous mangez le pain de douleurs. C’est ainsi qu’il donne le sommeil à son bien-aimé<!--Ez. 20:20 ; Mc. 2:27.-->.
127:3	Voici, les enfants sont un héritage donné par YHWH, et le fruit du ventre est une récompense d'Elohîm<!--Ps. 113:9, 128:3-6.-->.
127:4	Telles sont les flèches dans la main d'un homme puissant, tels sont les fils de la jeunesse.
127:5	Béni est l'homme qui en a rempli son carquois ! Ils ne seront pas honteux quand ils parleront avec leurs ennemis à la porte.

## Chapitre 128

### YHWH assure la paix à celui qui le craint

128:1	Cantique des degrés. Béni est tout homme qui craint YHWH et marche dans ses voies !
128:2	Car tu mangeras du travail de tes mains ! Tu seras heureux et tu prospéreras<!--Es. 3:10.-->.
128:3	Ta femme sera dans ta maison comme une vigne qui porte du fruit. Tes fils seront comme des plants d'oliviers autour de ta table.
128:4	Voyez ! C’est ainsi que sera béni l'homme qui craint YHWH.
128:5	YHWH te bénira de Sion et tu verras le bien de Yeroushalaim tous les jours de ta vie.
128:6	Tu verras les enfants de tes enfants. La paix sera sur Israël.

## Chapitre 129

### L'opprimé plus que vainqueur en YHWH

129:1	Cantique des degrés. Qu'Israël dise maintenant : Ils m'ont souvent tourmenté dès ma jeunesse.
129:2	Ils m'ont assez opprimé dès ma jeunesse, mais ils ne m'ont pas vaincu.
129:3	Des laboureurs ont labouré mon dos, ils y ont tracé de longs sillons.
129:4	YHWH est juste, il a coupé les cordes des méchants.
129:5	Ils auront honte et retourneront en arrière, tous ceux qui haïssent Sion !
129:6	Ils deviendront comme l'herbe des toits qui sèche avant qu'on l'arrache,
129:7	dont le moissonneur ne remplit pas sa main, ni celui qui lie les gerbes, ses bras.
129:8	Les passants ne disent pas : Que la bénédiction de YHWH soit sur vous ! Nous vous bénissons au Nom de YHWH !

## Chapitre 130

### La rédemption en abondance auprès de YHWH

130:1	Cantique des degrés. YHWH, je t'invoque depuis les profondeurs !
130:2	Adonaï, écoute ma voix ! Que tes oreilles soient attentives à la voix de mes supplications !
130:3	Yah ! si tu prenais garde aux iniquités, Adonaï, qui pourrait tenir debout ?
130:4	Mais le pardon se trouve auprès de toi, afin qu'on te craigne<!--Mt. 26:28 ; Ro. 3:24 ; Col. 1:12-14.-->.
130:5	J'espère en YHWH, mon âme espère, et j'attends sa parole.
130:6	Mon âme attend Adonaï plus que les sentinelles n'attendent le matin, plus que les sentinelles n'attendent le matin.
130:7	Israël, attends-toi à YHWH, car YHWH est miséricordieux et la rançon<!--1 Tm. 2:6.--> est auprès de lui en abondance.

## Chapitre 131

### Mettre son espoir en YHWH seul

131:1	Cantique des degrés de David. YHWH ! je n'ai ni un cœur qui s'élève ni un regard hautain<!--Pr. 6:17, 16:5.-->, je ne m'occupe pas de choses trop grandes et trop extraordinaires pour moi.
131:2	N'ai-je pas soumis et fait taire mon âme, comme un enfant sevré auprès de sa mère ? Mon âme est en moi comme un enfant sevré.
131:3	Israël attends-toi à YHWH dès maintenant et à jamais !

## Chapitre 132

### Sion, le trône de YHWH

132:1	Cantique des degrés. YHWH ! souviens-toi de David et de toute son affliction !
132:2	Il a juré à YHWH et fait ce vœu au puissant de Yaacov :
132:3	Je n'entrerai pas dans la tente où j'habite, je ne monterai pas sur le lit où je couche,
132:4	je ne donnerai pas de sommeil à mes yeux, je ne laisserai pas sommeiller mes paupières,
132:5	jusqu'à ce que j'aie trouvé un lieu pour YHWH, une demeure pour le puissant de Yaacov<!--1 Ch. 15:1.--> !
132:6	Voici, nous avons entendu parler d'elle à Éphrata, nous l'avons trouvée dans les champs de Yaar<!--Ou « Champs-de-la-Forêt ».-->.
132:7	Entrons dans sa demeure, prosternons-nous devant le marchepied de ses pieds.
132:8	Lève-toi, YHWH, dans ton lieu de repos, toi et l'arche de ta force<!--No. 10:35-36 ; 2 Ch. 6:41.-->.
132:9	Que tes prêtres soient revêtus de justice, et que tes bien-aimés chantent de joie<!--Es. 11:5 ; Ap. 19:8.--> !
132:10	Pour l'amour de David, ton serviteur, ne repousse pas la face de ton mashiah !
132:11	YHWH a juré la vérité à David, et il ne se rétractera pas, disant : Je mettrai le fruit de tes entrailles<!--2 S. 7:12 ; 1 R. 8:25 ; 2 Ch. 6:16 ; Lu. 1:69 ; Ac. 2:30.--> sur ton trône.
132:12	Si tes fils gardent mon alliance et mes témoignages que je leur enseignerai, leurs fils aussi seront assis à perpétuité sur ton trône.
132:13	Car YHWH a choisi Sion, il l'a préférée pour être son trône.
132:14	Elle est mon lieu de repos à perpétuité, j'y habiterai parce que je l'ai désirée.
132:15	Je bénirai, je bénirai sa nourriture, je rassasierai de pain ses pauvres.
132:16	Je revêtirai de salut ses prêtres, et ses bien-aimés chanteront avec des cris de joie.
132:17	Je ferai germer en elle une corne à David, je préparerai une lampe à mon mashiah,
132:18	je revêtirai de honte ses ennemis, et sur lui fleurira son diadème.

## Chapitre 133

### La bénédiction dans la communion fraternelle

133:1	Cantique des degrés de David. Voyez ! Qu’il est bon, qu’il est agréable pour des frères de demeurer unis ensemble<!--Ac. 2:46 ; Hé. 13:1.--> !
133:2	C'est comme l’huile excellente répandue sur la tête qui coule sur la barbe, sur la barbe d'Aaron<!--Ex. 30:22-30.-->, sur le bord de ses vêtements.
133:3	C'est comme la rosée de l'Hermon qui descend sur les montagnes de Sion. Car c'est là que YHWH a ordonné la bénédiction et la vie, pour l'éternité.

## Chapitre 134

### Bénissez YHWH, vous tous ses serviteurs

134:1	Cantique des degrés. Voici, bénissez YHWH, vous tous les serviteurs de YHWH, qui vous tenez toutes les nuits dans la maison de YHWH !
134:2	Élevez vos mains vers le lieu saint, et bénissez YHWH !
134:3	Que YHWH, qui a fait les cieux et la Terre, te bénisse de Sion !

## Chapitre 135

### La souveraineté d'Elohîm

135:1	Allélou-Yah, vous serviteurs de YHWH ! Louez-le !
135:2	Vous qui vous tenez dans la maison de YHWH, dans les parvis de la maison de notre Elohîm !
135:3	Allélou-Yah ! car YHWH est bon ! Chantez son Nom, car il est agréable !
135:4	Car Yah s'est choisi Yaacov et Israël pour sa propriété<!--Ex. 19:5 ; De. 7:6 ; Tit. 2:14 ; 1 Pi. 2:9.-->.
135:5	Certainement, je sais que YHWH est grand et que notre Seigneur est au-dessus de tous les elohîm.
135:6	YHWH fait tout ce qu'il lui plaît, dans les cieux et sur la Terre, dans la mer et dans tous les abîmes.
135:7	C'est lui qui fait monter les vapeurs des extrémités de la Terre, il fait les éclairs et la pluie, il tire le vent hors de ses trésors.
135:8	C'est lui qui a frappé les premiers-nés d'Égypte, tant des hommes que des bêtes,
135:9	qui a envoyé des prodiges et des miracles au milieu de toi, Égypte ! Contre pharaon et contre tous ses serviteurs,
135:10	qui a frappé des nations nombreuses et tué des rois puissants :
135:11	Sihon, roi des Amoréens, et Og, roi de Bashân, et ceux de tous les royaumes de Canaan<!--No. 21:33-35 ; De. 3:11.-->.
135:12	Il a donné leur pays en héritage, en héritage à Israël, son peuple.
135:13	YHWH, ton Nom est pour toujours ! YHWH, ta mémoire de génération en génération !
135:14	Car YHWH jugera son peuple et se repentira à l'égard de ses serviteurs.
135:15	Les idoles des nations ne sont que de l'or et de l'argent, un ouvrage de mains d’humain.
135:16	Elles ont une bouche et ne parlent pas, elles ont des yeux et ne voient pas,
135:17	elles ont des oreilles et n'entendent pas, il n'y a pas de souffle dans leur bouche.
135:18	Ceux qui les fabriquent, tous ceux qui se confient en elles, deviennent comme elles.
135:19	Maison d'Israël, bénissez YHWH ! Maison d'Aaron, bénissez YHWH !
135:20	Maison des Lévites, bénissez YHWH ! Vous qui craignez YHWH, bénissez YHWH !
135:21	Béni soit de Sion, YHWH qui habite dans Yeroushalaim ! Allélou-Yah !

## Chapitre 136

### La bonté de YHWH demeure à toujours

136:1	Célébrez YHWH, car il est bon, car sa bonté est éternelle !
136:2	Célébrez l'Elohîm des elohîm, car sa bonté est éternelle !
136:3	Célébrez le Seigneur des seigneurs, car sa bonté est éternelle !
136:4	Célébrez celui qui seul fait de grandes merveilles, car sa bonté est éternelle !
136:5	Celui qui a fait avec intelligence les cieux, car sa bonté est éternelle !
136:6	Celui qui a étendu la terre sur les eaux, car sa bonté est éternelle !
136:7	Celui qui a fait les grands luminaires, car sa bonté est éternelle !
136:8	Le soleil pour dominer sur le jour, car sa bonté est éternelle !
136:9	La lune et les étoiles pour dominer la nuit, car sa bonté est éternelle !
136:10	Celui qui a frappé l'Égypte dans ses premiers-nés, car sa bonté est éternelle !
136:11	Qui a fait sortir Israël du milieu d'eux, car sa bonté est éternelle !
136:12	Et cela avec main forte et bras étendu, car sa bonté est éternelle !
136:13	Il a fendu la Mer Rouge en deux, car sa bonté est éternelle !
136:14	Il a fait passer Israël par le milieu d'elle, car sa bonté est éternelle !
136:15	Il a renversé pharaon et son armée dans la Mer Rouge, car sa bonté est éternelle !
136:16	Il a conduit son peuple dans le désert, car sa bonté est éternelle !
136:17	Il a frappé de grands rois, car sa bonté est éternelle !
136:18	Et a tué des grands rois, car sa bonté est éternelle !
136:19	Sihon, roi des Amoréens, car sa bonté est éternelle !
136:20	Og, roi de Bashân, car sa bonté est éternelle !
136:21	Il a donné leur pays en héritage, car sa bonté est éternelle<!--Jos. 12:7.--> !
136:22	En héritage à Israël, son serviteur, car sa bonté est éternelle !
136:23	Et qui, lorsque nous étions humiliés, s'est souvenu de nous, car sa bonté est éternelle !
136:24	Il nous a délivrés de la main de nos adversaires, car sa bonté est éternelle !
136:25	Il donne la nourriture à toute chair, car sa bonté est éternelle<!--Ps. 104:21, 147:9 ; Mt. 6:26.--> !
136:26	Célébrez le El des cieux, car sa bonté est éternelle !

## Chapitre 137

### Le cœur des captifs

137:1	Sur les bords des fleuves de Babel, nous étions assis et nous pleurions en nous souvenant de Sion.
137:2	Nous avions suspendu nos harpes au milieu des saules.
137:3	Là, ceux qui nous avaient emmenés en captivité, nous ont demandé des paroles de chants, et nos oppresseurs de la joie : Chantez-nous quelques cantiques de Sion !
137:4	Comment chanterions-nous les cantiques de YHWH sur une terre étrangère ?
137:5	Si je t'oublie, Yeroushalaim, que ma droite s'oublie elle-même !
137:6	Que ma langue soit attachée à mon palais<!--Ez. 3:26.-->, si je ne me souviens pas de toi, si je ne fais pas de Yeroushalaim le sujet de ma réjouissance !
137:7	YHWH, souviens-toi des fils d'Édom, qui, au jour de Yeroushalaim, disaient : Rasez, rasez jusqu'à ses fondements<!--Jé. 25:15-21, 49:7-8 ; Ez. 25:12 ; La. 4:21 ; Am. 1:11.--> !
137:8	Fille de Babel, qui va être détruite, heureux celui qui te rend la pareille de ce que tu nous as fait<!--Jé. 50:15-29 ; Ap. 18:6.--> !
137:9	Heureux celui qui saisit tes petits enfants et qui les écrase contre le rocher<!--Es. 13:16.--> !

## Chapitre 138

### La renommée de YHWH dans les nations

138:1	Psaume de David. Je te célèbre de tout mon cœur, je te chante des louanges dans la présence d'Elohîm.
138:2	Je me prosterne dans le palais de ta sainteté et je célèbre ton Nom, à cause de ta bonté et de ta vérité, car tu as glorifié ta parole au-dessus de tout nom.
138:3	Le jour où je t'ai invoqué, tu m'as exaucé, tu m'as rassuré, tu m'as fortifié d'une nouvelle force en mon âme.
138:4	YHWH ! Tous les rois de la Terre te célèbrent, quand ils entendent les paroles de ta bouche.
138:5	Ils chantent les voies de YHWH, car la gloire de YHWH est grande.
138:6	Car YHWH est haut élevé, il voit les humbles et il reconnaît de loin les orgueilleux.
138:7	Quand je marche au milieu de l'adversité, tu me rends la vie, tu avances ta main contre la colère de mes ennemis, et ta droite me délivre.
138:8	YHWH achèvera ce qui me concerne. YHWH, ta bonté demeure toujours : tu n'abandonnes pas l'œuvre de tes mains<!--Ph. 1:6.-->.

## Chapitre 139

### L'omniscience de YHWH

139:1	Psaume de David. Au chef de musique. YHWH, tu me sondes et tu me connais<!--Jé. 12:3 ; Ps. 17:3.-->.
139:2	Tu sais quand je m'assieds et quand je me lève, tu discernes de loin ma pensée.
139:3	Tu sais quand je marche et quand je me couche, tu connais parfaitement toutes mes voies.
139:4	Car la parole n’est pas encore sur ma langue, que voici, YHWH, tu la connais entièrement.
139:5	Tu m'assièges par derrière et par devant, et tu as posé sur moi ta main.
139:6	Ta connaissance est trop merveilleuse pour moi, elle est trop élevée pour que je puisse l'atteindre<!--Ps. 92:6 ; Job 42:3 ; Ro. 11:33.-->.
139:7	Où irai-je loin de ton Esprit, et où fuirai-je loin de ta face<!--Jé. 23:24 ; Am. 9:2-4 ; Jon. 1:3.--> ?
139:8	Si je monte aux cieux, tu y es ! Si je me couche dans le shéol, t'y voilà !
139:9	Si je prends les ailes de l'aurore, et que je demeure à l'extrémité de la mer,
139:10	là aussi ta main me conduira, et ta droite me saisira.
139:11	Si je dis : Au moins les ténèbres me couvriront, la nuit même sera une lumière tout autour de moi !
139:12	Même les ténèbres ne me cacheront pas de toi, et la nuit resplendira comme le jour, et les ténèbres comme la lumière.
139:13	Tu as créé mes reins, tu me couvres dans le sein de ma mère.
139:14	Je te loue de ce que j'ai été fait d'une si étrange et si admirable manière. Tes œuvres sont merveilleuses, et mon âme le reconnaît très bien.
139:15	Mon corps n'était pas caché devant toi, lorsque j'ai été fait dans un lieu secret et brodé dans les parties inférieures de la Terre<!--Ps. 119:73 ; Ec. 11:5.-->.
139:16	Tes yeux me voyaient quand je n'étais qu'un embryon, et sur ton livre étaient inscrits tous les jours qui m'étaient destinés<!--Ph. 4:3 ; Ap. 3:5, 20:15.-->.
139:17	C'est pourquoi, El ! que tes pensées me sont précieuses ! Que le nombre en est grand !
139:18	Si je les compte, elles sont plus nombreuses que les grains de sable. Je m'éveille, et je suis encore avec toi.
139:19	Éloah ! ne tueras-tu pas le méchant ? C'est pourquoi, hommes sanguinaires, retirez-vous loin de moi !
139:20	Car ils ont parlé de toi en pensant à quelque méchanceté, ils ont élevé tes ennemis en mentant.
139:21	YHWH, n'aurais-je pas de la haine pour ceux qui te haïssent, et ne serais-je pas irrité contre ceux qui s'élèvent contre toi ?
139:22	Je les hais d'une parfaite haine, ils sont pour moi des ennemis.
139:23	El ! sonde-moi et considère mon cœur ! Éprouve-moi et considère mes discours !
139:24	Et regarde si je suis sur une mauvaise voie et conduis-moi sur la voie de l'éternité !

## Chapitre 140

### YHWH, le protecteur

140:1	Psaume de David. Au chef de musique. YHWH, délivre-moi de l'homme méchant, garde-moi de l'homme violent.
140:2	Ils méditent des méchancetés dans leur cœur, tous les jours ils complotent des guerres !
140:3	Ils aiguisent leur langue comme un serpent, il y a du venin de vipère sous leurs lèvres. Sélah.
140:4	YHWH, garde-moi de la main du méchant ! Préserve-moi de l'homme violent, de ceux qui méditent de me faire tomber !
140:5	Les orgueilleux me tendent un piège et des filets, et ils étendent des rets le long du chemin, ils me dressent des embûches. Sélah.
140:6	Je dis à YHWH : Tu es mon El, YHWH, prête l'oreille à la voix de mes supplications !
140:7	YHWH Adonaï, la force de mon salut ! Tu couvres ma tête au jour de la bataille.
140:8	YHWH, n'accorde pas au méchant ses souhaits, ne fais pas réussir son mauvais dessein, il s'élèverait. Sélah.
140:9	Quant aux têtes de ceux qui m'assiègent, que la peine de leurs lèvres les couvre !
140:10	Que des charbons embrasés tombent sur eux, qu'il les fasse tomber dans le feu, et dans des fosses profondes, sans qu'ils se relèvent<!--Pr. 25:21-22 ; Ro. 12:20.--> !
140:11	Que l'homme médisant ne soit pas affermi sur la Terre, et quant à l'homme violent et mauvais, qu'on le chasse jusqu'à ce qu'il soit exterminé !
140:12	Je sais que YHWH fera justice au malheureux et droit aux indigents.
140:13	Quoi qu'il en soit, les justes célébreront ton Nom, les hommes droits habiteront devant ta face.

## Chapitre 141

### YHWH, garde-moi du mal !

141:1	Psaume de David. YHWH, je t'invoque, hâte-toi de venir vers moi ! Prête l'oreille à ma voix, lorsque je crie à toi !
141:2	Que ma prière te soit agréable comme l'encens, et l'élévation de mes mains comme l'offrande du soir<!--Ex. 30:1 ; Ap. 5:8, 8:3.--> !
141:3	YHWH, mets une garde à ma bouche, garde l'entrée de mes lèvres !
141:4	N'incline pas mon cœur vers des choses mauvaises. Que je ne me livre pas à des méchantes actions par malice avec les hommes qui font le mal ! Que je ne mange pas de leurs délices !
141:5	Que le juste me frappe ! C'est une faveur. Et qu'il me réprimande ! Ce sera pour moi une huile excellente<!--Pr. 27:6 ; Ec. 7:5.-->. Elle ne blessera pas ma tête, mais de nouveau ma prière s'élèvera contre leurs calamités.
141:6	Quand leurs gouverneurs auront été précipités parmi des rochers, alors on écoutera mes paroles, car elles sont agréables.
141:7	Nos os sont dispersés dans la bouche du shéol comme quand on laboure la terre et on fend le bois.
141:8	C'est pourquoi, YHWH Adonaï ! mes yeux sont sur toi, je me suis retiré vers toi, n'abandonne pas mon âme !
141:9	Garde-moi du pouvoir de piège qu'ils m'ont tendu et des appâts de ceux qui font le mal.
141:10	Que tous les méchants tombent chacun dans son filet, jusqu'à ce que je sois passé !

## Chapitre 142

### YHWH, mon refuge

142:1	Cantique de David. Prière qu'il fit lorsqu'il était dans la caverne<!--1 S. 24:4.-->.
142:2	Je crie de ma voix à YHWH, je supplie de ma voix YHWH.
142:3	Je répands devant lui ma complainte, je déclare mon angoisse devant lui<!--1 S. 1:15 ; La. 2:19.-->.
142:4	Quand mon esprit est abattu en moi, toi, tu connais mon sentier. Ils me tendent un piège sur le chemin par lequel je marche.
142:5	Je contemple à ma droite, et je regarde ! Et il n'y a personne qui me reconnaît : tout refuge s'évanouit devant moi, il n'y a personne qui prend soin de mon âme.
142:6	YHWH, je crie vers toi. Je dis : Tu es mon refuge, ma part sur la Terre des vivants.
142:7	Sois attentif à mon cri car je suis devenu très affaibli. Délivre-moi de ceux qui me poursuivent car ils sont plus puissants que moi.
142:8	Délivre-moi du lieu où je suis renfermé, et je célébrerai ton Nom ! Les justes viendront autour de moi, parce que tu m'auras fait ce bien.

## Chapitre 143

### YHWH, enseigne-moi à faire ta volonté

143:1	Psaume de David. YHWH, écoute ma requête, prête l'oreille à mes supplications ! Exauce-moi dans ta fidélité, réponds-moi à cause de ta justice !
143:2	N'entre pas en jugement avec ton serviteur ! Car aucun vivant n'est juste devant toi.
143:3	Car l'ennemi poursuit mon âme, il foule ma vie par terre ; il me fait habiter dans les ténèbres comme ceux qui sont morts depuis longtemps.
143:4	Et mon esprit est abattu au-dedans de moi, mon cœur est épouvanté en mon sein.
143:5	Je me souviens des jours anciens, je médite sur toutes tes œuvres, je médite sur l'ouvrage de tes mains<!--Ps. 77:11-13.-->.
143:6	J'étends mes mains vers toi ; mon âme s'adresse à toi comme une terre desséchée<!--Ps. 28:1, 42:1-3.-->. Sélah.
143:7	YHWH ! hâte-toi, réponds-moi ! Mon esprit se consume ! Ne me cache pas ta face au point que je devienne semblable à ceux qui descendent dans la fosse !
143:8	Fais-moi entendre dès le matin ta miséricorde ! Car je me confie en toi. Fais-moi connaître le chemin par lequel je dois marcher ! Car j'ai élevé mon cœur vers toi<!--Ps. 25:1.-->.
143:9	YHWH, délivre-moi de mes ennemis ! Car je me suis réfugié auprès de toi !
143:10	Enseigne-moi à faire ta volonté ! Car tu es mon Elohîm ! Que ton bon Esprit me conduise sur la voie de la droiture<!--Jn. 16:13.--> !
143:11	YHWH, rends-moi la vie pour l'amour de ton Nom ! Retire mon âme de la détresse à cause de ta justice !
143:12	Et selon la bonté que tu as pour moi, retranche mes ennemis ! Détruis tous ceux qui tiennent mon âme oppressée ! parce que je suis ton serviteur !

## Chapitre 144

### Se confier en YHWH, le Rocher

144:1	Psaume de David. Béni soit YHWH, mon rocher<!--Voir commentaire en Es. 8:13-17.--> qui exerce mes mains au combat et mes doigts à la bataille,
144:2	qui déploie sa bonté envers moi, qui est ma forteresse, ma haute retraite, mon libérateur<!--Es. 59:20-21 ; Ro. 11:26.-->, mon bouclier<!--Ep. 6:16.-->, mon refuge<!--Ps. 91 ; Mt. 11:28-30.-->, qui m'assujettit mon peuple !
144:3	YHWH ! qu'est-ce que l'être humain, pour que tu le connaisses<!--Ps. 8:5 ; Job 7:17 ; Hé. 2:6-7.-->, le fils de l'homme pour que tu penses à lui ?
144:4	L'être humain est semblable à une vapeur, ses jours sont comme une ombre qui passe<!--Ps. 102:12 ; Job 14:1-2 ; Ec. 6:12.-->.
144:5	YHWH, abaisse tes cieux, et descends ! Touche les montagnes, et qu'elles soient fumantes<!--Es. 63:19 ; Ps. 18:7-8.-->.
144:6	Lance les éclairs, et disperse mes ennemis ! Lance tes flèches, et mets-les en déroute !
144:7	Étends tes mains d'en haut ; sauve-moi et délivre-moi des grandes eaux, de la main des fils de l'étranger,
144:8	dont la bouche profère le mensonge, et dont la droite est une droite trompeuse !
144:9	Elohîm ! je chanterai un cantique nouveau ! Je te célébrerai sur le luth à dix cordes !
144:10	Toi, qui donnes la délivrance aux rois et qui délivres de l'épée meurtrière David, ton serviteur,
144:11	délivre-moi et sauve-moi de la main des fils de l'étranger, dont la bouche profère le mensonge et dont la droite est une droite trompeuse !
144:12	Afin que nos fils soient comme des plantes qui croissent dans leur jeunesse ; et nos filles comme des pierres angulaires taillées pour l'ornement d'un palais.
144:13	Que nos greniers soient pleins, fournissant toutes sortes de provisions ! Que nos troupeaux multiplient par milliers, même par dix milliers dans nos rues !
144:14	Que nos bœufs soient chargés de graisse ! Qu'il n'y ait ni brèche, ni sortie, ni cri sur nos places !
144:15	Heureux le peuple pour qui il en est ainsi ! Heureux le peuple dont YHWH est l'Elohîm !

## Chapitre 145

### Louange à YHWH pour tout ce qu'il est

145:1	Psaume de louange, composé par David. [Aleph.] Mon Elohîm, mon roi, je t'exalterai et je bénirai ton Nom à toujours, et à perpétuité !
145:2	[Beth.] Je te bénirai chaque jour, et je louerai ton Nom à toujours, et à perpétuité !
145:3	[Guimel.] YHWH est grand et très digne de louanges, il n'est pas possible de sonder sa grandeur.
145:4	[Daleth.] Que chaque génération célèbre tes œuvres, et publie tes hauts faits !
145:5	[He.] Je parlerai de la splendeur glorieuse de ta majesté, et de tes paroles merveilleuses !
145:6	[Vav.] On parlera de ta puissance redoutable, et je raconterai ta grandeur.
145:7	[Zayin.] Ils proclameront le souvenir de ton immense bonté, et ils raconteront avec chants de triomphe ta justice !
145:8	[Heth.] YHWH est miséricordieux et compatissant, lent à la colère et grand en bonté.
145:9	[Teth.] YHWH est bon envers tous, et ses compassions sont au-dessus de toutes ses œuvres.
145:10	[Yod.] YHWH ! toutes tes œuvres te célébreront, et tes fidèles te béniront !
145:11	[Kaf.] Ils parleront de la gloire de ton règne, et ils proclameront ta puissance
145:12	[Lamed.] pour faire connaître aux fils de l'homme ta puissance, et la splendeur glorieuse de ton règne.
145:13	[Mem.] Ton règne est un règne de tous les siècles, et ta domination subsiste dans tous les âges.
145:14	[Samech.] YHWH soutient tous ceux qui tombent, et redresse tous ceux qui sont courbés<!--Ps. 146:8.-->.
145:15	[Ayin.] Les yeux de tous les animaux s'attendent à toi, et tu leur donnes leur nourriture en leur temps.
145:16	[Pe.] Tu ouvres ta main, et tu rassasies à souhait toute créature vivante.
145:17	[Tsade.] YHWH est juste dans toutes ses voies, et plein de bonté dans toutes ses œuvres<!--Da. 4:37.-->.
145:18	[Qof.] YHWH est près de tous ceux qui l'invoquent, de tous ceux qui l'invoquent avec vérité<!--Ps. 34:18.-->.
145:19	[Resh.] Il accomplit la volonté de ceux qui le craignent, il entend leur cri et les délivre.
145:20	[Shin.] YHWH garde tous ceux qui l'aiment, mais il exterminera tous les méchants.
145:21	[Tav.] Ma bouche racontera la louange de YHWH, et toute chair bénira le Nom de sa sainteté à toujours, et à perpétuité<!--Ps. 103:1.--> !

## Chapitre 146

### La fidélité de YHWH dure à toujours

146:1	Allélou-Yah ! Mon âme, loue YHWH !
146:2	Je louerai YHWH toute ma vie, je chanterai mon Elohîm tant que je vivrai !
146:3	Ne vous confiez pas aux nobles, aux fils de l'être humain à qui n'appartient pas la délivrance.
146:4	Son esprit sort, il retourne dans sa terre, et ce même jour ses desseins périssent.
146:5	Heureux celui qui a pour secours le El de Yaacov et dont l'espérance est en YHWH son Elohîm !
146:6	Il a fait les cieux et la Terre, la mer et tout ce qui s'y trouve. Il garde la vérité à toujours !
146:7	Il rend justice aux opprimés, il donne du pain aux affamés. YHWH délie ceux qui sont liés<!--Jn. 11:43-44.-->,
146:8	YHWH ouvre les yeux des aveugles<!--Les miracles de Yéhoshoua ha Mashiah (Jésus-Christ) confirment sa divinité (Es. 35:4-6 ; Lu. 7:19-23).--> ; YHWH redresse ceux qui sont courbés<!--Lu. 13:11-13.--> ; YHWH aime les justes.
146:9	YHWH protège les étrangers, il soutient l'orphelin et la veuve, mais il renverse la voie des méchants.
146:10	YHWH règne éternellement. Sion ! ton Elohîm subsiste d'âge en âge ! Allélou-Yah !

## Chapitre 147

### YHWH aime ceux qui le craignent et qui s'attendent à sa bonté

147:1	Allélou-Yah ! Car il est beau de chanter à notre Elohîm ! Car il est doux et bienséant de le louer !
147:2	YHWH est celui qui bâtit Yeroushalaim, il rassemblera ceux d'Israël qui sont dispersés çà et là ;
147:3	il guérit ceux qui ont le cœur brisé, et il bande leurs plaies<!--Ex. 15:26 ; De. 32:39 ; Job 5:18.-->.
147:4	Il compte le nombre des étoiles, il les appelle toutes par leur nom.
147:5	Notre Seigneur est grand, puissant par sa force, son intelligence n'a pas de limites.
147:6	YHWH soutient les malheureux, mais il abaisse les méchants jusqu'à terre.
147:7	Chantez à YHWH avec des actions de grâces ! Célébrez notre Elohîm avec la harpe !
147:8	Il couvre les cieux de nuées, il prépare la pluie pour la Terre, il fait germer l'herbe sur les montagnes.
147:9	Il donne la nourriture au bétail, et aux petits du corbeau qui crient.
147:10	Il ne prend pas plaisir dans la force du cheval, il ne fait pas cas des jambes de l'homme.
147:11	YHWH aime ceux qui le craignent, ceux qui s'attendent à sa bonté.
147:12	Yeroushalaim, loue YHWH ! Sion, loue ton Elohîm !
147:13	Car il a affermi les barres de tes portes, il a béni tes fils au milieu de toi.
147:14	C'est lui qui rend paisibles tes contrées, et qui te rassasie du meilleur froment.
147:15	C'est lui qui envoie sa parole sur la Terre, et sa parole court avec beaucoup de vitesse<!--Es. 55:10-11.-->.
147:16	C'est lui qui donne la neige comme des flocons de laine, et qui répand la gelée blanche comme de la cendre ;
147:17	c'est lui qui lance sa glace comme par morceaux, qui peut résister devant son froid ?
147:18	Il envoie sa parole, et il les fond ; il fait souffler son vent, et les eaux coulent<!--Ps. 135:7.-->.
147:19	Il déclare ses paroles à Yaacov, ses statuts et ses ordonnances à Israël<!--Ps. 78:5.-->.
147:20	Il n'a pas agi de même pour toutes les nations, c'est pourquoi elles ne connaissent pas ses ordonnances. Allélou-Yah !

## Chapitre 148

### La création loue son Elohîm

148:1	Allélou-Yah ! Louez des cieux YHWH ! Louez-le dans les lieux élevés !
148:2	Louez-le, vous tous ses anges ! Louez-le, vous toutes ses armées !
148:3	Louez-le, vous, soleil et lune ! Louez-le, vous toutes, étoiles lumineuses !
148:4	Louez-le, vous, cieux des cieux ! Et vous, eaux qui êtes au-dessus des cieux !
148:5	Qu'ils louent le Nom de YHWH ! Car il a commandé, et ils ont été créés<!--Ge. 1:3-6 ; Jé. 31:35.-->.
148:6	Il les a établis à perpétuité et à toujours ; il a donné des lois, et il ne les violera pas<!--Ps. 104:5, 119:91 ; Job 14:5.-->.
148:7	De la Terre, louez YHWH ! Louez-le, monstres marins, et tous les abîmes !
148:8	Feu et grêle, neige et brouillard, vent impétueux qui exécutez ses ordres,
148:9	montagnes et toutes les collines, arbres fruitiers et tous les cèdres,
148:10	bêtes sauvages et tout le bétail, reptiles et oiseaux ailés,
148:11	rois de la Terre et tous les peuples, princes et tous les juges de la Terre,
148:12	jeunes garçons et vierges, vieillards avec les jeunes hommes !
148:13	Qu'ils louent le Nom de YHWH ! car son Nom seul est haut élevé ! Sa majesté est au-dessus de la Terre et des cieux.
148:14	Il a relevé la force de son peuple, sujet de louange pour tous ses fidèles, pour les enfants d'Israël, du peuple qui est près de lui. Allélou-Yah !

## Chapitre 149

### Adorons YHWH

149:1	Allélou-Yah ! Chantez à YHWH un cantique nouveau ! Et louez-le dans l'assemblée de ses bien-aimés !
149:2	Qu'Israël se réjouisse en celui qui l'a fait ! Et que les enfants de Sion soient dans l'allégresse à cause de leur Roi<!--Ps. 100:3 ; Za. 9:9 ; Mt. 21:5.--> !
149:3	Qu'ils louent son Nom avec des danses ! Qu'ils le chantent avec le tambourin et la harpe !
149:4	Car YHWH prend plaisir à son peuple, il glorifie les pauvres en les délivrant.
149:5	Que les bien-aimés se réjouissent dans la gloire, qu'ils poussent des cris de joie sur leur couche !
149:6	Les louanges de El sont dans leur bouche, et les épées affilées à deux tranchants dans leur main,
149:7	pour se venger des nations, pour châtier les peuples,
149:8	pour lier leurs rois avec des chaînes, et les plus honorables parmi eux avec des ceps de fer,
149:9	pour exercer sur eux le jugement qui est écrit ! Cet honneur est pour tous ses bien-aimés. Allélou-Yah !

## Chapitre 150

### Que tout ce qui respire loue YHWH !

150:1	Allélou-Yah ! Louez El pour sa sainteté ! Louez-le à cause de ce firmament qu'il a fait par sa puissance !
150:2	Louez-le pour ses hauts faits ! Louez-le selon la grandeur de sa magnificence !
150:3	Louez-le au son du shofar ! Louez-le avec le luth et la harpe !
150:4	Louez-le avec le tambour et avec des danses ! Louez-le avec des instruments à cordes et la flûte !
150:5	Louez-le avec les cymbales retentissantes ! Louez-le avec les cymbales de cri de joie !
150:6	Que tout ce qui respire loue Yah ! Allélou-Yah !
