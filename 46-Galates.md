# Galates (Ga.)

Auteur : Paulos (Paul)

Thème : Le salut par la grâce

Date de rédaction : Env. 50 ap. J.-C.

Province antique de l'Asie Mineure, la Galatie se situait en Anatolie. Elle devait son nom à un peuple celte : les Galates (appelés aussi Gallogræci, « Gallo-Grecs ») qui s'étaient installés dans la région en 279 av. J.-C.

La lettre de Paulos (Paul) aux Galates est la seule épître dont le début ne contient pas de témoignage d'affection. Paulos commence par justifier l'origine de son appel, en employant un ton sec et sévère. Les Galates, qu'il avait lui-même évangélisés lors de son premier voyage, s'étaient promptement détournés de l'Évangile qu'ils avaient reçu. Ils ne l'avaient pas totalement abandonné, mais y avaient ajouté ce qui ne leur avait pas été prescrit. Troublés par les enseignements des judaïsants – des Juifs ayant cru en Yéhoshoua ha Mashiah, mais persistant toujours dans la pratique de la loi – les Galates avaient repris à leur compte leurs traditions, annihilant ainsi l'œuvre de la croix. Par cette lettre, Paulos les exhorte d'une part à revenir à l'Évangile véritable et d'autre part à marcher par l'Esprit afin d'en porter le fruit.

## Chapitre 1

### Introduction

1:1	Paulos, apôtre, non de la part des humains, ni par un être humain, mais par Yéhoshoua Mashiah et Elohîm le Père qui l'a<!--Vient du grec « autos » qui signifie « lui-même », « il », « le même ». Voir Ap. 1:1.--> ressuscité d'entre les morts,
1:2	et tous les frères qui sont avec moi, aux assemblées de Galatie<!--La Galatie, ou Gallo-Grèce, était une province de l'Asie Mineure (région de la Turquie actuelle). Au nord, elle était délimitée par la Bithynie et la Paphlagonie, à l'est par le Pont et la Cappadoce, au sud par la Cappadoce, la Lycaonie et la Phrygie, et à l'ouest par la Phrygie et la Bithynie. Son nom vient d'un peuple celte : les Galates (appelés aussi Gallogræci, « Gallo-Grecs ») qui s'étaient installés dans la région en 279 av. J.-C. Conquise par les Romains en 189 av. J.-C., elle devint une province de l'Empire en 25 av. J.-C.--> :
1:3	que la grâce et la paix vous soient données de la part d'Elohîm le Père et notre Seigneur Yéhoshoua Mashiah,
1:4	qui s'est donné lui-même pour nos péchés afin de nous arracher du présent âge mauvais, selon la volonté de notre Elohîm et Père.
1:5	À lui soit la gloire d'âge en âge ! Amen !

### Les Galates se détournent de l'Évangile véritable

1:6	Je m'étonne que vous vous détourniez si rapidement de celui qui vous a appelés par la grâce du Mashiah pour un autre évangile.
1:7	Non qu'il y en ait un autre, mais il y a des gens qui vous troublent et qui veulent renverser l'Évangile du Mashiah.
1:8	Mais si nous-mêmes, ou un ange du ciel vous prêche un autre évangile<!--Voir 1 R. 13:11-34.--> que celui que nous vous avons prêché, qu'il soit anathème !
1:9	Comme nous l'avons déjà dit, je le dis encore maintenant : si quelqu'un vous prêche un autre évangile que celui que vous avez reçu, qu'il soit anathème !

### L'esclave du Mashiah (Christ)

1:10	Car maintenant est-ce la faveur<!--Le mot grec signifie aussi « persuader », « se faire un ami », « convaincre », « lutter pour plaire à quelqu'un », etc.--> des humains que je désire, ou celle d'Elohîm ? Ou est-ce que je cherche à plaire aux humains ? Car si je plaisais encore aux humains, je ne serais pas un esclave du Mashiah.

### L'origine de l'appel de Paulos (Paul)

1:11	Mais je vous fais connaître, mes frères, que l'Évangile qui a été annoncé par moi n'est pas selon l'être humain.
1:12	Car je ne l'ai reçu d'aucun être humain, personne ne me l'a enseigné, mais par le moyen de la révélation de Yéhoshoua Mashiah.
1:13	Car vous avez entendu parler de ma conduite autrefois dans le judaïsme, parce que je persécutais excessivement l'Assemblée d'Elohîm et la ravageais,
1:14	et j'étais plus avancé dans le judaïsme que beaucoup de ceux de mon âge et de ma race, étant le plus ardent zélateur des traditions de mes pères.
1:15	Mais quand il a plu à Elohîm, qui m'avait choisi dès le ventre de ma mère, et qui m'a appelé par sa grâce,
1:16	de révéler en moi son Fils afin que je le prêche parmi les nations, immédiatement, je ne consultai ni la chair ni le sang,
1:17	et je ne montai pas non plus à Yeroushalaim vers ceux qui furent apôtres avant moi, mais je partis pour l'Arabie, et je revins encore à Damas.
1:18	Ensuite, 3 ans après, je montai à Yeroushalaim pour faire la connaissance<!--« s'enquérir de », « examiner », « trouver », « apprendre », « faire une enquête », « acquérir de la connaissance par une visite d'une personne distinguée », « devenir intime de », « connaître face à face ».--> de Petros et je demeurai chez lui 15 jours.
1:19	Et je ne vis aucun des autres apôtres, sinon Yaacov, le frère du Seigneur.
1:20	Or dans les choses que je vous écris, voici que devant Elohîm je ne mens pas.
1:21	J'allai ensuite dans les pays de Syrie et de Cilicie.
1:22	Or j'étais inconnu de visage aux assemblées de Judée qui sont dans le Mashiah,
1:23	mais elles avaient seulement entendu dire : Celui qui autrefois nous persécutait, prêche maintenant la foi qu'il détruisait autrefois.
1:24	Et elles glorifiaient Elohîm à cause de moi.

## Chapitre 2

### Paulos (Paul), Barnabas et Titos (Tite) à Yeroushalaim (Jérusalem)<!--Ac. 15.-->

2:1	Raison pour laquelle, 14 ans après, je montai de nouveau à Yeroushalaim<!--La grande assemblée de Yeroushalaim (Jérusalem). Voir Ac. 15.--> avec Barnabas et je pris aussi avec moi Titos.
2:2	Et ce fut d'après une révélation que j'y montai. Je leur exposai l'Évangile que je prêche parmi les nations, en particulier à ceux qui sont les plus considérés, afin de ne pas courir ou avoir couru en vain.
2:3	Mais Titos, qui était avec moi et qui est grec, n'a même pas été contraint de se faire circoncire.
2:4	Mais à cause des faux frères qui s'étaient furtivement introduits et glissés parmi nous pour épier la liberté que nous avons en Yéhoshoua Mashiah, afin de nous réduire en esclavage<!--Voir 2 Co. 11:20 ; Ro. 8:15.-->...
2:5	mais nous n'avons pas cédé un seul instant à ceux–là, nous ne nous sommes pas soumis, afin que la vérité de l'Évangile soit maintenue parmi vous.
2:6	Mais de la part de ceux qui sont les plus estimés, ce qu'ils étaient autrefois m'importe peu, Elohîm n'a pas égard à l'apparence extérieure de l'homme, car ceux qui sont en estime ne m'ont rien communiqué.
2:7	Mais au contraire, voyant que l'Évangile m'avait été confié pour l'incirconcision, comme à Petros pour la circoncision,
2:8	car celui qui a agi puissamment en Petros pour l’apostolat de la circoncision, a aussi agi puissamment en moi pour les nations.
2:9	Et Yaacov, Kephas et Yohanan, qui paraissaient être des colonnes, ayant reconnu la grâce que j'avais reçue, me donnèrent, à moi et à Barnabas, la main d'association, afin que nous allions, nous vers les nations, et eux vers la circoncision.
2:10	Nous devions seulement nous souvenir des pauvres, ce que je me suis aussi efforcé de faire.

### Paulos (Paul) reprend Petros (Pierre) à Antioche

2:11	Mais lorsque Petros vint à Antioche, je lui résistai en face parce qu’il était condamné<!--« Trouver une faute », « blâmer », « accuser », « condamner ».-->.
2:12	Car, avant la venue de quelques personnes de la part de Yaacov, il mangeait avec les nations, mais quand elles furent venues, il se cacha et se sépara d'eux, craignant ceux de la circoncision.
2:13	Les autres Juifs aussi agirent d'une manière hypocrite avec lui, de sorte que Barnabas même fut entraîné par leur hypocrisie<!--Le rôle d'un joueur de théâtre.-->.
2:14	Mais, quand je vis qu'ils ne marchaient pas dans le droit chemin selon la vérité de l'Évangile, je dis à Petros, devant tous : Si toi qui es Juif, tu vis à la manière des nations et non à la manière des Juifs, pourquoi contrains-tu les nations à judaïser ?

### La justification par la foi en Yéhoshoua ha Mashiah (Jésus-Christ)

2:15	Nous, juifs de nature, et non pas pécheurs d'entre les nations,
2:16	sachant que l'être humain n'est pas justifié en vertu des œuvres de la torah, mais par le moyen de la foi en Yéhoshoua Mashiah<!--La justification. Voir Ro. 5:1.-->, nous aussi nous avons cru en Yéhoshoua Mashiah, afin que nous soyons justifiés par la foi en Mashiah et non pas par les œuvres de la torah, parce qu'aucune chair ne sera justifiée par les œuvres de la torah.
2:17	Or si en cherchant à être justifiés par Mashiah, nous sommes aussi trouvés pécheurs, alors Mashiah est-il serviteur du péché ? Que cela n'arrive jamais !
2:18	Car si je rebâtis les choses que j'ai renversées, je montre que je suis moi-même un transgresseur.
2:19	Car c'est au moyen de la torah que je suis mort à la torah, afin de vivre pour Elohîm.
2:20	Et je suis crucifié avec Mashiah. Et si je vis, ce n'est plus moi qui vis, mais c'est Mashiah qui vit en moi. Si je vis maintenant dans la chair, je vis dans la foi au Fils d'Elohîm qui m'a aimé et qui s'est livré lui-même pour moi.
2:21	Je n'annule pas la grâce d'Elohîm, car si la justice est par le moyen de la torah, Mashiah est donc mort gratuitement.

## Chapitre 3

### L'Esprit est donné à ceux qui ont la foi

3:1	Ô Galates dénués d'intelligence<!--Lu. 24:25.--> ! Qui vous a ensorcelés pour que vous n'obéissiez plus à la vérité, vous, aux yeux de qui Yéhoshoua Mashiah a été ouvertement dépeint crucifié au milieu de vous ?
3:2	Je voudrais seulement entendre ceci de vous : Avez-vous reçu l'Esprit par les œuvres de la torah ou par la prédication de la foi ?
3:3	Êtes-vous ainsi dénués d'intelligence ? Ayant commencé par l'Esprit, finiriez-vous maintenant par la chair ?
3:4	Avez-vous tant souffert en vain ? Si toutefois c'est en vain.
3:5	Celui donc qui vous fournit l'Esprit et qui opère en vous des miracles, est-ce par les œuvres de la torah ou par la prédication de la foi ?

### Les fils d'Abraham<!--Ro. 4.-->

3:6	Comme Abraham crut à Elohîm, et cela lui fut compté comme justice<!--Voir Ge. 15:6.-->,
3:7	sachez donc que ce sont ceux qui ont la foi qui sont enfants d'Abraham.
3:8	Mais l'Écriture, prévoyant qu'Elohîm justifierait les nations par la foi, a prêché d'avance l'Évangile à Abraham en lui disant : Toutes les nations seront bénies en toi<!--Ge. 12:3.-->.
3:9	C'est pourquoi ceux qui ont la foi sont bénis avec Abraham le croyant.

### L'attachement aux œuvres de la torah produit la malédiction

3:10	Car tous ceux qui sont des œuvres de la torah sont sous la malédiction, car il est écrit : Maudit est quiconque ne persévère pas dans toutes les choses qui sont écrites dans le livre de la torah et ne les met pas en pratique<!--De. 27:26.-->.
3:11	Et que personne ne soit justifié devant Elohîm par la torah, cela est évident, puisqu'il est dit : Le juste vivra par la foi<!--Ha. 2:4.-->.
3:12	Or la torah ne procède pas de la foi, mais elle dit : L'être humain qui mettra ces choses en pratique vivra par elles<!--Lé. 18:5.-->.

### Yéhoshoua ha Mashiah (Jésus-Christ) a racheté les chrétiens de la malédiction de la torah

3:13	Mashiah nous a rachetés de la malédiction de la torah en devenant malédiction pour nous, car il est écrit : Maudit quiconque est pendu au bois<!--De. 21:23.-->,
3:14	afin que la bénédiction d'Abraham parvienne aux nations en Mashiah Yéhoshoua, et que nous recevions par le moyen de la foi l'Esprit de la promesse.
3:15	Frères, je parle à la manière des humains : un testament en bonne forme, bien que fait par un être humain, n'est annulé par personne et personne n'y ajoute.
3:16	Or les promesses ont été faites à Abraham et à sa postérité<!--Ge. 15:5, 22:18.-->. Il n'est pas dit : Et aux postérités, comme s'il avait parlé de beaucoup, mais comme d'une seule : et à ta postérité, c'est-à-dire Mashiah.
3:17	Mais voici ce que je dis : une alliance qu'Elohîm a établie d'avance en Mashiah ne peut pas être annulée par la torah survenue 430 ans plus tard, ce qui abolirait la promesse.
3:18	Car si l'héritage venait de la torah, il ne viendrait plus de la promesse. Or c'est par la promesse qu'Elohîm a fait à Abraham ce don de sa grâce.

### La torah, un pédagogue qui conduit au Mashiah (Christ)

3:19	Alors pourquoi la torah ? Elle a été ajoutée à cause des transgressions, jusqu'à ce que vienne la postérité à qui la promesse avait été faite. Et elle a été prescrite<!--Le mot grec signifie aussi « arranger », « nommer », « ordonner », « donner un ordre ». Mot associé « promulguer ».--> par le moyen des anges et par l'entremise d'un médiateur.
3:20	Or le médiateur n'est pas pour un seul, mais Elohîm est un<!--Za. 14:9 ; 1 Ti. 2:5 ; Ja. 2:19 ; Mc. 12:29.-->.
3:21	La torah est-elle donc contre les promesses d'Elohîm ? Nullement ! Car s'il avait été donné une torah qui puisse procurer la vie, la justice viendrait vraiment de la torah.
3:22	Mais l'Écriture a enfermé ensemble toutes choses sous le péché, afin que la promesse soit donnée par la foi en Yéhoshoua Mashiah à ceux qui croient.
3:23	Or avant que la foi vienne, nous étions gardés<!--« Garder, protéger par une garde militaire, que ce soit pour prévenir une invasion hostile ou pour empêcher les gens de s'enfuir d'une ville assiégée ».--> sous la torah, enfermés ensemble, en vue de la foi qui devait être révélée.
3:24	Ainsi la torah a donc été notre pédagogue<!--Le mot « pédagogue » du grec « paidagogos » : « celui qui dirige un garçon ». Un pédagogue était un tuteur, un gardien et un guide de garçons. Parmi les Grecs et les Romains, le mot était appliqué aux esclaves dignes de confiance qui étaient chargés de veiller à la vie et à la moralité des garçons appartenant aux classes supérieures. Les garçons ne pouvaient faire le moindre pas hors de la maison sans ces tuteurs tant qu'ils n'avaient pas atteint leur majorité.--> jusqu'au Mashiah, afin que nous soyons justifiés par la foi.
3:25	Mais la foi étant venue, nous ne sommes plus sous ce pédagogue.

### L'unité en Yéhoshoua ha Mashiah (Jésus-Christ)

3:26	Car vous êtes tous fils d'Elohîm par le moyen de la foi en Mashiah Yéhoshoua,
3:27	car vous tous qui avez été baptisés en Mashiah, vous avez revêtu Mashiah.
3:28	Il n'y a plus ni Juif ni Grec, il n'y a plus ni esclave ni libre, il n'y a plus ni mâle ni femelle, car vous êtes tous un en Yéhoshoua Mashiah<!--Ro. 10:12 ; Col. 3:11.-->.
3:29	Mais si vous êtes du Mashiah, vous êtes donc la postérité d'Abraham et héritiers selon la promesse.

## Chapitre 4

### L'esclave des rudiments du monde

4:1	Mais aussi longtemps que l'héritier est enfant<!--« Enfant », du grec « nepios », signifie aussi « ignorant ».-->, je dis qu'il ne diffère en rien d'un esclave, quoiqu'il soit le seigneur de tout.
4:2	Mais il est sous des tuteurs et des gestionnaires jusqu'au temps déterminé par le Père.
4:3	Nous aussi, lorsque nous étions enfants, nous étions sous l'esclavage des rudiments du monde.
4:4	Mais lorsque l'accomplissement du temps est venu, Elohîm a envoyé son Fils, venu d'une femme, venu sous la torah,
4:5	afin qu'il rachète ceux qui étaient sous la torah, afin que nous recevions l'adoption.
4:6	Mais parce que vous êtes fils, Elohîm a envoyé l'Esprit de son Fils dans vos cœurs, criant : Abba ! Père !
4:7	Maintenant donc tu n'es plus esclave, mais fils. Or si tu es fils, tu es aussi héritier d'Elohîm par le moyen du Mashiah.
4:8	Mais alors, ne connaissant pas Elohîm, vous étiez en effet esclaves<!--Ou « vous étiez des esclaves ».--> des elohîm qui ne le sont pas de leur nature.
4:9	Mais maintenant que vous avez connu Elohîm, ou plutôt que vous avez été connus d'Elohîm, comment retournez-vous encore à ces faibles et pauvres rudiments, et vouloir comme auparavant en être esclaves ?
4:10	Vous observez attentivement les jours, et les mois, et les temps, et les années.
4:11	Je crains pour vous d’avoir travaillé en vain parmi vous.
4:12	Soyez comme moi, car je suis aussi comme vous. Frères, je vous en supplie.
4:13	Vous ne m'avez fait aucun tort. Mais vous savez que ce fut à cause d'une infirmité de la chair<!--Les Écritures ne donnent pas de précisions au sujet de l'infirmité de la chair dont souffrait Paulos (Paul). On suppose toutefois qu'il avait un handicap au niveau de ses yeux. Quatre arguments viennent renforcer cette hypothèse. Tout d'abord, l'allusion de Paulos aux Galates qui étaient prêts à « s'arracher les yeux » pour les lui donner (Ga. 4:15) et le fait qu'il ait lui-même écrit cette épître avec de « grandes lettres » (Ga. 6:11). Ensuite, lors de sa comparution devant le sanhédrin à Yeroushalaim (Jérusalem), Paulos n'a pas reconnu le grand-prêtre pourtant facilement identifiable par sa tenue vestimentaire (Ac. 23:5). Enfin, l'apôtre avait l'habitude de dicter ses lettres, ce qui constitue un argument majeur. L'épître aux Galates était une exception parce qu'il n'avait sans doute pas de secrétaire à disposition.--> que je vous ai pour la première fois évangélisés.
4:14	Et, malgré ce qui dans ma chair était une tentation, vous ne m'avez ni méprisé, ni rejeté. Mais vous m'avez reçu comme un ange d'Elohîm, comme Mashiah Yéhoshoua.
4:15	Quel était donc votre bonheur ? Car je vous rends témoignage que, si cela avait été possible, vous vous seriez arrachés les yeux pour me les donner.
4:16	Suis-je donc devenu votre ennemi en vous disant la vérité ?
4:17	Le zèle qu'ils ont pour vous n'est pas vrai, mais ils veulent nous empêcher de vous approcher afin que vous soyez zélés pour eux.
4:18	Mais il est bon d'être toujours zélé pour ce qui est bon, et non pas seulement quand je suis présent parmi vous.

### Les fils de l'esclave et les fils de la femme libre

4:19	Mes petits enfants, pour qui j'éprouve de nouveau les douleurs de l'enfantement, jusqu'à ce que Mashiah soit formé en vous,
4:20	mais je voudrais être maintenant avec vous et changer mon langage, parce que je suis perplexe à votre sujet.
4:21	Dites-moi, vous qui voulez être sous la torah, n’entendez-vous pas la torah ?
4:22	Car il est écrit qu'Abraham eut deux fils, un de l'esclave, et un de la femme libre.
4:23	Mais celui de l'esclave fut engendré en effet selon la chair, et celui de la femme libre en vertu de la promesse.
4:24	Ces choses sont allégoriques, car ce sont les deux alliances. En effet, l'une du Mont Sinaï, qui engendre pour l'esclavage, c'est Agar.
4:25	Car le nom d'Agar veut dire Sinaï, qui est une montagne en Arabie correspondant à la Yeroushalaim actuelle qui est dans la servitude avec ses enfants.
4:26	Mais la Yeroushalaim d'en haut est la femme libre, et c'est notre mère à nous tous.
4:27	Car il est écrit : Réjouis-toi, stérile, toi qui n'enfantes pas ! Éclate et pousse des cris, toi qui n'as pas éprouvé les douleurs de l'enfantement ! Car les enfants de la délaissée seront plus nombreux que les enfants de celle qui était mariée<!--Es. 54:1.-->.
4:28	Or pour nous, frères, nous sommes enfants de la promesse comme Yitzhak.
4:29	Mais de même qu’alors celui qui avait été engendré selon la chair persécutait celui qui était selon l'Esprit, il en est de même maintenant.
4:30	Mais que dit l'Écriture ? Chasse l'esclave et son fils, car le fils de l'esclave n'héritera pas avec le fils de la femme libre<!--Ge. 21:10.-->.
4:31	C'est pourquoi, frères, nous ne sommes pas enfants de l'esclave, mais de la femme libre.

## Chapitre 5

### Yéhoshoua ha Mashiah (Jésus-Christ) nous a libérés de la servitude

5:1	Demeurez donc fermes dans la liberté pour laquelle Mashiah nous a rendus libres<!--Voir Jn. 8:32,36 ; Ro. 6:18,22, 8:2,21.-->, et ne soyez pas de nouveau pris au piège par le joug de l'esclavage.
5:2	Moi, Paulos, je vous dis que si vous vous faites circoncire, Mashiah ne vous servira à rien.
5:3	Et j'affirme encore une fois à tout homme qui se fait circoncire qu'il est obligé de pratiquer la torah tout entière.
5:4	Vous êtes séparés du Mashiah, vous tous qui vous justifiez par la torah, vous êtes déchus de la grâce.
5:5	Car nous, c'est par l'Esprit, en vertu de la foi, que nous attendons assidûment et patiemment l'espérance de la justice.
5:6	Car, dans le Mashiah Yéhoshoua, ni la circoncision ni le prépuce<!--Voir le commentaire en 1 Co. 7:18.--> n'ont de valeur, mais seulement la foi qui opère par le moyen de l'amour.
5:7	Vous couriez bien. Qui vous a arrêtés pour que vous ne soyez plus persuadés<!--Vient du grec « peitho ». Voir Mt. 27:20 ; Hé. 13:17, etc.--> par la vérité ?
5:8	Cette persuasion trompeuse ne vient pas de celui qui vous appelle.
5:9	Un peu de levain fait lever toute la pâte<!--1 Co. 5:6.-->.
5:10	J'ai cette confiance en vous dans le Seigneur que vous n'aurez pas d'autre pensée. Mais celui qui vous trouble, quel qu'il soit, en portera la condamnation.
5:11	Mais moi, frères, si je prêche encore la circoncision, pourquoi suis-je encore persécuté ? Le scandale de la croix est donc aboli.
5:12	Je voudrais aussi qu'ils soient retranchés, ceux qui vous agitent.
5:13	Car vous, frères, vous avez été appelés à la liberté. Seulement, ne faites pas de cette liberté une occasion pour la chair. Mais, par le moyen de l'amour, soyez esclaves les uns des autres.
5:14	Car toute la torah est accomplie dans cette seule parole : Tu aimeras ton prochain comme toi-même<!--Lé. 19:18 ; Mt. 22:39 ; Ga. 5:14 ; Ro. 13:9.-->.
5:15	Mais si vous vous mordez et vous vous dévorez les uns les autres, prenez garde que vous ne soyez détruits les uns par les autres.
5:16	Mais je dis : Marchez selon l'Esprit et vous n'accomplirez pas les désirs de la chair.

### La chair et ses œuvres s'opposent à l'Esprit d'Elohîm<!--Ro. 8:2.-->

5:17	Car la chair a des désirs contraires à ceux de l'Esprit, et l'Esprit en a des contraires à ceux de la chair. Et ils sont opposés entre eux, afin que vous ne fassiez pas ce que vous voudriez.
5:18	Or si vous êtes conduits par l'Esprit, vous n'êtes pas sous la torah.
5:19	Mais les œuvres de la chair sont évidentes : ce sont l'adultère, la relation sexuelle illicite, l'impureté, la luxure sans bride,
5:20	l'idolâtrie, la sorcellerie<!--La sorcellerie : du grec « pharmakeia » : « usage ou administration de drogues », « empoisonnement », « sorcellerie », « arts magiques », souvent trouvés en liaison avec l'idolâtrie et nourrie par celle-ci.-->, les inimitiés, les querelles, les jalousies, les animosités, les esprits de parti<!--Voir Ph. 2:3.-->, les divisions, les sectes,
5:21	les envies, les meurtres, les ivrogneries, les orgies, et les choses semblables à celles-là, au sujet desquelles je vous prédis, comme je vous l'ai déjà dit, que ceux qui commettent de telles choses n'hériteront pas le Royaume d'Elohîm.

### Le fruit de l'Esprit<!--Jn. 15:1-5 ; Ga. 2:20.-->

5:22	Mais le fruit de l'Esprit c'est l'amour<!--Il est question ici de l'amour « agape » : l'amour fraternel, l'amour désintéressé.-->, la joie, la paix, la patience, la bonté, la bienveillance<!--Vient d'un mot grec qui signifie aussi « intégrité, bénignité ».-->, la foi, la douceur, le contrôle de soi.
5:23	La torah n'est pas contre ces choses.
5:24	Mais ceux qui sont au Mashiah ont crucifié la chair avec ses passions et ses désirs.
5:25	Si nous vivons par l'Esprit, marchons aussi par l'Esprit.
5:26	Ne cherchons pas une vaine gloire en nous provoquant les uns les autres et en nous portant envie les uns aux autres.

## Chapitre 6

6:1	Frères, même si quelqu'un est surpris en quelque faute, vous qui êtes spirituels, redressez-le<!--Vient du grec « katartizo » qui signifie « redresser », « ajuster », « compléter », « réparer », « équiper », « mettre en ordre », « arranger », « ajuster ». Voir Mt. 4:21 ; Lu. 6:40.--> avec un esprit de douceur. Prends garde à toi-même, de peur que tu ne sois aussi tenté.
6:2	Portez les fardeaux les uns des autres, et vous accomplirez ainsi la torah du Mashiah.
6:3	Car si quelqu'un pense être quelque chose, quoiqu'il ne soit rien, il se trompe lui-même.
6:4	Mais que chacun éprouve<!--Voir 1 Ti. 3:10.--> ses propres œuvres, et alors il aura de quoi se glorifier pour lui-même seulement, et non par rapport aux autres.
6:5	Car chacun portera son propre fardeau.
6:6	Que celui à qui l'on enseigne la parole soit en communion avec celui qui l'enseigne pour tout ce qui est bon<!--Le mot « bon » vient du grec « agathos » qui donne en français : « de bonne constitution ou nature », « utile », « salutaire », « bon », « agréable », « plaisant », « joyeux », « heureux », « excellent », « distingué », « droit », « honorable ». Ce terme n'a rien à voir avec les biens matériels (Voir Ga. 6:10). Ce verset ne doit en aucun cas servir de prétexte à ceux qui enseignent la parole d'Elohîm pour exiger l'argent et les biens matériels des chrétiens. Ces derniers doivent donner sans contrainte, s'ils le veulent et comme ils le veulent (2 Co. 9:7). Le salaire de l'ouvrier du Seigneur, c'est avant tout le gîte et le couvert (Mt. 10:10 ; Lu. 10:8 ; 1 Ti. 6:8). Ainsi, malgré le droit qu'il avait de moissonner les biens matériels pour avoir semé des biens spirituels (1 Co. 9:11-12), Paulos (Paul) « n'a désiré ni l'or ni l'argent » mais a travaillé de ses propres mains afin de pourvoir à ses besoins et de n'être à la charge de personne (Ac. 20:33-35 ; 1 Th. 2:9 ; 2 Th. 3:8 ; 2 Co. 12:14).-->.

### Semer pour sa chair et semer pour l'Esprit

6:7	Ne vous égarez pas : on ne se moque pas d'Elohîm. Ce qu'un être humain aura semé, il le moissonnera aussi.
6:8	C'est pourquoi celui qui sème pour sa chair moissonnera de la chair la corruption, mais celui qui sème pour l'Esprit moissonnera de l'Esprit la vie éternelle.
6:9	Et ne perdons pas courage en pratiquant ce qui est bon, car nous moissonnerons au temps convenable, si nous ne nous relâchons pas.
6:10	C'est pourquoi, pendant que nous en avons le temps, pratiquons ce qui est bon envers tous, mais principalement envers ceux qui sont de la famille de la foi.
6:11	Vous voyez avec quelles grandes lettres je vous ai écrit de ma propre main.
6:12	Tous ceux qui veulent se rendre agréables selon la chair vous contraignent à vous faire circoncire, uniquement afin de ne pas être persécutés pour la croix du Mashiah.
6:13	Car les circoncis eux-mêmes n'observent pas la torah, mais ils veulent que vous soyez circoncis pour se glorifier dans votre chair.
6:14	Mais pour ce qui me concerne, loin de moi la pensée de me glorifier d'autre chose que de la croix de notre Seigneur Yéhoshoua Mashiah, par qui le monde est crucifié pour moi, comme je le suis pour le monde !
6:15	Car en Yéhoshoua Mashiah ce n'est ni la circoncision, ni l'incirconcision qui peuvent quelque chose, mais la nouvelle création.
6:16	Et que la paix et la miséricorde soient sur tous ceux qui marcheront selon cette règle<!--Voir Ph. 3:16.-->, et sur l'Israël d'Elohîm !

### Conclusion

6:17	Au reste, que personne ne me fasse de la peine, car je porte sur mon corps les marques du Seigneur Yéhoshoua.
6:18	Frères, que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec votre esprit ! Amen !
