# 1 Shemouél (1 Samuel) (1 S.)

Signification : Entendu, exaucé de El

Auteur : Inconnu

Thème : Histoire de Shemouél (Samuel), Shaoul (Saül) et David

Date de rédaction : 10ème siècle av. J.-C.

Shemouél était le fils d'Elkana, de la Montagne d'Éphraïm. Channah (Anne), sa mère, avait longtemps désiré un enfant. Elle fit donc une alliance avec Elohîm en lui promettant de lui consacrer son premier fils s'il la rendait féconde. Ainsi, dès son plus jeune âge, Shemouél fut amené à la maison d'Elohîm où il grandit aux côtés d'Éli, le prêtre. À la mort de ce dernier, Shemouél exerça les fonctions de juge, prêtre et prophète. C'est en son temps qu'Israël exprima le désir d'avoir un roi, marquant ainsi la fin de l'ère des juges et le début de la monarchie en Israël.

Ce livre relate l'histoire de Shaoul, premier roi d'Israël, à qui YHWH accorda de puissantes victoires notamment sur les Philistins, grands ennemis du peuple d'Elohîm. Mais très vite, Shaoul s'écarta de la volonté d'Elohîm, aussi YHWH le disqualifia et choisit pour lui succéder au trône un homme de la tribu de Yéhouda : David, fils d'Isaï. Son accession à la royauté ne fut pas immédiate. David dut faire preuve de patience, de courage et de foi en son Elohîm au milieu de nombreuses persécutions. L'expérience des deux premiers rois d'Israël est une exhortation à l'obéissance à Elohîm.

## Chapitre 1

### Stérilité de Channah (Anne)

1:1	Il y avait un homme de Ramathaïm-Tsophim, de la Montagne d'Éphraïm, du nom d'Elkana, fils de Yeroham, fils d'Élihou, fils de Tohou, fils de Tsouph, Éphratien.
1:2	Il avait deux femmes : le nom de l'une était Channah, et le nom de la seconde, Peninna. Peninna avait des enfants, mais Channah n'avait pas d'enfants.
1:3	D'année en année, cet homme montait de sa ville à Shiyloh<!--Jos. 18:1.--> pour adorer YHWH Sabaoth<!--YHWH des armées. Voir Es. 8:13 et Ap. 19.--> et lui offrir des sacrifices. Là étaient les deux fils d'Éli, Hophni et Phinées, prêtres de YHWH.
1:4	Le jour où Elkana offrait son sacrifice, il donnait des portions à Peninna, sa femme, à tous les fils et à toutes les filles qu'il avait d'elle.
1:5	Mais il donnait à Channah une portion double, car il aimait Channah, mais YHWH avait fermé sa matrice<!--Elohîm est celui qui ferme et ouvre les portes des bénédictions.-->.
1:6	Sa rivale la vexait pour la pousser à s'irriter parce que YHWH avait fermé sa matrice.
1:7	C’est ainsi qu’elle agissait, d’année en année, chaque fois qu’elle montait à la maison de YHWH. C’est ainsi qu'elle lui causait du chagrin, si bien qu'elle pleurait et ne mangeait pas.
1:8	Elkana, son mari, lui disait : Channah, pourquoi pleures-tu et pourquoi ne manges-tu pas ? Pourquoi ton cœur est-il triste ? Est-ce que je ne vaux pas pour toi mieux que dix fils ?

### Prière et vœu de Channah à YHWH

1:9	Channah se leva après avoir mangé et bu à Shiyloh. Et le prêtre Éli était assis sur un siège, près de l'un des poteaux du temple de YHWH.
1:10	Elle donc, ayant le cœur rempli d'amertume, pria YHWH en pleurant, en pleurant.
1:11	Et elle fit un vœu, en disant : YHWH Sabaoth ! Si tu regardes, si tu regardes l'affliction de ta servante, et si tu te souviens de moi, et n'oublies pas ta servante, et que tu donnes à ta servante un enfant mâle, je le donnerai à YHWH pour tous les jours de sa vie, et aucun rasoir ne passera sur sa tête.
1:12	Il arriva, comme elle continuait à prier devant YHWH, qu'Éli observait sa bouche.
1:13	Or Channah parlait dans son cœur, elle ne faisait que remuer ses lèvres et on n'entendait pas sa voix. C'est pourquoi Éli estima qu'elle était ivre,
1:14	et Éli lui dit : Jusqu'à quand seras-tu ivre ? Éloigne-toi du vin.
1:15	Mais Channah répondit et dit : Je ne suis pas ivre, mon seigneur, je suis une femme affligée en son esprit, je n'ai bu ni vin ni boisson forte, mais je répandais mon âme devant YHWH.
1:16	Ne mets pas ta servante au rang d'une fille de Bélial<!--Voir commentaire en De. 13:13.-->, car c'est l'excès de ma douleur et de mon affliction qui m'a fait parler jusqu'à présent.
1:17	Alors Éli répondit et dit : Va en paix, et que l'Elohîm d'Israël veuille t'accorder la demande que tu lui as faite !
1:18	Et elle dit : Que ta servante trouve grâce à tes yeux ! Puis cette femme poursuivit son voyage. Elle mangea, et son visage ne fut plus le même.

### Naissance de Shemouél (Samuel)

1:19	Après cela, ils se levèrent de bon matin et se prosternèrent devant YHWH, puis ils s'en retournèrent et revinrent dans leur maison à Ramah. Elkana connut Channah, sa femme, et YHWH se souvint d'elle.
1:20	Il arriva donc, quelque temps après, que Channah conçut et enfanta un fils. Elle l'appela du nom de Shemouél, parce que, dit-elle, je l'ai demandé à YHWH.
1:21	Puis Elkana son mari monta avec toute sa maison pour offrir à YHWH le sacrifice annuel et son vœu.
1:22	Mais Channah n'y monta pas, car elle dit à son mari : Je n'irai pas jusqu'à ce que le garçon soit sevré, et alors je le mènerai, afin qu'il soit présenté devant YHWH et qu'il demeure toujours-là.
1:23	Elkana son mari lui dit : Fais ce qui est bon à tes yeux, reste jusqu'à ce que tu l'aies sevré. Seulement que YHWH accomplisse sa parole ! Ainsi, cette femme resta et allaita son fils, jusqu'à ce qu'elle l'ait sevré.

### Shemouél (Samuel) chez Éli : Channah accomplit son vœu

1:24	Et dès qu'elle l'eut sevré, elle le fit monter avec elle, et ayant pris 3 veaux, un épha de farine et une outre de vin, elle le mena dans la maison de YHWH à Shiyloh. Le garçon était encore jeune.
1:25	Puis ils égorgèrent un veau, et ils amenèrent le garçon à Éli.
1:26	Elle dit : Excuse-moi, mon seigneur ! Aussi vrai que ton âme vit, mon seigneur, je suis cette femme qui me tenais en ta présence pour prier YHWH.
1:27	J'ai prié pour avoir ce garçon, et YHWH m'a accordé la demande que je lui ai faite.
1:28	C'est pourquoi je le prête à YHWH : il sera prêté à YHWH pour tous les jours de sa vie. Et ils se prosternèrent là devant YHWH.

## Chapitre 2

### Prière de Channah

2:1	Alors Channah pria et dit : Mon cœur se réjouit en YHWH, ma force<!--Littéralement « corne ».--> a été relevée par YHWH. Ma bouche s'est ouverte contre mes ennemis, parce que je me suis réjouie de ton salut<!--Le mot « salut » vient de l'hébreu « yeshuw'ah » c'est-à-dire « Yéhoshoua » (Jésus). Voir commentaire en Es. 26:1.-->.
2:2	Nul n'est saint comme YHWH. Car il n'y en a pas d'autres que toi, il n'y a pas de rocher<!--Voir commentaire en Es. 8:13-17.--> tel que notre Elohîm.
2:3	Ne proférez pas tant de paroles hautaines ! Qu'il ne sorte pas de votre bouche des paroles arrogantes. Car YHWH est le El qui sait tout, c'est lui qui pèse toutes les actions.
2:4	L'arc des puissants est brisé, mais ceux qui chancellent ont la force pour ceinture.
2:5	Ceux qui étaient rassasiés se louent pour du pain, mais les affamés ont cessé de l'être. Même la stérile en a enfanté sept, et celle qui avait beaucoup de fils est devenue languissante.
2:6	YHWH est celui qui fait mourir et qui fait vivre, qui fait descendre au shéol et qui en fait remonter.
2:7	YHWH appauvrit et il enrichit, il abaisse et il élève.
2:8	Il élève le pauvre de la poussière, et il tire le misérable de dessus le fumier, pour le faire asseoir avec les nobles. Et il leur donne en héritage un trône de gloire. Car les colonnes de la Terre sont à YHWH, et il a posé sur eux la terre habitable.
2:9	Il gardera les pieds de ses bien-aimés, et les méchants se tairont dans les ténèbres. Car l'homme ne triomphera pas par sa force.
2:10	Ceux qui contestent avec YHWH seront brisés. Des cieux, il lancera son tonnerre sur chacun d'eux. YHWH jugera les extrémités de la Terre. Et il donnera la force à son Roi<!--Le Roi dont il est question ici est le Seigneur Yéhoshoua ha Mashiah (Jésus-Christ), le Roi des rois (Za. 14:9 ; Ap. 19:16).-->, et élèvera la corne de son Mashiah<!--Channah (Anne) a annoncé la glorification ou la résurrection du Seigneur Yéhoshoua, le Mashiah (Jésus-Christ) (Jn. 3:14).-->.
2:11	Puis Elkana s'en alla à Ramah dans sa maison, et le jeune garçon resta au service de YHWH, en présence du prêtre Éli.

### Corruption des fils d'Éli

2:12	Or les fils d'Éli<!--Les fils d'Éli, Hophni et Phinées étaient corrompus. Ils volaient les offrandes d'Elohîm, couchaient avec les femmes qui venaient servir et adorer Elohîm. L'esprit qui animait ces prêtres n'a pas disparu après leur mort, mais il opère encore dans beaucoup d'institutions religieuses actuelles. Beaucoup de dirigeants d'assemblées continuent à s'approprier ce qui appartient à Elohîm (l'adoration, les âmes...). Ils ne craignent pas YHWH. Ils abusent de leur position et de leur autorité pour contraindre leurs fidèles à leur donner la dîme et toutes sortes d'offrandes. Ils font payer les entretiens, les prières, et les divers dons qu'ils peuvent avoir. Non seulement l'esprit qui animait les fils d'Éli existe encore, mais il s'est accru en ces temps actuels.--> étaient des fils de Bélial<!--Voir commentaire en De. 13:13. Ce terme est également utilisé au sujet des méchants qui incitèrent les Israélites à servir les dieux étrangers (De. 13:12-14), des hommes iniques de Guibea (Jg. 19:22, 20:13), des deux vauriens qui accusèrent Naboth (1 R. 21:10-13) et des individus qui s'opposèrent à la monarchie (1 S. 10:27 ; 2 S. 20:1 ; 2 Ch. 13:7). Voir aussi Job 34:18 ; Ps. 18:4, 34:17 ; Pr. 6:12, 16:27, 19:28 ; Na. 1:11.--> et ils ne connaissaient pas YHWH.
2:13	Et voici la coutume de ces prêtres envers le peuple : lorsque quelqu'un faisait quelque sacrifice, le serviteur du prêtre venait lorsqu'on faisait bouillir la chair, ayant à la main une fourchette à trois dents,
2:14	avec laquelle il piquait dans la chaudière, dans le chaudron, dans la marmite, dans le pot, et le prêtre prenait pour lui tout ce que la fourchette enlevait. C'est ainsi qu'ils agissaient envers tous ceux d'Israël qui venaient à Shiyloh.
2:15	Même avant qu'on fasse brûler la graisse, le serviteur du prêtre venait et disait à l'homme qui sacrifiait : Donne-moi de la chair à rôtir pour le prêtre, car il ne prendra pas de toi de chair bouillie, mais de la chair crue.
2:16	Et si l'homme lui répondait : On va d'abord faire brûler la graisse, et après cela tu prendras ce que ton âme souhaitera, alors le serviteur lui disait : Quoi qu'il en soit, tu en donneras maintenant, sinon j'en prendrai de force.
2:17	Et le péché de ces jeunes hommes fut très grand devant YHWH, car ces hommes méprisaient l'offrande de YHWH.

### Shemouél (Samuel) au service de YHWH

2:18	Shemouél faisait le service en présence de YHWH, étant jeune garçon, ceint d'un éphod en lin.
2:19	Sa mère lui faisait une petite tunique, qu'elle lui apportait d'année en année, quand elle montait avec son mari pour offrir le sacrifice annuel.
2:20	Éli bénit Elkana et sa femme, et dit : Que YHWH te donne des enfants de cette femme, pour le prêt qu'elle a fait à YHWH ! Et ils s'en retournèrent chez eux.
2:21	Et YHWH visita Channah, elle conçut, et enfanta trois fils et deux filles. Et le jeune garçon Shemouél grandissait en présence de YHWH.

### Éli averti des péchés commis par ses fils

2:22	Or Éli était très vieux. Il entendait dire comment ses fils agissaient à l'égard de tout Israël, et comment ils couchaient avec les femmes qui s'assemblaient à la porte de la tente d'assignation.
2:23	Et il leur dit : Pourquoi commettez-vous de telles choses ? Car j'apprends vos méchantes actions de tout le peuple.
2:24	Ne faites plus cela, mes fils, car ce que j'entends dire de vous n'est pas bon. Vous faites pécher le peuple de YHWH.
2:25	Si un homme a péché contre un autre homme, le Juge<!--Ici, le mot « juge » vient de l'hébreu « elohîm ». Elohîm est le juste Juge (Ec. 3:17 ; Ac. 10:42).--> interviendra, mais si quelqu'un pèche contre YHWH, qui interviendra pour lui ? Mais ils n'obéirent pas à la voix de leur père parce que YHWH voulait les faire mourir.
2:26	Cependant le jeune garçon Shemouél croissait, et il était agréable à YHWH et aux hommes.
2:27	Or un homme d'Elohîm vint auprès d'Éli, et lui dit : Ainsi parle YHWH : Ne me suis-je pas révélé, révélé à la maison de ton père, quand ils étaient en Égypte, dans la maison de pharaon ?
2:28	Je l'ai choisi parmi toutes les tribus d'Israël pour être mon prêtre, pour monter à mon autel, pour brûler de l'encens, pour porter l'éphod devant moi, et j'ai donné à la maison de ton père tous les holocaustes des enfants d'Israël.
2:29	Pourquoi avez-vous foulé aux pieds mes sacrifices et mes offrandes, que j'ai ordonné de faire dans ma demeure ? Et pourquoi as-tu honoré tes fils plus que moi, afin de vous engraisser de toutes les premières offrandes d'Israël, mon peuple ?
2:30	C'est pourquoi voici ce que dit YHWH, l'Elohîm d'Israël : J'avais dit et promis que ta maison et la maison de ton père marcheraient devant moi éternellement. Et YHWH dit : Loin de moi cela ! Car j'honorerai ceux qui m'honorent, mais ceux qui me méprisent seront méprisés.
2:31	Voici, les jours viennent où je couperai ton bras, et le bras de la maison de ton père, de telle sorte qu'il n'y ait plus de vieillards dans ta maison.
2:32	Tu verras un adversaire dans ma demeure, et tout le bien qu'il fera à Israël, et il n'y aura plus jamais de vieillards dans ta maison.
2:33	Il y aura un homme des tiens que je ne retrancherai pas d'auprès de mon autel afin de consumer tes yeux et affliger ton âme, mais tous les hommes qui viendront accroître ta maison mourront prématurément.
2:34	Ce qui arrivera à tes deux fils, Hophni et Phinées, sera pour toi un signe : ils mourront tous les deux le même jour.
2:35	Et je m'établirai un prêtre fidèle<!--Hé. 2:17, 7:26-28.-->, qui agira selon mon cœur et selon mon âme. Et je lui bâtirai une maison fidèle<!--La maison fidèle fait premièrement allusion à Israël (Mi. 4) et ensuite à l'Assemblée (Église) (Mt. 16:18). Cette prophétie sera pleinement réalisée lors du millénium (Za. 14).-->, et il marchera à toujours devant mon Mashiah.
2:36	Et il arrivera que quiconque sera resté de ta maison viendra se prosterner devant lui pour avoir une pièce d'argent et un morceau de pain, et dira : Attache-moi, je te prie, dans une des charges de la prêtrise pour manger un morceau de pain.

## Chapitre 3

### YHWH appelle Shemouél (Samuel)

3:1	Or le jeune garçon Shemouél servait YHWH en présence d'Éli. La parole de YHWH était rare en ce temps-là, et les visions n'étaient pas fréquentes.
3:2	Il arriva en ce temps qu'Éli était couché à sa place, ses yeux commençaient à se ternir et il ne pouvait plus voir.
3:3	Et avant que les lampes<!--Le chandelier d'or à 7 branches du tabernacle et du temple de Yeroushalaim (Jérusalem) a été décrit avec une extrême minutie dans plusieurs passages de la Bible. Il a été réalisé selon le modèle imposé par Elohîm à Moshè au Sinaï (Ex. 25:31-40, 37:17-24 ; No. 8:4).--> d'Elohîm soient éteintes, Shemouél était aussi couché dans le temple de YHWH, où était l'arche d'Elohîm.
3:4	YHWH appela Shemouél. Et il répondit : Me voici !
3:5	Et il courut vers Éli, et lui dit : Me voici, car tu m'as appelé. Mais Éli dit : Je ne t'ai pas appelé, retourne te coucher. Et il s'en alla et se coucha.
3:6	YHWH appela encore Shemouél. Et Shemouél se leva, et s'en alla vers Éli, et lui dit : Me voici, car tu m'as appelé. Et Éli dit : Mon fils, je ne t'ai pas appelé, retourne et couche-toi.
3:7	Or Shemouél ne connaissait pas encore YHWH, et la parole de YHWH ne lui avait pas encore été révélée.
3:8	Et YHWH appela encore Shemouél pour la troisième fois. Et Shemouél se leva, et s'en alla vers Éli, et dit : Me voici, car tu m'as appelé. Éli reconnut que YHWH appelait ce jeune garçon.
3:9	Alors Éli dit à Shemouél : Va et couche-toi et, si on t'appelle, tu diras : Parle, YHWH, car ton serviteur écoute. Shemouél donc s'en alla et se coucha à sa place.
3:10	YHWH donc vint, et se tint là, et appela comme les autres fois : Shemouél, Shemouél ! Et Shemouél dit : Parle, car ton serviteur écoute.

### Jugement de YHWH sur la maison d'Éli

3:11	Alors YHWH dit à Shemouél : Voici, je vais faire une chose en Israël, qui étourdira les oreilles de quiconque l'entendra.
3:12	En ce jour-là, j'accomplirai sur Éli tout ce que j'ai déclaré contre sa maison ; je commencerai et j'achèverai.
3:13	Car je l'ai averti que je vais punir sa maison à perpétuité, à cause de l'iniquité dont il a connaissance, par laquelle ses fils se sont rendus infâmes, sans qu'il les ait réprimés.
3:14	C'est pourquoi j'ai juré à la maison d'Éli, qu'aucune propitiation ne sera jamais faite pour l'iniquité de la maison d'Éli, ni par des sacrifices ni par des offrandes.
3:15	Et Shemouél resta couché jusqu'au matin, puis il ouvrit les portes de la maison de YHWH. Or Shemouél craignait de rapporter cette vision à Éli.
3:16	Mais Éli appela Shemouél et lui dit : Shemouél, mon fils ! Il répondit : Me voici !
3:17	Et Éli dit : Quelle est la parole qui t'a été adressée ? Je te prie ne me la cache pas. Qu'Elohîm te fasse ceci et qu'il y ajoute cela, si tu me caches un seul mot de tout ce qui t'a été dit.
3:18	Shemouél lui déclara donc toutes ces paroles, et ne lui en cacha rien. Et Éli répondit : C'est YHWH, qu'il fasse ce qui est bon à ses yeux !
3:19	Shemouél grandissait. Et YHWH était avec lui, il ne laissa tomber à terre aucune de ses paroles.
3:20	Tout Israël, depuis Dan jusqu'à Beer-Shéba, reconnut que Shemouél était établi prophète de YHWH.
3:21	YHWH continuait de se manifester dans Shiyloh, car YHWH se manifestait à Shemouél, dans Shiyloh, par la parole de YHWH.

## Chapitre 4

### Les Philistins s'emparent de l'arche ; YHWH juge la maison d'Éli

4:1	Or la parole de Shemouél arriva à tout Israël. Car Israël sortit en bataille pour aller à la rencontre des Philistins. Ils campèrent près d'Eben-Ezer, et les Philistins campaient à Aphek.
4:2	Les Philistins se rangèrent en bataille contre Israël, et le combat s'engagea. Israël fut battu par les Philistins, qui en tuèrent environ 4 000 hommes sur le champ de bataille.
4:3	Quand le peuple rentra au camp, les anciens d'Israël dirent : Pourquoi YHWH nous a-t-il battus aujourd'hui par les Philistins ? Ramenons de Shiyloh l'arche de l'alliance de YHWH, et qu'elle vienne au milieu de nous, et nous délivre de la main de nos ennemis.
4:4	Le peuple envoya donc à Shiyloh, d'où l'on apporta l'arche de l'alliance de YHWH Sabaoth qui habite entre les chérubins. Les deux fils d'Éli, Hophni et Phinées, étaient là, avec l'arche de l'alliance d'Elohîm.
4:5	Et il arriva que, comme l'arche de YHWH entrait dans le camp, tout Israël poussa de grands cris de joie et la terre en fut ébranlée.
4:6	Les Philistins entendirent le bruit de ces cris de joie, et ils dirent : Que veut dire ce bruit, et que signifient ces grands cris de joie dans le camp de ces Hébreux ? Et ils apprirent que l'arche de YHWH était arrivée dans le camp.
4:7	Les Philistins eurent peur, car ils disaient : Elohîm est entré dans le camp. Et ils dirent : Malheur à nous ! Car il n'en a pas été ainsi hier ni avant-hier.
4:8	Malheur à nous ! Qui nous délivrera de la main de ces elohîm<!--Le terme hébreu « elohîm », généralement traduit par « dieu » ou « dieux », signifie également « dirigeants », « juges » ou encore « anges ». Dans les textes bibliques, « elohîm » est employé pour désigner Moshè (Moïse), qui a été fait « dieu » pour pharaon (Ex. 7:1), ainsi que pour les dieux païens Baal, Kemosh et Dagon (Jg. 6:31, 11:24 ; 1 S. 5:7). Les Philistins avaient une vision polythéiste de la divinité et n'avaient pas la révélation de l'Elohîm des Hébreux qui est un (De. 6:4).--> majestueux ? Ce sont ces elohîm qui ont frappé les Égyptiens de toutes sortes de plaies dans le désert.
4:9	Philistins, prenez courage et agissez en hommes, de peur que vous ne soyez esclaves des Hébreux, comme ils vous ont été asservis ; agissez en hommes et combattez !
4:10	Les Philistins donc combattirent, et Israël fut battu. Et chacun s'enfuit dans sa tente. La défaite fut très grande, 30 000 hommes de pied d'Israël périrent.
4:11	L'arche d'Elohîm fut prise, et les deux fils d'Éli, Hophni et Phinées, moururent.
4:12	Un homme de Benyamin s'enfuit de la bataille et arriva à Shiyloh ce même jour, ayant ses vêtements déchirés et la tête recouverte de terre.
4:13	Et comme il arrivait, voici, Éli était assis sur un siège à côté du chemin, étant attentif, car son cœur tremblait à cause de l'arche d'Elohîm. Cet homme entra donc dans la ville et donna les nouvelles, et toute la ville se mit à crier.
4:14	Éli, entendant les cris, dit : Que veut dire ce grand tumulte ? Et aussitôt cet homme vint à Éli et lui raconta tout.
4:15	Or Éli était âgé de 98 ans, ses yeux étaient fixes, il ne pouvait plus voir.
4:16	L'homme dit à Éli : Je viens de la ligne de bataille, car je me suis enfui aujourd'hui de la ligne de bataille. Il dit : Quelle est la parole, mon fils ?
4:17	Celui qui apportait les nouvelles répondit et dit : Israël a fui devant les Philistins, et il y a eu une grande défaite du peuple ; tes deux fils Hophni et Phinées sont morts, et l'arche d'Elohîm a été prise.
4:18	Et il arriva qu'aussitôt qu'il eut fait mention de l'arche d'Elohîm, Éli tomba à la renverse, de dessus son siège, à côté de la porte, se rompit le cou et mourut, car cet homme était vieux et pesant. Il avait été juge en Israël pendant 40 ans.
4:19	Sa belle-fille, femme de Phinées, était enceinte, et sur le point d'accoucher. Lorsqu'elle apprit la nouvelle de la prise de l'arche d'Elohîm, de la mort de son beau-père et de son mari, elle se coucha et enfanta, car les douleurs la surprirent.
4:20	Comme elle mourait, celles qui l'assistaient lui dirent : N'aie pas peur, car tu as enfanté un fils ! Mais elle ne répondit rien et n'en tint pas compte.
4:21	Mais elle appela le garçon I-Kabod, en disant : La gloire s'en est allée d'Israël ! parce que l'arche de YHWH était prise à cause de son beau-père et de son mari.
4:22	Elle dit donc : La gloire s'en est allée d'Israël, car l'arche d'Elohîm est prise !

## Chapitre 5

### Jugements de YHWH sur les Philistins

5:1	Les Philistins prirent l'arche d'Elohîm et l'emmenèrent d'Eben-Ezer à Asdod.
5:2	Les Philistins donc prirent l'arche d'Elohîm, et l'emmenèrent dans la maison de Dagon<!--L'étymologie du nom « Dagon » avait justifié la représentation qu'on faisait de ce dieu : une sorte de sirène mâle ou un homme avec une queue de poisson. En effet, « dâg », en hébreu signifie « poisson ». Il était le dieu des semences et de l'agriculture chez les peuples d'origine sémite, mais également l'un des principaux dieux des Philistins.-->, et la posèrent auprès de Dagon.
5:3	Le lendemain les Asdodiens, s'étant levés de bon matin, trouvèrent Dagon le visage contre terre, devant l'arche de YHWH. Mais ils le prirent et le remirent à sa place.
5:4	Ils se levèrent encore le lendemain de bon matin, et voici, Dagon était tombé le visage contre terre, devant l'arche de YHWH ; la tête de Dagon et les deux paumes de ses mains découpées étaient sur le seuil, et il ne lui restait que le tronc.
5:5	C'est pour cela que les prêtres de Dagon, et tous ceux qui entrent dans la maison de Dagon, à Asdod, ne marchent pas sur le seuil jusqu'à aujourd'hui.
5:6	Puis la main de YHWH s'appesantit sur les Asdodiens et les dévasta ; et il les frappa d'hémorroïdes à Asdod et dans tout son territoire.
5:7	Les hommes d'Asdod, voyant qu'il en était ainsi, dirent : L'arche d'Elohîm d'Israël ne restera pas chez nous, car sa main s'est appesantie sur nous, et sur Dagon, notre elohîm.
5:8	Et ils firent appeler et rassemblèrent auprès d'eux tous les seigneurs des Philistins, et dirent : Que ferons-nous de l'arche d'Elohîm d'Israël ? Et ils répondirent : Qu'on transporte à Gath l'arche d'Elohîm d'Israël. Et l'on transporta là-bas l'arche d'Elohîm d'Israël.
5:9	Mais il arriva après qu'on l'eut transportée, la main de YHWH fut sur la ville et il y eut une très grande terreur, et il frappa les gens de la ville, depuis le plus petit jusqu'au plus grand, par une éruption d'hémorroïdes.
5:10	Ils envoyèrent donc l'arche d'Elohîm à Ékron. Or comme l'arche d'Elohîm entrait à Ékron, ceux d'Ékron s'écrièrent, en disant : Ils ont transporté vers nous l'arche d'Elohîm d'Israël, pour nous faire mourir, nous et notre peuple !
5:11	C'est pourquoi ils firent appeler, et rassemblèrent tous les seigneurs des Philistins, en disant : Renvoyez l'arche d'Elohîm d'Israël ! Qu'elle retourne en son lieu et qu'elle ne nous fasse pas mourir, nous et notre peuple. Car il régnait une terreur mortelle dans toute la ville, et la main d'Elohîm s'y appesantissait fortement.
5:12	Les hommes qui n'en mouraient pas étaient frappés d'hémorroïdes, de sorte que le cri de la ville montait jusqu'au ciel.

## Chapitre 6

### L'arche de YHWH revient en Israël

6:1	L'arche de YHWH était depuis sept mois dans le pays des Philistins,
6:2	les Philistins appelèrent les prêtres et les devins, et leur dirent : Que ferons-nous de l'arche de YHWH ? Dites-nous comment nous devons la renvoyer en son lieu.
6:3	Ils répondirent : Si vous renvoyez l'arche d'Elohîm d'Israël, ne la renvoyez pas à vide, mais payez-lui, payez-lui un sacrifice de culpabilité. Alors vous serez guéris et vous saurez pourquoi sa main ne s'est pas détournée de vous.
6:4	Et ils dirent : Quelle offrande lui payerons-nous pour la culpabilité<!--Le mot « culpabilité » vient de l'hébreu « asham » qui signifie « délit », « offense », « ce qui est acquis par un délit, mal acquis ». Il est question ici de l'arche qui avait été volée par les Philistins. Aux yeux d'Elohîm, cet acte était un délit.--> ? Et ils répondirent : Selon le nombre des seigneurs des Philistins, vous donnerez 5 hémorroïdes d'or, et 5 souris d'or, car une même plaie a été sur vous tous et sur vos seigneurs.
6:5	Vous ferez donc des figures de vos hémorroïdes, et des figures des souris qui détruisent le pays, et vous donnerez gloire à Elohîm d'Israël : peut-être retirera-t-il sa main de dessus vous, et de dessus vos elohîm, et de dessus votre pays.
6:6	Et pourquoi endurciriez-vous votre cœur, comme l'Égypte et pharaon ont endurci leur cœur ? Lorsqu'Elohîm les a traités avec sévérité, n'ont-ils pas renvoyé les Israélites pour qu'ils s'en aillent ?
6:7	Maintenant, donc prenez de quoi faire un chariot tout neuf, et deux jeunes vaches qui allaitent leurs veaux et qui n'aient pas porté le joug. Attelez au chariot les deux jeunes vaches et ramenez leurs petits à la maison.
6:8	Puis prenez l'arche de YHWH et mettez-la sur le chariot. Mettez les ouvrages d'or que vous lui aurez payés pour la culpabilité dans un petit coffre à côté de l'arche, puis renvoyez-la et elle s'en ira.
6:9	Et vous observerez : si l'arche monte vers Beth-Shémesh, par le chemin de sa frontière, c'est YHWH qui nous a fait tout ce grand mal ; si elle n'y va pas, nous saurons alors que sa main ne nous a pas touchés, mais que c'est une chose accidentelle qui nous est arrivée.
6:10	Ces gens firent ainsi. Ils prirent donc deux jeunes vaches qui allaitaient, ils les attelèrent au chariot, et ils enfermèrent leurs petits dans l'étable.
6:11	Ils mirent sur le chariot l'arche de YHWH, et le coffre avec les souris d'or et les figures de leurs hémorroïdes.
6:12	Alors les jeunes vaches prirent tout droit le chemin de Beth-Shémesh. Elles suivirent toujours le même chemin en marchant et en mugissant, et elles ne se détournèrent ni à droite ni à gauche. Les seigneurs des Philistins allèrent après elles jusqu'à la frontière de Beth-Shémesh.
6:13	Or ceux de Beth-Shémesh étaient impatients de moissonner les blés dans la vallée. Levant les yeux, ils virent l'arche et se réjouirent en la voyant.
6:14	Le chariot arriva dans le champ de Yéhoshoua de Beth-Shémesh, et s'arrêta là. Or il y avait là une grande pierre, et on fendit le bois du chariot, et on offrit les jeunes vaches en holocauste à YHWH.
6:15	Les Lévites descendirent l'arche de YHWH ainsi que le coffre dans lequel étaient les objets d'or, et ils les mirent sur la grande pierre. En ce même jour, ceux de Beth-Shémesh offrirent des holocaustes et sacrifièrent des sacrifices à YHWH.
6:16	Les cinq seigneurs des Philistins, après avoir vu cela, retournèrent le même jour à Ékron.
6:17	Voici les hémorroïdes d'or que les Philistins donnèrent à YHWH en offrande pour la culpabilité : un pour Asdod, un pour Gaza, un pour Askalon, un pour Gath, un pour Ékron.
6:18	Les souris d'or, selon le nombre de toutes les villes des Philistins, appartenant aux cinq seigneurs, tant des villes fortifiées que des villages sans murailles. Or la grande pierre<!--« Abel » pour certains, et « Eben » pour d'autres. Abel est une ville du nord d'Israël, près de Beth-Maaka.--> sur laquelle on posa l'arche de YHWH, est encore jusqu’à ce jour dans le champ de Yéhoshoua de Beth-Shémesh.
6:19	YHWH frappa les hommes de Beth-Shémesh parce qu'ils avaient regardé dans l'arche de YHWH. Il frappa parmi le peuple 50 070 hommes, et le peuple mena le deuil parce que YHWH l'avait frappé d'une grande plaie.
6:20	Alors ceux de Beth-Shémesh dirent : Qui pourrait subsister en présence de YHWH, ce Elohîm Saint ? Et vers qui montera-t-il en s'éloignant de nous ?
6:21	Et ils envoyèrent des messagers aux habitants de Qiryath-Yéarim, en disant : Les Philistins ont ramené l'arche de YHWH. Descendez donc et faites-la monter vers vous.

## Chapitre 7

### Un réveil après l'apostasie

7:1	Les gens de Qiryath-Yéarim vinrent et firent monter l'arche de YHWH. Ils la conduisirent dans la maison d'Abinadab, sur la colline, et ils consacrèrent Èl’azar son fils pour garder l'arche de YHWH.
7:2	Et depuis le jour où l'arche de YHWH fut déposée à Qiryath-Yéarim, il arriva que de nombreux jours s'écoulèrent. Au bout de vingt ans, toute la maison d'Israël gémit après YHWH.
7:3	Et Shemouél parla à toute la maison d'Israël, en disant : Si vous revenez à YHWH de tout votre cœur, ôtez du milieu de vous les elohîm étrangers et les Astartés, dirigez votre cœur vers YHWH et servez-le, lui seul. Et il vous délivrera de la main des Philistins.
7:4	Alors les enfants d'Israël ôtèrent les Baalim et les Astartés, et ils servirent YHWH seul.
7:5	Shemouél dit : Rassemblez tout Israël à Mitspa, et je prierai YHWH pour vous.
7:6	Ils se rassemblèrent donc à Mitspa. Ils puisèrent de l'eau qu'ils répandirent devant YHWH, et ils jeûnèrent ce jour-là, en disant : Nous avons péché contre YHWH ! Et Shemouél jugea les enfants d'Israël à Mitspa.
7:7	Or quand les Philistins eurent appris que les enfants d'Israël étaient rassemblés à Mitspa, les seigneurs des Philistins montèrent contre Israël. Les enfants d'Israël l'apprirent et ils eurent peur des Philistins.
7:8	Les enfants d'Israël dirent à Shemouél : Ne reste pas muet ! Crie pour nous vers YHWH, notre Elohîm, afin qu'il nous délivre de la main des Philistins !

### Victoire d'Israël contre les Philistins

7:9	Alors Shemouél prit un agneau de lait et l'offrit tout entier à YHWH en holocauste. Et Shemouél cria vers YHWH pour Israël, et YHWH l'exauça.
7:10	Il arriva donc, comme Shemouél offrait l'holocauste, les Philistins s'approchèrent pour combattre contre Israël, mais YHWH fit gronder, en ce jour-là, un grand tonnerre sur les Philistins, et les mit en déroute, et ils furent battus devant Israël.
7:11	Les hommes d'Israël sortirent de Mitspa, et poursuivirent les Philistins, et les frappèrent jusqu'au-dessous de Beth-Car.
7:12	Alors Shemouél prit une pierre, et la mit entre Mitspa et Shen, et il appela ce lieu Eben-Ezer, en disant : YHWH nous a secourus jusqu'ici.
7:13	Les Philistins furent humiliés et ils ne vinrent plus sur le territoire d'Israël. La main de YHWH fut contre les Philistins durant la vie de Shemouél.
7:14	Les villes que les Philistins avaient prises sur Israël retournèrent à Israël, depuis Ékron jusqu'à Gath, avec leurs territoires ; Israël les délivra donc de la main des Philistins. Et il y eut paix entre Israël et les Amoréens.

### Shemouél (Samuel), juge en Israël

7:15	Shemouél fut juge en Israël tous les jours de sa vie.
7:16	Il allait d'année en année faire le tour de Béth-El, de Guilgal et de Mitspa, et il jugeait Israël dans tous ces lieux.
7:17	Puis il revenait à Ramah où était sa maison. Et là il jugeait Israël et il y bâtit un autel pour YHWH.

## Chapitre 8

### Les fils de Shemouél (Samuel) corrompus ; Israël demande un roi

8:1	Et il arriva que, lorsque Shemouél fut devenu vieux, il établit ses fils pour juges sur Israël.
8:2	Le nom de son fils premier-né était Yoel, et le nom de son second Abiyah. Ils jugeaient à Beer-Shéba.
8:3	Mais ses fils ne marchèrent pas dans ses voies, ils s'en détournèrent pour les profits acquis par la violence. Ils recevaient des pots-de-vin et pervertissaient<!--Voir La. 3:35-36.--> la justice.
8:4	C'est pourquoi tous les anciens d'Israël se rassemblèrent et vinrent auprès de Shemouél à Ramah.
8:5	Ils lui dirent : Voici, tu es devenu vieux, et tes fils ne marchent pas dans tes voies. Maintenant, établis sur nous un roi pour nous juger, comme en ont toutes les nations.

### YHWH accepte la requête du peuple

8:6	Aux yeux de Shemouél c'était une mauvaise chose qu'ils aient dit : Établis sur nous un roi pour nous juger. Et Shemouél pria YHWH.
8:7	YHWH dit à Shemouél : Écoute le peuple dans tout ce qu'il te dira, car ce n'est pas toi qu'ils ont rejeté, mais c'est moi qu'ils ont rejeté, afin que je ne règne plus sur eux.
8:8	Ils agissent à ton égard comme ils ont agi depuis le jour où je les ai fait monter hors d'Égypte jusqu'à ce jour. Ils m'ont abandonné, pour servir d'autres elohîm.
8:9	Maintenant donc, obéis à leur voix, mais avertis-les, avertis-les en leur déclarant comment le roi qui régnera sur eux les traitera.

### Avertissement : Le roi sera un joug pour le peuple

8:10	Ainsi Shemouél dit toutes les paroles de YHWH au peuple qui lui avait demandé un roi.
8:11	Il leur dit donc : Voici comment vous traitera le roi qui régnera sur vous. Il prendra vos fils et les mettra sur ses chars et parmi ses cavaliers, afin qu'ils courent devant son char ;
8:12	il en établira des chefs de mille et des chefs de 50, pour labourer ses terres, pour récolter ses moissons, et pour fabriquer ses armes de guerre et l'équipement de ses chars.
8:13	Il prendra aussi vos filles pour en faire des parfumeuses, des cuisinières et des boulangères.
8:14	Il prendra le meilleur de vos champs, de vos vignes et de vos oliviers, et il les donnera à ses serviteurs.
8:15	Il prélèvera la dîme de ce que vous aurez semé et de ce que vous aurez vendangé, et il la donnera à ses eunuques et à ses serviteurs.
8:16	Il prendra vos serviteurs et vos servantes, l'élite de vos jeunes hommes, vos ânes, et les emploiera à ses ouvrages.
8:17	Il prélèvera la dîme de vos troupeaux, et vous serez ses esclaves.
8:18	En ce jour-là, vous crierez à cause du roi que vous vous serez choisi, mais YHWH ne vous répondra pas en ce jour-là.
8:19	Mais le peuple refusa d'écouter la voix de Shemouél, et ils dirent : Non ! Mais il y aura un roi sur nous.
8:20	Nous serons aussi comme toutes les nations, et notre roi nous jugera, il sortira devant nous et il conduira nos guerres.
8:21	Shemouél entendit donc toutes les paroles du peuple et les rapporta à YHWH.
8:22	Et YHWH dit à Shemouél : Obéis à leur voix et établis un roi sur eux. Et Shemouél dit aux hommes d'Israël : Allez-vous-en chacun dans sa ville.

## Chapitre 9

### Shaoul (Saül) choisi pour devenir le premier roi d'Israël

9:1	Or il y avait un homme de Benyamin, nommé Kis, vaillant et talentueux, fils d'Abiel, fils de Tseror, fils de Becorath, fils d'Aphiach, fils d'un Benyamite.
9:2	Il avait un fils du nom de Shaoul<!--Saül.-->. C'était un beau jeune homme, et aucun homme des fils d'Israël n'était plus beau que lui. Des épaules en haut, il dépassait tout le peuple.
9:3	Les ânesses de Kis, père de Shaoul, s'étant égarées, Kis dit à Shaoul, son fils : Prends maintenant avec toi l'un des serviteurs, lève-toi et va chercher les ânesses !
9:4	Il passa donc par la Montagne d'Éphraïm et traversa le pays de Shalisha, mais ils ne les trouvèrent pas. Puis ils passèrent par le pays de Shaalim, mais elles n'y étaient pas ; ils passèrent ensuite par le pays de Benyamin, mais ils ne les trouvèrent pas.
9:5	Quand ils furent arrivés dans le pays de Tsouph, Shaoul dit à son serviteur qui était avec lui : Viens, et retournons, de peur que mon père, cessant de penser aux ânesses, ne soit en peine de nous.
9:6	Le serviteur lui dit : Voici, je te prie, il y a dans cette ville un homme d'Elohîm, et c'est un homme honorable. Tout ce qu'il déclare arrive, arrive. Allons-y maintenant, peut-être nous renseignera-t-il sur le chemin que nous devons prendre.
9:7	Et Shaoul dit à son serviteur : Mais si nous y allons, qu'apporterons-nous à l'homme d'Elohîm ? Nous n'avons plus de provisions, et nous n'avons aucun présent pour l'homme d'Elohîm. Qu'est-ce que nous avons ?
9:8	Le serviteur reprit la parole et dit à Shaoul : Voici, j'ai encore entre mes mains le quart d'un sicle d'argent, et je le donnerai à l'homme d'Elohîm, et il nous indiquera notre chemin.
9:9	Autrefois en Israël, quand on allait consulter Elohîm, on se disait l'un à l'autre : Venez, allons vers le voyant ! Car le prophète s'appelait autrefois le voyant.
9:10	Shaoul dit à son serviteur : Ta parole est bonne ! Viens, allons ! Et ils s'en allèrent dans la ville où était l'homme d'Elohîm.
9:11	Et comme ils montaient par la montée de la ville, ils trouvèrent des jeunes filles qui sortaient pour puiser de l'eau, et ils leur dirent : Le voyant n'est-il pas ici ?
9:12	Elles leur répondirent et dirent : Il y est, le voilà devant toi, hâte-toi maintenant, car aujourd'hui il est venu à la ville, parce que le peuple a aujourd'hui un sacrifice sur le haut lieu.
9:13	Quand vous entrerez dans la ville, vous le trouverez avant qu'il monte au haut lieu pour manger. Car le peuple ne mangera pas jusqu'à ce qu'il soit venu, parce qu'il doit bénir le sacrifice ; après quoi, les conviés mangeront. Montez donc maintenant, car vous le trouverez aujourd'hui.
9:14	Ils montèrent donc à la ville. Comme ils entraient dans la ville, Shemouél, qui sortait pour monter au haut lieu, les rencontra.
9:15	Or un jour avant l'arrivée de Shaoul, YHWH avait découvert l'oreille de Shemouél, en disant :
9:16	Demain, à cette même heure, je t'enverrai un homme du pays de Benyamin, et tu l'oindras pour être le chef de mon peuple d'Israël. Il délivrera mon peuple de la main des Philistins, car j'ai regardé mon peuple, parce que son cri est venu jusqu'à moi.
9:17	Et dès que Shemouél eut aperçu Shaoul, YHWH lui dit : Voici l'homme dont je t'ai parlé, c'est lui qui dominera sur mon peuple.
9:18	Et Shaoul s'approcha de Shemouél au milieu de la porte, et dit : Indique-moi, je te prie, où est la maison du voyant.
9:19	Et Shemouél répondit à Shaoul, et dit : Je suis le voyant. Monte devant moi au haut lieu, et vous mangerez aujourd'hui avec moi. Je te laisserai partir demain, et je te dirai tout ce que tu as sur le cœur.
9:20	Mais quant aux ânesses que tu as perdues il y a trois jours, ne t'en inquiète pas, parce qu'elles ont été retrouvées. Et vers qui tend tout le désir d'Israël ? N'est-ce pas vers toi et vers toute la maison de ton père ?
9:21	Shaoul répondit et dit : Ne suis-je pas de Benyamin, l'une des moindres tribus d'Israël, et ma famille n'est-elle pas la plus petite de toutes les familles de la tribu de Benyamin ? Pourquoi m'as-tu tenu de tels discours ?
9:22	Shemouél prit Shaoul et son serviteur, et les fit entrer dans la salle, et les plaça à la tête des conviés, qui étaient environ trente hommes.
9:23	Et Shemouél dit au cuisinier : Apporte la portion que je t'ai donnée, en te disant : Mets-la à part.
9:24	Le cuisinier prit l'épaule et ce qui l'entoure, et il la servit à Shaoul. Et Shemouél dit : Voici ce qui a été réservé ; mets-le devant toi et mange, car cela a été gardé pour toi, pour le temps fixé, lorsque j'ai résolu de convier le peuple. Et Shaoul mangea avec Shemouél ce jour-là.
9:25	Puis ils descendirent du haut lieu dans la ville, et Shemouél parla avec Shaoul sur le toit.
9:26	Puis ils se levèrent de bon matin. Dès que monta l'aurore, Shemouél appela Shaoul sur le toit, et lui dit : Lève-toi et je te laisserai aller. Shaoul donc se leva, et ils sortirent tous les deux dehors, lui et Shemouél.
9:27	Et comme ils descendaient à l'extrémité de la ville, Shemouél dit à Shaoul : Dis au serviteur de passer devant nous. Et le serviteur passa devant. Arrête-toi maintenant, afin que je te fasse entendre la parole d'Elohîm.

## Chapitre 10

### Shemouél (Samuel) oint Shaoul (Saül) comme roi

10:1	Et Shemouél prit une fiole d'huile, qu'il répandit sur la tête de Shaoul. Il l'embrassa et lui dit : YHWH ne t'a-t-il pas oint pour être le chef de son héritage ?
10:2	Aujourd'hui, après m'avoir quitté, tu trouveras deux hommes près du sépulcre de Rachel, sur la frontière de Benyamin à Tseltsach, qui te diront : Les ânesses que tu es allé chercher sont retrouvées ; et voici, ton père ne pense plus à l'affaire des ânesses, mais il s'inquiète pour vous. Il se dit : Que dois-je faire à propos de mon fils ?
10:3	En allant plus loin, tu arriveras au chêne de Thabor, où tu seras rencontré par trois hommes qui montent vers Elohîm, à Béth-El, et l'un porte 3 chevreaux, l'autre 3 pains, et l'autre une outre de vin.
10:4	Ils te demanderont comment tu te portes, et ils te donneront deux pains, que tu recevras de leurs mains.
10:5	Après cela tu arriveras à Guibea-Elohîm, où se trouve une garnison de Philistins. Et il arrivera qu'en entrant dans la ville, tu rencontreras une troupe de prophètes descendant du haut lieu, précédés du luth, du tambourin, de la flûte et de la harpe, et qui prophétisent.
10:6	Alors l'Esprit de YHWH te saisira, et tu prophétiseras avec eux, et tu seras changé en un autre homme.
10:7	Et quand ces signes te seront arrivés, fais pour toi ce que ta main pourra, car Elohîm est avec toi.
10:8	Puis tu descendras devant moi à Guilgal. Voici, je descendrai vers toi pour offrir des holocaustes et des sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->. Tu m'attendras là 7 jours, jusqu'à ce que je vienne et que je te déclare ce que tu devras faire.
10:9	Il arriva donc qu'aussitôt que Shaoul eut tourné le dos pour se séparer de Shemouél, Elohîm changea son cœur, et tous ces signes s'accomplirent le même jour.
10:10	Quand ils arrivèrent à Guibea, voici, une troupe de prophètes vint à sa rencontre. L'Esprit d'Elohîm le saisit, et il prophétisa au milieu d'eux.
10:11	Et il arriva que, quand tous ceux qui l'avaient connu d'hier et d'avant-hier virent qu'il était avec les prophètes, et qu'il prophétisait, ceux du peuple se dirent l'un à l'autre : Qu'est-il arrivé au fils de Kis ? Shaoul est-il aussi parmi les prophètes ?
10:12	Un homme répondit : Et qui est leur père ? De là le proverbe : Shaoul est-il aussi parmi les prophètes ?
10:13	Lorsqu'il eut cessé de prophétiser, il se rendit au haut lieu.
10:14	L'oncle de Shaoul dit à Shaoul et à son serviteur : Où êtes-vous allés ? Et il répondit : Chercher les ânesses, mais ne les trouvant pas, nous sommes allés vers Shemouél.
10:15	Et l'oncle de Shaoul dit : Déclare-moi, je te prie, ce que vous a dit Shemouél.
10:16	Shaoul répondit à son oncle : Il nous a rapporté, rapporté que les ânesses étaient retrouvées. Mais il ne lui rapporta pas le discours que Shemouél avait tenu concernant la royauté.
10:17	Shemouél convoqua le peuple devant YHWH à Mitspa.
10:18	Et il dit aux enfants d'Israël : Ainsi parle YHWH, l'Elohîm d'Israël : J'ai fait monter Israël hors d'Égypte, et je vous ai délivrés de la main des Égyptiens et de la main de tous les royaumes qui vous opprimaient.
10:19	Mais aujourd'hui, vous avez rejeté votre Elohîm, celui qui vous a délivrés de tous vos malheurs et de vos afflictions, et vous avez dit : Non, établis un roi sur nous ! Présentez-vous donc maintenant, devant YHWH, par tribus et par familles.
10:20	Ainsi Shemouél fit approcher toutes les tribus d'Israël, et la tribu de Benyamin fut désignée.
10:21	Après, il fit approcher la tribu de Benyamin selon ses familles, et la famille de Matri fut désignée. Puis Shaoul<!--Voir en annexe le tableau « Chronologie : Les rois et les prophètes ».-->, fils de Kis, fut désigné. On le chercha, mais on ne le trouva pas.
10:22	On consulta de nouveau YHWH : Est-il encore venu quelqu'un ici ? YHWH répondit : Il est caché au milieu des armes.
10:23	Ils coururent donc le chercher, et il se présenta au milieu du peuple, et il était plus grand que tout le peuple, depuis les épaules en haut.
10:24	Et Shemouél dit à tout le peuple : Voyez-vous celui que YHWH a choisi ? Il n'y a personne dans tout le peuple qui soit semblable à lui. Et le peuple poussa des cris de joie, et dit : Vive le roi !
10:25	Alors Shemouél fit connaître au peuple les règles de la royauté et les écrivit dans un livre, qu'il déposa devant YHWH. Puis Shemouél renvoya le peuple, chacun dans sa maison.
10:26	Shaoul aussi s'en alla chez lui à Guibea. Il fut accompagné par des vaillants hommes dont Elohîm avait touché le cœur.
10:27	Mais il y eut des fils de Bélial<!--Voir commentaire en De. 13:13.--> qui dirent : Comment celui-ci nous délivrerait-il ? Et ils le méprisèrent et ne lui apportèrent pas de présent. Mais Shaoul fit le sourd.

## Chapitre 11

### Shaoul (Saül) bat les Ammonites

11:1	Nachash, l'Ammonite, vint et assiégea Yabesh en Galaad. Les habitants de Yabesh dirent à Nachash : Traite alliance avec nous et nous te servirons.
11:2	Mais Nachash, l'Ammonite, leur répondit : Je traiterai avec vous à la condition que je vous crève à tous l'œil droit et que je mette cet opprobre sur tout Israël.
11:3	Les anciens de Yabesh lui dirent : Donne-nous sept jours de trêve, et nous enverrons des messagers dans tout le territoire d'Israël. Si personne ne vient nous délivrer, nous nous rendrons à toi.
11:4	Les messagers arrivèrent à Guibea de Shaoul, et dirent ces paroles devant le peuple. Tout le peuple éleva sa voix et pleura.
11:5	Et voici, Shaoul revenait des champs derrière ses bœufs, et il dit : Qu'est-ce qu'a ce peuple pour pleurer ainsi ? Et on lui raconta ce qu'avaient dit ceux de Yabesh.
11:6	Et l'Esprit d'Elohîm saisit Shaoul, lorsqu'il entendit ces paroles, et sa colère s'enflamma extrêmement.
11:7	Il prit une paire de bœufs et les coupa en morceaux, qu'il envoya dans tout le territoire d'Israël par la main des messagers, en disant : Les bœufs de tous ceux qui ne sortiront pas pour suivre Shaoul et Shemouél, seront traités de la même manière. Et la frayeur de YHWH tomba sur le peuple, et ils sortirent comme un seul homme.
11:8	Shaoul en fit la revue à Bézek : les fils d'Israël étaient 300 000 et ceux de Yéhouda 30 000.
11:9	Puis, ils dirent aux messagers qui étaient venus : Vous parlerez ainsi à ceux de Yabesh en Galaad : Vous serez délivrés demain, quand le soleil sera dans sa force. Les messagers rapportèrent donc cela à ceux de Yabesh, qui s'en réjouirent.
11:10	Les hommes de Yabesh dirent aux Ammonites : Demain nous nous rendrons à vous, et vous nous traiterez selon votre bon plaisir.
11:11	Et il arriva que, le lendemain, Shaoul disposa le peuple en trois corps. Ils entrèrent dans le camp des Ammonites à la veille du matin et ils les battirent jusqu'à la chaleur du jour. Ceux qui échappèrent furent dispersés si bien qu'il n'en resta pas deux ensemble.

### Le peuple reconnaît Shaoul (Saül) comme roi

11:12	Le peuple dit à Shemouél : Qui est-ce qui dit : Shaoul régnera-t-il sur nous ? Donnez-nous ces hommes-là, et nous les ferons mourir.
11:13	Shaoul répondit : Personne ne sera mis à mort en ce jour, car YHWH a délivré Israël aujourd'hui.
11:14	Et Shemouél dit au peuple : Venez, allons à Guilgal, et nous y renouvellerons la royauté.
11:15	Tout le peuple se rendit à Guilgal. Et là, ils établirent Shaoul pour roi devant YHWH, à Guilgal. Ils y sacrifièrent des sacrifices d'offrande de paix devant YHWH. Shaoul et tous ceux d'Israël se réjouirent beaucoup.

## Chapitre 12

### Le peuple rend un bon témoignage de Shemouél (Samuel)

12:1	Alors Shemouél dit à tout Israël : Voici, j'ai obéi à votre voix dans tout ce que vous m'avez dit, et j'ai établi un roi sur vous.
12:2	Et maintenant, voici le roi qui marchera devant vous. Pour moi, je suis devenu vieux, j'ai blanchi et mes fils sont parmi vous. J'ai marché devant vous depuis ma jeunesse jusqu'à ce jour.
12:3	Me voici ! Témoignez contre moi, devant YHWH, et devant son mashiah. De qui ai-je pris le bœuf ? Et de qui ai-je pris l'âne ? Qui ai-je opprimé ? Qui ai-je traité durement ? Et de la main de qui ai-je reçu une rançon, afin de fermer les yeux sur lui ? Et je vous le rendrai.
12:4	Et ils répondirent : Tu ne nous as pas opprimés, tu ne nous as pas traités durement et tu n'as rien reçu de la main de personne.
12:5	Il leur dit encore : YHWH est témoin contre vous, et son mashiah aussi est témoin aujourd'hui, que vous n'avez rien trouvé entre mes mains. Et ils répondirent : Il en est témoin.

### Rappel des péchés du peuple ; exhortation à craindre YHWH

12:6	Alors Shemouél dit au peuple : YHWH est celui qui a établi Moshè et Aaron, et qui a fait monter vos pères hors du pays d'Égypte.
12:7	Maintenant donc, présentez-vous, et je vous jugerai devant YHWH sur tous les bienfaits que YHWH vous a accordés, à vous et à vos pères.
12:8	Après que Yaacov fut entré en Égypte, vos pères crièrent à YHWH, et YHWH envoya Moshè et Aaron qui firent sortir vos pères hors d'Égypte, et les firent habiter en ce lieu.
12:9	Mais ils oublièrent YHWH, leur Elohîm, et il les livra entre les mains de Sisera, chef de l'armée de Hatsor, et entre les mains des Philistins, et entre les mains du roi de Moab, qui leur firent la guerre.
12:10	Ils crièrent encore à YHWH, et dirent : Nous avons péché, car nous avons abandonné YHWH, et nous avons servi les Baalim et les Astartés. Maintenant donc, délivre-nous de la main de nos ennemis, et nous te servirons.
12:11	Et YHWH envoya Yeroubbaal, Bedan, Yiphtah et Shemouél, et il vous délivra de la main de tous vos ennemis d'alentour, et vous demeurâtes en sécurité.
12:12	Mais voyant que Nachash, roi des fils d'Ammon, marchait contre vous, vous m'avez dit : Non ! Mais un roi régnera sur nous. Alors que YHWH, votre Elohîm, était votre Roi.
12:13	Maintenant donc, voici le roi que vous avez choisi, que vous avez demandé, et voici YHWH l'a établi roi sur vous.
12:14	Si vous craignez YHWH, si vous le servez, et obéissez à sa voix, et que vous n'êtes pas rebelles au commandement de YHWH, alors vous et votre roi qui règne sur vous, vous serez sous la conduite de YHWH, votre Elohîm.
12:15	Mais si vous n'obéissez pas à la voix de YHWH, et si vous êtes rebelles au commandement de YHWH, la main de YHWH sera aussi contre vous, comme elle a été contre vos pères.
12:16	Maintenant, préparez-vous, et voyez cette grande chose que YHWH va opérer sous vos yeux.
12:17	N'est-ce pas aujourd'hui la moisson des blés ? Je crierai à YHWH, et il enverra des tonnerres et de la pluie. Sachez alors et voyez combien vous avez mal agi aux yeux de YHWH en demandant un roi.
12:18	Alors Shemouél cria à YHWH, et YHWH envoya des tonnerres et de la pluie ce même jour. Tout le peuple eut une grande crainte de YHWH et de Shemouél.
12:19	Et tout le peuple dit à Shemouél : Prie YHWH, ton Elohîm, pour tes serviteurs, afin que nous ne mourions pas, car nous avons ajouté à nos péchés, celui d'avoir demandé un roi.
12:20	Alors Shemouél dit au peuple : N'ayez pas peur ! Vous avez fait tout ce mal, néanmoins ne vous détournez pas de YHWH, mais servez YHWH de tout votre cœur.
12:21	Ne vous en détournez pas, car vous iriez après des choses de néant, qui ne vous apportent ni profit ni délivrance, puisque ce sont des choses de néant.
12:22	Car YHWH n'abandonne pas son peuple, pour l'amour de son grand Nom, car YHWH a résolu de faire de vous son peuple.
12:23	Loin de moi aussi de pécher contre YHWH, de cesser de prier pour vous ! Je vous enseignerai le bon et le droit chemin.
12:24	Craignez seulement YHWH, et servez-le en vérité, de tout votre cœur. Car vous avez vu les choses magnifiques qu'il a faites pour vous.
12:25	Mais si vous faites le mal, si vous faites le mal vous serez détruits vous et votre roi.

## Chapitre 13

### Impatience et désobéissance de Shaoul (Saül) ; la royauté lui sera enlevée

13:1	Shaoul était comme un enfant d'un an lorsqu'il commença à régner, et il régna deux ans sur Israël.
13:2	Shaoul choisit 3 000 hommes d'Israël, 2 000 avec lui à Micmash, et sur la Montagne de Béth-El, et 1 000 étaient avec Yonathan<!--Yehonathan et Yonathan correspondent au même nom, généralement traduit par « Jonathan ».--> à Guibea de Benyamin. Il renvoya le reste du peuple, chacun à sa tente.
13:3	Et Yonathan battit le poste des Philistins qui était à Guéba, et les Philistins en furent informés. Et Shaoul fit sonner le shofar dans tout le pays, en disant : Que les Hébreux écoutent !
13:4	Tout Israël apprit donc que Shaoul avait battu le poste des Philistins, et Israël se rendit odieux aux Philistins. Et le peuple fut convoqué auprès de Shaoul, à Guilgal.
13:5	Les Philistins se rassemblèrent pour combattre Israël, ayant 30 000 chars et 6 000 cavaliers, et le peuple était aussi nombreux que le sable au bord de la mer, tant il était en grand nombre. Ils allèrent prendre position à Micmash, à l'orient de Beth-Aven.
13:6	Les hommes d'Israël furent pris d'une grande angoisse, car ils étaient oppressés, c'est pourquoi le peuple se cacha dans les cavernes, dans les buissons, dans les rochers, dans les tours et dans des citernes.
13:7	Les Hébreux passèrent le Yarden pour aller au pays de Gad et de Galaad. Shaoul était encore à Guilgal, et derrière lui tout le peuple tremblait.
13:8	Il attendit 7 jours, jusqu'au temps fixé par Shemouél. Mais Shemouél ne venait pas à Guilgal et le peuple se dispersait.
13:9	Et Shaoul dit : Amenez-moi un holocauste et des sacrifices d'offrande de paix. Et il offrit l'holocauste.
13:10	Comme il achevait d'offrir l'holocauste, Shemouél arriva, et Shaoul sortit au-devant de lui pour le saluer.
13:11	Et Shemouél lui dit : Qu'as-tu fait ? Shaoul répondit : Lorsque j'ai vu que le peuple se dispersait, que tu ne venais pas au jour fixé, et que les Philistins étaient rassemblés à Micmash,
13:12	je me suis dit : Les Philistins descendront maintenant contre moi à Guilgal et je n'ai pas supplié la face de YHWH ! Je me suis maîtrisé un temps, mais j'ai fini par offrir l'holocauste.
13:13	Shemouél répondit à Shaoul : C'est en insensé que tu as agi ! Car tu n'as pas gardé le commandement que YHWH, ton Elohîm, t'avait donné. En effet, YHWH aurait maintenu à jamais ta royauté sur Israël.
13:14	Et maintenant ta royauté ne tiendra pas. YHWH s'est choisi un homme selon son cœur, et YHWH l'a destiné à être le chef de son peuple, parce que tu n'as pas respecté le commandement de YHWH.

### Shaoul (Saül) et ses hommes à Guibea de Benyamin

13:15	Puis Shemouél se leva et monta de Guilgal à Guibea de Benyamin. Et Shaoul passa en revue le peuple qui se trouvait avec lui, qui fut d'environ 600 hommes.
13:16	Or Shaoul vint s'établir avec son fils Yonathan, et le peuple qui était sous ses ordres à Guéba de Benyamin, et les Philistins étaient campés à Micmash.
13:17	Les Philistins sortirent du camp en trois divisions pour ravager : l'une de ces divisions prit le chemin d'Ophrah, vers le pays de Shoual,
13:18	l'autre division prit le chemin de Beth-Horon, et la troisième prit le chemin de la frontière qui regarde vers la vallée de Tseboïm, du côté du désert.
13:19	Or dans tout le pays d'Israël, il ne se trouvait aucun forgeron, car les Philistins avaient dit : Empêchons les Hébreux de faire des épées ou des lances.
13:20	C'est pourquoi chaque homme descendait vers les Philistins pour aiguiser son soc, son hoyau, sa hache et sa bêche,
13:21	le prix était d’un pim<!--Le pim était un poids en pierre et pesait à peu près deux tiers d’un sicle.--> pour les bêches, pour les socs de charrue et pour les fourches à trois dents, ainsi que pour les haches et pour redresser les aiguillons.
13:22	Aussi arriva-t-il qu'au jour du combat, il ne se trouvait ni épée ni lance entre les mains de tout le peuple qui était avec Shaoul et Yonathan. On en trouva néanmoins pour Shaoul et pour son fils Yonathan.
13:23	Un poste de Philistins s'établit au passage de Micmash.

## Chapitre 14

### Courage de Yehonathan (Yonathan)

14:1	Or, il arriva un jour que Yonathan<!--Voir commentaire en 1 S. 13:2.-->, fils de Shaoul, dit au garçon qui portait ses armes : Viens, et passons vers le poste des Philistins qui est de l'autre côté. Mais il ne dit rien à son père.
14:2	Shaoul se tenait à l'extrémité de Guibea sous un grenadier, à Migron, entouré d'environ 600 hommes.
14:3	Achiyah, fils d'Ahitoub, frère d'I-Kabod, fils de Phinées, fils d'Éli, prêtre de YHWH à Shiyloh, portait l'éphod. Et le peuple ignorait que Yonathan s'en était allé.
14:4	Or, entre les passages par lesquels Yonathan voulait arriver au poste de garde des Philistins, il y avait une dent de rocher d'un côté et une dent de rocher de l'autre, le nom de l'un était Botsets et le nom de l'autre Séné.
14:5	L'une des dents était une colonne au nord, en face de Micmash, et l'autre était au sud, en face de Guéba.
14:6	Yehonathan dit au garçon qui portait ses armes : Viens, poursuivons jusqu'au poste de garde de ces incirconcis. Peut-être YHWH agira-t-il pour nous, car on ne saurait empêcher YHWH de délivrer avec peu ou beaucoup de gens.
14:7	Et celui qui portait ses armes lui dit : Fais tout ce que tu as dans le cœur, vas-y, voici je serai avec toi où tu voudras.
14:8	Et Yehonathan lui dit : Allons vers ces hommes et montrons-nous à eux.
14:9	S'ils nous disent : Attendez jusqu'à ce que nous venions à vous ! Alors nous resterons sur place et nous ne monterons pas vers eux.
14:10	Mais s'ils disent : Montez vers nous ! Nous irons, car YHWH les aura livrés entre nos mains. Que cela soit pour nous un signe.
14:11	Ils se montrèrent donc tous les deux au poste de garde des Philistins, et les Philistins dirent : Voici, les Hébreux sortent des trous où ils s'étaient cachés.
14:12	Et ceux du poste de garde dirent à Yonathan et à celui qui portait ses armes : Montez vers nous, nous avons quelque chose à vous apprendre. Alors Yonathan dit à celui qui portait ses armes : Monte avec moi, car YHWH les a livrés entre les mains d'Israël.
14:13	Yonathan monta en s'aidant des mains et des pieds, et celui qui portait ses armes le suivit. Puis ceux du poste de garde tombèrent sous les coups de Yonathan, et celui qui portait ses armes les tuait à sa suite.
14:14	Dans cette première victoire, Yonathan et celui qui portait ses armes, tuèrent environ vingt hommes, dans un espace d'environ une moitié d'un arpent de terre.
14:15	Et il y eut de la crainte dans le camp, dans les champs et parmi tout le peuple ; le poste de garde et ceux qui ravageaient furent eux aussi effrayés. Et le pays fut tellement troublé que cela fut comme une frayeur d'Elohîm.

### Victoire d'Israël

14:16	Et les sentinelles de Shaoul, qui étaient à Guibea de Benyamin, virent que la multitude était en désordre et s'en allait en s'entre-tuant.
14:17	Alors Shaoul dit au peuple qui était avec lui : Faites donc la revue et voyez qui s'en est allé du milieu de nous. Ils firent donc la revue, et voici Yonathan n'y était pas, ni celui qui portait ses armes.
14:18	Et Shaoul dit à Achiyah : Fais approcher l'arche d'Elohîm ! Car l'arche d'Elohîm était, en ce jour-là, avec les enfants d'Israël.
14:19	Mais il arriva que pendant que Shaoul parlait au prêtre, le tumulte venant du camp des Philistins augmentait de plus en plus. Shaoul dit au prêtre : Retire ta main !
14:20	Shaoul et tout le peuple furent rassemblés à grand cri et arrivèrent à la bataille. Et voici, les Philistins tournèrent l'épée les uns contre les autres, dans une très grande confusion.
14:21	Les Hébreux, qui étaient montés comme hier et avant-hier dans le camp des Philistins et qui étaient dispersés, se joignirent aux Israélites qui étaient avec Shaoul et Yonathan.
14:22	Et tous les Israélites qui s'étaient cachés dans la Montagne d'Éphraïm, ayant appris que les Philistins s'enfuyaient, les poursuivirent aussi pour les combattre.
14:23	Ce jour-là, YHWH délivra Israël, et le combat s'étendit jusqu'à Beth-Aven.

### Yehonathan (Yonathan) épargné des conséquences du vœu de Shaoul (Saül)

14:24	Les hommes d'Israël furent épuisés cette journée-là. Mais Shaoul avait fait jurer le peuple, en disant : Maudit soit l'homme qui prendra de la nourriture avant le soir, avant que je me sois vengé de mes ennemis ! Et le peuple ne goûta pas de nourriture.
14:25	Tout le peuple arriva dans une forêt, où il y avait du miel à la surface du sol.
14:26	Lorsque le peuple entra dans la forêt, il vit le miel qui coulait, mais personne ne porta la main à sa bouche, car le peuple craignait le serment.
14:27	Or Yonathan n'avait pas entendu son père lorsqu'il avait fait jurer le peuple. Il étendit le bout du bâton qu'il avait à la main, le trempa dans un rayon de miel et porta sa main à sa bouche et ses yeux furent éclaircis.
14:28	Alors un homme du peuple lui dit : Ton père a fait jurer, il a fait jurer le peuple en disant : Maudit soit l'homme qui mangera aujourd'hui quelque chose ! Alors que le peuple était fatigué.
14:29	Yonathan dit : Mon père trouble le peuple. Voyez comment mes yeux sont éclaircis après avoir goûté un peu de ce miel.
14:30	Certes, si le peuple avait mangé, mangé du butin de ses ennemis, la défaite des Philistins n'aurait-elle pas été plus grande ?
14:31	En ce jour-là donc ils frappèrent les Philistins de Micmash à Ayalon. Le peuple était très fatigué.
14:32	Puis il se jeta sur le butin, il prit des brebis, des bœufs et des veaux, et les égorgea sur la terre, et le peuple les mangeait avec le sang.
14:33	On le rapporta à Shaoul, en disant : Voici, le peuple pèche contre YHWH, en mangeant avec le sang. Et il dit : Vous avez trahi ! Roulez-moi ici une grosse pierre !
14:34	Et Shaoul dit : Allez parmi le peuple, et dites à chacun d'amener son bœuf et ses brebis, vous les égorgerez ici. Vous les mangerez et vous ne pécherez plus contre YHWH, en mangeant avec le sang. Et chacun amena, cette nuit-là, son bœuf à la main, et ils les égorgèrent.
14:35	Shaoul bâtit un autel à YHWH. Ce fut le premier autel qu'il bâtit à YHWH.
14:36	Puis Shaoul dit : Descendons et poursuivons de nuit les Philistins, afin de les piller jusqu'au matin, et n'en laissons pas un de reste. Ils lui répondirent : Fais ce qui est bon à tes yeux. Mais le prêtre dit : Approchons-nous d'abord d'Elohîm.
14:37	Alors Shaoul consulta donc Elohîm en disant : Descendrai-je à la poursuite des Philistins ? Les livreras-tu entre les mains d'Israël ? Mais il ne lui répondit pas ce jour-là.
14:38	Et Shaoul dit : Approchez ici, vous tous les chefs du peuple, recherchez et voyez par qui ce péché est arrivé aujourd'hui.
14:39	Car YHWH est vivant, lui qui délivre Israël, même s'il s'agit de mon fils, Yonathan, il mourra, il mourra. Mais dans tout le peuple, personne ne lui répondit.
14:40	Puis il dit à tout Israël : Mettez-vous d'un côté, et nous serons de l'autre, moi et mon fils, Yonathan. Le peuple répondit à Shaoul : Fais ce qui est bon à tes yeux.
14:41	Et Shaoul dit à YHWH, l'Elohîm d'Israël : Fais connaître la vérité. Yonathan et Shaoul furent désignés, et le peuple fut écarté.
14:42	Et Shaoul dit : Jetez le sort entre moi et Yonathan, mon fils. Et Yonathan fut désigné.
14:43	Alors Shaoul dit à Yonathan : Déclare-moi ce que tu as fait. Et Yonathan lui déclara et dit : J'ai goûté, j'ai goûté avec le bout de mon bâton que j'avais à la main un peu de miel : me voici, que je meurs !
14:44	Et Shaoul dit : Qu'Elohîm agisse à mon égard comme il le veut, tu mourras, tu mourras<!--Répétition des mots « tu mourras, tu mourras ». Voir commentaire en Ge. 2:17.-->, Yonathan.
14:45	Mais le peuple dit à Shaoul : Yonathan qui a accompli cette grande délivrance en Israël mourrait-il ? Loin de là ! YHWH est vivant ! Il ne tombera pas à terre un seul des cheveux de sa tête, car c'est avec Elohîm qu'il a agi en ce jour. Le peuple délivra Yonathan de la mort.
14:46	Shaoul renonça à poursuivre les Philistins, qui regagnèrent leur pays.

### Les guerres sous le règne de Shaoul (Saül)

14:47	Après que Shaoul eut pris possession de la royauté sur Israël, il fit la guerre de tous côtés contre ses ennemis : contre Moab, contre les fils d'Ammon, contre Édom, contre les rois de Tsoba et contre les Philistins ; partout où il se tournait, il était vainqueur.
14:48	Il manifesta sa puissance en frappant Amalek et délivra Israël de la main de ceux qui le pillaient.
14:49	Les fils de Shaoul étaient Yonathan, Yishviy et Malkishoua. Et quant aux noms de ses deux filles, le nom de l'aînée était Mérab et le nom de la plus jeune, Miykal.
14:50	Et le nom de la femme de Shaoul était Achinoam, fille d'Achimaats. Le nom du chef de son armée était Abner, fils de Ner, oncle de Shaoul.
14:51	Kis, père de Shaoul, et Ner, père d'Abner, étaient fils d'Abiel.
14:52	La guerre contre les Philistins fut violente durant toute la vie de Shaoul. Chaque fois que Shaoul remarquait tout homme vaillant ou tout fils talentueux, il le prenait auprès de lui.

## Chapitre 15

### Shaoul (Saül) désobéit une fois de plus

15:1	Et Shemouél dit à Shaoul : YHWH m'a envoyé pour t'oindre afin que tu sois roi sur son peuple, sur Israël. Maintenant donc, écoute les paroles de YHWH !
15:2	Ainsi parle YHWH Sabaoth : Je me rappelle de ce qu'Amalek a fait à Israël, comment il s'opposa à lui sur le chemin, à sa sortie d'Égypte.
15:3	Va maintenant et frappe Amalek. Détruisez par interdit tout ce qui lui appartient. Ne l'épargne pas, mais fais mourir hommes et femmes, enfants et nourrissons, bœufs et menu bétail, chameaux et ânes.
15:4	Shaoul donc convoqua le peuple et en fit la revue à Thelaïm : il y avait 200 000 hommes de pied, et 10 000 hommes de Yéhouda.
15:5	Et Shaoul marcha jusqu'à la ville d'Amalek et mit une embuscade dans la vallée.
15:6	Et Shaoul dit aux Kéniens : Allez, retirez-vous, séparez-vous des Amalécites, de peur que je ne vous détruise avec eux. En effet, vous avez agi avec bonté envers tous les enfants d'Israël, quand ils montèrent d'Égypte. Et les Kéniens se séparèrent des Amalécites.
15:7	Et Shaoul frappa les Amalécites depuis Haviylah jusqu'à Shour, qui est face à l'Égypte.
15:8	Il fit passer tout le peuple au fil de l'épée, le dévouant par interdit, mais il épargna Agag, roi d'Amalek.
15:9	Shaoul et le peuple épargnèrent Agag, les meilleures brebis, les meilleurs bœufs, les bêtes grasses, les agneaux, ce qu'il y avait de meilleur. Ils ne voulurent pas les dévouer par interdit, détruisant seulement tout ce qui était chétif et méprisable.
15:10	Alors la parole de YHWH vint à Shemouél en disant :
15:11	Je me repens d'avoir établi Shaoul pour roi, car il s'est détourné de moi et n'a pas exécuté mes paroles. Shemouél fut très irrité, et il cria à YHWH toute la nuit.

### YHWH rejette Shaoul (Saül)

15:12	Puis Shemouél se leva de bon matin pour aller rencontrer Shaoul. Et l’on fit ce rapport à Shemouél en disant : Shaoul est venu à Carmel, et voici, il s’est érigé un monument, puis il s’est tourné, a traversé et est descendu à Guilgal.
15:13	Shemouél se rendit auprès de Shaoul, et Shaoul lui dit : Sois béni de YHWH ! J'ai accompli la parole de YHWH.
15:14	Shemouél dit : Quel est donc ce bêlement de brebis qui parvient à mes oreilles, et ce mugissement de bœufs que j'entends ?
15:15	Et Shaoul répondit : Ils les ont amenés de chez les Amalécites, car le peuple a épargné les meilleures brebis et les meilleurs bœufs, pour les sacrifier à YHWH, ton Elohîm ; et nous avons détruit le reste, nous l'avons dévoué par interdit.
15:16	Shemouél dit à Shaoul : Laisse-moi et je te déclarerai ce que YHWH m'a dit cette nuit. Et il lui répondit : Parle !
15:17	Shemouél dit : N'est-il pas vrai que, quand tu étais petit à tes yeux, tu as été fait chef des tribus d'Israël, et YHWH t'a oint pour roi sur Israël ?
15:18	YHWH t'avait envoyé par un chemin et t'avait dit : Va et dévoue par interdit ces pécheurs, les Amalécites, et fais-leur la guerre jusqu'à ce qu'ils soient exterminés.
15:19	Pourquoi n'as-tu pas obéi à la voix de YHWH ? Pourquoi t'es-tu jeté sur le butin et as-tu fait ce qui est mal aux yeux de YHWH ?
15:20	Et Shaoul répondit à Shemouél : J'ai pourtant obéi à la voix de YHWH, et je suis allé par le chemin par lequel YHWH m'a envoyé. Et j'ai amené Agag, roi des Amalécites et j'ai dévoué les Amalécites, par interdit ;
15:21	mais le peuple a pris des brebis, des bœufs, du butin, le meilleur de ce qui devait être voué à une entière destruction pour le sacrifier à YHWH, ton Elohîm, à Guilgal.
15:22	Shemouél répondit : YHWH prend-il plaisir aux holocaustes et aux sacrifices, autant qu'à l'obéissance à sa voix ? Voici, l'obéissance vaut mieux que les sacrifices, et l'observation de sa parole vaut mieux que la graisse des béliers.
15:23	Car la rébellion est un péché autant que la divination, et la résistance ne l'est pas moins que l'idolâtrie et les théraphim. Puisque tu as rejeté la parole de YHWH, il te rejette aussi afin que tu ne sois plus roi.
15:24	Et Shaoul répondit à Shemouél : J'ai péché parce que j'ai transgressé le commandement de YHWH, ainsi que tes paroles, car je craignais le peuple, et j'ai obéi à sa voix.
15:25	Mais maintenant, je te prie, pardonne-moi mon péché, et reviens avec moi pour que je me prosterne devant YHWH.
15:26	Et Shemouél dit à Shaoul : Je n'irai pas avec toi : parce que tu as rejeté la parole de YHWH, YHWH te rejette afin que tu ne sois plus roi d'Israël.
15:27	Comme Shemouél se détournait pour s'en aller, Shaoul le saisit par le pan de son manteau qui se déchira.
15:28	Alors Shemouél lui dit : YHWH déchire aujourd'hui le royaume d'Israël de dessus toi et le donne à un autre, qui est meilleur que toi.
15:29	En effet, le Puissant d'Israël ne ment pas, il ne se repent pas, car il n'est pas un être humain pour se repentir.
15:30	Et Shaoul répondit : J'ai péché ! Mais honore-moi maintenant, je te prie, en présence des anciens de mon peuple et en présence d'Israël, et reviens avec moi, et je me prosternerai devant YHWH, ton Elohîm.
15:31	Shemouél retourna et suivit Shaoul, et Shaoul se prosterna devant YHWH.
15:32	Puis Shemouél dit : Amenez-moi Agag, roi d'Amalek ! Et Agag s'avança vers lui délicatement. Agag se disait : Certainement l'amertume de la mort est passée.
15:33	Mais Shemouél dit : Comme ton épée a privé les femmes de leurs enfants, ainsi ta mère entre les femmes sera privée d'enfants. Et Shemouél mit Agag en pièces devant YHWH à Guilgal.
15:34	Puis il s'en alla à Ramah, et Shaoul monta dans sa maison à Guibea de Shaoul.
15:35	Et Shemouél n'alla plus voir Shaoul jusqu'au jour de sa mort. En effet, Shemouél pleurait sur Shaoul, parce que YHWH s'était repenti d'avoir établi Shaoul roi sur Israël.

## Chapitre 16

### Shemouél (Samuel) envoyé à Bethléhem pour oindre David

16:1	YHWH dit à Shemouél : Quand cesseras-tu de pleurer sur Shaoul ? Je l'ai rejeté afin qu'il ne règne plus sur Israël. Remplis ta corne d'huile et viens ! Je t'enverrai chez Isaï, Bethléhémite, car je me suis pourvu d'un de ses fils pour roi.
16:2	Et Shemouél dit : Comment irai-je ? Car Shaoul l'apprendra et il me tuera. Et YHWH répondit : Tu emmèneras avec toi une jeune vache du troupeau, et tu diras : Je suis venu pour sacrifier à YHWH.
16:3	Et tu inviteras Isaï au sacrifice, et je te ferai savoir ce que tu auras à faire, et tu m'oindras celui que je te dirai.
16:4	Shemouél fit donc comme YHWH lui avait dit, et il alla à Bethléhem. Les anciens de la ville tout effrayés accoururent au-devant de lui et lui dirent : Ton arrivée annonce-t-elle la paix ?
16:5	Et il répondit : Soyez en paix ! Je suis venu pour sacrifier à YHWH. Sanctifiez-vous et venez avec moi au sacrifice. Il fit sanctifier aussi Isaï et ses fils, et les invita au sacrifice.
16:6	À son entrée, il remarqua Éliab, et se dit : Le mashiah de YHWH est certainement devant lui.
16:7	Mais YHWH dit à Shemouél : Ne prête pas attention à son apparence ni à la hauteur de sa taille, car je l'ai rejeté. Il ne s'agit pas ici de ce que voient les humains. Les humains voient ce qui leur saute aux yeux, mais YHWH voit le cœur.
16:8	Ensuite Isaï appela Abinadab et le fit passer devant Shemouél, mais il dit : YHWH n'a pas non plus choisi celui-ci.
16:9	Isaï fit passer Shammah ; et Shemouél dit : YHWH n'a pas non plus choisi celui-ci.
16:10	Ainsi Isaï fit passer ses sept fils devant Shemouél. Shemouél dit à Isaï : YHWH n'a pas choisi ceux-ci.
16:11	Puis Shemouél dit à Isaï : Sont-ce là tous tes garçons ? Et il dit : Il reste encore le plus jeune, seulement, il fait paître les brebis. Alors Shemouél dit à Isaï : Envoie-le chercher, car nous ne retournerons pas avant qu'il ne soit venu ici.
16:12	Il le fit donc venir. Il était roux, avec de beaux yeux et une belle apparence. Et YHWH dit à Shemouél : Lève-toi et oins-le, car c'est celui que j'ai choisi !
16:13	Alors Shemouél prit la corne d'huile et l'oignit au milieu de ses frères. Et depuis ce jour-là, l'Esprit de YHWH saisit David. Et Shemouél se leva et s'en alla à Ramah.

### David entre au service de Shaoul (Saül)

16:14	L'Esprit de YHWH se retira de Shaoul, et un mauvais esprit<!--Shaoul (Saül) a été frappé d'un esprit d'égarement (2 Th. 2:9-12). Voir commentaires en Ge. 6:3 et Mt. 12:31.--> envoyé par YHWH le terrifiait.
16:15	Les serviteurs de Shaoul lui dirent : Voici, un mauvais esprit envoyé d'Elohîm te terrifie.
16:16	Que le roi, notre seigneur, parle ! Tes serviteurs sont devant toi. Ils chercheront un homme qui sache jouer de la harpe ; et quand le mauvais esprit envoyé par Elohîm sera sur toi, il jouera de sa main, et tu seras soulagé.
16:17	Shaoul répondit à ses serviteurs : Trouvez-moi un homme qui sache bien jouer et amenez-le-moi.
16:18	L'un des serviteurs répondit : Voici, j'ai vu l'un des fils d'Isaï, le Bethléhémite, qui sait jouer des instruments, il est vaillant et talentueux, c'est un guerrier qui parle bien, bel homme, et YHWH est avec lui.
16:19	Alors Shaoul envoya des messagers à Isaï, pour lui dire : Envoie-moi David, ton fils, qui est avec les brebis.
16:20	Isaï prit un âne, qu'il chargea de pain, et une outre de vin, et un jeune chevreau, et les envoya par David, son fils, à Shaoul.
16:21	David arrivé chez Shaoul, se présenta devant lui. Shaoul l'aima beaucoup et il fut désigné pour porter ses armes.
16:22	Shaoul fit dire à Isaï : Je t'en prie, que David se tienne devant moi, car il a trouvé grâce à mes yeux.
16:23	Il arrivait donc que quand le mauvais esprit envoyé d'Elohîm était sur Shaoul, David prenait la harpe et en jouait de sa main. Shaoul respirait alors et se trouvait mieux, parce que le mauvais esprit se retirait de lui.

## Chapitre 17

### Goliath sème la terreur dans le camp d'Israël

17:1	Or les Philistins réunirent leurs armées pour faire la guerre, et ils se rassemblèrent à Soco, qui appartient à Yéhouda. Ils campèrent entre Soco et Azéqah, à Éphès-Dammim.
17:2	Shaoul et ceux d'Israël se rassemblèrent aussi. Ils campèrent dans la vallée du chêne et ils se mirent en ordre de bataille contre les Philistins.
17:3	Les Philistins étaient sur une montagne d'un côté, et les Israélites sur une montagne de l'autre côté : la vallée était entre eux.
17:4	Il sortit du camp des Philistins un homme qui se présentait entre les deux armées, il s'appelait Goliath, de la ville de Gath, haut de 6 coudées et d'un empan.
17:5	Il avait un casque en cuivre sur sa tête et était armé d'une cuirasse à écailles pesant 5 000 sicles de cuivre.
17:6	Il avait aussi des jambières en cuivre et un javelot en cuivre entre ses épaules.
17:7	Le bois de sa lance était comme une ensouple d'un tisserand, et la pointe de sa lance pesait 600 sicles de fer. Celui qui portait son bouclier marchait devant lui.
17:8	Il se présenta donc, et cria aux troupes d'Israël rangées en bataille, et leur dit : Pourquoi sortez-vous pour vous ranger en bataille ? Ne suis-je pas Philistin, et n'êtes-vous pas esclaves de Shaoul ? Choisissez l'un d'entre vous et qu'il descende contre moi.
17:9	S'il peut me battre et qu'il me tue, nous serons vos esclaves. Mais si c'est moi le vainqueur et si je le tue, vous serez nos esclaves et vous nous serez asservis.
17:10	Le Philistin dit : Je jette un défi en ce jour aux troupes rangées d'Israël : donnez-moi un homme, et nous combattrons ensemble.
17:11	Shaoul et tout Israël entendirent ces paroles du Philistin, ils furent épouvantés et eurent très peur.
17:12	Or David était le fils d'un homme éphratien de Bethléhem de Yéhouda nommé Isaï qui avait huit fils. Au temps de Shaoul, cet homme était vieux et posé parmi les hommes.
17:13	Et les trois fils aînés d'Isaï avaient suivi Shaoul à la guerre. Les noms de ses trois fils qui s'en étaient allés à la guerre étaient Éliab, le premier-né, Abinadab, le second, et Shammah, le troisième.
17:14	David était le plus jeune, et les trois plus grands suivaient Shaoul.
17:15	David allait et revenait d'auprès de Shaoul pour paître les brebis de son père à Bethléhem.
17:16	Et le Philistin, s'approchant le matin et le soir, se présenta pendant 40 jours.

### David prêt à affronter Goliath

17:17	Isaï dit à David, son fils : Prends maintenant pour tes frères un épha de ce blé rôti, et ces 10 pains, et porte-les promptement au camp, à tes frères.
17:18	Tu porteras aussi ces 10 fromages au chef de leur millier, tu t'informeras du bien-être de tes frères et tu m'en apporteras des nouvelles sûres.
17:19	Or Shaoul, et eux, et tous ceux d'Israël étaient dans la vallée du chêne, combattant contre les Philistins.
17:20	David se leva de bon matin. Il laissa les brebis aux soins d'un gardien, prit sa charge et s'en alla, comme son père Isaï le lui avait ordonné. Il arriva au retranchement où l'armée sortait pour se ranger en bataille et on poussait des cris de guerre.
17:21	Car les Israélites et les Philistins se rangèrent armée contre armée.
17:22	Alors David se déchargea de son bagage, le laissant entre les mains de celui qui gardait le bagage, et courut vers les rangs de l'armée. Aussitôt arrivé, il demanda à ses frères s'ils se portaient bien.
17:23	Et comme il parlait avec eux, le Philistin de Gath, nommé Goliath, sortit des rangs de l'armée des Philistins, se présenta entre les deux armées et proféra les mêmes paroles qu'il avait proférées auparavant, et David les entendit.
17:24	À la vue de cet homme, tous les hommes d'Israël s'enfuirent devant lui, saisis d'une grande frayeur.
17:25	Et les Israélites disaient : Avez-vous vu s'avancer cet homme ? Il est monté pour jeter un défi à Israël ! Mais si quelqu'un le tue, le roi l'enrichira de grandes richesses, et lui donnera sa fille, et affranchira la maison de son père en Israël.
17:26	Alors David parla aux personnes qui étaient là avec lui, en disant : Quel bien fera-t-on à l'homme qui frappera ce Philistin et qui ôtera l'opprobre de dessus Israël ? Car qui est ce Philistin, cet incirconcis, pour insulter l'armée d'Elohîm vivant ?
17:27	Et le peuple lui répéta ces mêmes paroles et lui dit : Voilà ce qu'on fera pour l'homme qui le tuera.
17:28	Son frère aîné Éliab l'entendit qui parlait à ces hommes et la colère d'Éliab s'enflamma contre David et il lui dit : Pourquoi es-tu descendu, et à qui as-tu laissé ce peu de brebis dans le désert ? Je connais ton orgueil et la malice de ton cœur, car tu es descendu pour voir la bataille.
17:29	Et David répondit : Qu'ai-je donc fait ? Ne puis-je pas parler ainsi ?
17:30	Puis il se détourna de lui vers un autre et lui posa les mêmes questions. Et le peuple lui répondit de la même manière comme la première fois.
17:31	Les paroles que David avait dites furent entendues et rapportées devant Shaoul qui le fit venir.
17:32	David dit à Shaoul : Que le cœur d'aucun homme ne soit abattu à cause de ce Philistin ! Ton serviteur ira et se battra contre lui.
17:33	Mais Shaoul dit à David : Tu ne peux aller te battre contre ce Philistin, car tu n'es qu'un jeune homme, et il est un homme de guerre depuis sa jeunesse.
17:34	David répondit à Shaoul : Ton serviteur faisait paître les brebis de son père. Quand un lion ou un ours venait emporter une brebis du troupeau,
17:35	je le poursuivais, je le frappais et j'arrachais la brebis de sa gueule. S'il se jetait sur moi, je le saisissais par la mâchoire, je le frappais et je le tuais.
17:36	Ton serviteur a tué et le lion, et l'ours, et ce Philistin, cet incirconcis, sera comme l'un d'eux, car il a déshonoré l'armée d'Elohîm vivant.
17:37	David dit encore : YHWH qui m'a délivré de la griffe du lion et de la patte de l'ours me délivrera de la main de ce Philistin. Alors Shaoul dit à David : Va, et que YHWH soit avec toi !

### David tue Goliath ; les Philistins sont battus

17:38	Shaoul fit revêtir David de ses vêtements, et lui mit son casque de cuivre sur sa tête, et lui fit endosser une cuirasse.
17:39	Puis David ceignit l'épée par-dessus ses vêtements et voulut marcher, car il ne l'avait jamais essayé. Et David dit à Shaoul : Je ne pourrais pas marcher avec cela, car je ne l'ai jamais essayé ! Et il s'en débarrassa.
17:40	Alors il prit en main son bâton, et se choisit dans le torrent cinq pierres bien polies, et les mit dans le sac de berger et dans sa poche. Puis sa fronde en main, il s'approcha du Philistin.
17:41	Le Philistin aussi s'avança et s'approcha de plus en plus près de David, précédé de l'homme qui portait son bouclier.
17:42	Le Philistin regarda, et lorsqu'il vit David, il le méprisa, car ce n'était qu'un jeune garçon, roux et beau de figure.
17:43	Le Philistin dit à David : Suis-je un chien, pour que tu viennes contre moi avec des bâtons ? Et le Philistin maudit David par ses elohîm.
17:44	Le Philistin ajouta : Viens vers moi, et je donnerai ta chair aux oiseaux du ciel et aux bêtes des champs.
17:45	Et David dit au Philistin : Tu marches contre moi avec l'épée, la lance et le javelot, mais moi, je marche contre toi au Nom de YHWH Sabaoth, l'Elohîm des lignes de bataille d’Israël que tu as insulté.
17:46	Aujourd'hui YHWH te livrera entre mes mains. Je t'abattrai et je te couperai la tête. Aujourd'hui je donnerai les cadavres du camp des Philistins aux oiseaux du ciel et aux animaux de la terre. Et toute la Terre saura qu'Israël a un Elohîm.
17:47	Et toute cette assemblée saura que YHWH ne délivre pas par l'épée ni par la lance. Car la victoire appartient à YHWH, qui vous livrera entre nos mains.
17:48	Et il arriva que comme le Philistin se levait et s'avançait à la rencontre de David, David se hâta de courir vers la ligne de bataille à la rencontre du Philistin.
17:49	Alors David mit la main à son sac, prit une pierre et la lança avec sa fronde. Il frappa tellement le Philistin au front que la pierre s'enfonça dans son front. Il tomba le visage contre terre.
17:50	Ainsi avec une fronde et une pierre, David fut plus fort que le Philistin ; il le frappa et le tua, sans avoir une épée à la main.
17:51	Alors David courut, se jeta sur le Philistin, prit son épée, la tira de son fourreau, le tua, et lui coupa la tête. Les Philistins, voyant que leur homme vaillant était mort, prirent la fuite.
17:52	Alors les hommes d'Israël et de Yéhouda se levèrent, et poussèrent des cris de guerre, et poursuivirent les Philistins jusqu'à la vallée et jusqu'aux portes d'Ékron. Les Philistins blessés à mort tombèrent dans le chemin de Shaaraïm, jusqu'à Gath et jusqu'à Ékron.
17:53	Et les fils d'Israël revinrent de la poursuite des Philistins et pillèrent leurs camps.
17:54	David prit la tête du Philistin et la porta à Yeroushalaim, et il mit aussi dans sa tente les armes du Philistin.
17:55	Quand Shaoul vit David sortant à la rencontre du Philistin, il dit à Abner, chef de l'armée : Abner, de qui ce jeune homme est-il le fils ? Abner répondit : Que ton âme vive, ô roi ! Je n'en sais rien.
17:56	Le roi lui dit : Informe-toi de qui ce jeune garçon est le fils.
17:57	Et quand David fut de retour après avoir tué le Philistin, Abner le prit et le mena devant Shaoul. David avait la tête du Philistin à la main.
17:58	Et Shaoul lui dit : Jeune garçon, de qui es-tu le fils ? David répondit : Je suis le fils d'Isaï, Bethléhémite, ton serviteur.

## Chapitre 18

### Alliance entre Yehonathan (Yonathan) et David

18:1	Or il arriva qu'aussitôt que David eut achevé de parler à Shaoul, l'âme de Yehonathan fut attachée à l'âme de David. Yehonathan l'aima comme son âme.
18:2	Ce jour-là donc Shaoul le retint, et ne lui permit plus de retourner à la maison de son père.
18:3	Alors Yehonathan fit alliance avec David, parce qu'il l'aimait comme son âme.
18:4	Yehonathan se dépouilla de la robe qu'il portait et la donna à David, avec ses habits, jusqu'à son épée, son arc et sa ceinture.

### Shaoul (Saül) est jaloux de David et cherche à le tuer

18:5	David allait partout où l'envoyait Shaoul et il prospérait, de sorte que Shaoul le mit à la tête des hommes de guerre. Il était agréable aux yeux de tout le peuple et même aux yeux des serviteurs de Shaoul.
18:6	Or il arriva que, comme ils revenaient, lors du retour de David après qu'il eut tué le Philistin, des femmes sortirent de toutes les villes d'Israël, en chantant et dansant devant le roi Shaoul, avec des tambourins, des triangles et en poussant des cris de joie.
18:7	Les femmes chantaient, se répondant les unes aux autres, en disant : Shaoul a frappé ses 1 000, et David ses 10 000.
18:8	Shaoul fut très irrité car cette parole fut mauvaise à ses yeux. Il dit : Elles en ont donné 10 000 à David, et à moi 1 000 ! Il ne lui manque plus que le royaume.
18:9	Depuis ce jour-là, Shaoul regardait David d'un mauvais œil.
18:10	Et il arriva, dès le lendemain que le mauvais esprit d'Elohîm saisit Shaoul, et il faisait le prophète au milieu de la maison, et David joua de sa main, comme les autres jours, et Shaoul avait une lance dans sa main.
18:11	Shaoul jeta sa lance, se disant : Je frapperai David contre le mur. Mais David se détourna de devant lui par deux fois.
18:12	Shaoul avait peur de la présence de David, parce que YHWH était avec David, et qu'il s'était retiré de Shaoul.
18:13	C'est pourquoi Shaoul éloigna David de lui et l'établit chef de mille. Et David allait et venait devant le peuple.
18:14	David réussissait dans tout ce qu'il entreprenait, car YHWH était avec lui.
18:15	Shaoul, voyant que David réussissait beaucoup, avait peur de sa présence.
18:16	Mais tout Israël et Yéhouda aimaient David, parce qu'il allait et venait devant eux.

### David épouse Miykal, fille de Shaoul (Saül)

18:17	Shaoul dit à David : Voici, je te donnerai Mérab ma fille aînée pour femme. Mais sois pour moi un fils talentueux et conduis les guerres de YHWH. Car Shaoul disait : Que ma main ne le touche pas, mais que ce soit celle des Philistins.
18:18	David répondit à Shaoul : Qui suis-je, et quelle est ma vie, et la famille de mon père en Israël, pour que je devienne le gendre du roi ?
18:19	Or il arriva qu'au temps où l'on devait donner Mérab, fille de Shaoul, à David, elle fut donnée pour femme à Adriel de Mehola.
18:20	Mais Miykal, fille de Shaoul, aima David. On en informa Shaoul, et la chose fut bonne à ses yeux.
18:21	Et Shaoul dit : Je la lui donnerai, afin qu'elle soit pour lui un piège, et que par ce moyen la main des Philistins l'atteigne. Shaoul donc dit à David pour la seconde fois : Tu seras aujourd'hui mon gendre.
18:22	Et Shaoul ordonna à ses serviteurs de parler à David en secret et de lui dire : Voici, le roi prend plaisir en toi, et tous ses serviteurs t'aiment ; sois donc maintenant le gendre du roi.
18:23	Les serviteurs de Shaoul répétèrent toutes ces paroles à David, et David répondit : Pensez-vous qu'il soit facile de devenir le gendre du roi, moi qui suis un homme pauvre, et peu important ?
18:24	Et les serviteurs de Shaoul le lui rapportèrent et lui dirent : David a tenu tel discours.
18:25	Shaoul dit : Vous parlerez ainsi à David : Le roi ne désire pas de dot, mais 100 prépuces de Philistins, afin d'être vengé de ses ennemis. Or Shaoul avait pour but de faire tomber David aux mains des Philistins.
18:26	Les serviteurs de Shaoul rapportèrent tous ces discours à David, et le discours fut juste aux yeux de David de devenir le gendre du roi. Le temps n'était pas encore écoulé,
18:27	que David se leva et s'en alla, lui et ses hommes, et tua 200 hommes parmi les Philistins. Il apporta leurs prépuces, et on les livra au complet au roi, afin qu'il devienne le gendre du roi. Alors Shaoul lui donna pour femme Miykal, sa fille.
18:28	Shaoul vit et comprit que YHWH était avec David et que Miykal, fille de Shaoul, l'aimait.
18:29	Shaoul eut de plus en plus peur en face de David et fut toujours l'ennemi de David.
18:30	Les chefs des Philistins firent des incursions, mais chaque fois qu'ils sortaient, David remportait du succès mieux que tous les serviteurs de Shaoul, et son nom fut très estimé.

## Chapitre 19

### David échappe aux assauts de Shaoul (Saül)

19:1	Shaoul parla à Yonathan, son fils, et à tous ses serviteurs de faire mourir David.
19:2	Mais Yehonathan<!--Voir commentaire en 1 S. 13:2.-->, fils de Shaoul, avait une grande affection pour David. C'est pourquoi Yehonathan le fit savoir à David et lui dit : Shaoul, mon père, cherche à te faire mourir. Maintenant donc, tiens-toi sur tes gardes jusqu'au matin, demeure dans un lieu secret et cache-toi.
19:3	Pour moi, je sortirai et je me tiendrai à côté de mon père dans le champ où tu seras. Je parlerai de toi à mon père. Je verrai ce qu'il en est et je te le rapporterai.
19:4	Yehonathan parla favorablement de David à Shaoul, son père, et lui dit : Que le roi ne pèche pas contre son serviteur David, car il n'a pas péché contre toi. Au contraire, ses actions ont été très bonnes à ton égard :
19:5	car il a mis sa vie en sa main, il a tué le Philistin et YHWH a opéré une grande délivrance pour tout Israël. Tu l'as vu et tu t'en es réjoui. Pourquoi donc pécherais-tu contre le sang innocent en faisant mourir David sans cause ?
19:6	Shaoul écouta la voix de Yehonathan et jura : YHWH est vivant ! Il ne mourra pas.
19:7	Alors Yehonathan appela David, et Yehonathan lui rapporta toutes ces choses. Yehonathan l'introduisit auprès de Shaoul, et il fut à son service comme hier et avant-hier.
19:8	La guerre ayant recommencé, David se mit en campagne et frappa les Philistins, et leur infligea une grande défaite, de sorte qu'ils prirent la fuite.
19:9	Le mauvais esprit de YHWH fut sur Shaoul, comme il était assis dans sa maison, ayant sa lance à la main. David jouait de sa main,
19:10	Shaoul chercha à frapper de sa lance David et le mur, mais il se détourna de devant Shaoul, qui, de sa lance, frappa le mur. David s'enfuit et s'échappa cette nuit-là.
19:11	Shaoul envoya des messagers à la maison de David pour le garder et le faire mourir au matin. Miykal, femme de David, l'en informa en disant : Si tu ne sauves pas ton âme, demain on te fera mourir.
19:12	Miykal fit descendre David par une fenêtre, et ainsi il s'en alla et s'enfuit.
19:13	Ensuite Miykal prit un théraphim, qu'elle plaça dans le lit. Elle mit une peau de chèvre à son chevet et l'enveloppa d'une couverture.
19:14	Lorsque Shaoul envoya des messagers pour prendre David, elle dit : Il est malade.
19:15	Shaoul envoya encore des messagers pour prendre David, en leur disant : Apportez-le-moi dans son lit, afin que je le fasse mourir.
19:16	Ces messagers entrèrent, et voici, un théraphim était au lit, et la peau de chèvre à son chevet.
19:17	Shaoul dit à Miykal : Pourquoi m'as-tu trompé de la sorte, et as-tu laissé aller mon ennemi, de sorte qu'il s'est échappé ? Et Miykal répondit à Shaoul : Il m'a dit : Laisse-moi aller, pourquoi te tuerais-je ?
19:18	C'est ainsi que David prit la fuite et qu'il s'échappa. Il se rendit auprès de Shemouél à Ramah, et lui raconta tout ce que Shaoul lui avait fait. Puis il s'en alla avec Shemouél, et ils demeurèrent à Nayoth.
19:19	On le rapporta à Shaoul, en lui disant : Voici, David est à Nayoth, en Ramah.
19:20	Alors Shaoul envoya des messagers pour s'emparer de David. Ils virent une assemblée de prophètes qui prophétisaient, et Shemouél était debout, les présidant. L'Esprit d'Elohîm saisit les messagers de Shaoul, qui prophétisèrent aussi.
19:21	On le rapporta à Shaoul, qui envoya d'autres messagers, et eux aussi prophétisèrent. Shaoul en envoya encore pour la troisième fois, et ils prophétisèrent également.
19:22	Alors il alla lui-même à Ramah. Arrivé à la grande citerne qui est à Sécou, il s'informa en disant : Où sont Shemouél et David ? Et on lui répondit : Ils sont à Nayoth, en Ramah.
19:23	Il se dirigea vers Nayoth, en Ramah. Et l'Esprit d'Elohîm le saisit à son tour, et il continua son chemin en prophétisant, jusqu'à son arrivée à Nayoth, en Ramah.
19:24	Il se dépouilla lui aussi de ses vêtements et prophétisa devant Shemouél. Il se jeta à terre nu, tout ce jour-là et toute la nuit. C'est pourquoi on dit : Shaoul est-il aussi parmi les prophètes ?

## Chapitre 20

### Renouvellement de l'alliance entre David et Yehonathan (Yonathan)

20:1	David s'enfuit de Nayoth, qui est en Ramah. Il vint dire en face à Yehonathan : Qu'ai-je fait ? Quelle est mon iniquité, et quel est mon péché devant ton père, pour qu'il en veuille à ma vie ?
20:2	Il<!--Yehonathan.--> lui répondit : Loin de là ! Tu ne mourras pas. Voici, mon père ne fait ni grande chose ni petite chose, sans la découvrir à mon oreille. Pourquoi mon père me cacherait-il cette chose-là ? Il n'en est rien.
20:3	David dit encore en jurant : Ton père sait, il le sait que j'ai trouvé grâce à tes yeux et il aura dit : Que Yehonathan ne sache rien de ceci, de peur qu'il n'en soit attristé. Mais YHWH est vivant, et ton âme vit, il n'y a qu'un pas entre moi et la mort.
20:4	Alors Yehonathan dit à David : Que désires-tu que je fasse ? Et je le ferai pour toi.
20:5	Et David dit à Yehonathan : Voici, c'est demain la nouvelle lune, et je devrais m'asseoir auprès du roi pour manger, laisse-moi donc aller, et je me cacherai dans les champs jusqu'au soir du troisième jour.
20:6	Si ton père me cherche, s'il me cherche tu lui répondras : David m'a demandé la permission de courir à Bethléhem, sa ville, parce que toute sa famille fait un sacrifice annuel.
20:7	S'il dit ainsi : C'est bien ! Ton serviteur n'a rien à craindre. Mais s'il se fâche, s'il se fâche, sache qu'il a résolu mon malheur.
20:8	Use donc de bonté envers ton serviteur, puisque tu as conclu une alliance avec ton serviteur devant YHWH. S'il y a de l'iniquité en moi, tue-moi toi-même, car pourquoi me mènerais-tu jusqu'à ton père ?
20:9	Yehonathan lui dit : Elohîm t'en garde ! Si j'apprenais, si j'apprenais que mon père a résolu de te faire du mal, ne t'en informerais-je pas ?
20:10	David répondit à Yehonathan : Qui m'avertira si la réponse que t'aura faite ton père est sévère ?
20:11	Et Yehonathan dit à David : Viens et sortons dans les champs. Ils sortirent donc eux deux dans les champs.
20:12	Alors Yehonathan dit à David : Par YHWH, l'Elohîm d'Israël ! Je sonderai mon père demain, environ à cette heure, ou après-demain, et s'il est favorable envers David, et que je n'envoie personne vers toi pour t'en informer,
20:13	qu'ainsi YHWH traite Yehonathan et qu'ainsi il y ajoute ! Si mon père a résolu de te faire du mal, je t'en informerai, et je te laisserai aller, et tu t'en iras en paix, de sorte que YHWH sera avec toi comme il a été avec mon père.
20:14	Si je vis encore, tu useras de la bonté de YHWH envers moi, en sorte que je ne meure pas.
20:15	Ne retire jamais ta bonté de ma maison, pas même quand YHWH retranchera tous les ennemis de David de dessus la face de la Terre.
20:16	Ainsi Yehonathan traita alliance avec la maison de David, en disant : Que YHWH le redemande de la main des ennemis de David !
20:17	Yehonathan se lia encore par serment à David pour l'amour qu'il lui portait, car il l'aimait comme son âme.
20:18	Puis Yehonathan lui dit : C'est demain la nouvelle lune, et on te cherchera, car ta place sera vide.
20:19	Le troisième jour, au soir, tu descendras en hâte, jusqu'au fond du lieu où tu t'étais caché le jour de cette affaire, et tu resteras près de la pierre d'Ézel.
20:20	Je tirerai trois flèches à côté de cette pierre, comme si je visais un but.
20:21	Et voici, j'enverrai un jeune homme, et je lui dirai : Va, trouve les flèches ! Si je dis au jeune homme : Voici, les flèches sont au-deçà de toi, prends-les ! Alors viens, car la paix est avec toi et tu n'as rien à craindre, YHWH est vivant !
20:22	Mais si je dis ainsi au jeune homme : Voici, les flèches sont au-delà de toi ! Alors va-t'en, car YHWH te renvoie.
20:23	Et quant à la parole que nous nous sommes donnée, toi et moi, voici, YHWH est entre moi et toi, à jamais.

### Shaoul (Saül) en colère contre Yehonathan (Yonathan)

20:24	David donc se cacha dans le champ. La nouvelle lune étant venue, le roi s'assit pour prendre son repas.
20:25	Et le roi s'assit à sa place, comme à l'ordinaire, sur son siège près du mur, Yehonathan se leva, et Abner s'assit à côté de Shaoul ; mais la place de David resta vide.
20:26	Shaoul ne dit rien ce jour-là, car il se disait : C'est un accident, il n'est pas pur, certainement il n'est pas pur.
20:27	Mais le lendemain, le second jour de la nouvelle lune, la place de David était encore vide. Et Shaoul dit à Yehonathan, son fils : Pourquoi le fils d'Isaï n'a-t-il été ni hier ni aujourd'hui au repas ?
20:28	Et Yehonathan répondit à Shaoul : David m'a demandé, il m'a demandé la permission d'aller à Bethléhem.
20:29	Même il m'a dit : Je te prie, laisse-moi aller, car notre famille fait un sacrifice dans la ville et mon frère m'a ordonné de m'y trouver. Maintenant donc si j'ai trouvé grâce à tes yeux, je te prie, que j'y aille, afin que je voie mes frères. C'est pour cela qu'il n'est pas venu à la table du roi.
20:30	Alors la colère de Shaoul s'enflamma contre Yehonathan et il lui dit : Fils déformé et rebelle, ne sais-je pas que tu as choisi le fils d'Isaï à ta honte et à la honte de la nudité de ta mère ?
20:31	Car aussi longtemps que le fils d'Isaï sera vivant sur la Terre, tu ne seras pas stable, ni toi, ni ta royauté. C'est pourquoi, maintenant, amène-le-moi, car c'est un fils de la mort.
20:32	Et Yehonathan répondit à Shaoul, son père, et lui dit : Pourquoi le ferait-on mourir ? Qu'a-t-il fait ?
20:33	Et Shaoul lança sa lance contre lui pour le frapper. Alors Yehonathan reconnut que son père avait résolu la mort de David.
20:34	Yehonathan se leva de table dans une ardente colère et ne mangea pas le pain le deuxième jour de la nouvelle lune, car il était affligé à cause de David, parce que son père l'avait insulté.
20:35	Le matin venu, Yehonathan sortit dans les champs, au lieu convenu avec David, et il amena avec lui un petit garçon.
20:36	Et il dit à son garçon : Cours, trouve maintenant les flèches que je m'en vais tirer. Et le garçon courut, et il<!--Yehonathan.--> tira une flèche qui le dépassa.
20:37	Lorsque le garçon arriva au lieu où était la flèche que Yehonathan avait tirée, Yehonathan cria après lui et lui dit : La flèche n'est-elle pas plus loin de toi ?
20:38	Yehonathan cria encore après le garçon : Hâte-toi, ne t'arrête pas ! Et le garçon de Yehonathan ramassa les flèches et revint vers son maître.
20:39	Le garçon ne savait rien de cette affaire, car seuls David et Yehonathan le savaient.
20:40	Yehonathan remit ses armes au garçon et lui dit : Va, porte-les à la ville.
20:41	Le garçon parti, David se leva du côté du sud, se jeta le visage contre terre et se prosterna à trois reprises. Les deux amis s'embrassèrent et pleurèrent ensemble, David versa d'abondantes larmes.
20:42	Yehonathan dit à David : Va en paix, comme nous l'avons juré au Nom de YHWH, en disant : Que YHWH soit entre moi et toi, entre ma postérité et ta postérité.
20:43	David donc se leva, s'en alla, et Yehonathan rentra dans la ville.

## Chapitre 21

### David et Achimélec le prêtre

21:1	David se rendit à Nob, vers Achimélec, le prêtre, qui tout effrayé courut au-devant de David, et lui dit : Pourquoi es-tu seul et n'y a-t-il personne avec toi ?
21:2	David répondit à Achimélec, le prêtre : Le roi m'a donné un ordre à propos d'une affaire et m'a dit : Que personne ne sache rien de l'affaire pour laquelle je t'envoie, ni de l'ordre que je t'ai donné. J'ai donné rendez-vous à mes jeunes hommes en un certain lieu.
21:3	Maintenant donc qu'as-tu sous la main ? Donne-moi cinq pains ou ce qui s'y trouvera.
21:4	Le prêtre répondit à David et dit : Je n'ai pas de pain profane sous la main, il n'y a que du pain sacré<!--Mt. 12:4.-->, pourvu que tes jeunes hommes se soient abstenus de femmes !
21:5	David répondit au prêtre : Nous nous sommes abstenus de femmes depuis trois jours que je suis parti. Si les armes de mes jeunes hommes sont consacrées pour un voyage profane, à plus forte raison aujourd'hui sont-ils tous consacrés avec leurs armes.
21:6	Alors le prêtre lui donna du pain sacré, car il n'y avait pas là d'autres pains que les pains de proposition qui avaient été ôtés de devant YHWH, pour le remplacer par du pain chaud le jour où on l'avait pris.
21:7	Or il y avait là un homme d'entre les serviteurs de Shaoul, retenu ce jour-là devant YHWH. Il s'appelait Doëg, un Édomite, le plus puissant de tous les bergers de Shaoul.
21:8	David dit à Achimélec : Mais n'as-tu pas ici sous la main quelque lance ou quelque épée ? Car je n'ai pas pris mon épée ni mes armes sur moi, parce que l'ordre du roi était pressant.
21:9	Et le prêtre dit : Voici l'épée de Goliath, le Philistin, que tu as tué dans la vallée du chêne. Elle est enveloppée d'un drap, derrière l'éphod. Si tu veux la prendre pour toi, prends-la, car il n'y en a pas ici d'autres que celle-là. Et David dit : Il n'y en a pas de pareille. Donne-la-moi !

### David s'enfuit à Gath

21:10	Alors David se leva et s'enfuit ce jour-là, loin de Shaoul, et s'en alla vers Akish, roi de Gath.
21:11	Et les serviteurs d'Akish lui dirent : N'est-ce pas là David, roi du pays ? N'est-ce pas celui dont on chantait et répondait en dansant : Shaoul a tué ses 1 000, et David ses 10 000 ?
21:12	David mit ces paroles dans son cœur et eut une grande peur à cause d'Akish, roi de Gath.
21:13	Il changea son comportement<!--Ps. 34:1.--> à leurs yeux et se conduisit entre leur main comme un fou. Il faisait des marques sur les battants des portes et laissait couler sa salive sur sa barbe.
21:14	Et Akish dit à ses serviteurs : Ne voyez-vous pas que cet homme est fou ? Pourquoi me l'avez-vous amené ?
21:15	Est-ce que je manque de fous, pour que vous m'ameniez celui-ci pour faire le fou devant moi ? Faudrait-il que cet homme entre dans ma maison ?

## Chapitre 22

### David se réfugie dans la caverne d'Adoullam<!--1 Ch. 12:16-18.-->

22:1	Et David partit de là et se sauva dans la caverne d'Adoullam. Ses frères et toute la maison de son père l'ayant appris, ils descendirent vers lui.
22:2	Alors se rassemblèrent autour de lui tous les hommes qui étaient dans la détresse, tous les hommes qui avaient des créanciers ou qui avaient l'âme pleine d'amertume, et il devint leur chef. Et il y eut avec lui environ 400 hommes.
22:3	David s'en alla de là à Mitspé dans le pays de Moab. Il dit au roi de Moab : Permets, je te prie, à mon père et à ma mère de se retirer chez vous, jusqu'à ce que je sache ce qu'Elohîm fera de moi.
22:4	Il les amena devant le roi de Moab, et ils demeurèrent chez lui, tout le temps que David fut dans cette forteresse.
22:5	Or Gad, le prophète, dit à David : Ne reste pas dans cette forteresse, mais va-t'en et entre dans le pays de Yéhouda. David donc s'en alla et vint dans la forêt de Héreth.

### Shaoul (Saül) fait tuer les prêtres

22:6	Shaoul apprit qu'on avait découvert David et ses hommes. Or Shaoul était assis sous le tamaris, à Guibea, sur la hauteur. Il avait sa lance à la main et tous ses serviteurs se tenaient devant lui.
22:7	Et Shaoul dit à ses serviteurs qui se tenaient près de lui : Écoutez Benyamites ! Le fils d'Isaï vous donnera-t-il à vous tous des champs et des vignes ? Vous établira-t-il tous chefs de mille, et chefs de cent ?
22:8	Pourquoi avez-vous tous conspiré contre moi, pourquoi n'y a-t-il personne qui le révèle<!--Ou « découvrir l'oreille ».--> à mon oreille, quand mon fils conclut une alliance avec le fils d'Isaï ? Pourquoi n'y a-t-il aucun de vous qui souffre à mon sujet et qui révèle à mon oreille que mon fils a suscité mon serviteur contre moi pour me dresser des embûches, comme il le fait aujourd'hui ?
22:9	Alors Doëg, l'Édomite, qui était établi sur les serviteurs de Shaoul, répondit et dit : J'ai vu le fils d'Isaï venir à Nob, auprès d'Achimélec, fils d'Ahitoub.
22:10	Il a consulté YHWH pour lui, il lui a donné des vivres ainsi que l'épée de Goliath, le Philistin.
22:11	Alors le roi envoya appeler Achimélec, le prêtre, fils d'Ahitoub, la maison de son père, et les prêtres qui étaient à Nob. Et ils vinrent tous vers le roi.
22:12	Shaoul dit : Écoute, fils d'Ahitoub ! Et il répondit : Me voici, mon seigneur !
22:13	Alors Shaoul lui dit : Pourquoi avez-vous conspiré contre moi, toi et le fils d'Isaï ? Pourquoi lui as-tu donné du pain et une épée, et as-tu consulté Elohîm pour lui, pour qu'il s'élève contre moi comme il le fait aujourd'hui, pour me dresser des embûches ?
22:14	Et Achimélec répondit au roi et dit : Entre tous tes serviteurs y en a-t-il un comme David, fidèle, et gendre du roi, qui est parti sur ton commandement, et qui est si honoré dans ta maison ?
22:15	Est-ce aujourd'hui que j'ai commencé à consulter Elohîm pour lui ? Loin de moi ! Que le roi n'impute aucun tort à son serviteur, à personne de la maison de mon père, car ton serviteur ne sait rien de tout cela, petite ou grande.
22:16	Le roi lui dit : Tu mourras, tu mourras<!--Le mot est répété deux fois. Voir commentaire en Ge. 2:16.-->, Achimélec, toi et toute la maison de ton père.
22:17	Alors le roi dit aux coureurs qui se tenaient devant lui : Approchez-vous et mettez à mort les prêtres de YHWH, car leur main est avec David. Ils savaient en effet qu'il était en fuite et ils ne l'ont pas révélé à mon oreille. Mais les serviteurs du roi ne voulurent pas étendre leurs mains pour frapper les prêtres de YHWH.
22:18	Alors le roi dit à Doëg : Approche-toi et frappe les prêtres ! Et Doëg, l'Édomite, se tourna et frappa les prêtres. Il tua en ce jour-là 85 hommes qui portaient l'éphod en lin.
22:19	Il frappa encore du tranchant de l'épée Nob, ville des prêtres, hommes et femmes, enfants et nourrissons, bœufs, ânes et brebis, tombèrent sous le tranchant de l'épée.
22:20	Toutefois un des fils d'Achimélec, fils d'Ahitoub, qui s'appelait Abiathar, se sauva et s'enfuit auprès de David.
22:21	Abiathar rapporta à David que Shaoul avait tué les prêtres de YHWH.
22:22	David dit à Abiathar : Doëg, l'Édomite, était présent ce jour-là, et je savais bien qu'il raconterait, qu'il raconterait tout à Shaoul. Je suis la cause de la mort de toutes les personnes de la maison de ton père.
22:23	Reste avec moi, ne crains rien, car celui qui cherche ma vie cherche la tienne, avec moi, tu seras bien gardé.

## Chapitre 23

### David libère Qe'iylah (Keïla)

23:1	On fit ce rapport à David, en disant : Voici, les Philistins font la guerre à Qe'iylah, et pillent les aires de battage.
23:2	Et David consulta YHWH<!--La clé du succès de David était YHWH. Il consultait régulièrement Elohîm avant de s'engager dans une guerre (Ps. 60:14.).--> en disant : Irai-je et frapperai-je ces Philistins ? Et YHWH répondit à David : Va et tu frapperas les Philistins, et tu délivreras Qe'iylah.
23:3	Les hommes de David lui dirent : Voici, nous avons peur ici en Yéhouda. Que sera-ce donc quand nous irons à Qe'iylah contre les troupes des Philistins ?
23:4	C'est pourquoi David consulta encore YHWH, et YHWH lui répondit et dit : Lève-toi, descends à Qe'iylah, car je vais livrer les Philistins entre tes mains.
23:5	Alors David s'en alla avec ses hommes à Qe'iylah, et combattit contre les Philistins. Il emmena leur bétail et leur infligea une grande défaite. C'est ainsi que David délivra les habitants de Qe'iylah.
23:6	Or il était arrivé que quand Abiathar, fils d'Achimélec, s'était enfui vers David à Qe'iylah, il avait en main l'éphod.
23:7	On rapporta à Shaoul que David était venu à Qe'iylah et Shaoul dit : Elohîm l'a livré entre mes mains car il s'est enfermé en entrant dans une ville qui a des portes et des barres.
23:8	Shaoul convoqua tout le peuple pour aller à la guerre, afin de descendre à Qe'iylah, et d'assiéger David et ses hommes.
23:9	David, ayant eu connaissance des mauvais desseins de Shaoul à son égard, dit au prêtre Abiathar : Apporte l'éphod !
23:10	Puis David dit : Ô YHWH, Elohîm d'Israël, ton serviteur a entendu, il a entendu que Shaoul cherche à venir à Qe'iylah pour détruire la ville à cause de moi.
23:11	Les chefs de Qe'iylah me livreront-ils entre ses mains ? Shaoul descendra-t-il comme ton serviteur l'a entendu dire ? Ô YHWH, Elohîm d'Israël, je te prie, révèle-le à ton serviteur. Et YHWH répondit : Il descendra.
23:12	David dit encore : Les chefs de Qe'iylah me livreront-ils, moi et mes hommes, entre les mains de Shaoul ? Et YHWH répondit : Ils te livreront.

### David échappe encore à Shaoul (Saül)

23:13	Alors David se leva avec ses hommes au nombre d'environ 600 hommes. Ils sortirent de Qe'iylah et s'en allèrent où ils purent. On rapporta à Shaoul que David s'était sauvé de Qe'iylah, c'est pourquoi il cessa sa marche.
23:14	David resta dans le désert, dans des lieux forts, et il se tint sur la montagne au désert de Ziph. Et Shaoul le cherchait tous les jours, mais Elohîm ne le livra pas entre ses mains.
23:15	David vit que Shaoul était sorti pour chercher son âme. David se tenait dans le désert de Ziph, dans la forêt.
23:16	Alors Yehonathan, fils de Shaoul, se leva et s'en alla dans la forêt vers David. Il fortifia ses mains en Elohîm,
23:17	et lui dit : N'aie pas peur, car Shaoul mon père ne t'atteindra pas. Mais tu régneras sur Israël, et moi je serai le second après toi. Mon père Shaoul le sait bien aussi.
23:18	Ils firent tous les deux alliance devant YHWH. David resta dans la forêt, mais Yehonathan retourna dans sa maison.
23:19	Or les Ziphiens montèrent auprès de Shaoul à Guibea et lui dirent : David ne se tient-il pas caché parmi nous dans des lieux forts, dans la forêt, sur la colline de Hakila, qui est au sud du désert ?
23:20	Maintenant donc, ô roi, puisque tout le désir de ton âme est de descendre, descends, et ce sera à nous de le livrer entre les mains du roi.
23:21	Et Shaoul dit : Que YHWH vous bénisse de ce que vous avez eu pitié de moi !
23:22	Allez donc, je vous prie, assurez-vous encore davantage, pour savoir et trouver le lieu où il a dirigé ses pas, et qui l'a vu, car, m'a-t-on dit, il est fort rusé.
23:23	Examinez donc et reconnaissez tous les lieux où il se tient caché, puis retournez vers moi quand vous en serez assurés, et j'irai avec vous. S'il est dans le pays, je le chercherai soigneusement parmi tous les milliers de Yéhouda.
23:24	Ils se levèrent donc et s'en allèrent à Ziph avant Shaoul. David et ses hommes étaient dans le désert de Maon, dans la région aride, au sud du désert.
23:25	Shaoul et ses hommes partirent à la recherche de David. Et l'on en informa David, qui descendit le rocher et resta dans le désert de Maon. Shaoul, l'ayant appris, poursuivit David dans le désert de Maon.
23:26	Shaoul marchait d'un côté de la montagne, et David et ses hommes de l'autre côté de la montagne. David fuyait précipitamment pour échapper à Shaoul. Mais Shaoul et ses hommes entouraient David et ses hommes pour s'emparer d'eux,
23:27	lorsqu'un messager vint à Shaoul, en disant : Hâte-toi de venir, car les Philistins envahissent le pays !
23:28	Alors Shaoul cessa de poursuivre David et s'en retourna au-devant des Philistins. C'est pourquoi on appela ce lieu Séla-Hammachlekoth.

## Chapitre 24

### David épargne la vie de Shaoul (Saül) à En-Guédi

24:1	Puis David monta de là, et demeura dans les lieux forts d'En-Guédi.
24:2	Lorsque Shaoul fut revenu de la poursuite des Philistins, on lui fit ce rapport, disant : Voilà David dans le désert d'En-Guédi.
24:3	Alors Shaoul prit 3 000 hommes sélectionnés sur tout Israël et il s'en alla chercher David et ses hommes jusque sur le rocher des chèvres de montagne.
24:4	Shaoul arriva à des parcs de brebis qui étaient près du chemin, où il y avait une caverne dans laquelle il entra pour se couvrir les pieds. David et ses hommes se tenaient au fond de la caverne.
24:5	Et les hommes de David lui dirent : Voici le jour où YHWH te dit : Je te livre ton ennemi entre tes mains, afin que tu le traites comme cela paraîtra bon à tes yeux. David se leva et coupa tout doucement le pan de la robe de Shaoul.
24:6	Après cela, le cœur de David battit, parce qu'il avait coupé le pan du vêtement de Shaoul.
24:7	Et il dit à ses hommes : Que YHWH me garde de commettre une telle action contre mon seigneur, le mashiah de YHWH, en mettant ma main sur lui ! Car il est le mashiah de YHWH<!--David épargna Shaoul parce qu'il faisait confiance à YHWH. Il laissa Elohîm agir plutôt que d'agir par lui-même.-->.
24:8	Ainsi, David détourna ses hommes par ses paroles, et il ne leur permit pas de s'élever contre Shaoul. Puis Shaoul se leva de la caverne et poursuivit son chemin.
24:9	Après cela, David se leva, sortit de la caverne, et cria après Shaoul, en disant : Mon seigneur, le roi ! Shaoul regarda derrière lui, et David s'inclina le visage contre terre et se prosterna.
24:10	David dit à Shaoul : Pourquoi écouterais-tu les paroles des hommes qui te disent : Voici, David cherche ton malheur ?
24:11	Aujourd'hui, tes yeux ont vu que YHWH t'avait livré entre mes mains dans la caverne. On m'a dit de te tuer, mais je t'ai épargné et j'ai dit : Je ne porterai pas la main sur mon seigneur, car il est le mashiah de YHWH.
24:12	Regarde donc, mon père, regarde le pan de ton manteau dans ma main. Car j'ai coupé le pan de ton manteau et je ne t'ai pas tué. Sache et reconnais qu'il n'y a ni méchanceté ni transgression en ma main et que je n'ai pas péché contre toi. Cependant tu me dresses des embûches pour m'ôter la vie !
24:13	YHWH sera juge entre moi et toi, et YHWH me vengera de toi, mais ma main ne sera pas sur toi.
24:14	Comme dit le proverbe des anciens : C'est des méchants que vient la méchanceté ! C'est pourquoi je ne porterai pas la main contre toi.
24:15	Contre qui est sorti le roi d'Israël ? Qui poursuis-tu ? Un chien mort, une puce !
24:16	YHWH sera donc juge et jugera entre moi et toi. Il regardera et plaidera ma cause, il me rendra justice en me délivrant de ta main.
24:17	Or il arriva qu'aussitôt que David eut achevé d'adresser ces paroles à Shaoul, Shaoul dit : N'est-ce pas là ta voix, mon fils David ? Et Shaoul éleva la voix et pleura.
24:18	Et il dit à David : Tu es plus juste que moi, car tu m'as rendu le bien pour le mal que je t'ai fait,
24:19	et tu m'as fait connaître aujourd'hui comment tu as usé de bonté envers moi, car YHWH m'avait livré entre tes mains, et cependant tu ne m'as pas tué.
24:20	Si quelqu'un rencontre son ennemi, le laisse-t-il poursuivre tranquillement son chemin ? Que YHWH donc te récompense pour la grâce que tu m'as faite aujourd'hui !
24:21	Et maintenant voici, je sais que tu régneras, tu régneras et que le royaume d'Israël sera ferme entre tes mains.
24:22	C'est pourquoi maintenant, jure-moi par YHWH, que tu ne détruiras pas ma race après moi, et que tu n'extermineras pas mon nom de la maison de mon père.
24:23	Et David le jura à Shaoul. Puis Shaoul s'en alla dans sa maison, et David et ses hommes montèrent au lieu fort.

## Chapitre 25

### Israël pleure la mort de Shemouél (Samuel)

25:1	Shemouél mourut. Et tout Israël se rassembla pour le pleurer et on l'enterra dans sa maison à Ramah. David se leva et descendit dans le désert de Paran.

### Méchanceté de Nabal ; bon sens d'Abigaïl

25:2	Or il y avait à Maon un homme qui avait ses biens à Carmel, et cet homme-là était très puissant car il avait 3 000 brebis et 1 000 chèvres, et il se trouvait à Carmel quand on tondait ses brebis.
25:3	Le nom de l'homme était Nabal, et le nom de sa femme, Abigaïl. C'était une femme de bon sens<!--Ou « très-prudente ».--> et belle de visage, mais l'homme était cruel et méchant dans toutes ses actions. Il était de la race de Caleb.
25:4	Or David apprit dans le désert que Nabal tondait ses brebis.
25:5	Et il envoya dix jeunes hommes et leur dit : Montez à Carmel et rendez-vous auprès de Nabal. Vous le saluerez en mon nom,
25:6	et vous lui direz : Puisses-tu faire autant l'année prochaine à la même saison, et que la paix soit avec ta maison et tout ce qui est à toi.
25:7	Et maintenant, j'ai appris que tu as les tondeurs. Or tes bergers ont été avec nous, et nous ne leur avons fait aucune injure, et ils n'ont subi aucune perte pendant tout le temps qu'ils ont été à Carmel.
25:8	Demande-le à tes serviteurs et ils te le diront. Que ces jeunes hommes trouvent donc grâce à tes yeux, puisque nous venons dans un jour favorable. Nous te prions de donner à tes serviteurs et à David, ton fils, ce que tu trouveras sous ta main.
25:9	Les jeunes hommes de David arrivèrent et dirent à Nabal, au nom de David, toutes ces paroles. Puis ils se turent.
25:10	Nabal répondit aux serviteurs de David et dit : Qui est David, et qui est le fils d'Isaï ? Aujourd'hui le nombre des serviteurs qui s'échappent de leurs maîtres se multiplie.
25:11	Et prendrais-je mon pain et mon eau, et la viande que j'ai apprêtée pour mes tondeurs, afin de les donner à des hommes qui viennent je ne sais d'où ?
25:12	Ainsi les jeunes hommes de David rebroussèrent chemin. Ils s'en retournèrent et lui firent leur rapport selon toutes ces paroles.
25:13	Et David dit à ses hommes : Que chaque homme ceigne son épée ! Et ils ceignirent chaque homme son épée. David aussi ceignit son épée, et environ 400 hommes montèrent avec David. Il en resta 200 près des armes.
25:14	Or un des serviteurs de Nabal fit ce rapport à Abigaïl, femme de Nabal, et lui dit : Voici, David a envoyé du désert des messagers pour saluer notre maître, qui les a traités rudement.
25:15	Cependant ces hommes ont été très bons envers nous, et ne nous ont fait aucune injure, et rien ne nous a été enlevé, tout le temps que nous avons été avec eux lorsque nous étions dans les champs.
25:16	Ils nous ont servi de muraille nuit et jour, tout le temps que nous avons été avec eux, faisant paître les troupeaux.
25:17	Sache maintenant et vois ce que tu as à faire, car le mal est résolu contre notre maître et contre toute sa maison, parce qu'il est un fils de Bélial<!--Voir commentaire en De. 13:13.-->, et personne n'oserait lui parler.
25:18	Abigaïl se hâta donc, et prit 200 pains, 2 outres de vin, 5 pièces de menu bétail, 5 mesures de grain rôti, 100 grappes de raisins secs, 200 de figues sèches, et les mit sur des ânes.
25:19	Puis elle dit à ses jeunes hommes : Passez devant moi, je vais vous suivre. Elle n'en dit rien à Nabal, son mari.
25:20	Et étant montée sur un âne, elle descendait de la montagne par un chemin couvert ; voici, David et ses hommes descendaient en face d'elle, et elle les rencontra.
25:21	David avait dit : Certainement c'est en vain que j'ai gardé tout ce que cet homme a dans le désert, en sorte qu'il ne s'est rien perdu de tout ce qu'il possède. Il m'a rendu le mal pour le bien.
25:22	Qu'Elohîm traite ainsi les ennemis de David et qu'ainsi il y ajoute, si je laisse de tout ce qui lui appartient, jusqu'au matin, quelqu'un qui urine contre le mur !
25:23	Lorsque Abigaïl aperçut David, elle se hâta de descendre de son âne, et tomba sur sa face devant David, et se prosterna contre terre.
25:24	Elle se jeta donc à ses pieds et lui dit : À moi la faute, mon seigneur ! Permets à ta servante de parler devant toi, et écoute les paroles de ta servante.
25:25	Je t'en supplie, que mon seigneur ne prenne pas garde à cet homme de Bélial, à Nabal, car il est comme son nom : Nabal est son nom, et il y a de la folie en lui. Et moi, ta servante, je n'ai pas vu les jeunes hommes que mon seigneur a envoyés.
25:26	Maintenant donc, mon seigneur, aussi vrai que YHWH est vivant, et que ton âme vit, YHWH t'a empêché d'en venir au sang, et il a retenu ta main. Or que tes ennemis, et ceux qui cherchent à nuire à mon seigneur soient comme Nabal.
25:27	Mais maintenant, voici un présent que ta servante a apporté à mon seigneur, afin qu'on le donne aux jeunes hommes qui sont à la suite de mon seigneur.
25:28	Pardonne, je te prie, la transgression de ta servante, car YHWH fera, il fera à mon seigneur une maison stable, car mon seigneur conduit les batailles de YHWH, et il ne s'est trouvé en toi aucun mal pendant toute ta vie.
25:29	Si les humains se lèvent pour te persécuter et pour chercher ton âme, l'âme de mon seigneur sera liée au paquet des vivants auprès de YHWH ton Elohîm ; mais il lancera au loin, avec la fronde, l'âme de tes ennemis.
25:30	Il arrivera que YHWH fera à mon seigneur selon tout le bien qu'il t'a prédit et qu'il t'établira conducteur d'Israël,
25:31	ceci ne sera pas un obstacle, ni un sujet de regret dans l'âme de mon seigneur, pour avoir répandu le sang inutilement, et pour s'être vengé lui-même. Aussi, lorsque YHWH aura fait du bien à mon seigneur, tu te souviendras de ta servante.
25:32	Alors David dit à Abigaïl : Béni soit YHWH, l'Elohîm d'Israël, qui t'a aujourd'hui envoyée à ma rencontre !
25:33	Et béni soit ton bon sens, et bénie sois-tu, toi qui m'as aujourd'hui empêché d'en venir au sang et qui as retenu ma main !
25:34	Car certainement YHWH, l'Elohîm d'Israël, qui m'a empêché de te faire du mal, est vivant ! Si tu ne t'étais hâtée de venir à ma rencontre, il ne serait resté qui que ce soit à Nabal, d'ici à la lumière du matin.
25:35	David prit donc de sa main ce qu'elle lui avait apporté, et lui dit : Remonte en paix dans ta maison. Regarde, j'ai écouté ta voix et j'ai répondu favorablement à ta demande.

### Mort de Nabal ; Abigaïl et Achinoam deviennent les femmes de David

25:36	Puis Abigaïl revint auprès de Nabal. Et voici, il faisait un festin dans sa maison, comme un festin de roi. Nabal avait le cœur joyeux, et il était complètement ivre. C'est pourquoi elle ne lui dit aucune chose petite ou grande, jusqu'au matin.
25:37	Il arriva donc au matin, quand Nabal fut sorti de son vin, que sa femme lui raconta toutes ces choses. Alors son cœur mourut au dedans de lui, et il devint comme une pierre.
25:38	Or il arriva qu'environ dix jours après, YHWH frappa Nabal, et il mourut.
25:39	Et quand David eut appris que Nabal était mort, il dit : Béni soit YHWH, qui a combattu pour ma cause touchant l'outrage que j'avais reçu de la main de Nabal, et qui a préservé son serviteur de faire du mal, et a fait retomber le mal de Nabal sur sa tête ! Puis David envoya des gens pour parler à Abigaïl, afin de la prendre pour femme.
25:40	Les serviteurs de David vinrent auprès d'Abigaïl à Carmel et lui parlèrent en disant : David nous a envoyés vers toi, afin de te prendre pour femme.
25:41	Alors elle se leva, et se prosterna le visage contre terre, et dit : Voici, ta servante sera à ton service, afin de laver les pieds des serviteurs de mon seigneur.
25:42	Puis Abigaïl se leva promptement et monta sur un âne, et cinq de ses servantes la suivirent. Elle suivit les messagers de David et devint sa femme.
25:43	Or David avait pris aussi Achinoam de Yizre`e'l, et toutes les deux furent ses femmes.
25:44	Car Shaoul avait donné Miykal, sa fille, femme de David, à Palthi, fils de Laïsh, qui était de Gallim.

## Chapitre 26

### David épargne encore la vie de Shaoul (Saül)

26:1	Les Ziphiens allèrent encore auprès de Shaoul à Guibea, en disant : David ne se tient-il pas caché sur la colline de Hakila, en face du désert ?
26:2	Shaoul se leva et descendit dans le désert de Ziph, avec 3 000 hommes sélectionnés d'Israël, pour chercher David dans le désert de Ziph.
26:3	Shaoul campa sur la colline de Hakila, en face du désert, près du chemin. Or David se tenait dans le désert, et il aperçut que Shaoul marchait à sa poursuite dans le désert,
26:4	alors il envoya des espions et apprit avec certitude que Shaoul était arrivé.
26:5	Alors David se leva, et alla au lieu où Shaoul campait, et David vit la place où couchait Shaoul, avec Abner, fils de Ner, chef de son armée. Shaoul couchait au milieu du camp, et le peuple campait autour de lui.
26:6	David prit la parole et dit à Achimélec, le Héthien, et à Abishaï, fils de Tserouyah et frère de Yoab, il dit : Qui veut descendre avec moi dans le camp vers Shaoul ? Et Abishaï répondit : J'y descendrai avec toi.
26:7	David et Abishaï allèrent de nuit vers le peuple. Et voici, Shaoul dormait étant couché au milieu du camp, et sa lance était plantée en terre à son chevet. Et Abner et le peuple étaient couchés autour de lui.
26:8	Alors Abishaï dit à David : Aujourd'hui, Elohîm a livré ton ennemi entre tes mains. Laisse-moi donc le frapper avec la lance jusqu'en terre d'un seul coup, et je n'y retournerai pas une seconde fois.
26:9	Et David dit à Abishaï : Ne le tue pas ! Car qui porterait sa main sur le mashiah de YHWH et resterait impuni ?
26:10	David dit encore : YHWH est vivant ! C'est YHWH seul qui le frappera, soit que son jour vienne, soit qu'il descende au combat et qu'il y périsse.
26:11	Que YHWH me garde de mettre ma main sur le mashiah de YHWH ! Mais prends maintenant la lance qui est à son chevet et la cruche d'eau, et allons-nous-en.
26:12	David donc prit la lance et la cruche d'eau qui étaient au chevet de Shaoul, puis ils s'en allèrent. Personne ne les vit, ni ne s'aperçut de rien, ni ne se réveilla, car ils dormaient tous d'un profond sommeil dans lequel YHWH les avait plongés.
26:13	David passa de l'autre côté et s'arrêta au loin sur le sommet de la montagne, et il y avait une grande distance entre eux.
26:14	Et il cria au peuple et à Abner, fils de Ner, en disant : Ne répondras-tu pas, Abner ? Abner répondit et dit : Qui es-tu, toi, qui cries vers le roi ?
26:15	Alors David dit à Abner : N'es-tu pas un vaillant homme ? Qui est semblable à toi en Israël ? Pourquoi donc n'as-tu pas veillé sur le roi, ton seigneur ? Car quelqu'un du peuple est venu pour tuer le roi, ton seigneur.
26:16	Ce que tu as fait n'est pas bien. YHWH est vivant ! Vous êtes des fils de la mort, vous qui n'avez pas veillé sur votre seigneur, le mashiah de YHWH. Et maintenant, regarde où sont la lance du roi et la cruche d'eau qui étaient à son chevet.
26:17	Alors Shaoul reconnut la voix de David et dit : N'est-ce pas là ta voix, mon fils David ? Et David dit : C'est ma voix, ô roi, mon seigneur.
26:18	Il dit encore : Pourquoi mon seigneur poursuit-il son serviteur ? Car qu'ai-je fait, et quel mal y a t-il dans ma main ?
26:19	Maintenant donc je te prie, que le roi mon seigneur, écoute les paroles de son serviteur ! Si c'est YHWH qui te pousse contre moi, que ton offrande lui soit agréable. Mais si ce sont les fils des humains, qu'ils soient maudits devant YHWH, car aujourd'hui ils m'ont chassé, afin que je ne puisse me joindre à l'héritage de YHWH, et ils m'ont dit : Va, sers les elohîm étrangers !
26:20	Que mon sang ne tombe pas en terre loin de la face de YHWH ! Car le roi d'Israël est sorti pour chercher une puce, comme on poursuivrait une perdrix dans les montagnes.

### Shaoul (Saül) se repent devant David

26:21	Alors Shaoul dit : J'ai péché ! Reviens, mon fils David, car je ne te ferai plus de mal, parce qu'aujourd'hui ma vie a été précieuse à tes yeux. Voici, j'ai agi en insensé et je me suis lourdement trompé.
26:22	David répondit et dit : Voici la lance du roi. Que l'un de tes jeunes hommes vienne la prendre.
26:23	Que YHWH rende à chacun selon sa justice et selon sa fidélité, car il t'avait livré aujourd'hui entre mes mains, mais je n'ai pas voulu mettre ma main sur le mashiah de YHWH.
26:24	Voici, comme ta vie a été aujourd'hui de grand prix à mes yeux, ainsi ma vie sera de grand prix aux yeux de YHWH, et il me délivrera de toutes les angoisses.
26:25	Shaoul dit à David : Tu es béni, David, mon fils ! Oui, tu agiras, tu agiras ! Oui, tu vaincras, tu vaincras ! Alors David continua son chemin, et Shaoul s'en retourna chez lui.

## Chapitre 27

### David se réfugie dans le pays des Philistins

27:1	Mais David dit en son cœur : Maintenant, je périrai un jour par les mains de Shaoul. Ne vaut-il pas mieux que je me sauve, que je me sauve au pays des Philistins, afin que Shaoul renonce à me chercher encore dans tout le territoire d'Israël ? Ainsi j'échapperai à sa main.
27:2	David se leva, lui et les 600 hommes qui étaient avec lui, et il passa chez Akish, fils de Maoc, roi de Gath.
27:3	David et ses hommes restèrent à Gath auprès d'Akish. Chaque homme avec sa famille et David avait ses deux femmes, Achinoam de Yizre`e'l et Abigaïl de Carmel, la femme de Nabal.
27:4	Alors on informa Shaoul que David s'était enfui à Gath et il cessa de le chercher.
27:5	David dit à Akish : Si j'ai trouvé grâce à tes yeux, qu'on me donne dans l'une des villes du pays, un lieu où je puisse habiter, car pourquoi ton serviteur habiterait-il dans la ville royale avec toi ?
27:6	Akish lui donna ce même jour, Tsiklag. C'est pourquoi Tsiklag appartient aux rois de Yéhouda jusqu'à ce jour.
27:7	Le nombre des jours que David passa dans le pays des Philistins fut d'un an et quatre mois.
27:8	David montait avec ses hommes faire des incursions chez les Guéshouriens, les Guirziens et les Amalécites, car ces nations-là habitaient dans le territoire dès les temps anciens, depuis Shour jusqu'au pays d'Égypte.
27:9	David ravageait ce territoire, il ne laissait en vie ni homme ni femme, et il prenait les brebis, les bœufs, les ânes, les chameaux et les vêtements, puis il s'en retournait et allait chez Akish.
27:10	Akish disait : Où avez-vous fait vos incursions aujourd'hui ? Et David répondait : Vers le sud de Yéhouda, vers le sud des Yerachmeélites et vers le sud des Kéniens.
27:11	Mais David ne laissait en vie ni homme ni femme pour les amener à Gath, de peur, se disait-il, qu'ils ne fassent des rapports contre nous en disant : Voilà ce que David a fait. Telle fut sa manière d'agir tout le temps qu'il demeura dans le pays des Philistins.
27:12	Akish croyait David, et il disait : Il se rend odieux à Israël, son peuple, c'est pourquoi il sera mon serviteur à jamais.

## Chapitre 28

### Les Philistins vont en guerre contre Shaoul (Saül)

28:1	Et il arriva, en ces jours-là, que les Philistins rassemblèrent leurs armées pour faire la guerre, pour combattre Israël. Akish dit à David : Tu le sais, tu le sais, tu viendras avec moi au camp, toi et tes hommes.
28:2	David répondit à Akish : Alors tu verras ce que ton serviteur fera ! Et Akish dit à David : C'est pour cela que je te confierai toujours la garde de ma personne.
28:3	Or Shemouél était mort, et tout Israël avait fait le deuil, et on l'avait enseveli à Ramah qui était sa ville. Shaoul avait ôté du pays ceux qui évoquaient les morts et ceux qui ont un esprit de divination.
28:4	Les Philistins se rassemblèrent et vinrent camper à Shouném. Shaoul aussi rassembla tout Israël et ils campèrent à Guilboa.
28:5	À la vue du camp des Philistins, Shaoul eut peur, et son cœur fut saisi de crainte.
28:6	Shaoul consulta YHWH, mais YHWH ne lui répondit rien, ni par des rêves, ni par l'ourim, ni par les prophètes.

### Shaoul (Saül) chez la femme qui évoque les morts

28:7	Shaoul dit à ses serviteurs : Cherchez-moi une femme maîtresse qui évoque les morts, j'irai vers elle et je la consulterai. Ses serviteurs lui dirent : Voici, il y a une femme maîtresse à En-Dor qui évoque les morts.
28:8	Alors Shaoul se déguisa, prit d'autres vêtements et il partit avec deux hommes. Ils arrivèrent de nuit chez cette femme, et Shaoul lui dit : Je te prie, évoque les morts et fais monter vers moi celui que je te dirai.
28:9	Mais la femme lui répondit : Voici, tu sais ce que Shaoul a fait, et comment il a exterminé du pays ceux qui évoquent les morts et ceux qui ont un esprit de divination. Pourquoi donc tends-tu un piège à mon âme pour me faire mourir ?
28:10	Shaoul lui jura par YHWH et lui dit : YHWH est vivant ! Il ne t'arrivera aucun mal pour cela.
28:11	Alors la femme dit : Qui veux-tu que je te fasse monter ? Et il répondit : Fais-moi monter Shemouél.
28:12	Et la femme voyant Shemouél s'écria à grande voix, en disant à Shaoul : Pourquoi m'as-tu trompée ? Car tu es Shaoul !
28:13	Et le roi lui répondit : N'aie pas peur ! Mais que vois-tu ? La femme dit à Shaoul : Je vois un elohîm qui monte de la Terre.
28:14	Il lui dit encore : Comment est-il fait ? Elle répondit : C'est un vieil homme qui monte et il est couvert d'une robe. Et Shaoul comprit que c'était Shemouél, il s'inclina le visage contre terre et se prosterna.
28:15	Shemouél dit à Shaoul : Pourquoi m'as-tu troublé en me faisant monter ? Et Shaoul répondit : Je suis dans une grande angoisse, car les Philistins me font la guerre, et Elohîm s'est retiré de moi, et ne m'a plus répondu ni par les prophètes ni par des rêves. C'est pourquoi je t'ai appelé<!--Elohîm interdit formellement ces pratiques (De. 18:9-14 ; Es. 8:19.).-->, afin que tu me fasses entendre ce que j'aurai à faire.
28:16	Shemouél dit : Pourquoi donc me consultes-tu, puisque YHWH s'est retiré de toi, et qu'il est devenu ton ennemi ?
28:17	YHWH te traite comme je te l'avais annoncé de sa part : YHWH a déchiré le royaume d'entre tes mains et l'a donné aux mains de David, ton compagnon.
28:18	Parce que tu n'as pas écouté la voix de YHWH, et que tu n'as pas exécuté l'ardeur de sa colère contre Amalek, à cause de cela, YHWH te traite de cette manière aujourd'hui.
28:19	Et même YHWH livrera Israël avec toi entre les mains des Philistins, et vous serez demain avec moi, toi et tes fils. YHWH livrera aussi le camp d'Israël entre les mains des Philistins.
28:20	Aussitôt Shaoul tomba à terre de toute sa hauteur, car il avait très peur à cause des paroles de Shemouél, et même les forces lui manquèrent parce qu'il n'avait rien mangé ce jour, ni toute cette nuit.
28:21	Alors la femme vint auprès de Shaoul, et voyant qu'il était extrêmement terrifié, elle lui dit : Voici, ta servante a écouté ta voix, j'ai donc mis mon âme dans ma main et j'ai obéi aux paroles que tu m'as dites.
28:22	Maintenant, je te prie, écoute, toi aussi, ce que ta servante te dira : Laisse-moi mettre devant toi un morceau de pain, afin que tu manges pour avoir la force de te remettre en route.
28:23	Et il le refusa et dit : Je ne mangerai pas. Mais ses serviteurs et la femme aussi le pressèrent tellement qu'il écouta leur voix. Il se leva de terre et s'assit sur un lit.
28:24	Or cette femme avait dans sa maison un veau qu'elle engraissait et elle se hâta de le tuer. Puis elle prit de la farine, la pétrit et fit cuire des pains sans levain.
28:25	Elle les mit devant Shaoul et devant ses serviteurs. Et ils mangèrent. Puis s'étant levés, ils s'en allèrent cette nuit-là.

## Chapitre 29

### Les Philistins refusent que David combatte Israël

29:1	Or les Philistins rassemblèrent toutes leurs armées à Aphek, et Israël campa près de la fontaine de Yizre`e'l.
29:2	Les seigneurs des Philistins s'avancèrent avec leurs centaines et leurs milliers, et David et ses hommes marchèrent à l'arrière-garde avec Akish.
29:3	Les princes des Philistins dirent : Que font ici ces Hébreux ? Et Akish répondit aux princes des Philistins : N'est-ce pas ce David, serviteur de Shaoul, roi d'Israël ? Il est avec moi depuis des jours et des années, et je n'ai pas trouvé quelque chose à lui reprocher depuis son arrivée, jusqu'à ce jour.
29:4	Mais les princes des Philistins se mirent en colère contre lui, et lui dirent : Renvoie cet homme, et qu'il retourne dans le lieu où tu l'as établi, et qu'il ne descende pas avec nous dans la bataille, de peur qu'il ne soit pour nous un adversaire pendant la bataille. Car comment pourrait-il se remettre en grâce auprès de son maître ? Ne serait-ce pas par le moyen des têtes de nos hommes ?
29:5	N'est-ce pas ce David pour qui l'on chantait et répondait en dansant : Shaoul a frappé ses 1 000, et David ses 10 000 ?
29:6	Akish appela David et lui dit : YHWH est vivant ! Tu es certainement un homme droit, cela a été bon à mes yeux que tu sortes et rentres avec moi dans le camp, car je n'ai pas trouvé de mal en toi depuis le jour où tu es arrivé auprès de moi jusqu'à ce jour. Mais tu n'es pas agréable aux yeux des seigneurs.
29:7	Maintenant donc, retourne et va-t'en en paix, afin que tu ne fasses aucune chose qui déplaise aux yeux des seigneurs des Philistins.
29:8	Et David dit à Akish : Mais qu'ai-je fait ? Et qu'as-tu trouvé en ton serviteur depuis que je suis avec toi jusqu'à ce jour, pour que je n'aille pas combattre contre les ennemis du roi, mon seigneur ?
29:9	Akish répondit et dit à David : Je le sais, car tu es agréable à mes yeux comme un ange d'Elohîm. Mais c'est seulement les chefs des Philistins qui disent : Il ne montera pas avec nous dans la bataille.
29:10	C'est pourquoi lève-toi de bon matin, avec les serviteurs de ton maître qui sont venus avec toi. Levez-vous de bon matin et partez dès que vous verrez le jour, allez-vous-en !
29:11	Ainsi David et ses hommes se levèrent de bonne heure pour partir dès le matin et retourner dans le pays des Philistins. Et les Philistins montèrent à Yizre`e'l.

## Chapitre 30

### David libère Tsiklag

30:1	Et il arriva, tandis que David et ses hommes venaient vers Tsiklag, que les Amalécites firent une incursion dans le midi et à Tsiklag. Ils frappèrent et brûlèrent par le feu Tsiklag,
30:2	après avoir fait prisonniers les femmes et tous ceux qui étaient là, petits et grands. Ils n'avaient tué personne, mais ils les avaient emmenés, et s'étaient remis en chemin.
30:3	David et ses hommes revinrent dans la ville et voici, elle était brûlée, et leurs femmes, leurs fils, et leurs filles avaient été faits prisonniers.
30:4	C'est pourquoi David et le peuple qui était avec lui élevèrent leur voix et pleurèrent tellement qu'il n'y avait plus en eux de force pour pleurer.
30:5	Les deux femmes de David avaient été emmenées, Achinoam de Yizre`e'l, et Abigaïl de Carmel, qui avait été femme de Nabal.
30:6	David fut dans une grande angoisse, parce que le peuple parlait de le lapider, car tout le peuple avait de l'amertume dans l'âme à cause de leurs fils et de leurs filles. Toutefois, David se fortifia en YHWH, son Elohîm.
30:7	Et il dit à Abiathar, le prêtre, fils d'Achimélec : Apporte-moi, je te prie, l'éphod ! Abiathar apporta l'éphod à David.
30:8	Et David consulta YHWH, en disant : Poursuivrai-je cette troupe ? L'atteindrai-je ? Et il lui répondit : Poursuis-la, car tu l'atteindras, tu l'atteindras, et tu les délivreras, tu les délivreras.
30:9	David s'en alla avec les 600 hommes qui étaient avec lui, et ils arrivèrent au torrent de Besor, où s'arrêtèrent ceux qui restaient en arrière.
30:10	Ainsi David et 400 hommes continuèrent la poursuite, mais 200 hommes s'arrêtèrent, trop fatigués pour pouvoir passer le torrent de Besor.
30:11	Ayant trouvé un homme égyptien dans les champs, ils l'amenèrent à David, et lui donnèrent du pain, il mangea, puis ils lui donnèrent de l'eau à boire.
30:12	Ils lui donnèrent aussi quelques figues sèches et deux grappes de raisins secs. Et il mangea, et le cœur lui revint, car cela faisait trois jours et trois nuits qu'il n'avait pas mangé de pain ni bu d'eau.
30:13	Et David lui dit : À qui es-tu ? Et d'où es-tu ? Et il répondit : Je suis un garçon égyptien, serviteur d'un homme amalécite, et mon maître m'a abandonné, parce que j'étais malade il y a trois jours.
30:14	Nous avons envahi le sud des Kéréthiens, et sur ce qui est à Yéhouda et sur le sud de Caleb, et nous avons brûlé Tsiklag par le feu.
30:15	David lui dit : Me conduiras-tu vers cette troupe ? Et il répondit : Jure-moi par le Nom d'Elohîm que tu ne me feras pas mourir, et que tu ne me livreras pas entre les mains de mon maître, et je te conduirai vers cette troupe.
30:16	Et il le conduisit. Et voici, ils étaient dispersés sur toute la contrée, mangeant, buvant et dansant, à cause de ce grand butin qu'ils avaient pris au pays des Philistins et au pays de Yéhouda.
30:17	Et David les frappa depuis l'aube du jour, jusqu'au soir du lendemain, et il n'en échappa aucun d'eux, hormis 400 jeunes hommes qui montèrent sur des chameaux et s'enfuirent.
30:18	David recouvra tout ce que les Amalécites avaient emporté, il délivra aussi ses deux femmes.
30:19	Il ne leur manqua personne, depuis le plus petit jusqu'au plus grand, ni fils ni filles, ni butin, ni rien de ce qu'ils leur avaient emporté : David ramena tout.
30:20	David prit aussi tout le gros et menu bétail. Ceux qui marchaient devant ce troupeau pour le guider disaient : C'est le butin de David !

### David partage le butin

30:21	Puis David arriva auprès des 200 hommes qui avaient été tellement fatigués qu'ils n'avaient pu suivre David, et qu'on avait laissés au torrent de Besor. Ils sortirent au-devant de David et au-devant du peuple qui était avec lui. David s'étant approché du peuple, il les salua aimablement.
30:22	Mais tous les hommes de Bélial et méchants qui avaient accompagné David prirent la parole et dirent : Puisqu'ils ne sont pas venus avec nous, nous ne leur donnerons rien du butin que nous avons récupéré, sinon à chaque homme sa femme et ses enfants. Qu'ils les emmènent et s'en aillent !
30:23	Mais David dit : Mes frères, n'agissez pas ainsi au sujet de ce que YHWH nous a donné. Il nous a gardés et a livré entre nos mains la troupe qui était venue contre nous.
30:24	Qui vous écouterait dans cette affaire ? Car celui qui est resté près des armes doit avoir autant de part que celui qui est descendu sur le champ de bataille. Ils partageront ensemble.
30:25	Et il arriva, depuis ce jour-là, qu'on en a fait une ordonnance et une coutume en Israël, jusqu'à ce jour.
30:26	David revint à Tsiklag et envoya une partie du butin aux anciens de Yéhouda, à ses amis, en disant : Voici, un présent pour vous, du butin des ennemis de YHWH !
30:27	Il en envoya à ceux de Béth-El, à ceux qui étaient à Ramoth du sud, à ceux de Yattiyr,
30:28	à ceux d'Aroër, à ceux de Siphmoth, à ceux d'Eshthemoa,
30:29	à ceux de Racal, à ceux des villes des Yerachmeélites, à ceux des villes des Kéniens,
30:30	à ceux d'Hormah, à ceux de Cor-Ashan, à ceux d'Athac,
30:31	à ceux d'Hébron, et dans tous les lieux où David avait demeuré, lui et ses hommes.

## Chapitre 31

### Les Philistins battent Israël ; Shaoul (Saül) et ses fils meurent<!--1 Ch. 10:1-14.-->

31:1	Or les Philistins livrèrent bataille à Israël, et les hommes d'Israël s'enfuirent devant les Philistins, et furent tués sur la Montagne de Guilboa.
31:2	Les Philistins atteignirent Shaoul et ses fils, et tuèrent Yehonathan, Abinadab et Malkishoua, fils de Shaoul.
31:3	Le poids de la guerre se porta sur Shaoul. Les hommes qui tirent l'arc le trouvèrent et il se mit à trembler devant les tireurs.
31:4	Alors Shaoul dit à celui qui portait ses armes : Tire ton épée et transperce-moi, de peur que ces incirconcis ne viennent, ne me transpercent et ne m'outragent. Mais celui qui portait ses armes refusa, parce qu'il était saisi de crainte. Shaoul prit l'épée et se jeta dessus.
31:5	Alors celui qui portait les armes de Shaoul, voyant que Shaoul était mort, se jeta aussi sur son épée et mourut avec lui.
31:6	Ainsi périrent en ce jour, Shaoul et ses trois fils, celui qui portait ses armes, et tous ses gens.
31:7	Ceux d'Israël qui étaient de ce côté de la vallée et de ce côté du Yarden, ayant vu que les Israélites s'étaient enfuis, que Shaoul et ses fils étaient morts, abandonnèrent les villes et s'enfuirent, de sorte que les Philistins y entrèrent et s'y établirent.
31:8	Or il arriva que, dès le lendemain, les Philistins vinrent pour dépouiller les morts, et ils trouvèrent Shaoul et ses trois fils, étendus sur la Montagne de Guilboa.
31:9	Ils coupèrent la tête de Shaoul et le dépouillèrent de ses armes, qu'ils envoyèrent au pays des Philistins, dans tous les environs, pour en faire savoir les nouvelles dans les maisons de leurs idoles et parmi le peuple.
31:10	Ils déposèrent les armes de Shaoul dans le temple d'Astarté, et ils attachèrent son cadavre sur les murs de Beth-Shan.
31:11	Lorsque les habitants de Yabesh en Galaad apprirent ce que les Philistins avaient fait à Shaoul,
31:12	tous les hommes talentueux se levèrent et marchèrent toute la nuit, et ils enlevèrent des murs de Beth-Shan le cadavre de Shaoul et les cadavres de ses fils. Ils revinrent à Yabesh, où ils les brûlèrent.
31:13	Puis ils prirent leurs os, les ensevelirent sous un tamaris près de Yabesh, et ils jeûnèrent 7 jours.
