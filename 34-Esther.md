# Meguila Esther (Esther) (Est.)

Signification : Rouleau d'Esther

Auteur : Inconnu

Thème : Délivrance des Juifs de l'extermination

Date de rédaction : 5ème siècle av. J.-C.

Esther vient du persan « Ecter » et signifie « étoile ». Son nom hébreu était Hadassah (Est. 2:7) qui signifie « myrte ».

Dernier livre à caractère historique du Tanakh, l'histoire d'Esther se déroula à Suse, capitale du royaume de Perse. En ce temps, le peuple d'Israël était dispersé et le roi Assuérus régnait sur un large territoire allant de l'Inde à l'Éthiopie.

Ce livre raconte la vie d'Esther, son ascension au trône royal où elle succéda à la reine Vasthi et la manière dont elle fut utilisée pour éviter le génocide du peuple juif.

Bien que ne comportant pas le Nom d'Elohîm ni d'allusion à une œuvre spirituelle, hormis le jeûne, ce récit met en évidence le secours divin.

## Chapitre 1

### Un festin de 7 jours au palais de Suse

1:1	Il arriva au temps d'Assuérus, de cet Assuérus qui régnait depuis l'Inde jusqu'en Éthiopie sur 127 provinces ;
1:2	il arriva en ce temps-là, que le roi Assuérus était assis sur le trône royal à Suse, dans la capitale.
1:3	La troisième année de son règne, il fit un festin à tous les principaux princes de ses pays : à ses serviteurs, à l'armée des Perses et des Mèdes, aux nobles et aux chefs des provinces qui furent réunis devant lui.
1:4	Il montra la glorieuse richesse de son royaume et la splendeur de sa grande magnificence pendant de nombreux jours, pendant 180 jours.
1:5	Quand ces jours furent achevés, le roi fit un festin pendant 7 jours, dans la cour du jardin du palais royal, pour tout le peuple qui se trouvait à Suse la capitale, du plus grand au plus petit.
1:6	Des tissus blancs, du coton fin et des étoffes violettes, étaient attachés par des cordons de byssus et de pourpre rouge à des anneaux d'argent et à des colonnes de marbre. Les lits étaient d'or et d'argent sur un pavé de porphyre, de marbre blanc, de perle et de marbre noir.
1:7	Et on donnait à boire dans des vases d'or, des vases, des vases divers, et il y avait du vin royal dans une abondance qui reflétait la force du roi.
1:8	Et conformément au décret, on ne forçait personne à boire, car le roi avait ordonné à tous les grands de sa maison d'agir selon la volonté de chacun.
1:9	La reine Vasthi fit aussi un festin aux femmes dans la maison royale du roi Assuérus.

### Destitution de la reine Vasthi

1:10	Or le septième jour, comme le cœur du roi était réjoui par le vin, il ordonna à Mehoumân, Biztha, Harbona, Bigtha, Abagtha, Zéthar et Carcas, les sept eunuques qui servaient devant le roi Assuérus,
1:11	d'amener en sa présence la reine Vasthi, portant la couronne royale, afin de montrer sa beauté aux peuples et aux princes, car elle était belle de figure.
1:12	Mais la reine Vasthi refusa de venir selon la parole du roi que les eunuques lui avaient transmise. Le roi en fut très fâché et sa colère s'enflamma au-dedans de lui.
1:13	Alors le roi parla aux sages qui avaient la connaissance des temps : (car c'est ainsi que le roi traitait les affaires en présence de tous ceux qui connaissaient les lois et le droit.
1:14	Et les plus proches de lui étaient Carshena, Shéthar, Admatha, Tarsis, Mérès, Marsena, Memoukân, sept princes de Perse et de Médie, qui voyaient la face du roi et qui occupaient le premier rang dans le royaume.)
1:15	Qu'y a-t-il à faire selon les lois à la reine Vasthi, pour n'avoir pas exécuté l'ordre que le roi Assuérus lui avait transmis par les eunuques ?
1:16	Memoukân dit en présence du roi et des princes : La reine Vasthi n'a pas seulement mal agi contre le roi, mais aussi contre tous les princes et tous les peuples qui sont dans toutes les provinces du roi Assuérus.
1:17	Car cette affaire de la reine sortira vers toutes les femmes, aux yeux desquelles leurs maris seront méprisés. Elles diront : Le roi Assuérus avait ordonné qu'on lui amène la reine et elle n'est pas venue.
1:18	Et dès ce jour, les princesses de Perse et de Médie qui ont entendu parler de l'action de la reine, en parleront à tous les princes des pays du roi et il y aura assez de mépris et de colère !
1:19	Si le roi le trouve bon, qu'on publie de sa part et qu'on écrive parmi les lois des Perses et des Mèdes, avec défense de la transgresser, une parole royale d'après laquelle Vasthi n'entrera plus devant le roi Assuérus et le roi donnera sa royauté à l'une de ses compagnes qui soit meilleure qu'elle.
1:20	L'édit du roi sera présenté et connu dans tout son royaume, car il est grand, et toutes les femmes rendront honneur à leurs maris<!--Respect ou soumission de la femme à l'égard de son mari : Ep. 5:22 ; Col. 3:18 ; 1 Pi. 3:1-5.-->, depuis le plus grand jusqu'au plus petit.
1:21	Cette parole fut bonne aux yeux du roi et des princes, et le roi agit selon la parole de Memoukân.
1:22	Il envoya des lettres à toutes les provinces du roi, à chaque province selon sa manière d'écrire, et à chaque peuple selon sa langue, afin que chaque homme soit maître dans sa propre maison<!--L'homme est le chef de la femme et le maître de la maison : 1 Co. 11:3 ; Ep. 5:23.-->, et que cela soit publié selon la langue de chaque peuple.

## Chapitre 2

### Le roi choisit une autre reine

2:1	Après ces choses, quand la colère du roi Assuérus fut calmée, il se souvint de Vasthi, de ce qu'elle avait fait, et de ce qui avait été décrété contre elle.
2:2	Et les jeunes hommes qui servaient le roi dirent : Qu'on cherche pour le roi des jeunes filles, vierges et belles de figure.
2:3	Que le roi établisse des commissaires dans toutes les provinces de son royaume chargés de rassembler toutes les jeunes filles vierges et belles de figure, dans Suse, la capitale, dans la maison des femmes sous la charge d'Hégaï, eunuque du roi et gardien des femmes, qu'on leur donne les parfums nécessaires pour leur purification,
2:4	et la jeune fille qui plaira aux yeux du roi régnera à la place de Vasthi. Ce discours plut aux yeux du roi et il fit ainsi.
2:5	Or il y avait à Suse, la capitale, un homme juif nommé Mardochée, fils de Yaïr, fils de Shimeï, fils de Kis, Benyamite,
2:6	qui avait été exilé de Yeroushalaim parmi les déportés emmenés en exil avec Yekonyah, le roi de Yéhouda, que Neboukadnetsar, le roi de Babel<!--Babylone.--> avait emmené en exil.
2:7	Il élevait Hadassah qui est Esther, fille de son oncle, car elle n'avait ni père ni mère. La jeune fille était belle de taille et très belle de figure. Après la mort de son père et de sa mère, Mardochée l'avait prise pour sa fille.
2:8	Et quand la parole du roi et son édit furent connus et que beaucoup de jeunes filles furent rassemblées à Suse, la capitale, sous la charge d'Hégaï, Esther fut aussi amenée dans la maison du roi, sous la charge d'Hégaï, gardien des femmes.
2:9	Et la jeune fille plut à ses yeux et porta de la bonté devant lui. Il s'empressa de lui donner les parfums nécessaires pour sa purification et ses portions. Il lui donna aussi les sept filles les plus remarquables de la maison du roi, puis il la plaça avec ses jeunes filles dans l'un des plus beaux appartements de la maison des femmes.
2:10	Or Esther n'avait fait connaître ni son peuple ni sa parenté, car Mardochée lui avait ordonné de ne rien raconter.
2:11	Tous les jours, Mardochée allait et venait devant la cour de la maison des femmes, pour savoir comment se portait Esther et comment on s'occupait d'elle.
2:12	Or quand le tour de chaque jeune fille était venu pour entrer chez le roi Assuérus, après s'être conformée au décret concernant les femmes pendant 12 mois<!--Esther se soumit à une toilette particulière avant de rencontrer le roi. Le mot « toilette » vient de l'hébreu « tam-rook », qui signifie « grattement ». La racine de ce mot signifie « nettoyer », « purifier », « polir » (voir Lé. 6:21 ; Jé. 46:4). Ce grattage symbolise le dépouillement du vieil homme et le renoncement aux œuvres de la chair (Ep. 4:22). Douze mois étaient nécessaires pour préparer Esther aux noces : 6 mois avec de l'huile de myrrhe et 6 mois avec des aromates et des parfums. La myrrhe était l'une des composantes de l'huile pour l'onction sainte dont on s'est servie pour oindre notamment la tente d'assignation, l'arche du témoignage ainsi qu'Aaron et ses fils (Ex. 30:23-30). La toilette d'Esther symbolise la sanctification nécessaire à notre préparation pour les noces (Hé. 12:14). La myrrhe est par ailleurs citée à sept reprises dans le livre du Cantique des cantiques, véritable hymne de l'amour parfait qui lie le Mashiah (Christ) à son Assemblée (Église). Le parfum quant à lui symbolise les prières que nous devons faire en tout temps afin de maintenir notre communion avec Yéhoshoua (Jésus), notre Époux (Ep. 6:18 ; 1 Th. 5:17 ; Ap. 5:8, 8:4). Ainsi, à l'instar d'Esther qui se préparait à rencontrer le roi, l'Assemblée se prépare depuis plus de 2 000 ans pour les noces de l'Agneau (Ap. 19:7-9).-->, car c'est ainsi que s'accomplissaient les jours de leurs préparatifs, 6 mois avec de l'huile de myrrhe, et 6 mois avec des aromates et des parfums nécessaires à la purification des femmes.
2:13	Lorsque la jeune fille allait chez le roi, tout ce qu'elle demandait lui était donné pour aller avec elle de la maison des femmes à la maison du roi.
2:14	Elle y allait le soir, et le matin, elle retournait dans la seconde maison des femmes sous la charge de Shaashgaz, eunuque du roi et gardien des concubines. Elle ne retournait plus vers le roi, à moins que le roi n'en ait le désir et qu'elle ne soit appelée par son nom.

### Esther, reine de Suse

2:15	Quand son tour d'aller vers le roi fut arrivé, Esther, fille d'Abichaïl, oncle de Mardochée qui l'avait prise pour sa fille, ne demanda rien sinon ce qui fut ordonné par Hégaï, eunuque du roi et gardien des femmes. Esther portait grâce aux yeux de tous ceux qui la voyaient.
2:16	Ainsi Esther fut amenée auprès du roi Assuérus, dans sa maison royale, le dixième mois, qui est le mois de Tébeth, la septième année de son règne.
2:17	Et le roi aima Esther plus que toutes les autres femmes, et elle porta grâce et bonté plus que toutes les vierges. Il mit la couronne royale sur sa tête, et l'établit reine à la place de Vasthi.
2:18	Alors le roi fit un grand festin à tous les princes de ses pays et à ses serviteurs, un festin en l'honneur d'Esther. Il donna du repos aux provinces et fit des présents selon l'opulence royale.
2:19	Or pendant qu'on rassemblait les vierges pour la seconde fois, Mardochée s'assit à la porte du roi.
2:20	Esther n'avait fait connaître ni sa parenté ni son peuple, selon que Mardochée le lui avait ordonné, car elle exécutait ce que lui disait Mardochée, comme quand elle était élevée chez lui.

### Le roi sauvé par Mardochée

2:21	En ces jours-là, Mardochée s'étant assis à la porte du roi, Bigthan et Théresh, deux eunuques du roi, gardes du seuil, se fâchèrent et cherchèrent à mettre la main sur le roi Assuérus.
2:22	L'affaire fut connue de Mardochée qui informa la reine Esther, et Esther le dit au roi au nom de Mardochée.
2:23	Après une recherche, l'affaire se trouva exacte. Les deux eunuques furent pendus à un bois et cela fut écrit dans le livre des discours du jour devant le roi<!--Voir Mal. 3:16.-->.

## Chapitre 3

### Conspiration d'Haman contre les Juifs

3:1	Après ces choses, le roi Assuérus fit de grands honneurs à Haman, fils d'Hammedatha, l'Agaguite. Il l'éleva et plaça son trône au-dessus de tous les princes qui étaient auprès de lui.
3:2	Tous les serviteurs du roi qui étaient à la porte du roi s'inclinaient et se prosternaient devant Haman, car le roi l'avait ainsi ordonné. Mais Mardochée ne s'inclinait pas et ne se prosternait pas devant lui.
3:3	Les serviteurs du roi, qui étaient à la porte du roi disaient à Mardochée : Pourquoi transgresses-tu l'ordre du roi ?
3:4	Et il arriva que, comme ils lui parlaient jour après jour et qu'il ne les écoutait pas, ils informèrent Haman, pour voir si les paroles de Mardochée seraient fermes, parce qu'il leur avait déclaré qu'il était Juif.
3:5	Haman vit que Mardochée ne s'agenouillait pas et ne se prosternait pas devant lui et il fut rempli de colère.
3:6	Mais c'était chose méprisable à ses yeux de porter la main sur Mardochée seul, car on lui avait rapporté de quel peuple était Mardochée. Haman chercha à exterminer tous les Juifs, le peuple de Mardochée qui se trouvait dans tout le royaume d'Assuérus.
3:7	Au premier mois, qui est le mois de Nisan, la douzième année du roi Assuérus, on jeta le Pour<!--Pour ou Pourim : Ce terme signifie « sort ». Voir commentaire de Est. 9:26.-->, c'est-à-dire le sort, devant Haman, pour chaque jour et pour chaque mois jusqu'au douzième mois, qui est le mois d'Adar.
3:8	Haman dit au roi Assuérus : Il y a un peuple, dispersé et séparé entre les peuples dans toutes les provinces de ton royaume. Leurs lois sont différentes de celles de tous les autres peuples. Il n'observe pas les lois du roi. Le roi n’a pas intérêt à le laisser en repos.
3:9	S'il plaît au roi, qu'on écrive pour les détruire et je pèserai 10 000 talents d'argent entre les mains de ceux qui s'occupent des affaires, pour les porter dans le trésor du roi.
3:10	Alors le roi ôta son anneau de sa main et le donna à Haman fils de Hammedatha, l'Agaguite, l'adversaire des Juifs.
3:11	Outre cela, le roi dit à Haman : Cet argent t'est donné avec ce peuple. Fais-en ce que tu voudras.
3:12	Le treizième jour du premier mois, les secrétaires du roi furent appelés, et on écrivit selon l'ordre d'Haman, aux satrapes du roi, aux gouverneurs de chaque province et aux princes de chaque peuple, à chaque province selon son écriture et à chaque peuple selon sa langue. Ce fut au nom du roi Assuérus que l'on écrivit et on scella avec l'anneau du roi.
3:13	Les lettres furent envoyées par des coureurs dans toutes les provinces du roi, afin qu'on extermine, qu'on tue et qu'on fasse périr tous les Juifs, jeunes et vieux, petits enfants et femmes, en un seul jour, le treizième du douzième mois, qui est le mois d'Adar, et pour que leurs biens soient livrés au pillage.
3:14	Ces lettres qui furent écrites portaient une copie de l'édit qui devait être publié dans chaque province, et invitaient publiquement tous les peuples à se tenir prêts pour ce jour-là.
3:15	Ainsi les coureurs partirent en toute hâte d'après l'ordre du roi. L'édit fut aussi publié dans Suse, la capitale. Or le roi et Haman étaient assis pour boire, pendant que la ville de Suse était dans la confusion.

## Chapitre 4

### Esther avertie du complot d'Haman

4:1	Or quand Mardochée eut appris tout ce qui avait été fait, il déchira ses vêtements et se couvrit d'un sac et de cendre. Puis il alla au milieu de la ville en poussant avec force des cris amers,
4:2	et se rendit jusqu'à la porte du roi car il était interdit d'entrer dans le palais du roi revêtu d'un sac.
4:3	Dans chaque province, dans les lieux où la parole du roi et son ordonnance parvinrent, il y eut une grande désolation parmi les Juifs ; ils jeûnaient, pleuraient, gémissaient et beaucoup se couchaient sur le sac et la cendre.
4:4	Les servantes d'Esther et ses eunuques vinrent lui raconter ces choses et la reine fut très effrayée. Elle envoya des vêtements à Mardochée pour le couvrir et lui faire ôter son sac, mais il ne les prit pas.
4:5	Alors Esther appela Hathac, l'un des eunuques que le roi avait établi pour la servir et elle le chargea de demander à Mardochée ce qui s'était passé et pourquoi il agissait ainsi.
4:6	Hathac sortit donc vers Mardochée sur la place de la ville, devant la porte du roi.
4:7	Mardochée lui raconta tout ce qui lui était arrivé, et la somme d'argent que Haman avait promis de payer comptant au trésor du roi, pour la destruction des Juifs.
4:8	Il lui donna une copie de l'ordonnance qui avait été mise par écrit et qui avait été publiée dans Suse en vue de les exterminer afin qu'il la montre à Esther et lui fasse tout connaître, en la chargeant d'entrer chez le roi pour lui demander grâce et lui faire une requête en faveur de son peuple.
4:9	Ainsi Hathac vint et rapporta à Esther les paroles de Mardochée.

### Esther prête à risquer sa vie pour ses frères, elle demande un jeûne

4:10	Esther dit à Hathac et lui ordonna de dire à Mardochée :
4:11	Tous les serviteurs du roi et le peuple des provinces du roi savent qu'il existe une loi prescrivant la peine de mort contre quiconque, homme ou femme qui entre chez le roi, dans la cour intérieure sans avoir été appelé, à moins que le roi ne lui tende le sceptre d'or, celui-là a la vie sauve. Or, il y a déjà 30 jours que je n'ai pas été appelée pour entrer chez le roi.
4:12	On rapporta les paroles d'Esther à Mardochée.
4:13	Mardochée fit cette réponse à Esther : Ne t'imagine pas que tu échapperas seule d'entre tous les Juifs parce que tu es dans la maison du roi.
4:14	Mais si tu te tais, si tu te tais en ce temps-ci, les Juifs seront secourus et délivrés par quelque autre moyen, mais toi et la maison de ton père, vous périrez. Et qui sait si tu n'es pas arrivée à la royauté pour un temps comme celui-ci ?
4:15	Alors Esther dit qu'on fasse cette réponse à Mardochée :
4:16	Va, rassemble tous les Juifs qui se trouvent à Suse et jeûnez pour moi, sans manger ni boire pendant trois jours, ni la nuit ni le jour. Moi aussi et mes servantes nous jeûnerons de même, puis j'entrerai chez le roi malgré la loi et, si je dois périr, je périrai.
4:17	Mardochée s'en alla et fit comme Esther lui avait ordonné.

## Chapitre 5

### Requête d'Esther auprès du roi

5:1	Et il arriva qu'au troisième jour, Esther se revêtit d'un habit royal et se tint dans la cour intérieure de la maison du roi, au-devant de la maison du roi. Le roi était assis sur le trône de son royaume dans le palais royal, vis-à-vis de la porte du palais.
5:2	Dès que le roi vit la reine Esther qui se tenait debout dans la cour, elle porta grâce à ses yeux. Et le roi tendit à Esther le sceptre d'or qu'il avait en main. Esther s'approcha et toucha l'extrémité du sceptre.
5:3	Le roi lui dit : Qu'as-tu, reine Esther, et que demandes-tu ? Quand ce serait la moitié du royaume, elle te serait donnée.
5:4	Esther répondit : Si le roi le trouve bon, que le roi vienne aujourd'hui avec Haman au festin que je lui ai préparé.
5:5	Alors le roi dit : Qu'on fasse venir en toute hâte Haman pour accomplir la parole d'Esther. Le roi vint donc avec Haman au festin qu'Esther avait préparé.
5:6	Le roi dit à Esther, pendant qu'on buvait le vin : Quelle est ta demande ? Elle te sera accordée. Quelle est ta requête ? Quand ce serait la moitié du royaume, tu l'obtiendras.
5:7	Esther répondit et dit : Voici ce que je demande et ce que je désire.
5:8	Si j'ai trouvé grâce aux yeux du roi, et si le roi trouve bon d'accorder ma demande et d'exaucer ma requête, que le roi et Haman viennent au festin que je leur préparerai, et je ferai demain selon la parole du roi.
5:9	Haman sortit ce jour-là, joyeux et le cœur content. Mais aussitôt qu'il vit, à la porte du roi, Mardochée, qui ne se leva pas, et ne se remua pas pour lui, Haman fut rempli de colère contre Mardochée.
5:10	Toutefois, Haman se fit violence et il alla dans sa maison. Puis il envoya chercher ses amis et Zéresh, sa femme.
5:11	Alors Haman leur fit le compte de la gloire de ses richesses, du nombre de ses enfants, de tout ce que le roi avait fait pour l'agrandir, et comment il l'avait élevé au-dessus des princes et des serviteurs du roi.
5:12	Puis Haman dit : Même la reine Esther n'a fait venir que moi et le roi au festin qu'elle a fait, et je suis encore convié par elle avec le roi.
5:13	Mais tout cela ne me sert de rien, pendant tout le temps que je vois Mardochée, ce Juif, assis à la porte du roi.
5:14	Alors Zéresh sa femme, et tous ses amis lui répondirent : Qu'on fasse un bois haut de 50 coudées, et demain matin dis au roi qu'on y pende Mardochée. Et tu iras joyeux au festin avec le roi. Cette parole parut bonne devant Haman, et il fit préparer le bois.

## Chapitre 6

### Le roi Assuérus se souvient de Mardochée et le récompense

6:1	Cette nuit-là, le roi ne put dormir. Il ordonna qu'on lui apporte le livre de souvenirs<!--Voir Mal. 3:16.-->, les discours du jour, et on les lut devant le roi.
6:2	Et l'on trouva écrit ce que Mardochée avait rapporté au sujet de la conspiration de Bigthan et de Théresh, les deux eunuques du roi, de la garde du seuil, qui avaient cherché à mettre la main sur le roi Assuérus.
6:3	Alors le roi dit : Quelle marque d'honneur et de grandeur a-t-on accordée à Mardochée pour cela ? Et les jeunes hommes du roi, qui le servaient, répondirent : On ne lui a rien accordé.
6:4	Et le roi dit : Qui est dans la cour ? Or, Haman était venu dans la cour extérieure de la maison du roi, pour dire au roi qu'il fasse pendre Mardochée au bois qu'il avait fait préparer.
6:5	Les jeunes hommes du roi lui répondirent : Voici Haman qui se tient dans la cour. Et le roi dit : Qu'il entre !
6:6	Haman donc entra, et le roi lui dit : Comment faut-il traiter un homme que le roi désire honorer ? Or Haman dit en son cœur : À qui le roi voudrait-il faire plus d'honneur qu'à moi ?
6:7	Et Haman répondit au roi : Quant à l'homme que le roi désire honorer,
6:8	qu'on lui apporte le vêtement royal dont le roi se revêt et qu'on lui amène le cheval que le roi monte, et qu'on lui mette la couronne royale sur la tête.
6:9	Et que le vêtement et le cheval soient remis aux mains de l'un des princes nobles qui sont auprès du roi, et qu'on revête l'homme que le roi prend plaisir à honorer, et qu'on le promène dans les rues de la ville, monté sur le cheval et qu'on crie devant lui : C'est ainsi que l'on traite l'homme que le roi désire honorer !
6:10	Alors le roi dit à Haman : Hâte-toi de prendre le vêtement et le cheval, comme tu l'as dit, et fais ainsi pour Mardochée le Juif qui est assis à la porte du roi ! Ne laisse pas tomber une parole de tout ce dont tu as parlé !
6:11	Et Haman donc prit le vêtement et le cheval, il revêtit Mardochée et le promena à cheval dans les rues de la ville, et il criait devant lui : C'est ainsi que l'on traite l'homme que le roi désire honorer !
6:12	Puis Mardochée retourna à la porte du roi, et Haman se retira en hâte dans sa maison, pleurant et ayant la tête couverte.
6:13	Haman raconta à Zéresh, sa femme, et à tous ses amis, tout ce qui lui était arrivé. Ses sages, et Zéresh, sa femme, lui répondirent : Si Mardochée devant lequel tu as commencé à tomber, est de la race des Juifs, tu n'auras pas le dessus sur lui, mais tu tomberas, tu tomberas devant lui.
6:14	Comme ils parlaient encore avec lui, les eunuques du roi vinrent et se hâtèrent d'amener Haman au festin qu'Esther avait préparé.

## Chapitre 7

### La plaidoirie d'Esther

7:1	Le roi donc et Haman allèrent au festin chez la reine Esther.
7:2	Le roi dit encore à Esther, ce second jour, pendant qu'on buvait le vin : Quelle est ta demande, reine Esther ? Elle te sera accordée ! Et quelle est ta requête ? Quand ce serait la moitié du royaume, cela sera fait.
7:3	Alors la reine Esther répondit et dit : Si j'ai trouvé grâce à tes yeux, ô roi, et si le roi le trouve bon, que ma vie me soit donnée à ma demande, et que mon peuple me soit donné à ma requête.
7:4	Car nous avons été vendus, mon peuple et moi, pour être détruits, tués et exterminés. Si nous avions été vendus pour être esclaves et serviteurs, j'aurais gardé le silence, bien que l'oppresseur ne saurait compenser le dommage que le roi en recevrait.
7:5	Le roi Assuérus parla et dit à la reine Esther : Qui est-il, et où est-il celui dont le cœur est rempli de tels desseins ?
7:6	Esther répondit : L'homme qui est notre adversaire, notre ennemi, c'est Haman, le méchant que voici ! Alors Haman fut terrifié en présence du roi et de la reine.

### Haman pris à son propre piège

7:7	Le roi, dans sa colère, se leva et quitta le festin, il entra dans le jardin du palais. Mais Haman resta, afin de faire une requête pour sa vie à la reine Esther, car il voyait bien que sa perte était résolue par le roi.
7:8	Puis le roi revint du jardin du palais dans la salle du festin, or Haman s'était jeté sur le lit où était Esther et le roi dit : Va-t-il aussi violer la reine sous mes yeux, dans cette maison ? Dès que la parole fut sortie de la bouche du roi, aussitôt on couvrit le visage d'Haman.
7:9	Et Harbona, l'un des eunuques, dit en présence du roi : Voici, la potence que Haman a faite pour Mardochée, qui a parlé pour le bien du roi. Elle est dressée dans la maison d'Haman et elle a 50 coudées de haut. Alors le roi dit : Qu'on l'y pende !
7:10	Et ils pendirent Haman à la potence qu'il avait préparée pour Mardochée. Et la colère du roi fut apaisée.

## Chapitre 8

### L'échec du plan d'Haman

8:1	Ce même jour, le roi Assuérus donna à la reine Esther la maison d'Haman, l'oppresseur des Juifs et Mardochée fut introduit devant le roi, car Esther avait raconté ce qu'il était pour elle.
8:2	Le roi ôta son anneau, celui qu'il avait repris à Haman et le donna à Mardochée. Esther établit Mardochée sur la maison d'Haman.
8:3	Et Esther continua de parler en la présence du roi. Elle se jeta à ses pieds, elle pleura, et elle le supplia de faire que la malice d'Haman, l'Agaguite, et ce qu'il avait machiné contre les Juifs n'eût pas d'effet.
8:4	Et le roi tendit le sceptre d'or à Esther qui se releva et resta debout devant le roi.
8:5	Et elle dit : Si le roi le trouve bon, et si j'ai trouvé grâce devant lui, et si la chose semble convenable au roi, et si je suis agréable à ses yeux, qu'on écrive pour révoquer les lettres qui concernaient la machination d'Haman fils d'Hammedatha, l'Agaguite, qu'il avait écrites pour détruire les Juifs qui sont dans toutes les provinces du roi.
8:6	Car comment pourrais-je voir le mal qui arriverait à mon peuple, et comment pourrais-je voir la destruction de ma parenté ?
8:7	Le roi Assuérus dit à la reine Esther et au Juif Mardochée : Voici, j'ai donné la maison d'Haman à Esther, et il a été pendu au bois pour avoir étendu sa main contre les Juifs.
8:8	Vous, au nom du roi, écrivez comme bon vous semble au sujet des Juifs et scellez l'écrit avec l'anneau du roi ! En effet, un document écrit au nom du roi et scellé de l'anneau du roi ne peut être révoqué.
8:9	Et en ce même temps, le vingt-troisième jour du troisième mois, qui est le mois de Sivan, les secrétaires du roi furent appelés et on écrivit comme Mardochée l'ordonna aux Juifs, aux satrapes, aux gouverneurs, et aux princes des 127 provinces, de l'Inde jusqu'à l'Éthiopie, à chaque province selon son écriture, à chaque peuple selon sa langue, et aux Juifs selon leur écriture et selon leur langue.
8:10	On écrivit les lettres au nom du roi Assuérus, et on les scella de l'anneau du roi. On les envoya par des courriers, ayant pour montures des chevaux, des coursiers, fils de coursiers royaux.
8:11	Le roi accordait aux Juifs de toutes les villes de se rassembler et de défendre leur vie, de détruire, de tuer, et d'exterminer toute force armée d'un peuple ou d'une province qui voudrait les assiéger, avec leurs petits enfants et leurs femmes, et de piller leurs biens.
8:12	Cela en un seul jour, dans toutes les provinces du roi Assuérus, le treizième jour du douzième mois, qui est le mois d'Adar.
8:13	Les lettres qui furent écrites portaient, que cette ordonnance serait publiée dans chaque province, et proposée publiquement à tous les peuples, afin que les Juifs soient prêts en ce jour-là pour se venger de leurs ennemis.
8:14	Les courriers, montés sur des coursiers, sur des coursiers royaux, partirent, se dépêchant et se hâtant d'après la parole du roi. L'ordonnance fut aussi publiée dans Suse, la ville capitale.

### Mardochée honoré

8:15	Et Mardochée sortit de chez le roi en vêtement royal violet et blanc, avec une grande couronne en or, et une robe en byssus et en pourpre rouge. Et la ville de Suse poussait des cris de réjouissance, et elle fut dans la joie.
8:16	Il y eut pour les Juifs du bonheur et de la joie, des réjouissances et des honneurs.
8:17	Dans chaque province et dans chaque ville, partout où arrivait l'ordre du roi et son décret, il y eut pour les Juifs de la joie, des réjouissances, des festins, et des fêtes. Et beaucoup de gens d'entre les peuples du pays se faisaient Juifs, parce que la frayeur des Juifs les avait saisis.

## Chapitre 9

### Les Juifs triomphent de leurs ennemis

9:1	Le douzième mois, qui est le mois d'Adar, le treizième jour du mois, où la parole du roi et son ordonnance devaient être exécutées, au jour où les ennemis des Juifs espéraient dominer, ce fut le contraire qui arriva ; les Juifs furent maîtres de ceux qui les haïssaient.
9:2	Les Juifs se rassemblèrent dans leurs villes, dans toutes les provinces du roi Assuérus pour mettre la main sur ceux qui cherchaient leur perte. Personne ne put leur résister, car la crainte qu'on avait d'eux avait saisi tous les peuples.
9:3	Et tous les princes des provinces, les satrapes, les gouverneurs, et ceux qui s'occupaient des affaires du roi, soutenaient les Juifs, à cause de la terreur que leur inspirait Mardochée.
9:4	Car Mardochée était grand dans la maison du roi, et sa renommée se répandait dans toutes les provinces, parce que cet homme, Mardochée, allait toujours grandissant.
9:5	Les Juifs donc frappèrent tous leurs ennemis à coups d'épée et en firent un grand carnage, de sorte qu'ils traitèrent selon leurs désirs ceux qui les haïssaient.
9:6	Et même dans Suse, la capitale, les Juifs tuèrent et firent périr 500 hommes.
9:7	Ils tuèrent aussi Parshandatha, Dalphon, Aspatha,
9:8	Poratha, Adalia, Aridatha,
9:9	Parmashtha, Arizaï, Aridaï, et Vayezatha,
9:10	les dix fils d'Haman, fils d'Hammedatha, l'oppresseur des Juifs. Mais ils ne mirent pas leurs mains au pillage.
9:11	Ce jour-là, on rapporta au roi le nombre de ceux qui avaient été tués dans Suse, la capitale.
9:12	Le roi dit à la reine Esther : Dans Suse, la capitale, les Juifs ont tué et détruit 500 hommes, et les dix fils d'Haman, qu'auront-ils fait dans le reste des provinces du roi ? Quelle est ta demande ? Et elle te sera octroyée. Et quelle est encore ta prière ? Et cela sera fait.
9:13	Esther répondit : Si le roi le trouve bon qu'il soit permis aux Juifs qui sont à Suse, d'agir encore demain selon le décret d'aujourd'hui, et que l'on pende au bois les dix fils d'Haman.
9:14	Et le roi ordonna de faire ainsi, de sorte que l'ordonnance fut publiée dans Suse, et on pendit les dix fils d'Haman.
9:15	Les Juifs donc qui étaient dans Suse se rassemblèrent encore le quatorzième jour du mois d'Adar, et tuèrent dans Suse 300 hommes. Mais ils ne mirent pas la main au pillage.
9:16	Les autres Juifs qui étaient dans les provinces du roi se rassemblèrent et défendirent leur vie. Ils eurent du repos et furent délivrés de leurs ennemis, et ils tuèrent 75 000 hommes de ceux qui les haïssaient. Mais ils ne mirent pas la main au pillage.
9:17	Cela se fit le treizième jour du mois d'Adar, et le quatorzième du même mois ils se reposèrent, et ils en firent un jour de festin et de joie.

### Institution de la fête des Pourim

9:18	Les Juifs qui étaient dans Suse, se réunirent le treizième et le quatorzième jour du même mois, et ils se reposèrent le quinzième jour. Ils le célébrèrent comme un jour de festin et de joie.
9:19	C'est pourquoi les Juifs des campagnes, qui habitent dans des villes sans murailles, font le quatorzième jour du mois d'Adar un jour de joie, de festin, un jour de fête, où l'on s'envoie des portions les uns aux autres.
9:20	Car Mardochée écrivit ces choses, et il envoya les lettres à tous les Juifs qui étaient dans toutes les provinces du roi Assuérus, auprès et au loin,
9:21	leur ordonnant de célébrer d'année en année le quatorzième jour et le quinzième jour du mois d'Adar
9:22	comme les jours où les Juifs avaient eu du repos de leurs ennemis, selon le mois où leur angoisse fut changée en joie, et leur deuil en jour heureux, et de faire de ces jours des jours de festin et de joie, où l'on s'envoie des portions les uns aux autres, et des dons aux pauvres.
9:23	Et les Juifs acceptèrent de faire ce qu'on avait commencé et ce que Mardochée leur avait écrit.
9:24	Car Haman, fils d'Hammedatha, l'Agaguite, l'oppresseur de tous les Juifs, avait machiné contre les Juifs de les détruire, et il avait jeté le Pour, c'est-à-dire le sort, pour les défaire, et pour les détruire.
9:25	Mais quand Esther vint devant le roi, celui-ci ordonna par écrit de faire retomber sur la tête d'Haman le mauvais dessein qu'il avait conçu contre les Juifs et de le pendre au bois, ainsi que ses fils.
9:26	C'est pourquoi on appelle ces jours-là Pourim, du nom de Pour<!--Pour ou Pourim : ce terme signifie « sort » (Est. 3:7). La fête de Pourim a été instituée pour célébrer la délivrance de l'extermination planifiée par Haman, à la suite de l'intervention héroïque d'Esther. Les Juifs l'observent désormais chaque année le 14 du mois d'Adar (février ou mars) depuis le temps d'Esther jusqu'à ce jour.-->. Et suivant toutes les paroles de cette dépêche, et selon ce qu'ils avaient vu sur cela, et ce qui leur était arrivé,
9:27	les Juifs établirent et acceptèrent pour eux et pour leurs descendances, et pour tous ceux qui se joindraient à eux, de ne pas manquer de célébrer, en tout, d'année en année, ces deux jours, selon ce qui était écrit et selon leur temps déterminé.
9:28	Ces jours devaient être rappelés et célébrés d'âge en âge, dans chaque famille, dans chaque province et dans chaque ville. Ces jours des pourim ne devaient jamais être abolis parmi les Juifs, ni le souvenir s'en effacer parmi leurs descendants.
9:29	La reine Esther, fille d'Abichaïl, écrivit aussi avec le Juif Mardochée, de manière pressante pour la seconde fois, pour confirmer cette lettre sur les Pourim.
9:30	On envoya des lettres à tous les Juifs, dans les 127 provinces du royaume d'Assuérus, avec des paroles de paix et de vérité,
9:31	pour établir ces jours de Pourim au temps fixé, comme Mardochée le Juif et la reine Esther les avaient établis pour eux, et comme ils les avaient établis pour eux-mêmes et pour leur descendant, à l'occasion de leur jeûne et de leurs cris.
9:32	Ainsi l'édit d'Esther confirma l'institution des Pourim, et cela fut écrit dans le livre.

## Chapitre 10

### Mardochée : un grand homme

10:1	Le roi Assuérus fixa un impôt sur le pays et sur les îles de la mer.
10:2	Et quant à tous les faits concernant sa puissance et sa force, ainsi que l'exposé exact de la grandeur de Mardochée à laquelle le roi l'éleva, ces choses ne sont-elles pas écrites dans le livre des discours du jour des rois de Médie et de Perse ?
10:3	Car Mardochée le Juif fut le second après le roi Assuérus, et il fut grand parmi les Juifs, et agréable à la multitude de ses frères, procurant le bien de son peuple et parlant pour la prospérité de toute sa race.
