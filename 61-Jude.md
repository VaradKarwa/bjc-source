# Yéhouda (Jude) (Jud.)

Signification : Qu'il (YHWH) soit loué

Auteur : Yéhouda (Jude)

(Gr. : Ioudas)

Thème : Le combat de la foi

Date de rédaction : Env. 68 ap. J.-C.

Cette épître, écrite par Yéhouda, l'un des frères de Yéhoshoua ha Mashiah (Jésus-Christ) homme, était destinée aux assemblées d'Asie Mineure. Il les avertit contre les faux docteurs et les exhorte à se confier en Yéhoshoua pour leur salut éternel.

## Chapitre 1

### Introduction

1:1	Yéhouda, esclave de Yéhoshoua Mashiah et frère de Yaacov, aux appelés, sanctifiés en Elohîm le Père et gardés par Yéhoshoua Mashiah :
1:2	que la miséricorde, la paix et l'amour vous soient multipliés !

### Exhortation à combattre pour la foi

1:3	Bien-aimés, je le fais en toute hâte en vous écrivant au sujet de notre salut commun, je me suis trouvé dans la nécessité de vous écrire afin de vous exhorter à combattre pour la foi qui a été donnée une fois pour toutes aux saints.
1:4	Car il s'est glissé certains hommes, dont la condamnation est écrite depuis longtemps, des impies, qui changent la grâce de notre Elohîm en luxure sans bride, et qui renient le seul Maître<!--Vient du grec « despotes ». Voir Lu. 2:29 ; Ac. 4:24 ; 2 Pi. 2:1 ; Ap. 6:10. Yéhoshoua (Jésus) est le Seul Maître.--> Yéhoshoua Mashiah, notre Elohîm et Seigneur.

### Exemples d'incrédulité et de révolte

1:5	Mais je veux vous rappeler une chose que vous savez déjà, que le Seigneur après avoir délivré le peuple du pays d'Égypte, a fait ensuite disparaître ceux qui avaient refusé de croire.
1:6	Quant aux anges qui n'avaient pas gardé leur origine, mais qui avaient quitté leur propre demeure, il les a gardés sous l'obscurité, dans des liens éternels<!--Voir 2 Pi. 2:4.-->, pour le jugement du grand jour.
1:7	De même, Sodome et Gomorrhe et les villes voisines, s'étant livrées comme eux à la prostitution et étant allées après une autre chair, sont désignées comme exemple et subissent le châtiment du feu éternel.

### Caractéristiques des faux docteurs

1:8	De la même façon, ceux-là aussi, plongés dans leurs rêves, souillent en effet leur chair, méprisent la seigneurie et blasphèment contre les gloires.
1:9	Or l'archange Miyka'el, lorsqu'il contestait avec le diable et lui disputait le corps de Moshè, n'osa pas prononcer contre lui un jugement blasphématoire, mais il dit seulement : Que le Seigneur te réprimande d'une manière tranchante !
1:10	Mais ceux-là blasphèment en effet contre tout ce qu'ils ne connaissent pas, et ils se corrompent dans tout ce qu'ils savent naturellement, comme font les bêtes dépourvues de raison.
1:11	Malheur à eux ! Car ils ont suivi la voie de Caïn, ils se sont jetés dans l'égarement de Balaam pour une récompense, ils ont péri par la rébellion de Koré<!--No. 16:1-35 ; 2 Pi. 2:15 ; Ap. 2:14.-->.
1:12	Ce sont des écueils dans vos agapes, faisant des festins avec vous sans crainte, se nourrissant eux-mêmes. Ce sont des nuées sans eau, emportées par des vents çà et là, des arbres de fin d'automne sans fruits, deux fois morts, déracinés,
1:13	des vagues impétueuses de la mer, rejetant l'écume de leurs impuretés, des étoiles errantes, à qui l'obscurité des ténèbres est réservée éternellement.
1:14	Mais c'est aussi pour eux que Hénoc, le septième homme après Adam, a prophétisé, en disant : Voici, le Seigneur est venu avec la multitude innombrable de ses saints,
1:15	pour exercer un jugement contre tous et pour convaincre<!--Le mot grec « exelegcho » signifie aussi « prouver qu'une chose est fausse, condamner ».--> tous les impies d'entre eux, de toutes leurs actions d'impiété qu'ils ont commises d'une manière impie, et de toutes les paroles injurieuses que les pécheurs impies ont proférées contre lui.
1:16	Ce sont des gens qui ne font que murmurer, que se plaindre de leur sort, qui marchent selon leurs convoitises, dont la bouche prononce des discours enflés, et qui admirent les personnes pour le profit qui leur en revient.
1:17	Mais vous, mes bien-aimés, souvenez-vous des paroles qui ont été dites auparavant par les apôtres de notre Seigneur Yéhoshoua Mashiah.
1:18	Qui vous disaient que dans le dernier temps, il y aura des moqueurs, marchant selon leurs convoitises impies.
1:19	Ce sont des gens qui se séparent eux-mêmes, des hommes animaux, n'ayant pas l'Esprit.

### Exhortations

1:20	Mais vous, mes bien-aimés, édifiez-vous vous-mêmes sur votre très sainte foi, et priez par le Saint-Esprit,
1:21	conservez-vous les uns les autres dans l'amour d'Elohîm, en attendant la miséricorde de notre Seigneur Yéhoshoua Mashiah pour la vie éternelle.
1:22	Et ayez vraiment pitié de ceux qui doutent<!--Vient du grec « diakrino » qui signifie aussi « faire une distinction, s'opposer, lutter, être en désaccord avec quelqu'un, hésiter, douter, apprendre par discernement ».--> ;
1:23	mais les autres, sauvez-les avec crainte en les arrachant hors du feu, en haïssant même la robe souillée<!--Les vêtements représentent la justice (Es. 61:10 ; Ap. 19:8). La robe souillée par la chair représente la justice humaine (Es. 64:5 ; La. 1:9 ; Za. 3:1-9). Voir aussi Ec. 9:8. Les œuvres de la chair décrites en Ga. 5:19-21 souillent les humains.--> par la chair.

### Conclusion

1:24	Or, à celui qui est capable de vous garder de toute chute et de vous faire paraître sans défaut et dans l'allégresse devant sa gloire,
1:25	à Elohîm, seul sage, notre Sauveur, soient gloire et magnificence, force et puissance, et maintenant et dans tous les âges ! Amen !
