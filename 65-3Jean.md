# 3 Yohanan (3 Jean) (3 Jn.)

Signification : YHWH a fait grâce

Auteur : Yohanan (Jean)

(Gr. : Ioannes)

Thème : Sincérité, hospitalité et caractère du chrétien

Date de rédaction : Env. 85 ap. J.-C.

Cette épître était destinée à Gaïos, l'un des responsables d'une assemblée d'Asie Mineure, dont Yohanan loue la piété et la générosité. Il l'avertit de l'orgueil et des agissements de Diotrephes qui étaient contraires à la Parole, mais souligne le bon témoignage de Démétrios.

## Chapitre 1

### Introduction

1:1	L'ancien, à Gaïos le bien-aimé que j'aime en vérité.
1:2	Bien-aimé, je souhaite que tu prospères<!--La prospérité dont il est question dans ce passage n'a rien à voir avec l'évangile de prospérité qui met l'accent sur la richesse matérielle. Le mot grec « euodoo » signifie « concevoir un voyage prospère et diligent », « mener par une voie directe et facile », « prospérer », « être heureux ».--> en toutes choses et que tu sois en bonne santé, comme ton âme est en prospérité.
1:3	Car je me suis beaucoup réjoui lorsque des frères sont venus et ont rendu témoignage à la vérité qui est en toi, et à la manière dont tu marches dans la vérité.
1:4	Je n'ai pas de plus grande joie que d'apprendre que mes enfants marchent dans la vérité.

### L'hospitalité

1:5	Bien-aimé, tu agis fidèlement dans tout ce que tu fais envers les frères et envers les étrangers,
1:6	qui en présence de l'assemblée, ont rendu témoignage de ton amour. Tu feras bien de les escorter d'une manière digne d'Elohîm, en donnant le nécessaire pour leur voyage.
1:7	Car ils sont partis pour son Nom, ne prenant rien des nations.
1:8	Nous devons donc recevoir de tels hommes, afin que nous devenions leurs compagnons d'œuvre pour la vérité.

### Les mauvais actes de Diotrephes

1:9	J'ai écrit à l'assemblée, mais Diotrephes, qui désire être le premier parmi eux, ne nous reçoit pas.
1:10	C'est pourquoi, si je viens, je rappellerai les actions qu'il commet, en tenant contre nous de mauvais discours. Et n'étant pas content de cela, non seulement il ne reçoit pas les frères, mais il empêche même ceux qui veulent les recevoir et les chasse de l'assemblée.
1:11	Bien-aimé, n'imite pas ce qui est mauvais, mais ce qui est bon. Celui qui fait le bien est d'Elohîm, mais celui qui fait le mal n'a pas vu Elohîm.

### Témoignage de Démétrios

1:12	Tous rendent témoignage à Démétrios, et la vérité même le lui rend. Et nous aussi, nous lui rendons témoignage, et vous savez que notre témoignage est véritable.

### Salutations

1:13	J'avais beaucoup de choses à écrire, mais je ne veux pas t'écrire avec l'encre et la plume.
1:14	Mais j'espère te voir bientôt, et nous parlerons de bouche à bouche.
1:15	Que la paix soit avec toi ! Les amis te saluent. Salue les amis, chacun par son nom.
