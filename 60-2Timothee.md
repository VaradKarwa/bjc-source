# 2 Timotheos (2 Timothée) (2 Ti.)

Signification : Qui adore ou honore Elohîm

Auteur : Paulos (Paul)

Thème : Le maintien de la vérité

Date de rédaction : Env. 67 ap. J.-C.

Cette lettre s'adresse à Timotheos (Timothée) dont le père était grec et la mère juive. Le jeune homme se convertit au Mashiah (Christ) avec sa mère et sa grand-mère, dès le premier voyage missionnaire de Paulos (Paul), au cours duquel ce dernier passa à Lystre.

Paulos écrivit cette épître pastorale alors qu'il était emprisonné à Rome, après avoir été arrêté dans une province orientale à Éphèse ou Troas.

Ses conditions de détention étant plus rudes que la première fois, Paulos est dubitatif quant à sa remise en liberté. Il demande donc à Timotheos, son fils dans la foi et fidèle compagnon d'œuvre, de le rejoindre à Rome afin de recevoir, semble-t-il, ses dernières volontés. Après avoir exposé à Timotheos les qualités et les devoirs d'un bon serviteur de l'Évangile, il l'encourage à lutter contre les faux docteurs et l'apostasie en prêchant la parole en toutes circonstances.

## Chapitre 1

### Introduction

1:1	Paulos, apôtre de Yéhoshoua Mashiah, par la volonté d'Elohîm, selon la promesse de la vie qui est en Mashiah Yéhoshoua,
1:2	à Timotheos, mon fils bien-aimé : grâce, miséricorde et paix de la part d'Elohîm le Père et du Mashiah Yéhoshoua notre Seigneur !

### Paulos (Paul) encourage Timotheos (Timothée) à souffrir les afflictions de l'Évangile

1:3	Je rends grâce à Elohîm, à qui je rends mon culte, à l'exemple de mes ancêtres, avec une conscience pure, faisant sans cesse mention de toi dans mes supplications nuit et jour.
1:4	Me souvenant de tes larmes, j'ai un très vif désir de te voir afin d'être rempli de joie.
1:5	Je conserve en effet le souvenir de la foi sincère qui est en toi. Elle a premièrement habité en Loïs, ta grand-mère, et en Eunike<!--« Bénie par la victoire ».-->, ta mère, et qui, j'en suis persuadé, habite aussi en toi.
1:6	C'est à cause de cela que je te rappelle de ranimer le don de grâce d'Elohîm qui est en toi à travers l'imposition de mes mains.
1:7	Car Elohîm ne nous a pas donné un esprit de timidité, mais de force, d'amour<!--Il est question ici de l'amour « agape », c'est-à-dire l'amour divin.--> et de sagesse.
1:8	N'aie donc pas honte du témoignage de notre Seigneur, ni de moi son prisonnier. Mais souffre avec moi pour l'Évangile, selon la puissance d'Elohîm,
1:9	qui nous a sauvés et nous a appelés par une sainte vocation, non selon nos œuvres, mais selon son propre dessein et selon la grâce qui nous a été accordée en Yéhoshoua Mashiah avant les temps éternels,
1:10	et qui maintenant a été manifestée par l'apparition de notre Sauveur Yéhoshoua Mashiah, qui a en effet aboli<!--Vient du grec « katargeo » qui signifie « rendre vain », « inemployé », « inactif », « inopérant », « priver de force », « d'influence », « de pouvoir », « faire cesser », « amener à une fin », « annuler ».--> la mort et qui a mis en lumière la vie et l'immortalité par le moyen de l'Évangile,
1:11	pour lequel j'ai été établi prédicateur, apôtre et docteur des nations.
1:12	C'est pourquoi aussi je souffre ces choses, mais je n'en ai pas honte, car je connais celui en qui j'ai cru et je suis persuadé qu'il est puissant pour garder mon dépôt<!--Il est question ici de la connaissance correcte et de la pure doctrine de l'Évangile qui doit être fermement et fidèlement gardée, ainsi que consciencieusement délivrée aux autres.--> jusqu'à ce jour-là.
1:13	Retiens dans la foi et dans l'amour qui est en Yéhoshoua Mashiah, le modèle des saines paroles que tu as apprises de moi.
1:14	Garde le bon dépôt par le Saint-Esprit qui habite en nous.
1:15	Tu sais que tous ceux qui sont en Asie se sont détournés de moi, parmi eux sont Phygèlos et Hermogène.
1:16	Que le Seigneur accorde sa miséricorde à la maison d'Onésiphoros, car il m'a souvent consolé et il n'a pas eu honte de mes chaînes.
1:17	Au contraire, quand il est venu à Rome, il m'a cherché avec beaucoup d'empressement et il m'a trouvé.
1:18	Que le Seigneur lui donne de trouver miséricorde auprès du Seigneur en ce jour-là ! Et tu sais mieux que personne combien de services il m'a rendus à Éphèse.

## Chapitre 2

### La conduite d'un disciple du Mashiah : transmettre la parole, combattre le bon combat

2:1	Toi donc, mon fils, sois fortifié dans la grâce qui est dans le Mashiah Yéhoshoua.
2:2	Et ce que tu as entendu de moi devant beaucoup de témoins, confie-le à des gens fidèles qui seront capables de l'enseigner aussi à d'autres.
2:3	Toi donc, souffre avec moi comme un bon soldat de Yéhoshoua Mashiah.
2:4	Nul qui va à la guerre ne s'empêtre dans les affaires de la vie, afin de plaire à celui qui l'a enrôlé comme soldat.
2:5	Et de même, si quelqu'un lutte dans un jeu sportif, il n'est couronné que s'il a lutté selon les règles.
2:6	Il faut aussi que le laboureur travaille premièrement, et ensuite il recueille les fruits.
2:7	Considère ce que je dis, car le Seigneur te donne de l'intelligence en toutes choses.
2:8	Souviens-toi que Yéhoshoua Mashiah, qui est de la semence de David, est ressuscité des morts, selon mon Évangile,
2:9	pour lequel je souffre jusqu'à être dans les liens comme un malfaiteur. Mais la parole d'Elohîm n'est pas liée.
2:10	C'est à cause de cela que je supporte tout à cause des élus, afin qu'eux aussi obtiennent le salut qui est en Yéhoshoua Mashiah, avec la gloire éternelle.
2:11	Cette parole est sûre : en effet, si nous mourons avec lui, nous vivrons aussi avec lui.
2:12	Si nous supportons bravement et calmement les mauvais traitements, nous régnerons aussi avec lui. Si nous le renions, il nous reniera aussi<!--Lu. 9:26.-->.
2:13	Si nous sommes infidèles, il demeure fidèle, car il ne peut se renier lui-même.
2:14	Rappelle ces choses aux autres, attestant devant Elohîm de ne pas se battre pour des mots. Cela ne sert à rien, si ce n'est à la destruction des auditeurs.
2:15	Efforce-toi de te présenter<!--Voir 2 Co. 11:2.--> à Elohîm comme un homme approuvé<!--Le participe passé « approuvé » vient du grec « dokimos ». Du temps de Paulos (Paul), les systèmes bancaires actuels n'existaient pas, toute la monnaie était en métal. Pour obtenir les pièces de monnaie, le métal était fondu et versé dans des moules et après le démoulage, il était nécessaire d'enlever les bavures. Or de nombreuses personnes les grattaient pour récupérer le surplus de métal et même davantage, ce qui faussait le poids de la monnaie. Face à ce problème, de nombreuses lois furent promulguées à Athènes pour éradiquer la pratique du rognage des pièces en circulation. Il existait toutefois quelques changeurs intègres qui ne mettaient en circulation que des pièces au bon poids. On appelait ces personnes des « dokimos », ce qui signifie « éprouvés » ou « approuvés ».-->, un ouvrier qui n'a pas à avoir honte, qui enseigne correctement la parole de la vérité.
2:16	Mais évite les discours vains, inutiles et profanes, car ceux qui les tiennent avanceront toujours plus dans l'impiété,
2:17	et leur parole, comme une gangrène, aura des pâturages<!--Les brebis du Seigneur trouveront un pâturage selon Jean 10:9. De même, la parole des faux prophètes aura des pâturages. Ces pâturages représentent les personnes qui tombent dans leur piège.-->. De ce nombre sont Hymenaïos et Philètos,
2:18	qui se sont écartés<!--Écarter, dévier, s'écarter de, manquer le but. À l'époque des apôtres, il y avait plusieurs faux frères qui semaient la zizanie au milieu des enfants d'Elohîm. Parmi eux étaient Alexandros, le forgeron (1 Ti. 1:18-20), Hymenaïos (1 Ti. 1:18-20), Philètos (2 Ti. 2:16-18), les judaïsants (Ac. 15:1-29), Diotrephes (3 Jn. 1:9-11). Les faux frères sont des séducteurs.--> de la vérité, en disant que la résurrection est déjà arrivée, et qui renversent<!--Voir Tit. 1:11.--> la foi de quelques-uns.
2:19	Néanmoins, le fondement d'Elohîm demeure inébranlable, ayant ce sceau : Le Seigneur connaît ceux qui sont à lui<!--Le Seigneur connaît ses brebis. Voir No. 16:5 ; Jn. 10:14.-->, et : Quiconque invoque le Nom du Seigneur, qu'il s'éloigne de l'injustice.
2:20	Or dans une grande maison, il n'y a pas seulement des vases d'or et d'argent, mais il y en a aussi de bois et de terre. Les uns sont en effet des vases d'honneur, mais les autres sont d'un usage vil.
2:21	Si donc quelqu'un se purifie lui-même de ces choses, il sera un vase d'honneur, sanctifié et utile au Seigneur, et préparé pour toute bonne œuvre.
2:22	Mais fuis les désirs de la jeunesse et recherche la justice, la foi, l'amour et la paix avec ceux qui invoquent le Seigneur d'un cœur pur.
2:23	Mais évite les débats insensés<!--Questions folles. Il est question ici de disputes, débats, discussions ou questions oiseuses. Voir Tit. 3:9.--> et qui sont sans instruction, sachant qu'ils ne font que produire des querelles.
2:24	Or il n'est pas juste et correct qu'un esclave du Seigneur ait des querelles, mais qu'il soit doux envers tous, capable d'enseigner, supportant patiemment le mal,
2:25	enseignant avec douceur ceux qui ont un sentiment contraire, peut-être Elohîm leur donnera-t-il la repentance pour la connaissance précise et correcte de la vérité,
2:26	afin qu'ils retournent à la modération, hors des pièges du diable, qui les tient captifs pour qu'ils fassent sa volonté.

## Chapitre 3

### Le caractère des humains dans les derniers jours

3:1	Mais sache aussi ceci, que dans les derniers jours<!--Les derniers jours. Voir Ge. 49:1.--> il surviendra des temps difficiles.
3:2	Car les gens seront amoureux d'eux-mêmes, aimant l'argent, fanfarons, orgueilleux, blasphémateurs, rebelles à leurs parents, ingrats, sans religion,
3:3	sans affection naturelle, sans fidélité, calomniateurs<!--Vient du grec « diabolos » qui peut être aussi traduit par « diable ». Voir Mt. 4:1.-->, sans auto-contrôle, cruels, opposés à la bonté et aux hommes bons,
3:4	traîtres, téméraires, enflés d'orgueil, amis des voluptés plutôt qu'amis d'Elohîm<!--Le mot grec « philotheos » du préfixe « philos » qui signifie « amis, être lié d'amitié avec quelqu'un » (Mt. 11:19 ; Lu. 7:6 ; Jn. 15:13-15) et de « theos » généralement traduit par « dieu » (traduit par « elohîm » dans la présente Bible).-->,
3:5	ayant l'apparence de la piété<!--Le mot « apparence » vient du grec « morphosis » et du latin « forma » qui donnent « forme » en français. Il est question du formalisme, de l'attachement excessif aux règles, aux rites, aux coutumes et aux traditions. Dans l'assemblée de Laodicée, l'accent est plutôt mis sur les règles à observer et les apparences que sur la vie spirituelle et intérieure. Les manifestations extérieures du formalisme sont : les lieux « sacrés » pour adorer (temples, cathédrales, pèlerinages, etc.) ; l'observation des jours sacrés (dimanche et shabbat) ; les rituels censés permettre au croyant d'expérimenter Elohîm et de rentrer dans une vie bénie (circoncision, ordination, bénédiction nuptiale, paiement de la dîme, présentation des enfants à Elohîm par le pasteur, etc.) ; une manière spéciale de s'habiller (toge, soutane, collet clérical, kippa, voile, costume/cravate) ; un régime alimentaire spécial, etc. Voir Mt. 6:1-8.-->, mais en ayant renié la force. Éloigne-toi donc de telles gens.
3:6	Car c'est d'entre eux que sortent ceux qui se glissent dans les maisons et qui emmènent captives de petites femmes, chargées de péchés et guidées par toutes sortes de désirs,
3:7	qui apprennent toujours et qui ne peuvent jamais venir<!--Voir Jn. 6:44-45 et 65 ; 1 Ti. 2:4.--> à la connaissance précise et correcte de la vérité.
3:8	Mais comme Jannès et Jambrès s'opposèrent à Moshè, ceux-ci de même s'opposent à la vérité. Ce sont des gens à l'esprit corrompu et réprouvés en ce qui concerne la foi.
3:9	Mais ils ne feront pas de plus grands progrès, car leur folie sera manifestée à tous, comme le fut aussi celle de ces deux-là.
3:10	Mais toi, tu as suivi de près mon enseignement, ma conduite, mon dessein, ma foi, ma patience, mon amour, ma persévérance.
3:11	Et tu sais les persécutions et les afflictions qui me sont arrivées à Antioche, à Icone et à Lystre<!--Ac. 14.-->. Quelles persécutions n'ai-je pas supportées ? Et comment le Seigneur m'a délivré de toutes.
3:12	Mais tous ceux qui veulent vivre pieusement en Yéhoshoua Mashiah seront aussi persécutés.
3:13	Mais les gens méchants et imposteurs iront en empirant, égarant les autres et en s'égarant eux-mêmes.
3:14	Mais toi, demeure ferme dans les choses que tu as apprises et qui t'ont été confiées, sachant de qui tu les as apprises.
3:15	Parce que dès l'enfance<!--Vient du grec « brephos » qui signifie « un enfant à naître », « un embryon », « un fœtus », « un enfant nouveau-né », « un nourrisson », « un bébé ».-->, tu connais en effet les Lettres<!--Jn. 7:15.--> Sacrées, qui peuvent te rendre sage pour le salut par le moyen de la foi en Mashiah Yéhoshoua.
3:16	Toute Écriture est inspirée d'Elohîm et utile pour enseigner, pour convaincre, pour corriger et pour instruire<!--Toute l'éducation des enfants (qui est relative à la culture de l'esprit et de la moralité, et qui utilise dans ce but tantôt des ordres et des avertissements, tantôt des réprimandes, des corrections et des punitions). Elle inclut également l'exercice et le soin du corps.-->, selon la justice,
3:17	afin que l'homme d'Elohîm soit accompli et parfaitement instruit pour toute bonne œuvre.

## Chapitre 4

### Paulos (Paul) encourage solennellement Timotheos (Timothée) à prêcher la parole

4:1	Je t'en conjure donc devant l'Elohîm et le Seigneur, Yéhoshoua Mashiah, qui doit juger les vivants et les morts lors de son apparition et de son règne,
4:2	prêche la parole, sois présent<!--« Se tenir », « être prêt », « survenir ». Le mot grec « ephistemi » est utilisé pour des personnes survenant soudainement.--> quand l'occasion se présente ou hors saison. Réprimande d'une manière tranchante, censure, exhorte avec une entière patience et doctrine.
4:3	Car il viendra un temps où ils ne supporteront pas la saine doctrine, mais aimant qu'on leur chatouille les oreilles, ils accumuleront docteurs sur docteurs selon leurs propres désirs<!--Beaucoup refusent la saine doctrine et acceptent un évangile basé sur les biens matériels.-->.
4:4	Et ils détourneront vraiment leurs oreilles de la vérité et se tourneront vers les fables.
4:5	Mais toi, veille en toutes choses, endure les souffrances, fais l'œuvre d'un évangéliste, rends ton service pleinement approuvé.
4:6	Car pour moi, je sers déjà de libation et le temps de mon départ est survenu<!--Terme grec utilisé pour des personnes survenant soudainement ou un mal survenant sur quelqu'un. Voir 2 Ti. 4:2.-->.
4:7	J'ai combattu le bon combat, j'ai terminé la course, j'ai gardé la foi.
4:8	Désormais m'est réservée la couronne de justice, que le Seigneur, le juste Juge, me donnera en récompense en ce jour-là, et non seulement à moi, mais aussi à tous ceux qui auront aimé son apparition.
4:9	Hâte-toi de venir vers moi au plus tôt.
4:10	Car Démas m'a abandonné, ayant aimé chèrement le présent âge et il est parti pour Thessalonique. Crescens est allé en Galatie et Titos en Dalmatie.
4:11	Loukas seul est avec moi. Prends Markos et amène-le avec toi, car il m'est très utile pour le service.
4:12	Mais j'ai envoyé Tuchikos à Éphèse.
4:13	Quand tu viendras, apporte avec toi le manteau que j'ai laissé à Troas, chez Karpos, et les livres aussi, mais principalement mes parchemins.
4:14	Alexandros le forgeron m'a fait beaucoup de mal. Le Seigneur lui rendra selon ses œuvres.
4:15	Garde-toi aussi de lui, car il s'est fortement opposé à nos paroles.
4:16	Personne n'est venu à mon aide dans ma première défense, mais tous m'ont abandonné. Qu'il ne leur en soit pas tenu compte !
4:17	Mais le Seigneur s'est tenu près de moi et m'a fortifié, afin que ma prédication soit pleinement approuvée et que toutes les nations l'entendent. Et j'ai été délivré de la gueule du lion.
4:18	Le Seigneur me délivrera aussi de toute mauvaise œuvre et me sauvera dans son Royaume céleste. À lui soit la gloire d'âge en âge ! Amen !

### Salutations

4:19	Salue Priska<!--Priscilla.--> et Aquilas, et la famille d'Onésiphoros.
4:20	Erastos est resté à Corinthe, et j'ai laissé Trophimos malade à Milet.
4:21	Hâte-toi de venir avant l'hiver. Euboulos et Poudes, et Linos, et Klaudia, et tous les frères te saluent.
4:22	Que le Seigneur Yéhoshoua Mashiah soit avec ton esprit ! Que la grâce soit avec vous ! Amen !
