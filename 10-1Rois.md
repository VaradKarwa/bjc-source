# 1 Melakhim (1 Rois) (1 R.)

Signification : Roi, règne

Auteur : Inconnu

Thème : Unité du royaume après le schisme

Date de rédaction : 6ème siècle av. J.-C.

Ce livre relate la vie de Shelomoh (Salomon) : son accession à la royauté après la mort de son père David, son alliance avec Elohîm qui lui accorda une sagesse exceptionnelle ainsi que la construction du temple de YHWH et du palais royal.

Les premières années du règne de Shelomoh furent exemplaires. Malheureusement, il ne fit pas preuve de la même piété que son père et développa une affection toute particulière pour les femmes étrangères qui l'entraînèrent dans l'idolâtrie. À sa mort, son fils Rehabam (Roboam) accéda au trône et provoqua la division du royaume en deux. D'un côté les dix tribus du nord qui gardèrent le nom d'Israël, gouvernées par Yarobam (Jéroboam, serviteur de Shelomoh), et de l'autre côté les deux tribus du sud, Yéhouda et Benyamin, qui demeurèrent sous l'autorité de Rehabam.

Ce livre raconte également le règne et la conduite parfois abominable des rois d'Israël et de Yéhouda jusqu'à Achab et Yehoshaphat (Josaphat). Il présente la puissance de l'appel prophétique d'Eliyah (Élie), le Tishbite, qu'Elohîm suscita pour ramener son peuple à lui et montrer sa souveraineté.

## Chapitre 1

### Fin de la vie de David

1:1	Le roi David était vieux et avancé en âge. On le couvrait de vêtements parce qu'il ne parvenait pas à se réchauffer.
1:2	Ses serviteurs lui dirent : Que l'on cherche pour le roi, notre seigneur, une jeune fille vierge. Qu'elle se tienne devant le roi, qu'elle le soigne et qu'elle dorme en son sein afin que le roi notre seigneur se réchauffe.
1:3	On chercha donc dans toutes les contrées d'Israël une jeune et belle fille, et on trouva Abishag, la Shounamite, que l'on amena auprès du roi.
1:4	Cette jeune femme était très belle. Elle prit soin du roi et le servit, mais le roi ne la connut pas.

### Conspiration d'Adoniyah pour régner sur Israël

1:5	Alors Adoniyah, fils de Haggith, se laissa emporter par l'orgueil, en disant : Je suis le roi ! Il se procura un char, des cavaliers et 50 hommes qui couraient devant lui.
1:6	Or son père ne voulait jamais le chagriner de son temps, en disant : Pourquoi agis-tu ainsi ? Il était très beau d'apparence, il était né après Absalom.
1:7	Il eut des pourparlers avec Yoab fils de Tserouyah et avec le prêtre Abiathar, et ils aidèrent Adoniyah et le suivirent.
1:8	Mais le prêtre Tsadok, Benayah fils de Yehoyada, Nathan le prophète, Shimeï, Reï et les vaillants hommes de David n'étaient pas avec Adoniyah.
1:9	Or Adoniyah fit tuer des brebis, des bœufs et des veaux gras près de la pierre de Zohéleth qui est auprès d'En-Roguel. Il invita tous ses frères, fils du roi, et tous les hommes de Yéhouda qui étaient au service du roi.
1:10	Mais il n'invita pas Nathan le prophète, ni Benayah, ni les vaillants hommes, ni Shelomoh son frère.

### Opposition de Nathan et Bath-Shéba

1:11	Alors Nathan parla à Bath-Shéba, mère de Shelomoh, en disant : N'as-tu pas entendu qu'Adoniyah, fils de Haggith, a été fait roi ? Et David notre seigneur n'en sait rien.
1:12	Maintenant donc viens, je t'en donne le conseil afin que tu sauves ta vie et la vie de ton fils Shelomoh.
1:13	Va, entre chez le roi David, et dis-lui : Ô roi, mon seigneur, n'as-tu pas fait serment à ta servante, en disant : Ton fils Shelomoh régnera après moi et sera assis sur mon trône ? Pourquoi donc Adoniyah règne-t-il ?
1:14	Voici, pendant que tu seras là à parler avec le roi, j'entrerai moi-même après toi et je compléterai ton discours.
1:15	Bath-Shéba se rendit dans la chambre du roi. Or le roi était très vieux et Abishag, la Shounamite, le servait.
1:16	Bath-Shéba s'inclina et se prosterna devant le roi. Et le roi lui dit : Qu'as-tu ?
1:17	Et elle lui répondit : Mon seigneur, tu as juré par YHWH ton Elohîm à ta servante, et tu lui as dit : Certainement ton fils Shelomoh régnera après moi, et sera assis sur mon trône.
1:18	Mais maintenant voici, Adoniyah est proclamé roi ! Et tu ne le sais pas, ô roi, mon seigneur !
1:19	Il a même fait tuer des bœufs, des veaux gras et des brebis en grand nombre, il a convié tous les fils du roi, avec Abiathar, le prêtre, et Yoab, chef de l'armée, mais il n'a pas convié ton serviteur Shelomoh.
1:20	Or quant à toi ô roi, mon seigneur ! les yeux de tout Israël sont sur toi afin que tu lui fasses connaître qui s'assiéra sur le trône du roi mon seigneur après lui.
1:21	Autrement il arrivera qu'aussitôt que le roi mon seigneur sera endormi avec ses pères, nous serons traités comme coupables, moi et mon fils Shelomoh.
1:22	Et comme elle parlait encore avec le roi, voici, Nathan, le prophète, entra.
1:23	On l'annonça au roi, en disant : Voici Nathan, le prophète ! Il se présenta devant le roi et se prosterna devant lui, le visage contre terre.
1:24	Et Nathan dit : Ô roi, mon seigneur ! As-tu dit : Adoniyah régnera après moi et sera assis sur mon trône ?
1:25	Car il est descendu aujourd'hui, il a sacrifié des bœufs, des veaux gras et des brebis en grand nombre. Il a convié tous les fils du roi, les chefs de l'armée et le prêtre Abiathar. Et voici, ils mangent et boivent devant lui ; ils disent : Vive le roi Adoniyah !
1:26	Mais il n'a invité ni moi, ton serviteur, ni le prêtre Tsadok, ni Benayah, fils de Yehoyada, ni Shelomoh, ton serviteur.
1:27	Ceci aurait-il été fait par le roi mon seigneur, sans que tu aies fait connaître à ton serviteur, quel est celui qui doit s'asseoir sur le trône du roi mon seigneur après lui ?
1:28	Et le roi David répondit, en disant : Appelez-moi Bath-Shéba ! Elle se présenta devant le roi et se tint debout devant le roi.
1:29	Alors le roi jura et dit : YHWH, qui m'a délivré de toute détresse, est vivant !
1:30	Comme je te l'ai juré par YHWH, l'Elohîm d'Israël, en disant : C'est ton fils Shelomoh qui régnera après moi, c'est lui qui s'assiéra sur mon trône à ma place, je le ferai en ce jour.
1:31	Alors Bath-Shéba s'inclina le visage contre terre et se prosterna devant le roi, en disant : Que le roi David, mon seigneur, vive éternellement !
1:32	Et le roi David dit : Appelez-moi le prêtre Tsadok, le prophète Nathan et Benayah, fils de Yehoyada ! Et ils se présentèrent devant le roi.
1:33	Le roi leur dit : Prenez avec vous les serviteurs de votre seigneur, faites monter mon fils Shelomoh sur ma mule et faites-le descendre à Guihon.
1:34	Que Tsadok le prêtre et Nathan le prophète l'oignent en ce lieu-là pour roi sur Israël, puis vous sonnerez du shofar et vous direz : Vive le roi Shelomoh !
1:35	Vous monterez après lui et il viendra, il s'assiéra sur mon trône et il régnera à ma place, car j'ai ordonné qu'il soit le chef d'Israël et de Yéhouda.
1:36	Et Benayah fils de Yehoyada répondit au roi : Amen ! Ainsi parle YHWH, l'Elohîm de mon seigneur le roi !
1:37	Comme YHWH a été avec mon seigneur le roi, qu'il soit aussi avec Shelomoh, et qu'il élève son trône encore plus que le trône du roi David, mon seigneur !

### Shelomoh oint roi d'Israël par Tsadok<!--Cp. 1 Ch. 29:22.-->

1:38	Puis Tsadok le prêtre descendit avec Nathan le prophète et Benayah, fils de Yehoyada, les Kéréthiens et les Péléthiens. Ils firent monter Shelomoh sur la mule du roi David et le menèrent à Guihon.
1:39	Tsadok, le prêtre, prit du tabernacle une corne d'huile et il oignit Shelomoh. On sonna du shofar et tout le peuple dit : Vive le roi Shelomoh !
1:40	Et tout le monde monta après lui, et le peuple jouait de la flûte en se livrant à une grande joie, au point que la terre se fendait par leurs cris.
1:41	Or Adoniyah et tous les invités qui étaient avec lui entendirent ce bruit, comme ils achevaient de manger. Yoab aussi, entendit le son du shofar, et dit : Que veut dire ce bruit de la ville en tumulte ?
1:42	Et comme il parlait encore, voici Yonathan, fils du prêtre Abiathar, arriva et Adoniyah lui dit : Viens, car tu es un homme talentueux et tu apportes de bonnes nouvelles.
1:43	Oui ! répondit Yonathan à Adoniyah, Le roi David, notre seigneur, a établi Shelomoh roi.
1:44	Et le roi a envoyé avec lui Tsadok le prêtre, Nathan, le prophète, Benayah, fils de Yehoyada, les Kéréthiens et les Péléthiens, et ils l'ont fait monter sur la mule du roi.
1:45	Tsadok, le prêtre, et Nathan, le prophète, l'ont oint pour roi à Guihon, d'où ils sont remontés avec joie, et la ville s'agite. C'est là le bruit que vous avez entendu.
1:46	Shelomoh s'est même assis sur le trône royal.
1:47	Et les serviteurs du roi sont venus pour bénir le roi David, notre seigneur, en disant : Que ton Elohîm rende le nom de Shelomoh encore plus grand que ton nom et qu'il élève son trône encore plus que ton trône ! Et le roi s'est prosterné sur son lit.
1:48	Et voici ce que le roi a dit : Béni soit YHWH, l'Elohîm d'Israël, qui a aujourd'hui établi sur mon trône un successeur et qui m'a permis de le voir !
1:49	Alors, tous les invités d'Adoniyah furent saisis de frayeur ; ils se levèrent et s'en allèrent chacun son chemin.
1:50	Adoniyah eut peur de Shelomoh ; il se leva aussi et s'en alla empoigner les cornes de l'autel.
1:51	On vint l'apprendre à Shelomoh, en disant : Voici Adoniyah a peur du roi Shelomoh et il a saisi les cornes de l'autel, en disant : Que le roi Shelomoh me jure aujourd'hui qu'il ne fera pas mourir son serviteur par l'épée.
1:52	Et Shelomoh dit : S'il devient un fils talentueux, il ne tombera pas un seul de ses cheveux à terre, mais s'il se trouve du mal en lui, il mourra.
1:53	Alors, le roi Shelomoh envoya des personnes qui le firent descendre de l'autel. Il vint et se prosterna devant le roi Shelomoh, et Shelomoh lui dit : Va dans ta maison.

## Chapitre 2

### Dernières paroles de David à Shelomoh

2:1	David approchait du moment de sa mort et il donna ses ordres à Shelomoh son fils, en disant :
2:2	Je m'en vais par le chemin de toute la Terre, fortifie-toi et sois un homme !
2:3	Garde les commandements de YHWH, ton Elohîm, en marchant dans ses voies, en gardant ses statuts, ses commandements, ses ordonnances et ses préceptes, selon ce qui est écrit dans la torah de Moshè. Ainsi tu réussiras dans tout ce que tu feras et dans tout ce que tu entreprendras,
2:4	et YHWH accomplira la parole qu'il m'a donnée, en disant : Si tes fils prennent garde à leur voie, pour marcher devant moi dans la vérité, de tout leur cœur et de toute leur âme, tu ne manqueras jamais de successeur sur le trône d'Israël.
2:5	Au reste, tu sais ce que m'a fait Yoab, fils de Tserouyah et ce qu'il a fait aux deux chefs des armées d'Israël, Abner, fils de Ner et à Amasa, fils de Yether, qu'il a tués en versant pendant la paix, le sang qu'on verse en temps de guerre. Il a mis de ce sang sur la ceinture qu'il avait sur ses reins et sur les sandales qu'il avait aux pieds.
2:6	Tu agiras selon ta sagesse, en sorte que tu ne laisseras pas ses cheveux blancs descendre en paix dans le shéol.
2:7	Tu traiteras avec bonté les fils de Barzillaï, le Galaadite, et ils seront du nombre de ceux qui mangent à ta table. Car ils se sont approchés de moi quand je fuyais de devant Absalom ton frère.
2:8	Voici, tu as avec toi Shimeï, fils de Guéra, le Benyamite de Bahourim, qui proféra contre moi des malédictions violentes, le jour où je m'en allais à Mahanaïm. Mais il descendit au-devant de moi vers le Yarden<!--Jourdain.--> et je lui jurai par YHWH, en disant : Je ne te ferai pas mourir par l'épée.
2:9	Maintenant donc, tu ne le laisseras pas impuni, car tu es un homme sage et tu sais comment tu dois le traiter pour faire descendre dans le sang ses cheveux blancs au shéol.

### Mort de David ; début du règne de Shelomoh<!--1 Ch. 29:23-30.-->

2:10	Ainsi, David se coucha avec ses pères, il fut enseveli dans la cité de David.
2:11	Et le temps que David régna sur Israël fut de 40 ans. Il régna 7 ans à Hébron et il régna 33 ans à Yeroushalaim.
2:12	Et Shelomoh s'assit sur le trône de David, son père, et son règne fut très affermi.

### Mort d'Adoniyah

2:13	Alors Adoniyah, fils de Haggith, vint vers Bath-Shéba, mère de Shelomoh, et elle dit : Viens-tu dans une intention de paix ? Et il répondit : Dans une intention de paix.
2:14	Il ajouta : J'ai un mot à te dire. Elle répondit : Parle !
2:15	Et il dit : Tu sais bien que le royaume m'appartenait et que tout Israël s'attendait à ce que je règne. Mais la royauté s'est détournée de moi, elle est échue à mon frère parce que YHWH la lui a donnée.
2:16	Maintenant donc, je te demande une chose, ne me la refuse pas. Elle lui répondit : Parle !
2:17	Et il dit : Je te prie, dis au roi Shelomoh, car il ne te refusera rien, qu'il me donne Abishag, la Shounamite, pour femme.
2:18	Bath-Shéba répondit : Et bien, je parlerai pour toi au roi.
2:19	Bath-Shéba se rendit auprès du roi Shelomoh pour lui parler en faveur d'Adoniyah ; et le roi se leva pour aller au-devant d'elle, il se prosterna devant elle, puis il s'assit sur son trône. On plaça un siège pour la mère du roi et elle s'assit à sa droite.
2:20	Elle dit alors : J'ai une petite demande à te faire : ne me la refuse pas ! Et le roi lui répondit : Demande ma mère car je ne te la refuserai pas.
2:21	Et elle dit : Qu'on donne Abishag, la Shounamite, pour femme à Adoniyah, ton frère.
2:22	Mais le roi Shelomoh répondit à sa mère et dit : Et pourquoi demandes-tu Abishag, la Shounamite, pour Adoniyah ? Demande plutôt le royaume pour lui, parce qu'il est mon frère aîné. Demande-le pour lui, pour Abiathar, le prêtre, et pour Yoab, fils de Tserouyah !
2:23	Alors le roi Shelomoh jura par YHWH, en disant : Qu'ainsi me traite Elohîm et qu'ainsi il y ajoute si ce n'est pas contre son âme même qu'Adoniyah a déclaré cette parole !
2:24	Maintenant YHWH est vivant, lui qui m'a établi, qui m'a fait asseoir sur le trône de David, mon père, et qui m'a donné une maison, selon sa promesse ! Aujourd'hui Adoniyah mourra.
2:25	Et le roi Shelomoh envoya Benayah, fils de Yehoyada, qui le frappa, et Adoniyah mourut.

### Abiathar dépouillé de ses fonctions au temple

2:26	Puis le roi dit à Abiathar, le prêtre : Va-t-en à Anathoth sur tes terres, car tu es un homme digne de mort, mais je ne te ferai pas mourir aujourd'hui, car tu as porté l'arche d'Adonaï YHWH devant David, mon père et tu as eu part à toutes les afflictions de mon père.
2:27	Shelomoh chassa Abiathar pour qu'il ne fût plus prêtre de YHWH, accomplissant ainsi la parole que YHWH avait déclarée sur la maison d'Éli<!--1 S. 3:11-14.--> à Shiyloh.

### Mort de Yoab ; Benayah à la tête de l'armée

2:28	Cette nouvelle arriva à Yoab, car Yoab s'était détourné après Adoniyah, mais ne s'était pas détourné après Absalom, et Yoab s'enfuit vers la tente de YHWH et saisit les cornes de l'autel.
2:29	On alla l'apprendre au roi Shelomoh, en disant : Yoab s'en est enfui dans la tente de YHWH et il est auprès de l'autel. Shelomoh envoya Benayah, fils de Yehoyada, et lui dit : Va et frappe-le.
2:30	Benayah entra dans la tente de YHWH et dit à Yoab : Ainsi a parlé le roi : Sors de là ! Mais il répondit : Non ! Je veux mourir ici. Et Benayah rapporta la chose au roi, en disant : Yoab m'a parlé ainsi et c'est ainsi qu'il m'a répondu.
2:31	Et le roi dit à Benayah : Fais comme il t'a dit, frappe-le et enterre-le. Tu détourneras ainsi de moi et de la maison de mon père le sang que Yoab a versé sans cause.
2:32	Et YHWH fera retomber son sang sur sa tête, car il a frappé deux hommes plus justes et meilleurs que lui et les a tués par l'épée, sans que mon père David le sache : Abner, fils de Ner, chef de l'armée d'Israël et Amasa, fils de Yether, chef de l'armée de Yéhouda.
2:33	Leur sang retombera sur la tête de Yoab et sur la tête de sa postérité à perpétuité, mais il y aura paix à toujours de par YHWH, pour David, pour sa postérité, pour sa maison et pour son trône.
2:34	Et Benayah, fils de Yehoyada, monta et frappa Yoab à mort. On l'ensevelit dans sa maison, dans le désert.
2:35	Alors le roi établit Benayah, fils de Yehoyada, sur l'armée à la place de Yoab. Le roi établit aussi Tsadok prêtre à la place d'Abiathar.

### Mort de Shimeï

2:36	Puis le roi fit appeler Shimeï et lui dit : Bâtis-toi une maison à Yeroushalaim et demeures-y, et n'en sors pas pour aller de côté et d'autre.
2:37	Car sache que le jour où tu en sortiras et que tu passeras le torrent de Cédron tu mourras, tu mourras. Ton sang sera sur ta tête.
2:38	Et Shimeï répondit au roi : Cette parole est bonne ! Ton serviteur fera tout ce que le roi mon seigneur a dit. Ainsi Shimeï demeura de nombreux jours à Yeroushalaim.
2:39	Mais il arriva qu'au bout de 3 ans, deux serviteurs de Shimeï s'enfuirent vers Akish, fils de Ma'akah, roi de Gath et on le rapporta à Shimeï, en disant : Voilà tes serviteurs sont à Gath.
2:40	Alors Shimeï se leva, sella son âne, et s'en alla à Gath vers Akish pour chercher ses serviteurs. Shimeï s'en alla donc et ramena de Gath ses serviteurs.
2:41	On rapporta à Shelomoh que Shimeï était allé de Yeroushalaim à Gath, et qu'il était de retour.
2:42	Et le roi envoya appeler Shimeï et lui dit : Ne t'ai-je pas fait jurer par YHWH, et ne t'ai-je pas fait cette déclaration formelle : Sache, sache que le jour où tu sortiras pour aller de côté et d'autre, tu mourras, tu mourras ? Ne m'as-tu pas alors répondu : La parole que j'ai entendue est bonne ?
2:43	Pourquoi donc n'as-tu pas observé le serment de YHWH et le commandement que je t'avais donné ?
2:44	Le roi dit aussi à Shimeï : Tu sais en ton cœur tout le mal que tu as fait à David, mon père. C'est pourquoi YHWH a fait retomber ta méchanceté sur ta tête.
2:45	Mais le roi Shelomoh sera béni et le trône de David sera affermi devant YHWH à jamais.
2:46	Le roi donna un ordre à Benayah, fils de Yehoyada. Il sortit, se jeta sur lui et il mourut. La royauté fut ainsi affermie entre les mains de Shelomoh.

## Chapitre 3

### Shelomoh s'allie à pharaon

3:1	Or Shelomoh devint le gendre de pharaon, roi d'Égypte. Il prit pour femme la fille de pharaon et l'amena dans la cité de David, jusqu'à ce qu'il eût achevé de bâtir sa maison, la maison de YHWH et la muraille de Yeroushalaim tout autour.
3:2	Seulement le peuple sacrifiait dans les hauts lieux, car jusque-là on n'avait pas bâti de maison au Nom de YHWH.

### Shelomoh demande la sagesse à YHWH<!--2 Ch. 1:2-10.-->

3:3	Shelomoh aimait YHWH, il marchait dans les statuts de David, son père. Seulement, c'était dans les hauts lieux qu'il offrait des sacrifices et des parfums.
3:4	Le roi se rendit à Gabaon pour y sacrifier, car c'était le plus grand des hauts lieux. Et Shelomoh offrit 1 000 holocaustes sur cet autel.
3:5	Et YHWH apparut de nuit à Shelomoh à Gabaon en rêve et Elohîm lui dit : Demande ce que tu veux que je te donne.
3:6	Et Shelomoh répondit : Tu as usé d'une grande bonté envers ton serviteur David, mon père, parce qu'il a marché en ta présence dans la vérité, la justice et la droiture de cœur envers toi. Tu as gardé cette grande bonté envers lui en lui donnant un fils qui est assis sur son trône, comme on le voit aujourd'hui.
3:7	Or maintenant, ô YHWH, mon Elohîm ! Tu as fait régner ton serviteur à la place de David, mon père, et je ne suis qu'un jeune homme, je ne sais pas sortir et entrer.
3:8	Ton serviteur est parmi ce peuple que tu as choisi, un peuple nombreux qui ne peut être compté ni dénombré à cause de sa multitude.
3:9	Accorde donc, à ton serviteur un cœur intelligent pour juger ton peuple, pour discerner le bien du mal ! Car qui pourrait juger ce peuple si grand ?

### YHWH exauce Shelomoh<!--2 Ch. 1:11-13.-->

3:10	Et la chose fut bonne aux yeux d'Adonaï, parce que Shelomoh avait demandé cette chose.
3:11	Et Elohîm lui dit : Puisque c'est là, ta demande et que tu n'as pas demandé une longue vie, ni les richesses, ni la mort de tes ennemis, mais que tu as demandé de l'intelligence pour rendre justice,
3:12	voici, j'agis selon ta parole. Voici, je te donne un cœur sage et intelligent, de sorte qu'il n'y aura eu personne de semblable avant toi et qu'il n'y en aura jamais de semblable après toi.
3:13	Et même, je te donne ce que tu n'as pas demandé, les richesses et la gloire, de sorte qu'il n'y aura pas de roi semblable à toi parmi les rois tant que tu vivras.
3:14	Et si tu marches dans mes voies pour garder mes ordonnances et mes commandements, comme David, ton père, je prolongerai tes jours.
3:15	Shelomoh se réveilla, et voilà que c'était un rêve. Puis il s'en retourna à Yeroushalaim et se tint devant l'arche de l'alliance de YHWH. Là, il offrit des holocaustes et des offrandes de paix<!--Voir commentaire en Lé. 3:1.--> et fit un festin à tous ses serviteurs.
3:16	Alors deux femmes prostituées vinrent chez le roi et se présentèrent devant lui.
3:17	Et l'une de ces femmes dit : Excusez-moi, mon Seigneur ! Nous demeurions cette femme et moi dans une même maison et j'ai accouché près d'elle dans cette maison.
3:18	Et il arriva, trois jours après, que cette femme accoucha aussi. Nous étions ensemble, il n'y a aucun étranger avec nous dans la maison, il n'y a que nous deux dans la maison.
3:19	Or l'enfant de cette femme est mort la nuit, parce qu'elle s'était couchée sur lui.
3:20	Elle s'est levée au milieu de la nuit, et a pris mon fils à mes côtés pendant que ta servante dormait, et l'a couché dans son sein. Et son fils mort, elle l'a couché dans mon sein.
3:21	Le matin, je me suis levée pour allaiter mon fils. Et voici, il était mort. Je l'ai regardé attentivement ce matin-là ; et voici, ce n'était pas mon fils que j'avais enfanté.
3:22	L'autre femme dit : Non, c'est mon fils qui est vivant et c'est ton fils qui est mort. Mais la première répliqua : Nullement ! Celui qui est mort est ton fils, et c'est mon fils qui vit. Elles parlaient ainsi devant le roi.
3:23	Et le roi dit : L'une dit : C'est mon fils qui est vivant et c'est ton fils qui est mort, l'autre dit : Nullement ! C'est ton fils qui est mort, et c'est mon fils qui est vivant.
3:24	Alors le roi dit : Apportez-moi une épée ! Et on apporta une épée devant le roi.
3:25	Puis le roi dit : Partagez en deux l'enfant qui vit, et donnez-en la moitié à l'une et la moitié à l'autre.
3:26	Alors la femme dont le fils était vivant sentit ses entrailles s'émouvoir pour son fils et elle dit au roi : Excusez-moi, mon seigneur ! Donnez-lui l'enfant qui vit, ne le tuez pas, ne le tuez pas ! Mais l'autre dit : Il ne sera ni à moi ni à toi ; coupez-le !
3:27	Alors le roi répondit et dit : Donnez à la première l'enfant qui vit, et ne le tuez pas, ne le tuez pas. C'est elle qui est sa mère.
3:28	Tout Israël entendit parler du jugement que le roi avait prononcé. Et l'on craignit le roi, car on vit que la sagesse divine était en lui pour rendre la justice.

## Chapitre 4

### Shelomoh établit onze chefs et douze intendants

4:1	Le roi Shelomoh devint roi sur tout Israël.
4:2	Voici les chefs qu'il avait à son service : Azaryah, fils du prêtre Tsadok.
4:3	Élihoreph et Achiyah, enfants de Shisha, secrétaires. Yehoshaphat, fils d'Ahiloud, archiviste.
4:4	Benayah, fils de Yehoyada, commandait l'armée. Tsadok et Abiathar étaient prêtres.
4:5	Azaryah, fils de Nathan, était chef des intendants. Zaboud, fils de Nathan, était prêtre, favori du roi.
4:6	Achishar, chef de la maison du roi. Adoniram, fils d'Abda, préposé sur les impôts.
4:7	Shelomoh avait douze intendants établis sur tout Israël. Ils pourvoyaient à l'entretien du roi et de sa maison, chacun d'eux était chargé de cet entretien pendant un mois de l'année.
4:8	Voici leurs noms : le fils de Hour, sur la Montagne d'Éphraïm.
4:9	Le fils de Déker, sur Makats, sur Saalbim, sur Beth-Shémesh, à Élon de Beth-Hanan.
4:10	Le fils de Hésed, à Aroubboth. Il avait Soco et tout le pays de Hépher.
4:11	Le fils d'Abinadab avait toute la contrée de Dor. Il avait Thaphath, fille de Shelomoh, pour femme.
4:12	Baana, fils d'Ahiloud, avait Thaanac et Meguiddo, et tout le pays de Beth-Shean qui est près de Tsarthan au-dessous de Yizre`e'l, depuis Beth-Shean jusqu'à Abel-Mehola et jusqu'au-delà de Yoqme`am.
4:13	Le fils de Guéber, à Ramoth en Galaad. Il avait les villages de Yaïr, fils de Menashè, en Galaad. Il avait aussi toute la contrée d'Argob en Bashân, 60 grandes villes à murailles et garnies de barres de cuivre.
4:14	Achinadab, fils d'Iddo, à Mahanaïm.
4:15	Achimaats, qui avait pour femme Basmath, fille de Shelomoh, en Nephthali.
4:16	Baana, fils de Houshaï, en Asher et sur Bealoth.
4:17	Yehoshaphat, fils de Parouah, à Yissakar.
4:18	Shimeï, fils d'Élah, en Benyamin.
4:19	Guéber, fils d'Ouri, dans le pays de Galaad, le pays de Sihon, roi des Amoréens, et d'Og, roi de Bashân. Il y avait un seul intendant pour ce pays.

### L'étendue de la domination du royaume

4:20	Yéhouda et Israël étaient en grand nombre, semblable au sable sur le bord de la mer. Ils mangeaient, buvaient et se réjouissaient.
4:21	Et Shelomoh dominait sur tous les royaumes depuis le fleuve jusqu'au pays des Philistins et jusqu'à la frontière d'Égypte. Ils apportaient des présents et lui furent assujettis pendant toute sa vie.
4:22	Or les vivres de Shelomoh pour chaque jour étaient de 30 cors de fine farine et 60 d'autre farine,
4:23	10 bœufs gras, 20 bœufs de pâturages et 100 moutons, outre les cerfs, les gazelles, les daims et les volailles engraissées.
4:24	Il dominait sur toutes les régions de l'autre côté du fleuve, depuis Thiphsach jusqu'à Gaza, et tous les rois des régions de l'autre côté du fleuve lui étaient soumis. Il était en paix avec tous ses alentours, de tous côtés.
4:25	Yéhouda et Israël habitèrent en sécurité chacun sous sa vigne et sous son figuier, depuis Dan jusqu'à Beer-Shéba, durant toute la vie de Shelomoh.
4:26	Shelomoh avait aussi 40 000 crèches pour les chevaux destinés à ses chars et 12 000 cavaliers.
4:27	Or les intendants pourvoyaient à l'entretien du roi Shelomoh et de tous ceux qui s'approchaient de sa table, chacun en son mois ; ils ne les laissaient manquer de rien.
4:28	Ils faisaient aussi venir de l'orge et de la paille pour les chevaux et les coursiers dans le lieu où se trouvait le roi, chacun selon les ordres qu'il avait reçus.

### La sagesse de Shelomoh connue de toute la Terre

4:29	Elohîm donna à Shelomoh de la sagesse, une très grande intelligence, et des connaissances multipliées comme le sable qui est sur le bord de la mer.
4:30	La sagesse de Shelomoh surpassait la sagesse de tous les fils de l'orient et toute la sagesse des Égyptiens.
4:31	Il était plus sage que tout être humain, plus que Éthan, l'Ezrachite, plus qu'Héman, Calcol et Darda, les fils de Machol. Et sa renommée était répandue parmi toutes les nations d'alentour.
4:32	Il a prononcé 3 000 paraboles<!--Ou proverbes.--> et composa 1 005 cantiques.
4:33	Il a aussi parlé des arbres, depuis le cèdre du Liban jusqu'à l'hysope qui sort de la muraille. Il a aussi parlé sur les animaux, sur les oiseaux, sur les reptiles et sur les poissons.
4:34	Et de tous les peuples, on venait pour entendre la sagesse de Shelomoh, de la part de tous les rois de la Terre qui avaient entendu parler de sa sagesse.

## Chapitre 5

### Shelomoh prépare la construction du temple<!--2 Ch. 2:1, 13:16.-->

5:1	Hiram, roi de Tyr, envoya ses serviteurs vers Shelomoh, car il apprit qu'on l'avait oint pour roi à la place de son père, car Hiram avait toujours aimé David.
5:2	Et Shelomoh fit dire à Hiram :
5:3	Tu sais que David, mon père, n'a pu bâtir une maison au Nom de YHWH, son Elohîm, à cause des guerres dont ses ennemis l'entourèrent jusqu'à ce que YHWH les eût mis sous la plante de ses pieds.
5:4	Maintenant YHWH, mon Elohîm, m'a donné du repos de toutes parts et je n'ai plus d'adversaires, plus de calamités !
5:5	Voici donc, j'ai l'intention de bâtir une maison au Nom de YHWH, mon Elohîm, comme YHWH l'a promis à David, mon père, en disant : Ton fils que je mettrai à ta place sur ton trône sera celui qui bâtira une maison à mon Nom.
5:6	Ordonne maintenant que l'on coupe des cèdres du Liban pour moi. Mes serviteurs seront avec les tiens et je donnerai pour tes serviteurs le salaire que tu auras fixé, car tu sais qu'il n'y a personne parmi nous qui sache couper le bois comme les Sidoniens.
5:7	Lorsque Hiram eut entendu les paroles de Shelomoh, il eut une grande joie et il dit : Béni soit aujourd'hui YHWH, qui a donné à David, un fils, sage pour chef de ce grand peuple !
5:8	Hiram fit répondre à Shelomoh : J'ai entendu ce que tu m'as envoyé dire et je ferai tout ce qui te plaira au sujet des bois de cèdre et des bois de cyprès.
5:9	Mes serviteurs les descendront du Liban à la mer, puis je les expédierai sur la mer par radeaux jusqu'au lieu que tu m'auras indiqué. Là, je les ferai délier et tu les prendras. Et tu feras ce que je désire en fournissant des vivres à ma maison.
5:10	Hiram donna du bois de cèdre et du bois de cyprès à Shelomoh autant qu'il en voulait.
5:11	Et Shelomoh donna à Hiram 20 000 cors de froment pour la nourriture de sa maison et 20 cors d'huile d'olives concassées. Voilà ce que Shelomoh donnait à Hiram année par année.
5:12	Et YHWH donna de la sagesse à Shelomoh comme il le lui avait promis. Il y eut paix entre Hiram et Shelomoh, et ils firent alliance ensemble.

### Les hommes de corvée<!--2 Ch. 2:2, 17:18.-->

5:13	Le roi Shelomoh leva sur tout Israël des hommes de corvée. Ils étaient au nombre de 30 000 hommes.
5:14	Il en envoya 10 000 au Liban chaque mois, tour à tour, ils étaient un mois au Liban, et deux mois chez eux. Adoniram était préposé sur les hommes de corvée.
5:15	Shelomoh avait aussi 70 000 hommes qui portaient les fardeaux et 80 000 qui taillaient les pierres dans la montagne,
5:16	sans compter les chefs au nombre de 3 300, préposés par Shelomoh sur le suivi des travaux et chargés de surveiller ceux qui faisaient les travaux.
5:17	Le roi ordonna d'extraire de grandes pierres, des pierres précieuses, des pierres de taille pour servir de fondements à la maison.
5:18	De sorte que les maçons de Shelomoh et ceux d'Hiram, taillèrent les pierres et préparèrent le bois et les pierres pour bâtir la maison.

## Chapitre 6

### Construction du temple de YHWH<!--2 Ch. 3:1-14.-->

6:1	Et il arriva qu'en l'année, qu'en l'année 480 après la sortie des enfants d'Israël du pays d'Égypte, Shelomoh bâtit la maison de YHWH<!--Voir les annexes « Le temple de Shelomoh ».-->, la quatrième année du règne de Shelomoh sur Israël, au mois de Ziv, qui est le second mois.
6:2	La maison que le roi Shelomoh bâtit à YHWH avait 60 coudées de long, 20 de large, et 30 de haut.
6:3	Le portique devant le temple de la maison avait 20 coudées de longueur, répondant à la largeur de la maison, et il avait 10 coudées de profondeur sur le devant de la maison.
6:4	Il fit placer des fenêtres à la maison, fenêtres solidement grillées.
6:5	Il bâtit contre la muraille de la maison, à l'entour, des étages qui entouraient les murs de la maison, le temple et le lieu très-saint, ainsi il fit des chambres latérales tout autour.
6:6	L'étage inférieur était large de 5 coudées, celui du milieu de 6 coudées et le troisième de 7 coudées. Car il avait aménagé des retraites à la maison tout autour en dehors, afin que la charpente n'entrât pas dans les murailles de la maison.
6:7	Pour bâtir la maison, on se servit de pierres entièrement taillées dans les carrières, de sorte qu'en bâtissant la maison, on n'entendit ni marteau, ni hache, ni aucun outil de fer.
6:8	L'entrée des chambres de l'étage inférieur était au côté droit de la maison, et on montait à l'étage du milieu par un escalier tournant, et de l'étage du milieu au troisième.
6:9	Après avoir achevé de bâtir la maison, Shelomoh couvrit la maison de planches et de poutres de cèdre.
6:10	Et il bâtit les étages joignant toute la maison, avec chacun 5 coudées de haut, et il les lia à la maison par des bois de cèdre.
6:11	Alors la parole de YHWH vint à Shelomoh en ces termes :
6:12	Quant à cette maison que tu bâtis, si tu marches dans mes statuts, si tu pratiques mes ordonnances et que tu gardes tous mes commandements pour y marcher, j'accomplirai en ta faveur la parole que j'ai dite à David ton père.
6:13	J'habiterai au milieu des enfants d'Israël et je n'abandonnerai pas mon peuple d'Israël.
6:14	Ainsi Shelomoh bâtit la maison et l'acheva.
6:15	Il revêtit de cèdre les murs de la maison, depuis le sol jusqu'au plafond. Il revêtit ainsi de bois l'intérieur et il couvrit le sol de la maison de planches de cyprès.
6:16	Il revêtit aussi l'espace de 20 coudées de planches de cèdre à partir du fond de la maison, depuis le sol jusqu'au haut des murailles, et il bâtit cet espace au-dedans pour en faire le lieu très-saint, le Saint des saints.
6:17	Les 40 coudées sur le devant formaient la maison, le temple.
6:18	Le bois de cèdre à l'intérieur de la maison était sculpté en coloquintes et en fleurs épanouies. Tout l'intérieur était de cèdre, on ne voyait aucune pierre.
6:19	Shelomoh disposa aussi le lieu très-saint, au-dedans de la maison vers le fond, pour y mettre l'arche de l'alliance de YHWH.
6:20	Le lieu très-saint avait par-devant 20 coudées de long, 20 coudées de large et 20 coudées de haut, et on le couvrit d'or pur. On en couvrit aussi l'autel, fait de planches de bois de cèdre.
6:21	Shelomoh couvrit d'or pur l'intérieur de la maison et il fit passer un voile avec des chaînes d'or devant le lieu très-saint qu'il couvrit également d'or.
6:22	Ainsi, il couvrit d'or la maison tout entière. Il couvrit aussi d'or tout l'autel qui était devant le lieu très-saint.
6:23	Et il fit dans le lieu très-saint deux chérubins de bois d'olivier sauvage, qui avaient chacun 10 coudées de haut.
6:24	Chacune des ailes de l'un des chérubins avait 5 coudées et les ailes de l'autre chérubin avaient aussi 5 coudées. Depuis le bout d'une aile jusqu'au bout de l'autre aile, il y avait donc 10 coudées.
6:25	Le second chérubin était aussi de 10 coudées. Les deux chérubins étaient d'une même mesure et taillés l'un comme l'autre.
6:26	La hauteur d'un chérubin était de 10 coudées, et l'autre chérubin était de même.
6:27	Shelomoh plaça les chérubins à l'intérieur, au milieu de la maison. Les ailes des chérubins étaient déployées : l'aile de l'un touchait à l'un des murs, l'aile de l'autre chérubin touchait à l'autre mur. Leurs autres ailes se rencontraient par l'extrémité au milieu de la maison.
6:28	Shelomoh couvrit d'or les chérubins.
6:29	Il fit sculpter sur tout le pourtour des murs de la maison, à l'intérieur et à l'extérieur, des sculptures en relief de chérubins, des palmes et des fleurs épanouies.
6:30	Il couvrit aussi d'or le sol de la maison, tant à l'intérieur qu'au-dehors.
6:31	À l'entrée du lieu très-saint, il fit une porte à deux battants de bois d'olivier sauvage, dont les linteaux avec les poteaux équivalaient à un cinquième du mur.
6:32	Les deux battants étaient de bois d'olivier sauvage. Il y fit sculpter des chérubins, des palmes et des fleurs épanouies qu'il couvrit d'or, étendant également l'or sur les chérubins et sur les palmes.
6:33	Il fit aussi, à l'entrée du temple, des poteaux de bois d'olivier sauvage, du quart de la dimension du mur.
6:34	Les deux battants étaient de bois de cyprès : un battant était formé de deux planches brisées, et le second battant de deux planches brisées.
6:35	Il y fit sculpter des chérubins, des palmes et des fleurs épanouies, et les couvrit d'or, proprement posé sur la sculpture.
6:36	Il bâtit aussi le parvis de l'intérieur de trois rangées de pierres de taille et d'une rangée de poutres de cèdre.
6:37	La quatrième année, au mois de Ziv, les fondements de la maison de YHWH furent posés.
6:38	Et la onzième année, au mois de Boul, qui est le huitième mois, la maison fut achevée dans toutes ses parties et telle qu'elle devait être. Shelomoh la construisit en l'espace de 7 années.

## Chapitre 7

### Construction du palais royal

7:1	Shelomoh bâtit aussi sa maison et l'acheva complètement en 13 ans.
7:2	Il bâtit d'abord la maison de la forêt du Liban, de 100 coudées de long, de 50 coudées de large, et de 30 coudées de haut, sur quatre rangées de colonnes de cèdre et sur les colonnes il y avait des poutres de cèdre.
7:3	On couvrit de bois de cèdre les chambres qui portaient sur les colonnes qui étaient au nombre de 45, 15 par étages.
7:4	Et il y avait trois rangées de fenêtres à cadres, chaque fenêtre de ces trois rangées faisait face à une autre fenêtre.
7:5	Toutes les portes et tous les poteaux étaient formés de poutres carrées, avec les fenêtres. À chacun des trois étages, les ouvertures étaient en vis-à-vis les unes des autres.
7:6	Il fit aussi le portique des colonnes, long de 50 coudées, et large de 30 coudées ; et un autre portique en avant avec des colonnes et des degrés sur leur front.
7:7	Il fit aussi le portique du trône sur lequel il rendait ses jugements, appelé le portique du jugement. On le couvrit de cèdre depuis un bout du sol jusqu'à l'autre.
7:8	La maison où il demeurait fut construite de la même manière, dans une autre cour, derrière le portique. Shelomoh fit une maison bâtie comme ce portique à la fille de pharaon, qu'il avait prise pour femme.
7:9	Toutes ces constructions étaient de pierres de prix, taillées d'après des mesures, sciées à la scie, en dedans et en dehors, depuis les fondements jusqu'aux corniches, et par dehors jusqu'au grand parvis.
7:10	Le fondement était en pierres magnifiques et de grand prix, de grandes pierres, des pierres de 10 coudées et des pierres de 8 coudées.
7:11	Et par-dessus il y avait des pierres de prix, taillées d'après des mesures, et du bois de cèdre.
7:12	Et le grand parvis avait aussi tout alentour trois rangées de pierres de taille et une rangée de poutres de cèdre, comme le parvis intérieur de la maison de YHWH, et le portique de la maison.

### Hiram, artisan spécialiste en cuivre<!--2 Ch. 2:12-13.-->

7:13	Or le roi Shelomoh fit venir de Tyr Hiram.
7:14	C'était le fils d'une femme veuve de la tribu de Nephtali et d'un père Tyrien, qui travaillait le cuivre. Il était rempli de sagesse, d'intelligence et de connaissance pour faire toutes sortes d'ouvrages de cuivre. Il arriva auprès du roi Shelomoh et il fit tous ses ouvrages.

### Les colonnes du temple<!--2 Ch. 3:15-17.-->

7:15	Il fit les deux colonnes de cuivre, la première avait 18 coudées de hauteur. Un cordon de 12 coudées mesurait le tour de la seconde.
7:16	Il fit aussi deux chapiteaux de cuivre fondu pour mettre sur les sommets des colonnes. Le premier chapiteau était de 5 coudées de hauteur, le second était aussi de 5 coudées.
7:17	Il fit des treillis en forme de maillages, des festons façonnés en forme de chaînes, pour les chapiteaux qui étaient sur le sommet des colonnes, 7 pour le premier des chapiteaux, et 7 pour le second.
7:18	Il fit deux rangs de grenades autour de l'un des treillis, pour couvrir le chapiteau qui était sur le sommet d'une des colonnes. Il fit de même pour l'autre chapiteau.
7:19	Les chapiteaux qui étaient sur le sommet des colonnes dans le portique étaient d’un ouvrage de lis de 4 coudées.
7:20	Ces chapiteaux placés sur les deux colonnes étaient entourés de 200 grenades, en haut, depuis le renflement qui était au-delà du treillis. Il y avait aussi 200 grenades, disposées par rangs, autour du second chapiteau.
7:21	Il dressa donc les colonnes au portique du temple. Il dressa la colonne de droite qu'il appela du nom de Yakîn<!--Voir 2 Ch. 3:17.-->. Puis il dressa la colonne de gauche qu'il appela du nom de Boaz.
7:22	Et l'on mit sur le chapiteau des colonnes, l'ouvrage figurant des fleurs de lis. Ainsi l'ouvrage des colonnes fut achevé.

### La mer en métal fondu<!--2 Ch. 4:2-5.-->

7:23	Il fit aussi la mer en métal fondu. Elle avait 10 coudées d'un bord à l'autre, ronde tout autour, avec 5 coudées de haut. Un cordon de 30 coudées en mesurait le tour.
7:24	Au-dessous de son bord, des coloquintes l'environnaient, 10 à chaque coudée, lesquelles faisaient tout le tour de la mer. Il y avait deux rangées de coloquintes, coulées dans la même fonte.
7:25	Elle était posée sur 12 bœufs : 3 tournés vers le nord, 3 tournés vers l'occident, 3 tournés vers le sud et 3 tournés vers l'orient. La mer était sur eux et toute la partie postérieure de leur corps était tournée vers l'intérieur.
7:26	Son épaisseur avait la largeur d'une main, et son bord était comme le travail du bord d'une coupe en fleur de lis. Elle contenait 2 000 baths.

### Les dix bases en cuivre

7:27	Il fit aussi dix bases en cuivre. Chaque base avait 4 coudées de long, 4 coudées de large et 3 coudées de haut.
7:28	Voici comment étaient faites ces bases : elles avaient des bordures et les bordures étaient entre les montants.
7:29	Sur les bordures qui étaient entre les montants il y avait des lions, des bœufs et des chérubins. Et sur les bordures, au-dessus et en dessous des lions et des bœufs, il y avait des guirlandes, ouvrage pendant.
7:30	Chaque base avait quatre roues de cuivre avec des essieux de cuivre. Ses quatre pieds leur servaient de supports. Ces supports étaient fondus au-dessous de la cuve et au-delà de chacun étaient les guirlandes.
7:31	Le couronnement offrait à son intérieur une ouverture avec un prolongement d'une coudée vers le haut. Cette ouverture était ronde, comme l'ouvrage d'un piédestal et elle avait une coudée et demie de largeur. Il s'y trouvait aussi des sculptures ; les bordures étaient carrées, et non arrondies.
7:32	Les quatre roues étaient sous les panneaux et les essieux des roues fixés à la base. Chaque roue était haute d'une coudée et demie.
7:33	Les roues étaient faites comme les roues de chars. Leurs essieux, leurs jantes, leurs rais et leurs moyeux étaient tous de fonte.
7:34	Il y avait aux quatre angles de chaque base, quatre consoles d'une même pièce que la base.
7:35	La partie supérieure de la base se terminait par un cercle d'une demi-coudée de hauteur, et elle avait ses supports et ses bordures de la même pièce.
7:36	Puis, on sculpta sur la surface de ses supports et sur ses bordures, des chérubins, des lions et des palmes, selon les espaces libres, et des guirlandes tout autour.
7:37	Ainsi les dix bases étaient toutes d'une même fonte, d'une même mesure et d'une même forme.

### Les dix cuves en cuivre<!--2 Ch. 4:6.-->

7:38	Il fit aussi 10 cuves en cuivre dont chacune contenait 40 baths, et chaque cuve était de 4 coudées, chaque cuve était sur l'une des dix bases.
7:39	Il mit 5 bases au côté droit de la maison et 5 au côté gauche de la maison. Quant à la mer, il la plaça au côté droit de la maison, vers l'orient du côté sud.

### Totalité de l'œuvre d'Hiram

7:40	Ainsi Hiram fit les cuves, les pelles et les bassins, et il acheva tout l'ouvrage qu'il faisait au roi Shelomoh pour la maison de YHWH :
7:41	deux colonnes avec les deux chapiteaux et leurs bourrelets sur le sommet des colonnes : les deux maillages pour couvrir les deux bourrelets des chapiteaux qui étaient sur le sommet des colonnes ;
7:42	les 400 grenades pour les deux maillages, deux rangs de grenades pour chaque maillage, pour couvrir les deux bourrelets des chapiteaux qui étaient sur les colonnes ;
7:43	les 10 bases et les 10 cuves pour mettre sur les bases,
7:44	la mer avec les 12 bœufs sous la mer,
7:45	les pots, les pelles et les bassins. Tous ces ustensiles que Hiram fit au roi Shelomoh pour la maison de YHWH étaient de cuivre poli.
7:46	Le roi les fit fondre dans la plaine du Yarden, dans un sol argileux, entre Soukkoth et Tsarthan.
7:47	Et Shelomoh ne pesa aucun de ces ustensiles, parce qu'ils étaient en trop grand nombre, de sorte qu'on ne rechercha pas le poids du cuivre.

### Divers ustensiles d'or pour la maison de YHWH

7:48	Shelomoh fit aussi tous les ustensiles pour la maison de YHWH : l'autel en or, et les tables en or, sur lesquelles étaient les pains de proposition,
7:49	les chandeliers en or pur, cinq à droite et cinq à gauche devant le lieu très-saint, avec les fleurs, les lampes et les mouchettes en or,
7:50	les coupes, les couteaux, les bassins, les tasses et les encensoirs en or pur. Les gonds, même des portes de la maison, à l'entrée du Saint des saints, à la porte de la maison et à l'entrée du temple, étaient en or.
7:51	Ainsi fut achevé tout l'ouvrage que le roi Shelomoh fit pour la maison de YHWH. Puis il y fit apporter l'or, l'argent et les ustensiles que David son père avait consacrés et il les mit dans les trésors de la maison de YHWH.

## Chapitre 8

### L'arche de l'alliance placée dans le Saint des saints ; la gloire de YHWH remplit le temple<!--2 Ch. 5:2-14.-->

8:1	Alors le roi Shelomoh, convoqua près de lui à Yeroushalaim les anciens d'Israël, tous les princes des tribus et les chefs de famille des enfants d'Israël, pour transporter l'arche de l'alliance de YHWH de la cité de David, qui est Sion.
8:2	Tous les hommes d'Israël se rassemblèrent auprès du roi Shelomoh, au mois d'Éthanim, qui est le septième mois, pendant la fête.
8:3	Une fois tous les anciens d'Israël arrivés, les prêtres portèrent l'arche.
8:4	Ils transportèrent l'arche de YHWH, la tente d'assignation et tous les ustensiles qui étaient dans le tabernacle. Les prêtres et les Lévites les emportèrent.
8:5	Le roi Shelomoh et toute l'assemblée d'Israël convoquée auprès de lui, se tinrent devant l'arche. Ils sacrifièrent du gros et du menu bétail en si grand nombre, qu'on ne pouvait ni nombrer ni compter.
8:6	Et les prêtres portèrent l'arche de l'alliance de YHWH à sa place, dans le lieu très-saint de la maison, dans le Saint des saints, sous les ailes des chérubins.
8:7	Car les chérubins avaient les ailes étendues sur l'emplacement de l'arche, et ils couvraient l'arche et ses barres par-dessus.
8:8	On avait donné aux barres une longueur telle que leurs extrémités se voyaient du lieu saint devant le lieu très-saint, mais elles ne se voyaient pas du dehors. Elles sont demeurées là jusqu'à ce jour.
8:9	Il n'y avait rien dans l'arche que les deux tablettes de pierre que Moshè y déposa en Horeb, lorsque YHWH fit alliance avec les enfants d'Israël à leur sortie du pays d'Égypte.
8:10	Et il arriva qu'au moment où les prêtres sortirent du lieu saint, la nuée remplit la maison de YHWH.
8:11	De sorte que les prêtres ne pouvaient se tenir debout pour faire le service, à cause de la nuée. La gloire de YHWH remplissait en effet la maison de YHWH.

### Discours de Shelomoh<!--2 Ch. 6:1-11.-->

8:12	Alors Shelomoh dit : YHWH veut habiter dans les ténèbres épaisses !
8:13	J'ai bâti, j'ai bâti une maison pour ta demeure ! Ce sera une demeure, un lieu où tu résideras éternellement.
8:14	Le roi tourna son visage et bénit toute l'assemblée d'Israël. Car toute l'assemblée d'Israël se tenait là debout.
8:15	Et il dit : Béni soit YHWH, l'Elohîm d'Israël, qui a parlé de sa propre bouche à David, mon père, et qui a accompli par sa puissance ce qu'il avait déclaré, en disant :
8:16	Depuis le jour où j'ai fait sortir d'Égypte Israël, mon peuple, je n'ai choisi aucune ville parmi toutes les tribus d'Israël pour y bâtir une maison où serait mon Nom ; mais j'ai choisi David pour qu'il soit au-dessus d'Israël, mon peuple.
8:17	David, mon père, avait à cœur de bâtir une maison au Nom de YHWH, l'Elohîm d'Israël.
8:18	Et YHWH dit à David, mon père : Puisque tu as eu à cœur de bâtir une maison à mon Nom, tu as bien fait d'avoir eu cette intention.
8:19	Néanmoins, tu ne bâtiras pas cette maison, mais ton fils qui sortira de tes entrailles sera celui qui bâtira cette maison à mon Nom.
8:20	YHWH a donc accompli la parole qu'il avait prononcée. Je me suis levé à la place de David, mon père, et me suis assis sur le trône d'Israël, comme YHWH l'avait annoncé, et j'ai bâti cette maison au Nom de YHWH, l'Elohîm d'Israël.
8:21	J'y ai établi ici un lieu pour l'arche, dans laquelle est l'alliance de YHWH, qu'il traita avec nos pères quand il les fit sortir hors du pays d'Égypte.

### Prière de Shelomoh<!--2 Ch. 6:12-42.-->

8:22	Ensuite Shelomoh se tint devant l'autel de YHWH en la présence de toute l'assemblée d'Israël, et étendant ses mains vers les cieux,
8:23	il dit : Ô YHWH, Elohîm d'Israël ! Il n'y a pas d'Elohîm semblable à toi en haut dans les cieux, ni en bas sur la Terre. Tu gardes l'alliance et la miséricorde envers tes serviteurs qui marchent devant ta face de tout leur cœur !
8:24	Ainsi tu as tenu parole à ton serviteur David, mon père, car ce que tu as déclaré de ta bouche, tu l'as accompli en ce jour par ta main puissante.
8:25	Maintenant donc, ô YHWH, Elohîm d'Israël, prête attention à la promesse faite à ton serviteur David, mon père, en lui disant : Tu ne manqueras jamais devant moi d'un successeur assis sur le trône d'Israël, pourvu seulement que tes fils prennent garde à leur voie et qu'ils marchent devant ma face, comme tu y as marché.
8:26	Et maintenant, ô Elohîm d'Israël ! Je te prie, que s'accomplisse la promesse que tu as faite à ton serviteur David, mon père.
8:27	Mais Elohîm habiterait-il vraiment sur la Terre ? Voici, les cieux, même les cieux des cieux ne peuvent te contenir ! Combien moins cette maison que j'ai bâtie !
8:28	Toutefois, ô YHWH, mon Elohîm, sois attentif à la prière que t'adresse ton serviteur et à sa supplication, pour entendre le cri et la prière que ton serviteur fait aujourd'hui devant toi.
8:29	Que tes yeux soient ouverts jour et nuit sur cette maison, sur le lieu dont tu as dit : Là sera mon Nom ! Écoute la prière que ton serviteur fait en ce lieu.
8:30	Daigne exaucer la supplication de ton serviteur et de ton peuple d'Israël lorsqu'ils te prieront en ce lieu ! Exauce du lieu de ta demeure. Des cieux, exauce et pardonne !
8:31	Si quelqu'un pèche contre son prochain et qu'on lui impose un serment pour le faire jurer, et que le serment aura été fait devant ton autel dans cette maison,
8:32	écoute-le des cieux et agis ! Juge tes serviteurs, condamne le coupable en lui rendant selon sa conduite, rends justice à l'innocent et traite-le selon son innocence !
8:33	Quand ton peuple d'Israël sera battu par l'ennemi, pour avoir péché contre toi, s'il revient à toi et rend gloire à ton Nom, en t'adressant des prières et des supplications dans cette maison,
8:34	exauce-le des cieux, et pardonne le péché de ton peuple d'Israël, et ramène-le dans la terre que tu as donnée à leurs pères.
8:35	Quand les cieux seront fermés et qu'il n'y aura pas de pluie, à cause de ses péchés contre toi, s'il te fait une prière en ce lieu-ci, qu'il loue ton Nom, et s'il se détourne de ses péchés, parce que tu l'auras affligé,
8:36	exauce-le des cieux, pardonne le péché de tes serviteurs, et de ton peuple d'Israël, à qui tu enseigneras quel est le chemin par lequel ils doivent marcher, et envoie-leur la pluie sur la terre que tu as donnée à ton peuple pour héritage !
8:37	Quand il y aura dans le pays, la famine, la peste, la sécheresse, la nielle, les sauterelles d'une espèce ou d'une autre, même quand les ennemis assiégeront ton peuple dans son propre pays, quand il y aura un fléau ou une maladie quelconque,
8:38	s'il arrive qu'un humain ou que tout ton peuple d'Israël fasse entendre des prières et des supplications, qu'ils reconnaissent chacun la plaie de son cœur et qu'ils étendent leurs mains vers cette maison,
8:39	alors exauce-le des cieux, du lieu de ta demeure, pardonne et agis. Rends à chacun selon toutes ses voies, toi qui connais le cœur de chacun. En effet, toi seul tu connais le cœur de tous les fils des humains.
8:40	Ainsi ils te craindront toute leur vie dans le pays que tu as donné à nos pères !
8:41	Et même lorsque l'étranger, qui n'est pas de ton peuple d'Israël, viendra d'un pays éloigné à cause de ton Nom,
8:42	(car on saura que ton Nom est grand, ta main puissante et ton bras étendu), quand il viendra prier dans cette maison,
8:43	exauce-le des cieux, du lieu de ta demeure, et agis envers cet étranger selon ce qu'il t'aura demandé, afin que tous les peuples de la Terre connaissent ton Nom pour te craindre, comme ton peuple d'Israël ; et pour connaître que ton Nom est invoqué sur cette maison que j'ai bâtie !
8:44	Quand ton peuple sortira pour combattre son ennemi, par la voie par laquelle tu l'auras envoyé, s'ils prient YHWH en regardant vers cette ville que tu as choisie et vers cette maison que j'ai bâtie à ton Nom,
8:45	exauce des cieux leurs prières et leurs supplications, et fais leur justice !
8:46	Quand ils pécheront contre toi, car il n'y a pas d'être humain qui ne pèche, et que tu seras irrité contre eux et que tu les auras livrés à leurs ennemis, qui les emmènera captifs dans un pays ennemi, lointain ou proche,
8:47	s'ils reviennent à eux-mêmes dans le pays où ils auront été emmenés captifs, s'ils se repentent et te prient dans le pays de ceux qui les auront emmenés captifs, en disant : Nous avons péché, nous avons commis l'iniquité, nous avons agi méchamment ;
8:48	s'ils reviennent à toi de tout leur cœur et de toute leur âme, dans le pays de leurs ennemis, qui les auront emmenés captifs, et s'ils t'adressent leurs prières, les regards tournés vers le pays que tu as donné à leurs pères, vers la ville que tu as choisie, vers la maison que j'ai bâtie à ton Nom,
8:49	exauce des cieux, du lieu de ta demeure, leurs prières et leurs supplications, et fais-leur justice.
8:50	Pardonne à ton peuple qui a péché contre toi, ainsi que toutes leurs transgressions par lesquelles ils ont transgressé contre toi ! Fais qu'ils soient un objet de pitié devant ceux qui les auront emmenés captifs afin qu'ils aient pitié d'eux,
8:51	car ils sont ton peuple et ton héritage, et tu les as fait sortir hors d'Égypte, du milieu d'une fournaise de fer !
8:52	Que tes yeux donc soient ouverts sur la supplication de ton serviteur et celle de ton peuple d'Israël, pour les exaucer dans tout ce pourquoi ils crieront à toi !
8:53	Car tu les as séparés de tous les autres peuples de la Terre pour être ton héritage, comme tu l'as déclaré par Moshè, ton serviteur, quand tu fis sortir nos pères hors d'Égypte, ô Adonaï YHWH !

### Bénédictions et réjouissances<!--2 Ch. 7:4-10.-->

8:54	Et il arriva que, quand Shelomoh eut achevé de faire cette prière et cette supplication à YHWH, il se leva de devant l'autel de YHWH où il était agenouillé et les mains étendues vers les cieux.
8:55	Il se tint debout, et bénit toute l'assemblée d'Israël à grande voix, en disant :
8:56	Béni soit YHWH, qui a donné du repos à son peuple d'Israël, comme il l'avait annoncé ! De toutes les paroles qu'il avait prononcées par le moyen de Moshè, son serviteur, aucune n'est restée sans effet.
8:57	Que YHWH, notre Elohîm, soit avec nous, comme il a été avec nos pères ! Qu'il ne nous abandonne pas et qu'il ne nous délaisse pas,
8:58	mais qu'il incline nos cœurs vers lui, afin que nous marchions dans toutes ses voies, et que nous observions ses commandements, ses statuts et ses ordonnances, qu'il a prescrits à nos pères !
8:59	Que ces paroles par lesquelles j'ai fait supplication à YHWH soient présentes devant YHWH, notre Elohîm, jour et nuit, pour qu'il rende justice à son serviteur et justice à son peuple Israël, selon les affaires jour après jour,
8:60	afin que tous les peuples de la Terre sachent que c'est YHWH qui est Elohîm et qu'il n'y en a pas d'autre !
8:61	Que votre cœur soit intègre envers YHWH, notre Elohîm, comme aujourd'hui, pour marcher dans ses statuts et pour garder ses commandements.
8:62	Le roi et tout Israël avec lui offrirent des sacrifices devant YHWH.
8:63	Shelomoh offrit un sacrifice d'offrande de paix à YHWH : 22 000 bœufs et 120 000 brebis. Ainsi le roi et tous les enfants d'Israël firent la dédicace de la maison de YHWH.
8:64	En ce jour-là, le roi consacra le milieu du parvis, qui est devant la maison de YHWH ; c'est là en effet qu'il offrit les holocaustes, les offrandes et les graisses des sacrifices d'offrandes de paix, parce que l'autel de cuivre qui est devant YHWH, était trop petit pour contenir les holocaustes, les offrandes et les graisses des offrandes de paix.
8:65	Et en ce temps-là, Shelomoh célébra une fête, et tout Israël avec lui. Une grande multitude, venue depuis les environs de Hamath jusqu'au torrent d'Égypte, devant YHWH, notre Elohîm, pendant 7 jours, puis 7 autres jours, soit 14 jours.
8:66	Le huitième jour, il renvoya le peuple. Et ils bénirent le roi, et s'en allèrent dans leurs demeures, en se réjouissant, et le cœur heureux pour tout le bien que YHWH avait fait à David, son serviteur, et à Israël, son peuple.

## Chapitre 9

### YHWH apparaît à Shelomoh une seconde fois<!--2 Ch. 7:11-22.-->

9:1	Et il arriva que, lorsque que Shelomoh eut achevé de bâtir la maison de YHWH, la maison royale, et tout ce qui plaisait à Shelomoh, ce qu'il désirait faire,
9:2	YHWH apparut à Shelomoh une seconde fois, comme il lui était apparu à Gabaon.
9:3	Et YHWH lui dit : J'exauce ta prière, et la supplication que tu as faite devant moi, j'ai sanctifié cette maison que tu as bâtie pour y mettre mon Nom à jamais, et mes yeux et mon cœur seront toujours là.
9:4	Quant à toi, si tu marches en ma présence comme a marché David, ton père, avec intégrité de cœur et avec droiture, en faisant tout ce que je t'ai commandé, et si tu gardes mes statuts et mes ordonnances,
9:5	j'affermirai le trône de ton royaume sur Israël à jamais, comme je l'ai déclaré à David, ton père, en disant : Tu ne manqueras jamais d'un successeur sur le trône d'Israël.
9:6	Mais si vous vous détournez de moi, si vous vous détournez de moi, vous et vos fils, et que vous ne gardiez pas mes commandements, mes statuts que j'ai mis devant vous, et si vous allez servir d'autres elohîm et vous prosterner devant eux,
9:7	je retrancherai Israël de la terre que je lui ai donnée, je rejetterai loin de moi cette maison que j'ai consacrée à mon Nom et Israël sera un sujet de parabole et de raillerie parmi tous les peuples.
9:8	Et si haut placée qu'ait été cette maison, quiconque passera auprès d'elle sera étonné et sifflera. Et on dira : Pourquoi YHWH a-t-il ainsi traité ce pays et cette maison ?
9:9	Et on répondra : Parce qu'ils ont abandonné YHWH, leur Elohîm, qui avait fait sortir leurs pères du pays d'Égypte, qu'ils se sont attachés à d'autres elohîm, se sont prosternés devant eux et les ont servis, voilà pourquoi YHWH a fait venir sur eux tous ces maux.

### Les réalisations de Shelomoh<!--2 Ch. 8:1-18.-->

9:10	Au bout de 20 ans, Shelomoh avait bâti les deux maisons, la maison de YHWH et la maison royale.
9:11	Hiram, roi de Tyr, avait fourni à Shelomoh du bois de cèdre, du bois de cyprès et de l'or, autant qu'il en avait voulu, le roi Shelomoh donna à Hiram 20 villes dans le pays de Galilée.
9:12	Hiram sortit de Tyr pour voir les villes que Shelomoh lui avait données. Mais elles ne parurent pas agréables à ses yeux,
9:13	et il dit : Quelles villes m'as-tu assignées mon frère ? Et il les appela, pays de Kaboul, nom qu'elles ont conservé jusqu'à ce jour.
9:14	Hiram avait aussi envoyé au roi 120 talents d'or.
9:15	Voici ce qui concerne les hommes de corvée que le roi Shelomoh leva pour bâtir la maison de YHWH, sa maison, Millo, la muraille de Yeroushalaim, Hatsor, Meguiddo et Guézer.
9:16	Pharaon, roi d'Égypte, était venu s'emparer de Guézer et l'avait incendiée, il avait tué les Cananéens qui habitaient dans la ville. Puis il la donna en cadeau d'adieu à sa fille, femme de Shelomoh.
9:17	Shelomoh donc bâtit Guézer, et Beth-Horon la basse,
9:18	Baalath et Thadmor, dans le désert qui est au pays,
9:19	toutes les villes servant de magasins et lui appartenant, les villes pour les chars et les villes pour la cavalerie, et les choses désirables de Shelomoh qu'il avait désiré bâtir à Yeroushalaim, au Liban, et dans tout le pays dont il était le souverain.
9:20	Tout le peuple qui était resté des Amoréens, des Héthiens, des Phéréziens, des Héviens et des Yebousiens ne faisaient pas partie des enfants d'Israël,
9:21	leurs descendants qui étaient restés après eux dans le pays et que les fils d'Israël n'avaient pu dévouer par interdit, Shelomoh les leva pour le travail forcé des esclaves jusqu'à ce jour.
9:22	Mais Shelomoh n'employa aucun des fils d'Israël comme esclave, car ils étaient ses hommes de guerre, ses serviteurs, ses chefs, ses officiers, les chefs de ses chars et ses hommes d'armes.
9:23	Les chefs préposés aux travaux par Shelomoh étaient au nombre de 550, lesquels géraient l'intendance des ouvriers.
9:24	La fille de pharaon monta de la cité de David dans la maison que Shelomoh lui avait bâtie. C'est alors qu'il bâtit Millo.
9:25	Trois fois par an, Shelomoh offrait des holocaustes et des offrandes de paix sur l'autel qu'il avait bâti à YHWH, et il brûlait des parfums sur celui qui était devant YHWH. Et il acheva la maison.
9:26	Le roi Shelomoh construisit des navires à Etsyôn-Guéber, près d'Éloth, sur le rivage de la Mer Rouge, au pays d'Édom.
9:27	Et Hiram envoya sur ces navires, auprès des serviteurs de Shelomoh, ses propres serviteurs, des hommes de navires connaissant la mer.
9:28	Ils allèrent en Ophir, et ils prirent de là 420 talents d'or qu'ils apportèrent au roi Shelomoh.

## Chapitre 10

### La reine de Séba chez Shelomoh<!--2 Ch. 9:1-12.-->

10:1	Or la reine de Séba ayant appris la renommée de Shelomoh, à cause du Nom de YHWH, vint l'éprouver par des énigmes.
10:2	Elle entra dans Yeroushalaim avec une suite fort nombreuse, et avec des chameaux qui portaient des aromates, une grande quantité d'or, et des pierres précieuses. Elle se rendit auprès de Shelomoh, et lui parla de tout ce qu'elle avait dans le cœur.
10:3	Shelomoh répondit à toutes ses questions, et il n'y eut aucune parole à laquelle le roi ne put fournir une explication.
10:4	La reine de Séba vit toute la sagesse de Shelomoh et la maison qu'il avait bâtie,
10:5	les mets de sa table, la demeure de ses serviteurs, l'ordre de service, leurs vêtements, ses échansons, et les holocaustes qu'il offrait dans la maison de YHWH.
10:6	Elle en perdit le souffle et dit au roi : Elle était donc vraie, la parole que j'ai entendue dans mon pays, sur tes paroles et sur ta sagesse !
10:7	Je ne croyais pas à ces paroles avant d'être venue et d'avoir vu de mes yeux. Et voici, on ne m'en avait pas rapporté la moitié. Ta sagesse et ta prospérité surpassent la rumeur que j'avais entendue.
10:8	Heureux sont tes gens ! Heureux tes serviteurs qui se tiennent continuellement devant toi, et qui entendent ta sagesse !
10:9	Béni soit YHWH, ton Elohîm, qui a pris plaisir en toi et t'a établi sur le trône d'Israël ! Car YHWH aime Israël pour toujours, il t'a établi roi pour faire droit et justice.
10:10	Puis elle donna au roi 120 talents d'or, une très grande quantité d'aromates et des pierres précieuses. Il n'arriva jamais depuis une aussi grande abondance d'aromates que celle que la reine de Séba donna au roi Shelomoh.
10:11	Et les navires de Hiram, qui amenèrent de l'or d'Ophir, amenèrent aussi d'Ophir une grande quantité de bois de santal et de pierres précieuses.
10:12	Le roi fit des supports de ce bois de santal pour la maison de YHWH et pour la maison royale. Il en fit aussi des harpes et des luths pour les chanteurs. Il n'arriva plus pareil bois de santal et on n'en a plus vu jusqu'à ce jour.
10:13	Le roi Shelomoh donna à la reine de Séba tout ce qu'elle désira et répondit à tout ce qu'elle lui demanda. Il lui fit en outre des présents dignes d'un roi tel que Shelomoh. Puis elle s'en retourna, et alla dans son pays, elle et ses serviteurs.

### Les richesses de Shelomoh<!--2 Ch. 9:13-28.-->

10:14	Le poids de l'or qui revenait à Shelomoh chaque année, était de 666 talents d'or,
10:15	outre ce qui lui revenait des négociants, des hommes qui font du trafic, des marchandises, de tous les rois d'Arabie, et des gouverneurs de ces pays-là.
10:16	Le roi Shelomoh fit aussi 200 grands boucliers d'or battu au marteau, employant 600 sicles d'or pour chaque bouclier,
10:17	et 300 autres boucliers d'or battu au marteau, pour chacun desquels il employa 3 mines d'or. Le roi les mit dans la maison de la forêt du Liban.
10:18	Le roi fit aussi un grand trône en ivoire qu'il couvrit d'or pur.
10:19	Ce trône avait 6 degrés, et la partie supérieure, le haut du trône était arrondi par derrière. Il y avait des accoudoirs de chaque côté du siège et deux lions se tenaient auprès des accoudoirs.
10:20	Il y avait aussi 12 lions sur les six degrés du trône, de part et d'autre. Il ne s'est rien fait de tel dans aucun royaume.
10:21	Toute la vaisselle du buffet du roi Shelomoh était en or et toutes les coupes de la maison de la forêt du Liban étaient en or pur. Il n'y en avait pas en argent : on n'en faisait aucun cas du temps de Shelomoh.
10:22	Car le roi avait en mer des navires de Tarsis avec la flotte d'Hiram. Tous les 3 ans la flotte de Tarsis revenait, apportant de l'or, de l'argent, de l'ivoire, des singes et des paons.
10:23	Le roi Shelomoh fut plus grand que tous les rois de la Terre, tant en richesses qu'en sagesse.
10:24	Tous les habitants de la Terre cherchaient à voir la face de Shelomoh, pour écouter la sagesse qu'Elohîm avait mise en son cœur.
10:25	Et chacun d'eux lui apportait son présent, des vases d'or et d'argent, des vêtements, des armes, des aromates, des chevaux et des mulets. Cela se passait de cette manière d'année en année.
10:26	Shelomoh rassembla ses chars et sa cavalerie. Il y avait 1 400 chars et 12 000 chevaliers, qu'il plaça dans les villes où il tenait ses chars et à Yeroushalaim près du roi.
10:27	Le roi rendit l'argent aussi commun à Yeroushalaim que les pierres, il rendit les cèdres aussi nombreux que les sycomores du Bas-Pays.
10:28	C'est d'Égypte que provenaient les chevaux de Shelomoh. Une caravane de marchands du roi allait les chercher par troupes, à un prix fixe :
10:29	et un char montait et sortait d'Égypte pour 600 sicles d'argent et chaque cheval pour 150 sicles. Ils en amenaient de même avec eux pour tous les rois des Héthiens et pour les rois de Syrie.

## Chapitre 11

### Shelomoh détourne son cœur de YHWH

11:1	Or le roi Shelomoh aima beaucoup de femmes étrangères<!--Voir De. 17:17.-->, outre la fille de pharaon : des Moabites, des Ammonites, des Édomites, des Sidoniennes et des Héthiennes.
11:2	Elles étaient d'entre les nations dont YHWH avait dit aux enfants d'Israël : Vous n'irez pas vers elles, et elles ne viendront pas vers vous. Elles détourneraient certainement vos cœurs pour suivre leurs elohîm. Shelomoh s'attacha à elles et les aima.
11:3	Il eut donc pour femmes 700 princesses et 300 concubines, et ses femmes détournèrent son cœur.
11:4	Et il arriva au temps de la vieillesse de Shelomoh, que ses femmes détournèrent son cœur vers d'autres elohîm, et son cœur ne fut plus tout entier à YHWH son Elohîm comme avait été le cœur de David son père.
11:5	Et Shelomoh alla après Astarté, la divinité des Sidoniens, et après Milcom, l'abomination des Ammonites.
11:6	Ainsi Shelomoh fit ce qui est mal aux yeux de YHWH, et il ne persévéra pas à suivre YHWH, comme David, son père.
11:7	Et Shelomoh bâtit un haut lieu à Kemosh, l'abomination des Moabites, sur la montagne qui est vis-à-vis de Yeroushalaim et à Moloc, l'abomination des fils d'Ammon.
11:8	Il en fit de même pour toutes ses femmes étrangères qui offraient des parfums et des sacrifices à leurs elohîm.
11:9	C'est pourquoi YHWH fut irrité contre Shelomoh, parce qu'il avait détourné son cœur de YHWH, l'Elohîm d'Israël, qui lui était apparu deux fois.
11:10	Il lui avait ordonné, à ce sujet, de ne pas aller après d'autres elohîm, mais il ne garda pas ce que YHWH lui avait ordonné.
11:11	Et YHWH dit à Shelomoh : Puisque tu as agi de la sorte, et que tu n'as pas observé l'alliance et les statuts que je t'avais prescrits, je déchirerai, je déchirerai le royaume afin qu'il ne soit plus à toi et je le donnerai à ton serviteur.
11:12	Toutefois, je ne le ferai pas en ton temps pour l'amour de David, ton père. C'est de la main de ton fils que je déchirerai le royaume.
11:13	Néanmoins, je ne déchirerai pas tout le royaume, j'en donnerai une tribu à ton fils, pour l'amour de David, mon serviteur, et pour l'amour de Yeroushalaim, que j'ai choisie.

### Elohîm suscite des ennemis à Shelomoh

11:14	YHWH donc suscita un ennemi à Shelomoh : Hadad, l'Édomite, qui était de la race royale d'Édom.
11:15	Il était arrivé qu'au temps où David était en Édom, Yoab, chef de l'armée, étant monté pour ensevelir les morts, tua tous les mâles qui étaient en Édom.
11:16	Yoab demeura là six mois avec tout Israël, jusqu'à ce qu'il eût exterminé tous les mâles d'Édom.
11:17	C'est alors que Hadad s'enfuit avec des hommes édomites d'entre les serviteurs de son père pour aller en Égypte. Hadad était alors un jeune garçon.
11:18	Une fois partis de Madian, ils allèrent à Paran, prirent avec eux des hommes de Paran, et arrivèrent en Égypte auprès de pharaon, roi d'Égypte. Celui-ci lui donna une maison, lui assura sa nourriture et lui donna aussi une terre.
11:19	Et Hadad trouva une grâce abondante aux yeux de pharaon, de sorte que pharaon lui donna pour femme la sœur de sa propre femme, la sœur de la reine Thachpenès.
11:20	Et la sœur de Thachpenès lui enfanta son fils Guenoubath. Thachpenès le sevra dans la maison de pharaon. Ainsi, Guenoubath fut dans la maison de pharaon, parmi les fils de pharaon.
11:21	Lorsque Hadad apprit en Égypte que David s'était endormi avec ses pères, et que Yoab, chef de l'armée, était mort, il dit à pharaon : Laisse-moi aller, et je m'en irai dans mon pays.
11:22	Et pharaon lui répondit : Que te manque-t-il chez moi pour que tu cherches à aller dans ton pays ? Et il répondit : Rien, mais laisse-moi aller, laisse-moi aller.
11:23	Elohîm suscita aussi un autre ennemi à Shelomoh : Rezon, fils d'Éliada, qui s'était enfui de chez son maître Hadadézer, roi de Tsoba.
11:24	Il avait rassemblé des hommes auprès de lui et était devenu chef de bandes, lorsque David les fit périr. Ils s'en allèrent à Damas, s'y établirent et y régnèrent.
11:25	Rezon fut ennemi d'Israël au temps de Shelomoh, en même temps que Hadad le mettait à mal, il avait en aversion Israël et il régna sur la Syrie.
11:26	Yarobam aussi, serviteur de Shelomoh, leva la main contre le roi. Il était fils de Nebath, Éphratien, de Tseréda, et sa mère était une femme veuve du nom de Tseroua.
11:27	Voici à quelle occasion il s'éleva contre le roi. Shelomoh bâtissait Millo et fermait la brèche de la cité de David, son père.
11:28	Yarobam était un homme vaillant et talentueux. Ayant vu ce jeune homme à l'œuvre, Shelomoh lui assigna la charge de toute la maison de Yossef.
11:29	Et il arriva en ce temps-là que Yarobam sortit de Yeroushalaim et rencontra en chemin le prophète Achiyah de Shiyloh, revêtu d'un manteau neuf. Ils étaient tous les deux seuls dans les champs.
11:30	Et Achiyah prit le manteau neuf qu'il avait sur lui et le déchira en 12 morceaux,
11:31	et il dit à Yarobam : Prends-en pour toi 10 morceaux ! Car ainsi parle YHWH, l'Elohîm d'Israël : Voici, je vais arracher le royaume de la main de Shelomoh et je t'en donnerai 10 tribus.
11:32	Mais il aura une tribu, pour l'amour de David, mon serviteur, et pour l'amour de Yeroushalaim, la ville que j'ai choisie parmi toutes les tribus d'Israël.
11:33	Parce qu'ils m'ont abandonné et se sont prosternés devant Astarté, la déesse des Sidoniens, devant Kemosh, l'elohîm de Moab, et devant Milcom, l'elohîm des fils d'Ammon, et parce qu'ils n'ont pas marché dans mes voies pour faire ce qui est droit à mes yeux et garder mes statuts et mes ordonnances comme l'a fait David, père de Shelomoh.
11:34	Toutefois, je n'ôterai pas de sa main tout le royaume, car pendant toute sa vie je le maintiendrai prince, pour l'amour de David, mon serviteur, que j'ai choisi et qui a observé mes commandements et mes statuts.
11:35	Mais j'ôterai le royaume de la main de son fils et je t'en donnerai 10 tribus.
11:36	Je donnerai une tribu à son fils afin que David mon serviteur ait toujours une lampe devant moi à Yeroushalaim, la ville que j'ai choisie pour y mettre mon Nom.
11:37	Je te prendrai donc, tu régneras sur tout ce que ton âme désirera, tu seras roi sur Israël.
11:38	Et il arrivera que si tu m'obéis dans tout ce que je t'ordonnerai, et si tu marches dans mes voies, en faisant tout ce qui est droit à mes yeux, en gardant mes statuts et mes commandements comme l'a fait David, mon serviteur, je serai avec toi, je te bâtirai une maison qui sera stable comme j'en ai bâti une à David et je te donnerai Israël.
11:39	Ainsi j'humilierai la postérité de David à cause de cela, mais non pas à toujours.
11:40	Shelomoh chercha à faire mourir Yarobam, mais Yarobam se leva et s'enfuit en Égypte vers Shishak, roi d'Égypte. Il demeura en Égypte jusqu'à la mort de Shelomoh.

### Mort de Shelomoh (Salomon)<!--2 Ch. 9:29-31.-->

11:41	Et le reste des actions de Shelomoh, tout ce qu'il a fait et sa sagesse, cela n'est-il pas écrit dans le livre des actes de Shelomoh ?
11:42	Shelomoh régna à Yeroushalaim sur tout Israël pendant 40 ans.
11:43	Ainsi Shelomoh s'endormit avec ses pères, il fut enseveli dans la cité de David, son père. Et Rehabam, son fils, régna à sa place.

## Chapitre 12

### Règne de Rehabam (Roboam)<!--2 Ch. 10:1 ; cp. Ec. 2:18-19.-->

12:1	Rehabam se rendit à Sichem, parce que tout Israël était venu à Sichem pour l'établir roi.
12:2	Or il arriva que quand Yarobam, fils de Nebath qui se trouvait encore en Égypte, où il s'était enfui de devant le roi Shelomoh, l'eut appris, Yarobam resta en Égypte.
12:3	On envoya appeler Yarobam, et il vint avec toute l'assemblée d'Israël. Ils parlèrent à Rehabam, en disant :
12:4	Ton père a rendu lourd notre joug. Mais toi, allège maintenant cette rude servitude de ton père et ce pesant joug qu'il a mis sur nous, et nous te servirons.
12:5	Il leur répondit : Allez, et dans 3 jours revenez vers moi. Et le peuple s'en alla.
12:6	Le roi Rehabam consulta les vieillards qui se tenaient en présence de Shelomoh, son père, pendant sa vie et leur dit : Quelle parole conseillez-vous de répondre à ce peuple ?
12:7	Et ils lui répondirent, en disant : Si aujourd'hui tu deviens le serviteur de ce peuple, si tu les sers, lui réponds et dis de bonnes paroles, ils seront tes serviteurs pour toujours.
12:8	Mais Rehabam laissa le conseil que les vieillards lui avaient donné, et consulta les enfants qui avaient grandi avec lui et qui se tenaient en sa présence.
12:9	Il leur dit : Que me conseillez-vous ? Quelle parole allons-nous retourner à ce peuple qui m'a parlé en disant : Allège le joug que ton père a mis sur nous ?
12:10	Alors les enfants qui avaient grandi avec lui, lui dirent : Tu parleras ainsi à ce peuple qui est venu te dire : Ton père a mis sur nous un pesant joug, mais toi allège-le-nous ! Tu leur parleras ainsi : Mon petit doigt est plus gros que les reins de mon père.
12:11	Or mon père a mis sur vous un pesant joug, mais moi je rendrai votre joug encore plus pesant. Mon père vous a châtiés avec des fouets, mais moi je vous châtierai avec des scorpions.
12:12	Or 3 jours après, Yarobam avec tout le peuple vint vers Rehabam, selon que le roi leur avait dit : Retournez vers moi dans 3 jours.
12:13	Mais le roi répondit durement au peuple, laissant le conseil que les anciens lui avaient donné.
12:14	Il leur parla selon le conseil des enfants, en leur disant : Mon père a mis sur vous un pesant joug, mais moi, je rendrai votre joug plus pesant encore ; mon père vous a châtiés avec des fouets, mais moi, je vous châtierai avec des scorpions.
12:15	Le roi donc n'écouta pas le peuple. Cela fut en effet conduit par YHWH en vue d'accomplir la parole qu'il avait prononcée par Achiyah de Shiyloh, à Yarobam, fils de Nebath.

### Schisme du royaume ; Yarobam (Jéroboam) devient roi d'Israël<!--2 Ch. 10:12-19, 11:1-4.-->

12:16	Et quand tout Israël vit que le roi ne les avait pas écoutés, le peuple fit cette réponse au roi, en disant : Quelle part avons-nous avec David ? Nous n'avons pas de propriété avec le fils d'Isaï ! À tes tentes, Israël ! Et toi David, pourvois maintenant à ta maison ! Ainsi Israël s'en alla dans ses tentes.
12:17	Les enfants d'Israël qui habitaient dans les villes de Yéhouda furent les seuls sur qui Rehabam régna.
12:18	Or le roi Rehabam envoya Adoram, qui était préposé aux impôts, mais tout Israël le lapida avec des pierres, et il mourut. Alors le roi Rehabam se hâta de monter sur un char pour s'enfuir à Yeroushalaim.
12:19	C'est ainsi qu'Israël s'est détaché de la maison de David jusqu'à ce jour.
12:20	Et il arriva que, quand tout Israël apprit que Yarobam était de retour, ils l'envoyèrent appeler au rassemblement et l'établirent roi sur tout Israël. La tribu de Yéhouda fut la seule qui suivit la maison de David.
12:21	Rehabam arriva à Yeroushalaim, il rassembla toute la maison de Yéhouda et la tribu de Benyamin, savoir 180 000 hommes sélectionnés et disposés à faire la guerre, pour combattre contre la maison d'Israël, et ramener la domination à Rehabam, fils de Shelomoh.
12:22	Mais la parole d'Elohîm vint à Shema’yah, homme d'Elohîm, en disant :
12:23	Parle à Rehabam, fils de Shelomoh, roi de Yéhouda, et à toute la maison de Yéhouda, et de Benyamin, et au reste du peuple, en disant :
12:24	Ainsi parle YHWH : Vous ne monterez pas et vous ne combattrez pas contre vos frères, les fils d'Israël ! Que chacun de vous retourne dans sa maison, car c'est de moi que cela vient. Ils obéirent à la parole de YHWH et s'en retournèrent, selon la parole de YHWH.

### Idolâtrie de Yarobam

12:25	Or Yarobam bâtit Sichem sur la Montagne d'Éphraïm, et y demeura, puis il en sortit et bâtit Penouel.
12:26	Et Yarobam dit en son cœur : Maintenant le royaume pourrait bien retourner à la maison de David.
12:27	Si ce peuple monte à Yeroushalaim pour faire des sacrifices dans la maison de YHWH, le cœur de ce peuple se tournera vers son seigneur, Rehabam, roi de Yéhouda, et ils me tueront, et ils retourneront à Rehabam, roi de Yéhouda.
12:28	Après avoir demandé conseil, le roi fit deux veaux d’or et dit au peuple : Vous êtes longtemps montés à Yeroushalaim ! Voici tes elohîm, ô Israël, qui t’ont fait sortir hors du pays d'Égypte.
12:29	Il plaça l'un de ces veaux à Béth-El et il mit l'autre à Dan.
12:30	Et cela fut une occasion de péché, car le peuple allait jusqu'à Dan pour se prosterner devant l'un des veaux.
12:31	Il fit aussi des maisons dans les hauts lieux, et établit des prêtres pris parmi tout le peuple, qui n'étaient pas des enfants de Lévi.
12:32	Yarobam ordonna aussi une fête au huitième mois, le quinzième jour du mois, comme la fête qu'on célébrait en Yéhouda et il offrit des sacrifices sur un autel. C'est ainsi qu'il agit à Béth-El, en sacrifiant aux veaux qu'il avait faits. Il établit à Béth-El des prêtres des hauts lieux qu'il avait élevés.
12:33	Or le quinzième jour du huitième mois, au mois qu'il avait choisi lui-même, il monta sur l'autel qu'il avait fait à Béth-El. Il fit une fête pour les enfants d'Israël et il monta sur l'autel pour brûler de l'encens.

## Chapitre 13

### Un homme d'Elohîm envoyé vers Yarobam

13:1	Et voici, un homme d'Elohîm vint de Yéhouda à Béth-El avec la parole de YHWH, pendant que Yarobam se tenait près de l'autel pour brûler de l'encens.
13:2	Et il cria contre l'autel selon la parole de YHWH et dit : Autel ! Autel ! Ainsi parle YHWH : Voici, un fils naîtra à la maison de David. Son nom sera Yoshiyah<!--Josias.-->. Il immolera sur toi les prêtres des hauts lieux qui brûlent de l'encens sur toi et on brûlera sur toi des ossements humains !
13:3	Le même jour il donna un signe, en disant : Voici le signe dont YHWH a parlé : Voici, l'autel se fendra et la cendre qui est dessus sera répandue.
13:4	Et il arriva que, lorsque le roi entendit la parole que l'homme d'Elohîm avait criée contre l'autel de Béth-El, Yarobam étendit sa main de l'autel, en disant : Saisissez-le ! Et la main qu'il étendit contre lui devint sèche, et il ne put la ramener à lui.
13:5	L'autel aussi se fendit, et la cendre qui était sur l'autel fut répandue, selon le signe que l'homme d'Elohîm avait donné par la parole de YHWH.
13:6	Alors le roi prit la parole et dit à l'homme d'Elohîm : Implore la face de YHWH, ton Elohîm, et prie pour moi, afin que ma main revienne à moi. L'homme d'Elohîm implora YHWH, et la main du roi put revenir à lui et elle fut comme auparavant.
13:7	Alors le roi dit à l'homme d'Elohîm : Entre avec moi dans la maison, tu prendras quelque nourriture et je te donnerai un présent.
13:8	Mais l'homme d'Elohîm répondit au roi : Quand tu me donnerais la moitié de ta maison, je n'entrerai pas chez toi, je ne mangerai pas de pain, ni ne boirai d'eau en ce lieu.
13:9	Car cela m'a été ordonné par YHWH qui m'a dit : Tu ne mangeras pas de pain, tu ne boiras pas d'eau et tu ne retourneras pas par le chemin que tu auras pris à l'aller.
13:10	Il s'en alla donc par un autre chemin, il ne s'en retourna pas par le chemin qu'il avait pris pour venir à Béth-El.

### L'homme d'Elohîm séduit par un vieux prophète

13:11	Or il y avait un vieux prophète qui habitait à Béth-El. Ses fils vinrent raconter toutes les choses que l'homme d'Elohîm avait faites ce jour-là à Béth-El, et les paroles qu'il avait dites au roi ; et comme les fils de ce prophète les rapportaient à leur père,
13:12	il leur demanda : Par quel chemin s'en est-il allé ? Or ses fils avaient vu le chemin par lequel l'homme d'Elohîm qui était venu de Yéhouda s'en était allé.
13:13	Et il dit à ses fils : Sellez-moi un âne. Ils lui sellèrent, puis il monta dessus.
13:14	Et il s'en alla après l'homme d'Elohîm et le trouva assis sous un chêne. Et il lui dit : Es-tu l'homme d'Elohîm qui est venu de Yéhouda ? Et il lui répondit : C'est moi.
13:15	Alors il lui dit : Viens avec moi dans la maison, et tu prendras de quoi te nourrir.
13:16	Mais il répondit : Je ne puis retourner avec toi, ni entrer chez toi et je ne mangerai pas de pain, ni ne boirai d'eau avec toi en ce lieu.
13:17	Car il m'a été dit de la part de YHWH : Tu ne mangeras pas de pain, tu ne boiras pas d'eau, et tu ne retourneras pas par le chemin que tu auras pris à l'aller.
13:18	Et il lui dit : Et moi aussi je suis prophète comme toi. Et un ange m'a parlé de la part de YHWH, en disant : Ramène-le avec toi dans ta maison, qu'il mange du pain, et qu'il boive de l'eau. Mais il lui mentait.
13:19	Il s'en retourna donc avec lui, il mangea du pain et but de l'eau dans sa maison.
13:20	Et il arriva que comme ils étaient assis à table, la parole de YHWH vint au prophète qui l'avait ramené.
13:21	Et il cria à l'homme d'Elohîm qui était venu de Yéhouda, en disant : Ainsi a parlé YHWH : Parce que tu as été rebelle au commandement de YHWH et que tu n'as pas gardé l'ordre que YHWH, ton Elohîm, t'avait donné,
13:22	et que tu t'en es retourné, que tu as mangé du pain et bu de l'eau dans le lieu dont YHWH t'avait dit : N'y mange pas de pain et n'y bois pas d'eau, ton cadavre n'entrera pas dans le sépulcre de tes pères.
13:23	Et il arriva que, quand le prophète qu'il avait ramené eut mangé du pain et bu de l'eau, il sella l'âne pour lui.
13:24	Puis ce prophète s'en alla, et un lion le rencontra dans le chemin, et le tua. Son corps était étendu dans le chemin, l'âne resta auprès du corps, et le lion aussi resta à côté du cadavre.
13:25	Et voici des passants virent le corps étendu dans le chemin et le lion qui se tenait auprès du corps. Ils vinrent le dire dans la ville où le vieux prophète habitait.
13:26	Et le prophète qui avait ramené du chemin l'homme d'Elohîm, l'ayant appris, dit : C'est l'homme d'Elohîm qui a été rebelle au commandement de YHWH, c'est pourquoi YHWH l'a livré au lion, qui l'a brisé et l'a fait mourir, selon la parole que YHWH lui avait dite.
13:27	Et il parla à ses fils, en disant : Sellez-moi un âne. Ils le lui sellèrent,
13:28	et il s'en alla et trouva le corps de l'homme d'Elohîm étendu dans le chemin, l'âne et le lion qui se tenaient auprès du corps. Le lion n'avait pas dévoré le cadavre, ni brisé l'âne.
13:29	Alors le prophète leva le corps de l'homme d'Elohîm, le plaça sur l'âne et le ramena. Et ce vieux prophète revint dans la ville pour le pleurer et l'enterrer.
13:30	Il mit le corps de ce prophète dans le sépulcre, et il pleura sur lui, en disant : Hélas, mon frère !
13:31	Après l'avoir enterré, il parla à ses fils, en disant : Quand je serai mort, enterrez-moi dans le sépulcre où est enterré l'homme d'Elohîm, et vous déposerez mes os à côté de ses os.
13:32	Car elle s'accomplira, la parole qu'il a criée de la part de YHWH, contre l'autel qui est à Béth-El et contre toutes les maisons des hauts lieux qui sont dans les villes de Samarie.

### Yarobam continue dans le mal

13:33	Néanmoins, Yarobam ne se détourna pas de sa mauvaise voie, mais il établit de nouveau des prêtres des hauts lieux pris parmi tout le peuple. Quiconque le désirait, il remplissait sa main, et devenait prêtre des hauts lieux.
13:34	Cela fut une occasion de péché pour la maison de Yarobam, qui fut effacée et exterminée de dessus la Terre.

## Chapitre 14

### Maladie et mort du fils de Yarobam

14:1	En ce temps-là, Abiyah, fils de Yarobam, devint malade.
14:2	Et Yarobam dit à sa femme : Lève-toi maintenant et déguise-toi, pour qu'on ne sache pas que tu es la femme de Yarobam, et va à Shiyloh. Voici, là est Achiyah, le prophète qui m'a dit que je serais roi sur ce peuple.
14:3	Emmène avec toi dix pains, des gâteaux et un vase de miel, et entre chez lui. Il te dira ce qui arrivera au garçon.
14:4	La femme de Yarobam fit donc ainsi. Elle se leva et s'en alla à Shiyloh puis elle entra dans la maison d'Achiyah. Or Achiyah ne pouvait plus voir, parce qu'il avait les yeux figés à cause de sa vieillesse.
14:5	Et YHWH dit à Achiyah : Voici la femme de Yarobam, qui vient te consulter concernant l'état de son fils, parce qu'il est malade. Tu lui parleras de telle et de telle manière. Quand elle arrivera, elle se sera déguisée.
14:6	Lorsque Achiyah eut entendu le bruit de ses pas, comme elle franchissait la porte, il dit : Entre, femme de Yarobam. Pourquoi fais-tu semblant d'être quelqu'un d'autre ? Je suis chargé de t'annoncer des choses dures.
14:7	Va, dis à Yarobam : Ainsi parle YHWH, l'Elohîm d'Israël : Parce que je t'ai élevé du milieu du peuple et que je t'ai établi pour chef sur mon peuple d'Israël,
14:8	j'ai arraché le royaume de la maison de David et je te l'ai donné. Mais parce que tu n'as pas été comme David, mon serviteur, qui a gardé mes commandements et qui a marché après moi de tout son cœur, ne faisant que ce qui est droit à mes yeux.
14:9	Tu as fait pire que tous ceux qui ont été devant toi, tu es allé te faire d'autres elohîm et des images en métal fondu pour m'irriter, et tu m'as rejeté derrière ton dos !
14:10	À cause de cela, voici, je vais faire venir le malheur sur la maison de Yarobam. Je retrancherai de chez Yarobam quiconque qui urine contre le mur, qu'il soit esclave ou libre en Israël, et je brûlerai la maison de Yarobam comme on brûle les ordures, jusqu'à ce qu'il n'en reste plus.
14:11	Celui de la maison de Yarobam qui mourra dans la ville, les chiens le mangeront, et celui qui mourra aux champs, les oiseaux du ciel le mangeront, car YHWH a parlé.
14:12	Toi donc lève-toi, va dans ta maison. Dès que tes pieds entreront dans la ville, l'enfant mourra.
14:13	Tout Israël le pleurera et on l'enterrera. Lui seul, en effet, de la famille de Yarobam entrera dans un sépulcre, parce que YHWH, l'Elohîm d'Israël, a trouvé quelque chose de bon en lui seul dans toute la maison de Yarobam.
14:14	YHWH s'établira un roi sur Israël qui retranchera la maison de Yarobam. Ce jour-là, n'est-ce pas déjà ce qui arrive ?
14:15	YHWH frappera Israël, l'agitant comme le roseau est agité dans l'eau. Il arrachera Israël de ce bon pays qu'il a donné à leurs pères, et les dispersera au-delà du fleuve, parce qu'ils se sont fait des ashérim, irritant YHWH.
14:16	Il livrera Israël à cause des péchés que Yarobam a commis et qu'il a fait commettre à Israël.
14:17	Alors la femme de Yarobam se leva, s'en alla et vint à Tirtsah. Au moment où elle atteignit le seuil de la maison, le jeune garçon mourut.
14:18	Il fut enseveli et tout Israël le pleura, selon la parole de YHWH, celle qu'il avait prononcée par la main de son serviteur Achiyah, le prophète.

### Règne de Nadab sur Israël

14:19	Quant au reste des actions de Yarobam, comment il a fait la guerre et comment il a régné, cela est écrit dans le livre des discours du jour des rois d'Israël.
14:20	Et le temps que Yarobam régna fut de 22 ans, puis il s'endormit avec ses pères. Et Nadab, son fils, régna à sa place.

### Yéhouda dans l'apostasie<!--2 Ch. 12:1.-->

14:21	Rehabam, fils de Shelomoh, régna en Yéhouda. Il avait 41 ans quand il devint roi, et il régna 17 ans à Yeroushalaim, la ville que YHWH avait choisie d'entre toutes les tribus d'Israël pour y mettre son Nom. Sa mère s'appelait Na`amah, l'Ammonite.
14:22	Yéhouda fit ce qui est mal aux yeux de YHWH et, par les péchés qu'ils commirent, ils excitèrent sa jalousie plus que leurs pères ne l'avaient jamais fait.
14:23	Car eux aussi se bâtirent des hauts lieux. Ils firent des images et des ashérim sur toute haute colline et sous tout arbre verdoyant.
14:24	Il y eut même des hommes prostitués qui se prostituent dans le pays. Et ils agirent selon toutes les abominations des nations que YHWH avait chassées devant les enfants d'Israël.

### Le roi d'Égypte emporte les trésors de Yéhouda ; mort de Rehabam (Roboam)<!--2 Ch. 12:2-16.-->

14:25	Et il arriva, en la cinquième année du roi Rehabam, que Shishak, roi d'Égypte, monta contre Yeroushalaim.
14:26	Il prit les trésors de la maison de YHWH et les trésors de la maison royale, et il emporta tout. Il prit aussi tous les boucliers d'or que Shelomoh avait faits.
14:27	Le roi Rehabam fit des boucliers de cuivre au lieu de ceux-là, et les mit entre les mains des chefs des coureurs, qui gardaient la porte de la maison du roi.
14:28	Toutes les fois où le roi entrait dans la maison de YHWH, les coureurs les portaient, et ensuite, ils les rapportaient dans la chambre des coureurs.
14:29	Le reste des actions de Rehabam, et tout ce qu'il a fait, n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ?
14:30	Il y eut toujours guerre entre Rehabam et Yarobam.
14:31	Rehabam s'endormit avec ses pères et fut enseveli avec eux dans la cité de David. Le nom de sa mère était Na`amah, l'Ammonite. Et Abiyam, son fils, régna à sa place.

## Chapitre 15

### Règne d'Abiyam (ou Abiyah) sur Yéhouda<!--2 Ch. 13:1-2.-->

15:1	La dix-huitième année du roi Yarobam, fils de Nebath, Abiyam commença à régner sur Yéhouda.
15:2	Il régna 3 ans à Yeroushalaim. Sa mère s'appelait Ma'akah, et elle était fille d'Absalom.
15:3	Il marcha dans tous les péchés que son père avait commis avant lui. Son cœur ne fut pas intègre envers YHWH, son Elohîm, comme l'avait été le cœur de David, son père.
15:4	Mais pour l'amour de David, YHWH, son Elohîm, lui donna une lampe dans Yeroushalaim, lui suscitant son fils après lui et laissant subsister Yeroushalaim.
15:5	Parce que David avait fait ce qui est droit devant YHWH, et que pendant toute sa vie, il ne s'était détourné d'aucun de ses commandements, hormis dans l'affaire d'Ouriyah, le Héthien.
15:6	Or il y eut toujours guerre entre Rehabam et Yarobam, pendant toute la vie de Rehabam.
15:7	Le reste des actions d'Abiyam, et même tout ce qu'il fit, n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ? Il y eut aussi guerre entre Abiyam et Yarobam.
15:8	Ainsi Abiyam s'endormit avec ses pères, et on l'enterra dans la cité de David. Et Asa, son fils, régna à sa place.

### Règne d'Asa sur Yéhouda<!--2 Ch. 14:1-5, 15:1-19.-->

15:9	La vingtième année de Yarobam, roi d'Israël, Asa commença à régner sur Yéhouda.
15:10	Il régna 41 ans à Yeroushalaim. Le nom de sa mère était Ma'akah, elle était fille d'Absalom.
15:11	Asa fit ce qui est droit devant YHWH, comme David, son père.
15:12	Il chassa du pays les hommes prostitués qui se prostituent et il fit disparaître toutes les idoles que ses pères avaient faites.
15:13	Et il retira même à sa mère Ma'akah la dignité de reine-mère, parce qu'elle avait fait une chose horrible pour Asherah. Asa mit en pièces la chose horrible qu'elle avait faite, et la brûla au torrent de Cédron.
15:14	Mais les hauts lieux ne furent pas ôtés. Néanmoins, le cœur d'Asa fut intègre envers YHWH pendant toute sa vie.

### Guerre entre Yéhouda et Israël ; Asa s'allie à la Syrie<!--1 Ch. 14:6-15, 16:1-10.-->

15:15	Il remit dans la maison de YHWH les choses qui avaient été consacrées par son père et par lui-même, de l'argent, de l'or et les ustensiles.
15:16	Or il y eut guerre entre Asa et Baesha, roi d'Israël, pendant toute leur vie.
15:17	Baesha, roi d'Israël, monta contre Yéhouda, et bâtit Ramah, pour empêcher quiconque de sortir et entrer vers Asa, roi de Yéhouda.
15:18	Asa prit tout l'argent et l'or qui était resté dans les trésors de YHWH et dans les trésors de la maison royale, et les donna à ses serviteurs. Le roi Asa les envoya vers Ben-Hadad, fils de Thabrimmon, fils de Hezyôn, roi de Syrie, qui demeurait à Damas, pour lui dire :
15:19	Qu'il y ait alliance entre moi et toi, comme entre mon père et le tien. Voici, je t'envoie un pot-de-vin en argent et en or. Va, romps l'alliance que tu as avec Baesha, roi d'Israël, afin qu'il se retire de moi.
15:20	Et Ben-Hadad écouta le roi Asa ; il envoya les chefs de son armée contre les villes d'Israël, et il battit Iyôn, Dan, Abel-Beth-Ma'akah, tout Kinneroth, et tout le pays de Nephthali.
15:21	Et il arriva qu'aussitôt que Baesha l'apprit, il cessa de bâtir Ramah et demeura à Tirtsah.
15:22	Alors le roi Asa convoqua tout Yéhouda, personne n'était exempté. Ils emportèrent les pierres et le bois avec lesquels Baesha bâtissait Ramah, et le roi Asa s'en servit pour bâtir Guéba de Benyamin, et Mitspa.

### Mort d'Asa ; Yehoshaphat règne sur Yéhouda<!--2 Ch. 16:11-14, 17:1.-->

15:23	Le reste de tous les discours d'Asa, toutes ses actions puissantes, tout ce qu'il a accompli, et les villes qu'il a bâties, cela n'est-il pas écrit dans le livre des discours du jour des rois de Yéhouda ? Au reste, il fut malade des pieds au temps de sa vieillesse.
15:24	Et Asa s'endormit avec ses pères, avec lesquels il fut enseveli dans la cité de David, son père. Et son fils Yehoshaphat régna à sa place.

### Baesha tue Nadab et devient roi d'Israël

15:25	Or Nadab, fils de Yarobam, régna sur Israël la seconde année d'Asa, roi de Yéhouda, et il régna 2 ans sur Israël.
15:26	Il fit ce qui est mal aux yeux de YHWH. Il marcha sur la voie de son père, se livrant aux péchés que son père avait fait commettre à Israël.
15:27	Et Baesha, fils d'Achiyah, de la maison de Yissakar, fit une conspiration contre lui. Il le tua devant Guibbethon, qui était aux Philistins, lorsque Nadab et tout Israël assiégeaient Guibbethon.
15:28	Baesha le fit donc mourir la troisième année d'Asa, roi de Yéhouda, et il régna à sa place.
15:29	Et il arriva que, quand il fut roi, il frappa toute la maison de Yarobam et ne laissa échapper aucune âme vivante, il détruisit tout ce qui respirait, selon la parole de YHWH qu'il avait proférée par son serviteur Achiyah, Shiylonite,
15:30	à cause des péchés que Yarobam avait commis et fait commettre à Israël, irritant ainsi YHWH, l'Elohîm d'Israël.
15:31	Le reste des discours de Nadab, et même tout ce qu'il a accompli, n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?
15:32	Or il y eut guerre entre Asa et Baesha, roi d'Israël, pendant toute leur vie.
15:33	La troisième année d'Asa, roi de Yéhouda, Baesha, fils d'Achiyah, commença à régner sur tout Israël à Tirtsah, il régna 24 ans.
15:34	Et il fit ce qui est mal aux yeux de YHWH, et marcha sur la voie de Yarobam, en se livrant aux péchés que Yarobam avait fait commettre à Israël.

## Chapitre 16

### YHWH avertit Baesha avant sa mort

16:1	Alors la parole de YHWH vint à Yehuw, fils de Hanani, contre Baesha, en disant :
16:2	Je t'ai élevé de la poussière et je t'ai établi chef de mon peuple d'Israël. Malgré cela, tu as suivi la voie de Yarobam et fait pécher mon peuple d'Israël, pour m'irriter par leurs péchés.
16:3	Voici, je m'en vais entièrement consumer Baesha et sa maison, et je rendrai ta maison semblable à la maison de Yarobam, fils de Nebath.
16:4	Quiconque de chez Baesha qui mourra dans la ville, les chiens le mangeront, et celui des siens qui mourra aux champs, les oiseaux du ciel le mangeront.
16:5	Le reste des discours de Baesha, ce qu'il a accompli et ses actions puissantes, n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?
16:6	Ainsi Baesha s'endormit avec ses pères et fut enseveli à Tirtsah. Élah, son fils, régna à sa place.
16:7	La parole de YHWH vint par le moyen de Yehuw, fils d'Hanani, le prophète, contre Baesha et contre sa maison, à cause de tout le mal qu'il avait fait devant les yeux de YHWH, en l’irritant par l'œuvre de ses mains et en devenant comme la maison de Yarobam, parce qu’il tua celui-ci.

### Élah puis Zimri règnent sur Israël

16:8	La vingt-sixième année d'Asa, roi de Yéhouda, Élah, fils de Baesha, commença à régner sur Israël et il régna 2 ans à Tirtsah.
16:9	Son serviteur, Zimri, capitaine de la moitié des chars, fit une conspiration contre Élah, lorsqu'il était à Tirtsah, buvant et s'enivrant dans la maison d'Artsa, chef de la maison du roi à Tirtsah.
16:10	Alors, Zimri vint, le frappa et le tua, la vingt-septième année d'Asa, roi de Yéhouda, et il régna à sa place.
16:11	Et il arriva, dès qu’il fut roi et qu’il fut assis sur son trône, qu'il frappa toute la maison de Baesha. Il ne lui laissa personne qui urine contre un mur, ni rédempteur ni ami.
16:12	Ainsi Zimri extermina toute la maison de Baesha, selon la parole que YHWH avait proférée contre Baesha, par Yehuw, le prophète,
16:13	à cause de tous les péchés de Baesha, et des péchés d'Élah, son fils, qu'ils avaient commis et qu'ils avaient fait commettre à Israël, irritant YHWH, l'Elohîm d'Israël, par leurs idoles.
16:14	Le reste des actions d'Élah, et même tout ce qu'il a fait, n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?
16:15	La vingt-septième année d'Asa, roi de Yéhouda, Zimri régna 7 jours à Tirtsah. Or le peuple était campé contre Guibbethon qui appartenait aux Philistins.
16:16	Et le peuple qui était campé là, entendit que l'on disait : Zimri a fait une conspiration, et il a même tué le roi ! En ce même jour, tout Israël établit dans le camp pour roi d'Israël Omri, chef de l'armée d'Israël.
16:17	Omri et tout Israël avec lui partirent de Guibbethon, et assiégèrent Tirtsah.
16:18	Et il arriva que, quand Zimri vit que la ville était prise, il entra au palais de la maison royale et brûla par le feu sur lui la maison royale. C'est ainsi qu'il mourut,
16:19	à cause des péchés qu'il avait commis, faisant ce qui est mal aux yeux de YHWH, en suivant la voie de Yarobam et le péché qu'il avait fait commettre à Israël.
16:20	Le reste des actions de Zimri et la conspiration qu'il forma, cela n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?

### Omri règne sur Israël

16:21	Alors le peuple d'Israël se divisa en deux parties : la moitié du peuple était derrière Thibni, fils de Guinath, pour le faire roi, et l'autre moitié derrière Omri.
16:22	Mais le peuple qui suivait Omri fut plus fort que le peuple qui suivait Thibni, fils de Guinath. Thibni mourut et Omri régna.
16:23	La trente et unième année d'Asa, roi de Yéhouda, Omri commença à régner sur Israël et il régna 12 ans après avoir régné 6 ans à Tirtsah.
16:24	Puis il acheta de Shémer la Montagne de Samarie pour 2 talents d'argent. Il bâtit une ville sur cette montagne et appela la ville qu'il bâtit du nom de Samarie, d'après le nom de Shémer, seigneur de la montagne.
16:25	Omri fit ce qui est mal aux yeux de YHWH, et il agit plus mal encore que tous ceux qui avaient été avant lui.
16:26	Il marcha dans la voie de Yarobam, fils de Nebath, et se livra aux péchés que Yarobam avait fait commettre à Israël, irritant YHWH, l'Elohîm d'Israël, par leurs idoles.
16:27	Le reste des discours d'Omri, tout ce qu'il a accompli et les actions puissantes qu'il a accomplies, cela n'est-il pas écrit dans le livre des discours du jour des rois d'Israël ?
16:28	Ainsi Omri s'endormit avec ses pères et fut enseveli à Samarie. Achab, son fils, régna à sa place.

### Achab règne sur Israël et épouse Jézabel

16:29	Achab, fils d'Omri, régna sur Israël la trente-huitième année d'Asa, roi de Yéhouda. Et Achab, fils d'Omri, régna sur Israël à Samarie 22 ans.
16:30	Et Achab, fils d'Omri, fit ce qui est mal aux yeux de YHWH, plus que tous ceux qui avaient été avant lui.
16:31	Et il arriva, comme si cela lui eût été peu de chose de marcher dans les péchés de Yarobam, fils de Nebath, qu'il prit pour femme Jézabel, fille d'Ethbaal, roi des Sidoniens, puis il alla servir Baal et se prosterna devant lui.
16:32	Il dressa un autel à Baal, dans la maison de Baal, qu'il bâtit à Samarie.
16:33	Achab fit une asherah. Achab fit plus encore que tous les rois d'Israël qui avaient été avant lui, pour irriter YHWH, l'Elohîm d'Israël.
16:34	En son temps, Hiel de Béth-El bâtit Yeriycho. Il en jeta les fondements au prix d'Abiram, son premier-né et posa ses portes sur Segoub, son plus jeune fils, selon la parole que YHWH avait proférée par le moyen de Yéhoshoua<!--Jos. 6:26.-->, fils de Noun.

## Chapitre 17

### Eliyah annonce trois ans de sécheresse<!--1 R. 17.-->

17:1	Alors Eliyah le Thishbite, d’entre les habitants de Galaad, dit à Achab : YHWH, l'Elohîm d'Israël, en la présence duquel je me tiens est vivant ! Il n'y aura ces années-ci ni rosée ni pluie, sauf sur la parole de ma bouche.

### Eliyah au torrent de Kerith

17:2	Puis la parole de YHWH vint à lui, en disant :
17:3	Va-t'en d'ici, tourne-toi vers l'orient et cache-toi près du torrent de Kerith qui est en face du Yarden.
17:4	Tu boiras de l'eau du torrent, et j'ai ordonné aux corbeaux de t'y nourrir.
17:5	Il partit donc et fit selon la parole de YHWH, il s'en alla et demeura au torrent de Kerith, qui est en face du Yarden.
17:6	Les corbeaux lui apportaient du pain et de la viande le matin, et du pain et de la viande le soir, et il buvait de l'eau du torrent.
17:7	Mais il arriva qu'au bout d'un certain temps, le torrent tarit, parce qu'il n'y avait pas eu de pluie dans le pays.

### Eliyah chez la veuve de Sarepta

17:8	Alors la parole de YHWH vint à lui, en disant :
17:9	Lève-toi, va à Sarepta, qui appartient à Sidon, et demeure là. Voici, j'ai commandé là à une femme veuve de t'y nourrir.
17:10	Il se leva donc et s'en alla à Sarepta. Et comme il fut arrivé à l'entrée de la ville, voici, une femme veuve était là, qui ramassait du bois. Et il l'appela et lui dit : Apporte-moi, je te prie, un peu d'eau dans un vase et que je boive.
17:11	Elle alla en chercher. Il l'appela de nouveau et dit : Apporte-moi, je te prie, un morceau de pain de ta main.
17:12	Mais elle répondit : YHWH, ton Elohîm, est vivant ! Je n'ai rien de cuit, je n'ai qu'une poignée de farine dans un pot et un peu d'huile dans une cruche. Et voici, j'amasse deux morceaux de bois, puis je rentrerai, je l'apprêterai pour moi et pour mon fils, nous le mangerons, puis nous mourrons.
17:13	Et Eliyah lui dit : N'aie pas peur, va, fais comme tu l'as dit. Seulement, fais-moi d'abord avec cela un petit gâteau et tu me l'apporteras, tu en feras ensuite pour toi et pour ton fils.
17:14	Car ainsi parle YHWH, l'Elohîm d'Israël : La farine qui est dans le pot ne finira pas et l'huile qui est dans la cruche ne diminuera pas, jusqu'à ce que YHWH donne de la pluie sur la Terre.
17:15	Elle s'en alla donc et fit selon la parole d'Eliyah. Et elle eut à manger, elle et sa famille, ainsi qu'Eliyah pendant plusieurs jours.
17:16	La farine du pot ne finit pas, et l'huile de la cruche ne diminua pas, selon la parole que YHWH avait prononcée par le moyen d'Eliyah.

### Résurrection du fils de la veuve de Sarepta

17:17	Après ces choses, il arriva que le fils de la femme, maîtresse de la maison, devint malade, et sa maladie fut si forte qu'il ne resta plus en lui de respiration.
17:18	Et elle dit à Eliyah : Qu'y a-t-il entre moi et toi, homme d'Elohîm ? Es-tu venu chez moi pour rappeler le souvenir de mon iniquité et pour faire mourir mon fils ?
17:19	Et il lui dit : Donne-moi ton fils. Et il le prit du sein de cette femme, le porta dans la chambre haute où il demeurait, et le coucha sur son lit.
17:20	Puis il cria à YHWH et dit : YHWH, mon Elohîm ! Affligeras-tu cette veuve au point de faire mourir son fils, elle qui m'a reçu comme un hôte ?
17:21	Et il s'étendit sur l'enfant par trois fois, et cria à YHWH, en disant : YHWH, mon Elohîm ! Je te prie que l'âme de cet enfant revienne au-dedans de lui.
17:22	Et YHWH écouta la voix d'Eliyah, l'âme de l'enfant revint au-dedans de lui, et il fut rendu à la vie.
17:23	Eliyah prit l'enfant, le descendit de la chambre haute dans la maison et le donna à sa mère, en lui disant : Regarde, ton fils est vivant.
17:24	Et la femme dit à Eliyah : Maintenant je sais que tu es un homme d'Elohîm, et que la parole de YHWH qui est dans ta bouche est vérité.

## Chapitre 18

### Eliyah à la rencontre d'Obadyah puis d'Achab

18:1	Et il arriva, après bien des jours, que la parole de YHWH vint à Eliyah, dans la troisième année, en disant : Va, montre-toi à Achab et je ferai tomber de la pluie sur la terre.
18:2	Et Eliyah s'en alla pour se présenter devant Achab. Il y avait alors une grande famine en Samarie.
18:3	Achab avait appelé Obadyah, chef de sa maison. (Or Obadyah craignait beaucoup YHWH :
18:4	Et il était arrivé, quand Jézabel exterminait les prophètes de YHWH, qu'Obadyah prit 100 prophètes et les cacha, 50 dans une caverne et 50 dans une autre, et il les y nourrit de pain et d'eau.)
18:5	Achab dit alors à Obadyah : Va par le pays vers toutes les sources d'eaux et vers tous les torrents. Peut-être que nous trouverons de l'herbe, nous garderons ainsi en vie les chevaux et les mulets, et nous n'aurons pas besoin d'abattre du bétail.
18:6	Ils se partagèrent donc entre eux le pays pour le parcourir. Achab allait seul par un chemin et Obadyah allait seul par un autre chemin.
18:7	Comme Obadyah était en chemin, voici, Eliyah vint à sa rencontre. Obadyah reconnut Eliyah, il tomba sur son visage et lui dit : N'es-tu pas mon seigneur Eliyah ?
18:8	Il lui répondit : C'est moi. Va et dis à ton seigneur : Voici Eliyah !
18:9	Et Obadyah dit : Quel péché ai-je commis, pour que tu livres ton serviteur entre les mains d'Achab pour me faire mourir ?
18:10	YHWH, ton Elohîm, est vivant ! Il n'y a ni nation, ni royaume, où mon seigneur n'ait envoyé pour te chercher. Et quand on répondait que tu n'y étais pas, il faisait jurer aux rois et au peuple que l'on ne t'avait pas trouvé.
18:11	Et maintenant tu dis : Va, dis à ton seigneur : Voici Eliyah !
18:12	Et il arrivera que, quand je t'aurai quitté, l'Esprit de YHWH te transportera je ne sais où et j'irai informer Achab qui ne te trouvera pas et qui me tuera. Or ton serviteur craint YHWH dès sa jeunesse.
18:13	N'a-t-on pas raconté à mon seigneur ce que j'ai fait quand Jézabel tuait les prophètes de YHWH ? Comment j'ai caché 100 hommes parmi les prophètes de YHWH, 50 hommes dans une caverne et 50 dans une autre et je les ai nourris de pain et d'eau ?
18:14	Et maintenant tu dis : Va, dis à ton seigneur : Voici Eliyah ! Il me tuera !
18:15	Mais Eliyah lui répondit : YHWH Sabaoth, en la présence duquel je me tiens est vivant ! Aujourd'hui, je me montrerai à Achab.
18:16	Obadyah étant allé à la rencontre d'Achab, l'informa de la chose ; puis Achab alla au-devant d'Eliyah.
18:17	Et il arriva que, quand Achab vit Eliyah, Achab lui dit : Est-ce toi qui jettes le trouble en Israël ?
18:18	Et Eliyah lui répondit : Je n'ai pas troublé Israël. Mais c'est toi et la maison de ton père, puisque vous avez abandonné les commandements de YHWH et que vous êtes allés après les Baalim.
18:19	Et maintenant envoie et fais rassembler tout Israël auprès de moi, sur le mont Carmel, les 450 prophètes de Baal et les 400 prophètes d'Asherah qui mangent à la table de Jézabel.

### Confrontation entre Eliyah et les prophètes de Baal sur le mont Carmel

18:20	Ainsi Achab envoya des messagers vers tous les enfants d'Israël et il rassembla les prophètes sur le mont Carmel.
18:21	Alors Eliyah s'approcha de tout le peuple, et dit : Jusqu'à quand clocherez-vous des deux côtés ? Si YHWH est Elohîm, suivez-le ! Mais si Baal est elohîm, suivez-le ! Et le peuple ne lui répondit pas un seul mot.
18:22	Alors Eliyah dit au peuple : Je suis resté le seul prophète de YHWH, et les prophètes de Baal sont 450 hommes.
18:23	Que l'on nous donne deux veaux, qu'ils en choisissent l'un pour eux, qu'ils le coupent en pièces et qu'ils le mettent sur du bois ; mais qu'ils n'y mettent pas de feu ; et je préparerai l'autre veau, je le mettrai sur du bois, sans y mettre le feu.
18:24	Puis invoquez le nom de vos elohîm. Et moi j'invoquerai le Nom de YHWH. L'elohîm qui répondra par le feu, c'est lui qui est Elohîm. Et tout le peuple répondit, et dit : Ce discours est bon !
18:25	Et Eliyah dit aux prophètes de Baal : Choisissez un veau et préparez-le les premiers, car vous êtes en plus grand nombre et invoquez le nom de vos elohîm ; mais n'y mettez pas de feu.
18:26	Ils prirent donc un veau qu'on leur donna, ils l'apprêtèrent et ils invoquèrent le nom de Baal depuis le matin jusqu'à midi, en disant : Baal exauce-nous ! Mais il n'y avait ni voix ni réponse et ils sautaient devant l'autel qu'ils avaient fait.
18:27	Et il arriva qu’à midi, Eliyah se moqua d'eux et dit : Criez à grande voix ! Puisqu'il est elohîm, il doit être en méditation, ou il est occupé, ou bien il est en voyage. Peut-être qu'il dort et qu'il va se réveiller.
18:28	Ils criaient donc à grande voix. Ils se faisaient des incisions avec des couteaux et des lances, selon leur coutume, en sorte que le sang coulait sur eux.
18:29	Et il arriva, quand midi fut passé, qu’ils prophétisèrent jusqu’à l’heure où monte l’offrande de grain, mais il n’y eut ni voix, ni réponse, ni signe d'attention.
18:30	Eliyah dit alors à tout le peuple : Approchez-vous de moi ! Et tout le peuple s'approcha de lui et il répara l'autel de YHWH, qui avait été renversé.
18:31	Puis Eliyah prit 12 pierres, selon le nombre des tribus des fils de Yaacov, à qui était venue la parole de YHWH en disant : Israël sera ton nom.
18:32	Et il bâtit avec ces pierres un autel au Nom de YHWH. Puis il fit un fossé de la capacité de deux mesures de semence autour de l'autel.
18:33	Il rangea le bois, il coupa le veau en pièces, et il le plaça sur le bois.
18:34	Et il dit : Remplissez quatre cruches d'eau, puis versez-les sur l'holocauste et sur le bois ! Et il dit : Faites-le encore une seconde fois ! Et ils le firent une seconde fois. Il dit : Faites-le une troisième fois ! Et ils le firent pour la troisième fois.
18:35	De sorte que les eaux coulèrent autour de l'autel et l'on remplit aussi d'eau le fossé.
18:36	Et il arriva, à l’heure où monte l’offrande de grain, qu'Eliyah, le prophète, s'approcha et dit : Ô YHWH ! Elohîm d'Abraham, de Yitzhak et d'Israël ! Que l'on sache aujourd'hui que tu es Elohîm en Israël, que je suis ton serviteur et que j'ai fait toutes ces choses par ta parole !
18:37	Réponds-moi, YHWH ! Réponds-moi, afin que ce peuple sache que c'est toi, YHWH, qui es Elohîm et que c'est toi qui ramènes leur cœur.
18:38	Alors le feu de YHWH tomba et consuma l'holocauste, le bois, les pierres et la terre, et il absorba toute l'eau qui était dans le fossé.
18:39	Quand tout le peuple vit cela, ils tombèrent sur leur visage et dirent : C'est YHWH qui est Elohîm ! C'est YHWH qui est Elohîm !
18:40	Et Eliyah leur dit : Saisissez les prophètes de Baal et qu'il n'en échappe aucun ! Ils les saisirent. Eliyah les fit descendre au torrent de Kison, où il les fit égorger.

### Retour de la pluie selon la parole d'Eliyah<!--Ja. 5:17-18.-->

18:41	Puis Eliyah dit à Achab : Monte, mange et bois, car il se fait un bruit qui annonce la pluie.
18:42	Ainsi Achab monta pour manger et pour boire tandis qu'Eliyah monta au sommet du Carmel et, se penchant contre terre, il mit son visage entre ses genoux.
18:43	Il dit à son serviteur : Monte maintenant et regarde vers la mer. Le serviteur monta, il regarda, et dit : Il n'y a rien. Eliyah dit par sept fois : Retournes-y.
18:44	À la septième fois, il dit : Voici un petit nuage qui s'élève de la mer, il est comme la paume de la main d'un homme. Eliyah dit : Monte et dis à Achab : Attelle ton char et descends de peur que la pluie ne t'arrête.
18:45	Et il arriva entre temps que les cieux s'obscurcirent de nuages accompagnés de vent et il y eut une forte pluie. Achab monta sur son char et partit pour Yizre`e'l.
18:46	Et la main de YHWH fut sur Eliyah, qui se ceignit les reins et courut devant Achab, jusqu'à l'entrée de Yizre`e'l.

## Chapitre 19

### Fuite d'Eliyah devant les menaces de Jézabel

19:1	Achab rapporta à Jézabel tout ce qu'Eliyah avait fait, et comment il avait tué par l'épée tous les prophètes.
19:2	Et Jézabel envoya un messager vers Eliyah, pour lui dire : Qu'ainsi me traitent les elohîm et qu'ainsi ils y ajoutent si demain, à cette heure-ci, je ne me sers pas de ton âme comme l’âme de l’un d’eux ! 
19:3	Eliyah, voyant cela, se leva et s'en alla pour son âme. Il arriva à Beer-Shéba, qui appartient à Yéhouda, et il laissa là son serviteur.

### L'Ange de YHWH fortifie Eliyah

19:4	Pour lui, il marcha dans le désert, le chemin d’un jour, et il alla s'asseoir sous un genêt. Il demanda la mort pour son âme, en disant : C'en est assez, ô YHWH ! Prends mon âme, car je ne suis pas meilleur que mes pères.
19:5	Il se coucha et s'endormit sous un genêt. Voici un ange le toucha et lui dit : Lève-toi, mange.
19:6	Et il regarda, et voici à son chevet un gâteau cuit sur des pierres chaudes et une cruche d'eau. Il mangea et but, puis il retourna se coucher.
19:7	Et l'Ange de YHWH vint une seconde fois, le toucha et lui dit : Lève-toi, mange, car le chemin est trop long pour toi.

### Eliyah à Horeb, visitation et instructions de YHWH

19:8	Il se leva donc, mangea et but. Puis, avec la force que lui donna cette nourriture, il marcha 40 jours et 40 nuits jusqu'à Horeb, la montagne d'Elohîm.
19:9	Et là, il entra dans une caverne et y passa la nuit. Et voici, la parole de YHWH vint à lui en ces mots : Que fais-tu ici Eliyah ?
19:10	Et il répondit : J'ai été jaloux, j'ai été jaloux pour YHWH, Elohîm Sabaoth, parce que les enfants d'Israël ont abandonné ton alliance, ils ont renversé tes autels, ils ont tué tes prophètes par l'épée. Je suis resté, moi seul, et ils cherchent mon âme pour l’enlever !
19:11	YHWH lui dit : Sors et tiens-toi sur la montagne devant YHWH. Et voici, YHWH passa. Et devant YHWH, il y eut un grand vent impétueux qui déchirait les montagnes et brisait les rochers, mais YHWH n'était pas dans ce vent. Après le vent, ce fut un tremblement de terre, mais YHWH n'était pas dans ce tremblement de terre.
19:12	Après le tremblement de terre, un feu, mais YHWH n'était pas dans le feu. Et après le feu, une voix calme et petite.
19:13	Et il arriva que dès qu'Eliyah l'entendit, il s'enveloppa le visage de son manteau, il sortit et se tint à l'entrée de la caverne. Et voici, une voix lui fit entendre ces paroles : Que fais-tu ici Eliyah ?
19:14	Et il répondit : J'ai été jaloux, j'ai été jaloux pour YHWH, Elohîm Sabaoth, parce que les enfants d'Israël ont abandonné ton alliance, ils ont renversé tes autels, ils ont tué par l'épée tes prophètes. Je suis resté, moi seul, et ils cherchent mon âme pour l’enlever !
19:15	Mais YHWH lui dit : Va, retourne par ton chemin vers le désert de Damas. Quand tu seras arrivé, tu oindras Hazaël pour roi de Syrie.
19:16	Tu oindras aussi Yehuw, fils de Nimshi, pour roi d'Israël et tu oindras Eliysha, fils de Shaphath, d'Abel-Mehola, pour prophète à ta place.
19:17	Et il arrivera que quiconque échappera à l'épée de Hazaël, Yehuw le fera mourir, et quiconque échappera à l'épée de Yehuw, Eliysha le fera mourir.
19:18	Mais je laisserai en Israël 7 000 hommes<!--Ro. 11:1-4.-->, tous ceux qui n'ont pas fléchi les genoux devant Baal et dont la bouche ne l'a pas embrassé.

### Eliysha devient disciple d'Eliyah

19:19	Eliyah partit donc de là et il trouva Eliysha, le fils de Shaphath, qui labourait. Il y avait douze paires de bœufs devant lui et il était avec la douzième. Eliyah passa près de lui et jeta sur lui son manteau.
19:20	Eliysha laissa ses bœufs et courut après Eliyah, en disant : Je t'en prie, laisse-moi embrasser mon père et ma mère, et je te suivrai. Eliyah lui répondit : Vas-y et reviens. Pense en effet à ce que je t'ai fait.
19:21	Après s'être éloigné d'Eliyah, il revint prendre une paire de bœufs qu'il offrit en sacrifice. Avec l'attelage des bœufs, il fit cuire leur chair et la donna à manger au peuple. Puis il se leva, s'en alla après Eliyah et le servait.

## Chapitre 20

### Achab monte contre Ben-Hadad

20:1	Alors Ben-Hadad, roi de Syrie rassembla toute son armée. Il avait avec lui 32 rois, des chevaux et des chars. Puis il monta, assiégea Samarie et il lui fit la guerre.
20:2	Il envoya des messagers à Achab, roi d'Israël, dans la ville.
20:3	Il lui fit dire : Ainsi parle Ben-Hadad : Ton argent et ton or sont à moi, tes femmes aussi et tes beaux enfants sont à moi.
20:4	Et le roi d'Israël répondit et dit : Mon seigneur, je suis à toi, comme tu le dis, avec tout ce que j'ai.
20:5	Ensuite les messagers retournèrent et dirent : Ainsi parle Ben-Hadad, disant : je t'ai envoyé dire en effet : Donne-moi ton argent et ton or, ta femme et tes enfants,
20:6	en effet, demain à cette heure-ci, j'enverrai chez toi mes serviteurs, ils fouilleront ta maison et les maisons de tes serviteurs. Tout ce qui est désirable à tes yeux, ils le mettront dans leur main et le prendront.
20:7	Alors le roi d'Israël appela tous les anciens du pays, et il dit : Sachez et considérez, je vous prie, combien cet homme nous veut du mal. En effet, il m'a envoyé demander mes femmes, mes enfants, mon argent et mon or, et je ne lui avais rien refusé.
20:8	Et tous les anciens et tout le peuple lui dirent : Ne l'écoute pas et ne consens pas.
20:9	Il répondit donc aux messagers de Ben-Hadad : Dites au roi, mon seigneur : Je ferai tout ce que tu as envoyé demander la première fois à ton serviteur mais cette chose, je ne peux pas la faire. Les messagers s'en allèrent et lui rapportèrent cette réponse.
20:10	Et Ben-Hadad envoya dire à Achab : Qu'ainsi me traitent les elohîm et qu'ainsi ils y ajoutent, si la poussière de Samarie suffit pour remplir le creux de la main de tout le peuple qui me suit.
20:11	Mais le roi d'Israël répondit et dit : Dites-lui : Que celui qui met sa ceinture ne se glorifie pas comme celui qui l'enlève !
20:12	Or il arriva qu'aussitôt que Ben-Hadad entendit cette réponse, (il était en train de boire sous les tentes avec les rois), il dit à ses serviteurs : Rangez-vous en bataille ! Et ils se rangèrent en bataille contre la ville.

### Victoire d'Achab

20:13	Alors voici, un prophète s'approcha d'Achab, roi d'Israël, et lui dit : Ainsi parle YHWH : Vois-tu toute cette grande multitude ? Voici, je vais la livrer aujourd'hui entre tes mains et tu sauras que je suis YHWH.
20:14	Et Achab dit : Par qui ? Et il lui répondit : Ainsi parle YHWH : Ce sera par les serviteurs des chefs des provinces. Et il dit : Qui engagera le combat ? Et il lui répondit : Toi.
20:15	Alors il passa en revue les serviteurs des chefs des provinces qui furent 232. Et après eux, il dénombra tout le peuple, tous les fils d'Israël, et ils étaient 7 000.
20:16	Ils firent une sortie en plein midi, lorsque Ben-Hadad buvait et s'enivrait dans les tentes, lui et les 32 rois qui étaient venus à son secours.
20:17	Les serviteurs des chefs des provinces sortirent les premiers. Ben-Hadad envoya quelques-uns qui lui firent ce rapport en disant : Des hommes sont sortis de Samarie.
20:18	Et il dit : Qu'ils soient sortis pour la paix, ou qu'ils soient sortis pour faire la guerre, saisissez-les tous vivants.
20:19	Les serviteurs des chefs de province sortirent de la ville puis l'armée qui était après eux.
20:20	Chacun d'eux frappa son homme, de sorte que les Syriens s'enfuirent et Israël les poursuivit. Ben-Hadad, roi de Syrie, se sauva sur un cheval, avec des cavaliers.
20:21	Et le roi d'Israël sortit et frappa les chevaux et les chars, en sorte qu'il fit éprouver une grande défaite aux Syriens.

### Achab monte de nouveau contre les Syriens

20:22	Alors le prophète s'approcha du roi d'Israël et lui dit : Va, fortifie-toi, considère et vois ce que tu auras à faire. Car, au retour de l'année, le roi de Syrie montera contre toi.
20:23	Or les serviteurs du roi de Syrie lui dirent : Leur elohîm est un elohîm de montagnes, c'est pourquoi ils ont été plus forts que nous. Mais combattons-les dans la plaine, certainement nous serons plus forts qu'eux.
20:24	Fais donc cette chose : écarte chacun de ces rois de sa place et remplace-les par des chefs.
20:25	Puis lève une armée pareille à celle que tu as perdue, avec autant de chevaux et de chars, puis nous les combattrons dans la plaine et l'on verra si nous ne sommes pas plus forts qu'eux. Il les écouta et fit ainsi.
20:26	Et il arriva, qu’au retour de l’année, Ben-Hadad dénombra les Syriens et monta à Aphek pour combattre contre Israël.
20:27	On fit aussi le dénombrement des enfants d'Israël. Ils reçurent des vivres et ils marchèrent à la rencontre des Syriens. Les enfants d'Israël campèrent vis-à-vis d'eux, semblables à deux petits troupeaux de chèvres, tandis que les Syriens remplissaient le pays.
20:28	Alors l'homme d'Elohîm vint et dit au roi d'Israël : Ainsi parle YHWH : Parce que les Syriens ont dit : YHWH est un elohîm des montagnes et non un elohîm des vallées, je livrerai entre tes mains toute cette grande multitude, et vous saurez que je suis YHWH.
20:29	Ils campèrent 7 jours les uns vis-à-vis des autres. Et il arriva, le septième jour, qu'ils entrèrent en bataille, et les enfants d'Israël tuèrent les Syriens, 100 000 hommes de pied en un seul jour.
20:30	Le reste s'enfuit à la ville d'Aphek, où la muraille tomba sur 27 000 hommes qui restaient. Et Ben-Hadad s'était enfui et il allait dans la ville de chambre en chambre.

### Faute d'Achab qui épargne Ben-Hadad

20:31	Ses serviteurs lui dirent : Voici, maintenant nous avons appris que les rois de la maison d'Israël sont des rois miséricordieux. Maintenant donc mettons des sacs sur nos reins et des cordes à nos têtes, sortons vers le roi d'Israël, peut-être qu'il te laissera la vie sauve.
20:32	Ils se mirent donc des sacs autour des reins et des cordes autour de leurs têtes. Ils allèrent auprès du roi d'Israël. Ils lui dirent : Ton serviteur Ben-Hadad dit : Laisse-moi la vie ! Achab répondit : Est-il encore vivant ? Il est mon frère.
20:33	Ces hommes tirèrent de là un bon augure, ils se hâtèrent de le prendre au mot et ils dirent : Ben-Hadad est-il ton frère ? Et il répondit : Allez, amenez-le ! Ben-Hadad vint vers lui, et il le fit monter sur son char.
20:34	Et Ben-Hadad lui dit : Je te rendrai les villes que mon père avait prises à ton père et tu te feras des rues en Damas comme mon père avait fait en Samarie. Et moi, répondit Achab, je te laisserai aller en faisant alliance. Il traita donc alliance avec lui et le laissa aller.
20:35	Alors un homme d'entre les fils des prophètes dit à son compagnon, sur l'ordre de YHWH : Frappe-moi, je te prie ! Mais l'homme refusa de le frapper.
20:36	Et il lui dit : Parce que tu n'as pas obéi à la parole de YHWH, voilà, quand tu m'auras quitté, un lion te frappera. Quand il se fut séparé de lui, un lion survint et le frappa.
20:37	Puis il trouva un autre homme, et lui dit : Frappe-moi, je te prie. Cet homme-là le frappa et il le blessa.
20:38	Après cela, le prophète s'en alla, et se plaça sur le chemin du roi et il se déguisa avec un bandeau sur ses yeux.
20:39	Lorsque le roi passa, il cria vers lui et dit : Ton serviteur était allé au milieu de la bataille, et voici quelqu'un s'étant retiré, m'a amené un homme, en disant : Garde cet homme, s'il vient à s'échapper, ta vie en répondra ou tu paieras un talent d'argent.
20:40	Et il est arrivé que comme ton serviteur faisait quelques affaires çà et là, cet homme a disparu. Et le roi d'Israël lui répondit : Telle est ta condamnation, tu l'as toi-même prononcée.
20:41	Alors le prophète ôta promptement le bandeau de dessus ses yeux et le roi d'Israël reconnut que c'était l'un des prophètes.
20:42	Et il dit : Ainsi parle YHWH : Parce que tu as laissé échapper de tes mains l'homme que j'avais voué à une entière destruction, ta vie répondra de sa vie, et ton peuple de son peuple.
20:43	Mais le roi d'Israël se retira en sa maison, maussade et furieux, et il arriva en Samarie.

## Chapitre 21

### Achab convoite la vigne de Naboth

21:1	Et voici ce qui arriva après ces choses : Naboth de Yizre`e'l avait une vigne à Yizre`e'l, près du palais d'Achab, roi de Samarie.
21:2	Achab parla à Naboth et lui dit : Donne-moi ta vigne, afin que j'en fasse un jardin potager car elle est proche de ma maison et je te donnerai à la place une vigne meilleure ou, si cela est bon à tes yeux, je te donnerai le prix en argent.
21:3	Mais Naboth répondit à Achab : Que YHWH me garde de te donner l'héritage de mes pères !
21:4	Et Achab rentra dans sa maison, maussade et furieux, à cause de cette parole que lui avait dite Naboth de Yizre`e'l, en disant : Je ne te donnerai pas l'héritage de mes pères ! Il se coucha sur son lit, détourna son visage et ne mangea rien.

### Machination de Jézabel

21:5	Alors Jézabel, sa femme, vint auprès de lui et lui dit : D'où vient que ton esprit est si maussade ? Et pourquoi ne manges-tu pas ?
21:6	Et il lui répondit : J'ai parlé à Naboth de Yizre`e'l et je lui ai dit : Donne-moi ta vigne pour de l'argent, ou si tu le désires, je te donnerai une autre vigne à sa place, mais il m'a dit : Je ne te céderai pas ma vigne !
21:7	Alors Jézabel, sa femme, lui dit : Est-ce bien toi maintenant qui exerces la royauté sur Israël ? Lève-toi, prends un repas et que ton cœur se réjouisse ! Je te donnerai la vigne de Naboth de Yizre`e'l.
21:8	Et elle écrivit au nom d'Achab des lettres qu'elle scella du sceau du roi, et elle envoya ces lettres aux anciens et nobles qui habitaient avec Naboth, dans sa ville.
21:9	Elle écrivit dans les lettres, disant : Publiez un jeûne et placez Naboth à la tête du peuple.
21:10	Mettez face à lui deux hommes, fils de Bélial, et qu'ils témoignent contre lui, en disant : Tu as maudit Elohîm et le roi ! Puis vous le mènerez dehors et le lapiderez afin qu'il meure.
21:11	Les hommes de la ville de Naboth, les anciens et les nobles qui habitaient dans sa ville, agirent comme Jézabel le leur avait dit, et d'après ce qui était écrit dans les lettres qu'elle leur avait envoyées.
21:12	Ils publièrent un jeûne et ils placèrent Naboth à la tête du peuple.
21:13	Les deux hommes, fils de Bélial vinrent et se mirent face à lui. Et ces hommes de Bélial témoignèrent contre Naboth devant le peuple en disant : Naboth a maudit Elohîm et le roi ! Puis ils le menèrent hors de la ville, ils le lapidèrent avec des pierres, et il mourut.
21:14	Après cela, ils envoyèrent dire à Jézabel : Naboth a été lapidé et il est mort.
21:15	Et il arriva que lorsque Jézabel apprit que Naboth avait été lapidé et qu'il était mort, Jézabel dit à Achab : Lève-toi, mets-toi en possession de la vigne de Naboth de Yizre`e'l, qu'il avait refusé de te donner pour de l'argent, car Naboth n'est plus en vie, il est mort.
21:16	Et il arriva que, quand Achab apprit que Naboth était mort, Achab se leva pour descendre à la vigne de Yizre`e'l afin d'en prendre possession.

### Jugement d'Achab et de Jézabel ; Achab s'humilie devant Elohîm

21:17	Alors la parole de YHWH vint à Eliyah, le Thishbite, en disant :
21:18	Lève-toi, descends au-devant d'Achab, roi d'Israël, lorsqu'il sera à Samarie. Le voilà dans la vigne de Naboth, où il est descendu pour en prendre possession.
21:19	Tu lui diras : Ainsi parle YHWH : Tu as assassiné et tu prends possession ! Tu lui diras : Ainsi parle YHWH : Comme les chiens ont léché le sang de Naboth, les chiens lécheront aussi ton propre sang.
21:20	Achab dit à Eliyah : M'as-tu trouvé mon ennemi ? Mais il lui répondit : Oui, je t'ai trouvé, parce que tu t'es vendu pour faire ce qui est mal aux yeux de YHWH.
21:21	Voici, je vais faire venir le malheur sur toi et je te consumerai, je retrancherai de chez Achab quiconque urine contre un mur, qu'il soit esclave ou libre en Israël.
21:22	Je rendrai ta maison semblable à la maison de Yarobam, fils de Nebath, et la maison de Baesha, fils d'Achiyah, parce que tu m'as provoqué à la colère et que tu as fait pécher Israël.
21:23	YHWH parla aussi contre Jézabel, en disant : Les chiens mangeront Jézabel près du rempart de Yizre`e'l.
21:24	Celui de la maison d'Achab qui mourra dans la ville, les chiens le mangeront, et celui qui mourra aux champs, les oiseaux des cieux le mangeront.
21:25	En effet, il n'y avait pas eu de personne comme Achab qui se soit vendu pour faire ce qui est mal aux yeux de YHWH, et sa femme Jézabel l'avait séduit.
21:26	Il s’est mis à agir d’une manière très abominable, en allant après les idoles, comme l'avaient fait les Amoréens que YHWH avait chassés de devant les enfants d'Israël.
21:27	Et il arriva qu'aussitôt qu'Achab entendit ces paroles, qu'il déchira ses vêtements et mit un sac sur son corps et jeûna. Il se tenait couché avec ce sac et il marchait lentement.
21:28	Et la parole de YHWH vint à Eliyah, le Thishbite, en disant :
21:29	As-tu vu comment Achab s'est humilié devant moi ? Parce qu'il s'est humilié devant moi, je ne ferai pas venir le malheur pendant sa vie, ce sera aux jours de son fils que je ferai venir le malheur sur sa maison.

## Chapitre 22

### Yehoshaphat aide Achab contre les Syriens

22:1	Et on resta 3 ans sans qu'il y eût guerre entre la Syrie et Israël.
22:2	Et il arriva, dans la troisième année, que Yehoshaphat, roi de Yéhouda, descendit vers le roi d'Israël.
22:3	Le roi d'Israël dit à ses serviteurs : Ne savez-vous pas que Ramoth en Galaad nous appartient ? Et nous ne nous inquiétons pas de la reprendre des mains du roi de Syrie !
22:4	Puis il dit à Yehoshaphat : Viendras-tu avec moi à la guerre contre Ramoth en Galaad ? Et Yehoshaphat répondit au roi d'Israël : Nous irons, moi comme toi, mon peuple comme ton peuple, et mes chevaux comme tes chevaux.

### Les prophètes de mensonge<!--2 Ch. 18:4-5,9-11.-->

22:5	Yehoshaphat dit encore au roi d'Israël : Consulte aujourd'hui, je te prie, la parole de YHWH.
22:6	Et le roi d'Israël rassembla les prophètes, au nombre de 400 environ, auxquels il dit : Irai-je à la guerre contre Ramoth en Galaad ou dois-je y renoncer ? Et ils répondirent : Monte, car Adonaï la livrera entre les mains du roi.
22:7	Mais Yehoshaphat dit : N'y a-t-il pas ici encore quelque prophète de YHWH afin que nous le consultions ?
22:8	Et le roi d'Israël dit à Yehoshaphat : Il y a encore un homme par qui l'on pourrait consulter YHWH, mais je le hais, car il ne prophétise rien de bon, mais seulement du mal : c'est Miykayeh, fils de Yimla. Yehoshaphat dit : Que le roi ne parle pas ainsi !
22:9	Alors le roi d'Israël appela un eunuque auquel il dit : Fais venir promptement Miykayeh, fils de Yimla.
22:10	Or, le roi d'Israël et Yehoshaphat, roi de Yéhouda, étaient assis chacun sur son trône, revêtus de leurs habits, sur la place qui se trouve à l'entrée de la porte de Samarie, et tous les prophètes prophétisaient en leur présence.
22:11	Tsidqiyah<!--Généralement traduit par « Sédécias ».-->, fils de Kenaana, s'était fait des cornes de fer et il dit : Ainsi parle YHWH : De ces cornes-ci tu heurteras les Syriens, jusqu'à les détruire.
22:12	Et tous les prophètes prophétisaient de même, en disant : Monte à Ramoth en Galaad ! Tu connaîtras le succès et YHWH la livrera entre les mains du roi.

### Miykayeh (Michée) annonce la défaite et la mort d'Achab<!--2 Ch. 18:6-8,12-27,28-34.-->

22:13	Or le messager qui était allé appeler Miykayeh, lui parla ainsi : Voici les paroles des prophètes, d'une seule bouche elles annoncent ce qui est bon au roi. Je t'en prie, que ta parole soit comme la parole de l'un d'eux ! Déclare ce qui est bon !
22:14	Mais Miykayeh lui répondit : YHWH est vivant ! Je déclarerai ce que YHWH me dira.
22:15	Il vint donc vers le roi, et le roi lui dit : Miykayeh, devons-nous aller à la guerre contre Ramoth en Galaad, ou bien nous en abstenir ? Et il lui dit : Montes-y ! Tu connaîtras le succès et YHWH la livrera entre les mains du roi.
22:16	Et le roi lui dit : Combien de fois me faudra-t-il te faire jurer de ne me dire que la vérité au Nom de YHWH ?
22:17	Et il répondit : J'ai vu tout Israël dispersé sur les montagnes comme un troupeau de brebis qui n'a pas de berger. Et YHWH a dit : Ces gens n'ont pas de seigneur. Que chacun retourne en paix dans sa maison !
22:18	Alors le roi d'Israël dit à Yehoshaphat : Ne t'ai-je pas bien dit que quand il est question de moi, il ne prophétise rien de bon, mais seulement du mal ?
22:19	Et Miykayeh lui dit : Écoute néanmoins la parole de YHWH ! J'ai vu YHWH assis sur son trône, et toute l'armée des cieux se tenant devant lui, à sa droite et à sa gauche.
22:20	Et YHWH a dit : Quel est celui qui séduira Achab, afin qu'il monte et qu'il tombe à Ramoth en Galaad ? Et celui-ci dit ainsi, et celui-là dit ainsi.
22:21	Alors un esprit s'avança et se tint devant YHWH, il déclara : Je le séduirai. Et YHWH lui dit : Comment ?
22:22	Et il répondit : Je sortirai et je serai un esprit de mensonge dans la bouche de tous ses prophètes. Et YHWH dit : Tu le séduiras et tu le vaincras aussi. Sors et fais comme tu l'as dit !
22:23	Et maintenant, voici, YHWH a mis un esprit de mensonge dans la bouche de tous tes prophètes qui sont ici et YHWH a prononcé du mal contre toi.
22:24	Alors Tsidqiyah, fils de Kenaana, s'approcha et frappa Miykayeh sur la joue, et dit : Par où l'Esprit de YHWH est-il sorti de moi pour s'adresser à toi ?
22:25	Et Miykayeh répondit : Voici, tu le verras le jour où tu iras de chambre en chambre pour te cacher.
22:26	Alors le roi d'Israël dit : Qu'on prenne Miykayeh et qu'on le mène vers Amon, capitaine de la ville et vers Yoash, le fils du roi.
22:27	Et tu diras : Ainsi a parlé le roi : Mettez cet homme en maison d'arrêt, et nourrissez-le du pain d'oppression et de l'eau d'oppression, jusqu'à ce que je revienne en paix.
22:28	Et Miykayeh répondit : Si tu reviens, si tu reviens en paix, YHWH n'a pas parlé par moi. Il dit aussi : Vous tous, peuples, entendez !
22:29	Le roi d'Israël monta avec Yehoshaphat, roi de Yéhouda, contre Ramoth en Galaad.
22:30	Et le roi d'Israël dit à Yehoshaphat : Je me déguiserai pour aller à la guerre, mais toi, revêts-toi de tes habits ! Le roi d'Israël donc se déguisa et alla à la guerre.
22:31	Or le roi de Syrie avait donné un ordre aux 32 chefs de ses chars, en disant : Vous ne combattrez ni petits ni grands, mais seulement le roi d'Israël.
22:32	Et il arriva que, quand les chefs des chars virent Yehoshaphat, ils dirent : C’est sûrement le roi d'Israël. Et ils s'approchèrent de lui pour le combattre, mais Yehoshaphat s'écria.
22:33	Et quand les chefs des chars virent que ce n'était pas le roi d'Israël, ils se détournèrent de lui.

### Mort d'Achab

22:34	Alors un homme tira au hasard avec son arc et frappa le roi d'Israël entre les jointures de la cuirasse. Et le roi dit à son conducteur de char : Tourne ta main et fais-moi sortir du camp, car je suis blessé.
22:35	La guerre fut si violente ce jour-là que le roi dut être maintenu debout sur son char face aux Syriens, et il mourut sur le soir. Le sang de sa blessure coulait à l'intérieur du char.
22:36	Au coucher du soleil, un cri retentissant passait par le camp, disant : Chacun dans sa ville et dans son pays !
22:37	Ainsi mourut le roi. Il fut ramené à Samarie et l'on enterra le roi à Samarie.
22:38	Lorsqu'on lava le char à l'étang de Samarie, les chiens léchèrent le sang d'Achab et les prostituées s'y baignèrent, selon la parole que YHWH avait prononcée<!--Voir 1 R. 21:19.-->.
22:39	Le reste des discours d'Achab, tout ce qu'il a accompli et quant à la maison d'ivoire qu'il construisit et toutes les villes qu'il a bâties, toutes ces choses ne sont-elles pas écrites dans le livre des discours du jour des rois d'Israël ?

### Règne de Yehoshaphat sur Yéhouda<!--2 Ch. 17:1-9.-->

22:40	Ainsi Achab se coucha avec ses pères. Et Achazyah, son fils, régna à sa place.
22:41	Or Yehoshaphat, fils d'Asa, régna sur Yéhouda la quatrième année d'Achab, roi d'Israël.
22:42	Yehoshaphat avait 35 ans lorsqu'il devint roi, et il régna 25 ans à Yeroushalaim. Sa mère s'appelait Azouba, fille de Shilchi.
22:43	Il marcha dans toute la voie d'Asa, son père. Il ne s'en détourna pas, faisant tout ce qui est droit aux yeux de YHWH.
22:44	Toutefois les hauts lieux ne disparurent pas : le peuple continuait à offrir des sacrifices et à brûler de l'encens sur les hauts lieux.
22:45	Yehoshaphat fit aussi la paix avec le roi d'Israël.
22:46	Le reste des discours de Yehoshaphat, ses actions puissantes et les guerres qu'il mena ne sont-ils pas écrits dans le livre des discours du jour des rois de Yéhouda ?
22:47	Il extermina du pays le reste des hommes prostitués qui se prostituent, qui était demeuré là depuis le temps d'Asa, son père.
22:48	Il n'y avait pas alors de roi en Édom, mais un roi y était établi.
22:49	Yehoshaphat construisit des navires de Tarsis pour aller chercher de l'or à Ophir mais il n'y alla pas, parce que les navires se brisèrent à Etsyôn-Guéber.
22:50	Alors Achazyah, fils d'Achab, dit à Yehoshaphat : Que mes serviteurs aillent sur les navires avec les tiens ! Mais Yehoshaphat n'accepta pas.

### Yehoram (Yoram) règne sur Yéhouda<!--2 Ch. 21:1.-->

22:51	Et Yehoshaphat s'endormit avec ses pères et fut enterré avec eux dans la cité de David, son père. Et Yehoram, son fils, régna à sa place.

### Achazyah règne sur Israël

22:52	Achazyah, fils d'Achab, régna sur Israël à Samarie la dix-septième année de Yehoshaphat, roi de Yéhouda. Et il régna 2 ans sur Israël.
22:53	Il fit ce qui est mal aux yeux de YHWH : il marcha sur la voie de son père, de sa mère et de celle de Yarobam, fils de Nebath, qui avait fait pécher Israël.
22:54	Il servit Baal, il se prosterna devant lui et il irrita YHWH, l'Elohîm d'Israël, comme l'avait fait son père.
