# Colossiens (Col.)

Auteur : Paulos (Paul)

Thème : La prééminence du Mashiah (Christ)

Date de rédaction : Env. 60 ap. J.-C.

Située en Asie Mineure, Colosses était une ville de Phrygie qui se trouvait à environ 200 kilomètres d'Éphèse.

Rédigée lors de la première captivité romaine de Paulos (Paul), la lettre aux Colossiens a pour but de rétablir la suprématie du Mashiah. En effet, cette assemblée – dont Épaphras, le probable fondateur, s'était converti à Éphèse au cours des trois années que Paulos y passa – était sous l'influence d'enseignements séducteurs basés sur le gnosticisme. Cette philosophie à la fois attrayante et très dangereuse prônait entre autres le salut par la connaissance et le dualisme.

## Chapitre 1

### Introduction

1:1	Paulos, apôtre de Yéhoshoua Mashiah, par la volonté d'Elohîm, et le frère Timotheos,
1:2	aux saints et frères, fidèles en Mashiah, qui sont à Colosses : que la grâce et la paix vous soient données de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !
1:3	Nous rendons grâces à Elohîm et Père de notre Seigneur Yéhoshoua Mashiah, et nous prions toujours pour vous,
1:4	ayant entendu parler de votre foi en Mashiah Yéhoshoua, et de votre amour envers tous les saints,
1:5	à cause de l'espérance qui vous est réservée dans les cieux, et dont vous avez eu précédemment connaissance par la parole de la vérité, par l'Évangile,
1:6	qui est parvenu jusqu'à vous, de même que dans le monde entier. Et il y porte du fruit, comme aussi parmi vous, depuis le jour où vous avez entendu et connu la grâce d'Elohîm dans la vérité,
1:7	ainsi que vous avez aussi été instruits par Épaphras, notre cher compagnon de service, qui est pour vous un fidèle serviteur du Mashiah,
1:8	et qui nous a fait connaître votre amour par l'Esprit.

### Prière de Paulos (Paul) pour les Colossiens

1:9	C'est pourquoi depuis le jour où nous l'avons appris, nous ne cessons de prier pour vous, et de demander à Elohîm que vous soyez remplis de la connaissance précise et correcte de sa volonté, en toute sagesse et intelligence spirituelle,
1:10	afin que vous vous conduisiez d'une manière digne du Seigneur pour lui plaire en toutes choses, portant des fruits en toutes sortes de bonnes œuvres, et croissant dans la connaissance précise et correcte d'Elohîm,
1:11	étant fortifiés en toute puissance selon sa force glorieuse, en toute patience et constance avec joie,
1:12	rendant grâces au Père, qui nous a rendus capables d'avoir part à l'héritage des saints dans la lumière,
1:13	qui nous a délivrés de l'autorité des ténèbres<!--Voir Lu. 22:53 et Ac. 26:18.-->, et nous a transportés dans le Royaume du Fils de son amour,
1:14	en qui nous avons la rédemption par le moyen de son sang, le pardon des péchés.

### Yéhoshoua (Jésus), le Créateur

1:15	Lequel est l'image d'Elohîm invisible, le premier-né<!--Dans les Écritures, l'expression « premier-né » est appliquée au Seigneur pour exprimer trois réalités. Tout d'abord, on parle de Yéhoshoua (Jésus) en tant que premier-né de Myriam (Marie), c'est-à-dire son fils aîné (Lu. 2:6-7). Ensuite, on trouve cette expression au sens figuré, pour marquer une distinction (par exemple concernant Israël, Ex. 4:22) ou désigner la particularité et la suprématie d'une personne. Ainsi, bien que David était le dernier-né de son père Isaï (Ps. 89:28), Elohîm en fit « le premier-né, le plus élevé des rois de la Terre » (Ps. 89:28). Il en va de même pour Yéhoshoua Mashiah. Il n'est pas le premier-né de la création dans le sens de rang de naissance ou de création, autrement Paulos (Paul) aurait employé le terme grec « prôtoktisis » qui signifie littéralement « premier créé », au lieu de « prôtotokos », c'est-à-dire « premier-né ». Il faut donc voir dans cette expression un titre de supériorité et de hiérarchie, pour marquer sa prééminence. En effet, la parole d'Elohîm déclare clairement que le Seigneur Yéhoshoua Mashiah est l'Aleph (Alpha en grec), le commencement de toutes choses (Ap. 1:8, 21:6, 22:13), le Créateur suprême (Ge. 1:1, 2:7 ; Es. 45:11-18 ; Ps. 104:30 ; Job 33:4 ; Jn. 1:3 ; 1 Co. 8:6 ; Col. 1:12-16 ; Ap. 14:7, 22:3). D'ailleurs, il l'a lui-même affirmé sans ambiguïté : « Avant qu'Abraham soit venu à l'existence, JE SUIS » (Jn. 8:58). Enfin, Yéhoshoua Mashiah est aussi appelé le premier-né d'entre les morts (Col. 1:18). Cela ne signifie pas qu'il a été le premier à ressusciter, car il y a eu plusieurs résurrections avant la sienne, mais il fut le premier à ressusciter avec un corps glorieux. Sa résurrection est donc le gage de la promesse de la résurrection de tous ceux qui ont foi en lui (Jn. 3:16).--> de toute la création.
1:16	Parce que c'est par lui qu'ont été créées toutes les choses qui sont dans les cieux et sur la Terre, les visibles et les invisibles, soit les trônes, ou les seigneuries, ou les principautés, ou les puissances, toutes choses ont été créées par son moyen et pour lui.
1:17	Et il est avant toutes choses, et toutes choses subsistent par lui.
1:18	Et c'est lui qui est la tête du corps de l'Assemblée. Il est le commencement et le premier-né d'entre les morts, pour devenir celui qui tient la première place en toutes choses,
1:19	parce qu'en lui toute la plénitude s’est plu à habiter.
1:20	Et, par son moyen, à réconcilier toutes choses avec lui-même, aussi bien les choses qui sont dans les cieux que celles qui sont sur la Terre, ayant fait la paix par lui au moyen du sang de sa croix.
1:21	Et vous qui étiez autrefois étrangers et ennemis par votre pensée et par vos mauvaises œuvres, il vous a maintenant réconciliés
1:22	dans le corps de sa chair, au moyen de sa mort, pour vous présenter saints et sans défaut et irréprochables devant lui.
1:23	Si toutefois vous demeurez fondés et inébranlables dans la foi, et sans vous détourner de l'espérance de l'Évangile que vous avez entendu, lequel est prêché à toute créature qui est sous le ciel, dont moi Paulos, j'ai été fait le serviteur.
1:24	Je me réjouis maintenant dans mes souffrances pour vous et j'accomplis dans ma chair ce qui manque aux tribulations du Mashiah pour son corps, qui est l'Assemblée.
1:25	C'est d'elle que je suis devenu le serviteur, selon l'administration<!--1 Co. 9:17.--> qu'Elohîm m'a donnée auprès de vous, afin que j'exécute pleinement la parole d'Elohîm,
1:26	ce mystère qui avait été caché de tous les âges et de toutes les générations, mais qui est maintenant manifesté à ses saints,
1:27	à qui Elohîm a voulu faire connaître quelle est la richesse de la gloire de ce mystère parmi les nations : Mashiah en vous, l'espérance de la gloire.
1:28	C'est lui que nous annonçons, en exhortant tout être humain et en enseignant tout être humain en toute sagesse, afin de présenter<!--Ou « offrir ». Voir 2 Co. 11:2.--> tout être humain parfait en Yéhoshoua Mashiah.
1:29	C'est aussi à quoi je travaille, en combattant selon sa puissance<!--Le terme « puissance » vient du grec « energeia » qui signifie « action », « fonctionnement », « compétence », ou encore « force à l'œuvre dans ». Ce mot est utilisé seulement pour parler du pouvoir surhumain que ce soit celui d'Elohîm ou celui du diable (Ep. 1:19 ; Ph. 3:21 ; 2 Ti. 2:9).--> qui agit puissamment en moi.

## Chapitre 2

### Le combat de Paulos (Paul)

2:1	Car je veux que vous sachiez quel grand combat j'ai à votre sujet et au sujet de ceux qui sont à Laodicée, et de tous ceux qui n'ont pas vu mon visage dans la chair,
2:2	afin que leurs cœurs soient consolés, étant unis ensemble dans l'amour et enrichis d'une pleine intelligence, pour la connaissance précise et correcte du mystère d'Elohîm le Père, et du Mashiah,
2:3	en qui sont cachés tous les trésors de la sagesse et de la connaissance.

### Mise en garde contre les discours séduisants et la philosophie<!--1 Co. 2:4 ; Ro. 16:17-18 ; 2 Pi. 2:3.-->

2:4	Or je dis cela afin que personne ne vous trompe par de faux raisonnements avec un discours persuasif.
2:5	Car, si je suis absent de corps, néanmoins je suis avec vous en esprit, me réjouissant et voyant votre ordre et la fermeté de votre foi que vous avez en Mashiah.
2:6	Ainsi, comme vous avez reçu Mashiah Yéhoshoua, le Seigneur, marchez en lui,
2:7	étant enracinés et édifiés en lui et affermis dans la foi, telle qu'on vous l'a enseignée, abondant en elle avec action de grâce.
2:8	Prenez garde que personne ne fasse de vous sa proie<!--Emporter du butin, emmener quelqu'un comme captif et esclave, conduire hors de la vérité et assujettir à sa domination.--> par le moyen de la philosophie et d'une vaine tromperie, selon la tradition des humains, selon les rudiments du monde, et non selon Mashiah.

### La divinité du Mashiah

2:9	Parce qu'en lui habite corporellement toute la plénitude de la divinité<!--En Yéhoshoua Mashiah (Jésus-Christ) habite toute la plénitude de la divinité. Il est l'Elohîm Tout-Puissant.-->.
2:10	Et vous êtes comblés<!--Vient du grec « pleroo » qui signifie aussi « remplir jusqu'au bord », « amener à la réalisation, réaliser », « accomplir ». Voir Ph. 4:19.--> en lui, qui est le chef de toute principauté et de toute autorité.

### L'œuvre de la croix

2:11	C'est en lui aussi que vous êtes circoncis d'une circoncision qui n’a pas été faite avec la main, par le dépouillement du corps des péchés de la chair, par la circoncision du Mashiah.
2:12	Ayant été ensevelis avec lui dans le baptême, en lui aussi vous êtes ressuscités ensemble par le moyen de la foi en la puissance d'Elohîm qui l'a ressuscité des morts.
2:13	Et lorsque vous étiez morts dans vos fautes, et dans l'incirconcision de votre chair, il vous a vivifiés ensemble avec lui, vous ayant gratuitement pardonné toutes vos fautes.
2:14	Il a effacé l'acte dont les ordonnances étaient contre nous et qui nous était contraire, et il l'a entièrement aboli en le clouant à la croix.
2:15	Il a dépouillé les principautés et les autorités, et les a exposées publiquement en spectacle, en triomphant d'elles par la croix.

### Mise en garde contre les commandements et les doctrines des hommes

2:16	Que personne donc ne vous juge sur un aliment ou sur une boisson, ou en matière de fêtes, de nouvelles lunes ou de shabbats,
2:17	ce n’est que l'ombre des choses à venir, mais le corps est en Mashiah.
2:18	Que personne ne vous trompe sur le prix de la victoire, par humilité, prenant plaisir au culte des anges et faisant une incursion hostile dans les choses qu'il n'a pas vues, étant enflé d’un vain orgueil par les pensées de sa chair,
2:19	et ne tenant pas fermement la tête, dont tout le corps étant joint et ajusté ensemble par des jointures et des liens, s'accroît d'un accroissement d'Elohîm.
2:20	Si donc vous êtes morts avec le Mashiah quant aux rudiments du monde, pourquoi vous impose-t-on ces ordonnances, comme si vous viviez dans le monde :
2:21	Ne prends pas ! Ne goûte pas ! Ne touche pas !
2:22	Choses qui sont toutes vouées à la corruption par l'usage, selon les commandements et les doctrines des humains !
2:23	Qui ont, en effet, un renom<!--Ou un discours de sagesse.--> de sagesse avec leur culte volontaire, leur humilité et leur rigoureux traitement du corps, mais qui n'ont en fait aucune valeur et ne contribuent qu'à la satisfaction de la chair.

## Chapitre 3

### Rechercher les choses d'en haut

3:1	Si donc vous êtes ressuscités avec le Mashiah, cherchez les choses d'en haut, où le Mashiah est assis à la droite d'Elohîm.
3:2	Pensez<!--Voir Ro. 8:5.--> aux choses d'en haut, et non à celles qui sont sur la Terre.
3:3	Car vous êtes morts, et votre vie est cachée avec le Mashiah en Elohîm.
3:4	Quand le Mashiah, notre vie, apparaîtra, alors vous apparaîtrez aussi avec lui dans la gloire.

### La mort à soi en pratique

3:5	Faites donc mourir vos membres qui sont sur la Terre : la relation sexuelle illicite, l'impureté, la convoitise, le mauvais désir et la cupidité qui est une idolâtrie.
3:6	C'est à cause de ces choses que la colère d'Elohîm vient sur les fils de l'obstination,
3:7	parmi lesquels vous marchiez autrefois quand vous viviez dans ces choses.
3:8	Mais maintenant, vous aussi, mettez de côté<!--Hé. 12:1.--> toutes ces choses : la colère, la fureur, la malice, la calomnie, et qu'aucun discours obscène ne sorte de votre bouche.
3:9	Ne vous mentez pas les uns aux autres, car vous vous êtes dépouillés du vieil homme et de ses œuvres,
3:10	et vous avez revêtu le nouvel homme qui se renouvelle dans la connaissance précise et correcte, selon l'image de celui qui l'a créé.
3:11	Là, il n'y a pas Grec et Juif, circoncision et incirconcision, barbare, Scythe<!--« Grossier ou rude ». Un Scythe, un habitant de la Scythie, l'actuelle Russie. Pour la plupart des nations civilisées de l'antiquité, les Scythes étaient considérés comme les plus sauvages des barbares.-->, esclave, libre, mais le Mashiah est tout et en tous.
3:12	Ainsi donc, comme des élus d'Elohîm, saints et bien-aimés, revêtez-vous des entrailles de miséricorde, de bonté, d'humilité, de douceur, de patience.
3:13	Vous supportant les uns les autres et vous pardonnant réciproquement. Et si l'un de vous a une plainte à porter contre quelqu'un, comme le Mashiah vous a pardonné, vous aussi faites-en de même.
3:14	Mais par-dessus toutes ces choses, l'amour, qui est le lien de la perfection.
3:15	Et que la paix d'Elohîm, à laquelle aussi vous êtes appelés pour être un seul corps, règne<!--Le mot grec signifie aussi « diriger », « contrôler », « gouverner », « décider », « être un arbitre ».--> dans vos cœurs. Et soyez reconnaissants.
3:16	Que la parole du Mashiah habite abondamment en vous en toute sagesse ! Vous instruisant et vous exhortant les uns les autres par des psaumes, par des hymnes et des cantiques spirituels, chantant dans votre cœur au Seigneur avec reconnaissance.
3:17	Et quoi que vous fassiez, en parole ou en œuvre, faites tout au Nom du Seigneur Yéhoshoua, rendant grâces à notre Elohîm et Père par lui.

### La famille selon Elohîm

3:18	Femmes, soyez soumises à vos maris, comme il convient dans le Seigneur<!--Ep. 5:22.-->.
3:19	Maris, aimez vos femmes et ne soyez pas amers contre elles<!--Ep. 5:25.-->.
3:20	Enfants, obéissez à vos parents en toutes choses, car cela est agréable au Seigneur<!--Ep. 6:1-2.-->.
3:21	Pères, n'irritez pas vos enfants<!--Ep. 6:4.--> afin qu'ils ne se découragent pas.

### Les rapports entre esclaves et maîtres selon Elohîm

3:22	Esclaves, obéissez en toutes choses à vos seigneurs selon la chair, ne les servant pas seulement lorsqu'ils ont l'œil sur vous, comme si vous cherchiez à plaire aux hommes, mais avec simplicité de cœur, dans la crainte d'Elohîm<!--Ep. 6:5-6.-->.
3:23	Et quoi que vous fassiez, travaillez-y de toute votre âme, comme pour le Seigneur et non pas pour les humains,
3:24	sachant que vous recevrez du Seigneur l'héritage pour récompense. Car c'est du Seigneur le Mashiah que vous êtes esclaves.
3:25	Mais celui qui agit injustement recevra ce qu'il aura fait injustement : il n'y a pas d'égard à l'apparence des personnes.

## Chapitre 4

4:1	Seigneurs, donnez à vos esclaves ce qui est juste et équitable, sachant que vous avez, vous aussi, un Seigneur dans les cieux.

### La persévérance dans la prière

4:2	Persévérez dans la prière, veillant en elle avec action de grâce.
4:3	Priez aussi tous ensemble pour nous, afin qu'Elohîm nous ouvre une porte pour la parole, afin d'annoncer le mystère du Mashiah pour lequel aussi je suis prisonnier,
4:4	afin que je le fasse connaître comme je dois en parler.
4:5	Conduisez-vous avec sagesse envers ceux du dehors et rachetez le temps.
4:6	Que votre parole soit toujours assaisonnée de sel, avec grâce, afin que vous sachiez comment vous devez répondre à chacun.

### Salutations

4:7	Tuchikos, notre frère bien-aimé et fidèle serviteur, et mon compagnon de service en notre Seigneur, vous apprendra tout ce qui me concerne.
4:8	Je l'envoie exprès vers vous afin qu'il connaisse ce qui vous concerne et qu'il console vos cœurs,
4:9	avec Onesimos, notre fidèle et bien-aimé frère, qui est l'un des vôtres. Ils vous feront connaître tout ce qui se passe ici.
4:10	Aristarchos, mon compagnon de captivité, vous salue, ainsi que Markos, le cousin de Barnabas, au sujet duquel vous avez reçu un ordre : s'il vient chez vous, recevez-le.
4:11	Et Yéhoshoua, appelé Juste, vous salue aussi. Ils sont de la circoncision. Ce sont mes seuls compagnons d'œuvre pour le Royaume d'Elohîm, et ils ont été pour moi une consolation.
4:12	Épaphras, qui est des vôtres, et esclave de Yéhoshoua Mashiah, vous salue. Il ne cesse de combattre pour vous dans ses prières, afin que, parfaits et accomplis dans toute la volonté d'Elohîm, vous teniez debout.
4:13	Car je lui rends témoignage qu'il a un grand zèle pour vous, et pour ceux de Laodicée, et pour ceux d'Hiérapolis.
4:14	Loukas, le médecin bien-aimé, vous salue, ainsi que Démas.
4:15	Saluez les frères qui sont à Laodicée, et Nymphas, avec l'assemblée qui est dans sa maison.
4:16	Et quand cette lettre aura été lue chez vous, faites en sorte qu'elle soit aussi lue dans l'assemblée des Laodicéens, et que vous lisiez aussi celle qui viendra de Laodicée.
4:17	Et dites à Archippos : Considère le service que tu as reçu dans le Seigneur afin de bien le remplir.
4:18	Je vous salue, moi Paulos, de ma propre main. Souvenez-vous de mes liens. Que la grâce soit avec vous ! Amen !
