# Vayiqra (Lévitique) (Lé.)

Signification : Et il (YHWH) appela

Auteur : Probablement Moshè (Moïse)

Thème : La sainteté

Date de rédaction : Env. 1450 – 1410 av. J.-C.

Après avoir construit et dressé le tabernacle selon le modèle que YHWH avait donné à Moshè, les fils d'Israël reçurent le détail des prescriptions relatives aux offrandes, sacrifices et fêtes en l'honneur de YHWH.

Ce livre, dont le nom en français tire son origine de Lévi, explique la manière dont Aaron et ses fils devaient exercer la prêtrise et amener le peuple à s'approcher d'Elohîm dans le respect de ses ordonnances.

Les lois que Moshè avait recueillies présentent la voie du pardon, laquelle est impossible sans effusion de sang. Bien que les mêmes sacrifices furent réitérés tous les ans, ces préceptes mettaient en évidence l'impuissance de l'être humain à atteindre la justice d'Elohîm par ses propres moyens.

## Chapitre 1

### L'holocauste<!--Lé. 6:1-6.-->

1:1	YHWH appela Moshè et lui parla de la tente d'assignation en disant :
1:2	Parle aux enfants d'Israël, et dis-leur : Lorsqu'un humain parmi vous présentera à YHWH une offrande d'une bête à quatre pattes, il fera son offrande de gros ou de menu bétail.
1:3	Si son offrande pour un holocauste est de gros bétail, il présentera un mâle sans défaut<!--L'holocauste était le sacrifice pour l'expiation par excellence. Contrairement aux autres sacrifices, l'holocauste était entièrement consumé sur l'autel. Il symbolisait d'une part le sacrifice parfait du Mashiah (Christ) et d'autre part notre vie volontairement offerte à Elohîm (Ro. 12:1). Les animaux aptes à être offerts en holocauste devaient être des mâles sans défaut :\\- Le veau (Lé. 1:5), image du Mashiah, l'humble serviteur, soumis et obéissant (Mt. 20:28 ; Ph. 2:5-8).\\- L'agneau ou le chevreau, image du Mashiah qui livre sa vie à la croix sans résistance ni contestation, et qui prend sur lui nos péchés (Es. 53:7 ; Mt. 26:63 ; Ac. 8:32).\\- Les tourterelles ou les jeunes pigeons, image de la simplicité du Mashiah (Mt. 10:16).\\Toutes les étapes de la réalisation de ce sacrifice enseignent le disciple sur la mort à soi-même et le dépouillement des œuvres de la chair (Ga. 5:19-21).\\Le sang de l'animal égorgé devait être répandu sur l'autel (Lé. 1:5), image de la croix. L'âme (contenue dans le sang selon Lé. 17:14), liée à la chair et à ses désirs, doit être crucifiée (Ga. 2:20, 5:24). L'objet de la mise à mort était certainement un couteau tranchant comme une épée, image de la parole d'Elohîm (Hé. 4:12). La mise en pratique de la Parole nous amène nécessairement à nous séparer du monde et à renoncer à soi-même.--> ; il la présentera de son bon gré à l'entrée de la tente d'assignation devant YHWH<!--Ex. 29:10-11.-->.
1:4	Et il posera sa main sur la tête de l'holocauste, et il sera agréé en sa faveur pour faire la propitiation pour lui.
1:5	On tuera le jeune taureau devant YHWH et les fils d'Aaron, les prêtres, présenteront le sang et aspergeront de sang le pourtour de l'autel qui est à l'entrée de la tente d'assignation.
1:6	On dépouillera l'holocauste et on le coupera en morceaux.
1:7	Les fils du prêtre Aaron mettront le feu sur l'autel et arrangeront du bois sur le feu.
1:8	Les fils d'Aaron, les prêtres, arrangeront les morceaux, la tête et la graisse sur le bois qui sera au feu sur l'autel.
1:9	Mais il lavera avec de l'eau les entrailles et les jambes. Le prêtre brûlera toutes ces choses sur l'autel. C'est un holocauste, un sacrifice consumé par le feu, un parfum apaisant pour YHWH.
1:10	Si son offrande est un holocauste de menu bétail, d'entre les agneaux ou d'entre les chèvres, il présentera un mâle sans défaut.
1:11	On le tuera à côté de l'autel, vers le nord, devant YHWH. Les prêtres, fils d'Aaron, aspergeront de son sang le pourtour de l'autel.
1:12	On le coupera en morceaux, avec sa tête et sa graisse. Le prêtre les arrangera sur le bois qui sera au feu sur l'autel.
1:13	Mais il lavera avec de l'eau les entrailles et les jambes. Puis le prêtre présentera toutes ces choses et les brûlera sur l'autel. C'est un holocauste, un sacrifice consumé par le feu, un parfum apaisant pour YHWH<!--Ez. 40:38.-->.
1:14	Si son offrande à YHWH est un holocauste d'oiseaux, il présentera son offrande de tourterelles, ou de jeunes pigeons.
1:15	Le prêtre l'apportera sur l'autel, lui ouvrira la tête avec l'ongle, la brûlera sur l'autel, et il en exprimera le sang contre un côté de l'autel.
1:16	Il ôtera son jabot avec ses plumes, et le jettera près de l'autel, vers l'orient, dans le lieu où seront les cendres.
1:17	Il le déchirera avec ses ailes, sans le séparer. Le prêtre le brûlera sur l'autel, sur le bois qui sera au feu. C'est un holocauste, un sacrifice consumé par le feu, un parfum apaisant pour YHWH.

## Chapitre 2

### L'offrande de grain<!--Lé. 6:7-16.-->

2:1	Quand une personne présentera en offrande une offrande de grain<!--L'offrande de grain correspond aux perfections de la vie du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) en tant qu'homme. Ce sacrifice ne comporte ni victime ni sang, mais seulement de la farine, de l'huile, de l'encens et du sel. Yéhoshoua, le grain de blé (Jn. 12:24), a été complètement broyé, pétri et oint d'huile, éprouvé par toutes sortes de douleurs. Sa vie sainte était pour le Père un parfum de bonne odeur. Son amour pour les âmes, sa dépendance totale au Père, sa persévérance, sa douceur, sa sagesse et sa bonté, n'ont pas varié malgré toutes les souffrances par lesquelles il est passé. Voilà quelques-uns des fruits admirables qui correspondent à l'offrande de gâteau saupoudrée d'encens. Le levain, image du péché (1 Co. 5:6-8), n'y entrait pas, ni le miel, symbole des affections humaines (Pr. 5:3). Quant au sel, il préserve de la corruption des aliments, il est comparé à la saveur des disciples du Mashiah (Mt. 5:13).--> à YHWH, son offrande sera de fine farine. Il versera de l'huile dessus et mettra de l'encens.
2:2	Elle l'apportera aux fils d'Aaron, les prêtres, et le prêtre prendra une pleine poignée de cette fine farine, et de l'huile, avec tout l'encens, et il brûlera son souvenir<!--En hébreu « azkarah », offrande de souvenir, la portion de nourriture offerte et qui est consumée.--> sur l'autel. C'est une offrande, un parfum apaisant pour YHWH.
2:3	Ce qui restera de l'offrande de grain sera pour Aaron et ses fils. C'est le saint des saints parmi les offrandes consumées par le feu à YHWH.
2:4	Si tu présentes en offrande une offrande de grain cuite au four, ce sera de fine farine, des gâteaux sans levain, pétris avec de l'huile, et des galettes sans levain, ointes d'huile.
2:5	Si ton offrande est une offrande de grain cuite sur la plaque, elle sera de fine farine pétrie à l'huile, sans levain.
2:6	Tu la rompras en morceaux, et tu verseras de l'huile sur elle. C'est une offrande de grain.
2:7	Si ton offrande est une offrande de grain cuite sur le gril, elle sera faite de fine farine avec de l'huile.
2:8	Puis tu apporteras à YHWH l'offrande de grain qui sera faite de ces choses, et on la présentera au prêtre, qui l'apportera sur l'autel.
2:9	Le prêtre lèvera de l'offrande de grain, son souvenir, et le brûlera sur l'autel. C'est une offrande consumée par le feu de bonne odeur à YHWH.
2:10	Ce qui restera de l'offrande de grain sera pour Aaron et ses fils. C'est le saint des saints parmi les offrandes consumées par le feu devant YHWH.
2:11	Aucune offrande de grain que vous offrirez à YHWH ne sera faite avec du levain, car vous ne brûlerez pas de levain ni de miel, parmi l'offrande consumée par le feu devant YHWH.
2:12	Vous pourrez bien les offrir à YHWH dans l'offrande des prémices, mais ils ne seront pas mis sur l'autel comme un parfum apaisant.
2:13	Tu mettras du sel<!--Voir No. 18:19 ; 2 Ch. 13:5. Le sel est un agent purificateur (2 R. 2:19-22). Il préserve de la corruption et conserve les aliments. Les chrétiens sont le sel de la terre (Mt. 5:13). Nos paroles doivent être assaisonnées de sel (Col. 4:6).--> sur toutes tes offrandes de grain, et tu ne laisseras pas ton offrande de grain manquer de sel, signe de l'alliance de ton Elohîm. Mais sur toutes tes offrandes, tu offriras du sel.
2:14	Si tu offres à YHWH une offrande de grain de prémices, tu offriras, pour l'offrande de grain de prémices, des épis qui commencent à mûrir, rôtis au feu, les grains de quelques épis bien grenés, broyés entre les mains.
2:15	Puis tu mettras de l'huile dessus, et tu placeras de l'encens dessus. C'est une offrande de grain.
2:16	Et le prêtre brûlera son souvenir, pris de ses grains broyés, et de son huile avec tout l'encens. C'est une offrande consumée par le feu à YHWH.

## Chapitre 3

### Le sacrifice d'offrande de paix<!--Lé. 7:11-21.-->

3:1	Si son offrande est un sacrifice d'offrande de paix<!--La plupart des traducteurs ont traduit par « sacrifice d'actions de grâces », or l'étymologie hébraïque du mot grâce est « shelem », ce qui signifie d'abord « paix ». Ce terme peut aussi vouloir dire « remerciement » ou « reconnaissance ». La racine de « shelem » est « shalam » : « être dans une alliance de paix », « être en paix ». Il est donc question ici d'une offrande de paix qui préfigure l'ensemble de l'œuvre de la croix accomplie par le Mashiah, et grâce à laquelle nous sommes réconciliés avec le Père (Col. 1:20 ; Ep. 2:14-17). Cette offrande préfigure aussi la Pâque, incarnée par le Mashiah (1 Co. 5:7) ainsi que le repas du Seigneur. En effet, sur cette offrande Elohîm prenait pour lui la graisse et la queue entière (Lé. 3:3,9-17), le prêtre prenait la poitrine et l'épaule droite (Lé. 7:31-34), et celui qui offrait l'animal pouvait consommer le reste avec d'autres personnes pures (Lé. 7:20). Ainsi, comme pour le repas du Seigneur, tous ceux qui étaient saints pouvaient participer au repas (1 Co. 11:27-34).-->, et qu'il offre du gros bétail, soit mâle, soit femelle, il l'offrira sans défaut devant YHWH.
3:2	Il posera sa main sur la tête de son offrande, et l'égorgera à l'entrée de la tente d'assignation, et les fils d'Aaron, les prêtres, aspergeront du sang le pourtour de l'autel.
3:3	Puis on présentera de cette offrande de paix, un sacrifice consumé par le feu à YHWH : la graisse qui couvre les entrailles et toute la graisse qui est sur les entrailles ;
3:4	les deux rognons avec la graisse qui est dessus et qui est sur les flancs. On ôtera le grand lobe qui est sur le foie pour le mettre avec les rognons.
3:5	Les fils d'Aaron brûleront tout cela sur l'autel, sur l'holocauste, qui sera sur le bois mis au feu. C'est une offrande consumée par le feu, un parfum apaisant pour YHWH<!--Ex. 29:13-25.-->.
3:6	Si son offrande pour le sacrifice d'offrande de paix à YHWH est de menu bétail, soit mâle, soit femelle, il la présentera sans défaut.
3:7	S'il offre un agneau pour son offrande, il le présentera devant YHWH.
3:8	Il posera sa main sur la tête de son offrande, et la tuera devant la tente d'assignation, et les fils d'Aaron aspergeront de son sang le pourtour de l'autel.
3:9	De ce sacrifice d'offrande de paix, il présentera en offrande consumée par le feu à YHWH, sa graisse, et sa queue entière, séparée jusqu'à l'échine, avec la graisse qui couvre les entrailles et toute la graisse qui est sur les entrailles,
3:10	les deux rognons avec la graisse qui est dessus, sur les flancs, et il ôtera le grand lobe qui est sur le foie, jusqu'aux rognons.
3:11	Le prêtre brûlera tout cela sur l'autel. C'est un aliment d'offrande consumée par le feu à YHWH<!--No. 28:2.-->.
3:12	Si son offrande est une chèvre, il la présentera devant YHWH.
3:13	Il posera sa main sur sa tête et la tuera devant la tente d'assignation. Les fils d'Aaron aspergeront de son sang le pourtour de l'autel.
3:14	Puis il présentera son offrande en sacrifice consumé par le feu à YHWH, la graisse qui couvre les entrailles et toute la graisse qui est sur les entrailles,
3:15	les deux rognons, et la graisse qui est dessus, sur les flancs, et il ôtera le grand lobe qui est sur le foie, jusqu'aux rognons.
3:16	Puis le prêtre brûlera toutes ces choses sur l'autel. C'est un aliment d'offrande consumée par le feu, un parfum apaisant. Toute graisse appartient à YHWH.
3:17	C'est un statut perpétuel pour vos descendants, dans toutes vos demeures : vous ne mangerez ni graisse ni sang<!--Ge. 9:4 ; 1 S. 14:33.-->.

## Chapitre 4

### Le sacrifice pour le péché<!--Lé. 6:17-23.-->

4:1	YHWH parla encore à Moshè, en disant :
4:2	Parle aux enfants d'Israël, et dis-leur : Quand une personne aura péché involontairement<!--Avant la promulgation de la torah, certains hommes péchaient par ignorance (Ro. 5:13). Néanmoins, ces péchés étaient tout de même punis et nécessitaient un sacrifice (Lé. 4:13-14 ; No. 15:22-36 ; Job 1). Sous la grâce, l'excuse du péché par ignorance ne peut être invoquée puisque nous sommes scellés du Saint-Esprit qui nous enseigne toutes choses (1 Jn. 2:20,27).--> contre l'un des commandements de YHWH, en commettant des choses qui ne doivent pas se faire, et qu'elle aura fait une de ces choses,
4:3	si c'est le prêtre oint qui a commis un péché semblable à quelque faute du peuple, il offrira à YHWH pour son péché qu'il aura fait, un jeune taureau sans défaut, pris du troupeau en sacrifice pour le péché.
4:4	Il amènera le taureau à l'entrée de la tente d'assignation, devant YHWH, il posera sa main sur la tête du taureau, et l'égorgera devant YHWH.
4:5	Et le prêtre oint prendra du sang du taureau, et l'apportera dans la tente d'assignation.
4:6	Le prêtre trempera son doigt dans le sang, et fera 7 fois l'aspersion du sang devant YHWH, en face du voile du lieu saint<!--No. 19:4.-->.
4:7	Le prêtre mettra aussi devant YHWH du sang sur les cornes de l'autel de l'encens aromatique, qui est dans la tente d'assignation. Il répandra tout le reste du sang du taureau au pied de l'autel de l'holocauste, qui est à l'entrée de la tente d'assignation.
4:8	Il enlèvera toute la graisse du taureau du sacrifice pour le péché : la graisse qui couvre les entrailles, et toute la graisse qui est sur les entrailles,
4:9	et les deux rognons avec la graisse qui les entoure, qui couvre les flancs, et il ôtera le grand lobe qui est sur le foie, pour le mettre sur les rognons ;
4:10	comme on les enlève du taureau du sacrifice d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->, et le prêtre brûlera toutes ces choses-là sur l'autel de l'holocauste.
4:11	Mais quant à la peau du taureau et toute sa chair, avec sa tête, ses jambes, ses entrailles, et ses excréments,
4:12	et même tout le taureau, il l'emportera hors du camp, dans un lieu pur, où l'on répand les cendres, et il le brûlera au feu sur du bois. Il sera brûlé au lieu où l'on répand les cendres.
4:13	Et si toute l'assemblée d'Israël a péché involontairement, et que la chose soit restée cachée aux yeux de la congrégation, s'ils ont fait contre l'un des commandements de YHWH une des choses qui ne se font pas et se sont rendus coupables,
4:14	et que le péché qu'ils ont fait vienne en évidence, l'assemblée offrira en sacrifice pour le péché un jeune taureau pris du troupeau, et on l'amènera devant la tente d'assignation.
4:15	Les anciens de l'assemblée poseront leurs mains sur la tête du taureau devant YHWH, et on égorgera le taureau devant YHWH.
4:16	Et le prêtre oint, apportera du sang du taureau dans la tente d'assignation.
4:17	Ensuite le prêtre trempera son doigt dans le sang, et en fera aspersion devant YHWH en face du voile, par 7 fois.
4:18	Et il mettra du sang sur les cornes de l'autel, qui est devant YHWH dans la tente d'assignation. Il répandra tout le reste du sang au pied de l'autel de l'holocauste, qui est à l'entrée de la tente d'assignation.
4:19	Il enlèvera toute sa graisse et la brûlera sur l'autel.
4:20	Et il fera de ce taureau comme il l'a fait du taureau du sacrifice pour le péché ; il fera de même. C’est ainsi que le prêtre fera sur eux la propitiation, et il leur sera pardonné.
4:21	Puis il emportera le taureau hors du camp, et le brûlera comme il a brûlé le premier taureau. Car c'est le sacrifice pour le péché de l'assemblée.
4:22	Et si un prince a péché involontairement, en violant l'un des commandements de YHWH, son Elohîm, ce qui ne doit pas se faire, et s'en soit rendu coupable,
4:23	et qu'on vienne à connaître le péché qu'il a commis, il amènera pour sacrifice un jeune bouc, mâle, sans défaut.
4:24	Il posera sa main sur la tête du bouc, et le tuera au lieu où l'on tue l'holocauste devant YHWH. C'est un sacrifice pour le péché.
4:25	Puis le prêtre prendra avec son doigt du sang du sacrifice pour le péché, et le mettra sur les cornes de l'autel de l'holocauste, et il répandra le reste de son sang au pied de l'autel de l'holocauste.
4:26	Et il brûlera toute sa graisse sur l'autel, comme la graisse du sacrifice d'offrande de paix. Ainsi le prêtre fera pour lui la propitiation de son péché, et il lui sera pardonné.
4:27	Et si quelqu'un du peuple du pays a péché involontairement, en violant l'un des commandements de YHWH, et en commettant des choses qui ne doivent pas se faire, et s'en soit rendu coupable,
4:28	et qu'on vienne à connaître le péché qu'il a commis, il amènera pour offrande une jeune chèvre, femelle, sans défaut, pour le péché qu'il a commis.
4:29	Et il posera sa main sur la tête du sacrifice pour le péché, et tuera le sacrifice pour le péché au lieu où l'on tue l'holocauste.
4:30	Puis le prêtre prendra du sang de la chèvre avec son doigt, et le mettra sur les cornes de l'autel de l'holocauste, et il répandra tout le reste de son sang au pied de l'autel.
4:31	Et il ôtera toute sa graisse, comme on ôte la graisse de dessus le sacrifice d'offrande de paix, et le prêtre la brûlera sur l'autel. Ce sera un parfum apaisant pour YHWH. Il fera propitiation pour lui et il lui sera pardonné.
4:32	Et s'il amène un agneau comme offrande, pour le sacrifice pour le péché, il amènera une femelle sans défaut.
4:33	Et il posera sa main sur la tête du sacrifice pour le péché, et on le tuera comme sacrifice pour le péché au lieu où l'on tue l'holocauste.
4:34	Puis le prêtre prendra avec son doigt du sang du sacrifice pour le péché, et le mettra sur les cornes de l'autel de l'holocauste, et il répandra tout le reste de son sang au pied de l'autel.
4:35	Et il ôtera toute sa graisse, comme on ôte la graisse de l'agneau du sacrifice d'offrande de paix, et le prêtre la brûlera sur l'autel, par-dessus les sacrifices de YHWH consumés par le feu, et il fera la propitiation pour lui, pour son péché qu'il aura commis, et il lui sera pardonné.

## Chapitre 5

### Le sacrifice de culpabilité<!--Lé. 7:1-7.-->

5:1	Et quand quelqu'un, étant témoin, après avoir entendu la parole du serment, aura péché en ne déclarant pas ce qu'il a vu ou ce qu'il sait, il portera son iniquité<!--Pr. 29:24.-->.
5:2	Et quand quelqu'un, à son insu, aura touché une chose souillée, soit le cadavre d'un animal impur, soit le cadavre d'une bête sauvage impure, soit le cadavre d'une chose grouillante impure, il sera souillé et coupable<!--Ag. 2:13-14 ; 2 Co. 6:17.-->.
5:3	Ou quand il aura touché à l'impureté d'un être humain, quelle que soit son impureté par laquelle il se rend impur, bien que cela lui soit resté caché, dès qu'il l'apprend, alors il devient coupable.
5:4	Ou quand quelqu'un, parlant légèrement de ses lèvres, a juré de faire du mal ou du bien, selon tout ce qu'un être humain profère légèrement en jurant, bien que cela lui soit resté caché, dès qu'il l'apprend, alors il devient coupable dans l'un de ces points-là.
5:5	Quand donc quelqu'un sera coupable sur l'un de ces points-là, il confessera ce en quoi il aura péché.
5:6	Et il amènera son sacrifice de culpabilité à YHWH pour le péché qu'il a commis : une femelle du menu bétail, soit une brebis, soit une chèvre, en sacrifice pour le péché. Et le prêtre fera pour lui la propitiation de son péché.
5:7	Et s'il n'a pas le moyen de trouver une brebis ou une chèvre, il apportera à YHWH pour le péché dont il s'est rendu coupable deux tourterelles ou deux jeunes pigeons, l'un comme sacrifice pour le péché, l'autre pour l'holocauste<!--Lu. 2:24.-->.
5:8	Il les apportera au prêtre, qui offrira premièrement celui qui est pour le sacrifice pour le péché. Il leur ouvrira la tête avec l'ongle, près du cou, sans la séparer ;
5:9	puis il fera l'aspersion du sang du sacrifice pour le péché sur un côté de l'autel, et ce qui restera du sang sera exprimé au pied de l'autel. C'est un sacrifice pour le péché.
5:10	Et il fera de l'autre un holocauste, selon l'ordonnance. Et le prêtre fera pour lui la propitiation pour son péché qu'il aura commis, et il lui sera pardonné.
5:11	Si celui qui aura péché n'a pas le moyen de trouver deux tourterelles ou deux jeunes pigeons, il apportera pour son offrande un dixième d'épha de fine farine en offrande pour le sacrifice pour le péché ; il ne mettra ni huile ni encens, car c'est un sacrifice pour le péché.
5:12	Il l'apportera au prêtre, et le prêtre qui en prendra une pleine poignée pour souvenir<!--Lé. 2:2.-->, la brûlera sur l'autel, comme offrande consumée par le feu à YHWH. C'est un sacrifice pour le péché.
5:13	Ainsi le prêtre fera la propitiation pour lui, pour le péché qu'il a commis dans l'une de ces choses, et il lui sera pardonné. Le reste sera pour le prêtre, comme étant une offrande de grain.
5:14	YHWH parla aussi à Moshè, en disant :
5:15	Quand quelqu'un aura commis une transgression et péchera involontairement à l'égard des choses consacrées à YHWH, il amènera un sacrifice de culpabilité à YHWH : un bélier sans défaut, pris dans le troupeau avec l'estimation que tu feras de la chose sainte, la faisant en sicles d'argent, selon le sicle du lieu saint, à cause de sa culpabilité.
5:16	Il restituera ce en quoi il aura péché à l'égard du lieu saint et il y ajoutera un cinquième par-dessus, et le donnera au prêtre. Le prêtre fera propitiation pour lui par le bélier du sacrifice de culpabilité, et il lui sera pardonné.
5:17	Lorsque quelqu'un aura péché, en violant, sans le savoir, l'un des commandements de YHWH, des choses qu'on ne doit pas faire, il sera coupable et portera son iniquité.
5:18	Il amènera donc en sacrifice de culpabilité au prêtre, un bélier sans tâche, pris du troupeau, avec l'estimation que tu feras du péché involontaire. Le prêtre fera la propitiation pour lui, du péché involontaire qu'il a commis et dont il ne se sera pas aperçu, et ainsi il lui sera pardonné.
5:19	C'est un sacrifice de culpabilité. Il est coupable, il est coupable envers YHWH.

### La restitution au jour du sacrifice de culpabilité<!--Lé. 7:1-7.-->

5:20	YHWH parla aussi à Moshè, en disant :
5:21	Quand quelqu'un aura péché et aura commis une transgression contre YHWH, en mentant à son prochain pour un dépôt, pour une chose qu'on aura mise entre ses mains, un vol, ou qu'il ait extorqué son prochain,
5:22	ou s'il a trouvé quelque chose perdue, et qu'il mente à ce sujet, ou s'il jure faussement concernant l'une des choses qu'un être humain fait en péchant ;
5:23	quand il péchera et se rendra coupable, il rendra la chose qu'il a volée ou extorquée, ou le dépôt qui lui a été donné en garde, ou la chose perdue qu'il a trouvée,
5:24	ou tout ce dont il aura juré faussement. Il le restituera totalement et il y ajoutera un cinquième. Il le donnera à celui à qui il appartenait, le jour de son sacrifice de culpabilité.
5:25	Et il amènera pour YHWH, au prêtre le sacrifice de culpabilité : un bélier sans défaut, pris du troupeau, avec l'estimation que tu feras de la culpabilité.
5:26	Et le prêtre fera propitiation pour lui devant YHWH, et il lui sera pardonné, quelle que soit la faute dont il se sera rendu coupable.

## Chapitre 6

### Torah de l'holocauste<!--Lé. 1:1-17.-->

6:1	YHWH parla aussi à Moshè, en disant :
6:2	Ordonne à Aaron et à ses fils, et dis-leur : Voici la torah de l'holocauste. L'holocauste demeurera sur le foyer de l'autel toute la nuit jusqu'au matin, et le feu brûlera sur l'autel.
6:3	Et le prêtre revêtira sa tunique de lin, mettra ses caleçons de lin sur son corps, et il enlèvera la cendre de l'holocauste que le feu aura consumé sur l'autel, puis il la mettra près de l'autel.
6:4	Alors il ôtera ses vêtements et portera d'autres vêtements pour transporter les cendres hors du camp, dans un lieu pur.
6:5	Et quant au feu qui brûle sur l'autel, il continuera de brûler, on ne l'éteindra pas ; le prêtre y brûlera du bois tous les matins, il préparera l'holocauste sur le bois, et y brûlera les graisses des offrandes de paix.
6:6	Le feu brûlera continuellement sur l'autel, on ne le laissera pas s'éteindre.

### Torah de l'offrande de grain<!--Lé. 2:1-16.-->

6:7	Et voici la torah de l'offrande de grain. Les fils d'Aaron l'offriront devant YHWH sur l'autel<!--No. 15:4.-->.
6:8	Et on lèvera une poignée de la fine farine de l'offrande de grain et de son huile, avec tout l'encens qui est sur l'offrande de grain, et on le brûlera sur l'autel comme souvenir dont le parfum est apaisant pour YHWH.
6:9	Aaron et ses fils mangeront ce qui en restera. Ils le mangeront sans levain dans un lieu saint, ils le mangeront dans le parvis de la tente d'assignation<!--Ex. 29:26-37.-->.
6:10	On ne le cuira pas avec du levain. Je leur ai donné cela pour leur portion d'entre mes offrandes consumées par le feu. C'est le saint des saints, comme le sacrifice pour le péché et le sacrifice de culpabilité.
6:11	Tout mâle d'entre les fils d'Aaron en mangera. C'est une ordonnance perpétuelle pour vos descendants concernant les offrandes consumées par le feu à YHWH. Quiconque les touchera sera sanctifié.
6:12	YHWH parla aussi à Moshè, en disant :
6:13	Voici l'offrande d'Aaron et de ses fils, qu'ils offriront à YHWH le jour où il sera oint : Un dixième d'épha de fine farine, comme offrande de grain perpétuelle, une moitié le matin et une moitié le soir.
6:14	Elle sera apprêtée sur une plaque avec de l'huile, tu l'apporteras mélangée et tu offriras les morceaux cuits de l'offrande de grain en parfum apaisant pour YHWH.
6:15	Et le prêtre d'entre ses fils qui sera oint à sa place fera cela. C'est une ordonnance perpétuelle devant YHWH. On le brûlera tout entier.
6:16	Toute offrande de grain d'un prêtre sera entièrement consumée : on n'en mangera pas.

### Torah du sacrifice pour le péché<!--Lé. 4:1-35.-->

6:17	YHWH parla aussi à Moshè, en disant :
6:18	Parle à Aaron et à ses fils, et dis-leur : Voici la torah du sacrifice pour le péché. Le sacrifice pour le péché sera tué devant YHWH, dans le même lieu où l'on tue l'holocauste. C'est le saint des saints.
6:19	Le prêtre qui offrira le sacrifice pour le péché le mangera ; il le mangera dans un lieu saint, dans le parvis de la tente d'assignation<!--No. 18:10.-->.
6:20	Quiconque touchera sa chair sera saint. Et s'il en jaillit du sang sur le vêtement, ce sur quoi il aura jailli sera lavé dans un lieu saint.
6:21	Et le vase de terre dans lequel on l'aura fait cuire sera brisé. Mais si on l'a fait cuire dans un vase de cuivre, il sera nettoyé et lavé dans l'eau.
6:22	Tout mâle d'entre les prêtres en mangera, car c'est le saint des saints.
6:23	Aucun sacrifice pour le péché, dont le sang sera porté dans la tente d'assignation pour faire la propitiation dans le lieu saint, ne sera mangé ; mais il sera brûlé au feu<!--Hé. 13:11.-->.

## Chapitre 7

### Torah du sacrifice de culpabilité<!--Lé. 5:1-26.-->

7:1	Et voici la torah du sacrifice de culpabilité. C'est le saint des saints.
7:2	Au même lieu où l'on tuera l'holocauste, on tuera le sacrifice de culpabilité. On aspergera de son sang le pourtour de l'autel.
7:3	Puis on en offrira toute la graisse, avec la queue, et toute la graisse qui couvre les entrailles,
7:4	les deux rognons, la graisse qui est dessus, sur les flancs, et le grand lobe qui est sur le foie, qu'on ôtera jusqu'aux rognons.
7:5	Le prêtre brûlera toutes ces choses sur l'autel comme offrande consumée par le feu à YHWH. C'est un sacrifice pour la culpabilité.
7:6	Tout mâle parmi les prêtres en mangera. Il sera mangé dans un lieu saint. Car c'est le saint des saints.
7:7	Le sacrifice pour le péché sera semblable au sacrifice de culpabilité, il y aura une même torah pour les deux ; et la victime appartiendra au prêtre qui aura fait la propitiation par elle.
7:8	Et le prêtre qui offrira l'holocauste de quelqu'un aura la peau de l'holocauste qu'il aura offert.
7:9	Et toute offrande de grain cuite au four, apprêtée sur le gril ou sur la plaque, appartiendra au prêtre qui l'offre.
7:10	Et toute offrande de grain pétrie à l'huile, ou sèche, sera pour tous les fils d'Aaron, pour l'homme comme pour son frère.

### Torah du sacrifice d'offrande de paix<!--Lé. 3:1-17.-->

7:11	Et voici la torah du sacrifice d'offrande de paix<!--Voir commentaire en Lé. 3:1.--> qu'on offrira à YHWH.
7:12	Si quelqu'un le présente comme action de grâces, il le présentera avec le sacrifice d'action de grâces<!--Confession, louange, actions de grâces.-->, des gâteaux sans levain pétris à l'huile, des galettes sans levain ointes d'huile, et des gâteaux de fine farine mêlés et pétris à l'huile.
7:13	En plus des gâteaux, il présentera pour son offrande du pain levé avec le sacrifice d'actions de grâces de ses offrandes de paix.
7:14	Il présentera une part de chaque offrande, qu'il offrira comme offrande élevée à YHWH. Elle sera pour le prêtre qui a fait l'aspersion du sang du sacrifice d'offrande de paix.
7:15	Mais la chair du sacrifice d'action de grâces de ses offrandes de paix sera mangée le jour où elle sera offerte ; on n'en laissera rien jusqu'au matin.
7:16	Et si le sacrifice de son offrande est un vœu ou une offrande volontaire, son sacrifice sera mangé le jour où il l'aura offert ; ce qui en restera sera mangé le lendemain.
7:17	Mais ce qui restera de la chair du sacrifice sera brûlé au feu le troisième jour.
7:18	Et si on mange de la chair du sacrifice d'offrande de paix le troisième jour, celui qui l'aura offert ne sera pas agréé, il ne lui sera pas imputé, ce sera une chose infâme, et la personne qui en mangera portera son iniquité<!--Ez. 4:14.-->.
7:19	Et la chair de ce sacrifice qui a touché quelque chose d'impur ne sera pas mangée, elle sera brûlée au feu. Mais quiconque sera pur, mangera de cette chair.
7:20	La personne ayant quelque impureté sur elle qui mangera de la chair du sacrifice d'offrande de paix qui appartient à YHWH, cette personne-là sera retranchée de son peuple.
7:21	Si une personne touche quelque chose d'impur, soit une impureté humaine, soit une bête impure, ou quelque chose d'autre d'impure, et qu'elle mange de la chair du sacrifice d'offrande de paix qui appartient à YHWH, cette personne-là sera retranchée d'entre son peuple.
7:22	YHWH parla à Moshè, en disant :
7:23	Parle aux enfants d'Israël, et dis-leur : Vous ne mangerez aucune graisse de bœuf, ni d'agneau, ni de chèvre.
7:24	On pourra se servir pour un usage quelconque de la graisse d'une bête morte ou de la graisse d'une bête déchirée ; mais vous n'en mangerez pas.
7:25	Car quiconque mangera de la graisse d'une bête que l'on offre comme offrande consumée par le feu à YHWH, la personne qui en mangera sera retranchée de son peuple.
7:26	Vous ne mangerez pas de sang, ni d'oiseaux, ni d'autres bêtes, dans aucune de vos demeures.
7:27	Toute personne qui aura mangé de quelque sang que ce soit, sera retranchée de son peuple.
7:28	YHWH parla à Moshè, en disant :
7:29	Parle aux enfants d'Israël, et dis-leur : Celui qui offrira son sacrifice d'offrande de paix à YHWH, apportera son offrande à YHWH, prise sur son sacrifice d'offrande de paix.
7:30	Il apportera de ses mains les offrandes consumées par le feu devant YHWH. Il apportera la graisse avec la poitrine, la poitrine pour l'agiter d'un côté et de l'autre devant YHWH.
7:31	Puis le prêtre brûlera la graisse sur l'autel, mais la poitrine sera pour Aaron et ses fils.
7:32	Vous donnerez aussi au prêtre pour offrande élevée, l'épaule droite de vos sacrifices d'offrande de paix<!--No. 18:18.-->.
7:33	Celui des fils d'Aaron qui offrira le sang et la graisse de l'offrande de paix aura pour sa part l'épaule droite.
7:34	Car je prends sur les enfants d'Israël, la poitrine qu'on agite d'un côté et de l'autre, et l'épaule qu'on présente par élévation, de tous les sacrifices d'offrande de paix, et je les donne à Aaron le prêtre et à ses fils, par une ordonnance perpétuelle, de la part des fils d'Israël.
7:35	C'est là, le droit de l'onction d'Aaron et de l'onction de ses fils sur ces offrandes consumées par le feu devant YHWH, depuis le jour où on les aura présentés pour exercer la prêtrise à YHWH.
7:36	Et c'est ce que YHWH ordonne aux enfants d'Israël de leur donner depuis le jour où on les aura oints. C'est un statut perpétuel parmi leurs descendants<!--Ex. 40:15.-->.
7:37	Telle est donc la torah de l'holocauste, de l'offrande de grain, du sacrifice pour le péché, du sacrifice pour la culpabilité, de la consécration et du sacrifice d'offrande de paix.
7:38	YHWH l'ordonna à Moshè sur la montagne de Sinaï, le jour où il ordonna aux enfants d'Israël d'offrir leurs offrandes à YHWH dans le désert de Sinaï.

## Chapitre 8

### Consécration d'Aaron et de ses fils

8:1	YHWH parla aussi à Moshè, en disant :
8:2	Prends Aaron et ses fils avec lui, les vêtements, l'huile d'onction, le taureau du sacrifice pour le péché, deux béliers et une corbeille de pains sans levain<!--Ex. 29:1-2, 30:25.--> ;
8:3	et convoque toute l'assemblée à l'entrée de la tente d'assignation.
8:4	Et Moshè fit comme YHWH lui avait ordonné. L'assemblée se rassembla à l'entrée de la tente d'assignation.
8:5	Moshè dit à l'assemblée : Voici ce que YHWH a ordonné de faire.

### La purification avec l'eau

8:6	Et Moshè fit approcher Aaron et ses fils, et les lava avec de l'eau.

### Les vêtements d'Aaron

8:7	Et il mit sur Aaron la tunique, il le ceignit de la ceinture, le revêtit de la robe, mit sur lui l'éphod, et le ceignit avec la ceinture de l'éphod dont il le lia.
8:8	Puis il mit sur lui le pectoral, après avoir mis au pectoral l'ourim et le thoummim.
8:9	Il lui mit aussi la tiare sur la tête, et il mit sur le devant de la tiare la lame d'or, la couronne de sainteté, comme YHWH l'avait ordonné à Moshè<!--Ex. 28.-->.

### L'onction d'huile

8:10	Puis Moshè prit l'huile d'onction, et oignit le tabernacle et toutes les choses qui y étaient, et les sanctifia.
8:11	Et il en fit l'aspersion sur l'autel par 7 fois, et il oignit l'autel, tous ses ustensiles, et la cuve avec sa base, pour les sanctifier.
8:12	Il versa aussi de l'huile d'onction sur la tête d'Aaron, et l'oignit pour le sanctifier<!--Ps. 133:2.-->.

### Les vêtements des fils d'Aaron

8:13	Puis Moshè fit approcher les fils d'Aaron, les revêtit des tuniques, les ceignit des ceintures et leur attacha des turbans, comme YHWH l'avait ordonné à Moshè.

### Les offrandes et les sacrifices

8:14	Alors il fit approcher le taureau du sacrifice pour le péché, et Aaron et ses fils posèrent leurs mains sur la tête du taureau du sacrifice pour le péché.
8:15	Et Moshè le tua, prit de son sang, et en mit avec son doigt sur les cornes de l'autel tout autour, et purifia l'autel. Il répandit le reste du sang au pied de l'autel, ainsi il le sanctifia pour faire la propitiation sur lui.
8:16	Puis il prit toute la graisse qui était sur les entrailles, le grand lobe du foie, les deux rognons avec leur graisse, et Moshè les brûla sur l'autel.
8:17	Mais il brûla au feu, hors du camp, le jeune taureau avec sa peau, sa chair, et ses excréments, comme YHWH l'avait ordonné à Moshè.
8:18	Il fit aussi approcher le bélier de l'holocauste, et Aaron et ses fils posèrent leurs mains sur la tête du bélier.
8:19	Et Moshè l'égorgea et aspergea du sang le pourtour de l'autel.
8:20	Puis il coupa le bélier en morceaux, et Moshè brûla la tête, les morceaux, et la graisse.
8:21	Et il lava dans l'eau les entrailles et les jambes, et brûla tout le bélier sur l'autel. Ce fut un holocauste, une offrande consumée par le feu dont le parfum fut apaisant pour YHWH, comme YHWH l'avait ordonné à Moshè.
8:22	Il fit aussi approcher l'autre bélier, le bélier de consécration, et Aaron et ses fils posèrent les mains sur la tête du bélier.
8:23	Et Moshè l'égorgea, prit de son sang, et le mit sur le lobe de l'oreille droite d'Aaron, et sur le pouce de sa main droite et sur le gros orteil de son pied droit.
8:24	Il fit aussi approcher les fils d'Aaron, et mit du même sang sur le lobe de leur oreille droite, et sur le pouce de leur main droite, et sur le gros orteil de leur pied droit, et Moshè aspergea du reste du sang le pourtour de l'autel.
8:25	Après, il prit la graisse, la queue, toute la graisse qui est sur les entrailles, et le grand lobe du foie, et les deux rognons avec leur graisse, et l'épaule droite.
8:26	Il prit aussi de la corbeille des pains sans levain, qui étaient devant YHWH, un gâteau sans levain, et un gâteau de pain fait à l'huile et une galette, et il les mit sur les graisses, et sur l'épaule droite.
8:27	Puis il mit toutes ces choses sur les paumes des mains d'Aaron et sur les paumes des mains de ses fils, et les agita d'un côté et de l'autre devant YHWH.
8:28	Puis Moshè les prit de leurs mains et les brûla sur l'autel, sur l'holocauste. Ce fut l'offrande de consécration, une offrande consumée par le feu dont le parfum fut apaisant pour YHWH.
8:29	Moshè prit aussi la poitrine du bélier de consécration, et l'agita d'un côté et de l'autre devant YHWH. Ce fut la part de Moshè, comme YHWH l'avait ordonné à Moshè.

### L'aspersion d'huile et de sang

8:30	Moshè prit de l'huile d'onction et du sang qui était sur l'autel, et il en fit l'aspersion sur Aaron et sur ses vêtements, sur ses fils et sur les vêtements de ses fils ; ainsi il sanctifia Aaron et ses vêtements, les fils d'Aaron et les vêtements de ses fils.

### La nourriture de consécration<!--Ex. 29:26 ; Lé. 7:31-34, 8:29.-->

8:31	Après cela, Moshè dit à Aaron et à ses fils : Faites cuire la chair à l'entrée de la tente d'assignation, et vous la mangerez là, avec le pain qui est dans la corbeille de consécration, comme je l'ai ordonné, en disant : Aaron et ses fils la mangeront.
8:32	Mais vous brûlerez au feu ce qui restera de la chair et du pain.

### Les prêtres mis à part

8:33	Et vous ne sortirez pas pendant 7 jours, de l'entrée de la tente d'assignation, jusqu'à ce que vos jours de consécration soient accomplis, car on emploiera 7 jours à vous consacrer.
8:34	Ce qui s'est fait en ce jour, c'est ce que YHWH a ordonné de faire, pour faire la propitiation pour vous.
8:35	Vous resterez donc pendant 7 jours à l'entrée de la tente d'assignation, jour et nuit, et vous observerez ce que YHWH vous a ordonné d'observer, afin que vous ne mouriez pas, car il m'a été ainsi ordonné.
8:36	Ainsi Aaron et ses fils firent toutes les choses que YHWH avait ordonnées par Moshè.

## Chapitre 9

### Aaron et ses fils commencent leur service dans le tabernacle

9:1	Et il arriva au huitième jour que Moshè appela Aaron et ses fils, et les anciens d'Israël.
9:2	Et il dit à Aaron : Prends un jeune veau du troupeau en sacrifice pour le péché, et un bélier pour l'holocauste, tous les deux sans défaut, et offre-les devant YHWH.
9:3	Et tu parleras aux enfants d'Israël en disant : Prenez un jeune bouc en sacrifice pour le péché, un veau et un agneau âgés d'un an et sans défaut pour l'holocauste ;
9:4	un bœuf et un bélier pour l'offrande de paix<!--Voir commentaire en Lé. 3:1.-->, pour les sacrifier devant YHWH, et une offrande de grain pétrie à l'huile. Car aujourd'hui YHWH vous apparaîtra.
9:5	Ils prirent donc les choses que Moshè avait ordonnées et les amenèrent devant la tente d'assignation, et toute l'assemblée s'approcha, et se tint devant YHWH.
9:6	Et Moshè dit : Faites ce que YHWH vous a ordonné, et la gloire de YHWH vous apparaîtra.
9:7	Moshè dit à Aaron : Approche-toi de l'autel, fais ton sacrifice pour le péché et ton holocauste, et fais la propitiation pour toi et pour le peuple ; présente l'offrande pour le peuple et fais la propitiation pour eux, comme YHWH l'a ordonné<!--Hé. 7:26-27.-->.
9:8	Alors Aaron s'approcha de l'autel et égorgea le veau de son sacrifice pour le péché.
9:9	Et les fils d'Aaron lui présentèrent le sang, et il trempa son doigt dans le sang et le mit sur les cornes de l'autel. Puis il répandit le reste du sang au pied de l'autel.
9:10	Mais il brûla sur l'autel la graisse, et les rognons, et le grand lobe du foie du sacrifice pour le péché, comme YHWH l'avait ordonné à Moshè.
9:11	Et il brûla au feu la chair et la peau hors du camp.
9:12	Il égorgea aussi l'holocauste. Les fils d'Aaron lui présentèrent le sang, et il en aspergea le pourtour de l'autel.
9:13	Puis ils lui présentèrent l'holocauste coupé en morceaux, avec la tête, et il les brûla sur l'autel.
9:14	Et il lava les entrailles et les jambes, qu'il brûla sur l'holocauste, sur l'autel.
9:15	Il offrit l'offrande du peuple. Il prit le bouc du sacrifice pour le péché du peuple, il l'égorgea et l'offrit pour le péché, comme la première offrande.
9:16	Il l'offrit en holocauste, faisant selon l'ordonnance.
9:17	Ensuite, il offrit l'offrande de grain, et il en remplit la paume de sa main, et la brûla sur l'autel, outre l'holocauste du matin.
9:18	Il égorgea aussi le bœuf et le bélier pour le sacrifice d'offrande de paix qui était pour le peuple. Les fils d'Aaron lui présentèrent le sang, et il en aspergea le pourtour de l'autel.
9:19	Ils présentèrent la graisse du bœuf et du bélier, la queue, ce qui couvre les entrailles, les rognons, et le grand lobe du foie ;
9:20	ils mirent les graisses sur les poitrines, et il brûla les graisses sur l'autel.
9:21	Mais les poitrines et l'épaule droite, Aaron les balança en offrande balancée devant YHWH, comme Moshè l'avait ordonné.
9:22	Aaron éleva aussi ses mains vers le peuple, et le bénit. Puis il descendit, après avoir offert le sacrifice pour le péché, l'holocauste et l'offrande de paix.
9:23	Moshè donc et Aaron entrèrent dans la tente d'assignation, puis ils sortirent et ils bénirent le peuple. Et la gloire de YHWH apparut à tout le peuple.
9:24	Car le feu sortit de devant YHWH, et consuma sur l'autel l'holocauste et les graisses. Tout le peuple le vit et ils poussèrent des cris de joie et tombèrent sur leur face<!--1 R. 18:38 ; 2 Ch. 7:1.-->.

## Chapitre 10

### Un feu étranger présenté à YHWH

10:1	Or les fils d'Aaron, Nadab et Abihou, prirent chacun leur encensoir, mirent du feu, et ils posèrent dessus de l'encens. Ils présentèrent devant YHWH un feu étranger<!--Ce passage nous avertit du danger auquel s'exposent ceux qui apportent un feu étranger dans le temple. Les feux étrangers sont les fausses doctrines, le péché, les conceptions cartésiennes, pernicieuses, mercantiles, destinés à remplacer la parole d'Elohîm et à conduire le chrétien dans les ténèbres, loin de la présence de YHWH.-->, ce qu'il ne leur avait pas ordonné.
10:2	Et le feu sortit de devant YHWH et les dévora. Ils moururent devant YHWH<!--No. 3:4.-->.
10:3	Moshè dit à Aaron : C'est ce qu'avait déclaré YHWH en disant : Je serai sanctifié par ceux qui s'approchent de moi, et je serai glorifié en présence de tout le peuple. Et Aaron se tut.
10:4	Et Moshè appela Mishaël et Eltsaphan, les fils d'Ouzziel, oncle d'Aaron, et leur dit : Approchez-vous, emportez vos frères de devant le lieu saint, hors du camp.
10:5	Alors ils s'approchèrent et les emportèrent avec leurs tuniques hors du camp, comme Moshè l'avait dit.

### Instructions données par Moshè

10:6	Puis Moshè dit à Aaron, à Èl’azar et à Ithamar, ses fils : Ne découvrez pas vos têtes, et ne déchirez pas vos vêtements<!--Lé. 21:10 et Mt.26:65.-->, de peur que vous ne mouriez, et que YHWH ne se mette en colère contre toute l'assemblée. Mais que vos frères, toute la maison d'Israël, pleurent à cause de l'embrasement que YHWH a allumé<!--Ez. 24:17.-->.
10:7	Et ne sortez pas de l'entrée de la tente d'assignation, de peur que vous ne mouriez, car l'huile de l'onction de YHWH est sur vous. Et ils agirent selon la parole de Moshè.
10:8	Et YHWH parla à Aaron, en disant :
10:9	Vous ne boirez pas de vin, ni de boisson forte, ni toi ni tes fils avec toi, quand vous entrerez dans la tente d'assignation, de peur que vous ne mouriez. C'est un statut perpétuel pour vos descendants<!--No. 6:3 ; Jg. 13:7.-->,
10:10	afin que vous puissiez discerner entre ce qui est saint et ce qui est profane, entre ce qui est impur et ce qui est pur,
10:11	afin que vous enseigniez aux enfants d'Israël toutes les ordonnances que YHWH leur a prononcées par Moshè.
10:12	Puis Moshè parla à Aaron, à Èl’azar et à Ithamar, ses fils qui lui restaient : Prenez l'offrande de grain, ce qui reste des offrandes de YHWH consumées par le feu, et mangez-la avec des pains sans levain auprès de l'autel, car c'est le saint des saints.
10:13	Vous la mangerez dans un lieu saint, parce que c'est la portion qui est assignée à toi et à tes fils sur les offrandes consumées par le feu à YHWH, car c'est l'ordre que j'ai reçu.
10:14	Vous mangerez aussi la poitrine offerte par agitation et l'épaule présentée par élévation dans un lieu pur, toi, tes fils et tes filles avec toi, car ces choses-là t'ont été données, dans les sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.--> des enfants d'Israël, comme ton droit et le droit de tes fils.
10:15	Ils apporteront l'épaule présentée par élévation et la poitrine offerte par agitation, avec les offrandes consumées par le feu, qui sont les graisses, pour les agiter en offrande çà et là devant YHWH : Cela t'appartiendra, et à tes fils avec toi, par une ordonnance perpétuelle, comme YHWH l'a ordonné.
10:16	Or Moshè cherchait, il cherchait le bouc du sacrifice pour le péché, mais voici, il avait été brûlé. Et Moshè se mit en grande colère contre Èl’azar et Ithamar, les fils d'Aaron qui lui restaient, et leur dit :
10:17	Pourquoi n'avez-vous pas mangé le sacrifice pour le péché dans un lieu saint, puisque c'est le saint des saints ? Et cela vous a été donné pour porter l'iniquité de l'assemblée, afin de faire la propitiation pour elle devant YHWH.
10:18	Voici, son sang n'a pas été porté à l'intérieur du lieu saint. Vous deviez le manger, vous deviez le manger dans le lieu saint, comme je l'avais ordonné.
10:19	Alors Aaron répondit à Moshè : Voici, ils ont présenté aujourd'hui leur sacrifice pour le péché et leur holocauste devant YHWH, et ces choses-ci me sont arrivées. Si j'avais mangé aujourd'hui le sacrifice pour le péché, cela aurait-il plu à YHWH ?
10:20	Moshè l'entendit, et cela parut bon à ses yeux.

## Chapitre 11

### Torah de purification : Les bêtes pures et les bêtes impures

11:1	Et YHWH parla à Moshè et à Aaron, et leur dit :
11:2	Parlez aux enfants d'Israël, et dites-leur : Voici les bêtes que vous mangerez parmi toutes les bêtes qui sont sur la Terre<!--De. 14:4 ; Ac. 10:11-14.-->.
11:3	Vous mangerez parmi les bêtes toutes celles qui ont le sabot fendu, qui ont le pied fourchu, et qui ruminent.
11:4	Mais vous ne mangerez pas de celles qui ruminent uniquement, ou qui ont uniquement le sabot fendu, comme le chameau, car il rumine mais il n'a pas le sabot fendu. Il sera impur pour vous.
11:5	Et le lapin, car il rumine mais il n'a pas le sabot fendu. Il sera impur pour vous.
11:6	Le lièvre, car il rumine mais il n'a pas le sabot fendu. Il sera impur pour vous.
11:7	Le porc, car il a bien le sabot fendu et le pied fourchu, mais il ne rumine pas. Il sera impur pour vous.
11:8	Vous ne mangerez pas de leur chair, même vous ne toucherez pas leur cadavre. Ils seront impurs pour vous.
11:9	Voici ce que vous mangerez de tout ce qui est dans les eaux : vous mangerez tout ce qui a des nageoires et des écailles, et qui est dans les eaux, soit dans la mer, soit dans les fleuves.
11:10	Mais vous aurez en abomination tous ceux qui n'ont pas des nageoires et des écailles, parmi toutes les choses grouillantes des eaux et parmi toutes les âmes qui vivent dans les eaux.
11:11	Ils deviendront une abomination pour vous, vous ne mangerez pas de leur chair, et vous tiendrez pour une chose abominable leur cadavre.
11:12	Tout ce qui n'a pas de nageoires et d'écailles dans les eaux sera une abomination pour vous.
11:13	Et voici, parmi les oiseaux, ceux que vous tiendrez pour abominables, ceux qu'on ne mangera pas, ils seront une abomination pour vous : l'aigle, l'orfraie, la buse ;
11:14	le vautour, et le milan, selon leur espèce ;
11:15	tout corbeau, selon son espèce ;
11:16	l'autruche<!--Littéralement à traduire par « filles de l'autruche ».-->, le hibou, la mouette, et l'épervier selon leur espèce ;
11:17	le hibou, le plongeon, la chouette ;
11:18	le cygne, le cormoran, le pélican ;
11:19	la cigogne, le héron selon leur espèce, la huppe et la chauve-souris,
11:20	et toutes les choses grouillantes ailées qui marchent à quatre pattes seront en abomination pour vous.
11:21	Mais, parmi toutes les choses grouillantes ailées qui marchent à quatre pattes, vous pourrez manger celles qui ont des jambes au-dessus de leurs pieds pour sauter sur la terre.
11:22	Voici celles que vous mangerez : La sauterelle selon son espèce, le solam<!--« Solam », « hargol » et « hagab » sont diverses espèces de sauterelles.--> selon son espèce, le hargol, selon son espèce et le hagab, selon son espèce.
11:23	Mais toute autre chose grouillante ailée qui a quatre pattes sera une abomination pour vous.
11:24	Vous serez donc impurs par ces bêtes. Quiconque touchera leur cadavre sera impur jusqu'au soir,
11:25	et quiconque aussi portera leur cadavre lavera ses vêtements et sera impur jusqu'au soir.
11:26	Toute bête qui a le sabot fendu, et qui n'a pas le pied fourchu et ne rumine pas, sera impure pour vous. Quiconque les touchera sera impur.
11:27	Tout ce qui marche sur ses pattes, parmi tous les animaux qui marchent à quatre pieds, sera impur pour vous. Quiconque touchera leur cadavre sera impur jusqu'au soir,
11:28	et celui qui portera leur cadavre lavera ses vêtements et sera impur jusqu'au soir. Ils seront impurs pour vous.
11:29	Voici ce qui sera impur pour vous parmi les choses grouillantes qui grouillent sur la terre : La taupe, la souris et la tortue, selon leur espèce ;
11:30	le hérisson, la grenouille, le lézard, la limace et le caméléon.
11:31	Telles sont les choses grouillantes que vous tiendrez pour impures. Quiconque les touchera mortes sera impur jusqu'au soir.
11:32	Tout objet sur lequel il en tombera quelque chose quand elles seront mortes sera impur, soit ustensile de bois, soit vêtement, soit peau, ou sac, quelque objet que ce soit dont on se sert pour faire quelque chose. Il sera mis dans l'eau, et sera impur jusqu'au soir ; puis il sera pur.
11:33	Mais s'il en tombe quelque chose dans un vase de terre, tout ce qui est dedans sera impur, et vous casserez le vase.
11:34	Et tout aliment qu'on mange, sur lequel il y aura eu de cette eau, sera impur. Tout breuvage qu'on boit dans quelque vase que ce soit, en sera impur.
11:35	Et s'il tombe quelque chose de leur cadavre sur quoi que ce soit, cela sera impur. Le four et le foyer seront détruits. Ils seront impurs, et ils seront impurs pour vous.
11:36	Toutefois, les sources et les puits où se forment les masses d'eau resteront purs, mais celui donc qui touchera leur cadavre sera impur.
11:37	Et s'il tombe quelque chose de leur cadavre sur une semence qui doit être semée, la semence sera pure.
11:38	Mais si on avait mis de l'eau sur la semence, et que quelque chose de leur cadavre tombe sur elle, elle sera impure pour vous.
11:39	Et si une des bêtes qui vous servent de nourriture meurt, celui qui en touchera le cadavre sera impur jusqu'au soir.
11:40	Celui qui mangera de son cadavre lavera ses vêtements et sera impur jusqu'au soir, et celui aussi qui portera le cadavre de cette bête lavera ses vêtements et sera impur jusqu'au soir.
11:41	Toute chose grouillante qui grouille sur la terre est une abomination. On n'en mangera pas<!--Cp. Ge. 3:14.-->.
11:42	Parmi toutes les choses grouillantes qui grouillent sur la terre, vous ne mangerez aucune de celles qui se traînent sur le ventre, ni de celles qui marchent sur quatre pattes ou sur un grand nombre de pattes, car elles sont une abomination.
11:43	Ne rendez pas vos âmes abominables par toutes ces choses grouillantes qui grouillent. Ne vous rendez pas impurs par elles, ne vous souillez pas par elles.
11:44	Car je suis YHWH, votre Elohîm. Vous vous sanctifierez donc et vous serez saints, car je suis saint<!--1 Pi. 1:16.--> ! Et vous ne rendrez pas vos âmes impures par l'une de ces choses grouillantes qui rampent sur la terre.
11:45	Car je suis YHWH, qui vous ai fait monter du pays d'Égypte, afin que je sois votre Elohîm et que vous soyez saints, car je suis saint !
11:46	Telle est la torah concernant les animaux, les oiseaux, toute âme vivante qui rampe dans les eaux et toute âme vivante qui grouille sur la terre,
11:47	afin de discerner entre la chose impure et la chose pure, entre les animaux qu'on peut manger et les animaux dont on ne doit pas manger.

## Chapitre 12

### Torah de purification : Le flux de sang<!--Ps. 51:7.-->

12:1	YHWH parla aussi à Moshè, en disant :
12:2	Parle aux enfants d'Israël et dis-leur : lorsqu'une femme deviendra enceinte et qu'elle enfantera un mâle, elle sera impure pendant 7 jours. Elle sera impure comme au temps de ses règles<!--Vient de l'hébreu « niddah » qui signifie « impureté, ordurier, immonde, menstruation ».-->.
12:3	Et le huitième jour, on circoncira la chair du prépuce de l'enfant<!--Les parents de Yéhoshoua (Jésus) ont observé cette torah (Lu. 2:21-24). Voir Jn. 7:22.-->.
12:4	Et elle restera 33 jours à se purifier de son sang. Elle ne touchera aucune chose sainte et ne viendra pas au sanctuaire, jusqu'à ce que les jours de sa purification soient accomplis.
12:5	Si elle enfante une fille, elle sera impure deux semaines, comme au temps de ses règles, et elle restera 66 jours à se purifier de son sang.
12:6	Après que le temps de sa purification sera accompli, soit pour un fils ou pour une fille, elle présentera au prêtre un agneau d'un an en holocauste, et un jeune pigeon ou une tourterelle en sacrifice pour le péché, à l'entrée de la tente d'assignation<!--No. 6:10.-->.
12:7	Et le prêtre offrira ces choses devant YHWH et fera la propitiation pour elle, et elle sera purifiée du flux de son sang. Telle est la torah pour celle qui enfante un fils ou une fille.
12:8	Mais si elle n'a pas le moyen de trouver un agneau, alors elle prendra deux tourterelles ou deux jeunes pigeons<!--Lu. 2:21-24.-->, l'un pour l'holocauste et l'autre en sacrifice pour le péché. Le prêtre fera la propitiation pour elle et elle sera pure.

## Chapitre 13

### Torah de purification : La lèpre

13:1	YHWH parla aussi à Moshè et à Aaron, en disant :
13:2	L'être humain qui aura sur la peau de son corps une enflure, une croûte, ou une plaque blanche sur la peau, et que cela paraîtra sur la peau de son corps comme une plaie de lèpre, on l'amènera à Aaron, le prêtre, ou à l'un de ses fils prêtres.
13:3	Et le prêtre examinera la plaie qui est sur la peau du corps. Si le poil de la plaie est devenu blanc et, si à la vue la plaie est plus profonde que la peau du corps, c'est une plaie de lèpre. Le prêtre donc l'examinera et le déclarera impur.
13:4	Mais s'il y a une plaque blanche sur la peau du corps, et qu'à la vue elle n'est pas plus profonde que la peau, et si son poil n'est pas devenu blanc, le prêtre enfermera pendant 7 jours celui qui a la plaie.
13:5	Et le prêtre l'examinera le septième jour. Si à ses yeux la plaie s'est arrêtée, et qu'elle ne s'est pas étendue sur la peau, le prêtre l'enfermera une seconde fois pendant 7 jours.
13:6	Et le prêtre l'examinera une seconde fois le septième jour. Si la plaie est devenue pâle, et qu'elle ne s'est pas étendue sur la peau, le prêtre le déclarera pur. C'est de la dartre ; il lavera ses vêtements, et sera pur.
13:7	Mais si la dartre s'est étendue sur la peau, après avoir été vu par le prêtre pour être déclaré pur, il se fera examiner pour la seconde fois par le prêtre.
13:8	Le prêtre l'examinera encore. S'il aperçoit que la dartre s'est étendue sur la peau, le prêtre le déclarera impur. C'est de la lèpre.
13:9	Quand il y aura une plaie de lèpre sur un être humain, on l'amènera au prêtre.
13:10	Le prêtre l'examinera. Et s'il aperçoit qu'il y a une enflure blanche sur la peau, que le poil est devenu blanc, et qu'il y a une trace de chair vive dans la tumeur,
13:11	c'est une lèpre invétérée dans la peau de sa chair. Le prêtre le déclarera impur. Il ne l'enfermera pas, car il est déclaré impur.
13:12	Si la lèpre fait une éruption sur la peau, et qu'elle couvre toute la peau de celui qui a la plaie, depuis la tête de cet homme jusqu'à ses pieds, partout où pourra voir le prêtre, le prêtre l'examinera,
13:13	et si le prêtre voit que la lèpre couvre tout le corps de cet homme, alors il déclarera pur celui qui a la plaie. La plaie est devenue toute blanche, il est pur.
13:14	Mais le jour où l'on apercevra de la chair vive, il sera impur.
13:15	Alors le prêtre examinera la chair vive, et le déclarera impur. La chair vive est impure, c'est de la lèpre.
13:16	Si la chair vive se change et devient blanche, alors il viendra vers le prêtre.
13:17	Et le prêtre l'examinera et, s'il aperçoit que la plaie est devenue blanche, le prêtre déclarera pur celui qui a la plaie. Il est pur.
13:18	Si le corps a eu sur la peau un ulcère qui soit guéri,
13:19	et qu'à l'endroit où était l'ulcère il y ait une enflure blanche, ou une plaque blanche rougeâtre, cet homme se montrera au prêtre.
13:20	Le prêtre donc l'examinera. Et s'il aperçoit, qu'à la vue, elle paraît plus enfoncée que la peau, et que son poil est devenu blanc, alors le prêtre le déclarera impur. C'est une plaie de lèpre qui a fait éruption dans l'ulcère.
13:21	Mais si le prêtre voit qu'il n'y a pas de poil blanc dans la tache, et qu'elle n'est pas plus enfoncée que la peau, mais qu'elle est devenue pâle, le prêtre l'enfermera pendant 7 jours.
13:22	Si elle s'est étendue sur la peau en quelque sorte que ce soit, le prêtre le déclarera impur. C'est une plaie.
13:23	Mais si la plaque blanche sur la peau est restée à la même place et ne s'est pas étendue, c'est une cicatrice d'ulcère. Ainsi le prêtre le déclarera pur.
13:24	Si le corps a sur la peau une brûlure par le feu, et que la chair vive de la partie brûlée soit une plaque blanche sur la peau rougeâtre ou blanche seulement, le prêtre la regardera,
13:25	et si le poil est devenu blanc dans la plaque blanche sur la peau, et qu'à la vue, elle est plus profonde que la peau, c'est de la lèpre, elle a fait éruption dans la brûlure ; le prêtre donc le déclarera impur. C'est une plaie de lèpre.
13:26	Mais si le prêtre voit qu'il n'y a pas de poil blanc dans la plaque blanche sur la peau, et qu'elle n'est pas plus enfoncée que la peau, qu'elle est devenue pâle, le prêtre l'enfermera pendant 7 jours.
13:27	Puis le prêtre l'examinera le septième jour. Si la tache s'est étendue sur la peau, le prêtre le déclarera impur. C'est une plaie de lèpre.
13:28	Si la plaque blanche sur la peau est restée à la même place, ne s'est pas étendue, et est devenue pâle, c'est la enflure de la brûlure. Le prêtre le déclarera pur, c'est la cicatrice de la brûlure.
13:29	Si l'homme ou la femme a une plaie à la tête, ou l'homme à la barbe,
13:30	le prêtre examinera la plaie, et si, à la vue, elle est plus profonde que la peau, et qu'il y ait en elle du poil jaunâtre et fin, le prêtre le déclarera impur. C'est de la teigne, c'est une lèpre de la tête ou de la barbe.
13:31	Si le prêtre voit que la plaie de la teigne ne paraît pas plus profonde que la peau, et qu'il n'y a pas de poil noir, alors le prêtre enfermera pendant 7 jours celui qui a la plaie de la teigne.
13:32	Et le septième jour le prêtre examinera la plaie. Si la teigne ne s'est pas étendue, qu'elle n'a aucun poil jaunâtre, et, qu'à voir la teigne, elle n'est pas plus profonde que la peau,
13:33	celui qui a la teigne se rasera, mais il ne se rasera pas à l'endroit de la teigne, et le prêtre enfermera pendant 7 autres jours celui qui a la teigne.
13:34	Puis le prêtre examinera la teigne au septième jour. Si la teigne ne s'est pas étendue sur la peau et, qu'à la vue, elle n'est pas plus profonde que la peau, le prêtre le déclarera pur, et cet homme lavera ses vêtements, et il sera pur.
13:35	Mais si la teigne s'est étendue sur la peau, après sa purification, le prêtre l'examinera,
13:36	et si la teigne s'est étendue sur la peau, le prêtre ne cherchera pas de poil jaunâtre. Il est impur.
13:37	Mais si la teigne s'est arrêtée, et qu'il y ait poussé du poil noir, la teigne est guérie. Il est pur, et le prêtre le déclarera pur.
13:38	Si l'homme ou la femme a sur la peau de son corps des plaques blanches, des plaques qui sont blanches,
13:39	le prêtre l'examinera. Si sur la peau de son corps il y a des plaques blanches, d'un blanc pâle, ce ne sont que des taches blanches qui ont fait éruption inoffensive sur la peau. Il est donc pur.
13:40	Si l'homme a la tête dépouillée de cheveux, c'est un chauve. Il est pur.
13:41	Et si sa tête est dépouillée de cheveux du côté de son visage, c'est un front chauve. Il est pur.
13:42	Et si dans la partie chauve de devant ou de derrière, il y a une plaie d'un blanc rougeâtre, c'est une lèpre qui a fait éruption dans sa partie chauve de derrière ou de devant.
13:43	Et le prêtre le regardera. S'il aperçoit que l'enflure de la plaie est d'un blanc rougeâtre dans sa partie chauve de derrière ou de devant, semblable à la lèpre de la peau du corps,
13:44	c'est un homme lépreux, il est impur. Impur, le prêtre le déclarera impur. C'est à la tête qu'est sa plaie.
13:45	Et le lépreux qui sera atteint de la plaie aura ses vêtements déchirés, et sa tête nue. Il se couvrira la barbe et criera : Impur ! Impur !
13:46	Pendant tout le temps qu'il aura cette plaie, il sera déclaré impur. Il est impur. Il demeurera seul, sa demeure sera hors du camp<!--2 R. 7:3 ; La. 4:15 ; Lu. 17:12-13.-->.
13:47	Et si le vêtement est infecté de la plaie de la lèpre, soit sur un vêtement de laine, soit sur un vêtement de lin,
13:48	à la chaîne ou à la trame du lin, ou de laine, sur la peau ou sur quelque ouvrage de peau,
13:49	et si cette plaie est verdâtre ou rougeâtre sur le vêtement ou sur la peau, à la chaîne ou à la trame, ou sur un objet quelconque de peau, ce sera une plaie de lèpre, et elle sera montrée au prêtre.
13:50	Et le prêtre examinera la plaie, et enfermera pendant 7 jours celui qui a la plaie.
13:51	Et au septième jour, il examinera la plaie. Si la plaie s'est étendue sur le vêtement, à la chaîne ou à la trame, sur la peau ou sur quelque ouvrage de peau, la plaie est une lèpre invétérée. L'objet est impur.
13:52	Il brûlera le vêtement, la chaîne ou la trame de laine ou de lin, et tout objet de peau, qui auront cette plaie, car c'est une lèpre invétérée, il faut les brûler au feu.
13:53	Mais si le prêtre voit que la plaie ne s'est pas étendue sur le vêtement, sur la chaîne ou sur la trame, ou sur quelque objet de peau,
13:54	le prêtre ordonnera qu'on lave l'objet sur lequel est la plaie, et il l'enfermera une seconde fois pendant 7 jours.
13:55	Si le prêtre observe que la plaie n'a pas changé sa couleur après qu'on l'aura lavé, et qu'elle ne s'est pas étendue, l'objet est impur. Tu le brûleras au feu, c'est une partie de l'endroit ou de l'envers qui a été rongée.
13:56	Si le prêtre voit que la plaie est devenue pâle après avoir été lavé, il l'arrachera du vêtement ou de la peau, de la chaîne ou de la trame.
13:57	Si elle paraît encore sur le vêtement, à la chaîne ou à la trame, ou sur tout objet de peau, c'est une lèpre qui a fait éruption. Vous brûlerez au feu l'objet sur lequel est la plaie.
13:58	Mais si tu as lavé le vêtement, la chaîne ou la trame, ou quelque tout objet de peau, et que la plaie s'en est allée, il sera lavé une seconde fois, puis il sera pur.
13:59	Telle est la torah concernant le cas de la lèpre sur un vêtement de laine ou de lin, sur la chaîne ou sur la trame, ou sur un objet de peau quelconque, pour les déclarer purs ou impurs.

## Chapitre 14

### Torah concernant le lépreux pour le jour de sa purification

14:1	YHWH parla aussi à Moshè, en disant :
14:2	Voici la torah concernant le lépreux pour le jour de sa purification. Il sera amené au prêtre<!--Mt. 8:2-4 ; Mc. 1:42-44 ; Lu. 5:12-14.-->.
14:3	Le prêtre sortira hors du camp et l'examinera. Si la plaie de la lèpre du lépreux est guérie,
14:4	le prêtre ordonnera qu'on prenne pour celui qui doit être purifié, deux oiseaux vivants et purs, avec du bois de cèdre, de l'écarlate de cochenille et de l'hysope<!--Ex. 12:22.-->.
14:5	Et le prêtre ordonnera qu'on tue l'un des oiseaux sur un vase de terre, sur de l'eau vive.
14:6	Puis il prendra l'oiseau vivant, le bois de cèdre, l'écarlate de cochenille et l'hysope, et il trempera toutes ces choses avec l'oiseau vivant, dans le sang de l'autre oiseau qui aura été tué sur de l'eau vive.
14:7	Il en fera 7 fois l'aspersion sur celui qui doit être purifié de la lèpre. Il le déclarera pur, et il laissera aller par les champs l'oiseau vivant.
14:8	Et celui qui doit être purifié lavera ses vêtements, rasera tout son poil et se lavera dans l'eau, et il sera pur. Ensuite il entrera dans le camp, mais il restera 7 jours hors de sa tente.
14:9	Au septième jour, il rasera tout son poil, sa tête, sa barbe, les sourcils de ses yeux, tout son poil. Il rasera tout son poil. Il lavera ses vêtements et son corps, et il sera pur.
14:10	Et au huitième jour, il prendra deux agneaux sans défaut, une brebis d'un an sans défaut, et trois dixièmes de fine farine en offrande de grain, pétrie à l'huile, et un log<!--Une mesure de liquide d'environ 0,31 litre.--> d'huile.
14:11	Le prêtre qui fait la purification présentera celui qui doit être purifié et ces choses-là devant YHWH, à l'entrée de la tente d'assignation.
14:12	Puis le prêtre prendra l'un des agneaux et l'offrira en sacrifice de culpabilité avec un log d'huile. Il agitera ces choses devant YHWH, en offrande agitée.
14:13	Et il tuera l'agneau au lieu où l'on tue le sacrifice pour le péché et l'holocauste, dans le lieu saint. En effet, le sacrifice pour la culpabilité appartient au prêtre, comme le sacrifice pour le péché. C'est le saint des saints.
14:14	Le prêtre prendra du sang de l'offrande pour la culpabilité. Il le mettra sur le lobe de l'oreille droite de celui qui doit être purifié, sur le pouce de sa main droite et sur le gros orteil de son pied droit.
14:15	Puis le prêtre prendra du log d'huile et en versera dans la paume de sa main gauche.
14:16	Et le prêtre trempera le doigt de sa main droite dans l'huile qui est dans sa paume gauche, et fera l'aspersion de l'huile avec son doigt 7 fois devant YHWH.
14:17	Et du reste de l'huile qui sera dans sa paume, le prêtre en mettra sur le lobe de l'oreille droite de celui qui doit être purifié, sur le pouce de sa main droite et sur le gros orteil de son pied droit, sur le sang pris de l'offrande pour la culpabilité.
14:18	Mais ce qui restera de l'huile sur la paume du prêtre, il le mettra sur la tête de celui qui doit être purifié. C'est ainsi que le prêtre fera la propitiation pour lui devant YHWH.
14:19	Ensuite, le prêtre offrira le sacrifice pour le péché et fera la propitiation pour celui qui doit être purifié de son impureté, puis il tuera l'holocauste.
14:20	Le prêtre offrira l'holocauste et l'offrande de grain sur l'autel, et fera la propitiation pour celui qui doit être purifié, et il sera pur.
14:21	Mais s'il est pauvre et s'il n'a pas le moyen de fournir ces choses, il prendra un agneau en offrande agitée pour la culpabilité, afin de faire la propitiation pour lui, et un dixième de fine farine pétrie à l'huile pour l'offrande de grain, avec un log d'huile,
14:22	et deux tourterelles ou deux jeunes pigeons, selon ce qu'il pourra fournir, dont l'un sera pour le péché et l'autre pour l'holocauste.
14:23	Et le huitième jour de sa purification, il les apportera au prêtre, à l'entrée de la tente d'assignation, devant YHWH.
14:24	Et le prêtre recevra l'agneau du sacrifice de culpabilité et le log d'huile, et les agitera devant YHWH en offrande agitée.
14:25	Et il égorgera l'agneau du sacrifice de culpabilité. Puis le prêtre prendra du sang du sacrifice de culpabilité, il le mettra sur le lobe de l'oreille droite de celui qui doit être purifié, sur le pouce de sa main droite et sur le gros orteil de son pied droit.
14:26	Puis le prêtre versera de l'huile dans la paume de sa main gauche.
14:27	Et avec le doigt de sa main droite, il fera l'aspersion de l'huile qui est dans sa main gauche 7 fois devant YHWH.
14:28	Il mettra de cette huile qui est dans sa paume, sur le lobe de l'oreille droite de celui qui doit être purifié et sur le pouce de sa main droite et sur le gros orteil de son pied droit, sur le lieu du sang pris du sacrifice de culpabilité.
14:29	Après, il mettra le reste de l'huile qui est dans sa paume sur la tête de celui qui doit être purifié, afin de faire la propitiation pour lui devant YHWH.
14:30	Puis il sacrifiera l'une des tourterelles ou l'un des jeunes pigeons, selon ce qu'il aura pu fournir.
14:31	Selon ce qu'il aura pu fournir, l'un sera pour le sacrifice pour le péché et l'autre pour l'holocauste, avec l'offrande de grain. C'est ainsi que le prêtre fera la propitiation devant YHWH pour celui qui doit être purifié.
14:32	Telle est la torah de celui qui a une plaie de lèpre, et dont les ressources sont insuffisantes à sa purification.

### Torah pour une plaie de lèpre sur une maison

14:33	Puis YHWH parla à Moshè et à Aaron, en disant :
14:34	Quand vous serez entrés dans le pays de Canaan, que je vous donne en possession, si j'envoie une plaie de lèpre sur une maison du pays que vous posséderez,
14:35	celui à qui appartiendra la maison viendra et le déclarera au prêtre, en disant : J'aperçois comme une plaie dans ma maison.
14:36	Alors le prêtre ordonnera qu'on vide la maison avant qu'il y entre pour regarder la plaie, afin que rien de ce qui est dans la maison ne soit impur, puis le prêtre entrera pour voir la maison.
14:37	Et il regardera la plaie. Si la plaie qui est sur les murs de la maison a des creux verdâtres ou rougeâtres, qui soient, à les voir, plus enfoncés que le mur ;
14:38	le prêtre sortira de la maison, à l'entrée, et fera fermer la maison pendant 7 jours.
14:39	Au septième jour, le prêtre retournera et la regardera. Si la plaie s'est étendue sur les murs de la maison,
14:40	alors il ordonnera de retirer les pierres sur lesquelles est la plaie, et de les jeter hors de la ville, dans un lieu impur.
14:41	Il fera racler la maison à l'intérieur, tout autour, et l'on jettera la poussière qu'on aura raclée, hors de la ville, dans un lieu impur.
14:42	Puis on prendra d'autres pierres, et on les mettra à la place des premières pierres et on prendra d'autres mortiers pour recrépir la maison.
14:43	Mais si la plaie revient et fait éruption dans la maison, après avoir retiré les pierres, après avoir raclé et recrépi la maison,
14:44	le prêtre y entrera et l'examinera. Si la plaie s'est étendue dans la maison, c'est une lèpre invétérée dans la maison. Elle est impure.
14:45	On démolira la maison, ses pierres, son bois, et tout le mortier de la maison. On les transportera hors de la ville, dans un lieu impur.
14:46	Celui qui entrera dans la maison pendant tout le temps qu'elle est fermée sera impur jusqu'au soir.
14:47	Celui qui dormira dans cette maison lavera ses vêtements. Celui aussi qui mangera dans cette maison lavera ses vêtements.
14:48	Mais si le prêtre entre, s'il entre et voit que la plaie ne s'est pas étendue dans cette maison, après l'avoir recrépie, il jugera la maison pure, car sa plaie est guérie.
14:49	Alors il prendra pour purifier la maison deux oiseaux, du bois de cèdre, de l'écarlate de cochenille et de l'hysope.
14:50	Il tuera l'un des oiseaux sur un vase de terre, sur de l'eau vive.
14:51	Il prendra le bois de cèdre, l'hysope, l'écarlate de cochenille et l'oiseau vivant, et il trempera le tout dans le sang de l'oiseau qu'on aura tué et dans l'eau vive, puis il fera 7 fois l'aspersion sur la maison.
14:52	Il purifiera la maison avec le sang de l'oiseau, avec l'eau vive, avec l'oiseau vivant, le bois de cèdre, l'hysope et l'écarlate de cochenille.
14:53	Puis il laissera aller hors de la ville par les champs, l'oiseau vivant. C'est ainsi qu'il fera la propitiation pour la maison et elle sera pure.
14:54	Telle est la torah pour toute plaie de lèpre et de teigne,
14:55	de lèpre de vêtement et de maison,
14:56	d'enflure, de croûte et de plaque blanche sur la peau :
14:57	pour enseigner quand une chose est impure et quand elle est pure. Telle est la torah sur la lèpre.

## Chapitre 15

### Torah de purification : Gonorrhée et flux menstruel<!--Jn. 13:3-10 ; Ep. 5:25-27 ; 1 Jn. 1:9.-->

15:1	YHWH parla aussi à Moshè et à Aaron, en disant :
15:2	Parlez aux enfants d'Israël, et dites-leur : L'homme, l'homme qui a un écoulement dans sa chair sera impur à cause de sa gonorrhée<!--Gonorrhée : infection des organes génito-urinaires.-->.
15:3	Et voici son impureté à cause de sa gonorrhée : Que sa chair laisse couler son flux, ou que sa chair retienne son flux, c'est son impureté.
15:4	Tout lit sur lequel se couchera celui qui est atteint d'un flux sera impur et tout objet sur lequel il s'assiéra sera impur.
15:5	Quiconque touchera son lit lavera ses vêtements et se lavera avec de l'eau et il sera impur jusqu'au soir.
15:6	Et celui qui s'assiéra sur quelque chose sur laquelle celui qui a ce flux s'est assis, lavera ses vêtements et se lavera dans l'eau, et il sera impur jusqu'au soir.
15:7	Et celui qui touchera la chair de celui qui a ce flux lavera ses vêtements et se lavera dans l'eau, et il sera impur jusqu'au soir.
15:8	Si celui qui a ce flux crache sur celui qui est pur, celui qui était pur lavera ses vêtements et se lavera dans l'eau, et il sera impur jusqu'au soir.
15:9	Toute monture que celui qui a ce flux aura montée sera impure.
15:10	Quiconque touchera quelque chose qui aura été sous lui sera impur jusqu'au soir. Quiconque portera une telle chose lavera ses vêtements et se lavera dans l'eau, et il sera impur jusqu'au soir.
15:11	Quiconque aura été touché par celui qui a ce flux, sans qu'il ait lavé ses mains dans l'eau, lavera ses vêtements et il se lavera dans l'eau, et il sera impur jusqu'au soir.
15:12	Et le vase de terre que celui qui a ce flux aura touché sera cassé, mais tout vase de bois sera lavé dans l'eau.
15:13	Et quand celui qui a ce flux sera purifié de son flux, il comptera 7 jours pour sa purification. Il lavera ses vêtements et sa chair avec de l'eau vive, et ainsi il sera pur.
15:14	Au huitième jour, il prendra pour lui deux tourterelles ou deux jeunes pigeons, et il viendra devant YHWH à l'entrée de la tente d'assignation, et les donnera au prêtre.
15:15	Et le prêtre les sacrifiera, l'un en sacrifice pour le péché et l'autre en holocauste. Le prêtre fera ainsi la propitiation pour lui devant YHWH à cause de son flux.
15:16	L'homme qui aura un écoulement de sperme lavera tout son corps dans l'eau et sera impur jusqu'au soir.
15:17	Tout vêtement et toute peau sur lesquels il y aura du sperme seront lavés dans l'eau et seront impurs jusqu'au soir.
15:18	Quand une femme a couché avec un tel homme et qu'il y a eu du sperme pendant les rapports sexuels, elle se lavera dans l'eau avec son mari, et ils seront impurs jusqu'au soir.
15:19	Et quand une femme aura un flux, un flux de sang en sa chair, elle restera impure<!--Voir Lé. 12:2.--> durant 7 jours. Quiconque la touchera sera impur jusqu'au soir<!--Mt. 9:18-22 ; Mc. 5:21-34 ; Lu. 8:41-48.-->.
15:20	Toute chose sur laquelle elle aura couché pendant son impureté sera impure, de même que tout objet sur lequel elle se sera assise sera impur.
15:21	Quiconque touchera le lit de cette femme lavera ses vêtements et se lavera dans l'eau, et il sera impur jusqu'au soir.
15:22	Et quiconque touchera un objet quelconque sur lequel elle se sera assise lavera ses vêtements, et il sera impur jusqu'au soir.
15:23	Et s'il y a quelque chose sur le lit ou sur l'objet sur lequel elle s'est assise, quiconque touchera cet objet sera impur jusqu'au soir.
15:24	Et si un homme a couché avec elle et que son impureté soit sur lui, il sera impur 7 jours, et toute couche sur laquelle il dormira sera impure.
15:25	La femme qui aura un flux de sang pendant plusieurs jours, en dehors de l'époque de ses règles, ou dont le flux durera plus longtemps que l'époque de ses règles, sera impure tout le temps du flux de son impureté, comme au temps de ses règles.
15:26	Tout lit sur lequel elle couchera pendant les jours de son flux sera pour elle comme le lit de ses règles, et tout objet sur lequel elle s'assiéra sera impure comme lors de ses règles.
15:27	Et quiconque aura touché ces choses-là sera impur. Il lavera ses vêtements et se lavera dans l'eau, et il sera impur jusqu'au soir.
15:28	Mais si elle est purifiée de son flux, elle comptera 7 jours, et après elle sera pure.
15:29	Au huitième jour, elle prendra deux tourterelles ou deux jeunes pigeons, et les apportera au prêtre à l'entrée de la tente d'assignation.
15:30	Et le prêtre en offrira l'un en sacrifice pour le péché et l'autre en holocauste. C'est ainsi que le prêtre fera la propitiation pour elle devant YHWH, à cause du flux de son impureté.
15:31	Ainsi, vous séparerez les enfants d'Israël de leurs impuretés, et ils ne mourront pas à cause de leurs impuretés, en rendant impur mon tabernacle, qui est au milieu d'eux.
15:32	Telle est la torah pour celui qui a un flux, et pour celui qui est atteint d'un écoulement de sperme qui le rend impur.
15:33	Pour celle qui est indisposée par ses règles, pour l'homme ou la femme qui a un flux de sa gonorrhée et pour l'homme qui couche avec celle qui est impure.

## Chapitre 16

### Le jour de la fête des propitiations<!--Hé. 9:1-14.-->

16:1	Et YHWH parla à Moshè après la mort des deux fils d'Aaron, qui moururent lorsqu'ils s'étaient approchés de la présence de YHWH.
16:2	YHWH donc dit à Moshè : Parle à Aaron, ton frère, et dis-lui qu'il n'entre pas en tout temps dans le lieu saint, au-dedans du voile, devant le propitiatoire qui est sur l'arche, afin qu'il ne meure pas, car j'apparaîtrai dans une nuée sur le propitiatoire.
16:3	Voici de quelle manière Aaron entrera dans le lieu saint : Il prendra un jeune taureau du troupeau pour le sacrifice pour le péché et un bélier pour l'holocauste.
16:4	Il se revêtira de la sainte tunique de lin, et portera les caleçons de lin sur son corps. Il se ceindra de la ceinture de lin<!--La ceinture de vérité (Ep. 6:14).-->, et se couvrira la tête de la tiare<!--La tiare, le casque du salut (Ep. 6:17).--> de lin, qui sont les vêtements sacrés, et il s'en vêtira après avoir lavé son corps avec de l'eau<!--Le lavement préfigure ici la régénération (Tit. 3:5).-->.
16:5	Et il prendra de l'assemblée des enfants d'Israël deux jeunes boucs en sacrifice pour le péché et un bélier pour l'holocauste.
16:6	Puis Aaron offrira son veau en sacrifice pour le péché, et fera la propitiation tant pour lui que pour sa maison.

### Les deux boucs expiatoires<!--2 Co. 5:21.-->

16:7	Et il prendra les deux boucs, et les présentera devant YHWH, à l'entrée de la tente d'assignation.
16:8	Puis Aaron jettera le sort sur les deux boucs, un sort pour YHWH et un sort pour le bouc Azazel.
16:9	Et Aaron présentera le bouc sur lequel le sort sera tombé pour YHWH, et en fera un sacrifice pour le péché.
16:10	Mais le bouc sur lequel le sort sera tombé pour Azazel, sera présenté vivant devant YHWH pour faire la propitiation par lui, et on l'enverra dans le désert pour être Azazel.
16:11	Aaron donc, présentera le veau en sacrifice pour le péché, et fera la propitiation pour lui et pour sa maison. Et il tuera son veau qui est le sacrifice pour le péché.
16:12	Puis il prendra un encensoir plein de charbons ardents, de dessus l'autel devant YHWH, et deux poignées d'encens aromatique en poudre ; et il les apportera au-dedans du voile ;
16:13	et il mettra l'encens sur le feu devant YHWH, afin que la nuée de l'encens couvre le propitiatoire qui est sur le témoignage, ainsi il ne mourra pas.
16:14	Il prendra aussi du sang du veau, et il en fera l'aspersion avec son doigt au-devant du propitiatoire vers l'orient. Il fera l'aspersion de ce sang-là 7 fois avec son doigt devant le propitiatoire.
16:15	Il tuera aussi le bouc du sacrifice pour le péché du peuple et il apportera son sang au-dedans du voile. Il fera de son sang comme il a fait du sang du veau, en faisant l'aspersion sur le propitiatoire et sur le devant du propitiatoire.
16:16	Et il fera la propitiation pour le lieu saint, le purifiant des impuretés des enfants d'Israël et de leurs transgressions, selon tous leurs péchés. Il fera la même chose pour la tente d'assignation, qui demeure avec eux au milieu de leurs impuretés.
16:17	Et aucun être humain ne sera dans la tente d'assignation quand le prêtre y entrera pour faire la propitiation dans le lieu saint, jusqu'à ce qu'il en sorte, lorsqu'il fera propitiation pour lui et pour sa maison, et pour toute l'assemblée d'Israël.
16:18	Puis il sortira vers l'autel qui est devant YHWH, et fera la propitiation pour lui. Il prendra du sang du veau et du sang du bouc, il le mettra sur les cornes de l'autel tout autour.
16:19	Et il fera par 7 fois l'aspersion du sang avec son doigt sur l'autel, et le purifiera et le sanctifiera des impuretés des enfants d'Israël.
16:20	Et quand il achèvera de faire la propitiation pour le lieu saint, pour la tente d'assignation et pour l'autel, alors il offrira le bouc vivant.
16:21	Et Aaron posera ses deux mains sur la tête du bouc vivant, et il confessera sur lui toutes les iniquités des enfants d'Israël et toutes leurs transgressions, selon tous leurs péchés. Il les mettra sur la tête du bouc, et l'enverra dans le désert par un homme prêt pour cela.
16:22	Et le bouc portera sur lui toutes leurs iniquités dans une terre inhabitable, puis cet homme laissera aller le bouc par le désert.
16:23	Et Aaron reviendra dans la tente d'assignation ; il quittera les vêtements de lin dont il s'était vêtu quand il était entré dans le lieu saint, et les posera là.
16:24	Il lavera aussi son corps avec de l'eau dans le lieu saint et se revêtira de ses vêtements. Puis il sortira, il offrira son holocauste et l'holocauste du peuple, et fera la propitiation pour lui et pour le peuple.
16:25	Il brûlera aussi sur l'autel la graisse du sacrifice pour le péché.
16:26	Et celui qui aura conduit le bouc pour Azazel lavera ses vêtements et son corps avec de l'eau. Après cela, il rentrera dans le camp.
16:27	Mais on emportera hors du camp le veau et le bouc qui auront été offerts en sacrifice pour le péché, et dont le sang aura été porté dans le lieu saint pour y faire la propitiation, et on brûlera au feu leurs peaux, leur chair et leurs excréments<!--Hé. 13:11.-->.
16:28	Et celui qui les aura brûlés lavera ses vêtements et son corps avec de l'eau. Après cela, il rentrera dans le camp.
16:29	Et ceci sera pour vous un statut perpétuel : Le dixième jour du septième mois, vous affligerez vos âmes, et vous ne ferez aucune œuvre, tant celui qui est du pays que l'étranger qui fait son séjour parmi vous<!--La fête des expiations (ou yom kippour) avait lieu une fois par an, le dixième jour du septième mois (Ex. 30:10 ; Lé. 16:29). À cette occasion, le grand-prêtre jetait le sort sur deux boucs : un sort pour YHWH et un sort pour Azazel (Lé. 16:8-10). Le bouc pour YHWH était sacrifié, il préfigurait la mort expiatoire du Mashiah. Le bouc émissaire, pour Azazel, n'avait lui-même rien fait de mal, mais il était choisi par Elohîm pour porter le péché du peuple afin qu'il soit dégagé de toute accusation. Ce que l'on faisait de ce bouc préfigurait l'œuvre de Yéhoshoua ha Mashiah (Jésus-Christ). Il symbolisait le Seigneur qui s'est chargé de nos péchés pour les emporter loin de nous (Es. 53 ; Ps. 103:12 ; Hé. 10:17, 13:12-14). Le Mashiah est mort et ressuscité hors du camp et c'est là qu'il nous appelle à le rejoindre : hors du monde et des systèmes religieux (Hé. 13:10-14).-->.
16:30	Car en ce jour-là, le prêtre fera la propitiation pour vous, afin de vous purifier. Ainsi vous serez purifiés de tous vos péchés devant YHWH.
16:31	Ce sera pour vous donc un shabbat, un jour de repos, et vous affligerez vos âmes. C'est un statut perpétuel.
16:32	Et le prêtre qu'on aura oint et qu'on aura consacré pour exercer la prêtrise à la place de son père, fera la propitiation, s'étant revêtu des vêtements de lin, qui sont les vêtements sacrés.
16:33	Et il fera la propitiation pour le saint sanctuaire, et il fera propitiation pour la tente d'assignation et pour l'autel, et pour les prêtres et pour tout le peuple de l'assemblée.
16:34	Ceci donc sera pour vous un statut perpétuel, afin de faire la propitiation pour les enfants d'Israël, de tous leurs péchés, une fois par an. On fit comme YHWH l'avait ordonné à Moshè.

## Chapitre 17

### Les sacrifices apportés à l'entrée de la tente d'assignation

17:1	YHWH parla aussi à Moshè, en disant :
17:2	Parle à Aaron et à ses fils, et à tous les enfants d'Israël, et dis-leur : Voici ce que YHWH a ordonné, en disant :
17:3	Si un homme, un homme de la maison d'Israël tue un bœuf, un agneau ou une chèvre dans le camp, ou même le tue hors du camp<!--De. 12:6.-->,
17:4	sans l'amener à l'entrée de la tente d'assignation, pour en faire une offrande à YHWH, devant le tabernacle de YHWH, du sang sera porté au compte de cet homme-là. Il a répandu du sang, c'est pourquoi cet homme-là sera retranché du milieu de son peuple.
17:5	C'est afin que les enfants d'Israël amènent leurs sacrifices, qu'ils sacrifient dans les champs, qu'ils les amènent à YHWH, à l'entrée de la tente d'assignation, vers le prêtre, et qu'ils les sacrifient en sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.--> à YHWH.
17:6	Le prêtre aspergera de ce sang l'autel de YHWH, à l'entrée de la tente d'assignation, et en brûlera la graisse dont le parfum est apaisant pour YHWH.
17:7	Ils n'offriront plus leurs sacrifices aux démons<!--Le mot hébreu traduit par « démon » est « sa`iyr » qui signifie « velu, poilu, mâle de la chèvre, bouc ; comme animal de sacrifice ; satyre ». Dans la mythologie grecque, les satyres étaient des créatures lubriques, couvertes de poils, mi-homme, mi-bouc, faisant partie du cortège de Dionysos, dieu du vin et des excès (Bacchus chez les Romains). Souvent sujets de représentations ithyphalliques, les œuvres d'art les montrent souvent en train de pourchasser de leurs assiduités les nymphes et les ménades. Aujourd'hui, ce terme sert à désigner les exhibitionnistes et ceux qui commettent des attentats à la pudeur sur la voie publique.-->, avec lesquels ils se sont prostitués. Ce sera un statut perpétuel pour eux au fil des âges<!--De. 32:17 ; Ps. 106:37.-->.
17:8	Tu leur diras donc : Si un homme de la maison d'Israël, ou des étrangers qui font leur séjour parmi eux, aura offert un holocauste ou un sacrifice,
17:9	et qui ne l'aura pas amené à l'entrée de la tente d'assignation, pour le sacrifier à YHWH, cet homme-là sera retranché d'entre ses peuples.

### Importance du sang

17:10	Si un homme, un homme de la maison d'Israël ou des étrangers qui séjournent au milieu d'eux mange du sang, quel qu'il soit, je tournerai ma face contre cette personne qui aura mangé du sang, et je la retrancherai du milieu de son peuple<!--Ge. 9:4 ; De. 12:16-23 ; 1 S. 14:33.-->.
17:11	Car l'âme de la chair est dans le sang. C'est pourquoi je vous ai ordonné qu'il soit mis sur l'autel, afin de faire la propitiation pour vos âmes, car c'est le sang qui fera propitiation pour l'âme.
17:12	C'est pourquoi j'ai dit aux enfants d'Israël : Personne d'entre vous ne mangera du sang, même l'étranger qui séjourne au milieu de vous ne mangera pas de sang.
17:13	Si un homme, un homme des enfants d'Israël et des étrangers qui séjournent au milieu d'eux, prend à la chasse un gibier, une bête sauvage ou un oiseau que l'on mange, il en répandra le sang et le couvrira de poussière.
17:14	Car l'âme de toute chair est dans son sang, qui est son âme. C'est pourquoi j'ai dit aux enfants d'Israël : Vous ne mangerez le sang d'aucune chair, car l'âme de toute chair est son sang : Quiconque en mangera sera retranché.
17:15	Et toute personne, née dans le pays ou étrangère, qui aura mangé de la chair d'une bête morte d'elle-même ou déchirée par les bêtes sauvages, lavera ses vêtements et se lavera avec de l'eau, et elle sera impure jusqu'au soir, puis elle sera pure.
17:16	Si elle ne lave pas ses vêtements et son corps, elle supportera les conséquences de son iniquité.

## Chapitre 18

### Condamnation de l'inceste

18:1	YHWH parla encore à Moshè, en disant :
18:2	Parle aux enfants d'Israël et dis-leur : je suis YHWH, votre Elohîm.
18:3	Vous ne ferez pas ce qui se fait dans le pays d'Égypte où vous avez habité, ni ce qui se fait dans le pays de Canaan, où je vous amène. Vous ne vivrez pas selon leurs statuts<!--Jé. 10:2.-->.
18:4	Vous pratiquerez mes ordonnances, et vous garderez mes statuts pour les suivre. Je suis YHWH, votre Elohîm.
18:5	Vous garderez donc mes statuts et mes ordonnances, l'être humain qui les pratiquera vivra par elles. Je suis YHWH<!--Ez. 20:11-13 ; Ga. 3:12 ; Ro. 10:5.-->.
18:6	Nul homme, nul homme ne s'approchera de sa proche parente selon la chair pour découvrir sa nudité. Je suis YHWH.
18:7	Tu ne découvriras pas la nudité de ton père, ni la nudité de ta mère. C'est ta mère, tu ne découvriras pas sa nudité.
18:8	Tu ne découvriras pas la nudité de la femme de ton père. C'est la nudité de ton père<!--De. 22:30 ; 1 Co. 5:1.-->.
18:9	Tu ne découvriras pas la nudité de ta sœur, fille de ton père ou fille de ta mère, née dans la maison ou hors de la maison. Tu ne découvriras pas leur nudité.
18:10	Quant à la nudité de la fille de ton fils ou de la fille de ta fille, tu ne découvriras pas leur nudité. Car elles sont ta nudité.
18:11	Tu ne découvriras pas la nudité de la fille de la femme de ton père, née de ton père. C'est ta sœur.
18:12	Tu ne découvriras pas la nudité de la sœur de ton père. Elle est la proche parente de ton père.
18:13	Tu ne découvriras pas la nudité de la sœur de ta mère, car elle est la proche parente de ta mère.
18:14	Tu ne découvriras pas la nudité du frère de ton père. Et tu ne t'approcheras pas de sa femme. Elle est ta tante.
18:15	Tu ne découvriras pas la nudité de ta belle-fille. Elle est la femme de ton fils, tu ne découvriras pas sa nudité.
18:16	Tu ne découvriras pas la nudité de la femme de ton frère. C'est la nudité de ton frère.
18:17	Tu ne découvriras pas la nudité d'une femme et de sa fille. Et tu ne prendras pas la fille de son fils, ni la fille de sa fille pour découvrir leur nudité. Elles sont tes proches parentes : ce serait un inceste.
18:18	Tu ne prendras pas aussi une femme avec sa sœur pour exciter une rivalité en découvrant sa nudité à côté d'elle pendant sa vie.

### Condamnation des abominations

18:19	Tu ne t'approcheras pas d'une femme pendant qu'elle est impure à cause de ses règles pour découvrir sa nudité.
18:20	À la femme de ton prochain tu ne donneras pas ton sperme en couchant avec elle, tu en deviendrais impur<!--Ex. 20:17 ; De. 5:21 ; Mt. 5:28.-->.
18:21	Tu ne donneras pas tes enfants pour les faire passer par le feu devant Moloc<!--Moloc est le nom du dieu auquel les Ammonites, peuple issu de la relation incestueuse de Loth et sa fille, sacrifiaient leurs premiers-nés en les jetant dans un brasier. De. 18:9-10 ; 1 R. 11:5-7 ; 2 R. 23:10 ; Jé. 32:35.-->, et tu ne profaneras pas le Nom de ton Elohîm. Je suis YHWH.
18:22	Tu ne coucheras pas aussi avec un homme<!--Vient de l'hébreu « zakar » et signifie « un mâle ».-->, comme on couche avec une femme. C'est une abomination<!--Ge. 13:13, 19:5-8 ; 1 Co. 6:9-10 ; Ro. 1:26-27.-->.
18:23	Tu ne coucheras pas aussi avec une bête pour te souiller avec elle. Et une femme ne se tiendra pas non plus devant une bête pour coucher avec elle. C'est une perversion<!--Le mot hébreu signifie aussi « confusion » ou « violation de la nature, d'un ordre divin ». Lé. 20:12 ; 1 Co. 6:9-10 ; Ro. 1:26-27.-->.
18:24	Ne vous rendez pas impurs par aucune de ces choses, car les nations que je vais chasser de devant vous se sont rendues impures par toutes ces choses.
18:25	Le pays a été rendu impur et je punirai sur lui son iniquité, et le pays vomira ses habitants.
18:26	Mais quant à vous, vous garderez mes statuts et mes ordonnances, et vous ne ferez aucune de ces abominations, tant celui qui est né dans le pays, que l'étranger qui fait son séjour parmi vous.
18:27	Car les gens de ce pays-là qui ont été avant vous, ont fait toutes ces abominations, et le pays en a été rendu impur.
18:28	Prenez garde que le pays ne vous vomisse, si vous le rendez impur, comme il a vomi la nation qui était là avant vous.
18:29	Car tous ceux qui feront l'une de toutes ces abominations, seront retranchés du milieu de leur peuple.
18:30	Vous garderez donc ce que j'ai ordonné de garder, et vous ne pratiquerez aucun de ces statuts abominables qui ont été pratiqués avant vous, et vous ne vous rendrez pas impurs par elles. Je suis YHWH, votre Elohîm.

## Chapitre 19

### Mise en garde contre l'idolâtrie

19:1	YHWH parla aussi à Moshè, en disant :
19:2	Parle à toute l'assemblée des enfants d'Israël, et dis-leur : Soyez saints, car je suis saint, moi, YHWH, votre Elohîm.
19:3	Chacun de vous craindra sa mère et son père, et vous garderez mes shabbats. Je suis YHWH, votre Elohîm<!--Ex. 20:12 ; De. 5:16 ; Mt. 15:4.-->.
19:4	Vous ne vous tournerez pas vers les faux elohîm, et vous ne vous ferez pas des elohîm en métal fondu<!--Ex. 32.-->. Je suis YHWH, votre Elohîm<!--Ex. 20:3-5.-->.

### Recommandation pour les sacrifices

19:5	Si vous sacrifiez un sacrifice d'offrande de paix<!--Voir commentaire en Lé. 3:1.--> à YHWH, vous le sacrifierez de votre bon gré.
19:6	Il se mangera le jour où vous l'aurez sacrifié, et le lendemain, mais ce qui restera jusqu'au troisième jour sera brûlé au feu.
19:7	Si on en mange au troisième jour, ce sera une abomination. Il ne sera pas agréé.
19:8	Quiconque en mangera portera son iniquité, car il aura profané une chose consacrée à YHWH. Cette personne-là sera retranchée d'entre ses peuples.

### La justice de YHWH, l'amour pour son prochain

19:9	Quand vous ferez la moisson de votre pays, tu n'achèveras pas de moissonner le bout de ton champ, et tu ne glaneras pas ce qui restera à cueillir de ta moisson.
19:10	Tu ne grappilleras pas ta vigne, ni ne recueilleras pas les grains tombés de ta vigne, mais tu les laisseras au pauvre et à l'étranger<!--De. 24:19.-->. Je suis YHWH, votre Elohîm.
19:11	Vous ne déroberez pas, et vous ne vous tromperez pas les uns les autres ; et aucun de vous ne mentira à son prochain<!--Ex. 20:15 ; Ep. 4:25 ; Col. 3:9.-->.
19:12	Vous ne jurerez pas par mon Nom en mentant, car tu profanerais le Nom de ton Elohîm<!--Ex. 20:7 ; De. 5:11.-->. Je suis YHWH.
19:13	Tu n'opprimeras pas ton prochain, et tu ne le pilleras pas<!--De. 24:14-15 ; Ja. 5:4.-->. Le salaire de ton mercenaire ne demeurera pas chez toi jusqu'au lendemain.
19:14	Tu ne maudiras pas le sourd et tu ne mettras devant un aveugle rien qui puisse le faire trébucher, mais tu craindras ton Elohîm. Je suis YHWH.
19:15	Vous ne commettrez pas d'injustice dans vos jugements. Tu n'auras pas d'égard à la personne du pauvre, et tu n'honoreras pas la personne du grand, mais tu jugeras ton prochain selon la justice.
19:16	Tu ne répandras pas de calomnies parmi ton peuple. Tu ne t'élèveras pas contre le sang de ton prochain. Je suis YHWH.
19:17	Tu ne haïras pas ton frère dans ton cœur. Tu reprendras, tu reprendras ton prochain<!--Ge. 4:8 ; Mt. 18:15 ; 1 Jn. 2:9-11.-->, et tu ne te chargeras pas d'un péché à cause de lui.
19:18	Tu n'useras pas de vengeance, et tu ne garderas pas de rancune contre les enfants de ton peuple. Tu aimeras ton prochain comme toi-même<!--Mt. 7:12 ; Mc. 12:28-34.-->. Je suis YHWH.
19:19	Vous garderez mes statuts. Tu n'accoupleras pas dans ton bétail deux bêtes d'espèce différente. Tu ne sèmeras pas dans ton champ des graines de deux espèces différentes. Tu ne mettras pas sur toi de vêtements de diverses espèces, comme de la laine et du lin.
19:20	Si un homme couche avec une femme et a une émission de sperme, si c'est une esclave, fiancée à un homme, qui n'a pas été rachetée et que la liberté ne lui a pas été donnée, ils seront punis, mais on ne les fera pas mourir parce qu'elle n'a pas été affranchie.
19:21	L'homme amènera son sacrifice de culpabilité à YHWH à l'entrée de la tente d'assignation : un bélier pour la culpabilité.
19:22	Et le prêtre fera la propitiation pour lui devant YHWH, par le bélier du sacrifice pour la culpabilité, à cause de son péché qu'il aura commis, et son péché qu'il aura commis lui sera pardonné.

### Ordonnances diverses et interdiction de tatouage

19:23	Et quand vous serez entrés dans le pays, et que vous y aurez planté quelque arbre fruitier, vous considérerez son fruit comme incirconcis. Il sera pour vous incirconcis pendant trois ans, on n'en mangera pas.
19:24	Mais à la quatrième année, tout son fruit sera une chose sainte à la louange de YHWH.
19:25	Et à la cinquième année, vous mangerez son fruit, afin qu'il vous multiplie son produit. Je suis YHWH, votre Elohîm.
19:26	Vous ne mangerez rien avec le sang<!--De. 12:23.-->. Vous ne pratiquerez ni la divination ni le spiritisme.
19:27	Vous ne couperez pas en rond les coins de votre chevelure, et vous ne raserez pas les coins de votre barbe.
19:28	Vous ne ferez pas d'incisions dans votre chair pour un mort et vous ne ferez pas de tatouage<!--Incision, impression, tatouage, marque.-->. Je suis YHWH.
19:29	Tu ne profaneras pas ta fille en la prostituant ; afin que le pays ne se prostitue pas et ne se remplisse pas de prostitution.
19:30	Vous garderez mes shabbats et vous aurez en révérence mon sanctuaire. Je suis YHWH.
19:31	Ne vous tournez pas vers ceux qui évoquent les morts, ni vers ceux qui ont un esprit de divination<!--Ac. 16:16.-->. Ne cherchez pas à vous rendre impurs avec eux. Je suis YHWH, votre Elohîm.
19:32	Tu te lèveras devant les cheveux blancs, et tu honoreras la personne du vieillard. Tu craindras ton Elohîm. Je suis YHWH.
19:33	Si un étranger séjourne dans votre pays, vous ne l'opprimerez pas.
19:34	L'étranger qui séjourne parmi vous sera pour vous comme celui qui est né parmi vous, et vous l'aimerez comme vous-mêmes, car vous avez été étrangers dans le pays d'Égypte. Je suis YHWH, votre Elohîm.
19:35	Vous ne commettrez pas d'injustice dans les jugements, ni dans les mesures de dimension, ni dans les poids, ni dans les mesures de capacité.
19:36	Vous aurez des balances justes, des poids justes, un épha juste et un hin juste. Je suis YHWH, votre Elohîm, qui vous ai fait sortir du pays d'Égypte.
19:37	Gardez donc tous mes statuts et mes ordonnances, et pratiquez-les. Je suis YHWH.

## Chapitre 20

### Abominations diverses et leurs châtiments

20:1	YHWH parla aussi à Moshè, en disant :
20:2	Tu diras aux enfants d'Israël : Si un homme, un homme d'entre les fils d'Israël ou un étranger qui séjourne en Israël, donne l'un de ses enfants à Moloc, il mourra, il mourra. Le peuple du pays le lapidera.
20:3	Et je tournerai ma face contre un tel homme, et je le retrancherai du milieu de son peuple, parce qu'il a donné de ses enfants à Moloc, pour rendre impur mon sanctuaire et profaner le Nom de ma sainteté.
20:4	Si le peuple du pays ferme, ferme les yeux sur cet homme, qui donne un de ses enfants à Moloc, et s'il ne le fait pas mourir,
20:5	je tournerai ma face contre cet homme-là, contre sa famille, et je le retrancherai du milieu de mon peuple, avec tous ceux qui se prostituent comme lui, en se prostituant à Moloc.
20:6	Si une personne se tourne vers ceux qui évoquent les morts, vers ceux qui ont un esprit de divination, en se prostituant après eux, je tournerai ma face contre cette personne-là, et je la retrancherai du milieu de son peuple.
20:7	Sanctifiez-vous donc, et soyez saints, car je suis YHWH, votre Elohîm.
20:8	Gardez aussi mes statuts et pratiquez-les. Je suis YHWH, qui vous sanctifie.
20:9	Si un homme, si un homme maudit son père ou sa mère, il mourra, il mourra : il a maudit son père ou sa mère, son sang sera sur lui.
20:10	Quant à l'homme qui commet un adultère avec la femme d'un autre, parce qu'il a commis un adultère avec la femme de son prochain, l'homme et la femme adultères mourront, ils mourront.
20:11	L'homme qui couche avec la femme de son père, découvre la nudité de son père, les deux seront mis à mort, à mort, leur sang sera sur eux.
20:12	Si un homme couche avec sa belle-fille, ils mourront, ils mourront tous les deux. Ils ont commis une perversion<!--Vient de l'hébreu « tebel » qui signifie aussi « confusion ». Voir Lé. 18:23.-->. Leur sang sera sur eux.
20:13	Si un homme couche avec un homme comme on couche avec une femme, ils ont tous les deux fait une chose abominable. Ils mourront, ils mourront. Leur sang sera sur eux.
20:14	Et si un homme prend pour femmes la fille et la mère, c'est un inceste. Il sera brûlé au feu avec elles, pour qu'il n'y ait pas chez vous d'inceste.
20:15	Si un homme couche avec une bête, il sera mis à mort, à mort et vous tuerez aussi la bête.
20:16	Et si une femme s'approche d'une bête, tu tueras cette femme et la bête. Ils mourront, ils mourront, leur sang sera sur eux.
20:17	Si un homme prend sa sœur, fille de son père ou fille de sa mère, et voit sa nudité, et qu'elle voit la nudité de cet homme, c'est une chose infâme ; ils seront donc retranchés sous les yeux des fils de leur peuple. Il a découvert la nudité de sa sœur, il portera son iniquité.
20:18	Si un homme couche avec une femme qui a ses règles et découvre sa nudité, s'il met à nu son flux et qu'elle découvre le flux de son sang, ils seront tous les deux retranchés du milieu de leur peuple.
20:19	Tu ne découvriras pas la nudité de la sœur de ta mère, ni de la sœur de ton père, car c'est découvrir sa proche parente. Ils porteront tous les deux leur iniquité.
20:20	Si un homme couche avec sa tante, il a découvert la nudité de son oncle. Ils porteront leur péché, et ils mourront privés d'enfants.
20:21	Si un homme prend la femme de son frère, c'est une impureté. Il a découvert la nudité de son frère, ils seront privés d'enfants.
20:22	Vous garderez tous mes statuts et mes jugements et vous les pratiquerez, afin que le pays où je vous fais entrer pour y habiter ne vous vomisse pas.
20:23	Vous ne marcherez pas dans les statuts des nations que je vais chasser devant vous, car elles ont fait toutes ces choses-là, et je les ai eues en abomination.
20:24	Et je vous ai dit : Vous posséderez leur pays, je vous le donnerai en possession. C'est un pays où coulent le lait et le miel. Je suis YHWH, votre Elohîm, qui vous ai séparés des autres peuples.
20:25	C'est pourquoi séparez les bêtes pures de celles qui sont impures, les oiseaux purs de ceux qui sont impurs, et ne rendez pas abominables vos personnes en mangeant des bêtes et des oiseaux impurs, ni rien qui rampe sur la terre, rien de ce que je vous ai défendu comme une chose impure.
20:26	Vous serez saints pour moi, car je suis saint, moi, YHWH. Je vous ai séparés des autres peuples afin que vous soyez à moi.
20:27	Si un homme ou une femme ont en eux l'esprit d'un mort<!--Ou « un revenant ».--> ou un esprit de divination, ils mourront, ils mourront. On les lapidera avec des pierres, leur sang sera sur eux.

## Chapitre 21

### Recommandations aux prêtres

21:1	YHWH dit aussi à Moshè : Parle aux prêtres, fils d'Aaron, et dis-leur : Aucun d'eux ne se rendra impur parmi son peuple pour un mort,
21:2	excepté pour son proche parent, pour sa mère, pour son père, pour son fils, pour sa fille, et pour son frère,
21:3	et aussi pour sa sœur vierge, qui lui est proche, et qui n'aura pas eu de mari, il se rendra impur pour elle.
21:4	Chef parmi son peuple, il ne se rendra pas impur en se profanant.
21:5	Ils ne se feront pas de place chauve sur la tête, ils ne raseront pas les coins de leur barbe, ni ne feront d'incisions dans leur chair.
21:6	Ils seront consacrés à leur Elohîm, et ils ne profaneront pas le Nom de leur Elohîm, car ils offrent à YHWH les offrandes consumées par le feu, qui sont la nourriture de leur Elohîm. C'est pourquoi ils seront saints.
21:7	Ils ne prendront pas une femme prostituée ou déshonorée, ils ne prendront pas une femme répudiée par son mari, car ils sont saints pour leur Elohîm.
21:8	Tu regarderas chacun d'eux comme saint, parce qu'ils offrent la nourriture de ton Elohîm. Ils seront saints, car je suis saint, moi, YHWH, qui vous sanctifie.
21:9	Si la fille du prêtre se profane en se prostituant, elle déshonore son père. Qu'elle soit brûlée au feu !
21:10	Le grand-prêtre d'entre ses frères, sur la tête duquel l'huile d'onction a été répandue, et qui se sera consacré pour vêtir les saints vêtements, ne découvrira pas sa tête et ne déchirera pas ses vêtements<!--Le.10:6 et Mt.26:65.-->.
21:11	Il n'ira vers aucune personne morte, il ne se rendra pas impur pour son père ni pour sa mère.
21:12	Il ne sortira pas du sanctuaire, et ne profanera pas le sanctuaire de son Elohîm, car l'huile d'onction de son Elohîm est une couronne sur lui. Je suis YHWH.
21:13	Il prendra pour femme une vierge.
21:14	Il ne prendra pas une veuve, ni une répudiée, ni une femme déshonorée ou prostituée, mais il prendra pour femme une vierge parmi son peuple.
21:15	Il ne profanera pas sa postérité parmi son peuple, car je suis YHWH qui le sanctifie.
21:16	YHWH parla aussi à Moshè, en disant :
21:17	Parle à Aaron, et dis-lui : Si quelqu'un de ta postérité, parmi tes descendants, a quelque défaut corporel, il ne s'approchera pas pour offrir la nourriture de son Elohîm.
21:18	Car tout homme en qui il y aura un défaut n'en approchera pas, l'homme aveugle, boiteux, ayant le nez camus ou qui aura un membre allongé,
21:19	ou l'homme qui aura une fracture aux pieds ou aux mains,
21:20	ou qui sera bossu ou grêle, qui aura une tache à l'œil, qui aura une gale, une dartre, ou qui aura les testicules écrasés.
21:21	Aucun homme de la postérité d'Aaron, le prêtre, en qui il y aura un défaut corporel, ne s'approchera pour offrir les offrandes consumées par le feu à YHWH. Il y a un défaut en lui, il ne s'approchera donc pas pour offrir la nourriture de son Elohîm.
21:22	Il pourra manger la nourriture de son Elohîm, tant du saint des saints que du saint,
21:23	mais il n'entrera pas vers le voile, ni ne s'approchera de l'autel, car il a un défaut corporel, et il ne profanera pas mes sanctuaires, car je suis YHWH, qui les sanctifie.
21:24	C'est ainsi que Moshè parla à Aaron et à ses fils, et à tous les enfants d'Israël.

## Chapitre 22

### Consécration d'Aaron et de ses fils

22:1	Puis YHWH parla à Moshè, en disant :
22:2	Parle à Aaron et à ses fils, afin qu'ils s'abstiennent des choses saintes des enfants d'Israël, et qu'ils ne profanent pas le Nom de ma sainteté dans les choses qu'ils me consacrent. Je suis YHWH.
22:3	Dis-leur donc : Dans toutes vos générations, tout homme de toute votre postérité qui, étant impur, s'approchera des choses saintes que les enfants d'Israël auront consacrées à YHWH, cette personne-là sera retranchée de devant moi. Je suis YHWH.
22:4	Tout homme de la postérité d'Aaron, qui aura la lèpre ou une gonorrhée, ne mangera pas des choses saintes jusqu'à ce qu'il soit pur. Il en sera de même pour celui qui touchera quelqu'un s'étant rendu impur en touchant un mort, ou celui qui aura une perte séminale,
22:5	ou l'homme qui touchera une chose grouillante et qui en aura été souillé, ou un être humain atteint d'une impureté quelconque en deviendra impur.
22:6	La personne qui touchera ces choses sera impure jusqu'au soir. Elle ne mangera pas des choses saintes si elle n'a pas lavé son corps dans l'eau.
22:7	Ensuite elle sera pure après le coucher du soleil, et elle mangera des choses saintes, car c'est sa nourriture.
22:8	Elle ne mangera de la chair d'aucune bête morte d'elle-même ou déchirée par les bêtes sauvages, pour se rendre impure par elle. Je suis YHWH.
22:9	Ils garderont ce que j'ai ordonné de garder, et ils ne commettront pas de péché au sujet de la nourriture sainte, afin qu'ils ne meurent pas, pour l'avoir profanée. Je suis YHWH, qui les sanctifie.
22:10	Aucun étranger ne mangera des choses saintes : celui qui demeure chez un prêtre et le mercenaire ne mangeront pas des choses saintes.
22:11	Mais si un prêtre achète une personne avec son argent, elle en mangera, de même pour celui qui sera né dans sa maison. Ils mangeront de sa nourriture.
22:12	Si la fille d'un prêtre est mariée à un homme étranger, elle ne mangera pas des choses saintes présentées en offrande par élévation.
22:13	Mais si la fille d'un prêtre est veuve ou répudiée, si elle n'a pas d'enfants et qu'elle soit retournée chez son père comme au temps de sa jeunesse, alors elle mangera de la nourriture de son père. Mais aucun étranger n'en mangera.
22:14	Si quelqu'un pèche involontairement en mangeant d'une chose sainte, il y ajoutera un cinquième et le donnera au prêtre avec la chose sainte.
22:15	Et ils ne profaneront pas les choses sanctifiées des enfants d'Israël, qu'ils auront offertes à YHWH.
22:16	Mais on leur fera porter la peine du péché, parce qu'ils auront mangé de leurs choses saintes. Car je suis YHWH, qui les sanctifie.

### Animaux sans défaut pour les sacrifices<!--Hé. 9:14.-->

22:17	YHWH parla encore à Moshè, en disant :
22:18	Parle à Aaron, à ses fils, et à tous les enfants d'Israël, et dis-leur : Si un homme, un homme de la maison d'Israël ou un étranger en Israël présente une offrande pour quelque vœu ou pour quelque don volontaire que ce soit, s'il la présente à YHWH en holocauste,
22:19	il offrira de son bon gré, un mâle sans défaut, parmi les bœufs, les agneaux ou les chèvres.
22:20	Vous n'offrirez aucune chose qui ait un défaut, car elle ne serait pas agréée pour vous.
22:21	Si un homme offre à YHWH un sacrifice d'offrande de paix<!--Voir commentaire en Lé. 3:1.--> en s'acquittant d'un vœu, ou en faisant une offrande volontaire, soit de gros ou de menu bétail, elle sera sans défaut pour être agréée. Il ne doit y avoir aucun défaut.
22:22	Vous n'offrirez pas à YHWH ce qui sera aveugle, estropié, ou mutilé, qui ait un ulcère, une gale ou une dartre. Vous n'en ferez pas sur l'autel un sacrifice consumé par le feu pour YHWH.
22:23	Tu pourras bien faire une offrande volontaire d'un bœuf, ou d'une brebis, ou d'une chèvre ayant quelques membres allongés, ou quelque défaut dans ses membres, mais ils ne seront pas agréés pour le vœu.
22:24	Vous ne présenterez pas à YHWH un animal dont les testicules ont été écrasés, broyés, arrachés ou coupés. Vous ne l'offrirez pas en sacrifice dans votre pays.
22:25	Vous ne prendrez de la main de l'étranger aucune de toutes ces choses pour les offrir comme nourriture à votre Elohîm, car elles sont mutilées, elles ont des défauts, elles ne seront pas agréées pour vous.

### Ordonnances diverses sur les sacrifices

22:26	YHWH parla encore à Moshè, en disant :
22:27	Quand un veau, un agneau ou un chevreau naîtra, il restera 7 jours sous sa mère. Dès le huitième jour et les suivants, il sera agréable pour l'offrande du sacrifice consumé par le feu pour YHWH.
22:28	Vous n'égorgerez pas aussi en un même jour la vache, ou la brebis, ou la chèvre avec son petit.
22:29	Quand vous offrirez un sacrifice d'action de grâces à YHWH, vous le sacrifierez de votre bon gré.
22:30	Il sera mangé le jour même, vous n'en laisserez rien jusqu'au matin. Je suis YHWH.
22:31	Gardez mes commandements et pratiquez-les. Je suis YHWH.
22:32	Ne profanez pas le Nom de ma sainteté, car je serai sanctifié au milieu des enfants d'Israël. Je suis YHWH, qui vous sanctifie,
22:33	et qui vous ai fait sortir du pays d'Égypte, pour être votre Elohîm. Je suis YHWH.

## Chapitre 23

### Les fêtes de YHWH

23:1	YHWH parla aussi à Moshè, en disant :
23:2	Parle aux enfants d'Israël et dis-leur : Les fêtes<!--Les fêtes de YHWH étaient des jours solennels, c'est-à-dire des temps fixés pour s'approcher d'Elohîm et présenter des sacrifices (Voir en annexe : le tableau « Les 7 fêtes de YHWH » et également le dictionnaire).--> solennelles de YHWH, que vous publierez, seront de saintes convocations. Voici quelles sont mes fêtes solennelles.
23:3	On fera son travail 6 jours, mais le septième jour, qui est le shabbat, le jour du repos, il y aura une sainte convocation. Vous ne ferez aucun travail, car c'est le shabbat de YHWH dans toutes vos demeures.

### La Pâque

23:4	Voici les fêtes solennelles de YHWH, qui seront de saintes convocations, que vous publierez en leur saison.
23:5	Au premier mois, le quatorzième jour du mois, entre les deux soirs, sera la Pâque<!--La Pâque était une fête qui commémorait la sortie d'Égypte (Ex. 12:1-14). Elle préfigurait la rédemption en Yéhoshoua ha Mashiah (Jésus-Christ), notre Pâque (1 Co. 5:7). Elle était fixée au 14ème jour du mois de Nisan (Mars-Avril), le premier mois.--> de YHWH.

### La fête des pains sans levain<!--Ex. 12:18, 13:6-8 ; 1 Co. 11:23-26.-->

23:6	Et le quinzième jour de ce mois, sera la fête des pains sans levain<!--La fête des pains sans levain commençait le 15ème jour du mois de Nisan et durait 7 jours. Elle annonçait le Mashiah, notre Pain descendu du ciel (Jn. 6:32-35). Seul le Seigneur Yéhoshoua a été sans levain, c'est-à-dire sans aucun péché. Le croyant est sauvé à la Pâque du Mashiah (Christ) et doit vivre une vie sans péché (la fête des pains sans levain).--> pour YHWH. Vous mangerez des pains sans levain pendant 7 jours.
23:7	Le premier jour, vous aurez une sainte convocation. Vous ne ferez aucun travail, aucun service.
23:8	Mais vous offrirez à YHWH pendant 7 jours des offrandes consumées par le feu. Et au septième jour, il y aura une sainte convocation. Vous ne ferez aucun travail, aucun service.

### La fête des prémices ou des premiers fruits<!--1 Co. 15:20-23.-->

23:9	YHWH parla aussi à Moshè, en disant :
23:10	Parle aux enfants d'Israël et dis-leur : Quand vous serez entrés dans le pays que je vous donne et que vous y ferez la moisson, vous apporterez au prêtre la première<!--La fête des prémices annonce d'abord la résurrection du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ), ensuite celle de tous ceux qui lui appartiennent (1 Th. 4:13-18 ; 1 Co. 15:23). Elle commençait le premier jour de la semaine suivant le shabbat de la fête des pains sans levain, au mois de Nisan.--> gerbe de votre moisson.
23:11	Et il agitera cette gerbe-là devant YHWH, afin qu'elle soit agréée pour vous. Le prêtre l'agitera le lendemain du shabbat.
23:12	Et le jour où vous agiterez cette gerbe, vous sacrifierez un agneau sans défaut et d'un an, en holocauste à YHWH ;
23:13	et pour son offrande de grain, deux dixièmes de fine farine, pétrie à l'huile, une offrande consumée par le feu dont le parfum est apaisant pour YHWH, et sa libation de vin sera d'un quart de hin.
23:14	Vous ne mangerez ni pain, ni grain rôti, ni grain en épi, jusqu'à ce jour-là, même jusqu'à ce que vous ayez apporté l'offrande à votre Elohîm. C'est un statut perpétuel pour vos descendants, dans toutes vos demeures.

### La fête de la pentecôte<!--Ac. 2:1-13.-->

23:15	Depuis le lendemain du shabbat, depuis le jour où vous apporterez la gerbe qui doit être agitée, vous compterez 7 shabbats<!--Voir Mc. 16:9.-->. Ils seront complets.
23:16	Vous compterez donc 50 jours<!--La fête des semaines ou fête de la moisson est également désignée comme la pentecôte. Elle avait lieu au mois de Sivan (Mai-Juin) et préfigurait l'effusion du Saint-Esprit et l'inauguration de la Nouvelle Alliance (Ac. 2:1-4). Le levain autorisé lors de cette fête évoquait par avance la présence de l'ivraie, symbole du péché et des fils du malin, parmi le blé, c'est-à-dire les enfants d'Elohîm (Mt. 13:24-41). 50 jours séparent la Pâque de la pentecôte. Cet intervalle correspond exactement à la période séparant la résurrection du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) de la naissance de l'Assemblée (Église) (Ac. 2:1-4).--> jusqu'au lendemain du septième shabbat. Et vous offrirez à YHWH une nouvelle offrande de grain.
23:17	Vous apporterez de vos demeures deux pains pour en faire une offrande agitée. Ils seront de deux dixièmes, et de fine farine, pétris avec du levain. Ce sont les prémices pour YHWH.
23:18	Vous offrirez en plus de ces pains, 7 agneaux sans défaut et d'un an, un jeune taureau pris du troupeau et 2 béliers en holocauste à YHWH, avec leur offrande de grain et leurs libations. Ce sera une offrande consumée par le feu dont le parfum est apaisant pour YHWH.
23:19	Vous ferez aussi avec un bouc un sacrifice pour le péché et avec 2 agneaux d'un an pour le sacrifice d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->.
23:20	Et le prêtre les agitera avec le pain des prémices, et avec les 2 agneaux, en offrande agitée devant YHWH. Ils seront consacrés pour YHWH, pour le prêtre.
23:21	Vous publierez donc, en ce même jour-là, une sainte convocation. Vous ne ferez aucun travail, aucun service. C'est un statut perpétuel dans toutes vos demeures, pour vos descendants.
23:22	Et quand vous ferez la moisson de votre pays, tu n'achèveras pas de moissonner le bout de ton champ, et tu ne glaneras pas les épis qui resteront de ta moisson. Mais tu les laisseras pour le pauvre et pour l'étranger. Je suis YHWH, votre Elohîm.

### La fête des trompettes

23:23	YHWH parla aussi à Moshè, en disant :
23:24	Parle aux enfants d'Israël et dis-leur : Le septième mois, le premier jour du mois, il y aura un jour de repos pour vous, un mémorial au son des trompettes<!--La fête des trompettes préfigure le rassemblement futur du peuple d'Israël après sa longue dispersion et l'enlèvement de l'Assemblée (Église). Cette fête était fixée au premier jour du septième mois (Tishri qui correspond à Septembre-Octobre).-->, une sainte convocation.
23:25	Vous ne ferez aucun travail, aucun service, et vous offrirez à YHWH des offrandes consumées par le feu.

### Le jour des expiations<!--Hé. 9:1-16.-->

23:26	YHWH parla aussi à Moshè, en disant :
23:27	Pareillement en ce même mois, qui est le septième, le dixième jour sera le jour des expiations<!--Le jour des expiations ou du grand pardon (Voir Lé. 16) était célébré le dixième jour du septième mois (Tishri). Le Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) a fait l'expiation de nos péchés afin de nous amener à Elohîm. Le propitiatoire, au lieu d'être le trône du jugement, devenait ainsi le lieu de rencontre d'Elohîm avec le croyant (Ex. 25:22). Le Mashiah est la propitiation pour nos péchés (1 Jn. 2:2), mais il est aussi lui-même le propitiatoire (Ro. 3:25). Le péché ôté, les fautes confessées, le pardon acquis, l'holocauste offert, le chemin est ouvert pour la joie de la fête des tabernacles.-->. Vous aurez une sainte convocation, vous humilierez vos âmes, et vous offrirez à YHWH des offrandes consumées par le feu.
23:28	En ce jour-là, vous ne ferez aucun travail, car c'est le jour des expiations, afin de faire la propitiation pour vous devant YHWH, votre Elohîm.
23:29	Toute personne qui ne s'humiliera pas en ce jour-là sera retranchée de son peuple.
23:30	Et toute personne qui aura fait quelque travail en ce jour-là, je ferai périr cette personne-là du milieu de son peuple.
23:31	Vous ne ferez donc aucun travail. C'est un statut perpétuel pour vos descendants dans toutes vos demeures.
23:32	Ce sera pour vous un shabbat, un jour de repos, et vous humilierez vos âmes. Le neuvième jour du mois, au soir, depuis le soir jusqu'à l'autre soir, vous célébrerez votre shabbat.

### La fête des tabernacles<!--Esd. 3:4.-->

23:33	YHWH parla aussi à Moshè, en disant :
23:34	Parle aux enfants d'Israël, et dis-leur : Le quinzième jour de ce septième mois il y aura pendant 7 jours la fête des tabernacles<!--La fête des tabernacles ou des récoltes, était la fête du souvenir et de la joie. Célébrée au mois de Tishri, elle était aussi celle du repos, dans l'accomplissement des promesses. Elle préfigure le Royaume millénaire (Za. 14).--> pour YHWH.
23:35	Le premier jour, il y aura une sainte convocation. Vous ne ferez aucun travail, aucun service.
23:36	Pendant 7 jours, vous offrirez à YHWH des offrandes consumées par le feu. Et au huitième jour, vous aurez une sainte convocation, et vous offrirez à YHWH des offrandes consumées par le feu. Ce sera une assemblée solennelle. Vous ne ferez aucun travail, aucun service.
23:37	Ce sont là les fêtes solennelles de YHWH, que vous publierez pour être des convocations saintes, afin d'offrir à YHWH des offrandes consumées par le feu, des holocaustes, des offrandes de grain, des sacrifices et des libations, chacune de ces choses en son jour.
23:38	En plus des shabbats de YHWH et en plus de vos dons, en plus de tous vos vœux, en plus de toutes les offrandes volontaires que vous présenterez à YHWH.
23:39	Mais le quinzième jour du septième mois, quand vous aurez recueilli le produit du pays, vous célébrerez la fête de YHWH pendant 7 jours. Le premier jour sera un jour de repos, le huitième aussi sera un jour de repos.
23:40	Et le premier jour, vous prendrez du fruit d'un bel arbre, des branches de palmier, des rameaux d'arbres touffus et des saules de rivière ; et vous vous réjouirez pendant 7 jours, devant YHWH, votre Elohîm.
23:41	Et vous célébrerez pour YHWH cette fête pendant 7 jours chaque année. C'est un statut perpétuel pour vos descendants. Vous la célébrerez le septième mois.
23:42	Vous demeurerez 7 jours sous des tentes. Tous ceux qui sont nés en Israël habiteront sous des tentes,
23:43	afin que votre postérité sache que j'ai fait habiter les enfants d'Israël sous des tentes, quand je les ai fait sortir du pays d'Égypte. Je suis YHWH, votre Elohîm.
23:44	C'est ainsi que Moshè parla aux enfants d'Israël des fêtes de YHWH.

## Chapitre 24

### L'huile du chandelier<!--Ex. 25:6.-->

24:1	YHWH parla aussi à Moshè, en disant :
24:2	Ordonne aux enfants d'Israël de t'apporter de l'huile pure d'olives concassées pour le chandelier, afin de faire brûler les lampes continuellement.
24:3	Aaron les arrangera devant YHWH continuellement, depuis le soir jusqu'au matin, en dehors du voile du témoignage dans la tente d'assignation. C'est un statut perpétuel pour vos descendants.
24:4	Il arrangera continuellement les lampes sur le chandelier pur, devant YHWH.

### Les pains de proposition<!--Ex. 25:23-30.-->

24:5	Tu prendras aussi de la fine farine<!--La fine farine est une farine de blé très pure, la première qui passe à travers les tamis de bluterie.-->, et tu en feras cuire 12 gâteaux<!--Les pains de proposition étaient au nombre de 12 et ne pouvaient être consommés que par les prêtres (Lé. 24:9). Ils préfiguraient le Mashiah, le véritable Pain de vie descendu du ciel (Jn. 6:48-51). Sous la Nouvelle Alliance, chaque enfant d'Elohîm est également un prêtre (Ap. 1:6), et est invité par conséquent à manger ce pain. Le nombre 12 nous parle du fondement sur lequel nous devons être bâtis, à savoir Yéhoshoua ha Mashiah (Jésus-Christ) lui-même et l'enseignement des apôtres et des prophètes (1 Co. 3:11 ; Ep. 2:20).-->, chaque gâteau sera de deux dixièmes.
24:6	Et tu les exposeras devant YHWH en 2 rangées sur la table d'or pur, 6 à chaque rangée.
24:7	Et tu mettras de l'encens pur sur chaque rangée, qui sera comme un souvenir<!--Voir commentaire en Lé. 2:2.--> pour le pain, c'est une offrande consumée par le feu à YHWH.
24:8	On les arrangera chaque jour de shabbat continuellement devant YHWH, de la part des enfants d'Israël. C'est une alliance perpétuelle.
24:9	Et ils appartiendront à Aaron et à ses fils, qui les mangeront dans un lieu saint. En effet, ce sera pour eux le saint des saints parmi les offrandes de YHWH consumées par le feu. C'est une ordonnance perpétuelle.

### Le blasphème contre le Nom de YHWH<!--Jn. 8:59, 10:31.-->

24:10	Or le fils d'une femme israélite, qui était aussi fils d'un homme égyptien, sortit parmi les enfants d'Israël, et ce fils de la femme israélite se querella dans le camp avec un homme israélite.
24:11	Et le fils de la femme israélite blasphéma et maudit le Nom de YHWH. On l'amena à Moshè. Or sa mère s'appelait Shelomiyth, fille de Dibri, de la tribu de Dan.
24:12	Et on le mit en prison, jusqu'à ce que Moshè ait déclaré ce qu'il devrait faire selon la parole de YHWH.
24:13	Et YHWH parla à Moshè, en disant :
24:14	Fais sortir du camp celui qui a maudit. Que tous ceux qui l'ont entendu mettent les mains sur sa tête, et que toute l'assemblée le lapide.
24:15	Tu parleras aux enfants d'Israël, et tu leur diras : Si un homme, un homme maudit son Elohîm, alors il portera son péché.
24:16	Et celui qui aura blasphémé le Nom de YHWH, il mourra, il mourra. Toute l'assemblée le lapidera, le lapidera. On fera mourir tant l'étranger que celui qui est né au pays, quand il aura blasphémé le Nom de YHWH.

### La violence punie

24:17	Celui qui aura frappé mortellement un être humain, quel qu'il soit, il mourra, il mourra.
24:18	Celui qui aura frappé une bête mortellement, la remplacera : Vie pour vie.
24:19	Et quand un homme aura fait une blessure à son prochain, on lui fera comme il a fait :
24:20	Fracture pour fracture, œil pour œil, dent pour dent. Selon le mal qu'il aura fait à un être humain, il lui sera fait de même.
24:21	Celui qui frappera une bête mortellement la remplacera, mais on fera mourir celui qui aura tué un être humain.
24:22	Vous rendrez un même jugement. Vous traiterez l'étranger comme celui qui est né au pays, car je suis YHWH, votre Elohîm.
24:23	Moshè parla aux enfants d'Israël, qui firent sortir hors du camp celui qui avait maudit, et le lapidèrent avec des pierres. Ainsi les enfants d'Israël firent comme YHWH l'avait ordonné à Moshè.

## Chapitre 25

### L'année shabbatique

25:1	YHWH parla aussi à Moshè sur la montagne de Sinaï, en disant :
25:2	Parle aux enfants d'Israël, et dis-leur : Quand vous serez entrés dans le pays que je vous donne, la terre se reposera. Ce sera un shabbat à YHWH.
25:3	Pendant 6 ans tu sèmeras ton champ, et pendant 6 ans tu tailleras ta vigne et tu en recueilleras le produit.
25:4	Mais la septième année il y aura un shabbat, un temps de repos pour la terre, ce sera un shabbat à YHWH. Tu ne sèmeras pas ton champ, et tu ne tailleras pas ta vigne.
25:5	Tu ne moissonneras pas ce qui proviendra des grains tombés dans ta moisson, et tu ne vendangeras pas les raisins de ta vigne non taillée. Ce sera une année de repos total pour la terre.
25:6	Mais ce qui proviendra de la terre l'année du shabbat vous servira de nourriture, à toi, à ton serviteur et à ta servante, à ton mercenaire et à l'étranger qui demeurent avec toi,
25:7	à ton bétail et aux animaux qui sont dans ton pays ; tout son produit servira de nourriture.

### L'année du jubilé

25:8	Tu compteras aussi 7 shabbats d'années, 7 fois 7 années, et les jours de 7 shabbats feront 49 ans.
25:9	Puis tu feras retentir une alarme de shofar le dixième jour du septième mois. Le jour des expiations, vous ferez retentir le shofar dans tout votre pays.
25:10	Et vous sanctifierez la cinquantième année, et publierez la liberté dans le pays à tous ses habitants. Ce sera pour vous l'année du jubilé. Vous retournerez chacun dans sa possession, chacun dans sa famille.
25:11	Cette cinquantième année vous sera l'année du jubilé. Vous ne sèmerez pas et vous ne moissonnerez pas ce que la terre rapportera d'elle-même, et vous ne vendangerez pas les fruits de la vigne non taillée.
25:12	Car c'est l'année du jubilé, elle vous sera sainte. Vous mangerez ce que les champs rapporteront cette année-là.
25:13	En cette année du jubilé chacun de vous retournera dans sa possession.
25:14	Et si tu vends de la marchandise à ton prochain, ou si tu achètes de la main de ton prochain, qu'aucun de vous ne fasse de tort à son frère.
25:15	Mais tu achèteras de ton prochain selon le nombre des années après le jubilé. Pareillement on te fera les ventes selon le nombre des années de récolte.
25:16	Plus sera grand le nombre d'années, plus tu augmenteras le prix d'achat, moins il y aura d'années, plus tu le réduiras, car on te vend le nombre des récoltes.
25:17	Aucun de vous ne maltraitera son prochain et tu dois craindre ton Elohîm, car je suis YHWH, votre Elohîm.
25:18	Pratiquez mes statuts, gardez mes jugements et observez-les, et vous habiterez en sécurité dans le pays.
25:19	Et le pays vous donnera ses fruits, vous en mangerez, vous en serez rassasiés, et vous y habiterez en sécurité.
25:20	Et si vous dites : Que mangerons-nous la septième année si nous ne semons pas, et si nous ne recueillons pas notre récolte ?
25:21	J'ordonnerai à ma bénédiction de se répandre sur vous dans la sixième année, et la terre rapportera pour 3 ans.
25:22	Puis vous sèmerez la huitième année, et vous mangerez de l'ancienne récolte jusqu'à la neuvième année, jusqu'à ce que sa récolte soit venue, vous mangerez de l'ancienne.
25:23	La terre ne sera pas vendue à perpétuité, car la terre est à moi, et vous êtes étrangers et forains<!--Forain : quelqu'un d'extérieur, d'étranger à un lieu.--> chez moi.
25:24	C'est pourquoi dans tout le pays dont vous aurez la possession, vous donnerez le droit de rachat<!--Pour voir un exemple de ce droit de rachat, voir Ru. 4:1-13.--> pour la terre.

### Le droit de rachat

25:25	Si ton frère est devenu pauvre et vend quelque chose de ce qu'il possède, celui qui a le droit de rachat, son plus proche parent, viendra et rachètera la chose vendue par son frère.
25:26	Si cet homme n'a personne qui ait le droit de rachat, et qu'il ait trouvé de lui-même suffisamment de quoi faire le rachat de ce qu'il a vendu,
25:27	il comptera les années du temps qu'il a fait la vente, et il restituera le surplus à l'homme auquel il l'avait faite, et ainsi il retournera dans sa possession.
25:28	Mais si sa main n'a pas trouvé suffisamment de quoi lui rendre, la chose vendue restera en la main de celui qui l'aura achetée, jusqu'à l'année du jubilé, puis l'acheteur en sortira au jubilé, et le vendeur retournera dans sa possession.
25:29	Et si un homme vend une maison d'habitation dans une ville entourée de murailles, il aura le droit de rachat jusqu'à la fin de l'année de sa vente. Son droit de rachat sera d'une année.
25:30	Mais si elle n'est pas rachetée avant l'accomplissement d'une année entière, la maison qui est dans la ville entourée de murailles demeurera à perpétuité à l'acheteur et à ses descendants. Il n'en sortira pas au jubilé.
25:31	Mais les maisons des villages qui ne sont pas entourés de murs seront comptées comme des fonds de terre : le vendeur aura droit de rachat et l'acheteur sortira au jubilé.
25:32	Et quant aux villes des Lévites et aux maisons des villes de leur possession, le droit de rachat sera perpétuel pour les Lévites.
25:33	Et celui qui achètera une maison des Lévites, sortira au jubilé de la maison vendue, qui est dans la ville de sa possession, car les maisons des villes des Lévites sont leur possession parmi les enfants d'Israël.
25:34	Mais les champs situés autour des villes des Lévites ne seront pas vendus, car c'est leur possession perpétuelle.

### Les traitements du frère pauvre

25:35	Si ton frère devient pauvre et qu'il tende vers toi ses mains tremblantes, tu le soutiendras, tu soutiendras aussi l'étranger, et le forain, afin qu'il vive avec toi.
25:36	Tu ne prendras pas de lui d'usure ni d'intérêt, mais tu craindras ton Elohîm, et ton frère vivra avec toi.
25:37	Tu ne lui prêteras pas ton argent à intérêt ni ne lui prêteras de tes vivres pour en tirer du profit.
25:38	Je suis YHWH, votre Elohîm qui vous ai fait sortir du pays d'Égypte, pour vous donner le pays de Canaan, afin d'être votre Elohîm.
25:39	Pareillement, si ton frère devient pauvre auprès de toi et qu'il se vende à toi, tu ne te serviras pas de lui comme on se sert des esclaves.
25:40	Mais il sera chez toi comme un mercenaire, comme un étranger, et il te servira jusqu'à l'année du jubilé.
25:41	Alors il sortira de chez toi avec ses enfants, il retournera dans sa famille et rentrera dans la possession de ses pères.
25:42	Car ils sont mes serviteurs, parce que je les ai fait sortir du pays d'Égypte. C'est pourquoi ils ne seront pas vendus comme on vend les esclaves.
25:43	Tu ne domineras pas sur lui avec dureté, et tu craindras ton Elohîm.
25:44	C'est parmi les nations qui vous entourent que tu prendras ton esclave et ta servante qui t'appartiendront, c'est d'elles que vous achèterez l'esclave et la servante.
25:45	Vous pourrez aussi en acheter des fils des étrangers qui demeureront chez toi, et même de leurs familles qui seront parmi vous, qui auront engendré dans votre pays, et vous les posséderez.
25:46	Vous les aurez comme un héritage pour les laisser à vos enfants après vous, afin qu'ils en héritent la possession, et vous vous servirez d'eux à perpétuité. Mais quant à vos frères, les fils d'Israël, personne ne dominera avec dureté sur son frère.
25:47	Et lorsque l'étranger ou celui qui demeure avec toi deviendra riche, et que ton frère qui est avec lui deviendra si pauvre qu'il se soit vendu à l'étranger qui demeure avec toi, ou à quelqu'un issu de la famille de l'étranger,
25:48	après s'être vendu, il y aura pour lui le droit de rachat. Un de ses frères le rachètera.
25:49	Son oncle, ou le fils de son oncle ou quelque autre proche parent de son sang d'entre ceux de sa famille le rachètera. Ou lui-même, s'il en trouve le moyen, se rachètera.
25:50	Et il comptera avec son acheteur depuis l'année qu'il s'est vendu à lui, jusqu'à l'année du jubilé, de sorte que l'argent du prix pour lequel il s'est vendu, se comptera à raison du nombre des années, le temps qu'il aura servi lui sera compté comme les journées d'un mercenaire.
25:51	S'il y a encore plusieurs années, il restituera le prix de son achat à raison de ces années, selon le prix pour lequel il a été acheté ;
25:52	et s'il reste peu d'années jusqu'à l'année du jubilé, il comptera avec lui, et restituera le prix de son achat à raison des années qu'il a servi.
25:53	Il aura été avec lui comme un mercenaire qui se loue d'année en année, et cet étranger ne dominera pas sur lui avec dureté en ta présence.
25:54	S'il n'est racheté d'aucune de ces manières, il sortira l'année du jubilé, lui et ses fils avec lui.
25:55	Car c'est de moi que les enfants d'Israël sont esclaves. Ce sont mes esclaves que j'ai fait sortir du pays d'Égypte. Je suis YHWH, votre Elohîm.

## Chapitre 26

### Mise en garde contre le péché

26:1	Vous ne vous ferez pas de faux elohîm, vous ne vous dresserez pas d'image taillée, ni de statue, et vous ne mettrez pas de pierres sculptées dans votre pays, pour vous prosterner devant elles, car je suis YHWH, votre Elohîm.
26:2	Vous garderez mes shabbats et vous craindrez mon sanctuaire. Je suis YHWH.

### La bénédiction conditionnelle à l'obéissance à YHWH

26:3	Si vous marchez dans mes statuts et si vous gardez mes commandements et les pratiquez,
26:4	je vous donnerai les pluies en leur temps, la terre donnera ses produits, et les arbres des champs donneront leurs fruits.
26:5	Le foulage des grains atteindra la vendange chez vous, et la vendange atteindra les semailles. Vous mangerez votre pain à satiété et vous habiterez en sécurité dans votre pays.
26:6	Je donnerai la paix au pays, vous dormirez sans que personne ne vous trouble. Je ferai disparaître les bêtes méchantes du pays, et l'épée ne passera pas par votre pays.
26:7	Vous poursuivrez vos ennemis, et ils tomberont par l'épée devant vous.
26:8	5 d'entre vous en poursuivront 100, et 100 en poursuivront 10 000, et vos ennemis tomberont par l'épée devant vous.
26:9	Je me tournerai vers vous, je vous ferai porter du fruit, je vous multiplierai, et j'établirai mon alliance avec vous.
26:10	Vous mangerez de vieilles provisions, et vous sortirez le vieux pour y loger le nouveau.
26:11	Même, je mettrai mon tabernacle au milieu de vous, et mon âme ne vous prendra pas en dégoût.
26:12	Mais je marcherai au milieu de vous, je serai votre Elohîm, et vous serez mon peuple.
26:13	Je suis YHWH, votre Elohîm, qui vous ai fait sortir du pays d'Égypte, afin que vous ne soyez pas leurs esclaves ; j'ai brisé les liens de votre joug, et je vous ai fait marcher la tête levée.

### Les châtiments en cas de désobéissance à YHWH

26:14	Mais si vous ne m'écoutez pas et que vous ne pratiquez pas tous ces commandements,
26:15	et si vous rejetez mes statuts, et si votre âme prend en dégoût mes jugements, afin de ne pas pratiquer tous mes commandements, au point de rompre mon alliance,

### La domination par les ennemis

26:16	voici alors ce que je vous ferai : Je répandrai sur vous une fin soudaine, la maladie infectieuse et la fièvre, qui vous consumeront les yeux et feront languir votre âme. Vous sèmerez en vain votre semence car vos ennemis la mangeront.
26:17	Je tournerai ma face contre vous, vous serez battus devant vos ennemis. Ceux qui vous haïssent domineront sur vous et vous fuirez sans que personne ne vous poursuive.

### Le manque de fertilité de la terre

26:18	Si après ces choses vous ne m'écoutez pas, je vous châtierai 7 fois plus à cause de vos péchés.
26:19	Je briserai l'orgueil de votre force et je ferai que votre ciel soit pour vous comme du fer, et votre terre comme du cuivre.
26:20	Votre force se consumera en vain, votre terre ne donnera pas ses produits, et les arbres de la terre ne donneront pas leurs fruits.

### Les attaques des bêtes des champs

26:21	Si vous marchez en opposition avec moi et que vous ne voulez pas m'écouter, je vous frapperai 7 fois plus, selon vos péchés.
26:22	J'enverrai contre vous les bêtes des champs, qui vous priveront de vos enfants, qui détruiront votre bétail, et vous réduiront à un petit nombre, et vos chemins seront déserts.

### La peste

26:23	Si après ces choses, vous ne recevez pas ma correction, et que vous marchiez en opposition avec moi,
26:24	je marcherai aussi en opposition avec vous, et je vous frapperai 7 fois plus, selon vos péchés.
26:25	Et je ferai venir sur vous l'épée qui vengera la vengeance de mon alliance. Quand vous vous rassemblerez dans vos villes, j'enverrai la peste au milieu de vous, et vous serez livrés entre les mains de l'ennemi.

### La famine

26:26	Lorsque je vous briserai le bâton du pain, dix femmes cuiront votre pain dans un seul four, et vous rendront votre pain au poids. Vous en mangerez et vous n'en serez pas rassasiés.
26:27	Si avec cela vous ne m'écoutez pas, et que vous marchiez en opposition avec moi,
26:28	je marcherai aussi en opposition avec vous, avec fureur, et je vous châtierai aussi 7 fois plus, selon vos péchés.
26:29	Vous mangerez la chair de vos fils, et vous mangerez aussi la chair de vos filles<!--La. 4:10.-->.
26:30	Je détruirai vos hauts lieux, j'abattrai vos statues consacrées au soleil, je mettrai vos cadavres sur les cadavres de vos idoles, et mon âme vous aura en dégoût.
26:31	Je réduirai vos villes en désert, je dévasterai vos sanctuaires et je ne respirerai plus vos parfums apaisants.

### La dispersion dans les nations<!--De. 28:58-67.-->

26:32	Je dévasterai le pays, et vos ennemis qui l'habiteront en seront étonnés.
26:33	Je vous disperserai parmi les nations, et je tirerai l'épée après vous, et votre pays sera dévasté, et vos villes désertes.
26:34	Alors la terre prendra plaisir à ses shabbats<!--2 Ch. 36:21.-->, tout le temps qu'elle sera dévastée, et lorsque vous serez dans le pays de vos ennemis, la terre se reposera et prendra plaisir à ses shabbats.
26:35	Tout le temps qu'elle sera dévastée, elle se reposera parce qu'elle ne s'était pas reposée dans vos shabbats, lorsque vous y habitiez.
26:36	Et quant à ceux d'entre vous qui survivront dans le pays de leurs ennemis, je rendrai leur cœur lâche, de sorte que le bruit d'une feuille agitée les poursuivra, ils fuiront comme on fuit devant l'épée, et ils tomberont sans que personne ne les poursuive.
26:37	L'homme trébuchera sur son frère comme devant l'épée, sans que personne ne les poursuive. Vous ne tiendrez pas devant vos ennemis.
26:38	Vous périrez parmi les nations, et le pays de vos ennemis vous consumera.
26:39	Ceux d'entre vous qui survivront pourriront dans le pays de vos ennemis à cause de leurs iniquités. Ils pourriront aussi à cause des iniquités de leurs pères.

### Repentance et restauration de l'alliance d'Abraham, de Yitzhak (Isaac) et de Yaacov (Jacob)

26:40	Alors ils confesseront leurs iniquités et les iniquités de leurs pères, selon les transgressions qu'ils auront commises contre moi, et aussi parce qu'ils auront marché en opposition avec moi.
26:41	Moi aussi, je marcherai en opposition avec eux, je les amènerai dans le pays de leurs ennemis. Et alors, leur cœur incirconcis s'humiliera, et ils recevront la peine de leur iniquité.
26:42	Et alors je me souviendrai de mon alliance avec Yaacov, et de mon alliance avec Yitzhak, et je me souviendrai aussi de mon alliance avec Abraham, et je me souviendrai du pays.
26:43	Car le pays sera abandonné par eux. Il prendra plaisir à ses shabbats pendant qu'il restera dévasté loin d'eux. Ils recevront la peine de leur iniquité, parce qu'ils ont rejeté mes ordonnances et que leur âme a eu mes statuts en dégoût.
26:44	Je m'en souviendrai lorsqu'ils seront dans le pays de leurs ennemis. Je ne les rejetterai pas, et je ne les aurai pas en dégoût pour les consumer entièrement jusqu'à rompre mon alliance avec eux, car je suis YHWH, leur Elohîm.
26:45	Je me souviendrai en leur faveur de la Première Alliance, par laquelle je les ai fait sortir du pays d'Égypte, aux yeux des nations, pour être leur Elohîm. Je suis YHWH.
26:46	Ce sont là les statuts, les ordonnances, et la torah que YHWH établit entre lui et les enfants d'Israël sur la montagne de Sinaï, par Moshè.

## Chapitre 27

### Torah sur les personnes et les biens voués à YHWH

27:1	YHWH parla aussi à Moshè, en disant :
27:2	Parle aux enfants d'Israël, et dis-leur : Lorsque quelqu'un aura fait un vœu important, s'il s'agit de personnes, elles appartiendront à YHWH d'après ton estimation.
27:3	Et l'estimation que tu feras d'un fils, d'un mâle, depuis l'âge de 20 ans jusqu'à l'âge de 60 ans, sera du prix de 50 sicles d'argent pour les fils, selon le sicle du lieu saint.
27:4	Mais si c'est une femelle, alors ton estimation sera de 30 sicles.
27:5	Si c'est un fils, un mâle de 5 ans jusqu'à 20 ans, alors ton estimation sera de 20 sicles pour les fils, et de 10 sicles pour une femelle.
27:6	Et si c'est un fils, un mâle d'un mois jusqu'à 5 ans, ton estimation sera de 5 sicles d'argent pour les fils, et l'estimation d'une femelle sera de 3 sicles d'argent.
27:7	Et si c'est un fils, un mâle de l'âge de 60 ans et au-dessus, ton estimation sera de 15 sicles pour les fils, et si c'est une femelle, de 10 sicles.
27:8	Si celui qui a fait le vœu est plus pauvre que ton estimation, on le présentera devant le prêtre qui en fera l'estimation. Le prêtre en fera l'estimation à raison de ce que peut atteindre la main de celui qui a fait le vœu.
27:9	S'il s'agit d'une bête que l'on présente en offrande à YHWH, tout ce qu'on donnera à YHWH de la sorte sera saint.
27:10	On ne la changera pas, et on n'en mettra pas une autre à la place, d'une bonne pour une mauvaise, ou une mauvaise pour une bonne. Si l'on remplace une bête par une autre bête, elles seront l'une et l'autre chose sainte.
27:11	Si c'est d'une bête impure, qu'on ne peut présenter en offrande à YHWH, on présentera la bête devant le prêtre,
27:12	qui en fera l'évaluation selon qu'elle sera bonne ou mauvaise, et il en sera fait ainsi, selon l'estimation du prêtre.
27:13	Mais si l'on veut la racheter, la racheter, on ajoutera un cinquième à ton estimation.
27:14	Et si quelqu'un consacre sa maison comme une chose sainte pour YHWH, le prêtre l'estimera selon qu'elle sera bonne ou mauvaise, et on se tiendra à l'estimation que le prêtre en aura faite.
27:15	Mais si celui qui a consacré sa maison veut la racheter, il ajoutera par-dessus un cinquième de l'argent de ton estimation, et elle lui appartiendra.
27:16	Et si un homme consacre à YHWH une partie du champ de sa propriété, ton estimation sera selon ce qu'on y sème, l'omer de semence d'orge à 50 sicles d'argent.
27:17	S'il a consacré son champ dès l'année du jubilé, on s'en tiendra à ton estimation.
27:18	Mais s'il consacre son champ après le jubilé, le prêtre estimera l'argent selon le nombre des années qui restent jusqu'à l'année du jubilé, et il sera fait une réduction sur ton estimation.
27:19	Et si celui qui a consacré le champ veut le racheter en quelque sorte que ce soit, il ajoutera par-dessus un cinquième de l'argent de ton estimation, et il lui restera.
27:20	Mais s'il ne rachète pas le champ, et que le champ se vende à un autre homme, il ne se rachètera plus.
27:21	Et quand il en sortira au jubilé, ce champ sera consacré à YHWH comme un champ voué à une entière destruction : il deviendra la propriété du prêtre.
27:22	Et s'il consacre à YHWH un champ qu'il ait acheté, qui ne soit pas des champs de sa propriété,
27:23	le prêtre lui comptera la somme de ton estimation jusqu'à l'année du jubilé, et il donnera en ce jour-là ton estimation, afin que ce soit une chose consacrée à YHWH.
27:24	Mais l'année du jubilé, le champ retournera à celui de qui il avait été acheté, et auquel était la propriété de la terre.
27:25	Et toute estimation que tu auras faite, sera selon le sicle du lieu saint : le sicle est de 20 guéras.

### Consécration des premiers-nés du bétail

27:26	Toutefois, personne ne pourra consacrer le premier-né de ses bêtes, car il appartient à YHWH en tant que premier-né, soit de bœuf, soit d'agneau, il est à YHWH.
27:27	Mais s'il s'agit d'une bête impure, il le rachètera selon ton estimation, et il ajoutera à ton estimation un cinquième, et s'il n'est pas racheté, il sera vendu selon ton estimation.

### Consécration des choses et des personnes vouées à une entière destruction pour YHWH

27:28	Et toute chose qu'un homme vouera à une entière destruction pour YHWH, parmi tout ce qu'il possède, soit un être humain, soit une bête ou un champ de sa possession, ne pourra être vendue ni rachetée. Toute chose vouée à une entière destruction sera consacrée, consacrée à YHWH.
27:29	Aucun être humain voué à une entière destruction ne pourra être racheté, mais il mourra, il mourra.

### Consécration de la dîme de la terre et du bétail

27:30	Toute dîme de la terre, tant du grain de la terre que du fruit des arbres appartient à YHWH : c'est une chose consacrée à YHWH.
27:31	Mais si quelqu'un veut racheter, racheter quelque chose de sa dîme, il y ajoutera un cinquième par-dessus.
27:32	Mais toute dîme de bœufs, de brebis et de chèvres, tout ce qui passe sous la verge, le dixième sera consacré à YHWH.
27:33	On ne choisira pas le bon ou le mauvais, et l'on ne fera pas d'échange : si on l'échange, la bête changée et l'autre seront consacrées, et ne seront pas rachetées.
27:34	Ce sont là les commandements que YHWH donna à Moshè sur la montagne de Sinaï, pour les enfants d'Israël.
