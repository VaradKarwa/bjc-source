# Philippiens (Ph.)

Auteur : Paulos (Paul)

Thème : Expérience chrétienne

Date de rédaction : Env. 60 ap. J.-C.

Fondée par Philippos II (Philippe II) (382 – 336 av. J.-C.) en 356 av. J.-C., Philippes est une ville grecque de Macédoine orientale. Située sur une voie romaine qui traversait les Balkans (Via Egnatia), elle est restée de taille modeste en dépit de son fort taux de fréquentation.

La première mention de l'assemblée de Philippes se trouve dans Actes 16, lors de la rencontre de Paulos (Paul) avec des femmes réunies à l'extérieur de la ville pour la prière. À travers les paroles de Paulos, le Seigneur toucha particulièrement Ludia (Lydie) qui, après avoir été baptisée avec sa famille, le reçut avec ses compagnons d'œuvre dans sa maison.

C'est à Rome, sous le règne de Néron (37 – 68 ap. J.-C.), que Paulos, alors captif, rédigea cette lettre. Cet écrit de l'apôtre accusait réception d'un don monétaire que l'assemblée de Philippes lui avait fait parvenir par le biais d'Epaphroditos (Épaphrodite). Paulos y exprimait sa joie en dépit des souffrances et invitait les Philippiens à faire de même. Loin des erreurs doctrinales reprochées à d'autres, ces chrétiens de Philippes recevaient ainsi l'expression de l'affection de Paulos et ses encouragements à persévérer dans la foi en Mashiah en toutes circonstances.

## Chapitre 1

### Introduction

1:1	Paulos et Timotheos, esclaves de Yéhoshoua Mashiah, à tous les saints en Yéhoshoua Mashiah qui sont à Philippes, avec les surveillants et les diacres :
1:2	que la grâce et la paix vous soient données de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !
1:3	Je rends grâce à mon Elohîm toutes les fois que je fais mention de vous,
1:4	faisant toujours de supplication avec joie pour vous tous, dans toute ma supplication,
1:5	à cause de votre attachement à l'Évangile, depuis le premier jour jusqu'à maintenant.
1:6	Étant persuadé que celui qui a commencé cette bonne œuvre en vous, la mènera à son terme jusqu'au jour de Yéhoshoua Mashiah.
1:7	Comme il est juste que je pense ainsi de vous tous, parce que je retiens dans mon cœur que vous avez tous été participants de la grâce avec moi dans mes liens, et dans la défense et la confirmation de l'Évangile.
1:8	Car Elohîm m'est témoin que je vous aime tous tendrement, conformément à l'amour de Yéhoshoua Mashiah.
1:9	Et ce que je demande en priant, c'est que votre amour abonde encore de plus en plus en connaissance précise et correcte et en toute intelligence,
1:10	pour éprouver<!--Voir 1 Ti. 3:10.--> ce qui est important, afin que vous soyez purs et irréprochables pour le jour du Mashiah,
1:11	étant remplis de fruits de justice, par le moyen de Yéhoshoua Mashiah à la gloire et à la louange d'Elohîm.

### Les chrétiens encouragés par les souffrances de Paulos (Paul)

1:12	Or je veux que vous sachiez, mes frères, que les choses qui me sont arrivées ont plutôt servi au progrès de l'Évangile.
1:13	De sorte que mes liens en Mashiah ont été rendus célèbres dans tout le prétoire, et partout ailleurs.
1:14	Et la plupart de nos frères dans le Seigneur, ayant pris confiance par mes liens, ont beaucoup plus d'audace pour annoncer la parole sans aucune crainte.
1:15	En effet, certains prêchent le Mashiah par envie et par un esprit de dispute, mais d'autres le font au contraire avec une bonne volonté.
1:16	Les uns annoncent en effet le Mashiah par un esprit de parti<!--Voir Ph. 2:3.-->, et non pas purement, croyant ajouter de la tribulation à mes liens.
1:17	Mais les autres le font par amour, sachant que je suis établi pour la défense de l'Évangile<!--Les versets 16 et 17 sont inversés dans les bibles basées sur les textes minoritaires. Le texte majoritaire (byzantin) présente les versets dans cet ordre.-->.
1:18	Car, de toute façon, que ce soit seulement par prétexte ou en vérité, Mashiah est annoncé. Et cependant en cela je me réjouis et je me réjouirai.
1:19	Car je sais que cela tournera à mon salut par le moyen de vos supplications et de l'assistance de l'Esprit de Yéhoshoua Mashiah,
1:20	selon ma ferme attente et mon espérance, je n'aurai honte de rien. Mais avec une entière assurance, maintenant comme toujours, Mashiah sera glorifié dans mon corps, soit par ma vie, soit par ma mort.
1:21	Car pour moi, vivre c'est le Mashiah, et mourir est un avantage.
1:22	Mais si vivre dans cette chair m’est un fruit pour l'œuvre, je ne sais même pas lequel je choisirai.
1:23	Car je suis pressé des deux côtés : j'ai le désir de quitter<!--« Quitter » est la traduction du grec « analuo » qui signifie « délier, partir, quitter cette vie, interrompre, retourner, etc. ».--> cette vie et d'être avec Mashiah, ce qui est pour moi de beaucoup le meilleur,
1:24	et de demeurer dans la chair, chose plus nécessaire à cause de vous.
1:25	Et je suis persuadé, je sais que je resterai et que je demeurerai avec vous tous, pour votre avancement et pour votre joie dans la foi,
1:26	afin que vous ayez en moi un sujet de vous glorifier de plus en plus en Yéhoshoua Mashiah, par mon retour au milieu de vous.
1:27	Seulement, conduisez-vous d'une manière digne de l'Évangile du Mashiah, afin que, soit que je vienne vous voir, soit que je reste absent, j'entende quant à votre état, que vous persistez dans un même esprit, combattant ensemble d'une même âme pour la foi de l'Évangile,
1:28	et n'étant en rien effrayés par vos adversaires. En effet, pour eux c'est une preuve de perdition, mais pour vous de salut, et cela de la part d'Elohîm.
1:29	Parce qu'il vous a été gratuitement donné en ce qui concerne Mashiah, non seulement de croire en lui, mais aussi de souffrir pour lui,
1:30	en soutenant le même combat dans lequel vous m'avez vu, et dans lequel vous entendez dire que je suis encore.

## Chapitre 2

### Exhortation à l'unité

2:1	Si donc il y a quelque consolation dans le Mashiah, s'il y a quelque parole persuasive dans l'amour, s'il y a quelque communion d'esprit, s'il y a quelque compassion et quelque miséricorde,
2:2	rendez ma joie parfaite, ayant une même pensée<!--Voir Ph. 2:5.-->, un même amour, une même âme, et consentant tous à une même chose.
2:3	Ne faites rien par esprit de parti<!--« Parti » est la traduction du grec « eritheia ». Avant la Nouvelle Alliance, ce mot ne se trouve que dans les écrits d'Aristote (philosophe grec, disciple de Platon, né en 384 et mort en 322 av. J.-C.) où il dénote une « recherche personnelle, la poursuite d'une fonction politique par des moyens injustes ». Ce mot signifie aussi « faire une campagne électorale » ou « intriguer pour une fonction dans un esprit partisan, querelleur ». On retrouve le mot grec dans Ja. 3:14,16 ; Ga. 5:20 ; Ro. 2:8 ; Ph. 1:16.-->, ou par vaine gloire, mais dans l’humilité, estimez les autres supérieurs à vous-mêmes.
2:4	Ne regardez pas chacun à votre propre intérêt, mais aussi à celui des autres.

### L'humilité du Mashiah

2:5	Qu'il y ait donc en vous la même pensée que dans le Mashiah Yéhoshoua,
2:6	lequel étant en forme d'Elohîm, n'a pas regardé son égalité avec Elohîm comme une proie à saisir.
2:7	Cependant il s'est vidé de lui-même en prenant la forme d'esclave, en devenant semblable aux humains,
2:8	et, reconnu à son apparence comme un être humain, il s'est abaissé lui-même, en se rendant obéissant jusqu'à la mort, même jusqu'à la mort de la croix.
2:9	C'est pourquoi aussi Elohîm l'a élevé à la suprême majesté et lui a donné le Nom<!--Voir Jn. 17:6,11-12,26.--> qui est au-dessus de tout nom,
2:10	afin qu'au Nom de Yéhoshoua, tout genou fléchisse, tant de ceux qui sont dans les cieux, que de ceux qui sont sur la Terre, et sous la Terre,
2:11	et que toute langue confesse que Yéhoshoua Mashiah est le Seigneur, à la gloire d'Elohîm le Père.
2:12	C'est pourquoi, mes bien-aimés, comme vous avez toujours obéi, mettez en œuvre votre propre salut avec crainte et tremblement, non seulement comme en ma présence, mais beaucoup plus maintenant que je suis absent.
2:13	Car c'est Elohîm qui opère en vous le vouloir et le faire pour son bon plaisir.
2:14	Faites toutes choses sans murmures et sans raisonnements,
2:15	afin que vous soyez sans reproche et sans mélange<!--Voir Mt. 10:16.-->, des enfants d'Elohîm innocents au milieu d'une génération tordue<!--Voir Ac. 2:40.--> et déformée, parmi laquelle vous brillez comme des luminaires<!--Voir Ap. 21:11.--> dans le monde, portant la parole de vie,
2:16	pour que je puisse me glorifier au jour du Mashiah de n'avoir pas couru en vain, ni travaillé en vain.
2:17	Et même si je sers de libation sur le sacrifice et le service de votre foi, j'en suis heureux et je me réjouis avec vous tous.
2:18	Vous aussi, pareillement, soyez heureux et réjouissez-vous avec moi.

### Paulos (Paul) témoigne de Timotheos (Timothée) et d'Epaphroditos (Épaphrodite)

2:19	Et j'espère dans le Seigneur Yéhoshoua vous envoyer rapidement Timotheos pour être moi-même encouragé en apprenant ce qui vous concerne.
2:20	Car je n'ai personne d'un pareil courage pour s'inquiéter sincèrement de votre situation,
2:21	car tous cherchent leurs propres intérêts, et non ceux de Yéhoshoua Mashiah.
2:22	Mais vous savez qu'il a été mis à l'épreuve, puisqu'il a servi avec moi dans l'Évangile, comme un enfant avec son père.
2:23	J'espère donc vous l'envoyer dès que j'aurai vu en effet comment cela se passera avec moi.
2:24	Et j'ai cette confiance dans le Seigneur que moi-même aussi je viendrai rapidement.
2:25	Mais j'ai cru nécessaire de vous envoyer Epaphroditos, mon frère, mon compagnon d'œuvre et mon compagnon d'armes, par qui vous m'aviez envoyé de quoi pourvoir à mes besoins.
2:26	Car il soupirait après vous tous et il était troublé parce que vous aviez appris qu'il avait été malade.
2:27	Car il a été malade, et tout près de la mort, mais Elohîm a eu pitié de lui, et non seulement de lui, mais aussi de moi, afin que je n'aie pas tristesse sur tristesse.
2:28	Je l'ai donc envoyé à cause de cela avec plus de soin, afin qu'en le revoyant vous ayez de la joie, et que j'aie moins de tristesse.
2:29	Recevez-le donc en notre Seigneur avec une joie entière et ayez de l'estime pour ceux qui sont tels que lui.
2:30	Parce qu'il a été près de la mort à cause de l'œuvre du Mashiah, n'ayant eu aucun égard à sa propre vie, afin de suppléer au défaut de votre service envers moi.

## Chapitre 3

### Le légalisme et la justice de la loi mosaïque

3:1	Au reste, mes frères, réjouissez-vous dans le Seigneur. En effet, je ne me lasse<!--Littéralement « paresseux ».--> pas de vous écrire les mêmes choses, et pour vous, c'est une sécurité.
3:2	Prenez garde aux chiens, prenez garde aux mauvais ouvriers, prenez garde à la mutilation<!--Vient du grec « katatome » qui signifie aussi « couper ».-->.
3:3	Car c'est nous qui sommes la circoncision, nous qui rendons à Elohîm notre culte par l'Esprit, et qui nous glorifions en Yéhoshoua Mashiah et qui n'avons pas confiance dans la chair.
3:4	Bien que j'aie aussi confiance dans la chair. Si quelqu’un d’autre pense se confier dans la chair, à plus forte raison moi :
3:5	circoncis dès le huitième jour, de la race d'Israël, de la tribu de Benyamin, Hébreu né d'Hébreux, pharisien en ce qui concerne la torah.
3:6	Quant au zèle, persécutant l'Assemblée. Et quant à la justice à l'égard de la torah, devenu sans reproche.

### L'excellence de la connaissance de Yéhoshoua ha Mashiah

3:7	Mais ces choses qui étaient pour moi un avantage, je les ai regardées comme une perte<!--Le mot grec signifie aussi « dommage ». Voir Ac. 27:10,21.--> à cause du Mashiah.
3:8	Mais au contraire, je considère même que toutes choses sont une perte à cause de l'excellence de la connaissance de Yéhoshoua Mashiah, mon Seigneur. À cause de lui, j’ai accepté la perte de toutes choses, et je les considère comme les excréments des animaux afin de gagner Mashiah,
3:9	et d'être trouvé en lui, ayant non pas ma propre justice, celle qui vient de la torah, mais celle qui est par la foi au Mashiah, la justice qui vient d'Elohîm par la foi.
3:10	Ainsi, je connaîtrai Yéhoshoua Mashiah et la puissance de sa résurrection, et la communion de ses souffrances, en devenant conforme à lui dans sa mort,
3:11	pour parvenir en effet, par n'importe quel moyen, à la résurrection d'entre les morts.
3:12	Non que j'aie déjà atteint le but, ou que je sois déjà rendu parfait, mais je poursuis ce but pour tâcher d'y parvenir, car c'est pour cela que j'ai été aussi saisi par le Mashiah, Yéhoshoua.
3:13	Frères, pour moi, je n’estime pas moi-même l’avoir saisi,
3:14	mais je fais en effet une chose : oubliant les choses qui sont en arrière, et me portant vers celles qui sont en avant, je cours vers le but, pour remporter le prix de la vocation céleste d'Elohîm en Yéhoshoua Mashiah.
3:15	C'est pourquoi, nous tous qui sommes parfaits, ayons cette même pensée et, si sur quelque point vous avez une pensée différente, Elohîm vous révèlera aussi ce qu'il en est.
3:16	Cependant, marchons suivant une même règle<!--Du grec « kanon » signifiant « règle, modèle », lui-même emprunté à l'hébreu « qaneh » qui signifie « roseau, mesure, canne ». Voir Ga. 6:16.--> pour les choses auxquelles nous sommes parvenus, et ayons un même sentiment.
3:17	Soyez tous ensemble mes imitateurs, mes frères, et regardez à ceux qui marchent selon le modèle que vous avez en nous.
3:18	Car beaucoup dont je vous ai souvent parlé et dont je parle maintenant même en pleurant, marchent en ennemis de la croix du Mashiah.
3:19	Eux dont la fin est la destruction, qui ont pour elohîm leur ventre, qui mettent leur gloire dans leur honte, et qui ne pensent<!--Ro. 8:5.--> qu'aux choses de la Terre.
3:20	Mais pour nous, notre cité est dans les cieux, d'où nous attendons aussi assidûment et patiemment le Sauveur, le Seigneur Yéhoshoua Mashiah,
3:21	qui transformera le corps de notre humiliation pour le rendre conforme au corps de sa gloire selon le pouvoir<!--Ep. 3:7.--> par lequel il peut même s'assujettir toutes choses.

## Chapitre 4

### Encouragements

4:1	C'est pourquoi, mes très chers frères bien-aimés, vous qui êtes ma joie et ma couronne, demeurez ainsi fermes dans le Seigneur, mes bien-aimés.
4:2	J'exhorte Euodia, et j'exhorte aussi Syntyche, à être d'une même pensée dans le Seigneur.
4:3	Et toi aussi, mon vrai compagnon<!--Le mot « compagnon » est la traduction du grec « suzugos » qui signifie littéralement « ensemble sous le joug, compagnon de peine, de joug ». Cette expression renvoie à 2 Co. 6:14.-->, oui je te prie de les aider, elles qui ont combattu avec moi pour l'Évangile, avec Clément, et mes autres compagnons d'œuvre, dont les noms sont dans le livre de vie.
4:4	Réjouissez-vous toujours dans le Seigneur ! Je vous le répète : réjouissez-vous !
4:5	Que votre douceur soit connue de tous les humains. Le Seigneur est proche.
4:6	Ne vous inquiétez de rien, mais en toute chose faites connaître vos requêtes à Elohîm par la prière et la supplication avec action de grâce.
4:7	Et la paix d'Elohîm, qui surpasse toute intelligence, gardera vos cœurs et vos sentiments en Yéhoshoua Mashiah.

### L'objet de nos pensées

4:8	Au reste, mes frères, que toutes les choses qui sont vraies, toutes celles qui sont honorables, toutes celles qui sont justes, toutes celles qui sont pures, toutes celles qui sont aimables, toutes celles qui sont de bonne réputation, toutes celles où il y a quelque vertu et quelque louange, que ces choses soient l'objet de vos pensées.
4:9	Vous les avez aussi apprises, et reçues, et entendues et vues en moi : mettez-les en pratique, et l'Elohîm de paix sera avec vous.

### Le contentement dans l'abondance et dans le besoin

4:10	Or je me suis réjoui grandement dans le Seigneur de ce qu'enfin vous avez fait refleurir votre pensée pour moi. Vous y pensiez bien, mais l'occasion vous manquait.
4:11	Je ne dis pas cela à cause de mes besoins, car j'ai appris à être content de l'état où je me trouve.
4:12	Mais je sais être abaissé, je sais aussi être dans l'abondance. En tout et en tous je suis enseigné pleinement<!--« Initié dans les mystères », « instruire », « accoutumer quelqu'un à une chose », « donner à quelqu'un une connaissance intime d'une chose ».--> à être rassasié et à avoir faim, à être dans l'abondance et à être dans le besoin.
4:13	Je peux tout par le Mashiah qui me fortifie.
4:14	Néanmoins, vous avez bien fait de prendre part à ma tribulation.
4:15	Or vous savez aussi, vous Philippiens, qu'au commencement de l'Évangile, quand je partis de Macédoine, aucune assemblée ne communiqua avec moi en matière de donner et de recevoir, excepté vous seuls.
4:16	Et même lorsque j'étais à Thessalonique, vous m'avez envoyé une fois, et même deux fois, ce dont j'avais besoin.
4:17	Ce n'est pas que je recherche le don, mais je recherche le fruit qui se multiplie pour votre compte.
4:18	Mais j'ai tout et je suis dans l'abondance. J'ai été comblé de biens en recevant d'Epaphroditos ce qui vient de vous, un parfum de bonne odeur, un sacrifice accepté, agréable à Elohîm.
4:19	Mais mon Elohîm comblera<!--Vient du grec « pleroo » qui signifie « remplir jusqu'au bord », « amener à la réalisation, réaliser », « accomplir ». Voir Col. 2:10.--> tous vos besoins selon sa richesse, avec gloire en Yéhoshoua Mashiah.

### Salutations

4:20	Mais à notre Elohîm et Père soit la gloire d'âge en âge ! Amen !
4:21	Saluez tous les saints en Yéhoshoua Mashiah. Les frères qui sont avec moi vous saluent.
4:22	Tous les saints vous saluent, et principalement ceux qui sont de la maison de César.
4:23	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous tous ! Amen !
