# Chaggay (Aggée) (Ag.)

Signification : En fête, né un jour de fête

Auteur : Chaggay

Thème : Reconstruction du temple

Date de rédaction : 6ème siècle av. J.-C.

Chaggay, contemporain de Zekaryah (Zacharie), exerça son service dans le royaume de Yéhouda (Juda) après le retour de l'exil. Alors que la reconstruction du temple était négligée, Chaggay reçut un message rappelant au peuple quelles devaient être ses priorités et redéfinissant les exigences de YHWH en matière de sainteté. Ce récit montre la bénédiction accompagnant celui qui oublie ses propres intérêts et qui prend véritablement à cœur l'œuvre d'Elohîm.

## Chapitre 1

### Israël coupable de négligence

1:1	La seconde année du roi Darius, au sixième mois, le premier jour du mois, la parole de YHWH vint par le moyen de Chaggay, le prophète, à Zorobabel, fils de Shealthiel, gouverneur de Yéhouda, et à Yéhoshoua, fils de Yehotsadaq, le grand-prêtre, en ces mots<!--Esd. 4:24.--> :
1:2	Ainsi parle YHWH Sabaoth, disant : Ce peuple dit : Le temps n'est pas encore venu, le temps de rebâtir la maison de YHWH.
1:3	C'est pourquoi la parole de YHWH vint par le moyen de Chaggay, le prophète, en disant :
1:4	Est-il temps pour vous d'habiter dans vos maisons lambrissées pendant que cette maison est en ruine ?
1:5	Maintenant donc, ainsi parle YHWH Sabaoth : Fixez votre cœur sur vos voies !
1:6	Vous avez semé beaucoup, mais vous avez récolté peu. Vous avez mangé, mais non pas jusqu'à être rassasiés. Vous avez bu, mais vous n'avez pas eu de quoi boire abondamment. Vous êtes vêtus, mais non pas jusqu'à en être réchauffés. Et celui qui se loue se loue pour mettre son salaire dans un sac percé<!--Mi. 6:14-15.-->.
1:7	Ainsi parle YHWH Sabaoth : Fixez votre cœur sur vos voies.
1:8	Montez à la montagne, apportez du bois, et bâtissez cette maison ! J'y prendrai plaisir et je serai glorifié, a dit YHWH.
1:9	Vous comptiez sur beaucoup, et voici, il y a eu peu : vous l'avez apporté à la maison et j'ai soufflé dessus. Pourquoi ? À cause de ma maison, dit YHWH Sabaoth, parce qu'elle est en ruine pendant que vous vous empressez chacun pour sa maison.
1:10	À cause de cela, les cieux au-dessus de vous retiennent la rosée, et la terre a retenu ses fruits<!--Lé. 26:19 ; De. 28:23.-->.
1:11	J'ai appelé la sécheresse sur la terre, sur les montagnes, sur le blé, sur le moût, sur l'huile, sur ce que le sol produit, sur les humains, sur les bêtes et sur tout le travail des mains<!--Am. 4:7 ; Ps. 105:16.-->.

### YHWH réveille son peuple

1:12	Zorobabel donc, fils de Shealthiel, et Yéhoshoua, fils de Yehotsadaq, le grand-prêtre, et tout le reste du peuple, entendirent la voix de YHWH, leur Elohîm, et les paroles de Chaggay, le prophète, envoyé pour cela par YHWH, leur Elohîm. Et le peuple eut de la crainte devant YHWH.
1:13	Chaggay, le messager de YHWH, parla au peuple, selon le message de YHWH, en disant : Je suis avec vous, déclaration de YHWH.
1:14	YHWH réveilla l'esprit de Zorobabel, fils de Shealthiel, gouverneur de Yéhouda, et l'esprit de Yéhoshoua, fils de Yehotsadaq, le grand-prêtre, et l'esprit de tout le reste du peuple. Et ils vinrent et travaillèrent à la maison de YHWH, leur Elohîm,
1:15	le vingt-quatrième jour du sixième mois, de la seconde année du roi Darius.

## Chapitre 2

### Encouragements à poursuivre la construction

2:1	Le vingt et unième jour du septième mois, la parole de YHWH vint par le moyen de Chaggay, le prophète, en disant :
2:2	Parle maintenant à Zorobabel, fils de Shealthiel, gouverneur de Yéhouda, et à Yéhoshoua, fils de Yehotsadaq, le grand-prêtre, et au reste du peuple, en disant :
2:3	Quel est parmi vous le survivant qui a vu cette maison dans sa première gloire ? Et comment la voyez-vous maintenant ? N'est-elle pas comme un rien à vos yeux, au prix de celle-là<!--Esd. 3:12.--> ?
2:4	Maintenant donc Zorobabel, fortifie-toi ! dit YHWH. Toi aussi, Yéhoshoua, fils de Yehotsadaq, grand-prêtre, fortifie-toi ! Vous aussi, tout le peuple du pays, fortifiez-vous ! dit YHWH. Et travaillez ! Car je suis avec vous, dit YHWH Sabaoth.
2:5	Avec la parole de l'Alliance que j'ai traitée avec vous quand vous êtes sortis d'Égypte, et mon Esprit qui se tient debout au milieu de vous : n'ayez pas peur<!--Za. 4:6.--> !
2:6	Car ainsi parle YHWH Sabaoth : Encore un peu de temps, et je secouerai les cieux et la Terre, la mer et le sec<!--Hé. 12:26.-->.
2:7	Je secouerai toutes les nations, et le désir<!--Celui qui est digne du désir et de l'attente des nations n'est autre que le Mashiah (Christ).--> de toutes les nations viendra, et je remplirai de gloire cette maison, dit YHWH Sabaoth.
2:8	L'argent est à moi, et l'or est à moi, dit YHWH Sabaoth.
2:9	La gloire de cette dernière maison sera plus grande que celle de la première, dit YHWH Sabaoth, et je mettrai la paix en ce lieu, dit YHWH Sabaoth.

### Purification et sanctification du peuple

2:10	Le vingt-quatrième jour du neuvième mois de la seconde année de Darius, la parole de YHWH vint par le moyen de Chaggay le prophète, en disant :
2:11	Ainsi parle YHWH Sabaoth : Interroge maintenant les prêtres sur la torah en leur demandant :
2:12	Si un homme porte de la chair consacrée dans le pan de son vêtement, et que ce vêtement touche du pain, ou un mets cuit, ou du vin, ou de l'huile, ou un aliment quelconque, cela devient-il sanctifié ? Et les prêtres répondirent et dirent : Non !
2:13	Alors Chaggay dit : Si celui qui est souillé pour un mort touche toutes ces choses-là, ne seront-elles pas souillées ? Et les prêtres répondirent et dirent : Elles seront souillées<!--Lé. 17:15 ; No. 19:22 ; Tit. 1:15.-->.
2:14	Alors Chaggay répondit et dit : Tel est ce peuple et telle est cette nation devant ma face, dit YHWH, et telles sont toutes les œuvres de leurs mains : même ce qu'ils offrent ici est impur.
2:15	Maintenant donc, je vous prie, fixez votre cœur sur ce qui s'est passé depuis ce jour et par la suite ! Avant qu'on ait mis pierre sur pierre au temple de YHWH !
2:16	Avant que ces choses n'existent, quand on allait vers un tas de grain de 20 mesures, il n'en existait que 10. Et quand on allait au pressoir pour puiser 50 mesures de la cuve, il n'en existait que 20.
2:17	Je vous ai frappés par la sécheresse, par la rouille et par la grêle dans tout le travail de vos mains. Et vous n'êtes pas revenus à moi, dit YHWH<!--De. 28:22 ; 1 R. 8:37 ; Am. 4:9 ; 2 Ch. 6:28.-->.
2:18	Je vous prie, fixez votre cœur, depuis ce jour-ci et dans la suite, depuis le vingt-quatrième jour du neuvième mois, depuis le jour où les fondements du temple de YHWH ont été posés ! Fixez votre cœur !
2:19	Y a-t-il encore de la semence dans les greniers ? Même jusqu'à la vigne, au figuier, au grenadier, et à l'olivier, rien n'a rapporté ; mais depuis ce jour-ci, je donnerai la bénédiction.

### Destruction des royaumes des nations

2:20	Et la parole de YHWH vint pour la seconde fois à Chaggay, le vingt-quatrième jour du mois, en disant :
2:21	Parle à Zorobabel, gouverneur de Yéhouda, et dis-lui : Je secouerai les cieux et la Terre,
2:22	je renverserai le trône des royaumes. Je détruirai la force des royaumes des nations, je renverserai les chars et ceux qui les montent, les chevaux et ceux qui les montent seront abattus, chacun par l'épée de son frère.
2:23	En ce jour-là, dit YHWH Sabaoth, je te prendrai, ô Zorobabel, fils de Shealthiel, mon serviteur, dit YHWH, et je me servirai de toi comme un sceau, car c'est toi que j'ai choisi, dit YHWH Sabaoth.
