# Bereshit (Genèse) (Ge.)

Signification : Au commencement

Auteur : Probablement Moshè (Moïse)

Thème : La création de l'être humain

Date de rédaction : Env. 1450 – 1410 av. J.-C.

Premier livre du Tanakh, Bereshit est le livre du commencement. Il relate l'histoire des origines de l'humanité, la création des cieux, de la Terre et de tout ce qui s'y trouve par YHWH, l'Elohîm créateur.

Il y est décrit le péché de l'être humain et sa séparation d'avec Elohîm, ainsi que la décadence de l'univers qui en résulta. En réponse à la méchanceté du cœur de l'humain, YHWH exerça sa justice en détruisant la Terre par le déluge. Dans sa prescience, YHWH avait cependant résolu de se réconcilier avec l'être humain. Il se révéla donc comme Sauveur en accordant sa grâce à Noah (Noé) et à sa famille. Après cet événement, les êtres humains se tournèrent une fois de plus vers le mal en tentant Elohîm par la construction de la tour de Babel, œuvre à l'origine de la dispersion des nations.

Ce livre présente aussi l'élection d'Abraham, originaire d'Our en Chaldée (Mésopotamie antique, dans l'actuel Irak), qui reçut la promesse divine de devenir une grande nation, en qui toutes les familles de la Terre seraient bénies. Le récit se poursuit par l'histoire de ses descendants : Yitzhak (Isaac), Yaacov (Jacob) et ses douze fils, qui formèrent par la suite la nation d'Israël.

## Chapitre 1

### La Terre devint informe et vide

1:1	<w lemma="strong:H07225">Au commencement</w><!--Bereshit bara elohîm èth hashamaïm vehèth ah aretz. Dans ce passage le mot « èth » est composé de la première et la dernière lettre de l'alphabet hébraïque « aleph » et « tav ». Aleph Tav apparaît plus de 7 000 fois dans le Tanakh. Nous notons la présence d'Aleph Tav : au commencement (Ge. 1:1), avec la Lumière (Ge. 1:4), Adam (Ge. 4:1), Hénoc (Ge. 5:22), Yossef (Joseph, Ge. 37:23), Moshè (Moïse, Ex. 18:23), Aaron et le pectoral (Ex. 28:28-30), les Hébreux (No. 14:22), Shema Israël (De. 6:4). Aleph Tav est le symbole du caractère hébreu considéré comme la signature du Mashiah (Messie) : Et les Juifs regarderont vers Aleph Tav, lequel ils ont percé (Za. 12:10).--> <w lemma="strong:H0430">Elohîm</w> <w morph="strongMorph:TH8804" lemma="strong:H0853 strong:H01254">créa</w> <w lemma="strong:H08064">les cieux</w> <w lemma="strong:H0853">et</w> <w lemma="strong:H0776">la Terre</w>.
1:2	La Terre devint informe et vide<!--Les termes « informe » et « vide » viennent des mots hébreux « tohuw » et « bohuw » qui désignent la confusion, le chaos, la vanité.-->. Les ténèbres étaient sur la face de l'abîme et l'Esprit d'Elohîm planait au-dessus des eaux.

### Jour « un » : Apparition de la lumière

1:3	Elohîm dit : Que la lumière apparaisse !<!--« Que la lumière apparaisse ! » (Es. 9:1 ; Mt. 4:16 ; Jn. 1:1-5). Cette lumière n'est autre que YHWH lui-même qui va s'incarner en la personne de Yéhoshoua ha Mashiah (Jésus-Christ) pour chasser les ténèbres (2 S. 22:9-12 ; Es. 60:1,19-20 ; Jn. 1:1-5, 8:12-14 ; 2 Co. 4:6).--> Et la lumière apparut.
1:4	Et Elohîm vit que la lumière était bonne, et Elohîm sépara la lumière des ténèbres<!--Ou Elohîm mit une séparation entre la lumière et les ténèbres.-->.
1:5	Elohîm appela la lumière jour, et il appela les ténèbres nuit<!--La lumière et les ténèbres, ainsi que leurs champs lexicaux respectifs, personnifient souvent Yéhoshoua (Jésus) et Satan. Ainsi, Yéhoshoua est la Lumière du monde (Jn. 9:5), l'Étoile brillante du matin (Ap. 22:16), le Soleil levant ou le Soleil de la justice (Mal. 4:2 ; Ps. 19:6-7 ; Lu. 1:78). Il est associé au jour (Jn. 9:4) d'où les expressions « Jour du Seigneur » (1 Th. 5:2) ou « Jour de YHWH » (Joë. 1:15). À l'inverse, la Bible associe Satan aux ténèbres (Es. 8:23 ; Ps. 143:3 ; Ep. 6:12 ; Col. 1:13) et à la nuit (Jn. 9:4 ; Ro. 13:12).-->. Il y eut un soir et il y eut un matin<!--Contrairement au calendrier grégorien où le jour commence à minuit, selon Elohîm et le calendrier hébraïque, le jour commence le soir à 18 heures pour se terminer le lendemain à la même heure. Voir commentaire en Mc. 16:9.--> : un jour<!--L'hébreu utilise le terme « ehad » qui signifie « un », au sens de l'indivisible. Elohîm se révèle aux Hébreux comme étant un. De. 6:4: « Shema Yisrael YHWH elohénou YHWH ehad » (« Écoute Israël, YHWH [est] notre Elohîm, YHWH [est] un »). YHWH est un et indivisible.-->.

### Second jour : Un firmament entre les eaux

1:6	Elohîm dit : Que le firmament apparaisse entre les eaux et qu'il sépare les eaux d'avec les eaux !
1:7	Elohîm donc fit le firmament, et il sépara les eaux qui sont au-dessous du firmament d'avec celles qui sont au-dessus du firmament. Il en fut ainsi.
1:8	Et Elohîm appela le firmament cieux. Il y eut un soir et il y eut un matin : second jour.

### Troisième jour : Les mers, la terre et la végétation

1:9	Elohîm dit : Que les eaux qui sont au-dessous des cieux soient rassemblées en un lieu et que le sec apparaisse ! Il en fut ainsi.
1:10	Et Elohîm appela le sec terre, et il appela la masse des eaux mers. Et Elohîm vit que cela était bon.
1:11	Elohîm dit : Que la terre produise de la verdure, de l'herbe portant de la semence, et des arbres fruitiers portant du fruit selon leur espèce, qui aient leur semence en eux-mêmes sur la terre ! Il en fut ainsi.
1:12	La terre donc produisit de la verdure, de l'herbe portant de la semence selon son espèce, et des arbres portant du fruit qui avaient leur semence en eux-mêmes selon leur espèce. Elohîm vit que cela était bon.
1:13	Il y eut un soir et il y eut un matin : troisième jour.

### Quatrième jour : Les luminaires du ciel

1:14	Elohîm dit : Que des luminaires apparaissent dans le firmament du ciel pour séparer la nuit d'avec le jour, qu'ils deviennent des signes pour les temps fixés<!--Voir les fêtes de YHWH en annexe. Ex. 23:15.-->, pour les jours et pour les années,
1:15	et qu'ils deviennent des luminaires dans le firmament du ciel afin d'éclairer la Terre ! Il en fut ainsi.
1:16	Elohîm donc fit les deux grands luminaires, le grand luminaire pour dominer sur le jour et le petit luminaire pour dominer sur la nuit, ainsi que les étoiles.
1:17	Elohîm les plaça dans le firmament du ciel pour éclairer la Terre,
1:18	pour dominer sur le jour et sur la nuit, et pour séparer la lumière d'avec les ténèbres. Elohîm vit que cela était bon.
1:19	Il y eut un soir et il y eut un matin : quatrième jour.

### Cinquième jour : Les animaux marins et volants<!--Ge. 2:19.-->

1:20	Elohîm dit : Que les eaux grouillent de choses grouillantes, d'âmes vivantes et que des oiseaux volent au-dessus de la terre face au firmament du ciel !
1:21	Elohîm créa les grands monstres marins et toutes les âmes vivantes qui se meuvent, dont les eaux se mirent à grouiller, selon leur espèce. Il créa aussi tout oiseau ayant des ailes selon son espèce. Elohîm vit que cela était bon.
1:22	Elohîm les bénit, en disant : Portez du fruit, multipliez-vous, remplissez les eaux des mers et que les oiseaux se multiplient sur la Terre !
1:23	Il y eut un soir et il y eut un matin : cinquième jour.

### Sixième jour : Les animaux terrestres

1:24	Elohîm dit : Que la terre produise des âmes vivantes selon leurs espèces, le bétail, les reptiles et les bêtes de la terre selon leurs espèces ! Il en fut ainsi.
1:25	Elohîm donc fit les animaux de la Terre selon leur espèce, et le bétail selon son espèce, et les reptiles de la terre selon leur espèce. Elohîm vit que cela était bon.

### Mission confiée à l'être humain (Adam) ; son autorité sur la création

1:26	Elohîm dit : Faisons l'être humain<!--Il existe plusieurs termes hébreux qui ont été traduits en français par « homme », mot qui peut avoir plusieurs sens. Dans cette traduction, nous avons fait le choix de mettre en évidence ces subtilités pour la bonne compréhension du lecteur. Le terme hébreu [’âdâm] signifie littéralement « être humain », « de la terre ». Ce mot, qui est le plus fréquemment employé dans le Tanakh, désigne le genre humain dans sa globalité. Son équivalent grec est [ánthrōpos] (voir commentaire dans Mt. 4:4). On le retrouve notamment dans Ge. 1:26-27, 2:7,15-25, 3:8-9,12,20-24, 4:1. Lire également les autres termes [’Âdâm] rendus par « Adam » (voir commentaire dans Ge. 3:17) et [’îysh] en rapport avec le mâle ou le mari (voir commentaire dans Ge. 2:23).--> à notre image, selon notre ressemblance<!--L'image d'Elohîm n'est autre que Yéhoshoua ha Mashiah lui-même (Col. 1:15). Le dernier Adam (1 Co. 15:40-49) qui est venu comme Fils, afin de nous montrer le modèle de fils et de filles qu'Elohîm souhaite (Ro. 8:29). Nous avons ici une autre image de l'incarnation d'Elohîm en la personne de Yéhoshoua ha Mashiah. Ainsi, avant que l'homme ne pèche, le projet de la rédemption était déjà là (1 Pi. 1:19-21).-->, et qu'il domine sur les poissons de la mer, sur les oiseaux du ciel, sur le bétail, sur toute la terre et sur tout reptile qui rampe sur la terre.
1:27	Elohîm créa l'être humain à son image, il le créa à l'image d'Elohîm, il les créa mâle et femelle.
1:28	Elohîm les bénit, et Elohîm leur dit : Portez du fruit, multipliez-vous, remplissez la Terre et assujettissez-la. Dominez sur les poissons de la mer, sur les oiseaux du ciel et sur toute bête qui rampe sur la terre.
1:29	Et Elohîm dit : Voici, je vous donne toute herbe portant de la semence qui est sur toute la surface de la terre, et tout arbre ayant en lui du fruit d'arbre et portant de la semence, ce sera votre nourriture.
1:30	Et à tout animal de la terre, à tout oiseau du ciel, et à tout ce qui rampe sur la terre, ayant en soi une âme vivante, je donne toute herbe verte pour nourriture. Il en fut ainsi.
1:31	Elohîm vit tout ce qu'il avait fait, et voici cela était très bon. Il y eut un soir et il y eut un matin : sixième jour.

## Chapitre 2

### Septième jour : Le repos (shabbat)

2:1	Les cieux donc et la Terre furent achevés avec toute leur armée.
2:2	Et Elohîm acheva au septième jour son œuvre qu'il avait faite, et il se reposa au septième jour de toute son œuvre qu'il avait faite.
2:3	Et Elohîm bénit le septième jour et le sanctifia, parce qu'en ce jour-là il s'était reposé de toute son œuvre qu'il avait créée en la faisant.
2:4	Telles sont les généalogies des cieux et de la Terre, lorsqu'ils furent créés, au temps où YHWH Elohîm fit la Terre et les cieux.
2:5	Il n'y avait encore sur la terre aucune plante des champs, et aucune herbe des champs n'avait encore germé, car YHWH Elohîm n'avait pas fait pleuvoir sur la terre et il n'y avait pas d'être humain pour cultiver<!--Ou « servir ».--> le sol.
2:6	Mais une vapeur montait de la terre et arrosait toute la surface du sol.

### YHWH forme l'être humain (Adam) et le place en Éden<!--Ps. 119:73 ; Job 10:8-9.-->

2:7	Or YHWH Elohîm forma l'être humain de la poussière de la terre<!--Vient de l'hébreu « adamah » qui signifie aussi « sol ».-->. Il souffla dans ses narines le souffle de vie et l'être humain devint une âme vivante.
2:8	Et YHWH Elohîm planta un jardin en Éden, du côté de l'orient, et il y mit l'être humain qu'il avait formé.

### Description du jardin en Éden

2:9	YHWH Elohîm fit germer de la terre tout arbre désirable à la vue et bon pour la nourriture, et l'arbre de la vie au milieu du jardin, et l'arbre de la connaissance de ce qui est bon ou mauvais.
2:10	Et un fleuve sortait d'Éden pour arroser le jardin, et de là il se divisait en quatre bras.
2:11	Le nom du premier est Pishon : c'est le fleuve qui coule en entourant toute la terre de Haviylah où se trouve l'or.
2:12	L'or de cette terre est bon. C'est là aussi que se trouvent le bdellium et la pierre d'onyx.
2:13	Le nom du second fleuve est Guihon. C'est celui qui coule en entourant toute la terre de Koush<!--Le mot hébreu « kuwsh » traduit en français par « Koush » signifie Éthiopie (« noir, brûlé par le soleil »). Il est prophétiquement question de l'Afrique.-->.
2:14	Le nom du troisième fleuve est Hiddékel, c'est celui qui coule à l'est de l'Assyrie. Le quatrième fleuve est l'Euphrate.

### Commandement donné par YHWH à l'être humain (Adam)<!--Ge. 1:28.-->

2:15	YHWH Elohîm prit l'être humain et le fit se reposer<!--« Se reposer » est la traduction de l'hébreu « yanach ». La vie chrétienne commence par le repos. Voir Ep. 2:6.--> dans le jardin d'Éden pour le cultiver<!--Ou « servir ».--> et pour le garder.
2:16	YHWH Elohîm donna cet ordre à l'être humain en disant : Tu mangeras, tu mangeras<!--Répétition du mot « mangeras » du mot hébreu « akal ». À l'origine, dans les écrits hébraïques, la répétition de mots était utilisée pour accentuer quelque chose dans le texte. Elle pouvait appuyer un fait précis, traduire une urgence, un danger ou indiquer de l'émotion et la renforcer.--> de tout arbre du jardin.
2:17	Mais quant à l'arbre de la connaissance de ce qui est bon ou mauvais, tu n'en mangeras pas, car le jour où tu en mangeras tu mourras, tu mourras<!--Le terme signifie littéralement « mort », « muwth » en hébreu. Il est utilisé deux fois de suite dans ce passage. Le Seigneur met donc en garde contre ce qui arrive à tout homme pécheur : la mort spirituelle puis la mort physique et enfin « la seconde mort » qui est la damnation éternelle dans le lac de feu (Ap. 2:11, 20:12-15).-->.

### YHWH forme une femme pour l'être humain (Adam)<!--Ge. 1:27.-->

2:18	Et YHWH Elohîm dit : Il n'est pas bon que l'être humain soit seul. Je lui ferai une aide qui soit son vis-à-vis<!--Vient de l'hébreu « neged » qui signifie « ce qui est visible », « ce qui est en face de », « en face de ».-->.
2:19	YHWH Elohîm forma de la terre tous les animaux des champs et tous les oiseaux du ciel, puis il les fit venir vers l'être humain pour voir comment il les appellerait, afin que toute âme vivante porte le nom dont l'être humain l'appellerait.
2:20	L'être humain appela de leurs noms tout le bétail, et les oiseaux du ciel et tous les animaux des champs, mais pour l'être humain, il ne trouva pas d'aide qui fût son vis-à-vis.
2:21	YHWH Elohîm fit tomber un profond sommeil sur l'être humain, qui s'endormit. Il prit l'un de ses côtés<!--Voir la note en Ge. 2:22.-->, et referma la chair à la place de ce côté<!--Voir Ge. 2:22.-->.
2:22	YHWH Elohîm bâtit une femme du côté<!--Le mot hébreu « tsela » généralement traduit en français par « côte » en Ge. 2:21-22 signifie d'abord « côté ». Partout ailleurs, ce mot a été traduit non par « côte » mais par « côté » : voir Ex. 25:12,14, 26:20,26-27,35, 27:7, 30:4, 36:25,31-32, 37:3,5,27, 38:7 et Job 18:12. Ce mot a été traduit par « flanc » en 2 S. 16:13. Voir Ge. 1:27.--> qu'il avait pris de l'être humain, et il l'amena vers l'être humain<!--1 Co. 11:8.-->.

### Union de l'être humain (Adam) et sa femme

2:23	Alors l'être humain dit : Celle-ci pour le coup est l'os de mes os et la chair de ma chair<!--Voir Ge. 29:14 ; 2 S. 5:1, 19:12-13 ; 1 Ch. 11:1.-->. On l'appellera femme parce qu'elle a été prise de l'homme<!--Ce terme est [’îysh] en hébreu et se rapporte au genre masculin. Il désigne le mâle ou le mari. Il est notamment utilisé dans Ge. 2:23-24, 3:6,16, 4:1,23. Lire également les autres termes [’âdâm] rendus par « être humain » (voir commentaire dans Ge. 1:26) et [’Âdâm] le nom propre du premier être humain (voir commentaire dans Ge. 3:17).-->.
2:24	C'est pourquoi l'homme<!--« Ish ».--> quittera son père et sa mère et se joindra à sa femme<!--« Ishshah ».-->, et ils deviendront une seule chair<!--Mt. 19:5 ; Mc. 10:7 ; 1 Co. 6:16 ; Ep. 5:30-31.-->.
2:25	L'être humain et sa femme étaient tous les deux nus, et ils n'en avaient pas honte.

## Chapitre 3

### Séduction du serpent et chute de l'être humain (Adam) et sa femme

3:1	Or le serpent<!--Satan ou le serpent ancien (Ap. 12:9, 20:2).--> était le plus rusé de tous les animaux des champs que YHWH Elohîm avait faits. Il dit à la femme : Sûrement Elohîm a dit : Vous ne mangerez pas de tous les arbres du jardin !
3:2	Et la femme répondit au serpent : Nous mangeons du fruit des arbres du jardin.
3:3	Mais quant au fruit de l'arbre qui est au milieu du jardin, Elohîm a dit : Vous n'en mangerez pas, et vous ne le toucherez pas, de peur que vous ne mouriez !
3:4	Alors le serpent dit à la femme : Vous ne mourrez pas ! Vous ne mourrez pas !
3:5	Mais Elohîm sait que le jour où vous en mangerez, vos yeux seront ouverts, et vous serez comme Elohîm, connaissant ce qui est bon ou mauvais.
3:6	La femme vit que l'arbre était bon pour la nourriture, qu'il était appétissant pour les yeux et que l'arbre était désirable pour donner de l'intelligence. Elle prit de son fruit et en mangea. Elle en donna aussi à son mari qui était auprès d'elle et il en mangea.

### La connaissance de ce qui est bon ou mauvais

3:7	Les yeux de tous les deux s'ouvrirent et ils surent qu'ils étaient nus. Ils cousirent ensemble des feuilles de figuier pour se faire des ceintures.
3:8	Alors ils entendirent au vent du jour la voix de YHWH Elohîm qui se promenait dans le jardin. Et l'être humain et sa femme se cachèrent loin de la face de YHWH Elohîm, au milieu des arbres du jardin.
3:9	Mais YHWH Elohîm appela Adam et lui dit : Où es-tu ?
3:10	Il répondit : J'ai entendu ta voix dans le jardin, et j'ai eu peur parce que je suis nu, et je me suis caché.
3:11	Et Elohîm dit : Qui t'a appris que tu es nu ? Est-ce que tu as mangé de l'arbre dont je t'avais défendu de manger ?
3:12	Adam répondit : La femme que tu m'as donnée pour être avec moi m'a donné de l'arbre, et j'en ai mangé.
3:13	Et YHWH Elohîm dit à la femme : Pourquoi as-tu fait cela ? Et la femme répondit : Le serpent m'a trompée, et j'en ai mangé.

### La création soumise à la vanité<!--Ro. 8:20-22.-->

3:14	Alors YHWH Elohîm dit au serpent : Parce que tu as fait cela, tu es maudit entre tout le bétail et entre tous les animaux des champs. Tu marcheras sur ton ventre et tu mangeras de la poussière<!--La poussière dont il est question n'est autre que l'homme pécheur (Ge. 3:19). Satan ne peut rien contre les véritables enfants d'Elohîm (Mt. 16:18 ; Lu. 10:19).--> tous les jours de ta vie.
3:15	Je mettrai inimitié entre toi et la femme<!--La femme représente en premier lieu Chavvah (Ève), la mère de tous les hommes. Ici, elle représente aussi Israël, l'épouse de YHWH selon Ge. 37:5-11 et Ap. 12:1.-->, entre ta postérité<!--La postérité du serpent regroupe l'homme impie (2 Th. 2:3-4 ; 1 Jn. 2:18-22) et tous ceux qui n'ont pas reçu Yéhoshoua ha Mashiah (Jésus-Christ) comme Seigneur et Sauveur. En effet, seuls ceux qui ont reçu Yéhoshoua dans leur vie sont appelés enfants d'Elohîm (Jn. 1:12 ; 1 Jn. 3:8-10, 5:19).--> et sa postérité<!--La postérité de la femme regroupe Yéhoshoua ha Mashiah homme (Es. 7:14 ; Lu. 2:4-7) et l'Assemblée (Église), le corps du Mashiah (Col. 1:24).--> : celle-ci t'écrasera la tête et tu lui écraseras le talon.
3:16	Et il dit à la femme : J'augmenterai, j'augmenterai la douleur de tes grossesses. Tu enfanteras dans la douleur tes enfants. Tes désirs se porteront vers ton mari, mais lui, il dominera sur toi.
3:17	Puis il dit à Adam<!--Lorsque le mot adam prend une majuscule [’Âdâm], il désigne le nom propre du premier être humain. On trouve la première occurrence dans Ge. 3:17, puis dans Ge. 4:25, 5:1,3,4,5 ; mais aussi dans Jos. 3:16 ; 1 Ch. 1:1 ; Job 31:33 ; Os. 6:7. Lire également les autres termes [’âdâm] rendus par « être humain » (voir commentaire dans Ge. 1:26) et [’îysh] en rapport avec le mâle ou le mari (voir commentaire dans Ge. 2:23).--> : Parce que tu as écouté la voix de ta femme, et que tu as mangé le fruit de l'arbre au sujet duquel je t'avais donné cet ordre en disant : Tu n'en mangeras pas ! La terre est maudite à cause de toi, tu en mangeras les fruits dans la douleur tous les jours de ta vie.
3:18	Elle fera germer pour toi des épines et des chardons et tu mangeras l'herbe des champs.
3:19	Tu mangeras le pain à la sueur de ton visage jusqu'à ce que tu retournes à la terre d'où tu as été pris, car tu es poussière et tu retourneras à la poussière.
3:20	Adam appela sa femme du nom de Chavvah<!--« Vie », « vivant ». Généralement traduit par Ève.-->, car elle est devenue la mère de tous les vivants.

### L'homme et la femme revêtus de tuniques de peaux

3:21	YHWH Elohîm habilla Adam et sa femme avec les tuniques de peaux qu'il leur avait faites.

### Adam et Chavvah (Ève) chassés du jardin

3:22	YHWH Elohîm dit : Voici l'être humain devenu comme l'un de nous, pour connaître ce qui est bon ou mauvais. Mais maintenant il faut prendre garde qu'il n'avance sa main et qu'il ne prenne aussi de l'arbre de vie, et qu'il n'en mange et ne vive éternellement.
3:23	Et YHWH Elohîm le chassa du jardin d'Éden pour qu'il cultive la terre d'où il avait été pris.
3:24	C'est ainsi qu'il chassa l'être humain, et il mit à l'orient du jardin d'Éden des chérubins et la flamme<!--Voir Ex. 7:11.--> de l'épée qui tournait çà et là pour garder le chemin de l'arbre de vie.

## Chapitre 4

### La jalousie de Caïn envers son frère Abel

4:1	Adam connut Chavvah<!--« Vie », « vivant ». Généralement traduit par Ève.--> sa femme. Elle conçut et enfanta Caïn. Elle dit : J'ai acquis un homme avec YHWH.
4:2	Elle enfanta encore Abel, son frère. Abel devint berger de petit bétail et Caïn devint cultivateur du sol<!--Ou encore : « travailleur du sol ».-->.
4:3	Et il arriva au bout de quelque temps que Caïn offrît à YHWH une offrande des fruits de la terre.
4:4	Abel, de son côté, offrit des premiers-nés de son troupeau et de leur graisse<!--Abel était juste et pieux, aussi il sut instinctivement apporter une offrande agréable à Elohîm (Mt. 23:35 ; Lu. 11:51 ; Hé. 11:4). En l'occurrence, son offrande préfigurait le sacrifice du Seigneur.-->. YHWH porta son regard sur Abel et sur son offrande,
4:5	mais il ne porta pas son regard sur Caïn ni sur son offrande. Et Caïn fut très fâché, et son visage fut abattu.

### YHWH avertit Caïn

4:6	YHWH dit à Caïn : Pourquoi es-tu fâché, et pourquoi ton visage est-il abattu ?
4:7	Si tu agis bien, tu relèveras ton visage. Si tu agis mal, le péché est couché à la porte et son désir se porte vers toi, mais toi, domine sur lui.

### Caïn tue son frère Abel<!--Ge. 4:23.-->

4:8	Caïn parla à Abel, son frère. Il arriva que comme ils étaient dans les champs, Caïn s'éleva contre Abel, son frère et le tua.
4:9	YHWH dit à Caïn : Où est Abel ton frère ? Et il lui répondit : Je ne sais pas. Suis-je le gardien de mon frère, moi ?
4:10	Et il dit : Qu'as-tu fait ? La voix du sang de ton frère crie de la terre à moi.
4:11	Maintenant donc tu seras maudit de la terre qui a ouvert sa bouche pour recevoir de ta main le sang de ton frère.
4:12	Quand tu cultiveras la terre, elle ne te donnera plus son fruit, et tu seras vagabond et fugitif sur la Terre.
4:13	Et Caïn dit à YHWH : Le châtiment de mon iniquité est trop grand pour que je puisse le porter.
4:14	Voici que tu me chasses aujourd'hui de la face du sol. Je serai caché loin de ta face, je serai vagabond et fugitif sur la Terre, et il arrivera que quiconque me trouvera me tuera.
4:15	YHWH lui dit : Alors, si quelqu'un tue Caïn, on le vengera sept fois. Ainsi YHWH mit une marque sur Caïn afin que quiconque le trouverait ne le tue pas.

### Caïn bâtit une cité loin de YHWH

4:16	Alors Caïn s'éloigna de la présence de YHWH et habita dans la terre de Nod, à l'orient d'Éden.
4:17	Puis Caïn connut sa femme. Elle conçut et enfanta Hénoc. Il bâtit une ville, et appela le nom de la ville d'après le nom de son fils Hénoc.
4:18	Puis Hénoc engendra Irad, Irad engendra Mehouyaël, Mehouyaël engendra Metoushaël, et Metoushaël engendra Lémek.
4:19	Lémek prit deux femmes. Le nom de l'une était Adah, et le nom de l'autre Tsillah.
4:20	Et Adah enfanta Yabal. Il fut le père de ceux qui habitent dans les tentes et près des troupeaux.
4:21	Et le nom de son frère était Youbal. Il fut le père de tous ceux qui manient la harpe et la flûte.
4:22	Et Tsillah aussi enfanta Toubal-Caïn. Il forgeait toutes sortes d'instruments en cuivre et en fer. Et la sœur de Toubal-Caïn était Na`amah.
4:23	Lémek dit à Adah et à Tsillah ses femmes : Écoutez ma voix femmes de Lémek, prêtez l'oreille à ma parole ! En effet, j'ai tué un homme pour ma blessure et un enfant pour ma meurtrissure.
4:24	Caïn sera en effet vengé 7 fois, et Lémek, 77 fois !

### Naissance de Sheth

4:25	Adam connut encore sa femme. Elle enfanta un fils et il l'appela du nom de Sheth, car dit-elle, Elohîm a mis une autre postérité pour moi à la place d'Abel que Caïn a tué.
4:26	Il naquit aussi un fils à Sheth, et il l'appela du nom d'Enowsh. C'est alors que l'on commença à faire appel<!--« Réciter », « lire », « s'écrier », « proclamer ».--> au Nom de YHWH.

## Chapitre 5

### La postérité d'Adam soumise à la mort<!--Ro. 5:12.-->

5:1	Voici le livre de la généalogie d'Adam. Depuis le jour où Elohîm créa l'être humain, il le fit à la ressemblance d'Elohîm.
5:2	Il les créa mâle et femelle, il les bénit et les appela du nom d'êtres humains<!--Littéralement « Adam ».-->, le jour où ils furent créés.
5:3	Et Adam vécut 130 ans, et engendra un fils à sa ressemblance, selon son image<!--Désormais, les hommes naissent à la ressemblance d'Adam, c'est-à-dire pécheurs (Ro. 3:23, 5:14-17).-->, et il lui donna le nom de Sheth.
5:4	Et les jours d'Adam, après qu'il eut engendré Sheth, furent de 800 ans, et il engendra des fils et des filles.
5:5	Tous les jours qu'Adam vécut furent de 930 ans, puis il mourut.

### De Sheth aux fils de Noah<!--Ro. 5:12.-->

5:6	Sheth vécut 105 ans et engendra Enowsh.
5:7	Sheth, après qu'il eut engendré Enowsh, vécut 807 ans et il engendra des fils et des filles.
5:8	Tous les jours que Sheth vécut furent de 912 ans, puis il mourut.
5:9	Enowsh, ayant vécu 90 ans, engendra Kénan.
5:10	Et Enowsh, après qu'il eut engendré Kénan, vécut 815 ans et il engendra des fils et des filles.
5:11	Tous les jours qu'Enowsh vécut furent de 905 ans, puis il mourut.
5:12	Kénan, ayant vécu 70 ans, engendra Mahalal'el.
5:13	Kénan, après qu'il eut engendré Mahalal'el, vécut 840 ans et il engendra des fils et des filles.
5:14	Tous les jours que Kénan vécut furent de 910 ans, puis il mourut.
5:15	Mahalal'el vécut 65 ans et il engendra Yered.
5:16	Mahalal'el, après qu'il eut engendré Yered, vécut 830 ans et il engendra des fils et des filles.
5:17	Tous les jours donc que Mahalal'el vécut furent de 895 ans, puis il mourut.
5:18	Yered, ayant vécu 162 ans, engendra Hénoc.
5:19	Yered, après avoir engendré Hénoc, vécut 800 ans et il engendra des fils et des filles.
5:20	Tous les jours que Yered vécut furent de 962 ans, puis il mourut.
5:21	Hénoc vécut 65 ans et engendra Metoushèlah.
5:22	Hénoc, après qu'il eut engendré Metoushèlah, marcha avec Elohîm 300 ans et il engendra des fils et des filles.
5:23	Tous les jours que Hénoc vécut furent de 365 ans.
5:24	Hénoc marcha avec Elohîm, mais il ne parut plus parce qu'Elohîm le prit.
5:25	Metoushèlah, ayant vécu 187 ans, engendra Lémek.
5:26	Metoushèlah, après qu'il eut engendré Lémek, vécut 782 ans et il engendra des fils et des filles.
5:27	Tous les jours que Metoushèlah vécut furent de 969 ans, puis il mourut.
5:28	Lémek aussi vécut 182 ans, et il engendra un fils.
5:29	Il l'appela Noah en disant : Celui-ci nous consolera de notre œuvre et du travail pénible de nos mains, sur la terre que YHWH a maudite.
5:30	Lémek, après qu'il eut engendré Noah, vécut 595 ans et il engendra des fils et des filles.
5:31	Tous les jours que Lémek vécut furent de 777 ans, puis il mourut.
5:32	Noah était âgé de 500 ans, Noah engendra Shem, Cham et Yepheth.

## Chapitre 6

### Le monde avant le déluge<!--Lu. 17:27.-->

6:1	Or il arriva quand les êtres humains commencèrent à se multiplier sur la face de la terre et qu'ils eurent engendré des filles,
6:2	que les fils d'Elohîm<!--Ici, les fils d'Elohîm sont des anges qui ont quitté leur demeure (Jud. 1:5-7).--> voyant que les filles des êtres humains<!--Le mot « être humain » vient de l'hébreu « adam ». Donc ces filles étaient les descendantes du premier homme, c'est-à-dire d'Adam. Il y a un contraste entre les fils d'Elohîm qui n'étaient pas les descendants d'Adam et les filles des hommes ou d'Adam.--> étaient belles, ils en prirent pour femmes parmi toutes celles qu'ils choisirent.

### YHWH ne conteste plus avec l'être humain

6:3	Et YHWH dit : Mon Esprit ne contestera pas toujours avec l'être humain<!--C'est le Saint-Esprit qui nous convainc de péché, de justice et de jugement (Jn. 16:8). Lorsqu'il constate que le cœur d'une personne est définitivement endurci au point de refuser la repentance, il renonce à la convaincre de péché et il se retire. La génération antédiluvienne avait définitivement rejeté Elohîm en choisissant de faire le mal (Ge. 6:5). Elle était allée si loin dans l'abomination qu'elle s'accoupla avec des anges déchus (Ge. 6:4), ce qui laisse supposer un culte volontaire aux démons. Lorsque le Saint-Esprit est retiré d'une personne, il est remplacé par l'esprit d'égarement qui enferme le pécheur dans l'erreur et l'entraîne ainsi à sa condamnation éternelle (Mt. 12:31 ; 2 Th. 2:11).--> dans son égarement, car il n'est que chair et ses jours seront de 120 ans.
6:4	Les géants étaient sur la Terre en ces temps-là, et aussi après que les fils d'Elohîm furent venus vers les filles des êtres humains et qu'elles leur eurent donné des enfants. Ce sont ces puissants hommes qui, dès les temps anciens furent des gens de renom.

### YHWH prépare un jugement

6:5	Et YHWH vit que la méchanceté de l'être humain était très grande sur la Terre et que toute la structure des pensées de son cœur n'était que mal en tout temps.
6:6	YHWH se repentit d'avoir fait l'être humain sur la Terre, et il fut affligé en son cœur.
6:7	Et YHWH dit : J'exterminerai de la face de la Terre l'être humain que j'ai créé, depuis l'être humain jusqu'au bétail, jusqu'aux reptiles, et même jusqu'aux oiseaux du ciel, car je me repens de les avoir faits.

### La grâce de YHWH sur Noah : Construction de l'arche

6:8	Mais Noah trouva grâce aux yeux de YHWH.
6:9	Voici la postérité de Noah. Noah était un homme juste et intègre en son temps. Noah marchait avec Elohîm.
6:10	Et Noah engendra trois fils : Shem, Cham et Yepheth.
6:11	Et la Terre était corrompue devant Elohîm, la Terre était remplie de violence.
6:12	Elohîm donc regarda la Terre, et voici elle était corrompue, car toute chair avait corrompu sa voie sur la Terre.
6:13	Et Elohîm dit à Noah : La fin de toute chair est venue devant moi, car ils ont rempli la Terre de violence, et voici, je les détruirai avec la Terre.
6:14	Fais-toi une arche<!--Type du Mashiah, l'arche est l'image du refuge et du salut que l'on peut trouver en Yéhoshoua ha Mashiah (Jésus-Christ) (Col. 1:12-13, 3:3). Les différents types d'animaux purs et impurs qui sont entrés dans l'arche préfigurent l'Assemblée (Église) bâtie par le Seigneur Yéhoshoua (Mt. 16:18), composée de Juifs convertis (animaux purs) qui furent les premiers à recevoir l'adoption, les alliances, le culte, et les promesses d'Elohîm (Ro. 9:4), et d'hommes et de femmes issus des nations païennes (animaux impurs ; Ac. 10). Notons que le mot « poix » vient de l'hébreu « kaphar » qui signifie « propitiation », mais aussi « réconciliation », « pardon », « miséricordieux », « apaiser ».--> de bois de gopher ; tu feras cette arche en cellules, et tu l'enduiras de poix à l'intérieur et à l'extérieur.
6:15	Et tu la feras de cette manière : la longueur de l'arche sera de 300 coudées, sa largeur de 50 coudées et sa hauteur de 30 coudées.
6:16	Tu feras une fenêtre à l'arche et tu l'achèveras une coudée plus haut. Tu mettras la porte de l'arche sur le côté, et tu feras un étage inférieur, un deuxième et un troisième.
6:17	Et voici, je ferai venir un déluge d'eau sur la Terre pour détruire toute chair dans laquelle il y a souffle de vie sous les cieux. Tout ce qui est sur la Terre expirera.
6:18	Mais j'établirai mon alliance avec toi, et tu entreras dans l'arche toi et tes fils, et ta femme, et les femmes de tes fils avec toi.
6:19	Et de toute âme vivante, de tout ce qui est chair, tu en feras entrer deux de chaque espèce dans l'arche pour les conserver en vie avec toi : un mâle et une femelle.
6:20	Des oiseaux, selon leur espèce, des bêtes à quatre pattes, selon leur espèce, et de tous les reptiles, selon leur espèce. Ils y entreront tous par paires avec toi, afin que tu les conserves en vie.
6:21	Et toi, prends de toute nourriture qui se mange, et tu la recueilleras près de toi. Cela te servira de nourriture, ainsi qu'à eux.
6:22	Et Noah fit selon tout ce qu'Elohîm lui avait ordonné. Ainsi fit-il.

## Chapitre 7
 
### Le jugement par le déluge

7:1	Et YHWH dit à Noah : Entre, toi et toute ta maison dans l'arche, car je t'ai vu juste devant moi en ce temps-ci.
7:2	Tu prendras de toutes les bêtes pures, 7 par 7, le mâle et sa femelle, mais des bêtes qui ne sont pas pures, 2, le mâle et sa femelle.
7:3	Et aussi des oiseaux du ciel, 7 par 7, le mâle et sa femelle afin de conserver leur semence en vie sur toute la surface de la Terre.
7:4	Car dans 7 jours, je ferai pleuvoir sur la Terre pendant 40 jours et 40 nuits. J'exterminerai de la surface de la Terre toute existence que j’ai faite.
7:5	Et Noah fit selon tout ce que YHWH lui avait ordonné.
7:6	Or Noah était âgé de 600 ans quand le déluge des eaux vint sur la Terre.
7:7	Noah donc entra dans l'arche avec ses fils, sa femme, et les femmes de ses fils, pour échapper aux eaux du déluge.
7:8	Des bêtes pures, des bêtes qui ne sont pas pures, des oiseaux, et tout ce qui rampe sur la Terre,
7:9	elles entrèrent deux à deux vers Noah dans l'arche, le mâle et la femelle, comme Elohîm l'avait ordonné à Noah.
7:10	Et il arriva qu'au septième jour les eaux du déluge furent sur la Terre.
7:11	En l'an 600 de la vie de Noah, au second mois, le dix-septième jour du mois, en ce jour-là toutes les sources du grand abîme furent rompues, et les écluses des cieux furent ouvertes.
7:12	Et la pluie tomba sur la Terre pendant 40 jours et 40 nuits.
7:13	En ce même<!--Littéralement « dans l'os de ce jour ».--> jour, Noah, Shem, Cham et Yepheth, fils de Noah, entrèrent dans l'arche, avec la femme de Noah et les trois femmes de ses fils avec eux.
7:14	Eux et tous les animaux selon leur espèce, tout le bétail selon son espèce, tous les reptiles qui rampent sur la Terre selon leur espèce, tous les oiseaux selon leur espèce, tout petit oiseau et tout ce qui a des ailes.
7:15	Ils entrèrent donc vers Noah dans l’arche, deux par deux, de toute chair ayant en elle esprit de vie.
7:16	Et c’étaient un mâle et une femelle de toute chair qui entraient. Ils entrèrent comme Elohîm le lui avait ordonné. Ensuite YHWH ferma l'arche sur lui.
7:17	Et le déluge vint pendant 40 jours sur la Terre. Les eaux crûrent et soulevèrent l'arche, et elle s'éleva au-dessus de la terre.
7:18	Et les eaux grossirent et s'accrurent beaucoup sur la Terre, et l'arche flottait au-dessus des eaux.
7:19	Les eaux donc grossirent de plus en plus sur la Terre, et toutes les hautes montagnes qui sont sous le ciel entier en furent couvertes.
7:20	Les eaux s'élevèrent de 15 coudées au-dessus des montagnes qui furent couvertes.
7:21	Alors expira toute chair qui rampait sur la Terre, tant les oiseaux que le bétail et les animaux, toutes les choses grouillantes qui grouillaient sur la Terre, et tous les êtres humains.
7:22	Tout ce qui avait dans ses narines le souffle d'esprit de vie, tout ce qui était sur le sol sec, mourut.
7:23	Toute existence sur la Terre fut donc exterminée, depuis l'être humain jusqu'au bétail, jusqu'aux reptiles et jusqu'aux oiseaux du ciel : ils furent exterminés de la face de la Terre. Il ne resta seulement que Noah, et ce qui était avec lui dans l'arche.
7:24	Et les eaux grossirent sur la Terre pendant 150 jours.

## Chapitre 8

### Fin du déluge

8:1	Or Elohîm se souvint de Noah, de tous les animaux et de tout le bétail qui étaient avec lui dans l'arche. Elohîm fit passer un vent sur la Terre, et les eaux se calmèrent.
8:2	Car les sources de l'abîme et les écluses des cieux furent fermées et la pluie du ciel fut retenue.
8:3	Les eaux s'éloignèrent de dessus la Terre, s'en allant et s'éloignant. Au bout de 150 jours les eaux diminuèrent.
8:4	Et le dix-septième jour du septième mois, l'arche s'arrêta sur les montagnes d'Ararat.
8:5	Et les eaux allèrent en diminuant de plus en plus jusqu'au dixième mois. Le premier jour du dixième mois, les sommets des montagnes apparurent.
8:6	Et il arriva qu'au bout de 40 jours, Noah ouvrit la fenêtre qu'il avait faite à l'arche.
8:7	Et il lâcha le corbeau qui sortit, allant et revenant, jusqu'à ce que les eaux aient séché sur la Terre.
8:8	Il lâcha aussi une colombe pour voir si les eaux avaient diminué à la surface de la Terre.
8:9	Mais la colombe, ne trouvant pas de lieu de repos pour la plante de son pied, retourna à lui dans l'arche car les eaux étaient sur toute la Terre. Il étendit la main et la prit pour la faire rentrer dans l’arche.
8:10	Et il attendit anxieusement encore 7 autres jours et lâcha de nouveau la colombe hors de l'arche.
8:11	La colombe vint à lui au temps du soir, et voici, une feuille d'olivier fraîchement arrachée était dans son bec. Noah sut ainsi que les eaux avaient diminué sur la Terre.
8:12	Et il attendit encore 7 autres jours, puis il lâcha la colombe qui ne retourna plus à lui.
8:13	Et il arriva qu'en l'an 601, le premier jour du premier mois, les eaux avaient séché sur la terre. Noah ôta la couverture de l'arche, regarda, et voici, la surface de la terre avait séché.
8:14	Et au vingt-septième jour du second mois la terre était devenue sèche.

### Noah sort de l'arche<!--Ge. 8:11-15.-->

8:15	Alors Elohîm parla à Noah, en disant :
8:16	Sors de l'arche, toi et ta femme, tes fils et les femmes de tes fils avec toi.
8:17	Fais sortir avec toi tous les animaux qui sont avec toi, de toute chair, tant les oiseaux que le bétail, et tous les reptiles qui rampent sur la terre. Qu'ils grouillent sur la terre, qu'ils portent du fruit et se multiplient sur la terre.
8:18	Noah donc sortit, et avec lui ses fils, sa femme, et les femmes de ses fils.
8:19	Tous les animaux, tous les reptiles, tous les oiseaux, tout ce qui rampe sur la terre, selon leurs espèces, sortirent de l'arche.
8:20	Et Noah bâtit un autel à YHWH, il prit de toutes les bêtes pures, et de tout oiseau pur, et il offrit des holocaustes sur l'autel.
8:21	Et YHWH respira un parfum apaisant et YHWH dit en son cœur : Je ne maudirai plus la terre à cause de l'être humain, car la structure du cœur de l'être humain est mauvaise dès sa jeunesse, et je ne frapperai plus tout ce qui est vivant, comme je l'ai fait.
8:22	Tant que dureront les jours de la Terre, les semailles et les moissons, le froid et la chaleur, l'été et l'hiver, le jour et la nuit ne cesseront pas.

## Chapitre 9

### YHWH établit une alliance avec Noah

9:1	Et Elohîm bénit Noah et ses fils et leur dit : Portez du fruit, multipliez-vous et remplissez la Terre.
9:2	Et que toutes les bêtes de la terre, tous les oiseaux des cieux, avec tout ce qui rampe sur la terre, et tous les poissons de la mer vous craignent et vous redoutent : ils sont mis entre vos mains.
9:3	Tout ce qui se meut et qui a vie sera votre nourriture. Je vous donne tout cela comme l'herbe verte.
9:4	Toutefois, vous ne mangerez pas de chair avec son âme, c'est-à-dire son sang.
9:5	En effet, je redemanderai le sang de vos âmes, je le redemanderai de la main de tout animal. Je redemanderai l'âme de l'être humain de la main de l'être humain, de la main de l'homme qui est son frère.
9:6	Celui qui aura versé le sang de l'être humain, par l'être humain son sang sera versé, car Elohîm a fait l'être humain à son image.
9:7	Vous donc, portez du fruit et multipliez-vous, grouillez et multipliez-vous sur la terre.
9:8	Elohîm parla aussi à Noah et à ses fils qui étaient avec lui, en disant :
9:9	Et quant à moi, voici, j'établis mon alliance avec vous, et avec votre postérité après vous,
9:10	avec toutes les âmes vivantes qui sont avec vous, tant les oiseaux que le bétail, et tous les animaux de la terre qui sont avec vous, tous ceux qui sont sortis de l'arche jusqu'à tous les animaux de la terre.
9:11	J'établis donc mon alliance avec vous : toute chair ne sera plus retranchée par les eaux du déluge, et il n'y aura plus de déluge pour détruire la Terre.
9:12	Puis Elohîm dit : Voici le signe de l'alliance que j'établis entre moi et vous, et toutes les âmes vivantes qui sont avec vous, pour toutes les générations, pour toujours :
9:13	j'ai placé mon arc dans les nuages et il devient le signe de l'alliance entre moi et la Terre.
9:14	Et il arrivera que quand j'aurai rassemblé des nuages au-dessus de la terre, l'arc apparaîtra parmi les nuages,
9:15	et je me souviendrai de mon alliance entre moi et vous et toutes les âmes vivantes de toute chair, et les eaux ne deviendront plus un déluge pour détruire toute chair.
9:16	L'arc donc sera parmi les nuages et je le regarderai pour me souvenir de l'alliance éternelle entre Elohîm et toute âme vivante de toute chair qui est sur la Terre.
9:17	Elohîm donc dit à Noah : C'est là le signe de l'alliance que j'ai établie entre moi et toute chair qui est sur la Terre.
9:18	Et les fils de Noah qui sortirent de l'arche étaient Shem, Cham, et Yepheth. Cham fut le père de Canaan.
9:19	Ce sont là les trois fils de Noah, et c'est leur postérité qui peupla toute la Terre.

### Le péché de Cham

9:20	Et Noah, homme de la terre, commença à planter de la vigne.
9:21	Et il but du vin, s'enivra, et se découvrit au milieu de sa tente.
9:22	Et Cham, père de Canaan, vit la nudité de son père<!--Lé. 18:6-19, 20:11-21.-->, et il le rapporta dehors à ses deux frères.
9:23	Alors Shem et Yepheth prirent un manteau qu'ils mirent sur leurs deux épaules, et marchant à reculons, ils couvrirent la nudité de leur père. Leurs visages étaient tournés en arrière, de sorte qu'ils ne virent pas la nudité de leur père.
9:24	Et quand Noah se réveilla de son vin, il apprit ce que lui avait fait son fils cadet.

### Noah prononce une malédiction contre Canaan

9:25	C'est pourquoi, il dit : Maudit soit Canaan<!--Une idée erronée selon laquelle les noirs auraient été maudits par Elohîm au travers de la malédiction de Canaan s'est répandue pendant des siècles. Ainsi, certains hommes ont pris pour prétexte cette malédiction pour légitimer l'asservissement des populations africaines, de même que d'autres populations indigènes, comme les Amérindiens. Il faut préciser que les descendants de Cham furent Koush (Éthiopie), Mitsraïm (Égypte), Pouth (les Lybiens) et Canaan (Pays qu'Elohîm a donné aux descendants de Shem, selon Ge. 15). Cham est le fils cadet de Noah. Mais c'est à Canaan, le fils de Cham, donc petit-fils de Noah, que s'adresse la malédiction. Selon la Bible, les peuples africains sont des descendants de Cham, mais par son fils Koush et non par Canaan. La prétendue malédiction des noirs n'a donc aucun fondement.--> ! Il sera l'esclave des esclaves de ses frères.
9:26	Il dit aussi : Béni soit YHWH, l'Elohîm de Shem, et que Canaan soit leur esclave !
9:27	Qu'Elohîm mette Yepheth au large, qu'il habite dans les tentes de Shem et que Canaan soit leur esclave !
9:28	Et Noah vécut après le déluge 350 ans.
9:29	Et tous les jours de Noah furent de 950 ans, puis il mourut.

## Chapitre 10

### La postérité de Noah

10:1	Voici la postérité des fils de Noah : Shem, Cham et Yepheth. Il leur naquit des fils après le déluge.
10:2	Les fils de Yepheth furent : Gomer, Magog, Madaï, Yavan, Toubal, Méshec, et Tiras.
10:3	Et les fils de Gomer : Ashkenaz, Riphat, et Togarmah.
10:4	Les fils de Yavan : Éliyshah, Tarsis, Kittim, et Dodanim.
10:5	De ceux-là furent divisées les îles des nations selon leurs terres, chacun selon sa langue, selon leurs familles, entre leurs nations.
10:6	Et les fils de Cham furent : Koush, Mitsraïm, Pouth, et Canaan.
10:7	Et les fils de Koush : Saba, Haviylah, Sabta, Ra`mah, et Sabteca. Les fils de Ra`mah : Séba et Dedan.
10:8	Koush engendra aussi Nimrod<!--Nimrod ou Nemrod, dont le nom signifie « rebelle », fut le premier roi de l'histoire biblique. Fils de Koush (Éthiopie), lui-même premier-né de Cham, fils de Noah (Ge. 10:8-10), il fut à la tête du premier Empire après le déluge. Il se distingua en qualité de puissant chasseur « devant YHWH » ou « contre YHWH ». Le contexte du chapitre 10 nous démontre qu'il provoquait Elohîm. Fondateur de Ninive, il est surtout connu pour avoir été à l'origine du projet de la tour de Babel.-->, c'est lui qui commença à être puissant sur la Terre.
10:9	Il fut un puissant chasseur devant YHWH, c'est pourquoi l'on a dit : Comme Nimrod, le puissant chasseur devant YHWH.
10:10	Et le commencement de son règne fut Babel<!--Le nom Babel signifie la porte de El. Babylone : c'est la confusion par le mélange.-->, Érec, Accad, et Calné, au pays de Shinear.
10:11	De ce pays-là sortit Assour, et il bâtit Ninive, et la ville de Rehoboth, et Calach,
10:12	et Résen, entre Ninive et Calach, qui est une grande ville.
10:13	Mitsraïm engendra les Loudim, les Anamim, les Lehabim, les Naphtouhim,
10:14	les Patrousim, les Kaslouhim, d'où sont sortis les Philistins, et les Caphtorim.
10:15	Et Canaan engendra Sidon, son premier-né, et Heth,
10:16	et les Yebousiens, les Amoréens, les Guirgasiens,
10:17	les Héviens, les Arkiens, les Siniens,
10:18	les Arvadiens, les Tsemariens, les Hamathiens. Ensuite, les familles des Cananéens se sont dispersées.
10:19	Et les limites des Cananéens furent depuis Sidon, quand on vient vers Guérar, jusqu'à Gaza, en allant vers Sodome et Gomorrhe, Adma et Tseboïm, jusqu'à Lésha.
10:20	Ce sont là les fils de Cham selon leurs familles et leurs langues, selon leurs pays, et selon leurs nations.
10:21	Et il naquit aussi des fils à Shem, père de tous les fils d'Héber, et frère aîné de Yepheth.
10:22	Et les fils de Shem furent : Élam, Assour, Arpacshad, Loud et Aram.
10:23	Et les fils d'Aram : Outs, Houl, Guéter et Mash.
10:24	Arpacshad engendra Shélach et Shélach engendra Héber.
10:25	Et il naquit à Héber deux fils. Le nom de l'un était Péleg, parce que de son temps la Terre fut partagée, et le nom de son frère était Yoqtan. 
10:26	Et Yoqtan engendra Almodad, Shéleph, Hatsarmaveth, Yerach,
10:27	Hadoram, Ouzal, Diqlah,
10:28	Obal, Abimaël, Séba,
10:29	Ophir, Haviylah, et Yobab. Tous ceux-là sont les fils de Yoqtan.
10:30	Et leur demeure était depuis Mésha, du côté de Sephar, jusqu'à la montagne de l'orient.
10:31	Ce sont là les fils de Shem, selon leurs familles, selon leurs langues, selon leurs pays, et selon leurs nations.
10:32	Telles sont les familles des fils de Noah, selon leurs lignées, selon leurs nations. Et de ceux-là ont été divisées les nations sur la Terre après le déluge.

## Chapitre 11

### La tour de Babel ou Babylone

11:1	Alors toute la Terre avait un même langage et une même parole.
11:2	Mais il arriva qu'étant partis de l'orient, ils trouvèrent une vallée au pays de Shinear où ils habitèrent.
11:3	Ils se dirent chacun à son frère : Venez ! Faisons des briques, et cuisons-les très bien au feu ! Et la brique leur servit de pierre, et le bitume leur servit d'argile.
11:4	Ils dirent : Venez ! Bâtissons-nous une ville et une tour dont la tête soit jusqu'aux cieux. Faisons-nous un nom, de peur que nous ne soyons dispersés sur toute la Terre.

### YHWH confond le langage humain

11:5	Alors YHWH descendit pour voir la ville et la tour que les fils des humains bâtissaient.
11:6	Et YHWH dit : Voici, ce peuple est un, et ils ont tous le même langage, et ils commencent à travailler ! Maintenant, rien de ce qu'ils ont projeté d'accomplir ne leur sera inaccessible !
11:7	Allons ! Descendons et là confondons leur langage afin qu'ils ne comprennent plus le langage les uns des autres !
11:8	De là, YHWH les dispersa sur la face de toute la Terre et ils cessèrent de bâtir la ville.
11:9	C'est pourquoi on l'appela du nom de Babel<!--Porte de El. Babylone.-->, car c'est là que YHWH confondit le langage de toute la Terre, et c'est de là que YHWH les dispersa sur toute la Terre.

### La postérité de Shem, ancêtre d'Abram

11:10	Voici la postérité de Shem : Shem, âgé de 100 ans, engendra Arpacshad, 2 ans après le déluge.
11:11	Shem, après qu'il eut engendré Arpacshad, vécut 500 ans et engendra des fils et des filles.
11:12	Arpacshad vécut 35 ans et engendra Shélach.
11:13	Arpacshad, après qu'il eut engendré Shélach, vécut 403 ans et engendra des fils et des filles.
11:14	Shélach, ayant vécu 30 ans, engendra Héber.
11:15	Shélach, après qu'il eut engendré Héber, vécut 403 ans et engendra des fils et des filles.
11:16	Héber, ayant vécu 34 ans, engendra Péleg.
11:17	Héber, après qu'il eut engendré Péleg, vécut 430 ans et engendra des fils et des filles.
11:18	Péleg, ayant vécu 30 ans, engendra Réou.
11:19	Péleg, après qu'il eut engendré Réou, vécut 209 ans et engendra des fils et des filles.
11:20	Réou, ayant vécu 32 ans, engendra Seroug.
11:21	Réou, après qu'il eut engendré Seroug, vécut 207 ans et engendra des fils et des filles.
11:22	Seroug, ayant vécu 30 ans, engendra Nachor.
11:23	Seroug, après qu'il eut engendré Nachor, vécut 200 ans et engendra des fils et des filles.
11:24	Nachor, ayant vécu 29 ans, engendra Térach.
11:25	Nachor, après qu'il eut engendré Térach, vécut 119 ans et engendra des fils et des filles.
11:26	Térach, ayant vécu 70 ans, engendra Abram, Nachor et Haran.
11:27	Voici la postérité de Térach : Térach engendra Abram, Nachor et Haran. Haran engendra Lot.
11:28	Haran mourut en présence de Térach, son père, au pays de sa naissance, à Our en Chaldée.
11:29	Abram et Nachor prirent chacun une femme. Le nom de la femme d'Abram était Saraï et le nom de la femme de Nachor était Milkah, fille d'Haran, père de Milkah et père de Yiskah.
11:30	Or Saraï était stérile et n'avait pas d'enfants.

### Séjour à Charan

11:31	Térach prit Abram, son fils, et Lot, fils d'Haran, fils de son fils, et Saraï, sa belle-fille, femme d'Abram, son fils. Ils sortirent ensemble d'Our en Chaldée pour aller au pays de Canaan. Ils vinrent jusqu'à Charan et ils y habitèrent.
11:32	Et les jours de Térach furent de 205 ans, puis il mourut à Charan.

## Chapitre 12

### Appel d'Abram : La promesse de YHWH<!--Ge. 12:2, 13:14-18, 15:1-21, 17:4-8, 22:15-24, 26:1-5, 28:10-15.-->

12:1	Or YHWH avait dit à Abram : Va pour toi, hors de ta terre, de ta patrie, et de la maison de ton père, vers la terre que je te montrerai<!--Ac. 7:3 ; Hé. 11:8.-->.
12:2	Et je te ferai devenir une grande nation, et je te bénirai, je rendrai ton nom grand, et tu seras une bénédiction.
12:3	Je bénirai ceux qui te béniront, et je maudirai ceux qui te maudiront, et toutes les familles de la Terre seront bénies en toi<!--Ac. 3:25 ; Ga. 3:8.-->.

### Abram sur la terre de Canaan

12:4	Abram donc partit, comme YHWH le lui avait dit, et Lot alla avec lui. Abram était âgé de 75 ans quand il sortit de Charan.
12:5	Abram prit aussi Saraï, sa femme, et Lot, fils de son frère, avec tous les biens qu'ils avaient acquis, et toutes les personnes qu'ils avaient acquises à Charan. Ils partirent pour aller dans le pays de Canaan et ils arrivèrent au pays de Canaan<!--Ac. 7:4.-->.
12:6	Et Abram parcourut le pays jusqu'au lieu de Sichem, jusqu'aux chênes de Moré. Les Cananéens étaient alors dans le pays.
12:7	Et YHWH apparut à Abram et lui dit : Je donnerai ce pays à ta postérité. Et Abram bâtit là un autel à YHWH qui lui était apparu.
12:8	Et il se transporta de là vers la montagne, à l'orient de Béth-El, et il dressa ses tentes, ayant Béth-El à l'occident, et Aï à l'orient. Là, il bâtit un autel à YHWH et invoqua le Nom de YHWH.
12:9	Puis Abram partit de là, marchant et s'avançant vers le midi.

### Abram en Égypte

12:10	Mais la famine étant survenue dans le pays, Abram descendit en Égypte pour s'y retirer, car la famine était grande dans le pays.
12:11	Et il arriva, comme il était près d'entrer en Égypte, qu'il dit à Saraï, sa femme : S'il te plaît, je sais que tu es une femme de belle apparence.
12:12	Voici ce qui arrivera : quand les Égyptiens te verront, ils diront : C'est sa femme ! Ils me tueront, et toi, ils te laisseront vivre.
12:13	Dis que tu es ma sœur, je te prie, afin que je sois bien traité à cause de toi et que par ton moyen, ma vie soit préservée.
12:14	Il arriva que lorsque Abram fut arrivé en Égypte, les Égyptiens virent que cette femme était très belle.
12:15	Les princes de la cour de pharaon la virent aussi et la vantèrent à pharaon, si bien que la femme fut emmenée dans la maison de pharaon.
12:16	Il traita bien Abram à cause d'elle, de sorte qu'il en eut des brebis, des bœufs, des ânes, des serviteurs, des servantes, des ânesses, et des chameaux.
12:17	Mais YHWH frappa de grandes plaies pharaon et sa maison, à cause de Saraï, femme d'Abram.
12:18	Alors pharaon appela Abram et lui dit : Qu'est-ce que tu m'as fait ? Pourquoi ne m'as-tu pas déclaré que c'était ta femme ?
12:19	Pourquoi as-tu dit : C'est ma sœur ? Car je l'avais prise pour ma femme. Mais maintenant, voici ta femme, prends-la et va-t'en !
12:20	Et pharaon ayant donné ordre à ses gens, ils le renvoyèrent, lui, sa femme, et tout ce qui était à lui.

## Chapitre 13

### Retour d'Abram à Canaan

13:1	Abram donc monta d'Égypte vers le midi, lui, sa femme, et tout ce qui lui appartenait. Lot était avec lui.
13:2	Et Abram était très riche en bétail, en argent, et en or.
13:3	Et il revint par le même chemin qu'il avait suivi du midi à Béth-El, jusqu'au lieu où il avait dressé ses tentes au commencement, entre Béth-El et Aï,
13:4	au même lieu où était l'autel qu'il avait bâti au commencement, et Abram invoqua là le Nom de YHWH.

### Abram se sépare de Lot<!--Ge. 13:12.-->

13:5	Lot aussi, qui marchait avec Abram, avait des brebis, des bœufs, et des tentes.
13:6	Et le pays ne pouvait les porter pour demeurer ensemble. Car leurs biens étaient si grands qu'ils ne pouvaient demeurer ensemble.
13:7	Il y eut querelle entre les bergers du bétail d'Abram et les bergers du bétail de Lot. Or en ce temps-là, les Cananéens et les Phéréziens habitaient dans le pays.
13:8	Et Abram dit à Lot : Je te prie qu'il n'y ait pas de dispute entre moi et toi, ni entre mes bergers et les tiens, car nous sommes frères.
13:9	Tout le pays n'est-il pas devant toi ? Sépare-toi je te prie d'avec moi. Si tu vas à gauche, j'irai à droite, et si tu vas à droite, j'irai à gauche.

### Lot s'établit à Sodome<!--Ge. 13:10.-->

13:10	Et Lot leva les yeux et vit que toute la plaine du Yarden était entièrement arrosée. Avant que YHWH ait détruit Sodome et Gomorrhe, c'était, jusqu'à Tsoar, comme le jardin de YHWH, et comme le pays d'Égypte.
13:11	Lot choisit pour lui toute la plaine du Yarden, et Lot alla du côté de l'orient. Ainsi, l'homme se sépara de son frère.
13:12	Abram habita dans le pays de Canaan, et Lot habita dans les villes de la plaine, et dressa ses tentes jusqu'à Sodome.
13:13	Or les habitants de Sodome étaient méchants et de grands pécheurs contre YHWH.

### YHWH confirme son alliance avec Abram

13:14	Et YHWH dit à Abram, après que Lot se fut séparé de lui : Lève maintenant tes yeux et regarde du lieu où tu es, vers le nord, le midi, l'orient et l'occident.
13:15	Car je te donnerai, à toi et à ta postérité pour toujours, tout le pays que tu vois.
13:16	Et je rendrai ta postérité comme la poussière de la terre, de sorte que, si quelqu'un peut compter la poussière de la terre, il comptera aussi ta postérité<!--Ro. 4:18 ; Hé. 11:12.-->.
13:17	Lève-toi donc et parcours le pays dans sa longueur et dans sa largeur, car je te le donnerai.
13:18	Alors Abram transporta ses tentes, alla habiter dans les plaines de Mamré, qui sont près d'Hébron et là, il bâtit un autel à YHWH.

## Chapitre 14

### Abram va au secours de Lot

14:1	Or, il arriva du temps d'Amraphel, roi de Shinear, d'Aryok, roi d'Ellasar, de Kedorlaomer, roi d'Élam, et de Tideal, roi de Goyim<!--Mot hébreu signifiant « peuple, nation (non-hébreu) ».-->,
14:2	qu'ils firent la guerre contre Béra, roi de Sodome, et contre Birsha, roi de Gomorrhe, et contre Shineab, roi d'Adma, et contre Shémeéber, roi de Tseboïm, et contre le roi de Béla, qui est Tsoar.
14:3	Tous ceux-ci se joignirent dans la vallée de Siddim, qui est la Mer Salée.
14:4	Ils avaient été asservis 12 années à Kedorlaomer, et la treizième année, ils s'étaient révoltés.
14:5	À la quatorzième année, Kedorlaomer et les rois qui étaient avec lui vinrent et ils battirent les géants<!--Ou « Rephaïm ».--> à Ashteroth-Karnaïm, les Zouzim à Ham, et les Émim à Shaveh-Qiryathayim,
14:6	et les Horiens dans leur montagne de Séir, jusqu'au chêne de Paran, qui est près du désert.
14:7	Puis ils retournèrent et vinrent à En-Mishpath, qui est Qadesh, et ils frappèrent les Amalécites sur tout le pays et aussi les Amoréens qui habitaient dans Hatsatson-Thamar.
14:8	Alors le roi de Sodome, le roi de Gomorrhe, le roi d'Adma, le roi de Tseboïm, et le roi de Béla, qui est Tsoar, sortirent et rangèrent leurs troupes contre eux dans la vallée de Siddim,
14:9	contre Kedorlaomer, roi d'Élam, et contre Tideal, roi de Goyim, et contre Amraphel, roi de Shinear, et contre Aryok, roi d'Ellasar. Ils étaient 4 rois contre 5.
14:10	La vallée de Siddim était pleine de puits de bitume. Les rois de Sodome et de Gomorrhe s'enfuirent et y tombèrent, et le reste s'enfuit dans la montagne.
14:11	Ils prirent donc toutes les richesses de Sodome et de Gomorrhe, et tous leurs vivres, puis ils se retirèrent.
14:12	Ils prirent aussi Lot, fils du frère d'Abram, qui habitait dans Sodome, et tous ses biens, puis ils s'en allèrent.
14:13	Un fugitif vint l'annoncer à Abram l'Hébreu qui demeurait dans les plaines de Mamré, l'Amoréen, frère d'Eshcol et frère d'Aner. Ils avaient fait alliance avec Abram.
14:14	Dès qu'Abram eut appris que son frère avait été emmené prisonnier, il arma 318 hommes bien entraînés, nés dans sa maison, et il poursuivit ces rois jusqu'à Dan.
14:15	Et, ayant divisé sa troupe, il se jeta sur eux de nuit, lui et ses serviteurs. Il les battit et les poursuivit jusqu'à Choba, qui est au nord de Damas.
14:16	Et il ramena tous les biens qu'ils avaient pris. Il ramena aussi Lot, son frère, ses biens, les femmes et le peuple.

### Melchisédek, prêtre de El Elyon (El Très-Haut)

14:17	Et le roi de Sodome sortit à la rencontre d'Abram qui revenait vainqueur de Kedorlaomer, et des rois qui étaient avec lui, dans la vallée de la plaine, qui est la vallée royale.
14:18	Melchisédek<!--Melchisédek est un type du Mashiah (Ps. 110:4 ; Hé. 5:5-6, 6:20, 7:1-2). Ce personnage nous montre l'aspect du Mashiah en tant que Roi de Salem, ce qui signifie « roi de paix », et Grand-Prêtre possédant une prêtrise non transmissible (Hé. 7:24).-->, roi de Salem, fit sortir le pain et le vin. Il était prêtre de El Elyon<!--El Très-Haut.-->.
14:19	Et il bénit Abram, en disant : Béni soit Abram par El Elyon, Possesseur du ciel et de la Terre.
14:20	Béni soit El Elyon qui a livré tes ennemis entre tes mains ! Et Abram lui donna la dîme<!--Voir commentaire sur la dîme en No. 18:21 et Mal. 3:10.--> de tout.
14:21	Et le roi de Sodome dit à Abram : Donne-moi les personnes, et prends pour toi les richesses.
14:22	Et Abram répondit au roi de Sodome : Je lève ma main vers YHWH, El Elyon, Possesseur du ciel et de la Terre :
14:23	Je ne prendrai rien de tout ce qui est à toi, pas même un fil, ni un cordon de sandale, afin que tu ne dises pas : J'ai enrichi Abram.
14:24	Rien pour moi, seulement ce qu'ont mangé mes serviteurs et la part des hommes qui sont venus avec moi, Aner, Eshcol, et Mamré, qui prendront leur part.

## Chapitre 15

### YHWH promet un enfant à Abram

15:1	Après ces choses, la parole de YHWH vint à Abram dans une vision en disant : Abram, n'aie pas peur, je suis ton bouclier, ta grande et infinie récompense.
15:2	Abram répondit : Adonaï YHWH, que me donneras-tu ? Je m'en vais sans enfants et le fils qui aura l'acquisition de ma maison c'est Éliézer de Damas.
15:3	Abram dit aussi : Voici, tu ne m'as pas donné de postérité. Voilà, le fils de ma maison sera mon héritier.
15:4	Et voici, la parole de YHWH vint à lui en disant : Celui-ci ne sera pas ton héritier, mais celui qui sortira de tes entrailles sera ton héritier.
15:5	Puis l'ayant fait sortir dehors, il lui dit : Lève maintenant les yeux au ciel et compte les étoiles si tu peux les compter. Et il lui dit : Ainsi sera ta postérité.
15:6	Abram crut en YHWH, qui le lui compta comme justice<!--Ja. 2:23 ; Ga. 3:6 ; Ro. 4:3.-->.
15:7	Et il lui dit : Je suis YHWH qui t'ai fait sortir d'Our en Chaldée, afin de te donner ce pays en possession.
15:8	Abram répondit : Adonaï YHWH, à quoi connaîtrai-je que je le posséderai ?
15:9	Et YHWH lui répondit : Prends une génisse de 3 ans, une chèvre de 3 ans, un bélier de 3 ans, une tourterelle, et un jeune pigeon.
15:10	Il prit donc tous ces animaux, les coupa par le milieu, et mit chaque morceau l'un vis-à-vis de l'autre, mais il ne partagea pas les oiseaux.
15:11	Et les oiseaux de proie descendirent sur les cadavres, mais Abram les chassa.
15:12	Et il arriva, comme le soleil se couchait, qu'un profond sommeil tomba sur Abram, et voici, une frayeur d'une grande obscurité tomba sur lui.

### YHWH annonce l'esclavage de la postérité d'Abram

15:13	Et YHWH dit à Abram : Sache, sache que tes descendants habiteront comme étrangers dans un pays qui ne leur appartiendra pas. Ils y seront asservis et on les opprimera pendant 400 ans<!--Ac. 7:6 ; Ga. 3:17.-->.
15:14	Mais je jugerai aussi la nation à laquelle ils seront asservis, et après cela ils sortiront avec de grands biens<!--Ex. 3:22.-->.
15:15	Et toi tu iras vers tes pères en paix, et tu seras enterré après une heureuse vieillesse.
15:16	Et à la quatrième génération, ils reviendront ici, car l'iniquité des Amoréens n'est pas encore complète.
15:17	Il arriva aussi que le soleil étant couché, il y eut une obscurité profonde, et voici, une fournaise fumante et une torche de feu passèrent entre les animaux qui avaient été partagés.
15:18	En ce jour-là, YHWH traita alliance avec Abram en disant : J'ai donné ce pays à ta postérité, depuis le fleuve d'Égypte jusqu'au grand fleuve, le fleuve d'Euphrate,
15:19	le pays des Kéniens, des Kéniziens, des Kadmoniens,
15:20	des Héthiens, des Phéréziens, des géants<!--Ou « Rephaïm ».-->,
15:21	des Amoréens, des Cananéens, des Guirgasiens, et des Yebousiens.

## Chapitre 16

### Saraï donne sa servante pour femme à Abram

16:1	Or Saraï, femme d'Abram, ne lui avait enfanté aucun enfant, mais elle avait une servante égyptienne du nom d'Agar.
16:2	Et Saraï dit à Abram : Voici, YHWH m'a empêchée d'enfanter. Viens, je te prie vers ma servante, peut-être aurai-je des enfants par elle. Et Abram écouta la voix de Saraï.
16:3	Alors Saraï, femme d'Abram, prit Agar, sa servante égyptienne, et la donna pour femme à Abram, son mari, après qu'Abram eut habité 10 ans dans le pays de Canaan.
16:4	Il alla donc vers Agar et elle conçut. Quand Agar se vit enceinte, sa maîtresse devint insignifiante à ses yeux.
16:5	Et Saraï dit à Abram : Que la violence qui m'est faite soit sur toi. J'ai mis ma servante dans ton sein, mais quand elle a vu qu'elle avait conçu, je suis devenue insignifiante à ses yeux. Que YHWH soit juge entre moi et toi !
16:6	Alors Abram répondit à Saraï : Voici, ta servante est entre tes mains, traite-la comme il sera bon à tes yeux. Saraï donc l'opprima, et Agar s'enfuit de sa présence.

### L'Ange de YHWH apparaît à Agar

16:7	Mais l'Ange de YHWH la trouva près d'une fontaine d'eau dans le désert, près de la fontaine qui est sur le chemin de Shour.
16:8	Et il lui dit : Agar, servante de Saraï, d'où viens-tu ? Et où vas-tu ? Et elle répondit : Je m'enfuis de la présence de Saraï, ma maîtresse.
16:9	Et l'Ange de YHWH lui dit : Retourne vers ta maîtresse et humilie-toi sous sa main.
16:10	L'Ange de YHWH lui dit : Je multiplierai, je multiplierai ta postérité, elle sera si nombreuse qu'on ne pourra la compter.
16:11	L'Ange de YHWH lui dit aussi : Voici, tu as conçu, tu enfanteras un fils, et tu lui donneras le nom de Yishmael<!--Ismaël.-->, car YHWH a entendu ton affliction.
16:12	Il sera pareil à un âne sauvage. Sa main sera contre tous et la main de tous contre lui. Et il habitera en face de tous ses frères.
16:13	Alors elle appela Atta-El-roï<!--Tu es le El qui me voit.--> le Nom de YHWH qui lui avait parlé car elle dit : N'ai-je pas vu ici même celui qui me voyait ?
16:14	C'est pourquoi on a appelé ce puits le puits de Lachaï-roï<!--Du Vivant qui me voit.-->. Il se trouve entre Qadesh et Bared.

### Naissance de Yishmael (Ismaël)

16:15	Et Agar enfanta un fils à Abram, et Abram donna le nom de Yishmael au fils qu'Agar lui avait enfanté<!--Ga. 4:22.-->.
16:16	Abram était âgé de 86 ans quand Agar enfanta Yishmael à Abram.

## Chapitre 17

### El Shaddaï (El Tout-Puissant) confirme sa promesse

17:1	Lorsque Abram fut âgé de 99 ans, YHWH apparut à Abram et lui dit : Je suis El Shaddaï<!--Elohîm se révèle ici à Abraham comme l'Elohîm Tout-Puissant. Or le Mashiah (Christ) s'est présenté à Yohanan (Jean) comme l'Elohîm Tout-Puissant (Ap. 1:8). Plus loin en Ap. 5:6, le Seigneur apparaît au milieu du trône céleste sous la forme d'un Agneau ayant sept cornes qui représentent sa toute puissance. Yéhoshoua est bien l'Elohîm Tout-Puissant qui s'était révélé à Abraham.-->. Marche devant ma face, et sois intègre.
17:2	Et je mettrai mon alliance entre moi et toi, et je te multiplierai extrêmement, extrêmement.

### Abram devient Abraham

17:3	Alors Abram tomba sur sa face, et Elohîm lui parla et lui dit :
17:4	Quant à moi, voici, mon alliance est avec toi. Tu deviendras le père d'une multitude de nations<!--Ro. 4:17.-->.
17:5	Et on ne t'appellera plus du nom d'Abram<!--Né. 9:7.-->, mais ton nom sera Abraham, car je t'ai établi père d'une multitude de nations.
17:6	Je te ferai extrêmement, extrêmement porter du fruit : je te ferai devenir des nations, et des rois sortiront de toi<!--Mt. 1:6.-->.
17:7	J'établirai donc mon alliance entre moi et toi, et entre ta postérité après toi, selon leurs générations, ce sera une alliance éternelle en vertu de laquelle je serai ton Elohîm et celui de ta postérité après toi.
17:8	Je te donnerai, à toi et à ta postérité après toi, le pays où tu demeures comme étranger, tout le pays de Canaan, en possession perpétuelle, et je serai leur Elohîm.

### La circoncision, signe de l'alliance

17:9	Elohîm dit encore à Abraham : Tu garderas donc mon alliance, toi et ta postérité après toi, selon leurs générations.
17:10	Et voici mon alliance entre moi et vous, et entre ta postérité après toi, que vous garderez : Tout mâle parmi vous sera circoncis.
17:11	Et vous circoncirez la chair de votre prépuce, ce sera le signe d'alliance entre moi et vous<!--Ac. 7:8 ; Ro. 4:11.-->.
17:12	Tout enfant mâle de 8 jours sera circoncis parmi vous dans vos générations, tant celui qui est né dans la maison que l'esclave acquis à prix d'argent de tout étranger qui n'est pas de ta race<!--Lé. 12:3 ; Lu. 2:21.-->.
17:13	Celui qui est né dans ta maison et celui qui est acquis à prix d'argent seront circoncis, ils seront circoncis et mon alliance sera dans votre chair pour être une alliance perpétuelle.
17:14	Et le mâle incirconcis qui n'aura pas été circoncis dans la chair de son prépuce, cette âme sera retranchée du milieu de son peuple. Il aura rompu mon alliance.

### Saraï devient Sarah ; promesse de la naissance de Yitzhak (Isaac)

17:15	Elohîm dit aussi à Abraham : Quant à Saraï, ta femme, tu n'appelleras plus son nom Saraï, mais son nom sera Sarah.
17:16	Et je la bénirai, et même je te donnerai un fils d'elle. Je la bénirai et elle deviendra des nations ; des rois de peuples viendront d'elle.
17:17	Alors Abraham se prosterna la face contre terre, et rit, en disant en son cœur : Naîtrait-il un fils à un homme âgé de 100 ans ? Et Sarah, une femme âgée de 90 ans, aurait-elle un enfant ?
17:18	Et Abraham dit à Elohîm : Je te prie, que Yishmael vive devant toi.
17:19	Et Elohîm dit : Sarah, ta femme, t'enfantera vraiment un fils et tu appelleras son nom Yitzhak<!--Isaac.-->. J'établirai mon alliance avec lui pour être une alliance perpétuelle pour sa postérité après lui.

### Une nation sortira de Yishmael (Ismaël)

17:20	Je t'ai aussi exaucé concernant Yishmael : Voici, je le bénirai, je le ferai porter du fruit et je le multiplierai extrêmement, extrêmement. Il engendrera douze princes, et je le ferai devenir une grande nation.
17:21	Mais j'établirai mon alliance avec Yitzhak, que Sarah t'enfantera l'année qui vient, au temps fixé.
17:22	Et Elohîm ayant achevé de parler, s'éleva au-dessus d'Abraham.
17:23	Et Abraham prit son fils Yishmael, avec tous ceux qui étaient nés dans sa maison, et tous ceux qu'il avait acquis à prix d'argent, tous les mâles parmi les hommes de la maison d'Abraham, et il circoncit la chair de leur prépuce en ce même jour-là, comme Elohîm le lui avait dit.
17:24	Abraham était âgé de 99 ans quand il circoncit la chair de son prépuce,
17:25	et Yishmael, son fils, était âgé de 13 ans lorsque fut circoncise la chair de son prépuce.
17:26	En ce même jour, Abraham fut circoncis, et son fils Yishmael aussi.
17:27	Et tous les hommes de sa maison, tant ceux qui étaient nés dans sa maison que ceux qui avaient été acquis à prix d'argent, des étrangers, furent circoncis avec lui.

## Chapitre 18

### Abraham, ami de YHWH<!--Jn. 3:29, 15:13-15 ; Ja. 2:23.-->

18:1	YHWH lui apparut dans les plaines de Mamré, comme il était assis à la porte de sa tente, pendant la chaleur du jour.
18:2	Il leva les yeux et regarda, et voici, trois hommes se tenaient devant lui. Quand il les vit, il courut au-devant d'eux depuis la porte de sa tente et se prosterna à terre<!--Hé. 13:2.-->.
18:3	Il dit : Adonaï, je te prie, si j'ai trouvé grâce à tes yeux, ne passe pas, je te prie, loin de ton serviteur.
18:4	Qu'on prenne, je vous prie, un peu d'eau, et lavez vos pieds, et reposez-vous sous un arbre.
18:5	J'apporterai un morceau de pain pour fortifier votre cœur. Après quoi vous passerez outre, car c'est pour cela que vous êtes venus vers votre serviteur. Et ils dirent : Fais ce que tu as dit.
18:6	Abraham donc s'en alla en hâte dans la tente vers Sarah et lui dit : Vite ! Pétris 3 mesures de fleur de farine et fais des gâteaux !
18:7	Puis Abraham courut au troupeau et prit un veau tendre et bon, et le donna à un serviteur qui se hâta de l'apprêter.
18:8	Ensuite, il prit du beurre et du lait, et le veau qu'on avait apprêté, et le mit devant eux. Il se tint auprès d'eux sous l'arbre et ils mangèrent.
18:9	Et ils lui dirent : Où est Sarah, ta femme ? Et il répondit : La voilà dans la tente.
18:10	Et l'un d'entre eux dit : Je reviendrai, je reviendrai vers toi au temps du renouveau et voici que Sarah ta femme aura un fils. Et Sarah écoutait à la porte de la tente qui était derrière lui<!--Ro. 9:9.-->.
18:11	Or Abraham et Sarah étaient vieux, très avancés en âge, et Sarah n'avait plus ce que les femmes ont coutume d'avoir<!--Sarah avait cessé d'avoir ses règles. Ro. 4:19 ; Hé. 11:11.-->.
18:12	Et Sarah rit en elle-même et dit : Maintenant que je suis vieille et mon seigneur aussi est vieux, aurai-je encore des désirs ?
18:13	Et YHWH dit à Abraham : Pourquoi Sarah a-t-elle ri en disant : Enfanterai-je vraiment et réellement, moi qui suis vieille ?
18:14	Y a-t-il quelque chose qui soit difficile pour YHWH ? Je reviendrai vers toi au temps fixé, au temps du renouveau, et Sarah aura un fils<!--Mt. 19:26 ; Lu. 1:37.-->.
18:15	Et Sarah le nia en disant : Je n'ai pas ri, car elle avait peur. Il dit : Non, car tu as ri.
18:16	Et ces hommes se levèrent de là et regardèrent vers Sodome. Abraham alla avec eux pour les accompagner.
18:17	Et YHWH dit : Cacherai-je à Abraham ce que je vais faire ?
18:18	Abraham deviendra, il deviendra une nation grande et puissante, et toutes les nations de la Terre seront bénies en lui<!--Ac. 3:25 ; Ga. 3:8.-->.
18:19	Car je l'ai distingué afin qu'il ordonne à ses fils et à sa maison après lui, de garder la voie de YHWH en pratiquant la justice et la droiture, afin que YHWH fasse venir sur Abraham tout ce qu'il lui a dit.
18:20	Et YHWH dit : Le cri de détresse contre Sodome et Gomorrhe est grand, et leur péché est extrêmement pesant.
18:21	Je descendrai maintenant et je verrai s'ils ont agi tout à fait selon le cri qui est venu jusqu'à moi. S'il n'en est pas ainsi, je le saurai.
18:22	Ces hommes donc, partant de là, allèrent vers Sodome, mais Abraham se tint encore devant YHWH.

### Intercession d'Abraham

18:23	Et Abraham s'approcha et dit : Feras-tu périr le juste avec le méchant ?
18:24	Peut-être y a-t-il 50 justes dans la ville, les feras-tu périr aussi ? Ne pardonneras-tu pas à la ville à cause des 50 justes qui sont au milieu d'elle ?
18:25	Loin de toi de faire cette chose-là ! De faire mourir le juste avec le méchant, en sorte que le juste soit comme le méchant. Loin de toi ! Celui qui juge toute la Terre ne fera-t-il pas justice<!--Ro. 3:5-6.--> ?
18:26	Et YHWH dit : Si je trouve dans Sodome 50 justes au milieu de la ville, je pardonnerai à toute la ville à cause d'eux.
18:27	Et Abraham répondit en disant : Voici, j'ai pris maintenant la hardiesse de parler à Adonaï, moi qui ne suis que poussière et cendres.
18:28	Peut-être en manquera-t-il 5 des 50 justes. Détruiras-tu toute la ville pour ces 5 là ? Et YHWH lui répondit : Je ne la détruirai pas si j'y trouve 45 justes.
18:29	Abraham continua de lui parler en disant : Peut-être s'y trouvera-t-il 40 ? Et il dit : Je ne lui ferai rien à cause de ces 40.
18:30	Abraham dit : Je prie Adonaï de ne pas se fâcher si je parle. Peut-être s'y trouvera-t-il 30 ? Et il dit : Je ne lui ferai rien si j'y trouve 30 justes.
18:31	Abraham dit : Voici, maintenant j'ai pris la hardiesse de parler à Adonaï : Peut-être s'y trouvera-t-il 20 ? Et il dit : Je ne la détruirai pas à cause de ces 20.
18:32	Abraham dit : Je prie Adonaï de ne pas s'irriter, je parlerai encore une seule fois : Peut-être s'y trouvera-t-il 10 ? Et YHWH dit : Je ne la détruirai pas à cause de ces 10.
18:33	YHWH s'en alla quand il eut achevé de parler avec Abraham. Et Abraham retourna dans sa demeure.

## Chapitre 19

### Lot accueille les anges dans sa maison<!--Cp. Ge. 13:10,12, 19:1-3.-->

19:1	Or sur le soir, les deux anges arrivèrent à Sodome. Lot était assis à la porte de Sodome. Quand Lot les vit, il se leva pour aller au-devant d'eux, et se prosterna la face contre terre.
19:2	Et il leur dit : Voici, je vous prie, mes seigneurs, entrez maintenant dans la maison de votre serviteur pour y passer la nuit. Vous vous laverez les pieds, vous vous lèverez de bonne heure puis vous poursuivrez votre chemin. Non, répondirent-ils, nous passerons la nuit sur la place.
19:3	Mais il les pressa tellement qu'ils se retirèrent chez lui. Quand ils furent entrés dans sa maison, il leur donna un festin, fit cuire des pains sans levain, et ils mangèrent.
19:4	Ils n'étaient pas encore couchés que les hommes de la ville, les hommes de Sodome, environnèrent la maison, depuis les jeunes hommes jusqu'aux vieillards, tout le peuple était ensemble.
19:5	Ils appelèrent Lot et ils lui dirent : Où sont les hommes qui sont venus cette nuit chez toi ? Fais-les sortir afin que nous les connaissions.
19:6	Mais Lot sortit vers eux à l'entrée et, ayant fermé la porte derrière lui,
19:7	il leur dit : Je vous prie, mes frères, ne leur faites pas de mal.
19:8	Voici, j'ai deux filles qui n'ont pas encore connu d'homme. Je vous les amènerai et vous leur ferez ce qui est bon à vos yeux. Seulement, ne faites pas de mal à ces hommes, car ils sont venus à l'ombre de mon toit.
19:9	Ils lui dirent : Va-t-en loin d'ici. Ils dirent aussi : En voilà un qui est venu comme étranger, et il veut nous gouverner, nous gouverner ! Maintenant nous te ferons plus de mal qu'à eux. Ils poussèrent fortement l'homme, Lot, et s'approchèrent pour briser la porte<!--2 Pi. 2:7-8.-->.
19:10	Mais ces hommes étendirent leurs mains, firent rentrer Lot vers eux dans la maison, et fermèrent la porte.
19:11	Et ils frappèrent d'aveuglement les hommes qui étaient à la porte de la maison, depuis le plus petit jusqu'au plus grand, de sorte qu'ils se lassèrent à chercher la porte.
19:12	Alors ces hommes dirent à Lot : Qui as-tu encore ici qui t'appartienne ? Gendres, fils et filles, et tout ce qui t'appartient dans la ville, fais-les sortir de ce lieu.
19:13	Car nous allons détruire ce lieu parce que le cri contre ses habitants est grand devant YHWH. YHWH nous a envoyés pour le détruire.
19:14	Lot sortit donc et parla à ses gendres, qui devaient prendre ses filles, et leur dit : Levez-vous, sortez de ce lieu, car YHWH va détruire la ville. Mais aux yeux de ses gendres, il parut plaisanter.

### Jugement sur Sodome

19:15	Quand l'aube se leva, les anges pressèrent Lot en disant : Lève-toi, prends ta femme et tes deux filles qui se trouvent ici, de peur que tu ne périsses dans le châtiment de la ville.
19:16	Et comme il tardait, ces hommes saisirent sa main, la main de sa femme et les mains de ses deux filles, à cause de la miséricorde de YHWH à son égard. Ils l'emmenèrent et le mirent hors de la ville.
19:17	Et il arriva, quand ils les eurent fait sortir dehors, qu'il dit : Sauve ta vie, ne regarde pas derrière toi, et ne t'arrête en aucun endroit de la plaine. Sauve-toi sur la montagne de peur que tu ne périsses.
19:18	Lot leur répondit : Non, Seigneur, je te prie.
19:19	Voici, ton serviteur a maintenant trouvé grâce à tes yeux, et tu as montré la grandeur de ta bonté à mon égard en préservant ma vie, mais je ne pourrai pas me sauver vers la montagne de peur que le mal ne m'atteigne, et je mourrai.
19:20	Voici, je te prie, cette ville est assez proche pour que je m'y enfuie, et elle est petite. Je te prie que je m'y sauve ! N'est-elle pas petite ? Et mon âme vivra.
19:21	Et il lui dit : Voici, je t'accorde la chose que tu me demandes, je ne renverserai pas la ville dont tu as parlé.
19:22	Hâte-toi de t'y sauver, car je ne pourrai rien faire jusqu'à ce que tu y sois entré. C'est pourquoi le nom de cette ville fut appelé Tsoar.
19:23	Comme le soleil se levait sur la Terre, Lot entra dans Tsoar.
19:24	Alors YHWH fit pleuvoir du ciel, sur Sodome et sur Gomorrhe, du soufre et du feu, de la part de YHWH<!--De. 29:23 ; Lu. 17:29 ; Jud. 1:7.-->.
19:25	Et il renversa ces villes-là, et toute la plaine, et tous les habitants des villes, et les herbes du sol.
19:26	Mais la femme de Lot regarda derrière elle, et elle devint un pilier de sel<!--Lu. 17:31-33.-->.
19:27	Abraham se leva de bon matin et vint au lieu où il s'était tenu devant YHWH.
19:28	Et regardant vers Sodome et Gomorrhe, et vers tout le sol de cette plaine-là, il vit monter du sol une fumée comme la fumée d'une fournaise.
19:29	Mais il arriva, lorsqu'Elohîm détruisit les villes de la plaine, qu'il se souvînt d'Abraham. Il envoya Lot hors du milieu du désastre par lequel il renversa les villes où Lot habitait.

### Une abomination commise dans la famille de Lot<!--Ge. 13:10,12.-->

19:30	Et Lot monta de Tsoar et habita sur la montagne avec ses deux filles, car il craignait de demeurer dans Tsoar, et il se retira dans une caverne avec ses deux filles.
19:31	Et l'aînée dit à la plus jeune : Notre père est vieux, et il n'y a personne dans le pays pour venir vers nous, selon la coutume de tous les pays.
19:32	Allons ! Donnons à boire du vin à notre père et couchons avec lui afin que nous conservions la race de notre père.
19:33	Elles donnèrent donc du vin à boire à leur père cette nuit-là, et l'aînée vint et coucha avec son père, mais il ne s'aperçut ni quand elle se coucha ni quand elle se leva.
19:34	Et il arriva le lendemain que l'aînée dit à la plus jeune : Voici, j'ai couché la nuit dernière avec mon père, donnons-lui encore du vin à boire cette nuit, puis va et couche avec lui, et nous conserverons la race de notre père.
19:35	Cette nuit-là, elles donnèrent encore du vin à boire à leur père, et la plus jeune se leva et coucha avec lui. Mais il ne s'aperçut ni quand elle se coucha ni quand elle se leva.
19:36	Ainsi, les deux filles de Lot conçurent de leur père.
19:37	Et l'aînée enfanta un fils qu'elle appela du nom de Moab. C'est le père des Moabites jusqu'à ce jour.
19:38	Et la plus jeune aussi enfanta un fils qu'elle appela du nom de Ben-Ammi. C'est le père des fils d'Ammon jusqu'à ce jour.

## Chapitre 20

### Faute d'Abraham à Guérar<!--Ge. 26:6-32.-->

20:1	Et Abraham s'en alla de là pour le pays du midi. Il demeura entre Qadesh et Shour et habita comme étranger à Guérar.
20:2	Or Abraham disait de Sarah, sa femme : C'est ma sœur. Et Abimélec, roi de Guérar, envoya enlever Sarah.
20:3	Mais Elohîm apparut la nuit dans un rêve à Abimélec et lui dit : Voici, tu vas mourir à cause de la femme que tu as enlevée, car elle a un mari qui l'a épousée.
20:4	Abimélec, qui ne s'était pas approché d'elle, répondit : Adonaï, feras-tu donc mourir une nation juste ?
20:5	Ne m'a-t-il pas dit : C'est ma sœur ? Et elle-même aussi n'a-t-elle pas dit : C'est mon frère ? C'est dans l'intégrité de mon cœur et dans l'innocence de mes mains que j'ai fait cela.
20:6	Et Elohîm lui dit en rêve : Je sais que tu l'as fait dans l'intégrité de ton cœur, aussi ai-je empêché que tu ne pèches contre moi. C'est pourquoi je n'ai pas permis que tu la touches.
20:7	Maintenant donc rends la femme de cet homme, car il est prophète. Il priera pour toi et tu vivras. Mais si tu ne la rends pas, sache que tu mourras, tu mourras toi et tout ce qui est à toi.
20:8	Et Abimélec se leva de bon matin, appela tous ses serviteurs, et rapporta à leurs oreilles toutes ces choses, et ils furent saisis de crainte.
20:9	Puis Abimélec appela Abraham et lui dit : Que nous as-tu fait ? Et en quoi t'ai-je offensé que tu aies fait venir sur moi et sur mon royaume un grand péché ? Tu m'as fait des choses qui ne se font pas.
20:10	Abimélec dit aussi à Abraham : Qu'avais-tu en vue en agissant de la sorte ?
20:11	Et Abraham répondit : C'est parce que je disais : Il n'y a sûrement aucune crainte d'Elohîm dans cet endroit, et ils me tueront à cause de ma femme.
20:12	D'ailleurs elle est vraiment ma sœur, fille de mon père, mais elle n'est pas fille de ma mère et elle m'a été donnée pour femme.
20:13	Et il est arrivé, lorsqu'Elohîm m'a fait errer loin de la maison de mon père, que je lui ai dit : Aie la bonté de dire, dans tous les endroits où nous irons, que je suis ton frère.
20:14	Alors Abimélec prit des brebis, des bœufs, des serviteurs et des servantes, et les donna à Abraham, et lui rendit Sarah, sa femme.
20:15	Et Abimélec lui dit : Voici devant toi mon pays, demeure où il sera bon à tes yeux.
20:16	Il dit à Sarah : Voici, je donne à ton frère 1 000 pièces d'argent. Ce sera pour toi un voile sur les yeux pour tous ceux qui sont avec toi et cela te justifiera devant tous.
20:17	Et Abraham pria Elohîm, et Elohîm guérit Abimélec, sa femme, et ses servantes, de sorte qu'elles purent avoir des enfants.
20:18	Car YHWH avait fermé, fermé toute matrice de la maison d'Abimélec, à cause de Sarah, femme d'Abraham.

## Chapitre 21

### Naissance de Yitzhak (Isaac)

21:1	YHWH visita Sarah comme il avait dit, YHWH accomplit pour Sarah ce qu'il avait déclaré.
21:2	Sarah conçut et enfanta un fils à Abraham dans sa vieillesse, au temps fixé qu'Elohîm lui avait dit.
21:3	Abraham donna le nom de Yitzhak au fils qui lui était né, que Sarah lui avait enfanté.
21:4	Abraham circoncit son fils Yitzhak âgé de 8 jours, comme Elohîm le lui avait ordonné.
21:5	Or Abraham était âgé de 100 ans quand Yitzhak, son fils, lui naquit.
21:6	Et Sarah dit : Elohîm m'a donné de quoi rire. Tous ceux qui l'apprendront riront de moi.
21:7	Elle dit aussi : Qui aurait dit à Abraham que Sarah allaiterait des enfants ? Car je lui ai enfanté un fils dans sa vieillesse.
21:8	L'enfant grandit et fut sevré. Abraham fit un grand festin le jour où Yitzhak fut sevré.

### Abraham chasse Agar et Yishmael (Ismaël)<!--Ga. 4:21-31.-->

21:9	Sarah vit rire le fils qu'Agar l'Égyptienne avait enfanté à Abraham.
21:10	Et elle dit à Abraham : Chasse cette servante et son fils, car le fils de cette servante n'héritera pas avec mon fils, avec Yitzhak<!--Ga. 4:30.-->.
21:11	Cette parole déplut beaucoup aux yeux d'Abraham à cause de son fils.
21:12	Mais Elohîm dit à Abraham : Que cela ne déplaise pas à tes yeux, à cause du garçon et de ta servante. Écoute la voix de Sarah dans toutes les choses qu'elle te dira, car c'est en Yitzhak que ta postérité sera appelée de ton nom<!--Ro. 9:7.-->.
21:13	Toutefois, je ferai aussi devenir le fils de la servante une nation, parce qu'il est ta semence.
21:14	Puis Abraham se leva de bon matin et prit du pain et une outre d'eau, et il les donna à Agar en les mettant sur son épaule. Il lui donna aussi l'enfant et la renvoya. Elle se mit en chemin et fut errante dans le désert de Beer-Shéba.
21:15	Quand l'eau de l'outre fut épuisée, elle jeta l'enfant sous un arbrisseau,
21:16	et elle alla s'asseoir vis-à-vis, à une portée d'arc, car elle dit : Que je ne voie pas mourir mon enfant ! Elle s'assit donc vis-à-vis de lui, éleva la voix et pleura.
21:17	Elohîm entendit la voix du garçon. L'Ange d'Elohîm appela des cieux Agar et lui dit : Qu'as-tu Agar ? N'aie pas peur, car Elohîm a entendu la voix du garçon du lieu où il est.
21:18	Lève-toi ! Porte le garçon et soutiens-le de la main, car je le ferai devenir une grande nation.
21:19	Et Elohîm lui ouvrit les yeux et elle vit un puits d'eau. Elle alla remplir d'eau l'outre et donna à boire au garçon.
21:20	Elohîm fut avec le garçon qui devint grand et demeura dans le désert. Il devint un tireur d'arc.
21:21	Il habita dans le désert de Paran et sa mère lui prit une femme du pays d'Égypte.

### Abraham à Beer-Shéba

21:22	Et il arriva en ce temps-là qu'Abimélec et Picol, chef de son armée, parla à Abraham en disant : Elohîm est avec toi dans toutes les choses que tu fais.
21:23	Maintenant donc, jure-moi ici par Elohîm que tu ne me mentiras pas, ni à mes enfants ni aux enfants de mes enfants, et que selon la faveur que je t'ai faite, tu agiras envers moi et envers le pays où tu séjournes comme étranger.
21:24	Abraham répondit : Je le jurerai.
21:25	Mais Abraham fit des reproches à Abimélec au sujet d'un puits d'eau dont les serviteurs d'Abimélec s'étaient emparés de force.
21:26	Abimélec répondit : J'ignore qui a fait cela, et aussi tu ne m'en as pas informé, et moi, je ne l'apprends qu'aujourd'hui.
21:27	Alors Abraham prit des brebis et des bœufs et les donna à Abimélec, et ils firent alliance ensemble.
21:28	Abraham mit à part sept jeunes brebis de son troupeau.
21:29	Et Abimélec dit à Abraham : Que veulent dire ces sept jeunes brebis que tu as mises à part ?
21:30	Il répondit : C'est pour que tu acceptes de ma main ces sept jeunes brebis. Elles me serviront de témoignage que j'ai creusé ce puits.
21:31	C'est pourquoi on appela ce lieu-là Beer-Shéba, car c'est là qu'ils prêtèrent tous les deux serment.
21:32	Ils traitèrent donc alliance à Beer-Shéba, puis Abimélec se leva avec Picol, chef de son armée, et ils retournèrent au pays des Philistins.
21:33	Abraham planta des tamaris à Beer-Shéba, et là il invoqua le nom de YHWH, El-Olam<!--El d'éternité. Voir Es. 40:28.-->.
21:34	Abraham séjourna beaucoup de jours comme étranger dans le pays des Philistins.

## Chapitre 22

### Abraham offre Yitzhak (Isaac) en sacrifice<!--Hé. 11:17-19.-->

22:1	Or il arriva après ces choses qu'Elohîm éprouva Abraham et lui dit : Abraham ! Et il répondit : Me voici.
22:2	Et Elohîm lui dit : Prends maintenant ton fils, ton unique, celui que tu aimes, Yitzhak. Va-t'en au pays de Moriyah et là offre-le en holocauste sur l'une des montagnes que je te dirai.
22:3	Abraham donc s'étant levé de bon matin, sella son âne et prit deux de ses serviteurs avec lui et Yitzhak son fils. Ayant fendu le bois pour l'holocauste, il se mit en chemin et s'en alla au lieu qu'Elohîm lui avait dit.
22:4	Le troisième jour, Abraham levant ses yeux, vit le lieu de loin.
22:5	Abraham dit à ses serviteurs : Restez ici avec l'âne. Le garçon et moi, nous irons jusque là-bas pour adorer, puis nous reviendrons auprès de vous.
22:6	Abraham prit le bois de l'holocauste et le mit sur Yitzhak, son fils, et il prit le feu dans sa main et un couteau. Ils s'en allèrent tous les deux ensemble.
22:7	Alors Yitzhak parla à Abraham, son père, et dit : Mon père ! Abraham répondit : Me voici mon fils. Et il dit : Voici le feu et le bois, mais où est l'agneau pour l'holocauste<!--Yitzhak (Isaac) est un autre type du Mashiah qui s'offre en sacrifice pour l'expiation de nos péchés. La réponse à sa question au v. 7, « Voici le feu et le bois, mais où est l'agneau pour l'holocauste ? », a été apportée bien des siècles plus tard par Yohanan le Baptiste : « Voici l'Agneau d'Elohîm, qui ôte le péché du monde » (Jn. 1:29).--> ?
22:8	Abraham répondit : Mon fils, Elohîm examinera lui-même l'agneau pour l'holocauste. Et ils marchèrent tous les deux ensemble.
22:9	Et étant arrivés au lieu qu'Elohîm lui avait dit, Abraham bâtit là un autel et rangea le bois, et ensuite il lia Yitzhak, son fils, et le mit sur l'autel, par-dessus le bois<!--Ja. 2:21.-->.
22:10	Puis Abraham étendit sa main et prit le couteau pour tuer son fils.
22:11	Mais l'Ange de YHWH l'appela des cieux et dit : Abraham, Abraham ! Il répondit : Me voici.
22:12	L'Ange lui dit : Ne porte pas ta main sur le garçon et ne lui fais rien, car maintenant je sais que tu crains Elohîm, puisque tu ne m'as pas refusé ton fils, ton unique.
22:13	Abraham leva les yeux et regarda, et voici qu'un bélier était pris par les cornes dans un buisson. Abraham alla prendre le bélier et l'offrit en holocauste à la place de son fils.
22:14	Abraham donna à ce lieu le nom de YHWH-Yireh<!--YHWH pourvoira.-->. C'est pourquoi l'on dit aujourd'hui : Sur la montagne de YHWH il y sera pourvu.
22:15	L'Ange de YHWH appela des cieux Abraham pour la seconde fois,
22:16	et dit : Je le jure par moi-même<!--Hé. 6:13-15.-->, parole de YHWH ! Parce que tu as fait cela, et que tu n'as pas refusé ton fils, ton unique,
22:17	je te bénirai, je te bénirai. Je multiplierai, je multiplierai ta postérité comme les étoiles du ciel et comme le sable qui est sur le bord de la mer. Ta postérité possédera la porte de ses ennemis.
22:18	Toutes les nations de la Terre seront bénies en ta postérité<!--Ga. 3:16.-->, parce que tu as obéi à ma voix.
22:19	Ainsi, Abraham retourna vers ses serviteurs. Ils se levèrent et s'en allèrent ensemble à Beer-Shéba, car Abraham demeurait à Beer-Shéba.
22:20	Après ces choses, quelqu'un apporta des nouvelles à Abraham en disant : Voici, Milkah a aussi enfanté des fils à Nachor, ton frère.
22:21	Outs, son premier-né, et Bouz son frère, Kemouel, père d'Aram,
22:22	Késed, Hazo, Pildash, Yidlaph et Betouel.
22:23	Betouel a engendré Ribqah<!--Rébecca.-->. Milkah enfanta ces huit fils à Nachor, frère d'Abraham.
22:24	Sa concubine, nommée Reouma, enfanta aussi Thébach, Gaham, Tahash et Ma'akah.

## Chapitre 23

### Mort de Sarah

23:1	La vie de Sarah fut de 127 ans : ce sont là les années de la vie de Sarah.
23:2	Sarah mourut à Qiryath-Arba, qui est Hébron, dans le pays de Canaan. Abraham vint pour mener deuil sur Sarah et pour la pleurer.
23:3	Et Abraham se leva de devant son mort, il parla aux fils de Heth en disant :
23:4	Je suis étranger et habitant parmi vous. Donnez-moi une possession de sépulcre parmi vous afin que j'enterre mon mort et que je l'ôte de devant moi.
23:5	Les fils de Heth répondirent à Abraham et lui dirent :
23:6	Mon seigneur, écoute-nous ! Tu es un prince d'Elohîm parmi nous, enterre ton mort dans le meilleur de nos sépulcres. Aucun de nous ne te refusera son sépulcre afin que tu y enterres ton mort.
23:7	Alors Abraham se leva et se prosterna devant le peuple du pays, devant les Héthiens.
23:8	Et il leur parla et dit : Si c'est votre volonté que j'enterre mon mort hors de ma vue, écoutez-moi et intercédez en ma faveur auprès d'Éphron, fils de Tsochar,
23:9	afin qu'il me cède sa caverne de Macpéla qui est à l'extrémité de son champ. Qu'il me la cède contre sa valeur en argent afin qu'elle me serve de possession sépulcrale au milieu de vous.
23:10	Éphron était assis parmi les fils de Heth. Et Éphron, le Héthien, répondit à Abraham, aux oreilles des fils de Heth qui l'écoutaient, devant tous ceux qui entraient par la porte de sa ville, et dit :
23:11	Non, mon seigneur, écoute-moi ! Je te donne le champ, je te donne aussi la caverne qui s'y trouve. Je te la donne en présence des enfants de mon peuple. Enterres-y ton mort !
23:12	Abraham se prosterna devant le peuple du pays.
23:13	Il parla ainsi à Éphron, en présence de tout le peuple du pays qui écoutait, et dit : S'il te plaît, je te prie, écoute-moi ! Je donnerai l'argent du champ. Reçois-le de moi et j'y enterrerai mon mort.
23:14	Et Éphron répondit à Abraham en disant :
23:15	Mon seigneur, écoute-moi ! La terre vaut 400 sicles d'argent, qu'est-ce que cela entre moi et toi ? Enterre donc ton mort.
23:16	Abraham ayant entendu Éphron, lui paya l'argent dont il avait parlé, en présence des fils de Heth : 400 sicles d'argent ayant cours chez les marchands<!--Ac. 7:16.-->.
23:17	Le champ d'Éphron, qui était à Macpéla, vis-à-vis de Mamré, le champ et la caverne qui s'y trouve et tous les arbres qui sont dans le champ et dans toutes ses limites alentour,
23:18	tout fut acquis comme propriété à Abraham, en présence des fils de Heth, et de tous ceux qui entraient par la porte de la ville.
23:19	Après cela, Abraham enterra Sarah, sa femme, dans la caverne du champ de Macpéla, vis-à-vis de Mamré, qui est Hébron, dans le pays de Canaan.
23:20	Le champ et la caverne qui y est demeurèrent à Abraham comme possession sépulcrale, acquise des fils de Heth.

## Chapitre 24

### Abraham recherche une épouse pour Yitzhak (Isaac)

24:1	Or Abraham devint vieux et très avancé en âge. YHWH avait béni Abraham en toutes choses.
24:2	Abraham dit à son serviteur, le plus ancien des serviteurs de sa maison, l'intendant de tout ce qui lui appartenait : Mets je te prie ta main sous ma cuisse,
24:3	et je te ferai jurer par YHWH, l'Elohîm du ciel et l'Elohîm de la Terre, que tu ne prendras pas de femme pour mon fils parmi les filles des Cananéens, au milieu desquels j'habite.
24:4	Mais tu iras dans mon pays et vers mes parents, et tu y prendras une femme pour mon fils Yitzhak.
24:5	Le serviteur lui répondit : Peut-être la femme ne voudra-t-elle pas me suivre dans ce pays. Me faudra-t-il ramener, ramener ton fils dans le pays d'où tu es sorti ?
24:6	Abraham lui dit : Prête attention ! de peur d'y faire retourner mon fils.
24:7	YHWH, l'Elohîm du ciel, qui m'a fait sortir de la maison de mon père et de ma patrie, qui m'a parlé et qui m'a juré en disant : Je donnerai ce pays à ta postérité<!--Ge. 13:14-18.-->, enverra lui-même son ange devant toi. C'est là que tu prendras une femme pour mon fils.
24:8	Si la femme ne veut pas te suivre, tu seras alors quitte de ce serment. Mais ne fais pas revenir mon fils là-bas.
24:9	Le serviteur mit la main sous la cuisse d'Abraham, son seigneur, et lui jura d'observer ces choses.
24:10	Alors le serviteur prit 10 chameaux parmi les chameaux de son seigneur et s'en alla ayant en mains tous les biens de son seigneur. Il partit donc et s'en alla en Mésopotamie, à la ville de Nachor.
24:11	Il fit reposer les chameaux sur leurs genoux hors de la ville, près d'un puits d'eau, sur le soir, au temps où sortent celles qui vont puiser de l'eau.
24:12	Et il dit : Ô YHWH, Elohîm de mon seigneur Abraham, permets que je fasse aujourd'hui une rencontre et sois favorable à mon seigneur Abraham.
24:13	Voici, je me tiens près de la source d'eau, et les filles des hommes de la ville vont sortir pour puiser de l'eau.
24:14	Fais donc que la jeune fille à laquelle je dirai : Penche ta cruche je te prie afin que je boive et qui me répondra : Bois, et je donnerai aussi à boire à tes chameaux, soit celle que tu as destinée à ton serviteur Yitzhak ! Par là je saurai que tu agis avec bonté envers mon seigneur.
24:15	Il n'avait pas encore fini de parler que sortit, sa cruche sur l'épaule, Ribqah, fille de Betouel, fils de Milkah, femme de Nachor, frère d'Abraham.
24:16	Et la jeune fille était très belle de figure. Elle était vierge et aucun homme ne l'avait connue. Elle descendit donc à la source, et comme elle remontait après avoir rempli sa cruche,
24:17	le serviteur courut au-devant d'elle et lui dit : Laisse-moi boire, je te prie, un peu d'eau de ta cruche.
24:18	Elle répondit : Mon seigneur, bois. Elle s'empressa d'abaisser sa cruche sur sa main, et elle lui donna à boire.
24:19	Quand elle eut achevé de lui donner à boire, elle dit : Je puiserai aussi pour tes chameaux jusqu'à ce qu'ils aient achevé de boire.
24:20	Elle s'empressa de vider sa cruche dans l'abreuvoir et courut encore au puits pour puiser. Elle puisa pour tous ses chameaux.
24:21	L'homme la regardait fixement en silence, pour savoir si YHWH faisait réussir son voyage ou non.
24:22	Quand les chameaux eurent fini de boire, l'homme prit un anneau d'or du poids d'un demi-sicle et deux bracelets pesant 10 sicles d'or pour ses mains.
24:23	Et il lui dit : De qui es-tu fille ? Je te prie, fais-le-moi savoir. Y a-t-il dans la maison de ton père de la place pour nous loger ?
24:24	Elle lui répondit : Je suis fille de Betouel, fils de Milkah et de Nachor.
24:25	Elle lui dit encore : Il y a chez nous de la paille et du fourrage en abondance, et de la place pour loger.
24:26	Alors l'homme s'inclina et adora YHWH,
24:27	et dit : Béni soit YHWH, l'Elohîm de mon seigneur Abraham, qui n'a pas cessé d'exercer sa bonté et sa fidélité envers mon seigneur ! Lorsque j'étais en chemin, YHWH m'a conduit dans la maison des frères de mon seigneur.
24:28	La jeune fille courut et rapporta toutes ces choses à la maison de sa mère.
24:29	Ribqah avait un frère du nom de Laban qui courut dehors vers l'homme près de la source.
24:30	Il avait vu l'anneau et les bracelets aux mains de sa sœur, et il avait entendu les paroles de Ribqah sa sœur disant : Ainsi m'a parlé l'homme. Il vint donc à cet homme qui se tenait auprès des chameaux, près de la source,
24:31	et il lui dit : Entre, béni de YHWH ! Pourquoi te tiens-tu dehors ? J'ai préparé la maison et une place pour tes chameaux.
24:32	Et l'homme entra dans la maison. Laban fit décharger les chameaux et il donna de la paille et du fourrage aux chameaux. Il apporta de l'eau pour laver les pieds de l'homme et les pieds de ceux qui étaient avec lui.
24:33	Il mit en face de lui de quoi manger, mais il dit : Je ne mangerai pas avant d'avoir parlé de mon affaire. Parle ! dit Laban.
24:34	Alors il dit : Je suis serviteur d'Abraham.
24:35	YHWH a extrêmement béni mon seigneur et l'a rendu puissant. Il lui a donné des brebis, des bœufs, de l'argent, de l'or, des serviteurs, des servantes, des chameaux et des ânes.
24:36	Sarah, la femme de mon seigneur, a enfanté dans sa vieillesse un fils à mon seigneur et il lui a donné tout ce qu'il possède.
24:37	Mon seigneur m'a fait jurer en disant : Tu ne prendras pas de femme pour mon fils parmi les filles des Cananéens dans le pays desquels j'habite,
24:38	mais tu iras dans la maison de mon père et de ma famille prendre une femme pour mon fils.
24:39	J'ai dit à mon seigneur : Peut-être que la femme ne voudra pas me suivre.
24:40	Et il m'a répondu : YHWH, devant la face de qui j'ai marché, enverra son ange avec toi et fera réussir ton voyage. Tu prendras pour mon fils une femme de ma famille et de la maison de mon père.
24:41	Tu seras dégagé de ma malédiction quand tu seras arrivé dans ma famille. Si on ne te la donne pas, tu seras dégagé de ma malédiction.
24:42	Je suis arrivé aujourd'hui à la source et j'ai dit : Ô YHWH ! Elohîm de mon seigneur Abraham, si tu daignes faire réussir le voyage que j'ai entrepris,
24:43	voici, je me tiendrai près de la source d'eau. Qu'il arrive que la jeune fille qui sortira pour puiser et à qui je dirai : Laisse-moi boire je te prie un peu d'eau de ta cruche, et qui me répondra :
24:44	Bois toi-même, et je puiserai aussi pour tes chameaux, que cette jeune fille soit la femme que YHWH a destinée au fils de mon seigneur.
24:45	Avant que j'aie fini de parler en mon cœur, voici, Ribqah est sortie, ayant sa cruche sur son épaule. Elle est descendue à la source et a puisé de l'eau. Je lui ai dit : Donne-moi à boire je te prie.
24:46	Elle s'est empressée d'abaisser sa cruche de dessus son épaule et m'a dit : Bois, et même je donnerai à boire à tes chameaux. J'ai donc bu, et elle a aussi donné à boire aux chameaux.
24:47	Puis je l'ai interrogée en disant : De qui es-tu fille ? Elle a répondu : Je suis fille de Betouel, fils de Nachor et de Milkah. Alors je lui ai mis un anneau à son nez et les bracelets à ses mains.
24:48	Puis je me suis incliné, j'ai adoré YHWH, et j'ai béni YHWH, l'Elohîm de mon seigneur Abraham, qui m'a conduit fidèlement, afin que je prenne la fille du frère de mon seigneur pour son fils.
24:49	Maintenant donc, si vous voulez user de bonté et de fidélité envers mon seigneur, déclarez-le-moi. Sinon, déclarez-le-moi aussi et je me tournerai à droite ou à gauche.
24:50	Laban et Betouel répondirent et dirent : Cette affaire vient de YHWH, nous ne pouvons te parler ni en bien ni en mal.
24:51	Voici Ribqah est devant toi, prends-la et va, et qu'elle soit la femme du fils de ton seigneur, comme YHWH l'a dit.
24:52	Lorsque le serviteur d'Abraham eut entendu leurs paroles, il se prosterna à terre devant YHWH.
24:53	Et le serviteur sortit des objets d'argent et d'or et des vêtements, et les donna à Ribqah. Il donna aussi de riches présents à son frère et à sa mère.
24:54	Puis ils mangèrent et burent, lui et les gens qui étaient avec lui, et ils passèrent la nuit. Le matin, quand ils furent levés, le serviteur dit : Laissez-moi retourner vers mon seigneur.
24:55	Le frère et la mère lui dirent : Que la jeune fille reste avec nous quelques jours encore, une dizaine de jours ! Après quoi, elle s'en ira.
24:56	Il leur répondit : Ne me retardez pas puisque YHWH a fait réussir mon voyage. Laissez-moi partir afin que je m'en aille vers mon seigneur.
24:57	Alors ils dirent : Appelons la jeune fille et demandons-lui son avis.
24:58	Ils appelèrent donc Ribqah et lui dirent : Veux-tu aller avec cet homme ? Et elle répondit : J'irai !
24:59	Alors ils laissèrent leur sœur Ribqah et sa nourrice partir avec le serviteur d'Abraham et ses hommes.
24:60	Ils bénirent Ribqah et lui dirent : Tu es notre sœur, puisses-tu devenir des milliers de myriades et que ta postérité possède la porte de ses ennemis !
24:61	Alors Ribqah se leva avec ses servantes, et elles montèrent sur les chameaux et suivirent l'homme. Et le serviteur prit Ribqah et s'en alla.
24:62	Or Yitzhak était venu, il était venu du puits de Lachaï-roï et habitait dans le pays du midi.
24:63	Vers le soir, Yitzhak sortit pour méditer dans les champs, il leva les yeux et vit des chameaux qui arrivaient.
24:64	Ribqah leva aussi les yeux, vit Yitzhak et descendit de son chameau.
24:65	Elle avait dit au serviteur : Qui est cet homme qui marche dans les champs à notre rencontre ? Et le serviteur avait répondu : C'est mon seigneur. Alors elle prit son voile et se couvrit.
24:66	Le serviteur raconta à Yitzhak toutes les choses qu'il avait faites.
24:67	Alors Yitzhak conduisit Ribqah dans la tente de Sarah, sa mère. Il prit Ribqah pour sa femme<!--Pr. 18:22, 31:10-31.--> et l'aima. Ainsi Yitzhak fut consolé après la mort de sa mère.

## Chapitre 25

### Ketourah, femme d'Abraham

25:1	Or Abraham prit une autre femme du nom de Ketourah.
25:2	Elle lui enfanta Zimram, Yoqshan, Medan, Madian, Yishbaq et Shouah.
25:3	Yoqshan engendra Séba et Dedan. Les fils de Dedan furent Ashourim, Letoushim et Leoummim.
25:4	Les fils de Madian furent Éphah, Épher, Hénoc, Abida, Elda`ah. Ce sont là tous les fils de Ketourah.

### Yitzhak (Isaac) hérite d'Abraham

25:5	Abraham donna tout ce qui lui appartenait à Yitzhak.
25:6	Mais il fit des dons aux fils de ses concubines, et tandis qu'il vivait encore, il les envoya loin de son fils Yitzhak, du côté de l'orient, dans le pays d'orient.

### Mort d'Abraham

25:7	Voici les jours des années de la vie d'Abraham : Il vécut 175 ans.
25:8	Abraham expira et mourut après une heureuse vieillesse, très âgé et rassasié de jours, et il fut recueilli auprès de son peuple.
25:9	Yitzhak et Yishmael, ses fils, l'enterrèrent dans la caverne de Macpéla, dans le champ d'Éphron, fils de Tshoar, le Héthien, qui est vis-à-vis de Mamré.
25:10	C'est le champ qu'Abraham avait acheté des fils de Heth. Là furent enterrés Abraham et Sarah, sa femme.
25:11	Après la mort d'Abraham, Elohîm bénit Yitzhak, son fils. Yitzhak habitait près du puits de Lachaï-roï.

### Postérité de Yishmael (Ismaël)

25:12	Voici la postérité de Yishmael, fils d'Abraham, qu'Agar l'Égyptienne, servante de Sarah, avait enfanté à Abraham.
25:13	Voici les noms des fils de Yishmael, par leurs noms, selon leurs générations. Le premier-né de Yishmael fut Nebayoth, puis Kédar, Adbeel, Mibsam,
25:14	Mishma, Doumah, Massa,
25:15	Hadad, Théma, Yetour, Naphish, et Kedmah.
25:16	Ce sont là les fils de Yishmael et tels sont leurs noms, selon leurs villages et selon leurs campements : douze princes de leurs peuples.
25:17	Voici les années de la vie de Yishmael : 137 ans. Il expira et mourut, et il fut recueilli auprès de son peuple.
25:18	Il habita depuis Haviylah jusqu'à Shour, qui est en face de l'Égypte, en allant vers l'Assyrie. Il s'était établi en face de tous ses frères.

### Postérité de Yitzhak (Isaac)

25:19	Voici la postérité de Yitzhak, fils d'Abraham.
25:20	Abraham engendra Yitzhak. Yitzhak était âgé de 40 ans quand il épousa Ribqah, fille de Betouel, le Syrien, de Paddan-Aram, sœur de Laban, le Syrien.
25:21	Yitzhak pria instamment YHWH au sujet de sa femme parce qu'elle était stérile, et YHWH exauça ses prières ; et Ribqah, sa femme, conçut.
25:22	Mais les enfants se heurtaient dans son ventre, et elle dit : S'il en est ainsi, pourquoi suis-je enceinte ? Et elle alla consulter YHWH.
25:23	Et YHWH lui dit : Deux nations sont dans ton ventre, et deux peuples se sépareront au sortir de tes entrailles. Un de ces peuples sera plus fort que l'autre, et le plus grand servira le plus petit<!--Ro. 9:12.-->.
25:24	Les jours où elle devait accoucher s'accomplirent, et voici, il y avait des jumeaux dans son ventre.
25:25	Celui qui sortit le premier était roux, tout entier comme un manteau de poil, et on lui donna le nom d'Ésav<!--Ésaü.-->.
25:26	Ensuite sortit son frère, tenant de sa main le talon d'Ésav. C'est pourquoi on l'appela du nom de Yaacov<!--Yaacov (Jacob) : « celui qui prend par le talon » ou « qui supplante ».-->. Yitzhak était âgé de 60 ans quand ils naquirent.
25:27	Depuis, les garçons devinrent grands. Ésav devint un habile chasseur, un homme des champs, alors que Yaacov était un homme intègre, se tenant dans les tentes.
25:28	Yitzhak aimait Ésav car le gibier était sa nourriture. Mais Ribqah aimait Yaacov.

### Ésav (Ésaü) méprise son droit d'aînesse

25:29	Comme Yaacov faisait cuire du potage, Ésav arriva des champs et il était épuisé.
25:30	Et Ésav dit à Yaacov : Laisse-moi, je te prie, avaler<!--Dévorer.--> de ce roux, de ce roux-là<!--Probablement un plat de lentilles.-->, car je suis épuisé. C'est pourquoi on appela son nom Édom<!--Édom : « rouge, de couleur rousse ».-->.
25:31	Mais Yaacov lui dit : Vends-moi aujourd'hui ton droit d'aînesse.
25:32	Et Ésav répondit : Me voici sur le point de mourir, à quoi me sert ce droit d'aînesse ?
25:33	Et Yaacov dit : Jure-moi aujourd'hui. Il le lui jura, et il vendit son droit d'aînesse à Yaacov<!--Hé. 12:16.-->.
25:34	Et Yaacov donna à Ésav du pain et du potage de lentilles. Il mangea et but, puis il se leva et s'en alla. C'est ainsi qu'Ésav méprisa son droit d'aînesse.

## Chapitre 26

### YHWH confirme son alliance à Yitzhak (Isaac)

26:1	Or il y eut une famine dans le pays, outre la première famine qui eut lieu du temps d'Abraham, et Yitzhak s'en alla vers Abimélec, roi des Philistins, à Guérar.
26:2	YHWH lui apparut et lui dit : Ne descends pas en Égypte ! Demeure dans le pays que je te dirai,
26:3	demeure dans ce pays-ci. Je serai avec toi et je te bénirai, car je donnerai toutes ces contrées à toi et à ta postérité, et j'accomplirai le serment que j'ai juré à ton père Abraham.
26:4	Je multiplierai ta postérité comme les étoiles du ciel, je donnerai ces contrées à ta postérité et toutes les nations de la Terre seront bénies en ta postérité,
26:5	parce qu'Abraham a obéi à ma voix, et qu'il a gardé mon ordonnance, mes commandements, mes statuts et ma torah.

### Yitzhak (Isaac) et Ribqah (Rébecca) à Guérar<!--Ge. 20.-->

26:6	Yitzhak donc demeura à Guérar.
26:7	Et quand les hommes du lieu posaient des questions sur sa femme, il disait : C'est ma sœur. Il craignait, en effet, de dire : C'est ma femme, de peur que les habitants du lieu ne le tuent à cause de Ribqah, car elle est belle de figure.
26:8	Comme son séjour se prolongeait, il arriva qu'Abimélec, roi des Philistins, regardant par la fenêtre, vit Yitzhak qui plaisantait avec Ribqah, sa femme.
26:9	Alors Abimélec appela Yitzhak et lui dit : Voici, c'est véritablement ta femme ! Comment as-tu pu dire : C'est ma sœur ? Et Yitzhak lui répondit : Je me disais que je risquais de mourir à cause d'elle.
26:10	Et Abimélec dit : Que nous as-tu fait ? Il s'en est peu fallu que quelqu'un du peuple n'ait couché avec ta femme, et tu aurais fait venir la culpabilité sur nous.
26:11	Abimélec donc fit une ordonnance à tout le peuple en disant : Celui qui touchera à cet homme ou à sa femme, il mourra, il mourra.
26:12	Yitzhak sema dans cette terre-là et il recueillit cette année-là le centuple, car YHWH le bénit.
26:13	Cet homme devint grand et alla toujours en augmentant, jusqu'à ce qu'il devint extrêmement grand.
26:14	Il avait des troupeaux de menu bétail et des troupeaux de gros bétail, et un grand nombre de serviteurs. Les Philistins furent jaloux de lui.
26:15	Tous les puits que les serviteurs de son père avaient creusés du temps de son père Abraham, les Philistins les bouchèrent et les remplirent de terre.
26:16	Abimélec aussi dit à Yitzhak : Va-t'en de chez nous car tu es devenu beaucoup plus puissant que nous.

### Les puits de Yitzhak

26:17	Yitzhak donc partit de là et campa dans la vallée de Guérar où il s'établit.
26:18	Yitzhak creusa de nouveau les puits d'eau qu'on avait creusés du temps d'Abraham, son père, et que les Philistins avaient bouchés après la mort d'Abraham, et il leur donna les mêmes noms que son père leur avait donnés.
26:19	Les serviteurs de Yitzhak creusèrent dans cette vallée et y trouvèrent un puits d'eau vive.
26:20	Mais les bergers de Guérar eurent une querelle avec les bergers de Yitzhak, disant : L'eau est à nous. Et il appela le nom du puits Esek, parce qu'ils avaient contesté avec lui.
26:21	Ensuite, ils creusèrent un autre puits, pour lequel ils contestèrent aussi, et il appela son nom Sitna.
26:22	Alors il se transporta de là et creusa un autre puits pour lequel ils ne contestèrent pas. Il l'appela du nom de Rehoboth, en disant : C'est parce que YHWH nous a maintenant mis au large, et nous porterons du fruit dans le pays.
26:23	Et de là, il remonta à Beer-Shéba.
26:24	YHWH lui apparut cette nuit-là et lui dit : Je suis l'Elohîm d'Abraham, ton père. N'aie pas peur, car je suis avec toi, je te bénirai et je multiplierai ta postérité à cause d'Abraham, mon serviteur.
26:25	Alors il bâtit là un autel et invoqua le Nom de YHWH, et il y dressa ses tentes. Et les serviteurs de Yitzhak y creusèrent un puits.
26:26	Abimélec vint à lui de Guérar avec Ahouzath, son ami, et Picol, chef de son armée.
26:27	Mais Yitzhak leur dit : Pourquoi venez-vous vers moi puisque vous me haïssez et que vous m'avez renvoyé de chez vous ?
26:28	Ils répondirent : Nous avons vu, nous avons vu que YHWH est avec toi. C'est pourquoi nous avons dit : Qu'il y ait maintenant un serment solennel entre nous, c'est-à-dire entre nous et toi. Traitons alliance avec toi !
26:29	Jure que tu ne nous feras aucun mal, de même que nous ne t'avons pas maltraité, que nous t'avons fait seulement du bien, et que nous t'avons laissé partir en paix. Toi qui es maintenant béni de YHWH.
26:30	Alors il leur fit un festin et ils mangèrent et burent.
26:31	Ils se levèrent de bon matin et se lièrent l'un à l'autre par un serment<!--Littéralement : « Ils se lièrent l'homme à son frère par un serment ».-->. Puis Yitzhak les renvoya, et ils s'en allèrent en paix.
26:32	Ce même jour, les serviteurs de Yitzhak vinrent lui parler du puits qu'ils avaient creusé et lui dirent : Nous avons trouvé de l'eau.
26:33	Et il l'appela Shiba. C'est pourquoi le nom de la ville a été Beer-Shéba jusqu'à aujourd'hui.
26:34	Ésav, âgé de 40 ans, prit pour femmes Yehoudith, fille de Beéri, le Héthien, et Basmath, fille d'Élon, le Héthien.
26:35	Elles furent un sujet d'amertume pour l'esprit de Yitzhak et de Ribqah.

## Chapitre 27

### Yaacov (Jacob) prend la bénédiction de Yitzhak (Isaac) à la place d'Ésav (Ésaü)

27:1	Et il arriva que quand Yitzhak fut devenu vieux, et que ses yeux furent si affaiblis qu'il ne pouvait plus voir, il appela Ésav, son fils aîné, et lui dit : Mon fils ! Et il lui répondit : Me voici.
27:2	Yitzhak lui dit : Voici, maintenant je suis devenu vieux, et je ne connais pas le jour de ma mort.
27:3	Maintenant donc je te prie, prends tes armes, ton carquois et ton arc, va dans les champs et chasse-moi du gibier.
27:4	Apprête-moi un mets comme j'aime et apporte-le-moi, afin que je mange et que mon âme te bénisse avant que je meure.
27:5	Or Ribqah écoutait pendant que Yitzhak parlait à Ésav, son fils. Ésav donc s'en alla dans les champs pour chasser du gibier et pour le rapporter.
27:6	Et Ribqah parla à Yaacov, son fils, et lui dit : Voici, j'ai entendu parler ton père à Ésav, ton frère, en disant :
27:7	Apporte-moi du gibier et fais-moi un mets afin que je le mange, et je te bénirai devant YHWH avant de mourir.
27:8	Maintenant donc, mon fils, obéis à ma parole et fais ce que je vais t'ordonner.
27:9	Va maintenant à la bergerie et prends-moi là deux bons chevreaux parmi les chèvres, et j'en ferai un mets pour ton père comme il aime.
27:10	Et tu le porteras à ton père afin qu'il le mange et qu'il te bénisse avant sa mort.
27:11	Yaacov répondit à Ribqah, sa mère : Voici, Ésav, mon frère, est un homme velu, et je suis un homme sans poils.
27:12	Peut-être que mon père me palpera-t-il, et il me regardera comme un homme qui a voulu le tromper, et j'attirerai sur moi sa malédiction et non pas sa bénédiction.
27:13	Sa mère lui dit : Que ta malédiction soit sur moi, mon fils ! Écoute seulement ma voix, et va me les prendre.
27:14	Yaacov alla les prendre et les apporta à sa mère et sa mère fit un mets comme son père aimait.
27:15	Puis Ribqah prit les plus précieux habits d'Ésav, son fils aîné, qu'elle avait dans la maison, et elle les fit mettre à Yaacov, son fils cadet.
27:16	Elle couvrit ses mains et son cou, qui étaient sans poils, des peaux des chevreaux.
27:17	Puis elle mit entre les mains de son fils Yaacov le mets et le pain qu'elle avait apprêtés.
27:18	Il vint vers son père et lui dit : Mon père ! Il répondit : Me voici ! Qui es-tu, mon fils ?
27:19	Yaacov répondit à son père : Je suis Ésav, ton fils aîné. J'ai fait ce que tu m'as dit. Lève-toi je te prie, assieds-toi et mange de mon gibier, afin que ton âme me bénisse.
27:20	Yitzhak dit à son fils : Eh quoi ! Tu en as déjà trouvé mon fils ! Et il dit : YHWH, ton Elohîm, l'a fait venir devant moi.
27:21	Yitzhak dit à Yaacov : Approche-toi je te prie mon fils et que je te touche, afin que je sache si tu es mon fils Ésav ou non.
27:22	Yaacov donc s'approcha de son père Yitzhak qui le palpa et dit : Cette voix est la voix de Yaacov, mais ces mains sont les mains d'Ésav.
27:23	Et il ne le reconnut pas, car ses mains étaient velues comme les mains de son frère Ésav et il le bénit.
27:24	Il dit : C'est toi, mon fils Ésav ? Il répondit : Je le suis.
27:25	Yitzhak lui dit : Apporte-moi donc la viande et que je mange du gibier de mon fils, afin que mon âme te bénisse. Yaacov l'apporta et Yitzhak mangea ; il lui apporta aussi du vin et il but.
27:26	Puis Yitzhak, son père, lui dit : Approche-toi je te prie et embrasse-moi mon fils.
27:27	Yaacov s'approcha et l'embrassa. Il sentit l'odeur de ses habits et le bénit en disant : Voici l'odeur de mon fils, comme l'odeur d'un champ que YHWH a béni.
27:28	Qu'Elohîm te donne de la rosée du ciel et de la graisse de la Terre, du blé et du vin en abondance !
27:29	Que des peuples te servent, et que des nations se prosternent devant toi ! Sois le maître de tes frères, et que les fils de ta mère se prosternent devant toi ! Maudit soit quiconque te maudira, et béni soit quiconque te bénira.

### Déception d'Ésav<!--Hé. 12:16-17.-->

27:30	Yitzhak avait fini de bénir Yaacov, et Yaacov avait à peine quitté son père Yitzhak qu'Ésav, son frère, revint de la chasse.
27:31	Il apprêta aussi un mets, l'apporta à son père et lui dit : Que mon père se lève et mange du gibier de son fils, afin que ton âme me bénisse.
27:32	Yitzhak, son père, lui dit : Qui es-tu ? Et il dit : Je suis ton fils, ton fils aîné, Ésav.
27:33	Yitzhak fut saisi d'une grande, d'une violente émotion, et dit : Qui est donc celui qui a chassé du gibier et me l'a apporté ? J'ai mangé de tout avant que tu ne viennes, et je l'ai béni. Aussi sera-t-il béni !
27:34	Dès qu'Ésav entendit les paroles de son père, il poussa de grands cris, pleins d'amertume, et il dit à son père : Bénis-moi aussi, bénis-moi, mon père !
27:35	Mais il dit : Ton frère est venu avec tromperie, et il a enlevé ta bénédiction.
27:36	Ésav dit : N'est-ce pas avec raison qu'on a appelé son nom Yaacov ? Car il m'a déjà supplanté deux fois. Il m'a enlevé mon droit d'aînesse, et voici, maintenant il a enlevé ma bénédiction. Puis il dit : Ne m'as-tu pas réservé de bénédiction ?
27:37	Yitzhak répondit à Ésav en disant : Voici, je l'ai établi ton maître, je lui ai donné tous ses frères pour serviteurs et je l'ai pourvu de blé et de vin. Que ferai-je maintenant pour toi, mon fils ?
27:38	Ésav dit à son père : N'as-tu qu'une bénédiction, mon père ? Bénis-moi aussi, bénis-moi, mon père ! Et Ésav éleva la voix et pleura<!--Hé. 12:17.-->.
27:39	Yitzhak, son père, répondit et dit : Voici, ta demeure sera privée de la graisse de la terre, et de la rosée du ciel, d'en haut.
27:40	Tu vivras par ton épée, et tu seras asservi à ton frère. Mais il arrivera que lorsque tu seras devenu errant, tu briseras son joug de dessus ton cou.

### Fuite de Yaacov chez Laban

27:41	Et Ésav éprouva de la haine contre Yaacov, à cause de la bénédiction dont son père l'avait béni. Ésav dit en son cœur : Les jours du deuil de mon père approchent, et je tuerai Yaacov, mon frère.
27:42	On rapporta à Ribqah les paroles d'Ésav, son fils aîné. Elle fit alors appeler Yaacov, son fils cadet, et lui dit : Voici que ton frère Ésav veut te tuer pour se consoler.
27:43	Maintenant donc, mon fils, obéis à ma parole ! Lève-toi et enfuis-toi à Charan, vers Laban, mon frère.
27:44	Et reste avec lui quelque temps, jusqu'à ce que la fureur de ton frère s'apaise,
27:45	jusqu'à ce que la colère de ton frère se détourne de toi et qu'il oublie ce que tu lui as fait. Pourquoi serais-je privée de vous deux en un même jour ?
27:46	Ribqah dit à Yitzhak : Je suis dégoûtée de la vie à cause des filles de Heth. Si Yaacov prend une femme comme celles-ci, parmi les filles de Heth, parmi les filles du pays, à quoi me sert la vie ?

## Chapitre 28

### À Béth-El, YHWH confirme son alliance à Yaacov

28:1	Yitzhak donc appela Yaacov, et le bénit, et lui donna cet ordre : Tu ne prendras pas de femme parmi les filles de Canaan.
28:2	Lève-toi, va à Paddan-Aram, à la maison de Betouel, père de ta mère, et prends-toi une femme de là, parmi les filles de Laban, frère de ta mère.
28:3	El Shaddaï te bénira, il te fera porter du fruit et te multipliera, et tu deviendras une assemblée de peuples.
28:4	Qu'il te donne la bénédiction d'Abraham, à toi et à ta postérité avec toi, afin que tu obtiennes en héritage le pays où tu as été étranger, qu'Elohîm a donné à Abraham.
28:5	Yitzhak donc fit partir Yaacov, qui s'en alla à Paddan-Aram, vers Laban, fils de Betouel, le Syrien, frère de Ribqah, mère de Yaacov et d'Ésav.
28:6	Ésav vit que Yitzhak avait béni Yaacov, et qu'il l'avait envoyé à Paddan-Aram afin qu'il prenne une femme de ce pays-là pour lui, et qu'il lui avait donné cet ordre, quand il le bénissait, en disant : Ne prends pas de femme parmi les filles de Canaan.
28:7	Il vit que Yaacov avait obéi à son père et à sa mère, et qu'il était parti à Paddan-Aram.
28:8	C’est pourquoi Ésav voyant que les filles de Canaan déplaisaient aux yeux de Yitzhak, son père,
28:9	Ésav s'en alla vers Yishmael. Il prit pour femme, en plus des femmes qu'il avait, Mahalath, fille de Yishmael, fils d'Abraham, sœur de Nebayoth.
28:10	Yaacov partit de Beer-Shéba et s'en alla à Charan.
28:11	Il arriva dans un lieu où il passa la nuit, parce que le soleil était couché. Il y prit donc une pierre<!--1 Pi. 2:4. Voir commentaire en Es. 8:13-15.--> et en fit son chevet, et il se coucha dans ce lieu-là.
28:12	Alors il rêva. Et voici, une échelle était dressée sur la terre, et son sommet touchait le ciel. Il vit les anges d'Elohîm qui montaient et descendaient par cette échelle<!--Jn. 1:51.-->.
28:13	Il vit aussi YHWH qui se tenait sur l'échelle. Et il lui dit : Je suis YHWH, l'Elohîm d'Abraham, ton père, et l'Elohîm de Yitzhak. Je te donnerai à toi et à ta postérité la terre sur laquelle tu es couché.
28:14	Ta postérité sera comme la poussière de la terre, et tu t'étendras à l'occident et à l'orient, au nord et au midi, et toutes les familles de la Terre seront bénies en toi et en ta postérité<!--Ga. 3:16.-->.
28:15	Voici, je suis avec toi, je te garderai partout où tu iras et je te ramènerai vers cette terre, car je ne t'abandonnerai pas tant que je n'aurai pas accompli ce que je te dis.
28:16	Et Yaacov se réveilla de son sommeil et dit : Vraiment, YHWH est en ce lieu-ci, et moi, je ne le savais pas !
28:17	Il eut peur et dit : Que ce lieu est effrayant ! C'est ici la maison d'Elohîm, et c'est ici la porte des cieux !
28:18	Et Yaacov se leva de bon matin, prit la pierre dont il avait fait son chevet, il la dressa pour monument, et versa de l'huile sur son sommet.
28:19	Il donna à ce lieu le nom de Béth-El, mais auparavant la ville s'appelait Louz.
28:20	Yaacov fit un vœu, en disant : Si Elohîm est avec moi, et s'il me garde pendant le voyage que je fais, s'il me donne du pain à manger, et des habits pour me vêtir,
28:21	et si je retourne en paix à la maison de mon père, alors YHWH sera mon Elohîm.
28:22	Cette pierre que j'ai dressée pour monument deviendra la maison d'Elohîm et je te donnerai la dîme, la dîme de tout ce que tu me donneras<!--Voir commentaire sur la dîme en No. 18:21 et Mal. 3:10.-->.

## Chapitre 29

### Yaacov épouse Léah et Rachel chez Laban

29:1	Yaacov donc se mit en chemin, et s'en alla au pays des fils de l'orient.
29:2	Il regarda et aperçut un puits dans un champ. Et, près de ce puits, il aperçut trois troupeaux de brebis couchés, car c'était à ce puits qu'on abreuvait les troupeaux. Et il y avait une grosse pierre sur l'ouverture du puits.
29:3	Tous les troupeaux se rassemblaient là, on roulait la pierre de dessus l'ouverture du puits, on abreuvait les troupeaux et on remettait la pierre à sa place, sur l'ouverture du puits.
29:4	Yaacov leur dit : Mes frères, d'où êtes-vous ? Ils répondirent : Nous sommes de Charan.
29:5	Il leur dit : Connaissez-vous Laban, fils de Nachor ? Ils répondirent : Nous le connaissons.
29:6	Il leur dit : Est-il en paix ? Ils lui répondirent : Il est en paix, et voici Rachel, sa fille, qui vient avec le troupeau.
29:7	Il dit : Voici, il est encore grand jour, ce n'est pas le moment de rassembler les troupeaux. Donnez à boire aux brebis, puis allez les faire paître.
29:8	Ils répondirent : Nous ne le pouvons pas, jusqu'à ce que tous les troupeaux soient rassemblés et qu'on ait ôté la pierre de dessus l'ouverture du puits, afin d'abreuver les troupeaux.
29:9	Comme il parlait encore avec eux, Rachel arriva avec le troupeau de son père, car elle était bergère.
29:10	Lorsque Yaacov vit Rachel, fille de Laban, frère de sa mère, et le troupeau de Laban, frère de sa mère, il s'approcha et roula la pierre de dessus l'ouverture du puits, et abreuva le troupeau de Laban, frère de sa mère.
29:11	Et Yaacov embrassa Rachel, et il éleva sa voix et pleura.
29:12	Yaacov apprit à Rachel qu'il était frère de son père, et qu'il était fils de Ribqah. Elle courut le rapporter à son père.
29:13	Et il arriva que lorsque Laban entendit parler de Yaacov, fils de sa sœur, il courut au-devant de lui, il le prit dans ses bras et l'embrassa, et il le fit venir dans sa maison ; et Yaacov raconta à Laban tout ce qui lui était arrivé.
29:14	Et Laban lui dit : En effet, tu es mon os et ma chair. Yaacov demeura un mois entier chez Laban.
29:15	Puis Laban dit à Yaacov : Me serviras-tu pour rien parce que tu es mon frère ? Dis-moi quel sera ton salaire ?
29:16	Or Laban avait deux filles. L'aînée s'appelait Léah, et la cadette Rachel.
29:17	Léah avait les yeux délicats, mais Rachel était belle de taille et belle de figure.
29:18	Yaacov aimait Rachel, et il dit : Je te servirai 7 ans pour Rachel, ta cadette.
29:19	Et Laban répondit : Il vaut mieux que je te la donne que de la donner à un autre homme ; demeure avec moi.
29:20	Ainsi, Yaacov servit sept années pour Rachel. Elles furent à ses yeux comme quelques jours, parce qu'il l'aimait.
29:21	Et Yaacov dit à Laban : Donne-moi ma femme, car mon temps est accompli, et j'irai vers elle.
29:22	Laban réunit tous les gens du lieu et fit un festin.
29:23	Mais quand le soir fut venu, il prit Léah, sa fille, et l'amena vers Yaacov qui s'approcha d'elle.
29:24	Et Laban donna Zilpa, sa servante, à Léah, sa fille, pour servante.
29:25	Le matin arriva, et voilà que c'était Léah ! Alors Yaacov dit à Laban : Qu'est-ce que tu m'as fait ? N'ai-je pas servi chez toi pour Rachel ? Et pourquoi m'as-tu trompé ?
29:26	Laban répondit : Cela ne se fait pas chez nous, de donner la plus jeune avant l'aînée.
29:27	Achève la semaine avec celle-ci, et nous te donnerons aussi l'autre, pour le service que tu feras encore chez moi sept autres années.
29:28	C'est ce que fit Yaacov, il acheva la semaine avec Léah, et Laban lui donna aussi pour femme Rachel, sa fille.
29:29	Et Laban donna Bilhah, sa servante, à Rachel, sa fille, pour servante.
29:30	Yaacov alla aussi vers Rachel, et il aima Rachel plus que Léah, et il servit encore chez Laban sept autres années.
29:31	YHWH vit que Léah était haïe, et il ouvrit sa matrice, tandis que Rachel était stérile.

### Les enfants de Yaacov

29:32	Léah conçut et enfanta un fils à qui elle donna le nom de Reouben, car elle dit : C'est parce que YHWH a vu mon affliction, et maintenant mon mari m'aimera.
29:33	Elle conçut encore et enfanta un fils, et elle dit : Parce que YHWH a entendu que j'étais haïe, il m'a aussi donné celui-ci. Et elle lui donna le nom de Shim’ôn.
29:34	Elle conçut encore et enfanta un fils, et elle dit : Maintenant mon mari s'attachera à moi, car je lui ai enfanté trois fils. C'est pourquoi on lui donna le nom de Lévi.
29:35	Elle conçut encore et enfanta un fils, et elle dit : Cette fois je louerai YHWH. C'est pourquoi elle lui donna le nom de Yéhouda. Et elle cessa d'avoir des enfants.

## Chapitre 30

30:1	Alors Rachel, voyant qu'elle ne donnait pas d'enfants à Yaacov, elle fut jalouse de sa sœur et dit à Yaacov : Donne-moi des enfants, autrement je meurs !
30:2	Et Yaacov se mit très en colère contre Rachel, et lui dit : Suis-je à la place d'Elohîm pour t'empêcher d'avoir des enfants ?
30:3	Et elle dit : Voici ma servante Bilhah. Viens vers elle ! Qu'elle enfante sur mes genoux et que j'aie des fils par elle.
30:4	Et elle lui donna pour femme Bilhah, sa servante, et Yaacov alla vers elle.
30:5	Bilhah conçut et enfanta un fils à Yaacov.
30:6	Rachel dit : Elohîm a jugé en ma faveur, et il a aussi exaucé ma voix, et m'a donné un fils. C'est pourquoi elle l'appela du nom de Dan.
30:7	Bilhah, servante de Rachel, conçut encore et enfanta un second fils à Yaacov.
30:8	Rachel dit : J'ai lutté contre ma sœur les luttes d'Elohîm et j'ai vaincu. C'est pourquoi elle l'appela du nom de Nephthali.
30:9	Alors Léah, voyant qu'elle avait cessé de faire des enfants, prit Zilpa, sa servante, et la donna pour femme à Yaacov.
30:10	Zilpa, servante de Léah, enfanta un fils à Yaacov.
30:11	Léah dit : Le bonheur est arrivé, c'est pourquoi elle l'appela du nom de Gad.
30:12	Zilpa, servante de Léah, enfanta un second fils à Yaacov.
30:13	Léah dit : C'est pour me rendre heureuse, car les filles me diront bienheureuse. C'est pourquoi elle l'appela du nom d'Asher.
30:14	Reouben sortit au temps de la moisson des blés, trouva des mandragores<!--La mandragore, appelée pomme d'amour ou pomme du diable, est une plante qui est employée en médecine comme antidouleur et lors de contrôle ophtalmique pour dilater la pupille. Il lui est aussi attribué des effets prétendument aphrodisiaques et fertilisants. À forte dose, c'est un puissant hallucinogène, utilisé lors de rituels magiques, pour favoriser la sortie astrale des consommateurs.--> aux champs, et les apporta à Léah, sa mère ; et Rachel dit à Léah : Donne-moi, je te prie, des mandragores de ton fils.
30:15	Elle lui répondit : Est-ce peu que tu aies pris mon mari, pour que tu prennes aussi les mandragores de mon fils ? Et Rachel dit : Qu'il couche donc cette nuit avec toi pour les mandragores de ton fils.
30:16	Le soir, comme Yaacov revenait des champs, Léah sortit au-devant de lui et lui dit : Tu viendras vers moi, car je t'ai acheté pour les mandragores de mon fils. Et il coucha avec elle cette nuit-là.
30:17	Elohîm exauça Léah, et elle conçut et enfanta à Yaacov un cinquième fils.
30:18	Léah dit : Elohîm m'a récompensée, parce que j'ai donné ma servante à mon mari. C'est pourquoi elle l'appela du nom de Yissakar.
30:19	Léah conçut encore et enfanta un sixième fils à Yaacov.
30:20	Léah dit : Elohîm m'a donné un beau don. Maintenant mon mari habitera avec moi, car je lui ai enfanté six fils. C'est pourquoi elle l'appela du nom de Zebouloun.
30:21	Puis elle enfanta une fille et l'appela du nom de Diynah.
30:22	Elohîm se souvint de Rachel, il l'exauça et il ouvrit sa matrice.
30:23	Alors elle conçut et enfanta un fils, et elle dit : Elohîm a ôté mon opprobre.
30:24	Et elle lui donna le nom de Yossef, en disant : Que YHWH m'ajoute un autre fils !

### Yaacov devient de plus en plus riche

30:25	Et il arriva qu'après que Rachel eut enfanté Yossef, Yaacov dit à Laban : Laisse-moi partir, pour que je m'en aille chez moi, dans mon pays.
30:26	Donne-moi mes femmes et mes enfants, pour lesquels je t'ai servi, et je m'en irai, car tu sais de quelle manière je t'ai servi.
30:27	Et Laban lui répondit : Je te prie, si je pouvais trouver grâce à tes yeux ! J'ai appris par divination que YHWH m'a béni à cause de toi.
30:28	Il lui dit aussi : Fixe-moi le salaire que tu veux, et je te le donnerai.
30:29	Et Yaacov lui répondit : Tu sais comment je t'ai servi et ce qu'est devenu ton bétail avec moi.
30:30	Car le peu que tu avais avant moi a beaucoup augmenté et YHWH t'a béni depuis que j'ai mis les pieds chez toi. Maintenant, quand m'occuperai-je aussi de ma maison ?
30:31	Et Laban lui dit : Que te donnerai-je ? Et Yaacov répondit : Tu ne me donneras rien si tu fais pour moi ce que je vais dire. Je ferai paître encore tes troupeaux et je les garderai.
30:32	Que je passe aujourd'hui parmi tes troupeaux, et qu'on mette à part toutes les brebis tachetées et marquetées, et tous les agneaux noirs, et les chèvres marquetées et tachetées. Ce sera mon salaire.
30:33	Aujourd'hui ou demain, ma justice rendra témoignage pour moi quand elle viendra devant toi pour mon salaire : tout ce qui ne sera pas marqueté ou tacheté parmi les chèvres et noir parmi les agneaux, sera considéré comme un vol s'il est trouvé chez moi.
30:34	Et Laban dit : Voici, qu'il te soit fait comme tu l'as dit.
30:35	Et en ce même jour-là, il sépara les boucs rayés et marquetés, et toutes les chèvres tachetées et marquetées, toutes celles où il y avait du blanc, et tous les agneaux noirs. Il les remit entre les mains de ses fils.
30:36	Et il mit l'espace de trois journées de chemin entre lui et Yaacov. Et Yaacov fit paître le reste des troupeaux de Laban.
30:37	Mais Yaacov prit des branches vertes de peuplier, d'amandier et de platane. Il y pela des bandes blanches, mettant à nu le blanc qui était sur les branches.
30:38	Et il mit les branches qu'il avait pelées dans les auges, dans les abreuvoirs, sous les yeux des brebis qui venaient boire, et elles entraient en chaleur quand elles venaient boire.
30:39	Les brebis donc entraient en chaleur près des branches, et elles faisaient des brebis rayées, tachetées et marquetées.
30:40	Et Yaacov séparait les agneaux, et il mettait ensemble ce qui était rayé et tout ce qui était noir dans les troupeaux de Laban. Il se fit ainsi des troupeaux à part, qu'il ne réunit pas aux troupeaux de Laban.
30:41	Et il arrivait que toutes les fois que les brebis vigoureuses entraient en chaleur, Yaacov mettait les branches dans les auges sous les yeux des brebis, afin qu'elles entrent en chaleur près des branches.
30:42	Mais quand les brebis étaient chétives, il ne les mettait pas, de sorte que les chétives appartenaient à Laban et les vigoureuses à Yaacov.
30:43	Cet homme devint extrêmement, extrêmement riche, et il eut de nombreux troupeaux, des servantes et des serviteurs, des chameaux et des ânes.

## Chapitre 31

### YHWH dit à Yaacov de retourner au pays de ses pères

31:1	Or Yaacov entendit les discours des fils de Laban qui disaient : Yaacov a pris tout ce qui appartenait à notre père et c'est avec ce qui appartenait à notre père qu'il a acquis toute cette gloire.
31:2	Yaacov regarda le visage de Laban, et voici, il n'était plus à son égard comme hier ou avant-hier.
31:3	Alors YHWH dit à Yaacov : Retourne au pays de tes pères et vers ta parenté, et je serai avec toi.
31:4	Yaacov fit appeler Rachel et Léah qui étaient aux champs vers son troupeau,
31:5	et leur dit : Je vois au visage de votre père qu'il n'est plus envers moi comme il était hier ou avant-hier. Toutefois l'Elohîm de mon père a été avec moi.
31:6	Vous savez que j'ai servi votre père de tout mon pouvoir.
31:7	Mais votre père s'est moqué de moi et a changé dix fois mon salaire. Mais Elohîm ne lui a pas permis de me faire du mal.
31:8	Quand il disait : Les tachetées seront ton salaire, alors toutes les brebis faisaient des agneaux tachetés ; et quand il disait : Les marquetées seront ton salaire, alors toutes les brebis faisaient des agneaux marquetés.
31:9	Ainsi, Elohîm a ôté à votre père son bétail et me l'a donné.
31:10	Au temps où les brebis entraient en chaleur, je levai mes yeux et vis en rêve que les boucs qui couvraient les brebis étaient rayés, tachetés, et marquetés.
31:11	Et l'Ange d'Elohîm me dit en rêve : Yaacov ! Et je répondis : Me voici !
31:12	Il dit : Lève maintenant tes yeux et regarde : Tous les boucs qui couvrent les brebis sont rayés, tachetés et marquetés, car j'ai vu tout ce que te fait Laban.
31:13	Je suis le El de Béth-El, où tu oignis la pierre que tu dressas pour monument, où tu me fis un vœu. Maintenant lève-toi, sors de ce pays, et retourne au pays de ta naissance.
31:14	Alors Rachel et Léah lui répondirent et dirent : Avons-nous encore quelque portion et quelque héritage dans la maison de notre père ?
31:15	Ne nous a-t-il pas traitées comme des étrangères ? Car il nous a vendues, et même il a mangé, il a mangé notre argent.
31:16	Car toutes les richesses qu'Elohîm a ôtées à notre père nous appartenaient ainsi qu'à nos enfants. Maintenant donc fais tout ce qu'Elohîm t'a dit.
31:17	Ainsi, Yaacov se leva, et fit monter ses enfants et ses femmes sur des chameaux.
31:18	Il emmena tout son bétail et tous les biens qu'il avait acquis, et tout ce qu'il possédait et qu'il avait acquis à Paddan-Aram, pour aller vers Yitzhak, son père, au pays de Canaan.
31:19	Or comme Laban était allé tondre ses brebis, Rachel déroba les théraphim<!--Les théraphim étaient des idoles utilisées dans un sanctuaire de maison ou dans un lieu de culte. Voir Jg. 18:14 ; 2 R. 23:24.--> de son père.
31:20	Et Yaacov trompa l'esprit de Laban, le Syrien, en ne l'avertissant pas de sa fuite.
31:21	Il s'enfuit avec tout ce qui lui appartenait. Il se leva, passa le fleuve, et se dirigea vers la montagne de Galaad.
31:22	Le troisième jour, on rapporta à Laban que Yaacov s'était enfui.
31:23	Alors il prit avec lui ses frères, et il le poursuivit sept journées de marche, et l'atteignit à la montagne de Galaad.
31:24	Mais Elohîm apparut à Laban, le Syrien, en rêve la nuit, et lui dit : Garde-toi de parler à Yaacov ni en bien ni en mal.
31:25	Laban donc atteignit Yaacov. Yaacov avait dressé ses tentes sur la montagne. Laban dressa aussi les siennes avec ses frères sur la montagne de Galaad.
31:26	Et Laban dit à Yaacov : Qu'as-tu fait ? Tu as trompé mon esprit, tu as emmené mes filles comme des prisonnières de guerre.
31:27	Pourquoi as-tu pris la fuite secrètement, m'as-tu trompé et ne m'as-tu pas averti ? Car je t'aurais laissé partir avec joie et avec des chansons, au son des tambours et des harpes.
31:28	Tu ne m'as pas laissé embrasser mes fils et mes filles. C'est en insensé que tu as agi.
31:29	J'ai en main le pouvoir de vous faire du mal, mais l'Elohîm de votre père m'a parlé la nuit passée et m'a dit : Garde-toi de ne parler à Yaacov ni en bien ni en mal.
31:30	Maintenant que tu es parti, parce que tu languissais après la maison de ton père, pourquoi as-tu dérobé mes elohîm ?
31:31	Yaacov répondit, et dit à Laban : C'est que j'avais peur. Car je me disais que tu me prendrais peut-être tes filles.
31:32	Mais celui chez qui tu trouveras tes elohîm ne vivra pas. En présence de nos frères, examine s'il y a chez moi quelque chose qui t'appartienne, et prends-le. Or Yaacov ignorait que Rachel les avait dérobés.
31:33	Alors Laban entra dans la tente de Yaacov, et dans celle de Léah, et dans la tente des deux servantes, et il ne les trouva pas. Et étant sorti de la tente de Léah, il entra dans la tente de Rachel.
31:34	Mais Rachel avait pris les théraphim et les avait mis dans le bât d'un chameau, et s'était assise dessus. Laban palpa toute la tente et ne les trouva pas.
31:35	Elle dit à son père : Que mon seigneur ne se fâche pas de ce que je ne puis me lever devant lui, car j'ai ce que les femmes ont coutume d'avoir. Il chercha et ne trouva pas les théraphim.
31:36	Yaacov se fâcha et querella Laban. Il reprit la parole et lui dit : Quelle est ma transgression ? Quel est mon péché, pour que tu me poursuives avec tant d'ardeur ?
31:37	Car tu as palpé tous mes objets, qu'as-tu trouvé des objets de ta maison ? Mets-les ici devant mes frères et les tiens, et qu'ils soient juges entre nous deux.
31:38	Voilà 20 ans que j'ai passés chez toi. Tes brebis et tes chèvres n'ont pas avorté, je n'ai pas mangé les moutons de tes troupeaux.
31:39	Je ne t'ai pas rapporté de bêtes déchirées, j'en ai moi-même subi la perte. Et tu redemandais de ma main ce qui avait été dérobé de jour et ce qui avait été dérobé de nuit.
31:40	Le jour la chaleur me consumait, et la nuit le froid, et le sommeil fuyait de mes yeux.
31:41	Voilà 20 ans que j'ai passés dans ta maison, 14 ans pour tes deux filles, et 6 ans pour tes troupeaux, et tu m'as changé dix fois mon salaire.
31:42	Si je n'avais pas eu pour moi l'Elohîm de mon père, l'Elohîm d'Abraham, et celui que craint Yitzhak, certes tu m'aurais maintenant renvoyé à vide. Mais Elohîm a regardé mon affliction et le travail de mes mains, et il t'a repris la nuit passée.
31:43	Laban répondit à Yaacov, et dit : Ces filles sont mes filles, et ces enfants sont mes enfants, et ces troupeaux sont mes troupeaux, et tout ce que tu vois est à moi ; et que ferais-je aujourd'hui à mes filles et aux enfants qu'elles ont enfantés ?
31:44	Maintenant donc, viens, faisons ensemble une alliance, et qu'elle serve de témoignage entre moi et toi.
31:45	Yaacov prit une pierre et il la dressa pour monument.
31:46	Yaacov dit à ses frères : Ramassez des pierres. Et ils prirent des pierres et ils en firent un monceau, et ils mangèrent là sur ce monceau.
31:47	Laban l'appela Yegar-Sahadouta, et Yaacov l'appela Galed.
31:48	Et Laban dit : Ce monceau sera aujourd'hui témoin entre moi et toi. C'est pourquoi on lui donna le nom de Galed<!--Poste d'observation.-->.
31:49	On l'appela aussi Mitspa, parce qu'il avait dit : Que YHWH veille sur moi et sur toi, quand nous nous serons l'un et l'autre perdus de vue.
31:50	Si tu maltraites mes filles et si tu prends une autre femme que mes filles, ce n'est pas un homme qui sera avec nous, fais-y bien attention ! C'est Elohîm qui sera témoin entre toi et moi.
31:51	Laban dit encore à Yaacov : Regarde ce monceau, et considère le monument que j'ai dressé entre moi et toi.
31:52	Que ce monceau soit témoin et que ce monument soit témoin que je n'irai pas vers toi au-delà de ce monceau, et que tu ne viendras pas vers moi au-delà de ce monceau et de ce monument pour me faire du mal !
31:53	Que l'Elohîm d'Abraham et l'Elohîm de Nachor, l'Elohîm de leur père, juge entre nous ! Mais Yaacov jura par celui que craignait Yitzhak, son père.
31:54	Yaacov offrit un sacrifice sur la montagne et invita ses frères pour manger du pain. Ils mangèrent donc du pain et passèrent la nuit sur la montagne.
31:55	Laban se leva de bon matin, embrassa ses fils et ses filles, et les bénit. Ensuite il s'en alla. Ainsi Laban retourna chez lui.

## Chapitre 32

### Yaacov (Jacob) devient Israël

32:1	Et Yaacov continua son chemin, et des anges d'Elohîm le rencontrèrent.
32:2	En les voyant, Yaacov dit : C'est ici le camp d'Elohîm ! Et il donna à ce lieu le nom de Mahanaïm.
32:3	Yaacov envoya devant lui des messagers vers Ésav, son frère, au pays de Séir, dans le territoire d'Édom.
32:4	Il leur donna cet ordre : Vous parlerez de cette manière à mon seigneur Ésav : Ainsi a dit ton serviteur Yaacov : J'ai séjourné comme étranger chez Laban, et j'y ai habité jusqu'à présent.
32:5	J'ai des bœufs, des ânes, des brebis, des serviteurs, et des servantes, et j'envoie l'annoncer à mon seigneur, afin de trouver grâce à tes yeux.
32:6	Et les messagers revinrent auprès de Yaacov et lui dirent : Nous sommes allés vers ton frère Ésav, et il marche aussi à ta rencontre avec 400 hommes.
32:7	Alors Yaacov eut très peur et l’angoisse le saisit. Il partagea le peuple qui était avec lui, les brebis, les bœufs et les chameaux en deux camps et il dit :
32:8	Si Ésav attaque l'un des camps et le frappe, le camp qui restera pourra s'échapper.
32:9	Yaacov dit aussi : Ô Elohîm de mon père Abraham, Elohîm de mon père Yitzhak, ô YHWH qui m'as dit : Retourne dans ton pays, vers ta parenté, et je te ferai du bien.
32:10	Je suis trop petit pour toutes les faveurs et pour toute la fidélité dont tu as usé envers ton serviteur, car j'ai passé ce Yarden avec mon bâton, et maintenant je forme deux camps.
32:11	Je te prie, délivre-moi de la main de mon frère Ésav ! Car je crains qu'il ne vienne, et qu'il ne me frappe, avec la mère et les enfants.
32:12	Et toi, tu as dit : Certes, je te ferai du bien, et je rendrai ta postérité comme le sable de la mer, si abondant qu'on ne saurait le compter.
32:13	C'est dans ce lieu-là que Yaacov passa la nuit. Il prit de ce qu'il avait sous la main pour faire un présent à Ésav, son frère :
32:14	200 chèvres, 20 boucs, 200 brebis et 20 béliers.
32:15	30 femelles de chameaux qui allaitaient, et leurs petits, 40 jeunes vaches, 10 jeunes taureaux, 20 ânesses et 10 ânes.
32:16	Il les mit entre les mains de ses serviteurs, chaque troupeau à part, et leur dit : Passez devant moi, et faites qu'il y ait un intervalle entre chaque troupeau.
32:17	Il donna cet ordre au premier, disant : Quand Ésav, mon frère, te rencontrera et te demandera, disant : À qui es-tu et où vas-tu ? Et à qui sont ces choses qui sont devant toi ?
32:18	Alors tu diras : Je suis à ton serviteur Yaacov, c'est un présent qu'il envoie à mon seigneur Ésav. Et voici, il vient lui-même derrière nous.
32:19	Il donna le même ordre au deuxième, au troisième, et à tous ceux qui suivaient les troupeaux, disant : C'est ainsi que vous parlerez à mon seigneur Ésav, quand vous le rencontrerez.
32:20	Vous lui direz : Voici, ton serviteur Yaacov vient aussi derrière nous. Car il se disait : J'apaiserai sa colère par ce présent qui va devant moi. Après cela, je verrai sa face et peut-être qu'il me regardera favorablement.
32:21	Le présent passa devant lui, mais il resta cette nuit-là dans le camp.
32:22	Il se leva cette nuit, et prit ses deux femmes, ses deux servantes, et ses onze enfants, et passa le gué de Yabboq.
32:23	Il les prit donc, et leur fit passer le torrent, il fit aussi passer tout ce qu'il avait.
32:24	Yaacov demeura seul. Alors un homme lutta avec lui jusqu'au lever de l'aurore.
32:25	Voyant qu'il ne pouvait pas le vaincre, il le frappa à l'intérieur de la cuisse. Ainsi l'intérieur de la cuisse de Yaacov se disloqua pendant qu'il luttait avec lui.
32:26	Et cet homme lui dit : Laisse-moi, car l'aurore se lève. Mais il dit : Je ne te laisserai pas partir sans que tu m'aies béni.
32:27	Cet homme lui dit : Quel est ton nom ? Il répondit : Yaacov.
32:28	Alors il dit : On ne t'appellera plus du nom de Yaacov, mais Israël, car tu as lutté avec Elohîm et avec les hommes, et tu as vaincu.
32:29	Yaacov l'interrogea, en disant : Je te prie, fais-moi connaître ton Nom. Et il répondit : Pourquoi demandes-tu mon Nom ? Et il le bénit là<!--Jg. 13:18.-->.
32:30	Yaacov appela ce lieu du nom de Peniel, car, dit-il, j'ai vu Elohîm face à face, et mon âme a été délivrée.
32:31	Le soleil se levait lorsqu'il passa Peniel. Yaacov boitait à cause de sa cuisse.
32:32	C'est pourquoi, jusqu'à ce jour, les enfants d'Israël ne mangent pas le tendon qui est à l'intérieur de la cuisse, parce qu'il avait frappé Yaacov à l'intérieur de la cuisse, au tendon.

## Chapitre 33

### Yaacov demande pardon à son frère Ésav

33:1	Et Yaacov leva ses yeux et regarda. Et voici, Ésav arrivait avec 400 hommes. Alors il partagea les enfants entre Léah, Rachel et les deux servantes.
33:2	Il plaça en tête les servantes avec leurs enfants, puis Léah et ses enfants, enfin Rachel et Yossef au dernier rang.
33:3	Quant à lui, il passa devant eux et se prosterna à terre sept fois, jusqu'à ce qu'il soit près de son frère.
33:4	Ésav courut à sa rencontre, le prit dans ses bras, se jeta à son cou et l'embrassa. Et ils pleurèrent.
33:5	Ésav leva ses yeux, vit les femmes et les enfants, et dit : Qui sont ceux-là ? Sont-ils à toi ? Yaacov lui répondit : Ce sont les enfants qu'Elohîm, par sa grâce, a donnés à ton serviteur.
33:6	Les servantes s'approchèrent, elles et leurs enfants, et se prosternèrent.
33:7	Puis Léah aussi s'approcha avec ses enfants, et ils se prosternèrent, et ensuite Yossef et Rachel s'approchèrent et se prosternèrent aussi.
33:8	Ésav dit : Que veux-tu faire avec tout ce camp que j'ai rencontré ? Et Yaacov répondit : C'est pour trouver grâce aux yeux de mon seigneur.
33:9	Ésav dit : Je suis dans l'abondance, mon frère. Garde ce qui est à toi.
33:10	Et Yaacov répondit : Non, je te prie, si j'ai maintenant trouvé grâce à tes yeux, reçois ce présent de ma main ! Parce que j'ai vu ta face comme si j'avais vu la face d'Elohîm, et parce que tu m'as accueilli favorablement.
33:11	Accepte, je te prie, mon présent qui t'a été offert, car Elohîm m'a comblé de grâce, et je ne manque de rien. Il le pressa tellement qu'il le prit.
33:12	Ésav dit : Partons et marchons, et je marcherai devant toi.
33:13	Mais Yaacov lui dit : Mon seigneur sait que ces enfants sont jeunes et que j'ai des brebis et des vaches qui allaitent. Si l'on forçait leur marche un seul jour, tout le troupeau mourra.
33:14	Je te prie que mon seigneur passe devant son serviteur, et je m'avancerai tout doucement, au pas de ce bétail qui est devant moi, et au pas de ces enfants, jusqu'à ce que j'arrive chez mon seigneur à Séir.
33:15	Ésav dit : Je te prie, je vais au moins laisser avec toi une partie de ce peuple qui est avec moi. Et il répondit : Pourquoi cela ? Que je trouve grâce aux yeux de mon seigneur !
33:16	Ainsi Ésav retourna ce jour-là par son chemin à Séir.

### Yaacov dresse un autel à El-Élohé-Israël

33:17	Yaacov partit pour Soukkoth. Il bâtit une maison pour lui, et il fit des cabanes pour son bétail. C'est pourquoi il appela ce lieu du nom de Soukkoth.
33:18	À son retour de Paddan-Aram, Yaacov arriva sain et sauf à la ville de Sichem, dans le pays de Canaan, et il campa devant la ville.
33:19	Il acheta une portion du champ où il avait dressé sa tente de la main des fils de Hamor, père de Sichem, pour 100 pièces d'argent.
33:20	Et là, il dressa un autel qu'il appela El-Élohé-Israël<!--Le El Fort, le El d'Israël.-->.

## Chapitre 34

### Déshonneur de Diynah et vengeance de ses frères

34:1	Or Diynah, la fille que Léah avait enfantée à Yaacov, sortit pour voir les filles du pays.
34:2	Elle fut aperçue de Sichem, fils de Hamor, le Hévien, prince du pays. Il l'enleva et coucha avec elle, et la déshonora.
34:3	Son âme fut attachée à Diynah, fille de Yaacov. Il aima la jeune fille et parla au cœur de la jeune fille.
34:4	Et Sichem parla à Hamor, son père, en disant : Prends-moi cette fille pour femme.
34:5	Yaacov apprit qu'il avait souillé Diynah, sa fille. Or ses fils étaient avec son bétail aux champs. Yaacov garda le silence jusqu'à leur retour.
34:6	Hamor, père de Sichem, sortit vers Yaacov pour lui parler.
34:7	Et les fils de Yaacov revinrent des champs dès qu'ils apprirent ce qui était arrivé. Ces hommes furent affligés et très fâchés à cause de l'infamie qu'il avait commise contre Israël, en couchant avec la fille de Yaacov, ce qui ne devait pas se faire.
34:8	Hamor leur parla, en disant : L'âme de Sichem, mon fils, s'est attachée à votre fille. Donnez-la-lui je vous prie pour femme.
34:9	Alliez-vous avec nous, vous nous donnerez vos filles, et vous prendrez pour vous les nôtres.
34:10	Vous habiterez avec nous, et le pays sera à votre disposition. Restez pour y trafiquer et y acquérir des possessions.
34:11	Sichem dit aussi au père et aux frères de la fille : Que je trouve grâce à vos yeux, et je donnerai tout ce que vous me direz.
34:12	Exigez de moi une forte dot et beaucoup de présents, et je donnerai ce que vous me direz, mais donnez-moi la jeune fille pour femme.
34:13	Alors les fils de Yaacov répondirent avec ruse à Sichem et à Hamor, son père. Ils parlèrent ainsi parce que Sichem avait souillé Diynah, leur sœur.
34:14	Ils leur dirent : C'est une chose que nous ne pouvons pas faire, que de donner notre sœur à un homme incirconcis, car ce serait un opprobre pour nous.
34:15	Mais nous ne consentirons à ce que vous demandez que si vous deveniez semblables à nous en circoncisant tous les mâles qui sont parmi vous.
34:16	Alors nous vous donnerons nos filles, et nous prendrons vos filles pour nous, et nous habiterons avec vous, et nous ne serons qu'un seul peuple.
34:17	Mais si vous ne voulez pas nous écouter et vous circoncire, nous prendrons notre fille et nous nous en irons.
34:18	Leurs discours parurent bons aux yeux de Hamor et aux yeux de Sichem, fils de Hamor.
34:19	Le jeune homme ne tarda pas à faire ce qu'on lui avait proposé car la fille de Yaacov lui plaisait beaucoup. Il était le plus considéré de tous ceux de la maison de son père.
34:20	Hamor et Sichem son fils, se rendirent à la porte de leur ville et parlèrent aux hommes de leur ville en leur disant :
34:21	Ces hommes sont paisibles à notre égard. Qu'ils habitent dans le pays et qu'ils y trafiquent ! Car voici, le pays est assez vaste pour eux. Nous prendrons pour femmes leurs filles et nous leur donnerons nos filles.
34:22	Mais ces hommes ne consentiront à habiter avec nous pour former un seul peuple, que si tout mâle qui est parmi nous est circoncis, comme ils sont eux-mêmes circoncis.
34:23	Leur bétail, leurs biens et toutes leurs bêtes ne seront-ils pas à nous ? Accordons-leur seulement cela, et qu'ils demeurent avec nous.
34:24	Tous ceux qui sortaient par la porte de leur ville obéirent à Hamor et à Sichem son fils. Et tout mâle d'entre tous ceux qui sortaient par la porte de leur ville fut circoncis.
34:25	Mais il arriva, au troisième jour, quand ils étaient dans la douleur, que deux des fils de Yaacov, Shim’ôn et Lévi, frères de Diynah, prirent leurs épées, entrèrent hardiment dans la ville et tuèrent tous les mâles.
34:26	Ils passèrent aussi au tranchant de l'épée Hamor et Sichem son fils. Ils enlevèrent Diynah de la maison de Sichem et sortirent.
34:27	Les fils de Yaacov se jetèrent sur les tués et pillèrent la ville, parce qu'on avait souillé leur sœur.
34:28	Ils prirent leurs troupeaux, leurs bœufs, leurs ânes et ce qui était dans la ville et dans les champs.
34:29	Ils emmenèrent et pillèrent toutes leurs richesses, tous leurs petits enfants et leurs femmes, ainsi que tout ce qui était dans les maisons.
34:30	Alors Yaacov dit à Shim’ôn et Lévi : Vous m'avez troublé en me rendant odieux aux habitants du pays, aux Cananéens et aux Phéréziens, et je n'ai qu'un petit nombre d'hommes ; ils se rassembleront contre moi, me frapperont et me détruiront, moi et ma maison.
34:31	Ils répondirent : Doit-on traiter notre sœur comme une prostituée ?

## Chapitre 35

### Yaacov revient à Béth-El pour adorer YHWH

35:1	Or Elohîm dit à Yaacov : Lève-toi, monte à Béth-El et demeures-y. Tu y dresseras un autel au El qui t'apparut lorsque tu fuyais Ésav, ton frère.
35:2	Yaacov dit à sa famille et à tous ceux qui étaient avec lui : Ôtez les elohîm des étrangers qui sont au milieu de vous, purifiez-vous et changez de vêtements<!--Jos. 24:23.-->.
35:3	Levons-nous et montons à Béth-El. Là, je dresserai un autel au El qui m'a exaucé dans le jour de ma détresse et qui a été avec moi dans le chemin où j'ai marché.
35:4	Alors ils donnèrent à Yaacov tous les elohîm des étrangers qui étaient entre leurs mains et les anneaux qui étaient à leurs oreilles. Yaacov les cacha sous un térébinthe qui est près de Sichem.
35:5	Puis ils partirent. Et Elohîm frappa de terreur les villes qui les entouraient et l'on ne poursuivit pas les fils de Yaacov.
35:6	Ainsi, Yaacov et tout le peuple qui était avec lui, arrivèrent à Louz qui est Béth-El, dans le pays de Canaan.
35:7	Il bâtit là un autel et il appela cet endroit El-Béth-El<!--El de la maison de El.-->, car c'est là qu'Elohîm s'était révélé à lui lorsqu'il fuyait son frère.
35:8	Déborah, nourrice de Ribqah, mourut. Elle fut ensevelie au-dessous de Béth-El, sous le chêne auquel on a donné le nom de Chêne des pleurs.
35:9	Elohîm apparut encore à Yaacov, après son retour de Paddan-Aram, et il le bénit<!--Os. 12:5.-->.
35:10	Elohîm lui dit : Ton nom est Yaacov, mais tu ne seras plus appelé Yaacov, car ton nom sera Israël. Et il lui donna le nom d'Israël.
35:11	Elohîm lui dit aussi : Je suis El Shaddaï ! Porte du fruit et multiplie-toi ! Une nation, une assemblée de nations naîtra de toi et des rois sortiront de tes reins.
35:12	Je te donnerai la terre que j'ai donnée à Abraham et à Yitzhak, et je la donnerai à ta postérité après toi.
35:13	Elohîm s'éleva au-dessus de lui à l'endroit où il lui avait parlé.
35:14	Et Yaacov dressa un monument à l'endroit où il lui avait parlé, un monument de pierres sur lequel il fit une libation et versa de l'huile.
35:15	Yaacov donna le nom de Béth-El à l'endroit où Elohîm lui avait parlé.
35:16	Puis ils partirent de Béth-El, et il y avait encore une certaine distance jusqu'à Éphrata<!--Éphrata : « lieu de la fécondité ».--> lorsque Rachel accoucha. Elle eut un accouchement difficile.
35:17	Et comme elle avait beaucoup de peine à accoucher, la sage-femme lui dit : N'aie pas peur, car tu as encore un fils.
35:18	Et comme son âme sortait, car elle était mourante, elle lui donna le nom de Ben-Oni<!--Ben-Oni : « fils de ma douleur ».-->, mais son père l'appela Benyamin<!--Benyamin : « fils de ma main droite », « fils de félicité ».-->.
35:19	C'est ainsi que mourut Rachel et elle fut ensevelie sur le chemin d'Éphrata, qui est Bethléhem.
35:20	Yaacov dressa un monument sur son sépulcre. C'est le monument du sépulcre de Rachel, aujourd'hui encore.
35:21	Puis Israël partit et dressa ses tentes au-delà de Migdal-Éder.
35:22	Et il arriva que, quand Israël habitait dans ce pays-là, Reouben vint et coucha avec Bilhah, concubine de son père. Et Israël l'apprit. Or Yaacov avait 12 fils.
35:23	Les fils de Léah étaient Reouben, premier-né de Yaacov, Shim’ôn, Lévi, Yéhouda, Yissakar et Zebouloun.
35:24	Les fils de Rachel : Yossef et Benyamin.
35:25	Les fils de Bilhah, servante de Rachel : Dan et Nephthali.
35:26	Les fils de Zilpa, servante de Léah : Gad et Asher. Ce sont là les enfants de Yaacov qui lui naquirent à Paddan-Aram.

### Yaacov va vers son père Yitzhak (Isaac) avant sa mort

35:27	Yaacov arriva auprès de Yitzhak, son père, à la plaine de Mamré, à Qiryath-Arba qui est Hébron, où Abraham et Yitzhak avaient séjourné comme étrangers.
35:28	Les jours de Yitzhak furent de 180 ans.
35:29	Yitzhak expira et mourut, et fut recueilli auprès de son peuple, âgé et rassasié de jours, et Ésav et Yaacov ses fils l'ensevelirent.

## Chapitre 36

### Postérité d'Ésav

36:1	Et voici la postérité d'Ésav, qui est Édom.
36:2	Ésav prit ses femmes parmi les filles de Canaan : Adah, fille d'Élon, le Héthien, Oholiybamah fille d'Anah, fille de Tsibeon, le Hévien.
36:3	Il prit aussi Basmath, fille de Yishmael, sœur de Nebayoth.
36:4	Adah enfanta à Ésav Éliphaz, et Basmath enfanta Reouel.
36:5	Et Oholiybamah enfanta Yéoush, Ya`lam et Koré. Ce sont là les fils d'Ésav qui lui naquirent dans le pays de Canaan.
36:6	Ésav prit ses femmes, ses fils et ses filles, et toutes les personnes de sa maison, tous ses troupeaux, ses bêtes et tout le bien qu'il avait acquis dans le pays de Canaan, et il s'en alla dans un autre pays, loin de Yaacov, son frère.
36:7	Car leurs richesses étaient si grandes qu'ils n'auraient pas pu demeurer ensemble et le pays où ils séjournaient comme étrangers ne pouvait plus les contenir à cause de leurs troupeaux.
36:8	Ainsi, Ésav habita dans la montagne de Séir. Ésav, c'est Édom.
36:9	Voici la postérité d'Ésav, père d'Édom, dans la montagne de Séir.
36:10	Voici les noms des fils d'Ésav : Éliphaz fils d'Adah, femme d'Ésav ; Reouel, fils de Basmath, femme d'Ésav.
36:11	Les fils d'Éliphaz furent : Théman, Omar, Tsepho, Gaetham et Kenaz.
36:12	Et Timna était la concubine d'Éliphaz, fils d'Ésav, et elle enfanta à Éliphaz Amalek. Ce sont là les fils d'Adah, femme d'Ésav.
36:13	Voici les fils de Reouel : Nahath, Zérach, Shammah et Mizzah. Ce sont là les fils de Basmath, femme d'Ésav.
36:14	Voici les fils d'Oholiybamah, fille d'Anah, petite fille de Tsibeon, femme d'Ésav : elle enfanta à Ésav Yéoush, Ya`lam et Koré.
36:15	Voici les chefs des fils d'Ésav. Voici les fils d'Éliphaz, premier-né d'Ésav, le chef Théman, le chef Omar, le chef Tsepho, le chef Kenaz,
36:16	le chef Koré, le chef Gaetham, le chef Amalek. Ce sont là les chefs d'Éliphaz dans le pays d'Édom. Ce sont les fils d'Ada.
36:17	Voici les fils de Reouel, fils d'Ésav : le chef Nahath, le chef Zérach, le chef Shammah et le chef Mizzah. Ce sont là les chefs sortis de Reouel, dans le pays d'Édom. Ce sont là les fils de Basmath, femme d'Ésav.
36:18	Voici les fils d'Oholiybamah, femme d'Ésav : Le chef Yéoush, le chef Ya`lam, le chef Koré. Ce sont là les chefs sortis d'Oholiybamah, fille d'Anah, femme d'Ésav.
36:19	Ce sont là les fils d'Ésav, qui est Édom, et ce sont là leurs chefs.
36:20	Voici les fils de Séir, le Horien, qui avaient habité dans le pays : Lothan, Shobal, Tsibeon, Anah,
36:21	Dishon, Etser et Dishan. Ce sont là les chefs des Horiens, fils de Séir, dans le pays d'Édom.
36:22	Les fils de Lothan furent Hori et Héman. Et Thimna était sœur de Lothan.
36:23	Voici les fils de Shobal : Alvan, Manahath, Ébal, Shepho et Onam.
36:24	Voici les fils de Tsibeon : Ayah et Anah. C'est cet Anah qui trouva les sources chaudes dans le désert, quand il faisait paître les ânes de Tsibeon, son père.
36:25	Voici les fils d'Anah : Dishon et Oholiybamah, fille d'Anah.
36:26	Voici les fils de Dishon : Hemdan, Eshban, Yithran et Keran.
36:27	Voici les fils d'Etser : Bilhan, Zaavan et Akan.
36:28	Voici les fils de Dishan : Outs et Aran.
36:29	Voici les chefs des Horiens : Le chef Lothan, le chef Shobal, le chef Tsibeon, le chef Anah,
36:30	le chef Dishon, le chef Etser, le chef Dishan. Ce sont là les chefs des Horiens, les chefs qu'ils établirent dans le pays de Séir.
36:31	Voici les rois qui ont régné dans le pays d'Édom avant qu'un roi règne sur les enfants d'Israël.
36:32	Béla, fils de Béor, régna sur Édom, et le nom de sa ville était Dinhabah.
36:33	Béla mourut et Yobab, fils de Zérach de Botsrah, régna à sa place.
36:34	Yobab mourut et Housham, du pays des Thémanites, régna à sa place.
36:35	Housham mourut et Hadad, fils de Bédad, régna à sa place. C'est lui qui frappa Madian dans le territoire de Moab. Le nom de sa ville était Avith.
36:36	Hadad mourut et Samlah, de Masreqah, régna à sa place.
36:37	Samlah mourut et Shaoul de Réhoboth sur le fleuve, régna à sa place.
36:38	Shaoul mourut et Baal-Hanan, fils d'Acbor, régna à sa place.
36:39	Baal-Hanan, fils d'Acbor mourut et Hadar régna à sa place. Le nom de sa ville était Paou, et le nom de sa femme Mehéthabeel. Elle était la fille de Mathred et la petite-fille de Mézahab.
36:40	Voici les noms des chefs d'Ésav selon leurs familles, selon leurs territoires et d'après leurs noms : le chef Thimna, le chef Alvah, le chef Yetheyh,
36:41	le chef Oholiybamah, le chef Élah, le chef Pinon,
36:42	le chef Kenaz, le chef Théman, le chef Mibtsar,
36:43	le chef Magdiel et le chef Iram. Ce sont là les chefs d'Édom, selon leurs habitations dans le pays qu'ils possédaient. C'est Ésav le père d'Édom.

## Chapitre 37

### Yossef, aimé de son père et haï par ses frères

37:1	Or Yaacov demeura dans le pays de Canaan, pays où avait séjourné son père comme étranger.
37:2	Voici la postérité de Yaacov. Yossef, âgé de 17 ans, faisait paître le troupeau avec ses frères. Ce garçon était auprès des fils de Bilhah et des fils de Zilpa, femmes de son père. Et Yossef rapportait à leur père leurs mauvais propos.
37:3	Or Israël aimait Yossef plus que tous ses fils, parce qu'il était le fils de sa vieillesse, et il lui fit une tunique de plusieurs couleurs.
37:4	Et ses frères, voyant que leur père l'aimait plus qu'eux tous, le haïssaient et ne pouvaient lui parler avec douceur.

### Les rêves de Yossef

37:5	Yossef rêva un rêve, et il le raconta à ses frères qui le haïrent encore davantage.
37:6	Il leur dit donc : Écoutez, je vous prie, ce rêve que j'ai rêvé.
37:7	Voici, nous étions à lier des gerbes au milieu d'un champ, et voici que ma gerbe se leva et se tint droite. Et voici, vos gerbes l'entourèrent et se prosternèrent devant elle.
37:8	Alors ses frères lui dirent : Régnerais-tu sur nous ? Et dominerais-tu sur nous ? Et ils le haïrent encore plus pour ses rêves et pour ses paroles.
37:9	Il rêva encore un autre rêve et il le raconta à ses frères, en disant : Voici, j'ai encore rêvé un rêve. Et voici, le soleil, la lune et onze étoiles se prosternaient devant moi<!--Ap. 12:1.-->.
37:10	Il le raconta à son père et à ses frères. Son père le réprimanda et lui dit : Que signifie ce rêve que tu as rêvé ? Est-ce que nous viendrons, nous viendrons, moi, ta mère, tes frères, pour nous prosterner à terre devant toi ?
37:11	Ses frères devinrent jaloux de lui, mais son père retenait ses discours<!--Ac. 7:9.-->.
37:12	Les frères de Yossef s'en allèrent paître les troupeaux de leur père à Sichem.
37:13	Israël dit à Yossef : Tes frères ne font-ils pas paître le troupeau à Sichem ? Viens, que je t'envoie vers eux. Et il lui répondit : Me voici !
37:14	Israël lui dit : Va, je te prie, et vois si tes frères sont en paix et si le troupeau est en paix, et rapporte-le-moi. Il l'envoya ainsi de la vallée d'Hébron, et il alla à Sichem.
37:15	Un homme le rencontra errant dans les champs et cet homme lui demanda en disant : Que cherches-tu ?
37:16	Yossef répondit : Je cherche mes frères. Je t'en prie, dis-moi où ils font paître leur troupeau.
37:17	Et l'homme dit : Ils sont partis d'ici, et j'ai entendu qu'ils disaient : Allons à Dothan. Yossef alla après ses frères et les trouva à Dothan.

### Les frères de Yossef complotent contre lui

37:18	Ils le virent de loin et, avant qu'il soit près d'eux, ils complotèrent contre lui pour le tuer.
37:19	Et chacun dit à son frère : Voici le maître des rêves qui vient !
37:20	Maintenant, venez, tuons-le et jetons-le dans l'une de ces citernes. Nous dirons qu'une bête féroce l'a dévoré, et nous verrons ce que deviendront ses rêves.
37:21	Mais Reouben entendit cela et le délivra de leurs mains, en disant : Ne lui ôtons pas la vie.
37:22	Reouben leur dit encore : Ne répandez pas le sang ! Jetez-le dans cette citerne qui est dans le désert, mais ne mettez pas la main sur lui. C'était pour le délivrer de leurs mains et le renvoyer à son père.
37:23	Lorsque Yossef fut arrivé auprès de ses frères, ils le dépouillèrent de sa tunique, de cette tunique de plusieurs couleurs qui était sur lui.
37:24	Ils le prirent et le jetèrent dans la citerne. Cette citerne était vide, il n'y avait pas d'eau.

### Yossef vendu à des marchands et emmené en Égypte

37:25	Ils s'assirent ensuite pour manger du pain. Et comme ils levaient les yeux, voici qu'ils aperçurent une caravane de Yishmaelites qui venait de Galaad. Leurs chameaux étaient chargés d'aromates, de baume et de la myrrhe, qu'ils allaient livrer en Égypte.
37:26	Et Yéhouda dit à ses frères : Que gagnerons-nous à tuer notre frère et à cacher son sang ?
37:27	Venez, vendons-le à ces Yishmaelites et ne mettons pas notre main sur lui, car il est notre frère, c'est notre chair. Et ses frères lui obéirent.
37:28	Et comme les marchands madianites passaient, ils tirèrent et firent remonter Yossef de la citerne, et le vendirent pour 20 pièces d'argent aux Yishmaelites, qui emmenèrent Yossef en Égypte<!--Ps. 105:17.-->.
37:29	Puis Reouben revint à la citerne, et voici, Yossef n'était plus dans la citerne. Alors il déchira ses vêtements.
37:30	Il retourna vers ses frères et leur dit : L'enfant n'y est plus ! Et moi, moi, où irai-je ?
37:31	Ils prirent la tunique de Yossef, et ayant tué un bouc d'entre les chèvres, ils plongèrent la tunique dans le sang.
37:32	Puis ils envoyèrent et firent porter à leur père la tunique de plusieurs couleurs, en lui disant : Voici ce que nous avons trouvé ! Reconnais maintenant si c'est la tunique de ton fils ou non.
37:33	Yaacov la reconnut et dit : C'est la tunique de mon fils ! Une bête féroce l'a dévoré ! Yossef a été déchiré, il a été déchiré !
37:34	Et Yaacov déchira ses vêtements, il mit un sac sur ses reins et il porta le deuil de son fils durant plusieurs jours.
37:35	Tous ses fils et toutes ses filles vinrent pour le consoler, mais il rejeta toute consolation. Il disait : C'est en pleurant que je descendrai vers mon fils dans le shéol ! C'est ainsi que son père le pleurait.
37:36	Les Madianites le vendirent en Égypte à Potiphar, eunuque de pharaon, chef des gardes.

## Chapitre 38

### Yéhouda et Tamar

38:1	Il arriva qu'en ce temps-là, Yéhouda descendit de chez ses frères et se retira vers un homme d'Adoullam, du nom de Chiyrah.
38:2	Là, Yéhouda vit la fille d'un Cananéen, nommé Shoua, il la prit pour femme et alla vers elle.
38:3	Elle conçut et enfanta un fils qu'elle appela Er.
38:4	Elle conçut encore et enfanta un fils qu'elle appela Onan.
38:5	Elle enfanta de nouveau un fils qu'elle appela du nom de Shelah. Yéhouda était à Czib quand elle l'enfanta.
38:6	Yéhouda prit une femme pour Er, son premier-né, une femme nommée Tamar.
38:7	Mais Er, le premier-né de Yéhouda, était méchant devant YHWH, et YHWH le fit mourir<!--No. 26:19.-->.
38:8	Alors Yéhouda dit à Onan : Va avec la femme de ton frère, exerce envers elle ton droit de beau-frère et suscite une postérité à ton frère<!--Lé. 25:25,48 ; De. 25:5-7. Voir commentaire en Ru. 2:20.-->.
38:9	Mais Onan, sachant que les enfants ne seraient pas à lui, se souillait à terre lorsqu'il allait vers la femme de son frère, afin de ne pas donner de postérité à son frère.
38:10	Ce qu’il faisait fut mauvais aux yeux de YHWH, c'est pourquoi il le fit aussi mourir.
38:11	Et Yéhouda dit à Tamar, sa belle-fille : Demeure veuve dans la maison de ton père, jusqu'à ce que Shelah, mon fils, soit devenu grand. Car il se disait : Il faut prendre garde qu'il ne meure comme ses frères. Ainsi, Tamar s'en alla et demeura dans la maison de son père.
38:12	Après beaucoup de jours, la fille de Shoua, femme de Yéhouda, mourut. Lorsque Yéhouda fut consolé, il monta vers ceux qui tondaient ses brebis à Thimna, avec Chiyrah, l'Adoullamite, son ami intime.
38:13	On en informa Tamar et on lui dit : Voici, ton beau-père monte à Thimna pour tondre ses brebis.
38:14	Alors elle ôta ses habits de veuve, se couvrit d'un voile et s'enveloppa, et elle s'assit à l'entrée d'Énaïm, sur le chemin de Thimna, car elle voyait que Shelah était devenu grand et qu'elle ne lui était pas donnée pour femme.
38:15	Et quand Yéhouda la vit, il s'imagina que c'était une prostituée, car elle avait couvert son visage.
38:16	Il l'aborda sur le chemin et lui dit : Permets, je te prie, que je vienne vers toi ! Car il ne savait pas que c'était sa belle-fille. Et elle répondit : Que me donneras-tu pour venir vers moi ?
38:17	Il répondit : Je t'enverrai un chevreau d'entre les chèvres du troupeau. Elle répondit : Me donneras-tu un gage jusqu'à ce que tu l'envoies ?
38:18	Il répondit : Quel gage te donnerai-je ? Et elle répondit : Ton sceau, ton cordon et ton bâton que tu as à la main. Et il les lui donna. Il alla vers elle et elle devint enceinte de lui.
38:19	Puis elle se leva et s'en alla. Elle ôta son voile et remit ses habits de veuve.
38:20	Yéhouda envoya un chevreau d'entre ses chèvres par son ami intime l'Adoullamite, pour qu'il retire le gage de la main de la femme, mais il ne la trouva pas.
38:21	Il interrogea les hommes du lieu où elle avait été, en disant : Où est cette prostituée qui était à Énaïm, sur le chemin ? Ils répondirent : Il n'y a pas eu ici de prostituée.
38:22	Il retourna auprès de Yéhouda et lui dit : Je ne l'ai pas trouvée, et même les gens du lieu m'ont dit : Il n'y a pas eu ici de prostituée.
38:23	Yéhouda dit : Qu'elle garde ce qu'elle a ! Il ne faut pas nous faire mépriser. Voici, j'ai envoyé ce chevreau, mais tu ne l'as pas trouvée.
38:24	Environ trois mois après, on fit un rapport à Yéhouda, en disant : Tamar, ta belle-fille, a commis un adultère, et voici elle est même enceinte. Et Yéhouda dit : Faites-la sortir et qu'elle soit brûlée !
38:25	Comme on la faisait sortir, elle envoya dire à son beau-père : Je suis enceinte de l'homme à qui ces choses appartiennent. Elle dit aussi : Reconnais, je te prie, à qui sont cet anneau à cacheter, ce cordon et ce bâton.
38:26	Alors Yéhouda les reconnut et il dit : Elle est plus juste que moi, parce que je ne l'ai pas donnée à Shelah, mon fils. Et il ne la connut plus.
38:27	Quand elle fut au moment d'accoucher, voici, des jumeaux étaient dans son ventre.
38:28	Et pendant qu'elle accouchait, il y en eut un qui présenta la main. La sage-femme la prit et y attacha un fil cramoisi, en disant : Celui-ci sort le premier.
38:29	Mais il retira la main et son frère sortit. Alors la sage-femme dit : Quelle brèche tu as faite ! Et elle lui donna le nom de Pérets.
38:30	Ensuite, sortit son frère qui avait à la main le fil cramoisi, et on lui donna le nom de Zérach.

## Chapitre 39

### Yossef fidèle à YHWH devant la tentation

39:1	Or quand on fit descendre Yossef en Égypte, Potiphar, eunuque de pharaon, chef des gardes, homme égyptien, l'acheta de la main des Yishmaelites qui l'y avaient amené.
39:2	Et YHWH était avec Yossef et il était un homme qui faisait tout prospérer. Il était dans la maison de son seigneur<!--Seigneur vient de l'hébreu « adhonaw » qui est le pluriel de « adhôn ». L'auteur veut exprimer l'excellence, la majesté. Voir Ge. 39:3,7,8,16,19,20.--> égyptien.
39:3	Son seigneur vit que YHWH était avec lui et que YHWH faisait prospérer entre ses mains tout ce qu'il faisait.
39:4	C'est pourquoi Yossef trouva grâce aux yeux de son maître, qui l'employa à son service. Et son seigneur l'établit sur sa maison et lui remit entre les mains tout ce qui lui appartenait.
39:5	Et il arriva que, depuis qu'il l'eut établi sur sa maison et sur tout ce qu'il possédait, YHWH bénit la maison de l'Égyptien à cause de Yossef, et la bénédiction de YHWH fut sur tout ce qui lui appartenait, soit à la maison, soit aux champs.
39:6	Il remit donc tout ce qui était à lui entre les mains de Yossef et ne s'occupa plus de rien, excepté de la nourriture qu'il mangeait. Or Yossef était beau de taille et beau de figure.
39:7	Après ces choses, il arriva que la femme de son seigneur porta les yeux sur Yossef et elle lui dit : Couche avec moi<!--Pr. 7:9-13.--> !
39:8	Mais il le refusa et dit à la femme de son seigneur : Voici, mon seigneur ne prend avec moi connaissance de rien dans la maison et il a remis entre mes mains tout ce qui lui appartient.
39:9	Il n'y a personne dans cette maison qui soit plus grand que moi, et il ne m'a rien interdit, excepté toi, parce que tu es sa femme. Comment ferais-je un si grand mal et pécherais-je contre Elohîm ?
39:10	Quoiqu'elle parlât tous les jours à Yossef, il refusa de coucher à côté d'elle, d'être avec elle.
39:11	Mais il arriva, un certain jour, qu'il entra dans la maison pour faire son travail, et de tous les hommes de la maison, personne n'était dans la maison,
39:12	elle le saisit par son vêtement et lui dit : Couche avec moi ! Mais il laissa son vêtement entre ses mains, s'enfuit et sortit dehors<!--1 Co. 6:18.-->.
39:13	Et lorsqu'elle vit qu'il lui avait laissé son vêtement entre les mains et qu'il s'était enfui dehors,
39:14	elle appela les gens de sa maison et leur parla, en disant : Voyez ! On nous a amené un homme, un Hébreu, pour se moquer de nous. Il est venu vers moi pour coucher avec moi, et j’ai appelé à grands cris.
39:15	Et il est arrivé, quand il a entendu que j’élevais ma voix et que je criais, qu’il a laissé son vêtement à côté de moi et s’est enfui, et est sorti dehors.
39:16	Et elle garda le vêtement de Yossef jusqu'à ce que son seigneur rentre à la maison.
39:17	Alors elle lui parla en ces mêmes termes, et dit : Le serviteur hébreu que tu nous as amené est venu vers moi pour se moquer de moi.
39:18	Mais comme j'ai élevé ma voix et que j'ai crié, il a laissé son vêtement à côté de moi et s'est enfui.
39:19	Et il arriva, dès que son seigneur entendit les paroles que lui disait sa femme, en ces termes : Ton serviteur m'a fait ce que je t'ai dit, sa colère s'enflamma.

### Yossef en prison

39:20	Et le seigneur de Yossef le prit et le mit en prison, à l'endroit où les prisonniers du roi étaient enfermés. Il resta donc là, en prison.
39:21	Mais YHWH fut avec Yossef. Il étendit sa bonté sur lui et lui fit trouver grâce aux yeux du chef de la prison.
39:22	Et le chef de la prison mit entre les mains de Yossef tous les prisonniers qui étaient dans la prison, et tout ce qu'il y avait à faire, il le faisait.
39:23	Le chef de la prison ne prenait aucune connaissance de ce que Yossef avait en main, parce que YHWH était avec lui. Et YHWH faisait prospérer tout ce qu'il faisait.

## Chapitre 40

### Yossef interprète les rêves de l'échanson et du panetier du roi

40:1	Après ces choses, il arriva que l'échanson et le panetier du roi d'Égypte offensèrent leur seigneur, le roi d'Égypte.
40:2	Pharaon fut très fâché contre ces deux eunuques, contre le chef des échansons et contre le chef des panetiers.
40:3	Et il les fit mettre dans la maison du chef des gardes, dans la prison étroite, dans le même lieu où Yossef était enfermé.
40:4	Le chef des gardes les plaça sous la surveillance de Yossef qui les servait. Ils furent quelques jours en prison.
40:5	Pendant une nuit, l'échanson et le panetier du pharaon enfermés dans la prison rêvèrent tous les deux un rêve, chacun son rêve, chacun selon l'interprétation de son rêve.
40:6	Yossef, étant venu le matin vers eux, les regarda et vit qu'ils étaient tristes.
40:7	Et il interrogea ces deux eunuques de pharaon, qui étaient avec lui dans la prison de son maître, et leur dit : Pourquoi avez-vous mauvais visage aujourd'hui ?
40:8	Ils lui répondirent : Nous avons rêvé un rêve et il n'y a personne pour l'interpréter. Et Yossef leur dit : Les interprétations n'appartiennent-elles pas à Elohîm ? Je vous prie, racontez-le moi<!--Job 33:15 ; 1 Co. 12:8-10.-->.
40:9	Le chef des échansons raconta son rêve à Yossef et lui dit : Dans mon rêve, voici, il y avait un cep devant moi.
40:10	Ce cep avait trois sarments. Quand il a poussé, sa fleur s'est développée et ses grappes ont donné des raisins mûrs.
40:11	La coupe de pharaon était dans ma main. J'ai pris les raisins, je les ai pressés dans la coupe de pharaon et j'ai mis la coupe dans la main de pharaon.
40:12	Yossef lui dit : Voici son interprétation : Les trois sarments sont trois jours.
40:13	Dans trois jours, pharaon élèvera ta tête et te rétablira dans ta charge, et tu mettras la coupe de pharaon dans sa main, comme tu le faisais auparavant lorsque tu étais son échanson.
40:14	Mais souviens-toi de moi quand tu seras heureux et use de bonté envers moi je te prie : fais mention de moi à pharaon, afin qu'il me fasse sortir de cette maison.
40:15	Car j'ai été enlevé du pays des Hébreux et ici non plus je n'ai rien fait pour être mis en prison.
40:16	Le chef des panetiers voyant que Yossef avait bien interprété, lui dit : Voici, il y avait aussi dans mon rêve trois corbeilles de pain blanc sur ma tête.
40:17	Dans la corbeille la plus élevée, il y avait pour pharaon, des mets de toute espèce, cuits au four. Les oiseaux les mangeaient dans la corbeille au-dessus de ma tête.
40:18	Yossef répondit, et dit : Voici son interprétation : Les trois corbeilles sont trois jours.
40:19	Dans trois jours, pharaon enlèvera ta tête de dessus toi et te fera pendre à un bois, et les oiseaux mangeront ta chair sur toi.
40:20	Il arriva au troisième jour, qui était le jour de la naissance de pharaon, qu'il fit un festin à tous ses serviteurs. Il éleva la tête du chef des échansons et la tête du chef des panetiers, au milieu de ses serviteurs.
40:21	Il rétablit le chef des échansons dans sa charge d'échanson, pour qu'il mette la coupe dans la main de pharaon.
40:22	Mais il fit pendre le chef des panetiers, selon l'explication que Yossef leur avait donnée.
40:23	Cependant, le chef des échansons ne se souvint pas de Yossef. Il l'oublia.

## Chapitre 41

### Yossef interprète les rêves de pharaon

41:1	Mais il arriva qu'au bout de 2 ans entiers, pharaon eut un rêve. Et il lui semblait qu'il était près du fleuve.
41:2	Et voici, sept jeunes vaches belles à voir, grasses de chair, montèrent hors du fleuve et se mirent à paître dans les prairies.
41:3	Et voici sept autres jeunes vaches, laides à voir et maigres de chair, montèrent hors du fleuve derrière les autres et se tinrent auprès des autres jeunes vaches sur le bord du fleuve.
41:4	Les jeunes vaches laides à voir et maigres, mangèrent les sept jeunes vaches belles à voir et grasses. Alors pharaon s'éveilla.
41:5	Il se rendormit et il eut un second rêve. Voici, sept épis gras et beaux montèrent sur une même tige.
41:6	Et sept épis maigres et brûlés par le vent d'orient poussèrent après eux.
41:7	Les épis maigres engloutirent les sept épis gras et pleins. Et pharaon se réveilla. Voilà que c'était un rêve !
41:8	Il arriva au matin que, pharaon eut l'esprit troublé, et il envoya appeler tous les magiciens et tous les sages d'Égypte et leur raconta ses rêves. Mais personne ne put les interpréter à pharaon.
41:9	Alors le chef des échansons parla à pharaon, en disant : Je rappellerai aujourd'hui le souvenir de mes fautes.
41:10	Lorsque pharaon fut irrité contre ses serviteurs et nous fit mettre, le chef des panetiers et moi, en prison, dans la maison du chef des gardes,
41:11	nous avons rêvé un rêve, une même nuit, moi et lui. Nous rêvions chacun selon l'interprétation de son rêve.
41:12	Or il y avait là avec nous un jeune homme hébreu, esclave du chef des gardes. Nous lui avons raconté nos rêves et il nous les a interprétés. Il a interprété le rêve de chacun
41:13	et les choses sont arrivées comme il nous les avait interprétées. Je fus rétabli dans ma fonction et l'autre fut pendu.
41:14	Alors pharaon envoya appeler Yossef. On le fit sortir en hâte de la prison. On le rasa et on lui fit changer de vêtements, puis il se rendit vers pharaon.
41:15	Et pharaon dit à Yossef : J'ai eu un rêve et personne n'a pu l'interpréter. Or j'ai entendu dire de toi que, quand tu entends un rêve, tu l'interprètes.
41:16	Et Yossef répondit à pharaon, en disant : Ce n'est pas moi ! C'est Elohîm qui donnera une réponse concernant la paix de pharaon.
41:17	Et pharaon dit alors à Yossef : Dans mon rêve, voici, je me tenais sur le bord du fleuve.
41:18	Et voici, sept vaches grasses de chair et belles d'apparence montèrent hors du fleuve et se mirent à paître dans la prairie.
41:19	Et voici sept autres vaches montèrent derrière elles, maigres, très laides d'apparence et décharnées. Je n'en ai pas vu d'aussi laides dans tout le pays d'Égypte.
41:20	Mais les vaches décharnées et laides mangèrent les sept premières vaches qui étaient grasses ;
41:21	elles les engloutirent dans leur ventre, sans qu'on s'aperçoive qu'elles y étaient entrées, et leur apparence était laide comme auparavant. Puis je me suis réveillé.
41:22	Je vis encore en rêve sept épis pleins et beaux qui montèrent sur une même tige.
41:23	Puis voici, sept épis vides, maigres, brûlés par le vent d'orient, poussèrent après eux.
41:24	Les épis maigres engloutirent les sept beaux épis. Je l'ai dit aux magiciens, mais personne ne m'a donné l'explication.
41:25	Yossef dit à pharaon : Le rêve du pharaon ne fait qu’un. Elohîm indique au pharaon ce qu’il va faire.
41:26	Les sept vaches belles sont sept années, et les sept épis beaux sont sept années : c'est un seul rêve.
41:27	Les sept vaches décharnées et laides qui montaient derrière les premières sont sept années, tout comme les sept épis vides brûlés par le vent d'orient. Ce sont sept années de famine.
41:28	Ainsi, comme je viens de le dire à pharaon, Elohîm a fait connaître à pharaon ce qu'il va faire.
41:29	Voici, il y aura sept années de grande abondance dans tout le pays d'Égypte.
41:30	Sept années de famine viendront après elles, et l'on oubliera toute cette abondance au pays d'Égypte. La famine consumera le pays.
41:31	Cette famine qui suivra sera si forte qu'on ne reconnaîtra plus de cette abondance dans le pays.
41:32	Si le rêve s'est répété deux fois au pharaon, c'est que la chose est arrêtée de la part d'Elohîm, et qu'Elohîm se hâtera de l'exécuter.
41:33	Or maintenant, que pharaon choisisse un homme intelligent et sage, et qu'il l'établisse sur le pays d'Égypte.
41:34	Que pharaon établisse et institue des commissaires sur le pays, et qu'ils prennent la cinquième partie du revenu du pays d'Égypte durant les sept années d'abondance.
41:35	Qu'ils rassemblent tous les produits de ces bonnes années qui viennent, qu'ils fassent, sous l'autorité de pharaon, des amas de blé, des provisions de nourriture dans les villes et qu'ils en aient la garde.
41:36	Ces provisions de nourriture seront en réserve pour le pays durant les sept années de famine qui seront dans le pays d'Égypte, afin que le pays ne soit pas consumé par la famine.

### Yossef est établi sur l'Égypte par pharaon

41:37	Et la chose fut bonne aux yeux du pharaon et aux yeux de tous ses serviteurs<!--Ac. 7:10.-->.
41:38	Et pharaon dit à ses serviteurs : Pourrions-nous trouver un homme semblable à celui-ci, qui a l'Esprit d'Elohîm ?
41:39	Et pharaon dit à Yossef : Puisqu'Elohîm t'a fait connaître toutes ces choses, il n'y a personne qui soit aussi intelligent et aussi sage que toi.
41:40	Tu seras sur ma maison, et tout mon peuple obéira à ta bouche. Je serai seulement plus grand que toi par le trône.
41:41	Pharaon dit encore à Yossef : Regarde, je t'établis sur tout le pays d'Égypte.
41:42	Alors pharaon ôta son anneau de sa main et le mit à la main de Yossef. Il le fit revêtir d'habits de fin lin et lui mit un collier d'or au cou.
41:43	Il le fit monter sur le char qui suivait le sien et on criait devant lui : À genoux ! Et il l'établit sur tout le pays d'Égypte.
41:44	Et pharaon dit à Yossef : Je suis pharaon ! Et sans toi, personne ne lèvera la main ni le pied dans tout le pays d'Égypte.

### Yossef épouse une Égyptienne

41:45	Pharaon appela Yossef du nom de Tsaphnath-Paenéach, et il lui donna pour femme Asnath, fille de Poti-Phéra, prêtre d'On. Et Yossef alla visiter le pays d'Égypte.
41:46	Yossef était âgé de 30 ans lorsqu'il se présenta devant pharaon, roi d'Égypte. Il quitta pharaon et parcourut tout le pays d'Égypte.
41:47	Et la terre rapporta très abondamment pendant les sept années de fertilité.
41:48	Yossef rassembla tous les produits de ces sept années dans le pays d'Égypte. Il fit des provisions de nourriture dans les villes, mettant dans l'intérieur de chaque ville les productions des champs d'alentour.
41:49	Ainsi, Yossef amassa du blé comme le sable de la mer, en si grande quantité qu'on cessa de le compter, parce qu'il était sans nombre.
41:50	Avant les années de famine, il naquit à Yossef deux fils, que lui enfanta Asnath, fille de Poti-Phéra, prêtre d'On.
41:51	Yossef donna au premier-né le nom de Menashè<!--Manassé.-->, parce que, dit-il, Elohîm m'a fait oublier toute ma peine et toute la maison de mon père.
41:52	Et il donna au second le nom d'Éphraïm, parce que, dit-il, Elohîm m'a fait porter du fruit dans le pays de mon affliction.
41:53	Alors finirent les sept années de l'abondance qui avaient été dans le pays d'Égypte.
41:54	Et les sept années de la famine commencèrent à venir comme Yossef l'avait prédit. Et la famine fut dans tous les pays ; mais il y avait du pain dans tout le pays d'Égypte.
41:55	Ensuite, tout le pays d'Égypte fut affamé, et le peuple cria à pharaon pour avoir du pain. Et pharaon répondit à tous les Égyptiens : Allez vers Yossef et faites ce qu'il vous dira.
41:56	La famine sévissait sur toute la surface du pays. Yossef ouvrit tous les lieux de dépôt et vendit du blé aux Égyptiens. La famine augmentait dans le pays d'Égypte.
41:57	On venait de tous les pays jusqu'en Égypte pour acheter du blé auprès de Yossef, car la famine était très grande sur toute la Terre.

## Chapitre 42

### Yaacov envoie ses fils en Égypte

42:1	Et Yaacov, voyant qu'il y avait du blé à vendre en Égypte, dit à ses fils : Pourquoi vous regardez-vous les uns les autres ?
42:2	Il leur dit aussi : Voici, j'ai appris qu'il y a du blé à vendre en Égypte, descendez-y pour nous en acheter là, afin que nous vivions et que nous ne mourions pas.
42:3	Alors les frères de Yossef descendirent pour acheter du blé en Égypte.
42:4	Mais Yaacov n'envoya pas Benyamin, frère de Yossef, avec ses frères. Car il se disait : Il faut prendre garde qu'un malheur ne lui arrive.
42:5	Ainsi, les fils d'Israël allèrent en Égypte pour acheter du blé avec ceux qui y allaient, car la famine était dans le pays de Canaan.

### Yossef éprouve ses frères

42:6	Yossef commandait dans le pays, et c'était lui qui vendait le blé à tous les peuples de la Terre. Les frères de Yossef vinrent et se prosternèrent devant lui, la face contre terre.
42:7	Yossef vit ses frères et les reconnut, mais il feignit d'être un étranger pour eux, et il leur parla rudement, en leur disant : D'où venez-vous ? Et ils répondirent : Du pays de Canaan, pour acheter des vivres.
42:8	Yossef reconnut ses frères, mais eux ne le reconnurent pas.
42:9	Alors Yossef se souvint des rêves qu'il avait eus à leur sujet, et leur dit : Vous êtes des espions, vous êtes venus pour observer les lieux faibles du pays.
42:10	Et ils lui répondirent : Non, mon seigneur, tes serviteurs sont venus pour acheter des vivres.
42:11	Nous sommes tous fils d'un même homme, nous sommes des gens honnêtes ! Tes serviteurs ne sont pas des espions.
42:12	Et il leur dit : Nullement ! Vous êtes venus pour observer les lieux faibles du pays.
42:13	Et ils répondirent : Nous, tes serviteurs, étions douze frères, fils d'un même homme, dans le pays de Canaan. Et voici, le plus jeune est aujourd'hui avec notre père, et l'un n'est plus.
42:14	Yossef leur dit : C'est ce que je vous disais, vous êtes des espions !
42:15	Voici comment vous serez éprouvés : Par la vie de pharaon ! vous ne sortirez pas d'ici jusqu'à ce que votre jeune frère ne soit venu ici.
42:16	Envoyez l'un de vous et qu'il amène votre frère pendant que vous, vous resterez prisonniers. Vos paroles seront éprouvées et je saurai si vous avez dit la vérité. Autrement, par la vie de pharaon, vous êtes des espions !
42:17	Et il les mit tous ensemble en prison pendant trois jours.
42:18	Le troisième jour, Yossef leur dit : Faites ceci et vous vivrez. Je crains Elohîm !
42:19	Si vous êtes des gens honnêtes, que l'un de vos frères reste en prison dans la maison d'arrêt. Et vous, partez et emportez du blé pour nourrir vos familles.
42:20	Puis amenez-moi votre jeune frère, afin que vos paroles soient éprouvées et vous ne mourrez pas. Et ils firent ainsi.
42:21	Et chacun dit à son frère : Nous sommes vraiment coupables à l'égard de notre frère. Car nous avons vu l'angoisse de son âme quand il nous demandait grâce et nous ne l'avons pas écouté. C'est pour cela que cette détresse nous est arrivée.
42:22	Reouben leur répondit, en disant : Ne vous disais-je pas de ne pas commettre de péché envers cet enfant ? Mais vous n'avez pas écouté, et voici que son sang nous est redemandé.
42:23	Ils ne savaient pas que Yossef les comprenait, parce qu'il se servait d'un interprète pour leur parler.
42:24	Il s'éloigna d'eux pour pleurer. Et il revint et leur parla, puis il prit parmi eux Shim’ôn et le fit enchaîner sous leurs yeux.
42:25	Et Yossef ordonna qu'on remplisse leurs sacs de blé, qu'on remette l'argent de chacun d'eux dans son sac, et qu'on leur donne de la provision pour la route ; et cela fut fait ainsi.
42:26	Ils chargèrent donc leur blé sur leurs ânes et s'en allèrent.
42:27	L'un d'eux ouvrit son sac pour donner du fourrage à son âne dans l'hôtellerie, et il vit son argent qui était à l'ouverture de son sac.
42:28	Il dit à ses frères : Mon argent m'a été rendu et le voici dans mon sac. Alors leur cœur fut en défaillance, ils furent saisis de peur et se dirent chacun à son frère : Qu'est-ce qu'Elohîm nous a fait ?
42:29	Et étant arrivés dans le pays de Canaan, vers Yaacov leur père, ils lui racontèrent toutes les choses qui leur étaient arrivées, en disant :
42:30	L'homme qui est le seigneur du pays nous a parlé rudement et nous a pris pour des espions du pays.
42:31	Mais nous lui avons répondu : Nous sommes sincères, nous ne sommes pas des espions.
42:32	Nous étions douze frères, fils de notre père. L'un n'est plus, et le plus jeune est aujourd'hui avec notre père dans le pays de Canaan.
42:33	Et cet homme qui est le seigneur du pays, nous a dit : Voici comment je saurai que vous êtes d'honnêtes gens. Laissez-moi l'un de vos frères, et prenez de quoi nourrir vos familles et partez.
42:34	Puis amenez-moi votre jeune frère et je saurai que vous n'êtes pas des espions mais que vous êtes des gens honnêtes. Je vous rendrai votre frère et vous pourrez librement trafiquer dans le pays.
42:35	Lorsqu'ils vidèrent leurs sacs, voici, le paquet d'argent de chacun était dans son sac. Ils virent, eux et leur père, leurs paquets d'argent, et ils eurent peur.
42:36	Yaacov leur père leur dit : Vous me privez de mes enfants ! Yossef n'est plus, et Shim’ôn n'est plus, et vous prendriez Benyamin ! C'est sur moi que tout cela retombe.
42:37	Reouben parla à son père et lui dit : Fais mourir deux de mes fils si je ne te ramène pas Benyamin ! Remets-le entre mes mains et je te le ramènerai.
42:38	Yaacov répondit : Mon fils ne descendra pas avec vous, car son frère est mort et il reste seul. S'il lui arrivait un malheur dans le voyage que vous allez faire, vous feriez descendre mes cheveux blancs avec douleur dans le shéol.

## Chapitre 43

### Yaacov renvoie ses fils en Égypte

43:1	Or la famine devint très grande dans le pays.
43:2	Et il arriva, lorsqu'ils eurent achevé de manger le blé qu'ils avaient apporté d'Égypte, que leur père leur dit : Retournez, achetez-nous un peu de vivres.
43:3	Yéhouda lui répondit et lui dit : Cet homme nous a répété, il nous a répété, en disant : Vous ne verrez pas ma face, à moins que votre frère ne soit avec vous.
43:4	Si donc tu envoies notre frère avec nous, nous descendrons en Égypte et nous t'achèterons des vivres.
43:5	Mais si tu ne l'envoies pas, nous n'y descendrons pas. En effet, cet homme nous a dit : Vous ne verrez pas ma face, à moins que votre frère ne soit avec vous.
43:6	Et Israël dit : Pourquoi avez-vous mal agi à mon égard en disant à cet homme que vous aviez encore un frère ?
43:7	Ils répondirent : Cet homme nous a interrogés sur nous et sur notre famille, en disant : Votre père vit-il encore ? N'avez-vous pas de frère ? Et nous lui avons déclaré selon ce qu'il nous avait demandé. Pouvions-nous savoir qu'il dirait : Faites descendre votre frère ?
43:8	Yéhouda dit à Israël, son père : Laisse venir le garçon avec moi, afin que nous nous levions et que nous partions. Nous vivrons et nous ne mourrons pas, nous, toi et nos enfants.
43:9	Je réponds de lui, tu le redemanderas de ma main. Si je ne te le ramène pas auprès de toi et si je ne le remets pas devant ta face, je serai coupable toute ma vie envers toi.
43:10	Car si nous n'avions pas tardé, certainement nous serions déjà de retour deux fois.
43:11	Alors Israël leur père leur dit : Si cela est ainsi, faites ceci, prenez dans vos bagages les meilleures productions du pays, pour en porter un présent à cet homme, un peu de baume, un peu de miel, des épices, de la myrrhe, des dattes et des amandes.
43:12	Prenez avec vous de l'argent au double dans vos mains et rapportez l'argent qu'on avait mis à l'entrée de vos sacs : peut-être était-ce une erreur.
43:13	Prenez votre frère et levez-vous, retournez vers cet homme.
43:14	Que El Shaddaï vous accorde miséricorde devant cet homme et qu'il relâche votre autre frère et Benyamin. S'il faut que je sois privé de ces deux fils, que j'en sois privé !
43:15	Alors ils prirent le présent, et ayant pris de l'argent au double dans leurs mains, et Benyamin, ils se levèrent et descendirent en Égypte. Puis ils se présentèrent devant Yossef.
43:16	Dès que Yossef vit Benyamin avec eux, il dit à l'intendant de sa maison : Fais entrer ces hommes dans la maison, tue et apprête quelques bêtes, car ils mangeront à midi avec moi.
43:17	Cet homme fit ce que Yossef lui avait dit et il conduisit ces hommes dans la maison de Yossef.
43:18	Ils eurent peur lorsqu'ils furent conduits dans la maison de Yossef, et ils dirent : Nous sommes emmenés à cause de l'argent remis l'autre fois dans nos sacs. C'est pour se jeter sur nous, se précipiter sur nous. C'est pour nous prendre comme esclaves et s'emparer de nos ânes.
43:19	Ils s'approchèrent de l'intendant de la maison de Yossef et lui adressèrent la parole, à l'entrée de la maison.
43:20	Ils dirent : Excuse-nous, mon seigneur, nous sommes déjà descendus une fois pour acheter des vivres.
43:21	Puis, quand nous sommes arrivés au lieu où nous devions passer la nuit, nous avons ouvert nos sacs, et voici, l'argent de chacun était à l'entrée de son sac, notre argent selon son poids. Nous le rapportons avec nous.
43:22	Nous avons aussi apporté d'autre argent en nos mains pour acheter de la nourriture. Nous ne savons pas qui a remis notre argent dans nos sacs.
43:23	L'intendant leur dit : Que la paix soit avec vous ! N'ayez pas peur : c'est votre Elohîm, l'Elohîm de votre père qui vous a donné un trésor dans vos sacs. Votre argent est parvenu jusqu'à moi. Et il leur amena Shim’ôn.
43:24	Cet homme les fit entrer dans la maison de Yossef. Il leur donna de l'eau et ils lavèrent leurs pieds ; il donna aussi à manger à leurs ânes.
43:25	Ils préparèrent leur présent en attendant que Yossef revienne à midi, car ils avaient appris qu'ils mangeraient du pain chez lui.
43:26	Quand Yossef fut arrivé à la maison, ils lui offrirent le présent qu'ils avaient dans leurs mains et se prosternèrent<!--Ge. 37:5-10.--> à terre devant lui dans la maison.
43:27	Il leur demanda comment ils se portaient et leur dit : Votre vieux père, dont vous m'avez parlé, se porte-t-il bien ? Vit-il encore ?
43:28	Ils répondirent : Ton serviteur, notre père, se porte bien, il vit encore. Et ils s'inclinèrent et se prosternèrent.
43:29	Yossef leva les yeux, il vit Benyamin, son frère, fils de sa mère, et il dit : Est-ce là votre jeune frère dont vous m'avez parlé ? Et il ajouta : Mon fils, qu'Elohîm te fasse grâce !
43:30	Et Yossef se retira promptement, car ses entrailles étaient émues à la vue de son frère et il cherchait un lieu pour pleurer. Il entra dans sa chambre et il y pleura.
43:31	Après s'être lavé le visage, il sortit de là, et faisant des efforts pour se contenir, il dit : Servez le pain.
43:32	On servit Yossef à part, et ses frères à part, et les Égyptiens qui mangeaient avec lui furent aussi servis à part, car les Égyptiens ne pouvaient manger du pain avec les Hébreux, parce que c'est à leurs yeux une abomination.
43:33	Les frères de Yossef s'assirent en sa présence, le premier-né selon son droit d'aînesse, et le plus jeune selon son âge. Ils se regardaient les uns les autres avec étonnement.
43:34	Yossef leur fit porter des mets qui étaient devant lui, et Benyamin en eut cinq fois plus que les autres. Ils burent et s'enivrèrent avec lui.

## Chapitre 44

### Yéhouda remplit ses engagements<!--Ge. 43:9.-->

44:1	Et Yossef donna un ordre à son intendant, en disant : Remplis de vivres les sacs de ces hommes, autant qu'ils en pourront porter, et remets l'argent de chacun à l'entrée de son sac.
44:2	Tu mettras aussi ma coupe, la coupe d'argent, à l'entrée du sac du plus petit avec l'argent de son blé. Et il fit comme Yossef lui avait dit.
44:3	Le matin, dès qu'il fit jour, on renvoya ces hommes avec leurs ânes.
44:4	Ils étaient sortis de la ville, ils n'en étaient guère éloignés, lorsque Yossef dit à son intendant : Va, poursuis ces hommes, et quand tu les auras atteints, tu leur diras : Pourquoi avez-vous rendu le mal pour le bien ?
44:5	N'est-ce pas la coupe dont se sert mon seigneur pour boire et pour deviner ? Vous avez mal fait d'agir ainsi.
44:6	L'intendant les atteignit et leur dit ces paroles.
44:7	Ils lui répondirent : Pourquoi mon seigneur parle-t-il ainsi ? Loin de tes serviteurs de faire une chose pareille !
44:8	Voici, nous t'avons rapporté du pays de Canaan l'argent que nous avions trouvé à l'entrée de nos sacs, et comment aurions-nous dérobé de l'argent ou de l'or de la maison de ton maître ?
44:9	Que celui de tes serviteurs sur qui se trouvera la coupe meure ! Et nous serons aussi esclaves de mon seigneur !
44:10	Il leur dit : Qu'il soit fait maintenant selon vos paroles ! Qu'il en soit ainsi ! Que celui sur qui se trouvera la coupe soit mon esclave, et vous, vous serez innocents.
44:11	Et ils se hâtèrent de déposer chacun son sac à terre, et chacun ouvrit son sac.
44:12	L'intendant les fouilla, en commençant par le plus âgé et en finissant par le plus jeune. Et la coupe fut trouvée dans le sac de Benyamin.
44:13	Alors ils déchirèrent leurs vêtements, et chacun rechargea son âne, et ils retournèrent à la ville.
44:14	Yéhouda et ses frères arrivèrent à la maison de Yossef, qui était encore là, et ils se jetèrent à terre devant lui.
44:15	Yossef leur dit : Quelle action avez-vous faite ? Ne savez-vous pas qu'un homme tel que moi ne devinerait, qu'il ne devinerait ?
44:16	Yéhouda lui répondit : Que dirons-nous à mon seigneur ? Comment parlerons-nous ? Et comment nous justifierons-nous ? Elohîm a trouvé l'iniquité de tes serviteurs ; voici, nous sommes esclaves de mon seigneur, nous et celui entre les mains de qui la coupe a été trouvée.
44:17	Mais il dit : Loin de moi d'agir ainsi ! L'homme dans la main duquel la coupe a été trouvée sera mon esclave. Mais vous, remontez en paix vers votre père.
44:18	Alors Yéhouda s'approcha de lui, en disant : Excuse-moi, mon seigneur ! Je te prie, que ton serviteur dise un mot, je te prie, aux oreilles de mon seigneur, et que ta colère ne s'enflamme pas contre ton serviteur, car tu es comme pharaon.
44:19	Mon seigneur interrogea ses serviteurs, en disant : Avez-vous un père ou un frère ?
44:20	Nous avons répondu à mon seigneur : Nous avons un vieux père et l'enfant qu'il a eu dans sa vieillesse est encore jeune. Son frère est mort, il est resté le seul de sa mère et son père l'aime.
44:21	Tu as dit à tes serviteurs : Faites-le descendre vers moi et que je le voie de mes yeux.
44:22	Nous avons répondu à mon seigneur : Ce garçon ne peut quitter son père, car s'il le quitte, son père mourra.
44:23	Alors tu as dit à tes serviteurs : Si votre petit frère ne descend avec vous, vous ne verrez plus ma face.
44:24	Lorsque nous sommes remontés auprès de ton serviteur, mon père, nous lui avons rapporté les paroles de mon seigneur.
44:25	Notre père nous a dit : Retournez et achetez-nous un peu de vivres.
44:26	Nous lui avons répondu : Nous ne pouvons pas descendre. Mais si notre petit frère est avec nous, nous descendrons, car nous ne pouvons pas voir la face de cet homme, à moins que notre jeune frère ne soit avec nous.
44:27	Ton serviteur, mon père, nous a dit : Vous savez que ma femme m'a enfanté deux fils.
44:28	L'un étant sorti de chez moi, je pense qu'il a été sans doute déchiré, car je ne l'ai pas revu jusqu'à présent.
44:29	Si vous me prenez encore celui-ci et qu'il lui arrive un malheur, vous ferez descendre mes cheveux blancs avec douleur dans le shéol.
44:30	Et maintenant, quand je retournerai auprès de ton serviteur mon père, si le garçon dont l'âme est liée étroitement à la sienne n'est pas avec nous,
44:31	il arrivera que, dès qu'il verra que le garçon n'y est pas, il mourra. Et tes serviteurs feront descendre avec douleur dans le shéol les cheveux blancs de ton serviteur notre père.
44:32	Car moi, ton serviteur, je me suis porté garant pour le garçon devant mon père en disant : Si je ne te le ramène pas, je serai pour toujours coupable envers mon père.
44:33	Ainsi maintenant, je te prie, que ton serviteur soit esclave de mon seigneur à la place du garçon et que le garçon monte avec ses frères !
44:34	Car comment monterai-je vers mon père, si le garçon n'est pas avec moi ? Que je ne voie pas le mal qui atteindrait alors mon père !

## Chapitre 45

### Yossef révèle son identité à ses frères

45:1	Alors, Yossef ne pouvant plus se contenir devant tous ceux qui étaient là présents, cria : Faites sortir tout le monde ! Et il ne resta personne quand il se fit connaître à ses frères.
45:2	Et en pleurant, il éleva sa voix, et les Égyptiens l'entendirent, et la maison de pharaon l'entendit aussi.
45:3	Et Yossef dit à ses frères : Je suis Yossef ! Mon père vit-il encore ? Mais ses frères ne pouvaient lui répondre, car ils étaient tout troublés en sa présence.
45:4	Yossef dit encore à ses frères : Je vous prie, approchez-vous de moi. Et ils s'approchèrent, et il leur dit : Je suis Yossef, votre frère, que vous avez vendu pour être mené en Égypte<!--Ac. 7:13.-->.
45:5	Mais maintenant ne vous affligez pas, et que ce ne soit pas à vos yeux un sujet de colère de ce que vous m'avez vendu ici, car Elohîm m'a envoyé devant vous pour la conservation de votre vie.
45:6	Car voici, il y a déjà 2 ans que la famine est sur la Terre, et il y aura encore 5 ans pendant lesquels il n'y aura ni labour ni moisson.
45:7	Mais Elohîm m'a envoyé devant vous, pour vous faire subsister sur la Terre et vous faire vivre par une grande délivrance.
45:8	Maintenant donc ce n'est pas vous qui m'avez envoyé ici, mais c'est Elohîm. Il m'a établi père de pharaon, seigneur sur toute sa maison et gouverneur de tout le pays d'Égypte.
45:9	Hâtez-vous d'aller vers mon père et dites-lui : Ainsi a dit ton fils Yossef : Elohîm m'a établi seigneur sur toute l'Égypte, descends vers moi, ne t'arrête pas.
45:10	Et tu habiteras dans la région de Gosen et tu seras près de moi avec tes fils et les fils de tes fils, tes brebis et tes bœufs, ainsi que tout ce qui est à toi.
45:11	Là, je t'entretiendrai, car il y aura encore 5 années de famine. Ainsi tu ne tomberas pas dans la pauvreté, toi, ta maison et tout ce qui est à toi.
45:12	Et voici, vous voyez de vos yeux, et Benyamin mon frère voit aussi de ses yeux, que c'est moi qui vous parle de ma propre bouche.
45:13	Rapportez donc à mon père quelle est ma gloire en Égypte et tout ce que vous avez vu. Hâtez-vous et faites descendre ici mon père.
45:14	Alors il se jeta au cou de Benyamin, son frère, et pleura. Benyamin pleura aussi sur son cou.
45:15	Puis il embrassa tous ses frères en pleurant. Après cela, ses frères parlèrent avec lui.
45:16	Et le bruit se répandit dans la maison de pharaon que les frères de Yossef étaient venus : cela fut bon aux yeux du pharaon et aux yeux de ses serviteurs.
45:17	Alors pharaon dit à Yossef : Dis à tes frères : Faites ceci : chargez vos bêtes et allez, retournez dans le pays de Canaan,
45:18	et prenez votre père et vos familles et revenez vers moi. Je vous donnerai le meilleur du pays d'Égypte, et vous mangerez la graisse du pays.
45:19	Et toi, donne-leur cet ordre : Faites ceci : prenez avec vous du pays d'Égypte, des chariots pour vos plus petits enfants et pour vos femmes. Amenez votre père et venez.
45:20	Ne regrettez pas vos objets, car le meilleur de tout le pays d'Égypte sera à vous.
45:21	Et les fils d'Israël firent ainsi. Et Yossef leur donna des chariots selon l'ordre de pharaon ; il leur donna aussi de la provision pour le chemin.
45:22	Il leur donna à chacun des vêtements de rechange, et il donna à Benyamin 300 pièces d'argent et 5 vêtements de rechange.
45:23	Il envoya aussi à son père 10 ânes chargés des plus excellentes choses qu'il y avait en Égypte, et 10 ânesses portant du blé, du pain et des vivres à son père pour la route.
45:24	Il renvoya donc ses frères et ils partirent. Il leur dit : Ne vous querellez pas en chemin.
45:25	Ainsi ils montèrent d'Égypte et vinrent vers Yaacov leur père, au pays de Canaan.
45:26	Et ils lui rapportèrent et lui dirent : Yossef vit encore, et même c'est lui qui gouverne tout le pays d'Égypte ! Mais son cœur s'engourdit, car il ne les croyait pas.
45:27	Ils lui dirent toutes les paroles que Yossef leur avait dites, et lorsqu'il vit les chariots que Yossef avait envoyés pour le transporter, alors l'esprit de Yaacov leur père reprit vie.
45:28	Alors Israël dit : C'est assez ! Yossef mon fils, vit encore ! J'irai et je le verrai avant que je meure !

## Chapitre 46

### Yaacov et ses fils en Égypte

46:1	Israël donc partit avec tout ce qui lui appartenait, et vint à Beer-Shéba, et il offrit des sacrifices à Elohîm de son père Yitzhak.
46:2	Et Elohîm parla à Israël dans les visions de la nuit, en disant : Yaacov, Yaacov ! Et il répondit : Me voici !
46:3	Et Elohîm lui dit : Je suis le El, l'Elohîm de ton père. N'aie pas peur de descendre en Égypte, je t'y ferai devenir une grande nation.
46:4	Je descendrai avec toi en Égypte et je t'en ferai moi-même monter, monter. C'est Yossef qui posera sa main sur tes yeux.
46:5	Ainsi Yaacov partit de Beer-Shéba, et les enfants d'Israël mirent Yaacov leur père, leurs petits enfants et leurs femmes sur les chariots que pharaon avait envoyés pour les porter.
46:6	Ils emmenèrent aussi leur bétail et leurs biens qu'ils avaient acquis dans le pays de Canaan. Yaacov et toute sa famille avec lui vinrent en Égypte.
46:7	Il amena donc avec lui en Égypte, ses fils et les fils de ses fils, ses filles et les filles de ses fils, et toute sa famille.
46:8	Voici les noms des fils d'Israël qui vinrent en Égypte : Yaacov et ses fils. Le premier-né de Yaacov fut Reouben.
46:9	Et les fils de Reouben : Hénoc, Pallou, Hetsron et Carmi.
46:10	Et les fils de Shim’ôn : Yemouel, Yamin, Ohad, Yakîn, Tsochar et Shaoul, fils d'une Cananéenne.
46:11	Et les fils de Lévi : Guershon, Kehath et Merari.
46:12	Et les fils de Yéhouda : Er, Onan, Shelah, Pérets et Zérach. Mais Er et Onan moururent au pays de Canaan. Les fils de Pérets furent Hetsron et Hamoul.
46:13	Et les fils de Yissakar : Thola, Pouva, Yob et Shimron.
46:14	Et les fils de Zebouloun : Séred, Élon et Yahleel.
46:15	Ce sont là les fils de Léah qu'elle enfanta à Yaacov à Paddan-Aram, avec Diynah sa fille. Ses fils et ses filles formaient en tout 33 personnes.
46:16	Et les fils de Gad : Tsiphyon, Haggi, Shouni, Etsbon, Éri, Arodi et Areéli.
46:17	Et les fils d'Asher : Yimnah, Yishvah, Yishviy, Beriy`ah et Sérach, leur sœur. Les fils de Beriy`ah : Héber et Malkiel.
46:18	Ce sont là les fils de Zilpa que Laban donna à Léah, sa fille ; elle les enfanta à Yaacov : 16 personnes en tout.
46:19	Les fils de Rachel, femme de Yaacov, furent Yossef et Benyamin.
46:20	Et il naquit à Yossef dans le pays d'Égypte, Menashè et Éphraïm, qu'Asnath, fille de Poti-Phéra, prêtre d'On, lui enfanta.
46:21	Et les fils de Benyamin étaient Béla, Béker, Ashbel, Guéra, Naaman, Éhi, Rosh, Mouppim, Houppim et Ard.
46:22	Ce sont là les fils de Rachel qu'elle enfanta à Yaacov. En tout 14 personnes.
46:23	Et les fils de Dan : Houshim.
46:24	Et les fils de Nephthali : Yahtseel, Gouni, Yetser et Shillem.
46:25	Ce sont là les fils de Bilhah que Laban donna à Rachel, sa fille, et elle les enfanta à Yaacov. En tout 7 personnes.
46:26	Toutes les personnes appartenant à Yaacov qui vinrent en Égypte et qui étaient issues de lui, sans les femmes des fils de Yaacov, furent en tout 66.
46:27	Et les fils de Yossef qui lui étaient nés en Égypte furent 2 personnes. Toutes les personnes de la maison de Yaacov qui vinrent en Égypte furent 70.
46:28	Yaacov envoya Yéhouda devant lui vers Yossef pour le faire venir devant lui en Gosen. Ils vinrent donc dans la région de Gosen.
46:29	Et Yossef fit atteler son char et y monta pour aller à la rencontre d'Israël, son père, en Gosen. Dès qu'il le vit, il se jeta à son cou et pleura longtemps sur son cou.
46:30	Et Israël dit à Yossef : Je peux mourir maintenant, puisque j'ai vu ton visage et que tu es encore en vie !
46:31	Puis Yossef dit à ses frères et à la famille de son père : Je monterai, et ferai savoir à pharaon, et je lui dirai : Mes frères et la famille de mon père qui étaient en terre de Canaan sont venus vers moi.
46:32	Et ces hommes sont bergers, ils se sont toujours occupés du bétail, et ils ont amené leurs brebis et leurs bœufs, et tout ce qui était à eux.
46:33	Et quand pharaon vous fera appeler et vous dira : Quel est votre métier ?
46:34	Vous direz : Tes serviteurs se sont toujours occupés de bétail dès leur jeunesse jusqu'à maintenant, nous et nos pères. De cette manière, vous habiterez dans la terre de Gosen, car les Égyptiens ont en abomination les bergers.

## Chapitre 47

### La famille de Yaacov honorée en Égypte

47:1	Yossef alla avertir pharaon et lui dit : Mon père et mes frères sont arrivés de la terre de Canaan avec leurs troupeaux et leurs bœufs, et tout ce qui est à eux, et voici, ils sont dans la terre de Gosen.
47:2	Il prit cinq de ses frères, et les présenta à pharaon.
47:3	Et pharaon dit aux frères de Yossef : Quel est votre métier ? Ils répondirent à pharaon : Tes serviteurs sont bergers comme l'ont été nos pères.
47:4	Ils dirent aussi à pharaon : Nous sommes venus séjourner sur cette terre parce qu'il n'y a plus de pâturages pour les troupeaux de tes serviteurs. En effet, la famine pèse lourdement sur la terre de Canaan. Maintenant, nous t'en prions, que tes serviteurs demeurent dans la région de Gosen.
47:5	Et pharaon parla à Yossef et lui dit : Ton père et tes frères sont arrivés auprès de toi.
47:6	Le pays d'Égypte est devant toi. Fais habiter ton père et tes frères dans le meilleur endroit du pays. Qu'ils demeurent dans la région de Gosen. Si tu connais parmi eux des hommes habiles, tu les établiras chefs de tous mes troupeaux.
47:7	Alors Yossef amena Yaacov son père et le présenta à pharaon. Yaacov bénit pharaon.
47:8	Et pharaon dit à Yaacov : Quel est le nombre de jours des années de ta vie ?
47:9	Yaacov répondit à pharaon : Les jours des années de mes pèlerinages sont de 130 ans. Les jours des années de ma vie ont été courts et mauvais et n'ont pas atteint les jours des années de la vie de mes pères, du temps de leurs pèlerinages.
47:10	Yaacov donc bénit pharaon et sortit de devant lui.
47:11	Et Yossef assigna une demeure à son père et à ses frères, et leur donna une possession au pays d'Égypte, au meilleur endroit du pays, en la contrée de Ramsès, comme pharaon l'avait ordonné.
47:12	Et Yossef fournit du pain à son père et à ses frères, et à toute la maison de son père, selon le nombre de leurs familles.
47:13	Or il n'y avait pas de pain sur toute la Terre, car la famine était très grande. Le pays d'Égypte et le pays de Canaan étaient épuisés par la famine.
47:14	Et Yossef recueillit tout l'argent qui se trouvait dans le pays d'Égypte et dans le pays de Canaan contre le blé qu'on achetait, et il apporta l'argent à la maison de pharaon.
47:15	Quand l'argent du pays d'Égypte et du pays de Canaan fut épuisé, tous les Égyptiens vinrent à Yossef, en disant : Donne-nous du pain ! Faut-il que nous mourions devant toi parce que l'argent manque ?
47:16	Yossef répondit : Donnez votre bétail et je vous en donnerai pour votre bétail, puisque l'argent manque.
47:17	Alors ils amenèrent à Yossef leur bétail et Yossef leur donna du pain contre les chevaux, contre les troupeaux de brebis, contre les troupeaux de bœufs et contre les ânes. Ainsi il leur fournit du pain contre leurs troupeaux cette année-là.
47:18	Lorsque cette année fut écoulée, ils revinrent à Yossef l'année suivante et lui dirent : Nous ne cacherons pas à mon seigneur que l'argent est épuisé et les troupeaux de bétail ont été amenés à mon seigneur, il ne nous reste plus rien devant mon seigneur que nos corps et nos terres.
47:19	Pourquoi mourrions-nous sous tes yeux ? Achète-nous avec nos terres contre du pain et nous serons esclaves de pharaon, et nos terres seront à lui. Donne-nous aussi de quoi semer afin que nous vivions et que ne mourrions pas, et que nos terres ne soient pas désolées.
47:20	Ainsi, Yossef acheta toutes les terres de l'Égypte pour pharaon. En effet, les Égyptiens vendirent chacun son champ parce que la famine les pressait. Et le pays devint la propriété de pharaon.
47:21	Et il fit passer le peuple dans les villes, d'un bout à l'autre des frontières de l'Égypte.
47:22	Seulement, il n'acheta pas les terres des prêtres, parce qu'il y avait une loi de pharaon en faveur des prêtres, qui vivaient du revenu que leur assurait pharaon, c'est pourquoi ils ne vendirent pas leurs terres.
47:23	Et Yossef dit au peuple : Voici, je vous ai achetés aujourd'hui, vous et vos terres pour pharaon, voilà de la semence pour ensemencer la terre.
47:24	Et quand le temps de la récolte viendra, vous donnerez la cinquième partie à pharaon et les quatre autres seront à vous, pour ensemencer les champs, et pour votre nourriture, et pour celle de ceux qui sont dans vos maisons, et pour la nourriture de vos petits enfants.
47:25	Et ils dirent : Tu nous sauves la vie ! Que nous trouvions grâce aux yeux de mon seigneur et nous serons esclaves de pharaon.
47:26	Et Yossef fit de cela une loi qui a subsisté jusqu'à ce jour, et d'après laquelle un cinquième du revenu des terres de l'Égypte appartient à pharaon ; il n'y a que les terres des prêtres qui ne soient pas à pharaon.
47:27	Israël habita dans le pays d'Égypte, dans la région de Gosen. Ils eurent des possessions, ils portèrent du fruit et multiplièrent beaucoup.
47:28	Yaacov vécut 17 ans dans le pays d'Égypte. Les jours des années de la vie de Yaacov furent de 147 ans.

### Israël demande à être enterré à Canaan

47:29	Et quand le jour de la mort d'Israël approcha, il appela Yossef son fils et lui dit : Je te prie, si j'ai trouvé grâce à tes yeux, mets présentement ta main sous ma cuisse et jure-moi que tu useras envers moi de bonté et de fidélité : Je te prie, ne m'enterre pas en Égypte !
47:30	Quand je serai couché avec mes pères, tu me transporteras hors de l'Égypte et m'enterreras dans leur sépulcre. Et il répondit : Je le ferai selon ta parole.
47:31	Et Yaacov lui dit : Jure-le-moi ! Et il le lui jura. Et Israël se prosterna sur le chevet du lit.

## Chapitre 48

### Yaacov bénit les fils de Yossef

48:1	Or il arriva après ces choses que l'on vint dire à Yossef : Voici, ton père est malade ! Et il prit avec lui ses deux fils, Menashè et Éphraïm.
48:2	On avertit Yaacov et on lui dit : Voici Yossef ton fils qui vient vers toi. Alors Israël rassembla ses forces et s'assit sur son lit.
48:3	Puis Yaacov dit à Yossef : El Shaddaï m'est apparu à Louz, au pays de Canaan et m'a béni.
48:4	Et il m'a dit : Voici, je te ferai porter du fruit et je te multiplierai, je te ferai devenir une assemblée de peuples et je donnerai ce pays en possession perpétuelle à ta postérité après toi.
48:5	Maintenant, tes deux fils qui te sont nés au pays d'Égypte avant mon arrivée vers toi seront à moi : Éphraïm et Menashè seront à moi comme Reouben et Shim’ôn.
48:6	Mais les enfants que tu auras engendrés après eux, seront à toi, et ils seront appelés selon le nom de leurs frères dans leur héritage.
48:7	À mon retour de Paddan, Rachel mourut en route auprès de moi, dans le pays de Canaan, à quelque distance d'Éphrata. C'est là que je l'ai enterrée, sur le chemin d'Éphrata, qui est Bethléhem.
48:8	Puis Israël vit les fils de Yossef et il dit : Ceux-ci, qui sont-ils ?
48:9	Et Yossef répondit à son père : Ce sont mes fils qu'Elohîm m'a donnés ici. Et il dit : Amène-les-moi, je te prie, afin que je les bénisse.
48:10	Or les yeux d'Israël étaient appesantis par la vieillesse et il ne pouvait plus voir. Et il les fit approcher de lui, les embrassa et les prit dans ses bras.
48:11	Et Israël dit à Yossef : Je ne pensais pas revoir ton visage ; et voici, Elohîm m'a fait voir et toi et ta postérité.
48:12	Et Yossef les retira des genoux de son père et se prosterna le visage contre terre.
48:13	Puis Yossef les prit tous les deux, Éphraïm de sa main droite à la gauche d'Israël, et Menashè de sa main gauche à la droite d'Israël, et il les fit approcher de lui.
48:14	Israël étendit sa main droite et la posa sur la tête d'Éphraïm qui était le plus jeune, et il posa sa main gauche sur la tête de Menashè. Ce fut avec intention qu'il posa ses mains ainsi, car Menashè était le premier-né.
48:15	Il bénit Yossef et dit : Que l'Elohîm en présence duquel ont marché mes pères, Abraham et Yitzhak, que l'Elohîm qui m'a conduit depuis que j'existe jusqu'à ce jour<!--Hé. 11:21.-->,
48:16	que l'Ange qui m'a délivré de tout mal, bénisse ces garçons ! Qu'ils soient appelés de mon nom et du nom de mes pères, Abraham et Yitzhak, et qu'ils multiplient en abondance comme les poissons au milieu du pays.
48:17	Yossef vit que son père avait posé sa main droite sur la tête d'Éphraïm et cela fut mauvais à ses yeux. Il saisit la main de son père pour la détourner de dessus la tête d'Éphraïm et la diriger sur celle de Menashè.
48:18	Et Yossef dit à son père : Ce n'est pas juste, mon père, car celui-ci est l'aîné. Mets ta main droite sur sa tête !
48:19	Mais son père le refusa, en disant : Je le sais, mon fils, je le sais. Lui aussi deviendra un peuple, lui aussi sera grand. Toutefois, son frère qui est plus jeune, sera plus grand que lui et sa postérité deviendra une multitude de nations.
48:20	Il les bénit ce jour-là et dit : C'est par toi qu'Israël bénira en disant : Qu'Elohîm te traite comme Éphraïm et comme Menashè ! Et il mit Éphraïm avant Menashè.
48:21	Puis Israël dit à Yossef : Voici, je vais mourir, mais Elohîm sera avec vous et vous fera retourner au pays de vos pères.
48:22	Et je te donne une portion de plus qu'à tes frères, celle que j'ai prise avec mon épée et mon arc sur les Amoréens.

## Chapitre 49

### Prophétie de Yaacov

49:1	Puis Yaacov appela ses fils et leur dit : Rassemblez-vous et je vous annoncerai ce qui vous arrivera dans les derniers<!--L'adjectif « derniers » vient de l'hébreu « achariyth ». Son équivalent grec est « eschatos » : « dernier », « extrémité ». Yaacov est le premier homme à avoir utilisé l'expression « derniers jours ». Cette promesse de Yaacov devait arriver à Israël dans les derniers jours, selon leurs tribus. Ainsi, les promesses du droit d'aînesse de Ge. 49 étaient pour l'âge messianique, lequel est associé aux derniers jours, et a commencé à la fête de la pentecôte (Ac. 2:14-21). Ces jours impliquent :\\- L'effusion de l'Esprit, le réveil de l'Assemblée (Église) du Mashiah (Mt. 25:1-13 ; Ac. 2).\\- Le réveil des faux prophètes ou l'apostasie (2 Pi. 3:3 ; 1 Jn. 2).\\- La dégradation de la moralité (2 Ti. 3).\\- Le fait qu'Elohîm nous parle par le Fils (Hé. 1:2).\\- La future résurrection des saints lors du retour du Mashiah (Jn. 6:39-54 ; 1 Th. 4:13-17).\\- Le temps des nations (fin des temps) s'achèvera lors du retour visible de Yéhoshoua ha Mashiah pour établir son règne sur toute la terre. Le temps des nations a commencé lorsque, à la suite de l'infidélité d'Israël, la gloire d'Elohîm a quitté le temple et la ville de Yeroushalaim (Ez. 11) ; la puissance fut confiée aux nations en la personne de Neboukadnetsar qui s'empara de Yeroushalaim (2 R. 24-25 ; Jé. 39 ; Da. 1 ; 2 Ch. 36:6-21). Ces temps dureront jusqu'à la destruction finale du dernier empire des nations représenté par la Bête romaine ressuscitée (Ap. 13:3). Cette destruction n'aura lieu que lorsque Yéhoshoua ha Mashiah, la Pierre détachée sans le secours d'aucune main, deviendra une grande montagne qui remplira toute la terre (Mi. 4 ; Da. 2:34). Yeroushalaim ne sera délivrée du joug des nations qu'à ce moment-là. Le temps des nations ne sera accompli que lorsque le trône d'Elohîm sera de nouveau établi à Yeroushalaim.--> jours.
49:2	Rassemblez-vous et écoutez, fils de Yaacov ! Écoutez Israël<!--Le verbe écouter vient de l'hébreu « shama ». C'est sur cette interpellation solennelle de Yaacov que se base le texte principal de la liturgie juive. Récité matin et soir avec des bénédictions, le « Shema » se compose de trois extraits de la torah : De. 6:4, 11:13-21 et No. 15:37-41. Véritable profession de foi, le « Shema » commence par affirmer et rappeler le fondement de la piété juive : Elohîm est un.-->, votre père !
49:3	Reouben, tu es mon premier-né, ma force et le premier de ma vigueur, l’excellence en dignité et l’excellence en force,
49:4	impétueux comme les eaux, tu n'auras pas la prééminence, car tu es monté sur la couche de ton père et tu as souillé mon lit en y montant.
49:5	Shim’ôn et Lévi, sont frères, leurs glaives sont des instruments de violence dans leurs demeures.
49:6	Que mon âme n'entre pas dans leur conseil secret, que ma gloire ne s'unisse pas à leur assemblée, car, dans leur colère, ils ont tué des hommes, et pour leur plaisir, ils ont enlevé des bœufs.
49:7	Maudite soit leur colère, car elle a été violente, et leur fureur, car elle a été cruelle ! Je les diviserai dans Yaacov et les disperserai dans Israël.
49:8	Yéhouda, quant à toi, tes frères te loueront. Ta main sera sur la nuque de tes ennemis. Les fils de ton père se prosterneront devant toi.
49:9	Yéhouda est un jeune lion. Tu es revenu du carnage, mon fils ! Il s'est agenouillé, il s'est couché comme un lion, comme une lionne : Qui le fera lever ?
49:10	Le sceptre ne s'éloignera pas de Yéhouda, ni le bâton de législateur d'entre ses pieds, jusqu'à ce que le Shiyloh<!--« Le Pacifique » ou « le Repos ».--> vienne et que les peuples lui obéissent.
49:11	Il attache à la vigne son ânon et au cep excellent le petit de son ânesse. Il lavera son vêtement dans le vin et son vêtement dans le sang des raisins.
49:12	Il a les yeux rouges de vin et les dents blanches de lait.
49:13	Zebouloun habitera sur la côte des mers, il sera un port des navires et ses côtés s'étendront vers Sidon.
49:14	Yissakar est un âne robuste, couché entre les barres des étables.
49:15	Il voit que le lieu où il repose est agréable et que la contrée est magnifique, il courbe son épaule sous le fardeau, il s'assujettit à un tribut.
49:16	Dan jugera son peuple, comme l'une des tribus d'Israël.
49:17	Dan sera un serpent sur le chemin, une vipère sur le sentier, mordant les talons du cheval, pour que le cavalier tombe à la renverse.
49:18	Ô YHWH ! J'espère en ton salut<!--Le mot salut se dit « yeshuw'ah » en hébreu et veut littéralement dire « salut », « délivrance ». Avant de mourir, Yaacov a donc placé son espérance en Yéhoshoua ha Mashiah qui est la résurrection et la vie (Jn. 11:25). Voir commentaire en Es. 26:1.--> !
49:19	Quant à Gad, des troupes viendront l'attaquer, mais il ravagera leur arrière-garde.
49:20	Le pain excellent viendra d'Asher et il fournira les mets délicats des rois.
49:21	Nephtali est une biche en liberté. Il donne des paroles de bonté.
49:22	Yossef est le fils qui porte du fruit<!--Voir Jn. 15:1-6.-->, le fils qui porte du fruit près de la source<!--Es. 12:3, 55:1 ; Je. 2:13, 17:13 ; Ps. 1:3, 87:7 ; Jn. 7:37 ; Ap. 21:6, 22:17.-->. Les filles enjambent le mur<!--Ou encore : les filles courent sur le mur pour voir Yossef.-->.
49:23	Des archers l'ont provoqué amèrement, ils lui ont tiré dessus, ils ont gardé de la haine contre lui.
49:24	Mais son arc est resté ferme et ses mains ont été fortifiées par les mains du Puissant de Yaacov : Il est ainsi devenu le berger, le rocher d'Israël,
49:25	par le El de ton père qui t'aidera, par Shaddaï qui te bénira des bénédictions des cieux en haut, des bénédictions de l'abîme qui repose en bas, des bénédictions des mamelles et du sein maternel.
49:26	Les bénédictions de ton père ont surpassé les bénédictions de ceux qui m'ont engendré, jusqu'à la cime des antiques collines. Elles seront sur la tête de Yossef et sur le sommet de la tête du Nazaréen d'entre ses frères.
49:27	Benyamin est un loup qui déchirera. Le matin il dévorera la proie et sur le soir il partagera le butin.
49:28	Ce sont là tous ceux qui forment les douze tribus d'Israël. Et c'est là ce que leur père leur dit en les bénissant. Il bénit chacun d'eux selon la bénédiction qui lui était propre.
49:29	Il leur donna aussi cet ordre : Je vais être recueilli auprès de mon peuple, enterrez-moi avec mes pères dans la caverne qui est au champ d'Éphron, le Héthien,
49:30	dans la caverne du champ de Macpéla, vis-à-vis de Mamré, dans le pays de Canaan. C'est le champ qu'Abraham a acheté d'Éphron, le Héthien, comme propriété sépulcrale.
49:31	C'est là qu'on a enterré Abraham avec Sarah, sa femme, c'est là qu'on a enterré Yitzhak et Ribqah, sa femme, et c'est là que j'ai enterré Léah.
49:32	Le champ a été acquis des fils de Heth avec la caverne qui s'y trouve.

### Mort de Yaacov (Jacob)

49:33	Lorsque Yaacov eut achevé de donner ses ordres à ses fils, il retira ses pieds dans le lit, il expira et fut recueilli auprès de son peuple.

## Chapitre 50

50:1	Alors Yossef se jeta sur le visage de son père, pleura sur lui et l'embrassa.
50:2	Et Yossef ordonna à ceux de ses serviteurs qui étaient médecins d'embaumer son père, et les médecins embaumèrent Israël.
50:3	Cela dura 40 jours pleins, car c'était la coutume d'embaumer les corps pendant 40 jours. Les Égyptiens le pleurèrent 70 jours.
50:4	Quand les jours du deuil furent passés, Yossef s'adressa aux gens de la maison de pharaon et leur dit : Si j'ai trouvé grâce à vos yeux, rapportez, je vous prie, à pharaon ce que je vous dis.
50:5	Mon père m'a fait jurer, en disant : Voici, je vais mourir ! Tu m'enterreras dans le sépulcre que je me suis acheté au pays de Canaan. Je voudrais donc y monter pour enterrer mon père ; et je reviendrai.
50:6	Et pharaon répondit : Monte et enterre ton père comme il t'a fait jurer.
50:7	Alors Yossef monta pour enterrer son père et les serviteurs de pharaon, les anciens de la maison de pharaon, et tous les anciens du pays d'Égypte montèrent avec lui.
50:8	Et toute la maison de Yossef, et ses frères, et la maison de son père y montèrent aussi, laissant seulement leurs familles, et leurs troupeaux, et leurs bœufs dans la région de Gosen.
50:9	Il y avait encore avec Yossef des chars et des cavaliers, en sorte que le camp était très nombreux.
50:10	Arrivés à l'aire d'Athad qui est au-delà du Yarden, ils firent entendre de grandes et profondes lamentations. Yossef fit en l'honneur de son père un deuil de sept jours.
50:11	Et les Cananéens, habitants du pays, voyant ce deuil dans l'aire d'Athad, dirent : Ce deuil est grand pour les Égyptiens. C'est pourquoi on a appelé du nom d'Abel-Mitsraïm<!--Deuil de l'Égypte.--> cet endroit qui est au-delà du Yarden.
50:12	Les fils de Yaacov firent à l'égard de son corps ce qu'il leur avait ordonné.
50:13	Ils le transportèrent au pays de Canaan et l'enterrèrent dans la caverne du champ de Macpéla, qu'Abraham avait achetée d'Éphron, le Héthien, comme propriété sépulcrale, et qui est vis-à-vis de Mamré.
50:14	Et après que Yossef eut enseveli son père, il retourna en Égypte avec ses frères et tous ceux qui étaient montés avec lui pour ensevelir son père.
50:15	Et les frères de Yossef, voyant que leur père était mort, se dirent entre eux : Peut-être que Yossef nous haïra, et nous rendra, nous rendra tout le mal que nous lui avons fait.
50:16	Et ils firent dire à Yossef : Ton père a donné cet ordre avant de mourir, en disant :
50:17	Vous parlerez ainsi à Yossef : Oh ! Je te prie, pardonne maintenant l'iniquité de tes frères et leur péché, car ils t'ont fait du mal. Maintenant, je te supplie, pardonne la transgression des serviteurs d'Elohîm de ton père. Et Yossef pleura quand on lui parla.
50:18	Ses frères vinrent eux-mêmes se prosterner devant lui et ils dirent : Nous sommes tes serviteurs.
50:19	Et Yossef leur dit : N'ayez pas peur ! Car suis-je à la place d'Elohîm ?
50:20	Le mal que vous comptiez me faire, Elohîm comptait en faire une bonne chose, pour accomplir ce qui arrive aujourd'hui, pour faire vivre un peuple nombreux.
50:21	Maintenant soyez donc sans crainte ; je vous entretiendrai, vous et vos familles. Et il les consola en parlant à leur cœur.
50:22	Yossef demeura donc en Égypte, lui et la maison de son père, et vécut 110 ans.
50:23	Et Yossef vit les fils d'Éphraïm jusqu'à la troisième génération. Makir aussi, fils de Menashè, eut des fils qui furent élevés sur les genoux de Yossef.
50:24	Et Yossef dit à ses frères : Je vais mourir ! Mais Elohîm vous visitera, il vous visitera et vous fera remonter de ce pays-ci jusque dans le pays qu'il a juré de donner à Abraham, Yitzhak et à Yaacov.
50:25	Et Yossef fit jurer les enfants d'Israël et leur dit : Elohîm vous visitera, il vous visitera et alors vous transporterez mes os d'ici<!--Ex. 13:19 ; Hé. 11:22.-->.
50:26	Puis Yossef mourut, âgé de 110 ans. On l'embauma et on le mit dans un cercueil en Égypte.
