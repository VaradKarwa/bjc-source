# Routh (Ruth) (Ru.)

Signification : Amitié, une amie

Auteur : Inconnu

Thème : Les origines de la famille messianique

Date de rédaction : 11ème siècle av. J.-C.

Au temps des juges, la famine qui frappa le pays de Yéhouda (Juda) poussa Élimélec, sa femme Naomi et leurs deux fils à s'installer dans le pays de Moab. Ils y rencontrèrent Routh qui devint ensuite la belle-fille de Naomi. Après la mort de son mari, cette Moabite montra son attachement à sa belle-mère et à l'Elohîm de celle-ci, qui devint le sien. Sa détermination, sa fidélité, son obéissance et son humilité bouleversèrent sa destinée.

Son histoire est l'image du rachat des nations par Yéhoshoua ha Mashiah (Jésus-Christ), dont elle fut l'une des ancêtres.

## Chapitre 1

### De Yéhouda à Moab

1:1	Et il arriva au temps où les juges jugeaient, qu'il y eût une famine dans le pays. Et un homme de Bethléhem de Yéhouda s'en alla pour séjourner dans la campagne de Moab, lui, sa femme et ses deux fils.
1:2	Et le nom de cet homme était Élimélec, le nom de sa femme Naomi et les noms de ses deux fils, Machlon et Kilyon, Éphratiens, de Bethléhem de Yéhouda. Ils arrivèrent dans la campagne de Moab et s'y établirent.
1:3	Or Élimélec, mari de Naomi, mourut, et elle resta avec ses deux fils.
1:4	Ils prirent des femmes moabites, dont le nom de l'une était Orpa et le nom de la seconde Routh<!--Routh, la Moabite, dont l'ancêtre était issu d'une relation incestueuse (Ge. 19:36-37), est devenue l'ancêtre du Mashiah (Mt. 1:5-6).-->. Et ils demeurèrent là environ 10 ans.
1:5	Puis ses deux fils Machlon et Kilyon moururent. Ainsi cette femme resta seule, privée de ses deux fils et de son mari.

### Naomi renvoie ses belles-filles dans leur famille

1:6	Puis elle se leva avec ses belles-filles, afin de quitter la campagne de Moab. Car elle y avait appris que YHWH avait visité son peuple en lui donnant du pain.
1:7	Ainsi elle partit du lieu où elle était, avec ses deux belles-filles, et elles se mirent en chemin pour retourner dans le pays de Yéhouda.
1:8	Naomi dit à ses deux belles-filles : Allez, retournez chacune à la maison de sa mère ! Que YHWH vous fasse du bien, comme vous en avez fait à ceux qui sont morts et à moi.
1:9	Que YHWH vous fasse trouver du repos à chacune à la maison de son mari. Et elle les embrassa. Mais elles élevèrent leur voix et pleurèrent,
1:10	et lui dirent : Nous retournerons avec toi vers ton peuple.

### Fidélité de Routh à Naomi

1:11	Et Naomi répondit : Retournez, mes filles ! Pourquoi viendriez-vous avec moi ? Ai-je encore des fils dans mon sein afin que vous les ayez pour maris ?
1:12	Retournez, mes filles, allez-vous-en ! Je suis trop âgée pour avoir un mari. Et quand je dirais que j'en ai l'espérance, quand même dès cette nuit je serais avec un mari et que j'enfanterais des fils,
1:13	les attendriez-vous jusqu'à ce qu'ils soient grands ? Resteriez-vous pour cela sans être à un mari ? Non, mes filles ! Certes je suis dans une plus grande amertume que vous parce que la main de YHWH s'est déployée contre moi.
1:14	Alors elles élevèrent leur voix et pleurèrent encore. Orpa embrassa sa belle-mère, mais Routh s'attacha à elle.
1:15	Et Naomi dit à Routh : Voici, ta belle-sœur est retournée vers son peuple et vers ses elohîm. Retourne après ta belle-sœur.
1:16	Mais Routh répondit : Ne me prie pas de te laisser pour m'éloigner de toi ! Car où tu iras, j'irai, et où tu demeureras, je demeurerai ; ton peuple sera mon peuple et ton Elohîm sera mon Elohîm.
1:17	Là où tu mourras, je mourrai, et j'y serai ensevelie. Qu'ainsi me fasse YHWH et plus encore ; il n'y aura que la mort qui me séparera de toi !
1:18	Naomi donc, voyant qu'elle était résolue d'aller avec elle, cessa de lui en parler.

### Arrivée à Bethléhem

1:19	Et elles marchèrent toutes les deux jusqu'à ce qu'elles arrivèrent à Bethléhem. Lorsqu'elles entrèrent à Bethléhem, toute la ville fut agitée à cause d'elles. Et les femmes disaient : Est-ce bien là Naomi ?
1:20	Et elle leur répondit : Ne m'appelez plus Naomi, appelez-moi Mara, car Shaddaï m'a rendue extrêmement amère.
1:21	Je suis partie pleine de biens et YHWH me ramène à vide. Pourquoi m'appelleriez-vous Naomi<!--Le nom Naomi signifie « ma gracieuse ».-->, puisque YHWH m'a abattue, et que Shaddaï m'a affligée ?
1:22	C'est ainsi que revint Naomi et avec elle, Routh, sa belle-fille, la Moabite, qui revenait de la campagne de Moab. Et elles entrèrent dans Bethléhem au commencement de la moisson des orges.

## Chapitre 2

### Routh trouve grâce aux yeux de Boaz

2:1	Or le mari de Naomi avait là un parent, un homme puissant et riche, de la famille d'Élimélec, son nom était Boaz.
2:2	Et Routh, la Moabite, dit à Naomi : Je te prie, laisse-moi aller glaner des épis dans le champ de celui aux yeux duquel je trouverai grâce. Et elle lui répondit : Va, ma fille.
2:3	Elle alla donc et entra glaner dans un champ derrière les moissonneurs. Il arriva qu'elle se trouva accidentellement dans un champ appartenant à Boaz, qui était de la famille d'Élimélec.
2:4	Et voici, Boaz arriva de Bethléhem, et il dit aux moissonneurs : Que YHWH soit avec vous ! Et ils lui répondirent : Que YHWH te bénisse !
2:5	Et Boaz dit à son serviteur qui était établi sur les moissonneurs : À qui est cette jeune fille ?
2:6	Et le serviteur qui était établi sur les moissonneurs répondit et dit : C'est une jeune femme moabite qui est venue avec Naomi du pays de Moab.
2:7	Et elle nous a dit : Laissez-moi, je vous prie, glaner et ramasser des épis entre les gerbes derrière les moissonneurs. Et, depuis qu'elle est venue ce matin jusqu'à présent, elle est restée debout et s'est assise à peine dans la maison.
2:8	Alors Boaz dit à Routh : Écoute, ma fille, ne va pas glaner dans un autre champ. Ne t'éloigne pas d'ici, mais reste ici auprès de mes jeunes filles.
2:9	Que tes yeux soient sur le champ qu'on moissonne et tu iras après elles. N'ai-je pas ordonné à mes serviteurs de ne pas te toucher ? Et si tu as soif, tu iras aux vases et tu boiras de ce que les serviteurs auront puisé.
2:10	Alors elle tomba sur sa face et se prosterna contre terre. Et elle lui dit : Comment ai-je pu trouver grâce à tes yeux pour que tu prêtes attention à moi, alors que je suis une étrangère ?
2:11	Boaz lui répondit et dit : On m'a raconté, on m'a raconté tout ce que tu as fait pour ta belle-mère depuis que ton mari est mort, comment tu as laissé ton père et ta mère, le pays de ta naissance, et tu es venue vers un peuple que tu ne connaissais pas ni d'hier ni d'avant-hier.
2:12	Que YHWH récompense ton œuvre et que ton salaire soit entier de la part de YHWH, l'Elohîm d'Israël, sous les ailes duquel tu es venue te réfugier !
2:13	Et elle dit : Mon seigneur, je trouve grâce à tes yeux, car tu m'as consolée et tu as parlé selon le cœur de ta servante, bien que je ne sois pas, moi, comme l'une de tes servantes.
2:14	Boaz lui dit encore à l'heure du repas : Approche-toi d'ici, et mange du pain, et trempe ton morceau dans le vinaigre. Et elle s'assit à côté des moissonneurs. Et il lui donna du grain rôti, et elle en mangea, et fut rassasiée, et garda le reste.
2:15	Puis, elle se leva pour glaner. Et Boaz ordonna à ses serviteurs en disant : Qu'elle glane même entre les gerbes et ne l'insultez pas.
2:16	Et vous retirerez même pour elle des gerbes quelques épis que vous lui laisserez glaner et vous ne devez pas la réprimander.
2:17	Elle glana donc dans le champ jusqu'au soir et elle battit ce qu'elle avait glané, et il y eut environ un épha d'orge.
2:18	Et elle l'emporta, entra dans la ville, et sa belle-mère vit ce qu'elle avait glané. Elle sortit aussi ce qu'elle avait gardé de reste, après avoir été rassasiée, et elle le lui donna.
2:19	Alors sa belle-mère lui dit : Où as-tu glané aujourd'hui et où as-tu travaillé ? Béni soit celui qui t'a reconnue ! Et elle raconta à sa belle-mère chez qui elle avait travaillé, et dit : L'homme chez qui j'ai travaillé aujourd'hui s'appelle Boaz.
2:20	Et Naomi dit à sa belle-fille : Qu'il soit béni de YHWH, qui n'abandonne pas sa bonté envers les vivants et les morts ! Et Naomi lui dit : Cet homme est un proche parent, et il est un de ceux qui ont sur nous le droit de rachat<!--En hébreu, le verbe « racheter » se dit « ga'al », ce qui signifie « racheter, être racheté, venger, se venger, vengeur de sang ». Ce terme est employé pour désigner le fait d'épouser la veuve d'un frère pour lui susciter une descendance (De. 25:5-6), racheter une terre, un bien, un esclave (Lé. 25:24-55), ou encore, venger une personne assassinée (No. 35:21). Sous l'Ancienne Alliance, la loi prévoyait qu'un proche parent puisse exercer le droit de rachat dans l'un des cas évoqués afin que justice soit rendue et pour éviter que les biens acquis par une famille ne soient dispersés en dehors du clan familial. Le rédempteur était donc celui qui exerçait le droit de rachat par le paiement d'une rançon. Sous la Nouvelle Alliance, le Seigneur Yéhoshoua est le rédempteur suprême qui nous a rachetés de l'esclavage imposé par le diable en donnant sa propre vie en rançon (Ga. 3:13 ; Ro. 3:23-24).-->.
2:21	Et Routh, la Moabite, dit : Il m'a même dit : Reste avec mes serviteurs jusqu'à ce qu'ils aient achevé toute la moisson qui m'appartient.
2:22	Et Naomi dit à Routh, sa belle-fille : Ma fille, il est bon que tu sortes avec ses jeunes filles et qu'on ne te rencontre pas dans un autre champ.
2:23	Elle resta donc avec les jeunes filles de Boaz, afin de glaner jusqu'à la fin de la moisson des orges et la moisson des froments. Puis elle habitait avec sa belle-mère.

## Chapitre 3

### Routh obéit soigneusement aux instructions

3:1	Et Naomi, sa belle-mère, lui dit : Ma fille, ne te chercherai-je pas du repos afin que tu sois heureuse ?
3:2	Maintenant donc Boaz, avec les jeunes filles duquel tu as été, n'est-il pas de notre parenté ? Voici, il vanne cette nuit les orges qui ont été foulées dans l'aire.
3:3	C'est pourquoi lave-toi et oins-toi, puis mets sur toi tes plus beaux habits et descends dans l'aire. Mais ne te fais pas connaître à lui jusqu'à ce qu'il ait achevé de manger et de boire.
3:4	Et quand il se couchera, tu sauras le lieu où il se couche. Puis entre, découvre ses pieds et couche-toi. Alors il te dira ce que tu auras à faire.
3:5	Et elle lui répondit : Je ferai tout ce que tu as dit.
3:6	Elle descendit donc à l'aire et fit tout ce que sa belle-mère lui avait ordonné.
3:7	Boaz mangea et but, et son cœur était joyeux. Il vint se coucher à l'extrémité d'un tas. Elle vint secrètement, découvrit ses pieds, et se coucha.
3:8	Au milieu de la nuit, cet homme eut peur et se tourna, et voici, une femme était couchée à ses pieds.
3:9	Et il lui dit : Qui es-tu ? Et elle répondit : Je suis Routh, ta servante. Étends le pan de ta robe sur ta servante, car tu as le droit de rachat.
3:10	Et il dit : Ma fille, que YHWH te bénisse ! Cette dernière bonté que tu me témoignes est plus grande que la première, car tu n'es pas allée après des jeunes gens, pauvres ou riches.
3:11	Et maintenant, ma fille, n'aie pas peur ! Je ferai pour toi tout ce que tu me diras, car toute la porte de mon peuple sait que tu es une femme talentueuse.
3:12	Maintenant, oui, sûrement, oui, j'ai le droit de rachat, mais il y en a un qui a le droit de rachat, et qui est plus proche que moi.
3:13	Passe ici la nuit. Et demain, s'il veut user envers toi du droit de rachat, à la bonne heure, qu'il te rachète ; mais s'il ne lui plaît pas de te racheter, moi je te rachèterai, YHWH est vivant ! Couche-toi jusqu'au matin.
3:14	Elle se coucha à ses pieds jusqu'au matin, puis elle se leva avant qu'on puisse se reconnaître l'un l'autre. Car il dit : Qu'on ne sache pas qu'une femme est entrée dans l'aire.
3:15	Puis il dit : Donne-moi le manteau qui est sur toi et tiens-le. Elle le tint et il mesura 6 mesures d'orge qu'il posa sur elle. Puis il entra dans la ville.
3:16	Et elle vint vers sa belle-mère qui lui dit : Qui es-tu, ma fille ? Et elle lui raconta tout ce que cet homme avait fait pour elle.
3:17	Et elle dit : Il m'a donné ces 6 mesures d'orge, car il m'a dit : Tu ne retourneras pas à vide vers ta belle-mère.
3:18	Et Naomi dit : Ma fille, reste ici, jusqu'à ce que tu saches comment l'affaire se terminera, car cet homme ne se donnera pas de repos qu'il n'ait achevé cette affaire aujourd'hui.

## Chapitre 4

### Boaz exerce son droit de rachat

4:1	Boaz monta donc à la porte et s'y assit. Et voici, celui qui avait le droit de rachat, et dont Boaz avait parlé, passa. Boaz lui dit : Toi un tel, détourne-toi et assieds-toi ici. Et il se détourna et s'assit.
4:2	Et il<!--Boaz.--> prit dix hommes d'entre les anciens de la ville, et leur dit : Asseyez-vous ici. Et ils s'assirent.
4:3	Puis il dit à celui qui avait le droit de rachat : Naomi, qui est revenue de la terre de Moab, a vendu la portion d'un champ qui appartenait à notre frère Élimélec.
4:4	Et moi, je me suis dit : Je découvrirai ton oreille pour dire : Achète-la devant ceux qui sont assis ici et devant les anciens de mon peuple ! Si tu veux la racheter par droit de rachat, rachète-la ! Mais si tu ne veux pas la racheter, déclare-le-moi afin que je le sache car il n'y a pas d'autre que toi qui ait le droit de rachat, et moi je suis après toi. Il répondit : Je rachèterai.
4:5	Et Boaz dit : Le jour où tu achèteras le champ de la main de Naomi, tu l'achèteras aussi de Routh la Moabite, femme du défunt, pour maintenir le nom du défunt dans son héritage.
4:6	Et celui qui avait le droit de rachat dit : Je ne peux pas racheter pour moi sans détruire mon héritage. Toi rachète pour toi mon droit de rachat, puisque je ne peux pas racheter.
4:7	Autrefois en Israël, pour confirmer une affaire quelconque relative à un rachat ou à un échange, l'homme ôtait sa sandale et la donnait à son parent ; cela servait de témoignage en Israël.
4:8	Celui qui avait le droit de rachat dit à Boaz : Achète toi-même ! Et il ôta sa sandale.
4:9	Alors Boaz dit aux anciens et à tout le peuple : Vous êtes aujourd'hui témoins que j'ai acheté de la main de Naomi tout ce qui appartenait à Élimélec, à Kilyon et à Machlon,
4:10	et que j'ai également acheté pour femme Routh la Moabite, femme de Machlon, pour maintenir le nom du défunt dans son héritage et afin que le nom du défunt ne soit pas retranché d'entre ses frères et de la porte de sa ville. Vous en êtes témoins aujourd'hui !

### Boaz prend Routh pour femme

4:11	Et tout le peuple qui était à la porte et les anciens dirent : Nous en sommes témoins ! Que YHWH fasse que la femme qui entre dans ta maison soit comme Rachel et comme Léah, qui toutes les deux ont bâti la maison d'Israël ! Montre ta puissance dans Éphrata<!--Éphrata, signifiant « lieu de la fécondité », est un autre nom désignant Bethléhem (Mi. 5:2).--> et rends ton nom célèbre dans Bethléhem !
4:12	Que ta maison soit comme la maison de Pérets, que Thamar enfanta à Yéhouda, par la postérité que YHWH te donnera de cette jeune femme !
4:13	Alors Boaz prit Routh et elle devint sa femme. Il alla vers elle, et YHWH lui fit la grâce de concevoir et elle enfanta un fils.
4:14	Et les femmes dirent à Naomi : Béni soit YHWH qui ne t'a pas laissé manquer aujourd'hui d'un homme ayant le droit de rachat, et dont le nom sera proclamé en Israël !
4:15	Il te fera revenir à la vie et sera le soutien de ta vieillesse, car ta belle-fille qui t'aime l'a enfanté, elle qui vaut mieux pour toi que sept fils.
4:16	Alors Naomi prit l'enfant, le mit dans son sein et lui servit de nourrice.

### Obed, le grand-père de David

4:17	Et les voisines lui donnèrent un nom, en disant : Un fils est né à Naomi ! Et elles l'appelèrent du nom d'Obed. Ce fut le père d'Isaï, père de David.
4:18	Et voici la généalogie de Pérets. Pérets engendra Hetsron,
4:19	Hetsron engendra Ram, Ram engendra Amminadab,
4:20	Amminadab engendra Nahshôn, Nahshôn engendra Salmon<!--Ou Salmah.-->,
4:21	Salmon engendra Boaz, Boaz engendra Obed,
4:22	Obed engendra Isaï, et Isaï engendra David.
