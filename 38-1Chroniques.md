# 1 Hayyamim dibre (1 Chroniques) (1 Ch.)

Signification : Actes des journées

Auteur : Probablement Ezra (Esdras)

Thème : Généalogies et Histoire

Date de rédaction : 5ème siècle av. J.-C.

Les deux livres des Hayyamim dibre (Chroniques) constituent des compléments aux livres des Melakhim (Rois) dans la mesure où ils confirment les récits de ceux-ci.

Après avoir établi la généalogie d'Adam à Yaacov (Jacob), puis une généalogie plus détaillée de la descendance de Yaacov jusqu'au retour de la captivité babylonienne, le premier livre des Hayyamim dibre reprend l'histoire du roi David et met un accent particulier sur certains combats qu'il eut à mener, les rapports avec ses serviteurs, ainsi que les préparatifs de la construction du temple. Il présente aussi l'organisation du travail des prêtres et des Lévites au service d'Elohîm et du peuple.

## Chapitre 1

### Généalogie d'Adam à Noah<!--Ge. 5:1-32.-->

1:1	Adam, Sheth, Enowsh<!--Les généalogies se faisaient par les premiers-nés de chaque famille.-->.
1:2	Kénan, Mahalal'el, Yered ;
1:3	Hénoc, Metoushèlah, Lémek.
1:4	Noah, Shem, Cham et Yepheth<!--Ge. 5:1-32.-->.

### Les fils de Yepheth<!--Ge. 10:2-5.-->

1:5	Les fils de Yepheth furent : Gomer, Magog, Madaï, Yavan, Toubal, Méshec et Tiras.
1:6	Les fils de Gomer furent : Ashkenaz, Diphat et Togarmah.
1:7	Les fils de Yavan furent : Éliyshah, Tarsisa, Kittim et Rodanim.

### Les fils de Cham<!--Ge. 10:6-20.-->

1:8	Les fils de Cham furent : Koush, Mitsraïm, Pouth et Canaan.
1:9	Les fils de Koush furent : Saba, Haviylah, Sabta, Ra`mah et Sabteca. Les fils de Ra`mah furent : Séba et Dedan.
1:10	Koush engendra aussi Nimrod qui commença à être puissant sur la Terre.
1:11	Mitsraïm engendra les Loudim, les Anamim, les Lehabim, les Naphtouhim,
1:12	les Patrousim, les Kaslouhim, desquels sont issus les Philistins et les Caphtorim.
1:13	Canaan engendra Sidon, son fils aîné, et Heth ;
1:14	les Yebousiens, les Amoréens, les Guirgasiens,
1:15	les Héviens, les Arkiens, les Siniens,
1:16	les Arvadiens, les Tsemariens et les Hamathiens.

### Les fils de Shem<!--Ge. 10:21-31.-->

1:17	Les fils de Shem furent : Élam, Assour, Arpacshad, Loud, Aram, Outs, Houl, Guéter et Méshec.
1:18	Arpacshad engendra Shélach, et Shélach engendra Héber.
1:19	À Héber naquirent deux fils : le nom de l'un fut Péleg, car en son temps la Terre fut partagée, et le nom de son frère fut Yoqtan.
1:20	Yoqtan engendra Almodad, Shéleph, Hatsarmaveth, Yerach,
1:21	Hadoram, Ouzal, Diqlah,
1:22	Ébal, Abimaël, Séba,
1:23	Ophir, Haviylah et Yobab. Tous ceux-là furent des fils de Yoqtan<!--Ge. 10:2-31.-->.

### De Shem aux fils d'Abraham<!--Ge. 11:10-26.-->

1:24	Shem, Arpacshad, Shélach<!--Ge. 11:10-26.-->,
1:25	Héber, Péleg, Réou,
1:26	Seroug, Nachor, Térach,
1:27	et Abram, qui est Abraham.
1:28	Les fils d'Abraham furent Yitzhak et Yishmael.

### Les fils de Yishmael (Ismaël)<!--Ge. 25:12-18.-->

1:29	Voici leur postérité<!--Ge. 25:12-18.--> : Le premier-né de Yishmael fut Nebayoth, puis Kédar, Adbeel, Mibsam,
1:30	Mishma, Doumah, Massa, Hadad, Téma,
1:31	Yetour, Naphish et Kedmah. Ce sont là les fils de Yishmael.

### Les fils de Ketourah<!--Ge. 25:1-4.-->

1:32	Quant aux fils de Ketourah, concubine d'Abraham, elle enfanta Zimran, Yoqshan, Medan, Madian, Yishbaq et Shouah ; et les fils de Yoqshan furent Séba et Dedan.
1:33	Les fils de Madian furent Éphah, Épher, Hénoc, Abida et Elda`ah. Tous ceux-là furent les fils de Ketourah.

### Les fils de Yitzhak (Isaac)<!--Ge. 25:19-26.-->

1:34	Or Abraham engendra Yitzhak. Les fils de Yitzhak furent Ésav et Israël.

### Les descendants d'Ésav (Ésaü)<!--Ge. 36:1-14.-->

1:35	Les fils d'Ésav furent Éliphaz, Reouel, Yéoush, Ya`lam et Koré<!--Ge. 36:1-14.-->.
1:36	Les fils d'Éliphaz furent Théman, Omar, Tsephi, Gaetham et Kenaz ; Thimna lui enfanta Amalek.
1:37	Les fils de Reouel furent Nahath, Zérach, Shammah et Mizzah.
1:38	Les fils de Séir furent Lothan, Shobal, Tsibeon, Ana, Dishon, Etser et Dishan.
1:39	Les fils de Lothan furent Hori et Homam, et Thimna fut la sœur de Lothan.
1:40	Les fils de Shobal furent Alvan, Manahath, Ébal, Shephi et Onam. Les fils de Tsibeon furent Ayah et Anah.
1:41	Anah eut un fils : Dishon. Les fils de Dishon furent Hamran, Eshban, Yithran et Keran.
1:42	Les fils d'Etser furent Bilhan, Zaavan et Ya`aqan. Les fils de Dishon furent Outs et Aran.

### Les rois et les chefs d'Édom<!--Ge. 36:15-19,25-43.-->

1:43	Voici les rois qui ont régné au pays d'Édom, avant qu'un roi ne règne sur les enfants d'Israël : Béla, fils de Beor, et le nom de sa ville était Dinhabah.
1:44	Béla mourut, et Yobab, fils de Zérach de Botsrah, régna à sa place.
1:45	Yobab mourut, et Housham, du pays des Thémanites, régna à sa place.
1:46	Housham mourut, et Hadad, fils de Bedad, régna à sa place. C'est lui qui frappa Madian dans les champs de Moab. Le nom de sa ville était Avith.
1:47	Hadad mourut, et Samlah de Masreqah, régna à sa place.
1:48	Samlah mourut, et Shaoul de Rehoboth, sur le fleuve, régna à sa place.
1:49	Shaoul mourut, et Baal-Hanan, fils d'Acbor, régna à sa place.
1:50	Baal-Hanan mourut, et Hadad régna à sa place. Le nom de sa ville était Pahi, et le nom de sa femme Mehéthabeel, qui était fille de Mathred, et petite-fille de Mézahab.
1:51	Enfin Hadad mourut. Ensuite vinrent les chefs d'Édom, le chef Thimna, le chef Alvah, le chef Yetheyh.
1:52	Le chef Oholiybamah, le chef Élah, le chef Pinon.
1:53	Le chef Kenaz, le chef Théman, le chef Mibtsar.
1:54	Le chef Magdiel, et le chef Iram. Ce sont là les chefs d'Édom.

## Chapitre 2

### Les douze fils de Yaacov (Israël)<!--Ge. 29:31-35, 30:6-24, 35:16-18.-->

2:1	Voici les fils d'Israël : Reouben, Shim’ôn, Lévi, Yéhouda, Yissakar, Zebouloun,
2:2	Dan, Yossef, Benyamin, Nephthali, Gad et Asher.

### Les descendants de Yéhouda jusqu'aux fils d'Hetsron<!--Ge. 46:12 ; No. 26:19-22.-->

2:3	Les fils de Yéhouda furent Er, Onan, et Shelah. Ces trois lui naquirent de la fille de Shoua, la Cananéenne. Mais Er, premier-né de Yéhouda, fut méchant aux yeux de YHWH, qui le fit mourir.
2:4	Et Tamar, belle-fille de Yéhouda, lui enfanta Pérets et Zérach. Tous les fils de Yéhouda furent 5.
2:5	Les fils de Pérets furent Hetsron et Hamoul.
2:6	Et les fils de Zérach furent Zimri, Éthan, Héman, Calcol et Dara, 5 en tout.
2:7	Fils de Carmi : Acar<!--Voir Jos. 7.-->, qui troubla Israël, lorsqu'il commit un délit au sujet de ce qui était voué à une entière destruction.
2:8	Éthan eut un seul fils : Azaryah.
2:9	Les fils qui naquirent à Hetsron furent Yerachme'el, Ram et Keloubaï.

### Les descendants de Ram jusqu'à David<!--Ru. 4:17-22.-->

2:10	Ram engendra Amminadab et Amminadab engendra Nahshôn, prince des fils de Yéhouda.
2:11	Nahshôn engendra Salma et Salma engendra Boaz.
2:12	Boaz engendra Obed et Obed engendra Isaï.
2:13	Isaï engendra son premier-né Éliab, le second Abinadab, le troisième Shimea,
2:14	le quatrième Netanél, le cinquième Raddaï,
2:15	le sixième Otsem, et le septième, David.
2:16	Tserouyah et Abigaïl furent leurs sœurs. Tserouyah eut 3 fils : Abishaï, Yoab, et Asaël.
2:17	Abigaïl enfanta Amasa, dont le père fut Yether le Yishmaelite.

### Les descendants de Caleb

2:18	Or Caleb, fils de Hetsron, eut des enfants d'Azouba sa femme, et aussi de Yerioth ; et ses fils furent Yesher, Shobab et Ardon.
2:19	Azouba mourut, et Caleb prit pour femme Éphrath, qui lui enfanta Hour.
2:20	Hour engendra Ouri, et Ouri engendra Betsaleel.
2:21	Après cela, Hetsron vint vers la fille de Makir, père de Galaad, et la prit pour sa femme ; il était âgé de 60 ans, et elle lui enfanta Segoub.
2:22	Segoub engendra Yaïr, qui eut 23 villes au pays de Galaad.
2:23	Il prit sur Guéshour et sur la Syrie les bourgades de Yaïr, et Kenath, avec les villes de son ressort, au nombre de 60. Tous ceux-là furent fils de Makir, père de Galaad.
2:24	Après la mort de Hetsron, à Caleb-Éphratha, la femme de Hetsron, Abiyah, lui enfanta Ashhour, père de Tekoa.
2:25	Les fils de Yerachme'el, premier-né de Hetsron furent : Ram, son fils aîné, puis Bouna, Oren et Otsem, nés d'Achiyah.
2:26	Yerachme'el eut aussi une autre femme, dont le nom était Athara, qui fut mère d'Onam.
2:27	Les fils de Ram, premier-né de Yerachme'el, furent Maats, Yamin et Éker.
2:28	Les fils d'Onam furent Shammaï et Yada, et les fils de Shammaï furent Nadab et Abishour.
2:29	Le nom de la femme d'Abishour fut Abichaïl, qui lui enfanta Achban et Molid.
2:30	Les fils de Nadab furent Séled et Appaïm, mais Séled mourut sans fils.
2:31	Appaïm eut un seul fils : Yish`iy. Yish`iy eut un seul fils : Shéshan. Shéshan n'eut qu'Achlaï.
2:32	Les fils de Yada, frère de Shammaï, furent Yether et Yonathan ; mais Yether mourut sans fils.
2:33	Les fils de Yonathan furent Péleth et Zara. Ce furent là les fils de Yerachme'el.
2:34	Shéshan n'eut pas de fils, mais des filles. Or il avait un esclave Égyptien dont le nom était Yarcha.
2:35	Shéshan donna sa fille pour femme à Yarcha, son esclave, et elle lui enfanta Attaï.
2:36	Attaï engendra Nathan, et Nathan engendra Zabad ;
2:37	Zabad engendra Éphlal ; et Éphlal engendra Obed ;
2:38	Obed engendra Yehuw ; Yehuw engendra Azaryah ;
2:39	Azaryah engendra Halets ; Halets engendra Élasa ;
2:40	Élasa engendra Sismaï ; Sismaï engendra Shalloum ;
2:41	Shalloum engendra Yeqamyah ; Yeqamyah engendra Éliyshama.

### Les autres fils de Caleb

2:42	Les fils de Caleb, frère de Yerachme'el, furent Mésha, son premier-né, qui fut le père de Ziph, et les fils de Maréshah, père d'Hébron.
2:43	Les fils d'Hébron furent Koré, Thappuach, Rékem et Shéma.
2:44	Shéma engendra Racham, père de Yorqeam, et Rékem engendra Shammaï.
2:45	Le fils de Shammaï fut Maon. Maon fut père de Beth-Tsour.
2:46	Et Éphah, concubine de Caleb, enfanta Haran, Motsa et Gazez ; Haran aussi engendra Gazez.
2:47	Les fils de Yehday furent Réguem, Yotham, Guéshan, Péleth, Éphah et Shaaph.
2:48	Ma'akah, la concubine de Caleb, enfanta Shéber et Tirchana.
2:49	La femme de Shaaph, père de Madmannah, enfanta Sheva, père de Macbéna, et le père de Guibea, et la fille de Caleb fut Acsa.

### Les descendants de Hour, fils de Caleb<!--Cp. 1 Ch. 4:1.-->

2:50	Ceux-ci furent les fils de Caleb, fils de Hour, premier-né d'Éphrata : Shobal, père de Qiryath-Yéarim.
2:51	Salma, père de Bethléhem, Hareph, père de Beth-Gader ;
2:52	Shobal, père de Qiryath-Yéarim, eut des fils : Haroé et Hatsi-Hammenouhoth.
2:53	Les familles de Qiryath-Yéarim furent les Yithriens, les Poutites, les Shoumatiens et les Mishraïens, desquels sont sortis les Tsoreathiens et les Eshthaoliens.
2:54	Les fils de Salma : Bethléhem et les Nethophathiens, Athroth-Beth-Yoab, Hatsi-Hammanachthi et les Tsoreïens.
2:55	Et les familles des scribes, qui habitaient à Yahbets : les Thireathiens, les Shimeathiens, les Soukkatiens ; ce sont les Kéniens, qui sont sortis de Hamath père de Récab.

## Chapitre 3

### Les fils de David<!--2 S. 3:2-5, 5:13-16.-->

3:1	Voici les fils de David, qui lui naquirent à Hébron<!--2 S. 3:2-5.--> : le premier-né fut Amnon, fils d'Achinoam de Yizre`e'l ; le second Daniye'l, d'Abigaïl de Carmel ;
3:2	le troisième, Absalom, fils de Ma'akah, fille de Talmaï, roi de Guéshour ; le quatrième, Adoniyah, fils de Haggith ;
3:3	le cinquième, Shephatyah, d'Abithal ; le sixième, Yithream, d'Églah sa femme.
3:4	Ces six lui naquirent à Hébron, où il régna 7 ans et 6 mois. Puis il régna 33 ans à Yeroushalaim.
3:5	Ceux-ci lui naquirent à Yeroushalaim : Shimea, Shobab, Nathan et Shelomoh, tous quatre de Bath-Shoua, fille d'Ammiel ;
3:6	et Yibhar, Éliyshama, Éliphéleth,
3:7	Noga, Népheg, Yaphiya,
3:8	Éliyshama, Éliada et Éliphéleth, qui sont 9.
3:9	Ce sont tous des fils de David, outre les fils de ses concubines. Et Tamar était leur sœur.

### De Shelomoh (Salomon) à Tsidqiyah (Sédécias)

3:10	Le fils de Shelomoh fut Rehabam. Abiyah, son fils ; Asa, son fils ; Yehoshaphat, son fils ;
3:11	Yoram, son fils ; Achazyah, son fils ; Yoash, son fils ;
3:12	Amatsyah, son fils ; Azaryah, son fils ; Yotham, son fils ;
3:13	Achaz, son fils ; Hizqiyah, son fils ; Menashè, son fils ;
3:14	Amon, son fils ; Yoshiyah, son fils.
3:15	Les fils de Yoshiyah furent Yohanan, son premier-né ; le deuxième, Yehoyaqim ; le troisième Tsidqiyah ; le quatrième, Shalloum.
3:16	Les fils de Yehoyaqim furent Yekonyah, son fils, qui eut pour fils Tsidqiyah.

### Les fils de Yekonyah

3:17	Quant aux fils de Yekonyah, Assir qui fut emmené en captivité, Shealthiel fut son fils ;
3:18	dont les fils furent Malkiram, Pedayah, Shénatsar, Yeqamyah, Hoshama et Nedabyah.
3:19	Les fils de Pedayah furent Zorobabel et Shimeï ; et les fils de Zorobabel furent Meshoullam et Chananyah, et Shelomiyth était leur sœur.
3:20	De Meshoullam, Hashouba, Ohel, Berekyah, Hasadia et Youshab-Hésed, en tout 5.
3:21	Les fils de Chananyah furent Pelatyah et Yesha`yah ; les fils de Rephayah, les fils d'Arnan, les fils d'Obadyah et les fils de Shekanyah.
3:22	De Shekanyah naquit Shema’yah ; et les fils de Shema’yah, Hattoush, Yigeal, Bariach, Ne`aryah, Shaphath, en tout 6.
3:23	Les fils de Ne`aryah furent 3 : Elyoénaï, Hizqiyah et Azrikam.
3:24	Et les fils d'Elyoénaï furent 7 : Hodayevah, Éliashib, Pelayah, Aqoub, Yohanan, Delayah, et Anani.

## Chapitre 4

### Les autres fils de Hour<!--1 Ch. 2:50.-->

4:1	Les fils de Yéhouda furent Pérets, Hetsron, Carmi, Hour et Shobal.
4:2	Reayah, fils de Shobal, engendra Yahath ; et Yahath engendra Ahoumaï et Lahad. Ce sont les familles des Tsoreathiens.
4:3	Voici les descendants du père d'Étham : Yizre`e'l, Yishma, et Yidbash ; le nom de leur sœur était Hatselelponi.
4:4	Penouel, père de Guedor, et Ézer, père de Housha, sont les fils de Hour, premier-né d'Éphrata, père de Bethléhem.

### Les descendants d'Ashhour<!--1 Ch. 2:24.-->

4:5	Ashhour, père de Tekoa, eut deux femmes : Hélea et Naara.
4:6	Naara lui enfanta Ahouzzam, Hépher, Thémeni et Achashthari. Ce sont là les fils de Naara.
4:7	Les fils de Hélea furent Tséreth, Tsochar et Ethnan.
4:8	Kots engendra Anoub, Hatsobéba et les familles d'Acharchel, fils de Haroum.
4:9	Yahbets fut plus honoré que ses frères. Sa mère lui avait donné le nom de Yahbets, parce que, dit-elle, je l'ai enfanté avec douleur.

### Yahbets invoque Elohîm

4:10	Yahbets invoqua l'Elohîm d'Israël en disant : Si tu me bénis, me bénis, tu agrandiras mon territoire, ta main sera avec moi et tu me préserveras du malheur pour que je ne sois pas affligé. Et Elohîm lui accorda ce qu'il avait demandé.

### Les fils de Yéhouda et de Caleb

4:11	Keloub, frère de Shouha, engendra Mechir, qui fut père d'Eshthon.
4:12	Et Eshthon engendra la maison de Rapha, Paséach et Thechinna, père de la ville de Nachash. Ce sont là les gens de Réca.
4:13	Les fils de Kenaz furent Othniel et Serayah. Et le fils d'Othniel, Hathath.
4:14	Meonothaï engendra Ophrah ; et Serayah engendra Yoab, père de la vallée des ouvriers, car ils étaient ouvriers.
4:15	Les fils de Caleb, fils de Yephounné, furent Irou, Élah et Naam, et les fils d'Élah, Kenaz.
4:16	Les fils de Yehalléleel furent Ziph, Zipha, Tiyreya, et Asareel.
4:17	Les fils d'Ezra furent Yether, Méred, Épher, et Yalon. Elle conçut Myriam, Shammaï, et Yishbach, père d'Eshthemoa.
4:18	Sa femme, la Juive, enfanta Yered, père de Guedor, Héber, père de Soco, et Yeqoutiel, père de Zanoach. Ceux-là sont les fils de Bithyah, fille de pharaon, que Méred prit pour femme.
4:19	Les fils de la femme de Hodiyah, sœur de Nacham : Le père de Qe'iylah, le Garmien, et Eshthemoa, le Maakathien.
4:20	Et les fils de Shiymôn furent Amnon, Rinna, Ben-Hanan et Thilon. Les fils de Yish`iy furent Zocheth et Ben-Zocheth.

### Les fils de Yéhouda par Shelah<!--1 Ch. 2:3.-->

4:21	Les fils de Shelah, fils de Yéhouda : Er, père de Léca, Laeda, père de Maréshah, et les familles de la maison où l'on travaille le byssus, de la maison d'Ashbéa.
4:22	Yoqiym, et les gens de Cozéba, Yoash et Saraph dominèrent sur Moab, avec Yashoubi-Léhem. Mais ce sont là des choses anciennes.
4:23	C'étaient les potiers et les habitants des plantations et des parcs. Ils cohabitaient là chez le roi et œuvraient pour lui.

### Les descendants de Shim’ôn ; leurs terres et leurs conquêtes

4:24	Les fils de Shim’ôn furent Nemouel, Yamin, Yarib, Zérach et Shaoul.
4:25	Shalloum son fils, Mibsam son fils, et Mishma son fils.
4:26	Les fils de Mishma furent Hammouel son fils, Zakkour son fils, et Shimeï son fils.
4:27	Shimeï eut 16 fils et 6 filles ; mais ses frères n'eurent pas beaucoup de fils, et toute leur famille ne put être aussi nombreuse que celle des fils de Yéhouda.
4:28	Ils habitèrent à Beer-Shéba, à Moladah, à Hatsar-Shoual,
4:29	à Bilhah, à Etsem, à Tholad,
4:30	à Betouel, à Hormah, à Tsiklag,
4:31	à Beth-Marcaboth, à Hatsar-Sousim, à Beth-Bireï, et à Shaaraïm. Ce furent là leurs villes jusqu'au temps où David devint roi.
4:32	Leurs villages furent Étham, Aïn, Rimmon, Thoken, et Ashan, 5 villes ;
4:33	et tous leurs villages, qui étaient autour de ces villes-là, jusqu'à Baal. Ce sont là leurs habitations et leur généalogie :
4:34	Meshobab, Yamlek, Yoshah fils d'Amatsyah ;
4:35	Yoel, Yehuw fils de Yoshibyah, fils de Serayah, fils d'Asiel ;
4:36	Elyoénaï, Yaacovah, Yeshohayah, Asayah, Adiel, Yesiyma'el, Benayah,
4:37	Ziza, fils de Shipheï, fils d'Allon, fils de Yedayah, fils de Shimri, fils de Shema’yah.
4:38	Ceux-là furent désignés pour être des princes dans leurs familles, et les maisons de leurs pères s'étendirent abondamment.
4:39	Et ils allèrent pour entrer dans Guedor, jusqu'à l'orient de la vallée, cherchant des pâturages pour leurs troupeaux.
4:40	Ils trouvèrent des pâturages gras et bons, et un pays spacieux, paisible et fertile, car ceux qui habitaient là auparavant étaient descendus de Cham.
4:41	Ceux-ci, dont les noms sont inscrits vinrent du temps de Yehizqiyah, roi de Yéhouda. Ils détruisirent leurs tentes et les habitations qui se trouvaient là, ils les vouèrent à l'interdit jusqu'à ce jour. Puis ils habitèrent à leur place, car il y avait là des pâturages pour leurs troupeaux.
4:42	500 hommes d'entre eux, c'est-à-dire des fils de Shim’ôn, s'en allèrent à la montagne de Séir, et ils avaient à leur tête Pelatyah, Ne`aryah, Rephayah, et Ouziel, fils de Yish`iy.
4:43	Ils frappèrent le reste des rescapés d'Amalek, et ils demeurèrent là jusqu'à ce jour.

## Chapitre 5

### Les descendants de Reouben jusqu'au temps des captivités

5:1	Les fils de Reouben, le premier-né d'Israël. En effet, il était le premier-né, mais, parce qu'il avait souillé le lit de son père<!--Ge. 35:22.-->, son droit d'aînesse fut donné aux fils de Yossef, fils d'Israël, ainsi il ne fut pas enregistré dans les généalogies comme le premier-né.
5:2	En effet, Yéhouda fut puissant parmi ses frères et de lui est issu le chef, mais le droit d'aînesse est à Yossef.
5:3	Les fils de Reouben, premier-né d'Israël, furent donc Hénoc, Pallou, Hetsron, et Carmi.
5:4	Les fils de Yoel furent Shema’yah son fils, Gog son fils, Shimeï son fils,
5:5	Miykah son fils, Reayah son fils, Baal son fils,
5:6	Beéra son fils, qui fut emmené captif par Tilgath-Pilnéser, roi d'Assyrie. C'est lui qui était le prince des Reoubénites.
5:7	Ses frères, selon leurs familles, d'après le registre généalogique et selon leurs générations, avaient pour chefs Yéiël et Zekaryah.
5:8	Béla, fils d'Azaz, fils de Shéma, fils de Yoel, habitait depuis Aroër jusqu'à Nebo et Baal-Meon.
5:9	Ensuite, il habita du côté de l'orient jusqu'à l'entrée du désert, depuis le fleuve d'Euphrate. Car son bétail s'était multiplié dans le pays de Galaad.
5:10	Du temps de Shaoul, ils firent la guerre contre les Hagaréniens, qui tombèrent par leurs mains, et ils habitèrent dans leurs tentes, dans toute la partie orientale de Galaad.

### Les descendants de Gad et leurs villes

5:11	Les fils de Gad habitaient près d'eux, au pays de Bashân, jusqu'à Salca.
5:12	Yoel fut le premier chef, et Shapham le deuxième après lui, puis Ya`anay, puis Shaphath en Bashân.
5:13	Et leurs frères, selon la maison de leurs pères, furent 7 : Miyka'el, Meshoullam, Shéba, Yoray, Ya`kan, Zia, et Héber.
5:14	Ceux-ci furent les fils d'Abichaïl, fils de Houri, fils de Yarowach, fils de Galaad, fils de Miyka'el, fils de Yeshiyshay, fils de Yachdow, fils de Bouz.
5:15	Achi, fils d'Abdiel, fils de Gouni, fut le chef de la maison de leurs pères.
5:16	Ils habitèrent en Galaad, et en Bashân, dans les villes de son ressort, et dans tous les faubourgs de Saron, jusqu'à leurs limites.
5:17	Tous ceux-ci furent inscrits dans la généalogie du temps de Yotham, roi de Yéhouda, et du temps de Yarobam, roi d'Israël.

### Captivité de Reouben (Ruben), Gad et la demi-tribu de Menashè (Manassé)

5:18	Les fils de Reouben, les Gadites et la demi-tribu de Menashè avaient des fils talentueux, des hommes portant le bouclier et l'épée, tirant de l'arc et exercés à la guerre, au nombre de 44 760, en état d'aller à l'armée.
5:19	Ils firent la guerre contre les Hagaréniens, contre Yetour, Naphish, et Nodab.
5:20	Et ils reçurent du secours contre eux, de sorte que les Hagaréniens, et tous ceux qui étaient avec eux furent livrés entre leurs mains, parce qu'ils crièrent à Elohîm dans la bataille, et il les exauça parce qu'ils avaient mis leur confiance en lui.
5:21	Ainsi ils prirent leurs troupeaux, consistant en 50 000 chameaux, 250 000 brebis, 2 000 ânes, avec 100 000 âmes humaines ;
5:22	car il y eut beaucoup de morts, parce que la bataille venait d'Elohîm. Ils habitèrent là, à leur place, jusqu'au temps de la déportation.
5:23	Les fils de la demi-tribu de Menashè habitèrent aussi dans ce pays-là, et s'étendirent depuis Bashân jusqu'à Baal-Hermon et à Sheniyr, à la montagne de l'Hermon ; ils étaient nombreux.
5:24	Et voici les chefs de la maison de leurs pères : Épher, Yish`iy, Éliel, Azriel, Yirmeyah, Hodavyah, et Yachdiy'el, hommes vaillants et talentueux, des hommes de réputation, qui étaient chefs des maisons de leurs pères.
5:25	Mais ils péchèrent contre l'Elohîm de leurs pères, et se prostituèrent après les elohîm des peuples du pays, qu'Elohîm avait détruits devant eux.
5:26	L'Elohîm d'Israël excita l'esprit de Poul, roi d'Assyrie, et l'esprit de Thilgath-Pilnéser, roi d'Assyrie, qui emmena en captivité les Reoubénites, les Gadites et la demi-tribu de Menashè, et il les emmena à Chalach, à Chabor, à Hara, et au fleuve de Gozan, où ils sont restés jusqu'à ce jour.

## Chapitre 6

### Les fils de Kehath le Lévite, jusqu'à la captivité

6:1	Les fils de Lévi furent Guershon, Kehath et Merari.
6:2	Les fils de Kehath furent Amram, Yitshar, Hébron, et Ouziel.
6:3	Et les fils d'Amram furent Aaron, Moshè et Myriam. Les fils d'Aaron furent Nadab, Abihou, Èl’azar et Ithamar.
6:4	Èl’azar engendra Phinées, et Phinées engendra Abishoua.
6:5	Abishoua engendra Bouqqi, et Bouqqi engendra Ouzzi.
6:6	Ouzzi engendra Zerachyah, et Zerachyah engendra Merayoth.
6:7	Merayoth engendra Amaryah, et Amaryah engendra Ahitoub.
6:8	Ahitoub engendra Tsadok, et Tsadok engendra Achimaats.
6:9	Achimaats engendra Azaryah, et Azaryah engendra Yohanan.
6:10	Yohanan engendra Azaryah, qui exerça la prêtrise au temple que Shelomoh bâtit à Yeroushalaim.
6:11	Azaryah engendra Amaryah, et Amaryah engendra Ahitoub.
6:12	Ahitoub engendra Tsadok, et Tsadok engendra Shalloum.
6:13	Shalloum engendra Chilqiyah, et Chilqiyah engendra Azaryah.
6:14	Azaryah engendra Serayah, et Serayah engendra Yehotsadaq,
6:15	Yehotsadaq s'en alla, quand YHWH emmena en exil Yéhouda et Yeroushalaim par le moyen de Neboukadnetsar.

### Les fils de Guershon, Kehath et Mérari

6:16	Les fils de Lévi furent donc Guershon, Kehath et Merari.
6:17	Voici les noms des fils de Guershon : Libni et Shimeï.
6:18	Les fils de Kehath furent Amram, Yitshar, Hébron et Ouziel.
6:19	Les fils de Merari furent Machli et Moushi. Ce sont là les familles des Lévites, selon les maisons de leurs pères.
6:20	De Guershon, Libni, son fils, Yahath, son fils, Zimma, son fils,
6:21	Yoach, son fils, Iddo, son fils, Zérach, son fils, Yeatheray, son fils.
6:22	Des fils de Kehath, Amminadab, son fils, Koré, son fils, Assir, son fils,
6:23	Elkana, son fils, Ebyacaph, son fils, Assir, son fils,
6:24	Thachath, son fils, Ouriel, son fils, Ouzyah, son fils, et Shaoul, son fils.
6:25	Les fils d'Elkana furent Amasaï, Achimoth ;
6:26	Elkana, son fils ; les fils d'Elkana furent Elkana-Tsophaï, son fils, Nachath son fils,
6:27	Éliab son fils, Yeroham, son fils, Elkana, son fils.
6:28	Quant aux fils de Shemouél, fils d'Elkana, son fils aîné fut Vashni, puis Abiyah.
6:29	Les fils de Merari furent Machli, Libni, son fils, Shimeï, son fils, Ouzza, son fils,
6:30	Shimea, son fils, Chaggiyah, son fils, Asayah, son fils.

### Les chefs des chanteurs

6:31	Or voici ceux que David établit pour la direction de la musique dans la maison de YHWH, depuis que l'arche fut en lieu de repos.
6:32	Ils faisaient le service comme chantres devant le tabernacle, devant la tente d'assignation, jusqu'à ce que Shelomoh eût bâti la maison de YHWH à Yeroushalaim ; ils continuèrent dans leur service selon l'ordonnance qui était prescrite.
6:33	Voici ceux qui firent le service avec leurs fils : d'entre les fils des Kehathites, Héman le chanteur, fils de Yoel, fils de Shemouél,
6:34	fils d'Elkana, fils de Yeroham, fils d'Éliel, fils de Thoach,
6:35	fils de Tsouph, fils d'Elkana, fils de Machath, fils d'Amasaï,
6:36	fils d'Elkana, fils de Yoel, fils d'Azaryah, fils de Tsephanyah,
6:37	fils de Thachath, fils d'Assir, fils d'Ebyacaph, fils de Koré,
6:38	fils de Yitshar, fils de Kehath, fils de Lévi, fils d'Israël.
6:39	Son frère Asaph, qui se tenait à sa droite. Asaph était fils de Berekyah, fils de Shimea,
6:40	fils de Miyka'el, fils de Ba`aseyah, fils de Malkiyah,
6:41	fils d'Ethni, fils de Zérach, fils d'Adayah,
6:42	fils d'Éthan, fils de Zimma, fils de Shimeï,
6:43	fils de Yahath, fils de Guershon, fils de Lévi.
6:44	Les fils de Merari, leurs frères étaient à la gauche ; Éthan, fils de Kishi, fils d'Abdi, fils de Mallouk,
6:45	fils de Chashabyah, fils d'Amatsyah, fils de Chilqiyah,
6:46	fils d'Amtsi, fils de Bani, fils de Shémer,
6:47	fils de Machli, fils de Moushi, fils de Merari, fils de Lévi.
6:48	Et leurs autres frères Lévites furent ordonnés pour tout le service du tabernacle de la maison d'Elohîm.
6:49	Mais Aaron et ses fils faisaient brûler des offrandes sur l'autel de l'holocauste et sur l'autel de l'encens. Ils assumaient tout le service du Saint des saints, et ils faisaient la propitiation pour Israël, selon tout ce qu'avait ordonné Moshè, serviteur d'Elohîm.

### Les prêtres d'Aaron à Achimaats

6:50	Voici les fils d'Aaron : Èl’azar, son fils, Phinées, son fils, Abishoua, son fils,
6:51	Bouqqi, son fils, Ouzzi, son fils, Zerachyah, son fils,
6:52	Merayoth, son fils, Amaryah, son fils, Ahitoub, son fils,
6:53	Tsadok, son fils, Achimaats, son fils.

### Villes des fils d'Aaron et des Lévites

6:54	Voici leurs lieux d'habitation, selon leurs demeures et leurs limites. Aux fils d'Aaron, qui appartiennent à la famille des Kehathites, désignés par le sort,
6:55	on leur donna Hébron dans le pays de Yéhouda, et ses faubourgs tout autour.
6:56	Mais on donna à Caleb, fils de Yephounné, le territoire de la ville et ses villages.
6:57	On donna donc aux fils d'Aaron, d'entre les villes de refuge, Hébron, Libnah et ses faubourgs, Yattiyr et Eshthemoa, avec leurs faubourgs,
6:58	Hilen, avec ses faubourgs, Debir avec ses faubourgs,
6:59	Ashan avec ses faubourgs, et Beth-Shémesh avec ses faubourgs.
6:60	De la tribu de Benyamin, Guéba, avec ses faubourgs, Allémeth avec ses faubourgs, et Anathoth avec ses faubourgs. Toutes leurs villes, selon leurs familles, étaient au nombre de 13.
6:61	On donna au reste des fils de Kehath, par le sort, 10 villes des familles de la demi-tribu, c'est-à-dire de la demi-tribu de Menashè.
6:62	Et aux fils de Guershon, selon leurs familles, de la tribu de Yissakar, de la tribu d'Asher, de la tribu de Nephthali, et de la tribu de Menashè en Bashân, 13 villes.
6:63	Aux fils de Merari, selon leurs familles, par le sort, 12 villes, de la tribu de Reouben, de la tribu de Gad, et de la tribu de Zebouloun.
6:64	Ainsi, les enfants d'Israël donnèrent aux Lévites ces villes-là, avec leurs faubourgs.
6:65	Et ils donnèrent, par le sort, de la tribu des fils de Yéhouda, de la tribu des fils de Shim’ôn, et de la tribu des fils de Benyamin, ces villes qu'ils désignèrent par leurs noms.
6:66	Et pour les autres familles des fils de Kehath, ils eurent pour territoire des villes de la tribu d'Éphraïm.
6:67	Car on leur donna entre les villes de refuge, Sichem avec ses faubourgs, dans la montagne d'Éphraïm, Guézer avec ses faubourgs,
6:68	Yoqme`am avec ses faubourgs, Beth-Horon avec ses faubourgs,
6:69	Ayalon avec ses faubourgs, et Gath-Rimmon avec ses faubourgs.
6:70	De la demi-tribu de Menashè, Aner avec ses faubourgs, et Bileam avec ses faubourgs, on donna ces villes-là aux familles qui restaient des fils de Kehath.
6:71	Aux fils de Guershon, on donna, des familles de la demi-tribu de Menashè, Golan en Bashân avec ses faubourgs, et Ashtaroth, avec ses faubourgs.
6:72	De la tribu de Yissakar, Kédesh avec ses faubourgs, Dobrath avec ses faubourgs,
6:73	Ramoth avec ses faubourgs, et Anem avec ses faubourgs.
6:74	Et de la tribu d'Asher, Mashal, avec ses faubourgs, Abdon, avec ses faubourgs,
6:75	Houqoq avec ses faubourgs, et Rehob avec ses faubourgs.
6:76	De la tribu de Nephthali, Kédesh en Galilée avec ses faubourgs, Hammon avec ses faubourgs, et Qiryathayim avec ses faubourgs.
6:77	Aux fils de Merari, qui étaient le reste d'entre les Lévites, on donna, de la tribu de Zebouloun, Rimmono avec ses faubourgs et Thabor avec ses faubourgs.
6:78	Au-delà du Yarden<!--Jourdain.-->, vis-à-vis de Yeriycho, vers l'orient du Yarden, de la tribu de Reouben, Betser dans le désert avec ses faubourgs, Yahtsa avec ses faubourgs,
6:79	Kedémoth avec ses faubourgs, et Méphaath avec ses faubourgs.
6:80	De la tribu de Gad, Ramoth en Galaad avec ses faubourgs, Mahanaïm avec ses faubourgs,
6:81	Hesbon avec ses faubourgs, et Ya`azeyr avec ses faubourgs.

## Chapitre 7

### Les descendants de Yissakar

7:1	Les fils de Yissakar furent Thola, Poua, Yashoub et Shimron, 4.
7:2	Les fils de Thola furent Ouzzi, Rephayah, Yeriel, Yahmaï, Yibsam et Shemouél, chefs des maisons de leurs pères qui étaient de Thola, gens vaillants et talentueux dans leurs générations. Leur nombre, aux jours de David, était de 22 600.
7:3	Le fils d'Ouzzi : Yizrachyah. Et les fils de Yizrachyah : Miyka'el, Obadyah, Yoel, et Yishshiyah, en tout 5 chefs.
7:4	Ils avaient avec eux, selon leurs générations, et selon les familles de leurs pères, 36 000 hommes de troupe, armés pour la guerre, car ils avaient beaucoup de femmes et de fils.
7:5	Leurs frères selon toutes les familles de Yissakar, hommes vaillants et talentueux, formaient un total de 87 000 enregistrés dans les généalogies.

### Les descendants de Benyamin

7:6	Les fils de Benyamin furent Béla, Béker et Yediyael, 3<!--Benyamin avait encore d'autres fils (Ge. 46:21 ; No. 26:38-41 ; 1 Ch. 8:1-2).-->.
7:7	Les fils de Béla furent Etsbon, Ouzzi, Ouziel, Yeriymoth et Iri, cinq chefs des familles de leurs pères, hommes vaillants et talentueux, et leur dénombrement selon leur généalogie monta à 22 034.
7:8	Les fils de Béker furent Zemira, Yoash, Éliézer, Elyoénaï, Omri, Yerémoth, Abiyah, Anathoth, et Alameth, tous ceux-là furent fils de Béker,
7:9	et leur dénombrement selon leur généalogie, selon leurs générations, comme chefs des familles de leurs pères, hommes vaillants et talentueux au nombre de 20 200.
7:10	Yediyael eut pour fils Bilhan. Et les fils de Bilhan furent Yéoush, Benyamin, Éhoud, Kenaana, Zéthan, Tarsis, et Achishachar.
7:11	Tous ceux-là furent fils de Yediyael, comme chefs des familles de leurs pères, 17 200 hommes vaillants et talentueux, en état de porter les armes et d'aller à la guerre.
7:12	Shouppim et Houppim furent des fils d'Ir : Houshim était fils d'Acher.

### Les descendants de Nephtali

7:13	Les fils de Nephthali furent Yahtsiy'el, Gouni, Yetser, et Shalloum, fils de Bilhah.

### Les descendants de Menashè

7:14	Les fils de Menashè : Asriel, qu'enfanta sa concubine Araméenne. Elle enfanta Makir, père de Galaad.
7:15	Makir prit une femme pour Houppim et pour Shouppim. Le nom de sa sœur était Ma'akah. Le nom du deuxième fils était Tselophhad. Tselophhad n'eut que des filles.
7:16	Ma'akah, femme de Makir, enfanta un fils et l'appela Péresh, et le nom de son frère Shéresh, dont les fils furent Oulam et Rékem.
7:17	Le fils d'Oulam fut Bedan. Ce sont là les fils de Galaad, fils de Makir, fils de Menashè.
7:18	Mais sa sœur Hammoléketh enfanta Ishhod, Abiézer et Machlah.
7:19	Les fils de Shemida furent Ahyân, Sichem, Likchi et Aniam.

### Les descendants d'Éphraïm et leurs villes

7:20	Or les fils d'Éphraïm furent Shoutélah : Béred son fils, Tachath son fils, Éleada son fils, Tachath son fils.
7:21	Zabad son fils, Shoutélah son fils, Ézer, et Élead. Mais ceux de Gath, nés dans le pays, les mirent à mort, parce qu'ils étaient descendus pour prendre leur bétail.
7:22	Éphraïm, leur père, fut dans le deuil plusieurs jours, et ses frères vinrent pour le consoler.
7:23	Puis il alla vers sa femme, qui conçut et enfanta un fils ; et elle l'appela du nom de Beriy`ah, parce que le malheur était dans sa maison.
7:24	Il eut pour fille Shééra, qui bâtit la basse et la haute Beth-Horon, et Ouzzèn-Shééra.
7:25	Son fils fut Réphach, puis Résheph, et Thélach son fils, Thachan son fils,
7:26	Laedan son fils, Ammihoud son fils, Éliyshama son fils,
7:27	Noun son fils, Yéhoshoua son fils.
7:28	Ils possédaient et habitaient Béth-El ainsi que les villes de son ressort ; à l'orient Naaran, à l'occident Guézer, avec les villes de son ressort, et Sichem avec les villes de son ressort, jusqu'à Gaza avec les villes de son ressort.
7:29	Les lieux qui étaient aux fils de Menashè furent Beth-Shean avec les villes de son ressort, Thaanac avec les villes de son ressort, Meguiddo avec les villes de son ressort, et Dor avec les villes de son ressort. Les fils de Yossef, fils d'Israël, habitèrent dans ces villes.

### Les descendants d'Asher

7:30	Les fils d'Asher furent Yimnah, Yishvah, Yishviy, Beriy`ah, et Sérach leur sœur.
7:31	Les fils de Beriy`ah furent Héber et Malkiel, qui fut père de Birzavith.
7:32	Héber engendra Yaphlet, Shomer, Hotham, et Shoua leur sœur.
7:33	Les fils de Yaphlet furent Pasac, Bimhal, et Ashvath. Ce sont là les fils de Yaphlet.
7:34	Et les fils de Shamer furent Achi, Rohega, Yehoubba et Aram.
7:35	Les fils d'Hélem, son frère, furent Tsophach, Yimna, Shélesh et Amal.
7:36	Les fils de Tsophach furent Souah, Harnépher, Shoual, Béri, Yimrah,
7:37	Betser, Hod, Shamma, Shilshah, Yithran, et Beéra.
7:38	Les fils de Yether furent Yephounné, Pispa et Ara.
7:39	Les fils d'Oulla furent Arach, Hanniel et Ritsya.
7:40	Tous ceux-là étaient les enfants d'Asher, des chefs des maisons de leurs pères, des hommes vaillants, choisis et talentueux, des chefs des princes, enregistrés dans l'armée pour la guerre au nombre de 26 000 hommes.

## Chapitre 8

### Les descendants de Benyamin

8:1	Benyamin engendra Béla, qui fut son premier-né, Ashbel le deuxième, Achrach le troisième,
8:2	Nocha le quatrième, et Rapha le cinquième.
8:3	Les fils de Béla furent Addar, Guéra, Abihoud,
8:4	Abishoua, Naaman, Achoach,
8:5	Guéra, Shephouphân et Houram.
8:6	Voici les fils d'Éhoud, qui étaient chefs des maisons des pères des habitants de Guéba, et qui les transportèrent à Manachath :
8:7	Naaman, Achiyah, et Guéra. Guéra, qui les transporta et qui après engendra Ouzza et Ahihoud.
8:8	Or Shacharaïm eut des enfants au pays de Moab, après avoir renvoyé Houshim et Baara, ses femmes.
8:9	Il engendra, de Hodesh sa femme, Yobab, Tsibya, Mésha, Malcam,
8:10	Yeouts, Shobyah et Mirma. Ce sont là ses fils, chefs des pères.
8:11	Mais de Houshim, il engendra Abitoub, Elpaal.
8:12	Les fils d'Elpaal furent Héber, Misheam, et Shémer, qui bâtit Ono, Lod et les villes de son ressort.
8:13	Et Beriy`ah et Shéma furent chefs des pères des habitants d'Ayalon ; ils mirent en fuite les habitants de Gath.
8:14	Ahyo, Shashak, Yerémoth,
8:15	Zebadyah, Arad, Éder,
8:16	Miyka'el, Yishpah, et Yocha, fils de Beriy`ah.
8:17	Zebadyah, Meshoullam, Hizki, Héber,
8:18	Yishmeray, Yizliyah, et Yobab, fils d'Elpaal.
8:19	Yaqiym, Zicri, Zabdi,
8:20	Éliénaï, Tsilthaï, Éliel,
8:21	Adayah, Berayah, et Shimrath, fils de Shimeï.
8:22	Yishpan, Héber, Éliel,
8:23	Abdon, Zicri, Hanan,
8:24	Chananyah, Élam, Anthothiyah,
8:25	Yiphdeyah et Penouel, fils de Shashak.
8:26	Shamsheraï, Shecharyah, Athalyah,
8:27	Ya`areshyah, Eliyah, et Zicri, fils de Yeroham.
8:28	Ce sont là les chefs des pères, selon leurs générations ; et ils habitèrent à Yeroushalaim.

### Les fils du père de Gabaon, ascendant de Shaoul (Saül)

8:29	Le père de Gabaon habita à Gabaon, sa femme avait pour nom Ma'akah.
8:30	Son fils premier-né fut Abdon, puis Tsour, Kis, Baal, Nadab,
8:31	Guedor, Ahyo, et Zéker.
8:32	Mikloth engendra Shimea. Ils habitèrent aussi vis-à-vis de leurs frères à Yeroushalaim, avec leurs frères.
8:33	Ner engendra Kis, et Kis engendra Shaoul, et Shaoul engendra Yehonathan, Malki-Shoua, Abinadab, et Eshbaal.
8:34	Le fils de Yehonathan fut Merib-Baal ; et Merib-Baal engendra Miykah.
8:35	Les fils de Miykah furent Pithon, Mélec, Thaeréa, et Achaz.
8:36	Achaz engendra Yehoaddah ; et Yehoaddah engendra Alémeth, Azmaveth et Zimri ; Zimri engendra Motsa.
8:37	Motsa engendra Binea, qui eut pour fils Rapha, qui eut pour fils Éleasa, qui eut pour fils Atsel.
8:38	Atsel eut six fils, dont les noms sont : Azrikam, Bokrou, Yishmael, She`aryah, Obadyah, et Hanan. Tous ceux-là furent fils d'Atsel.
8:39	Les fils d'Eshek, son frère, furent Oulam son premier-né, Yéoush le second, Éliphéleth, le troisième.
8:40	Et les fils d'Oulam furent des hommes vaillants et talentueux, tirant bien à l'arc, et ils eurent beaucoup de fils et de petits-fils, jusqu'à 150. Tous des fils de Benyamin.

## Chapitre 9

### Les habitants de Yeroushalaim (Jérusalem)

9:1	Ainsi, tous ceux d'Israël furent enregistrés par généalogie et inscrits dans le livre des rois d'Israël. Et ceux de Yéhouda furent emmenés en captivité à Babel<!--Babylone.--> à cause de leurs péchés<!--La captivité babylonienne voir 2 R. 24-25.-->.
9:2	Et les premiers à habiter dans leurs possessions et dans leurs villes, furent ceux d'Israël : des prêtres, des Lévites et des Néthiniens.
9:3	À Yeroushalaim habitaient les fils de Yéhouda, les fils de Benyamin, et les fils d'Éphraïm et de Menashè.
9:4	Outaï, fils d'Ammihoud, fils d'Omri, fils d'Imri, fils de Bani, des fils de Pérets, fils de Yéhouda.
9:5	Des Shiylonites, Asayah le premier-né, et ses fils.
9:6	Des fils de Zérach, Yeouel, et ses frères, 690.
9:7	Des fils de Benyamin, Sallou fils de Meshoullam, fils de Hodavyah, fils de Hassenoua.
9:8	Yibneyah, fils de Yeroham, et Élah fils d'Ouzzi, fils de Micri ; et Meshoullam fils de Shephatyah, fils de Reouel, fils de Yibniyah.
9:9	Leurs frères, selon leurs générations, furent 956. Tous ces hommes-là furent chefs des pères dans les maisons de leurs pères.
9:10	Des prêtres : Yekda`yah, Yehoyariyb, et Yakîn.
9:11	Azaryah fils de Chilqiyah, fils de Meshoullam, fils de Tsadok, fils de Merayoth, fils d'Ahitoub, intendant de la maison d'Elohîm.
9:12	Adayah, fils de Yeroham, fils de Pashhour, fils de Malkiyah ; et Maesaï, fils d'Adiel, fils de Yahzéra, fils de Meshoullam, fils de Meshillémith, fils d'Immer.
9:13	Leurs frères, chefs de la maison de leurs pères, 1 760 hommes, vaillants et talentueux, occupés au service de la maison d'Elohîm.
9:14	Des Lévites : Shema’yah, fils de Hashoub, fils d'Azrikam, fils de Chashabyah, des fils de Merari,
9:15	Bakbakkar, Héresh, Galal, et Mattanyah, fils de Miyka, fils de Zicri, fils d'Asaph,
9:16	Obadyah fils de Shema’yah, fils de Galal, fils de Yedoutoun ; et Berekyah, fils d'Asa, fils d'Elkana, qui habita dans les villages des Nethophathiens.
9:17	Et les portiers : Shalloum, Aqoub, Thalmon, et Achiman, et leurs frères, mais Shalloum était le chef.
9:18	Il l'a été jusqu'à maintenant, ayant la charge de la porte du roi vers l'orient. Ceux-là furent portiers pour le camp des fils de Lévi.
9:19	Shalloum, fils de Koré, fils d'Ebyacaph, fils de Koré, et ses frères Koréites, de la maison de son père, remplissaient les fonctions de gardiens, gardant les seuils de la tente, comme leurs pères en avaient gardé l'entrée au camp de YHWH ;
9:20	Phinées, fils d'Èl’azar, fut établi chef sur eux en présence de YHWH qui était avec lui.
9:21	Zekaryah, fils de Meshelemyah, était le portier de l'entrée de la tente d'assignation.
9:22	Ils étaient en tout 212, choisis pour être les portiers des seuils, et enregistrés selon les familles dans la généalogie, selon leurs villages ; David et Shemouél, le voyant, les avaient établis dans leurs fonctions.
9:23	Eux et leurs fils furent établis sur les portes de la maison de YHWH, qui est la maison du tabernacle, pour y faire la garde.
9:24	Il y avait des portiers aux quatre vents, à l'orient, à l'occident, au nord et au sud.
9:25	Et leurs frères, qui étaient dans leurs villages, devaient de temps à autre venir auprès d'eux pendant sept jours.
9:26	Car selon cette fonction, il y avait toujours quatre chefs des portiers, des Lévites, qui avaient la surveillance des chambres et des trésors de la maison d'Elohîm.
9:27	Ils se tenaient la nuit tout autour de la maison d'Elohîm, dont ils avaient la garde, et qu'ils devaient ouvrir tous les matins.
9:28	Certains d'entre eux prenaient soin des ustensiles du service, car c'est selon le nombre qu'ils les rentraient et c'est selon le nombre qu'ils les sortaient.
9:29	D'autres veillaient sur les ustensiles, sur tous les ustensiles du sanctuaire, sur la fleur de farine, sur le vin, sur l'huile, sur l'encens et sur les aromates.
9:30	Mais ceux qui composaient les parfums aromatiques étaient des fils de prêtres.
9:31	Mattithyah, d'entre les Lévites, premier-né de Shalloum, Koréite, s'occupait des gâteaux cuits sur les plaques.
9:32	Et quelques-uns de leurs frères, parmi les fils des Kehathites, avaient la charge des pains de proposition<!--Les pains de proposition sont une image du Seigneur Yéhoshoua, notre Pain de vie (Jn. 6:48-59).--> pour l'apprêter chaque shabbat.
9:33	Certains étaient des chanteurs, chefs des pères des Lévites qui demeuraient dans les chambres. Ils étaient exemptés des autres charges, parce qu'ils devaient être en fonction le jour et la nuit.
9:34	Ce sont là les chefs des pères des Lévites, selon leurs familles. Ils furent chefs et ils habitèrent à Yeroushalaim.

### De Yéiël au roi Shaoul (Saül), de Yehonathan (Yonathan) à Arsel<!--1 Ch. 10 ; 1 S. 1 ; 1 S. 30.-->

9:35	Or Yéiël, le père de Gabaon, habita à Gabaon, et le nom de sa femme était Ma'akah.
9:36	Son fils premier-né, Abdon, puis Tsour, Kis, Baal, Ner, Nadab,
9:37	Guedor, Ahyo, Zekaryah, et Mikloth.
9:38	Mikloth engendra Shimeam. Ils habitèrent vis-à-vis de leurs frères à Yeroushalaim, avec leurs frères.
9:39	Ner engendra Kis, et Kis engendra Shaoul, et Shaoul engendra Yehonathan, Malki-Shoua, Abinadab et Eshbaal.
9:40	Le fils de Yehonathan fut Merib-Baal. Merib-Baal engendra Miykah.
9:41	Et les fils de Miykah furent Pithon, Mélec, Thachréa et Achaz.
9:42	Achaz engendra Ya`rah, Ya`rah engendra Alémeth, Azmaveth et Zimri. Zimri engendra Motsa.
9:43	Motsa engendra Binea, qui eut pour fils Rephayah, qui eut pour fils Éleasa, qui eut pour fils Atsel.
9:44	Atsel eut six fils, dont les noms sont Azrikam, Bokrou, Yishmael, She`aryah, Obadyah et Hanan. Ce furent là les fils d'Atsel.

## Chapitre 10

### Mort de Shaoul<!--1 S. 31:1-10 ; 2 S. 1.-->

10:1	Les Philistins combattirent contre Israël, et les hommes d'Israël s'enfuirent devant les Philistins, et tombèrent blessés à mort sur la montagne de Guilboa<!--1 S. 31:1-10.-->.
10:2	Les Philistins poursuivirent et atteignirent Shaoul et ses fils, et les Philistins tuèrent Yonathan, Abinadab et Malki-Shoua, les fils de Shaoul.
10:3	Le poids de la guerre se porta sur Shaoul. Les tireurs d'arc le trouvèrent et il se mit à trembler devant les tireurs.
10:4	Alors Shaoul dit à celui qui portait ses armes : Tire ton épée, et transperce-moi, de peur que ces incirconcis ne viennent et ne me traitent avec sévérité. Mais celui qui portait ses armes ne voulut pas, parce qu'il avait très peur. Shaoul prit donc son épée, et se jeta dessus.
10:5	Alors celui qui portait les armes de Shaoul, ayant vu que Shaoul était mort, se jeta aussi sur son épée, et il mourut.
10:6	Ainsi mourut Shaoul, et ses trois fils, et toute sa maison périt avec lui.
10:7	Tous ceux d'Israël, qui étaient dans la vallée, ayant vu qu'on avait fui, et que Shaoul et ses fils étaient morts, abandonnèrent leurs villes et s'enfuirent, de sorte que les Philistins y entrèrent et y habitèrent.
10:8	Or il arriva que dès le lendemain, les Philistins vinrent pour dépouiller les morts, et ils trouvèrent Shaoul et ses fils étendus sur la montagne de Guilboa.
10:9	Ils le dépouillèrent et emportèrent sa tête et ses armes. Puis ils envoyèrent dans le pays des Philistins, de tous côtés, pour annoncer la nouvelle à leurs idoles et au peuple.
10:10	Ils mirent ses armes dans la maison de leur elohîm, et ils attachèrent sa tête dans la maison de Dagon<!--1 S. 5:1-11.-->.
10:11	Or tous ceux de Yabesh de Galaad, ayant appris tout ce que les Philistins avaient fait à Shaoul,
10:12	tous les hommes talentueux d'entre eux se levèrent et enlevèrent le corps de Shaoul et les corps de ses fils. Ils les apportèrent à Yabesh, et ils ensevelirent leurs os sous un chêne à Yabesh, et jeûnèrent pendant sept jours.
10:13	Shaoul mourut à cause de la transgression dont il s'était rendu coupable envers YHWH, parce qu'il n'avait pas observé la parole de YHWH, et aussi pour avoir consulté ceux qui évoquent les morts<!--1 S. 28:7-20.-->.
10:14	Il ne consulta pas YHWH, c'est pourquoi YHWH le fit mourir, et transféra<!--« Tourner vers », « changer de direction ».--> la royauté à David, fils d'Isaï.

## Chapitre 11

### David règne sur Israël<!--2 S. 2-4, 5:1-3.-->

11:1	Tous ceux d'Israël se rassemblèrent auprès de David à Hébron et lui dirent : Voici, nous sommes tes os et ta chair.
11:2	Hier même, avant-hier même, même lorsque Shaoul était roi, tu as été celui qui faisait sortir et entrer Israël. YHWH, ton Elohîm, t'a dit : Tu paîtras mon peuple d'Israël, et tu seras le chef de mon peuple d'Israël.
11:3	Ainsi, tous les anciens d'Israël vinrent auprès du roi à Hébron et là, David traita alliance avec eux à Hébron, devant YHWH. Ils oignirent David pour roi sur Israël, selon la parole de YHWH, prononcée par Shemouél<!--2 S. 2-4, 5:1-3.-->.

### Yeroushalaim devient la cité de David<!--2 S. 5:6-10.-->

11:4	David et tous ceux d'Israël s'en allèrent à Yeroushalaim, qui est Yebous. Là étaient les Yebousiens qui habitaient le pays.
11:5	Ceux qui habitaient à Yebous dirent à David : Tu n'entreras pas ici. Mais David prit la forteresse de Sion, qui est la cité de David.
11:6	Car David avait dit : Quiconque battra le premier les Yebousiens, sera chef et prince. Yoab, fils de Tserouyah, monta le premier et fut fait chef.
11:7	David s'établit dans la forteresse. C'est pourquoi on l'appela la cité de David<!--2 S. 5:6-10.-->.
11:8	Il bâtit aussi la ville tout autour, depuis Millo et ses environs, et Yoab répara le reste de la ville.
11:9	Et David allait toujours en avançant et en croissant, car YHWH Sabaoth était avec lui.

### Les vaillants hommes de David<!--2 S. 23:8-39.-->

11:10	Voici les chefs des hommes vaillants qui étaient au service de David, qui l'aidèrent avec tout Israël à assurer sa royauté, afin de le faire régner selon la parole de YHWH au sujet d'Israël.
11:11	Ceux-ci sont du nombre des vaillants hommes que David avait. Yashob`am, fils de Hacmoni, chef entre les trois principaux. Il brandit sa lance contre 300 hommes et les blessa à mort en une seule fois<!--2 S. 23:8-39.-->.
11:12	Après lui était Èl’azar, fils de Dodo, l'Achochite, qui fut l'un des trois vaillants hommes.
11:13	Il se trouvait avec David à Pas-Dammim, lorsque les Philistins s'étaient rassemblés pour combattre. Il y avait là une parcelle de terre remplie d'orge ; et le peuple fuyait devant les Philistins.
11:14	Ils s'arrêtèrent au milieu de cette parcelle de champ, la défendirent, et battirent les Philistins. Ainsi, YHWH accorda une grande délivrance.
11:15	Il en descendit encore trois des trente chefs près du rocher, auprès de David, dans la caverne d'Adoullam, lorsque l'armée des Philistins campait dans la vallée des géants.
11:16	David était alors dans la forteresse, et la garnison des Philistins était en ce même temps-là à Bethléhem.
11:17	David eut un désir, et dit : Qui est-ce qui me fera boire de l'eau du puits qui est à la porte de Bethléhem ?
11:18	Alors ces trois hommes passèrent au travers du camp des Philistins, et puisèrent de l'eau du puits qui était à la porte de Bethléhem ; et l'ayant apportée, la présentèrent à David, qui ne voulut pas la boire, mais la répandit en l'honneur de YHWH.
11:19	Car il dit : Que mon Elohîm me garde de faire une telle chose ! Boirais-je le sang de ces hommes qui ont fait un tel voyage au péril de leur vie ? Car ils m'ont apporté cette eau au péril de leur vie. Ainsi, il ne voulut pas la boire. Voilà ce que firent ces trois vaillants hommes.
11:20	Abishaï, frère de Yoab, était chef des trois. Il sortit sa lance sur 300 hommes, les blessa à mort et se fit un nom parmi les trois.
11:21	Entre les trois, il fut plus honoré que les deux autres, et il fut leur chef, cependant, il n'égala pas ces trois premiers.
11:22	Benayah aussi, fils de Yehoyada, fils d'un vaillant homme de Kabtseel, avait fait de grands exploits. Il tua deux des plus puissants hommes de Moab. Il descendit et frappa un lion au milieu d'une fosse en un jour de neige.
11:23	Il tua aussi un homme égyptien qui était haut de 5 coudées. Cet Égyptien avait à la main une lance grosse comme une ensouple de tisserand. Mais il descendit contre lui avec un bâton, et arracha la lance de la main de l'Égyptien et le tua avec sa propre lance.
11:24	Benayah, fils de Yehoyada, fit ces choses-là, et fut célèbre entre ces trois vaillants hommes.
11:25	Voilà, il était le plus honoré des trente, cependant, il n'égala pas les trois premiers. David l'établit dans son conseil privé.
11:26	Et les plus vaillants d'entre les gens de guerre furent Asaël, frère de Yoab ; et Elchanan fils de Dodo, de Bethléhem,
11:27	Shammoth d'Haror, Hélets de Palon,
11:28	Ira, fils d'Ikkesh, de Tekoa, Abiézer d'Anathoth,
11:29	Sibbecaï le Houshatite, Ilaï d'Achoach,
11:30	Maharaï de Nethopha, Héled fils de Baana de Nethopha,
11:31	Ittaï fils de Ribaï, de Guibea des fils de Benyamin, Benayah de Pirathon,
11:32	Houraï de Nachalé-Gaash, Abiel d'Araba,
11:33	Azmaveth de Baharoum, Éliachba de Shaalbon,
11:34	Bené-Hashem de Guizon, Yonathan fils de Shagué d'Harar,
11:35	Achiam fils de Sacar d'Harar, Éliphal fils d'Our,
11:36	Hépher de Mekéra, Achiyah de Palon,
11:37	Hetsro de Carmel, Naaraï fils d'Ezbaï,
11:38	Yoel frère de Nathan, Mibchar fils d'Hagri,
11:39	Tsélek l'Ammonite, Nachraï de Béroth, qui portait les armes de Yoab fils de Tserouyah,
11:40	Ira de Yithriy, Gareb de Yithriy,
11:41	Ouriyah le Héthien, Zabad fils d'Achlaï,
11:42	Adina fils de Shiza le Reoubénite, chef des Reoubénites, et 30 avec lui.
11:43	Hanan fils de Ma'akah, et Yehoshaphat de Mithni,
11:44	Ouzya d'Ashtharoth, Shama et Yéiël fils de Hotham d'Aroër,
11:45	Yediyael fils de Shimri, et Yocha son frère, le Thitsite,
11:46	Éliel de Machavim, Yeriybay, et Yoshavyah fils d'Elnaam, et Yithmah le Moabite,
11:47	Éliel, et Obed, et Ya`asiy'el-Metsobayah.

## Chapitre 12

### Les guerriers venus chez David à Tsiklag<!--2 S. 5:17 ; 1 Ch. 12:8-15, 14:8.-->

12:1	Voici ceux qui allèrent trouver David à Tsiklag, lorsqu'il était encore éloigné de la présence de Shaoul, fils de Kis. Ils étaient parmi les vaillants hommes qui lui prêtèrent leur secours pendant la guerre.
12:2	Ils étaient équipés d'arcs, se servant de la main droite et de la gauche pour jeter des pierres, et pour tirer des flèches avec l'arc. Ils étaient frères de Shaoul, de Benyamin,
12:3	Achiézer, le chef, et Yoash, fils de Shemaa, qui était de Guibea, Yezav'el, Péleth, fils d'Azmaveth, Beraca et Yehuw d'Anathoth.
12:4	Yishma`yah de Gabaon, homme vaillant entre les trente, et même au-dessus des trente, et Yirmeyah, Yachaziy'el, Yohanan et Yozabad de Guedéra.
12:5	Élouzaï, Yeriymoth, Be`alyah, Shemaryah et Shephatyah de Haroph.
12:6	Elkana, Yishshiyah, Azareel, Yoezer et Yashob`am Koréites.
12:7	Yoelah et Zebadyah, fils de Yeroham de Guedor.

### Les guerriers venus chez David dans la forteresse de Moab<!--1 S. 22:2-4.-->

12:8	Quelques-uns aussi des Gadites se retirèrent auprès de David, dans la forteresse, dans le désert, hommes vaillants et talentueux, exercés à la guerre et maniant le bouclier et la lance. Leurs visages étaient comme des faces de lion, et ils étaient aussi prompts que des gazelles sur les montagnes.
12:9	Ézer le premier, Obadyah le second, Éliab le troisième ;
12:10	Mishmanna le quatrième, Yirmeyah le cinquième ;
12:11	Attaï le sixième, Éliel le septième ;
12:12	Yohanan le huitième, Elzabad le neuvième ;
12:13	Yirmeyah le dixième, Macbannaï le onzième.
12:14	C'étaient des fils de Gath, qui furent chefs de l'armée. Le plus petit avait la charge de 100 hommes, et le plus grand de 1 000.
12:15	Ce sont ceux qui passèrent le Yarden au premier mois, quand il déborde sur tous ses rivages, et ils chassèrent ceux qui demeuraient dans les vallées, vers l'orient et l'occident.
12:16	Il vint aussi des fils de Benyamin et de Yéhouda vers David à la forteresse.
12:17	David sortit au-devant d'eux, et prenant la parole, il leur dit : Si vous êtes venus en paix vers moi pour m'aider, mon cœur s'unira à vous. Mais si c'est pour me trahir et me livrer à mes ennemis, quoique je ne sois coupable d'aucune violence, que l'Elohîm de nos pères le voie, et qu'il fasse justice !
12:18	Alors Amasaï, le chef des officiers des trente, fut revêtu de l'Esprit : Que la paix soit avec toi, ô David ! Qu'elle soit avec toi, fils d'Isaï ! Que la paix soit à ceux qui t'aident, puisque ton Elohîm t'aide ! Et David les reçut, et les établit parmi les chefs de ses troupes.
12:19	Des hommes de Menashè se joignirent à David, lorsqu'il alla combattre Shaoul avec les Philistins. Mais ils ne les aidèrent pas, parce que les seigneurs des Philistins, après en avoir délibéré entre eux, le renvoyèrent, en disant : Il se tournera vers son maître Shaoul, au péril de nos têtes.
12:20	Quand il retourna à Tsiklag, Adnach, Yozabad, Yediyael, Miyka'el, Yozabad, Élihou et Tsilthaï, chefs des milliers qui étaient en Menashè, se tournèrent vers lui.
12:21	Et ils aidèrent David contre la troupe des Amalécites, car ils étaient tous forts et vaillants, et ils furent faits chefs dans l'armée.
12:22	De jour en jour, il venait des gens auprès de David pour l'aider, de sorte qu'il eut une grande armée, comme une armée d'Elohîm<!--1 S. 22:2-4.-->.

### Les guerriers venus chez David à Hébron<!--2 S. 5:1-3.-->

12:23	Voici le nombre des hommes équipés pour la guerre, qui vinrent auprès de David à Hébron, afin de lui transférer la royauté de Shaoul, selon le commandement de YHWH<!--2 S. 5:1-3.-->.
12:24	Des fils de Yéhouda, qui portaient le bouclier et la lance, 6 800, armés pour la guerre.
12:25	Des fils de Shim’ôn, vaillants et talentueux pour la guerre, 7 100.
12:26	Des fils de Lévi : 4 600.
12:27	Et Yehoyada, prince de ceux d'Aaron, et avec lui 3 700 ;
12:28	et Tsadok, jeune homme vaillant et talentueux, et 22 chefs de la maison de son père.
12:29	Des fils de Benyamin, parents de Shaoul : 3 000, car jusqu'alors la plus grande partie d'entre eux soutenaient la maison de Shaoul.
12:30	Des fils d'Éphraïm : 20 800 vaillants et talentueux hommes de renom, dans la maison de leurs pères.
12:31	De la demi-tribu de Menashè : 18 000 qui furent désignés par leur nom pour aller établir David roi.
12:32	Des fils de Yissakar, ayant le discernement et la connaissance des temps, pour savoir ce que devait faire Israël : 200 de leurs chefs, et tous leurs frères sous leurs ordres.
12:33	De Zebouloun : 50 000 combattants, rangés en bataille avec toutes sortes d'armes, prêts à livrer bataille et n’ayant pas un cœur double.
12:34	De Nephthali : 1 000 capitaines, et avec eux 37 000 portant le bouclier et la lance.
12:35	Des Danites : 28 600 équipés pour la guerre.
12:36	D'Asher : 40 000 combattants et prêts à combattre.
12:37	De l'autre côté du Yarden, des Reoubénites, des Gadites, et de la demi-tribu de Menashè : 120 000 avec tous les instruments de guerre pour combattre.
12:38	Tous ces hommes, gens de guerre, prêts a combattre, vinrent tous de bon cœur à Hébron pour établir David roi sur tout Israël. Et tout le reste d'Israël était aussi d'un même sentiment pour établir David roi.
12:39	Et ils furent là avec David, mangeant et buvant pendant trois jours, car leurs frères leur avaient préparé des vivres.
12:40	Et même ceux qui étaient les plus proches d'eux, jusqu'à Yissakar, Zebouloun et Nephthali, apportaient du pain sur des ânes, sur des chameaux, sur des mulets et sur des bœufs, de la farine, des figues sèches, des grappes de raisins, du vin et de l'huile. Ils amenaient des bœufs et des brebis en abondance, car il y avait une joie en Israël.

## Chapitre 13

### Retour de l'arche, Ouzza frappé par YHWH<!--2 S. 6:1-11.-->

13:1	Or David tint conseil avec les chefs de milliers et de centaines, avec tous les princes du peuple.
13:2	Et il dit à toute l'assemblée d'Israël : Si vous l'approuvez et que cela vient de YHWH notre Elohîm, envoyons partout vers nos autres frères, qui sont dans toutes les contrées d'Israël, et avec lesquels sont les prêtres et les Lévites, dans leurs villes et dans leurs faubourgs, afin qu'ils se réunissent à nous,
13:3	et que nous ramenions auprès de nous l'arche de notre Elohîm, car nous ne nous en sommes pas occupés du temps de Shaoul.
13:4	Et toute l'assemblée répondit qu'on le fasse ainsi ! En effet, la chose fut approuvée par tout le peuple.
13:5	David donc rassembla tout Israël, depuis le Shichor<!--Le Nil ou une rivière ou un canal à la frontière Est de l'Égypte, affluent du Nil.--> d'Égypte, jusqu'à l'entrée du pays de Hamath, pour ramener de Qiryath-Yéarim l'arche d'Elohîm.
13:6	Et David monta avec tout Israël vers Ba`alah à Qiryath-Yéarim, qui appartient à Yéhouda, pour faire amener de là l'arche d'Elohîm, devant laquelle est invoqué le Nom de YHWH, qui habite entre les chérubins.
13:7	Ils mirent l'arche d'Elohîm sur un chariot neuf, et l'emmenèrent de la maison d'Abinadab. Ouzza et Ahyo conduisaient le char.
13:8	Et David et tout Israël dansaient en présence d'Elohîm de toute leur force, en chantant des cantiques et en jouant sur des harpes, des luths, des tambourins, des cymbales, et des trompettes.
13:9	Quand ils furent arrivés à l'aire de Kidon, Ouzza<!--L'arche devait être transportée grâce à des barres faites spécialement à cet effet, qui ne devaient pas être enlevées (Ex. 27:6-7 ; No. 1:51). Selon la torah, seuls les Lévites devaient préparer et déplacer tout ce qui concernait le tabernacle. Et même parmi les Lévites, chaque famille avait une fonction spécifique (No. 3 ; No. 4). Les Kehathites n'étaient pas autorisés à toucher l'arche, leur rôle se limitait seulement à la transporter à l'aide des barres (No. 4:15). Ouzza a étendu sa main sur l'arche, alors qu'il n'était certainement pas Lévite. Il était devenu trop familier avec les choses saintes et avait pris à la légère les principes d'Elohîm. Il a voulu aider le Seigneur. Or, il ne faut jamais chercher à servir Elohîm sans être appelé par lui.--> étendit sa main pour retenir l'arche, parce que les bœufs avaient glissé.
13:10	Et la colère de YHWH s'enflamma contre Ouzza, et il le frappa, parce qu'il avait étendu sa main sur l'arche. Ouzza mourut en présence d'Elohîm.
13:11	David fut irrité de ce que YHWH avait fait une brèche en la personne de Ouzza. On a appelé jusqu'à ce jour ce lieu-là Pérets-Ouzza, brèche d'Ouzza.
13:12	David eut peur d'Elohîm en ce jour-là, et il dit : Comment ferais-je entrer chez moi l'arche d'Elohîm ?
13:13	C'est pourquoi David ne la retira pas chez lui, dans la cité de David, mais il la fit conduire dans la maison d'Obed-Édom de Gath.
13:14	Et l'arche d'Elohîm demeura 3 mois avec la famille d'Obed-Édom, dans sa maison. YHWH bénit la maison d'Obed-Édom, et tout ce qui lui appartenait.

## Chapitre 14

### Rayonnement du règne de David<!--2 S. 5:11-25, 23:13-17 ; 1 Ch. 3:5-9, 11:15-19, 12:8-15.-->

14:1	Hiram, roi de Tyr, envoya des messagers à David, et du bois de cèdre, des tailleurs de pierres et des charpentiers, pour lui bâtir une maison.
14:2	Alors David reconnut que YHWH l'affermissait comme roi sur Israël et que son règne était élevé haut, à cause de son peuple d'Israël.
14:3	David prit encore des femmes à Yeroushalaim, et il engendra encore des fils et des filles.
14:4	Voici les noms des fils qu'il eut à Yeroushalaim : Shammoua, Shobab, Nathan, Shelomoh,
14:5	Yibhar, Élishoua, Éliphéleth,
14:6	Noga, Népheg, Yaphiya,
14:7	Éliyshama, Beéliada et Éliphéleth.
14:8	Or quand les Philistins apprirent que David avait été oint pour roi sur tout Israël, ils montèrent tous à sa recherche. David l'ayant appris, sortit au-devant d'eux.
14:9	Les Philistins vinrent et se répandirent dans la vallée des géants.
14:10	David consulta Elohîm, en disant : Monterai-je contre les Philistins, et les livreras-tu entre mes mains ? YHWH lui répondit : Monte, et je les livrerai entre tes mains.
14:11	Alors ils montèrent à Baal-Peratsim<!--Baal-Peratsim signifie « seigneur des brèches ».-->, où David les battit. Puis il dit : Elohîm a fait une brèche au milieu de mes ennemis par ma main, comme une brèche faite par les eaux. C'est pourquoi on donna à ce lieu-là le nom de Baal-Peratsim.
14:12	Et ils laissèrent là leurs elohîm, et David ordonna qu'on les brûle au feu.
14:13	Les Philistins se répandirent encore une autre fois dans cette même vallée.
14:14	David consulta encore Elohîm et Elohîm lui répondit : Tu ne monteras pas vers eux, mais tu te détourneras d'eux, et tu iras contre eux vis-à-vis des mûriers.
14:15	Et il arrivera que quand tu entendras le bruit de la marche au sommet des mûriers, alors tu sortiras pour le combat, car c'est Elohîm qui sort devant toi pour frapper le camp des Philistins.
14:16	David fit selon ce qu'Elohîm lui avait ordonné, et on frappa le camp des Philistins, depuis Gabaon jusqu'à Guézer.
14:17	Ainsi, la renommée de David se répandit par tous ces pays-là, et YHWH remplit de frayeur toutes ces nations-là, au seul nom de David.

## Chapitre 15

### David organise l'arrivée de l'arche à Yeroushalaim<!--2 S. 6:12.-->

15:1	David se bâtit des maisons dans la cité de David. Il prépara un lieu pour l'arche d'Elohîm, et dressa pour elle une tente.
15:2	Et David dit : L'arche d'Elohîm ne doit être portée que par les Lévites, car YHWH les a choisis pour porter l'arche d'Elohîm et pour faire le service à toujours<!--No. 4:15.-->.
15:3	David donc rassembla tous ceux d'Israël à Yeroushalaim, pour faire monter l'arche de YHWH dans le lieu qu'il lui avait préparé.
15:4	David rassembla aussi les fils d'Aaron et les Lévites.
15:5	Des fils de Kehath : Ouriel, le chef, et ses frères, 120.
15:6	Des fils de Merari : Asayah, le chef, et ses frères, 220.
15:7	Des fils de Guershon : Yoel, le chef, et ses frères, 130.
15:8	Des fils d'Élitsaphan : Shema’yah, le chef, et ses frères, 200.
15:9	Des fils de Hébron : Éliel, le chef, et ses frères, 80.
15:10	Des fils d'Ouziel : Amminadab, le chef, et ses frères, 112.
15:11	David appela les prêtres Tsadok et Abiathar, ainsi que les Lévites Ouriel, Asayah, Yoel, Shema’yah, Éliel et Amminadab.
15:12	Il leur dit : Vous qui êtes les chefs des pères des Lévites, sanctifiez-vous, vous et vos frères et transportez l'arche de YHWH, l'Elohîm d'Israël, au lieu que je lui ai préparé.
15:13	Parce que vous n'y étiez pas la première fois, YHWH, notre Elohîm, a fait une brèche parmi nous, car nous ne l'avons pas cherché selon la loi.
15:14	Les prêtres donc et les Lévites se sanctifièrent pour faire monter l'arche de YHWH, l'Elohîm d'Israël.
15:15	Et les fils des Lévites portèrent l'arche d'Elohîm sur leurs épaules, avec les barres qu'ils avaient sur eux, comme Moshè l'avait ordonné selon la parole de YHWH.
15:16	David dit aux chefs des Lévites d'établir quelques-uns de leurs frères chanteurs, avec des instruments de musique, des luths, des harpes et des cymbales qui feraient retentir des sons éclatants, en signe de réjouissance.
15:17	Les Lévites donc établirent Héman, fils de Yoel, et parmi ses frères, Asaph, fils de Berekyah ; et des fils de Merari, qui étaient leurs frères, Éthan, fils de Koushaya ;
15:18	avec eux leurs frères pour être du second ordre : Zekaryah, Ben, Ya`aziy'el, Shemiramoth, Yechiy'el, Ounni, Éliab, Benayah, Ma`aseyah, Mattithyah, Éliphelé, Miqneyah, Obed-Édom, et Yéiël, les portiers.
15:19	Quant aux chanteurs : Héman, Asaph et Éthan, ils avaient des cymbales de cuivre pour les faire retentir.
15:20	Zekaryah, Aziel, Shemiramoth, Yechiy'el, Ounni, Éliab, Ma`aseyah, et Benayah jouaient des luths sur alamoth ;
15:21	et Mattithyah, Éliphelé, Miqneyah, Obed-Édom, Yéiël et Azazyah jouaient des harpes à huit cordes, pour conduire le chant.
15:22	Mais Kenanyah, le chef des Lévites, avait la charge de faire porter l'arche, enseignant comment il fallait la porter, car il était un homme très intelligent.
15:23	Berekyah et Elkana étaient portiers de l'arche.
15:24	Shebanyah, Yehoshaphat, Netanél, Amasaï, Zekaryah, Benayah, Éliézer, les prêtres, sonnaient des trompettes devant l'arche d'Elohîm, et Obed-Édom et Yehiyah étaient portiers de l'arche.

### L'arche transportée au milieu des réjouissances<!--2 S. 6:12.-->

15:25	David et les anciens d'Israël, avec les gouverneurs de milliers, marchaient, amenant avec joie l'arche de l'alliance de YHWH, de la maison d'Obed-Édom.
15:26	Elohîm aidait les Lévites qui portaient l'arche de l'alliance de YHWH et l'on sacrifia 7 veaux et 7 béliers.
15:27	David était revêtu d'une robe de byssus, ainsi que tous les Lévites qui portaient l'arche, les chanteurs et Kenanyah le chef en charge des chanteurs. David avait sur lui un éphod en fin lin.
15:28	Ainsi tout Israël amena l'arche de l'alliance de YHWH, avec de grands cris de joie, et au son du cor, des shofars et des cymbales, faisant retentir leur voix avec des luths et des harpes.
15:29	Mais il arriva, comme l'arche de l'alliance de YHWH entrait dans la cité de David, que Miykal, fille de Shaoul, regardant par la fenêtre, vit le roi David sautant et dansant, et elle le méprisa dans son cœur.

## Chapitre 16

### L'arche placée dans une tente à Yeroushalaim ; sacrifices et cantiques pour YHWH<!--2 S. 6:17-19.-->

16:1	Ils amenèrent donc l'arche d'Elohîm et la posèrent au milieu de la tente que David avait dressée pour elle. On offrit devant Elohîm des holocaustes et des sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->.
16:2	Quand David eut achevé d'offrir les holocaustes et les sacrifices d'offrande de paix, il bénit le peuple au Nom de YHWH.
16:3	Et il distribua à chacun, tant aux hommes qu'aux femmes, un pain, un morceau de viande et un gâteau de raisin.
16:4	Et il établit quelques-uns des Lévites pour faire le service devant l'arche de YHWH, pour célébrer, remercier, et louer l'Elohîm d'Israël.
16:5	Asaph était le chef et Zekaryah le second, Yéiël, Shemiramoth, Yechiy'el, Mattithyah, Éliab, Benayah, Obed-Édom et Yéiël. Ils avaient des instruments de musique, des luths et des harpes, et Asaph faisait retentir sa voix avec des cymbales.
16:6	Benayah et Yachaziy'el, les prêtres, étaient continuellement avec des trompettes devant l'arche de l'alliance d'Elohîm.
16:7	Et en ce même jour, David en tête donna à célébrer YHWH par la main d'Asaph et de ses frères.
16:8	Célébrez YHWH, invoquez son Nom ! Faites connaître parmi les peuples ses œuvres !
16:9	Chantez-le, célébrez-le ! Parlez de toutes ses merveilles !
16:10	Glorifiez-vous de son saint Nom ! Que le cœur de ceux qui cherchent YHWH se réjouisse !
16:11	Recherchez YHWH et sa force, cherchez continuellement sa face !
16:12	Souvenez-vous des merveilles qu'il a faites, de ses miracles et des jugements de sa bouche.
16:13	Postérité d'Israël, son serviteur, fils de Yaacov, ses élus !
16:14	YHWH est notre Elohîm. Ses jugements s'exercent sur toute la Terre.
16:15	Souvenez-vous toujours de son alliance, de la parole qu'il a ordonnée pour mille générations,
16:16	du traité qu'il a fait avec Abraham et du serment qu'il a fait à Yitzhak,
16:17	et qu'il a confirmé à Yaacov et à Israël, pour être une loi et une alliance éternelle,
16:18	en disant : Je te donnerai le pays de Canaan, comme l'héritage qui vous est échu.
16:19	Ils étaient alors un petit nombre d'hommes, peu nombreux, et étrangers dans le pays,
16:20	car ils étaient errants de nation en nation, et d'un royaume vers un autre peuple.
16:21	Il ne permit à personne de les opprimer. Il a même châtié des rois à cause d'eux.
16:22	Et il a dit : Ne touchez pas à mes mashiah<!--Oints.-->, et ne faites pas de mal à mes prophètes<!--L'expression « ne touchez pas à mes mashiah (oints) » signifie qu'il ne fallait pas porter atteinte physiquement aux rois, aux prophètes et aux prêtres, sur qui reposait l'onction d'Elohîm. Il est donc clair que ce verset, que l'on retrouve également dans le Ps. 105:15, ne concerne absolument pas le fait de remettre en question les enseignements d'un quelconque pasteur, prophète ou apôtre. Aujourd'hui, tous les chrétiens sont oints d'Elohîm (Ep. 1:13, 4:30).--> !
16:23	Habitants de la Terre, chantez à YHWH ! Racontez chaque jour sa délivrance.
16:24	Racontez sa gloire parmi les nations, et ses merveilles parmi tous les peuples !
16:25	Car YHWH est grand et très digne de louanges, il est plus redoutable que tous les elohîm.
16:26	Car tous les elohîm des peuples sont des faux elohîm<!--Voir 1 Co. 8:6 ; 1 Jn. 5:20.-->, mais YHWH a fait les cieux.
16:27	La majesté et la magnificence marchent devant lui ; la force et la joie sont dans le lieu où il habite.
16:28	Familles des peuples, donnez à YHWH, donnez à YHWH gloire et force !
16:29	Donnez à YHWH la gloire due à son Nom ! Apportez des offrandes, et présentez-vous devant lui. Prosternez-vous devant YHWH avec des ornements saints !
16:30	Tremblez, vous tous habitants de la Terre, tout étonnés devant sa face ! Car la terre habitable est affermie par lui et elle ne chancelle pas.
16:31	Que les cieux se réjouissent, que la Terre soit dans l'allégresse ! Et que l'on dise parmi les nations : YHWH règne !
16:32	Que la mer retentisse avec tout ce qu'elle contient ! Que la campagne se réjouisse avec tout ce qu'elle renferme !
16:33	Que les arbres de la forêt poussent des cris de joie au-devant de YHWH, parce qu'il vient juger la terre<!--YHWH vient juger la terre. Cette prophétie confirme de façon incontestable la divinité de Yéhoshoua ha Mashiah (Jésus-Christ). Voir Za. 14:1-7.-->.
16:34	Célébrez YHWH, car il est bon, car sa miséricorde demeure à jamais !
16:35	Et dites : Ô Elohîm de notre salut, sauve-nous, rassemble-nous et délivre-nous des nations, afin que nous célébrions ton saint Nom et que nous nous glorifions de ta louange !
16:36	Béni soit YHWH, l'Elohîm d'Israël, de siècle en siècle ! Et tout le peuple dit : Amen ! Louez YHWH !
16:37	On laissa donc là, devant l'arche de l'alliance de YHWH, Asaph et ses frères, pour faire le service continuellement, remplissant leur tâche jour par jour devant l'arche.
16:38	On laissa Obed-Édom, et ses frères, au nombre de 68, Obed-Édom fils de Yedoutoun, et Hosa comme portiers.
16:39	On établit le prêtre Tsadok, et les prêtres ses frères, devant le tabernacle de YHWH, dans le haut lieu qui était à Gabaon,
16:40	pour offrir des holocaustes à YHWH continuellement sur l'autel de l'holocauste, matin et soir, selon tout ce qui est écrit dans la torah de YHWH, qu'il ordonna à Israël.
16:41	Auprès d'eux étaient Héman et Yedoutoun, et les autres qui furent choisis et désignés par leur nom, pour célébrer YHWH, parce que sa miséricorde demeure éternellement.
16:42	Héman et Yedoutoun avaient avec eux des trompettes et des cymbales pour ceux qui les faisaient retentir, et les instruments pour chanter les cantiques d'Elohîm. Les fils de Yedoutoun étaient portiers.
16:43	Puis tout le peuple s'en alla chacun dans sa maison, et David aussi s'en retourna pour bénir sa maison.

## Chapitre 17

### David veut construire un temple à YHWH<!--2 S. 7:1-3.-->

17:1	Or il arriva après que David fut établi dans sa maison, qu'il dit à Nathan, le prophète : Voici, j'habite dans une maison de cèdres, et l'arche de l'alliance de YHWH est sous des tapis.
17:2	Nathan dit à David : Fais tout ce que tu as dans le cœur, car Elohîm est avec toi.

### Réponse de YHWH à David<!--2 S. 7:4-17.-->

17:3	Mais il arriva cette nuit-là que la parole d'Elohîm vint à Nathan, en disant :
17:4	Va, et dis à David, mon serviteur : Ainsi parle YHWH : Tu ne me bâtiras pas de maison pour y habiter.
17:5	Puisque je n'ai pas habité dans une maison depuis le jour où j'ai fait monter les enfants d'Israël hors d'Égypte jusqu'à ce jour, mais j'ai été de tente en tente, et de tabernacle en tabernacle.
17:6	Partout où j'ai marché avec tout Israël, ai-je dit un mot à un seul des juges d'Israël, auxquels j'ai ordonné de paître mon peuple, ai-je dit : Pourquoi ne m'avez-vous pas bâti une maison de cèdres ?
17:7	Maintenant donc tu diras ainsi à David, mon serviteur : Ainsi parle YHWH Sabaoth : Je t'ai pris d'une cabane, d'auprès des brebis, afin que tu sois le conducteur de mon peuple d'Israël ;
17:8	j'ai été avec toi partout où tu as marché, j'ai exterminé devant toi tous tes ennemis, et j'ai rendu ton nom semblable au nom des grands qui sont sur la Terre.
17:9	J'ai établi un lieu pour mon peuple d'Israël, et je l'ai planté afin qu'il habite chez lui et ne soit plus agité. Les fils d'injustice ne le détruiront plus comme ils l'ont fait auparavant,
17:10	et comme à l'époque où j'ai établi des juges sur mon peuple d'Israël. J'ai humilié tous tes ennemis. Je t'informe que YHWH te bâtira une maison.
17:11	Il arrivera donc que quand tes jours seront accomplis pour t'en aller avec tes pères, je ferai lever ta postérité après toi, l'un de tes fils, et j'affermirai son règne<!--Cette prophétie est relative au Mashiah. Voir 2 S. 7:12-17.-->.
17:12	Il me bâtira une maison, et j'affermirai son trône éternellement.
17:13	Je serai pour lui un père, et il sera pour moi un fils. Je ne retirerai pas de lui ma grâce comme je l'ai retirée de celui qui a été avant toi.
17:14	Mais je l'établirai dans ma maison et dans mon royaume éternellement, et son trône sera affermi pour toujours.
17:15	Nathan récita à David toutes ces paroles, et toute cette vision.

### Adoration et reconnaissance de David à YHWH<!--2 S. 7:18-29.-->

17:16	Alors le roi David entra, et se tint devant YHWH, et dit : Ô YHWH Elohîm ! Qui suis-je, et quelle est ma maison, que tu m'aies fait parvenir au point où je suis ?
17:17	Mais cela t'a semblé être peu de chose, ô Elohîm ! Et tu as parlé au sujet de la maison de ton serviteur pour le temps à venir, et tu as porté les regards sur moi à la manière d'un être humain, toi qui es élevé, ô YHWH Elohîm !
17:18	Que pourrait te dire encore David de l'honneur que tu fais à ton serviteur ? Car tu connais ton serviteur.
17:19	Ô YHWH ! Pour l'amour de ton serviteur, et selon ton cœur, tu as fait toutes ces grandes choses, pour lui révéler toutes ces grandeurs.
17:20	Ô YHWH ! Personne n'est semblable à toi, et il n'y a pas d'autre Elohîm que toi selon tout ce que nous avons entendu de nos oreilles.
17:21	Et qui est comme ton peuple d'Israël, la seule nation sur la Terre qu'Elohîm lui-même est venu racheter pour lui, afin qu'elle soit son peuple, et pour te faire un Nom et pour accomplir des miracles et des prodiges, en chassant les nations devant ton peuple que tu as racheté d'Égypte ?
17:22	Et tu as établi ton peuple d'Israël afin qu'il soit ton peuple à toujours, et toi, ô YHWH ! Tu as été son Elohîm.
17:23	Maintenant donc, ô YHWH ! Que la parole que tu as prononcée sur ton serviteur et sur sa maison, soit ferme à jamais, et agis selon ta parole !
17:24	Et que ton Nom subsiste et soit magnifié éternellement, de sorte qu'on dise : YHWH Sabaoth, l'Elohîm d'Israël, est Elohîm pour Israël et que la maison de David, ton serviteur, soit affermie devant toi.
17:25	Car toi, ô mon Elohîm, tu as révélé à l'oreille de ton serviteur que tu lui bâtirais une maison. C'est pourquoi ton serviteur a trouvé une prière à présenter devant toi.
17:26	Maintenant, ô YHWH ! Tu es Elohîm, et tu as parlé de ce bien à ton serviteur.
17:27	Veuille donc maintenant bénir la maison de ton serviteur, afin qu'elle soit éternellement devant toi, car tu l'as bénie, ô YHWH ! Et elle sera bénie à jamais !

## Chapitre 18

### Le règne de David affermi<!--2 S. 8:1-18.-->

18:1	Et il arriva que David battit les Philistins, et les humilia, et il enleva de la main des Philistins, Gath et les villes de son ressort<!--2 S. 8.-->.
18:2	Il battit aussi les Moabites, et les Moabites furent asservis à David et lui payèrent un tribut.
18:3	David battit aussi Hadarézer, roi de Tsoba, vers Hamath, lorsqu'il alla établir sa domination sur le fleuve de l'Euphrate.
18:4	David lui prit 1 000 chars, 7 000 cavaliers, et 20 000 hommes de pied. Il coupa les jarrets des chevaux de tous les chars, mais il réserva 100 chars.
18:5	Les Syriens de Damas vinrent au secours d'Hadarézer, roi de Tsoba, et David battit parmi les Syriens 22 000 hommes.
18:6	Puis David mit des garnisons dans la Syrie de Damas. Et les Syriens furent assujettis à David et lui payèrent un tribut. YHWH sauvait David partout où il allait.
18:7	Et David prit les boucliers d'or qui étaient aux serviteurs de Hadarézer, et les apporta à Yeroushalaim.
18:8	Il emporta aussi de Thibchath et de Koun, villes de Hadarézer, une grande quantité de cuivre, dont Shelomoh fit la mer de cuivre, les colonnes et les ustensiles de cuivre.
18:9	Tohou, roi de Hamath, apprit que David avait défait toute l'armée de Hadarézer, roi de Tsoba.
18:10	Et il envoya Hadoram, son fils, vers le roi David pour le saluer et le féliciter de ce qu'il avait combattu Hadarézer, et qu'il l'avait défait. Car Hadarézer était un homme en guerre contre Tohou. Quant à tous les vases d'or, d'argent et de cuivre,
18:11	le roi David les consacra aussi à YHWH, avec l'argent et l'or qu'il avait emporté de toutes les nations, d'Édom, de Moab, des fils d'Ammon, des Philistins et d'Amalek.
18:12	Et Abishaï, fils de Tserouyah battit 18 000 Édomites dans la vallée du sel.
18:13	Il mit une garnison dans Édom, et tous les Édomites furent asservis à David. Et YHWH sauvait David partout où il allait.
18:14	Ainsi, David régna sur tout Israël, rendant jugement et justice à tout son peuple.
18:15	Yoab, fils de Tserouyah, avait la charge de l'armée, et Yehoshaphat, fils d'Ahiloud, était archiviste.
18:16	Tsadok, fils d'Ahitoub, et Abimélec, fils d'Abiathar, étaient les prêtres et Shavsha était le secrétaire.
18:17	Benayah, fils de Yehoyada, était sur les Kéréthiens et les Péléthiens ; mais les fils de David étaient les premiers auprès du roi.

## Chapitre 19

### David monte contre les Ammonites et les Syriens<!--2 S. 10.-->

19:1	Or il arriva après cela que Nachash, roi des fils d'Ammon, mourut et son fils devint roi à sa place.
19:2	David dit : J'userai de bonté envers Hanoun, fils de Nachash, car son père a usé de bonté envers moi. Ainsi, David envoya des messagers pour le consoler de la mort de son père, et les serviteurs de David vinrent au pays des fils d'Ammon vers Hanoun pour le consoler.
19:3	Mais les chefs d'entre les fils d'Ammon dirent à Hanoun : Penses-tu que ce soit pour honorer ton père que David t'a envoyé des consolateurs ? N'est-ce pas pour examiner et épier le pays, afin de le détruire, que ses serviteurs sont venus vers toi ?
19:4	Alors Hanoun prit les serviteurs de David, les fit raser, et les fit couper leurs habits par le milieu jusqu'aux hanches. Puis il les renvoya.
19:5	On alla informer David de ce qui était arrivé à ces hommes, et il envoya quelqu’un à leur rencontre, car ces hommes étaient très humiliés. Et le roi leur fit dire : Restez à Yeriycho jusqu'à ce que votre barbe ait repoussé, et revenez ensuite.
19:6	Or les fils d'Ammon voyant qu'ils s'étaient rendus odieux à David, Hanoun et les fils d'Ammon envoyèrent 1 000 talents d'argent pour prendre à leur solde des chars et des cavaliers de Mésopotamie, de Syrie, de Ma'akah et de Tsoba.
19:7	Ils prirent à leur solde 32 000 hommes et des chars, et le roi de Ma'akah avec son peuple, lesquels vinrent camper devant Médeba. Les fils d'Ammon aussi s'assemblèrent de leurs villes et vinrent pour combattre.
19:8	David l'ayant appris, envoya Yoab et ceux de toute l'armée qui étaient les plus vaillants.
19:9	Les fils d'Ammon sortirent et rangèrent leur armée en bataille à l'entrée de la ville. Les rois qui étaient venus étaient à part dans la campagne.
19:10	Yoab, voyant que l'armée était tournée contre lui devant et derrière, prit de tous les jeunes hommes d'Israël et les rangea contre les Syriens.
19:11	Et il donna la conduite du reste du peuple à Abishaï, son frère et on les rangea contre les fils d'Ammon.
19:12	Et Yoab lui dit : Si les Syriens sont plus forts que moi, tu viendras me délivrer, et si les fils d'Ammon sont plus forts que toi, je te délivrerai.
19:13	Sois ferme et montrons-nous vaillants pour notre peuple et pour les villes de notre Elohîm ! et que YHWH fasse ce qui est bon à ses yeux !
19:14	Alors Yoab et le peuple qui était avec lui s'approchèrent pour livrer bataille aux Syriens qui s'enfuirent de devant lui.
19:15	Et les fils d'Ammon voyant que les Syriens s'étaient enfuis, eux aussi s'enfuirent devant Abishaï, frère de Yoab, et rentrèrent dans la ville, et Yoab revint à Yeroushalaim.
19:16	Mais les Syriens, qui avaient été battus par ceux d'Israël, envoyèrent des messagers et firent venir les Syriens qui étaient au-delà du fleuve. Shophach, chef de l'armée d'Hadarézer, les conduisait.
19:17	On le rapporta à David, qui rassembla tout Israël, passa le Yarden, alla au-devant d'eux et se rangea en bataille contre eux. David donc rangea la bataille contre les Syriens et ils combattirent contre lui.
19:18	Mais les Syriens s'enfuirent devant Israël. David tua aux Syriens 7 000 cavaliers et 40 000 hommes de pied. Il tua Shophach, le chef de l'armée.
19:19	Alors les serviteurs d'Hadarézer, voyant qu'ils avaient été battus par ceux d'Israël, firent la paix avec David, et lui furent asservis. Les Syriens ne voulurent plus secourir les fils d'Ammon.

## Chapitre 20

### Conquête de Rabba<!--2 S. 11 ; 2 S. 12.-->

20:1	Or il arriva l'année suivante, au temps où sortent les rois, que Yoab conduisit une forte armée et ravagea le pays des fils d'Ammon. Puis il alla assiéger Rabba, tandis que David resta à Yeroushalaim. Yoab battit Rabba et la détruisit<!--2 S. 12:26-31.-->.
20:2	David enleva la couronne de dessus la tête de son roi, il trouva qu'elle pesait un talent d'or et qu'elle était garnie de pierres précieuses. On la mit sur la tête de David, et il fit sortir de la ville un très grand butin.
20:3	Il fit sortir le peuple qui s'y trouvait, et les mit aux scies, aux pics de fer et aux haches de fer. David traita de la sorte toutes les villes des fils d'Ammon. Puis David retourna avec tout le peuple à Yeroushalaim.

### Guerre contre les Philistins<!--2 S. 21:15-22.-->

20:4	Il arriva après cela que la guerre continua à Guézer contre les Philistins. Alors Sibbecaï, le Houshatite, frappa Sippaï, qui était des fils de géant, et ils furent humiliés<!--2 S. 21:15-22.-->.
20:5	Il y eut encore une autre guerre contre les Philistins. Et Elchanan, fils de Yaïr, frappa Lachmi, frère de Goliath de Gath, qui avait une lance dont le bois était comme une ensouple de tisserand.
20:6	Il y eut encore une autre guerre à Gath. Il s'y trouva un homme de grande stature, qui avait six doigts à chaque main et à chaque pied, de sorte qu'il en avait en tout 24. Lui aussi était né de géant.
20:7	Il défia Israël et Yehonathan, fils de Shimea, frère de David, le tua.
20:8	Ceux-là étaient nés de géants qui habitaient à Gath. Ils tombèrent par la main de David et par la main de ses serviteurs.

## Chapitre 21

### David fait le dénombrement contre la volonté de YHWH<!--2 S. 24:1-17.-->

21:1	Mais Satan s'éleva contre Israël, et il incita David à faire le dénombrement d'Israël.
21:2	Et David dit à Yoab et aux chefs du peuple : Allez et faites le dénombrement d'Israël, depuis Beer-Shéba jusqu'à Dan, et rapportez-le-moi, afin que j'en connaisse le nombre.
21:3	Mais Yoab répondit : Que YHWH veuille augmenter son peuple 100 fois encore plus qu'il ne l'est, ô roi, mon seigneur. Tous ne sont-ils pas serviteurs de mon seigneur ? Mais pourquoi mon seigneur demande-t-il cela ? Pourquoi entraîner Israël dans la culpabilité ?
21:4	Mais la parole du roi l'emporta sur Yoab. Et Yoab partit et parcourut tout Israël, puis il revint à Yeroushalaim.
21:5	Et Yoab donna à David le rôle du dénombrement du peuple : il y avait dans tout Israël 1 100 000 hommes tirant l'épée, et en Yéhouda 470 000 hommes tirant l'épée.
21:6	Bien qu'il n'eût pas compté parmi eux ni Lévi ni Benyamin, parce que Yoab exécutait la parole du roi en l'ayant en abomination.
21:7	Cela fut une chose mauvaise aux yeux d'Elohîm, c'est pourquoi il frappa Israël.
21:8	Et David dit à Elohîm : C'est un grand péché que j'ai commis en cette affaire ! Maintenant, fais passer, je te prie, l'iniquité de ton serviteur, car j'ai agi très follement.
21:9	Et YHWH parla à Gad, le voyant de David, en disant :
21:10	Va, parle à David, et dis-lui : Ainsi parle YHWH, je te propose trois choses. Choisis l'une d'elles, et je l'exécuterai contre toi.
21:11	Et Gad vint à David, et lui dit : Ainsi parle YHWH :
21:12	Choisis ou la famine durant l'espace de trois ans, ou trois mois de défaites devant tes adversaires en sorte que l'épée de tes ennemis t'atteigne, ou trois jours pendant lesquels l'épée de YHWH et la peste seront dans le pays et l'Ange de YHWH portera la destruction dans toutes les contrées d'Israël. Et maintenant, vois quelle parole je rapporterai à celui qui m'a envoyé.
21:13	Alors David répondit à Gad : Je suis dans une très grande angoisse ! Que je tombe, je te prie, entre les mains de YHWH, parce que sa miséricorde est très grande. Mais que je ne tombe pas entre les mains des humains !
21:14	YHWH envoya donc la peste sur Israël, et il tomba 70 000 hommes d'Israël.
21:15	Elohîm envoya aussi un ange à Yeroushalaim pour la détruire, et comme il la détruisait, YHWH regarda et se repentit de ce mal. Et il dit à l'ange qui détruisait : C'est assez ! Retire à présent ta main. Et l'Ange de YHWH se tenait près de l'aire d'Ornan, le Yebousien.
21:16	Or David leva les yeux et vit l'Ange de YHWH<!--Ge. 16:7.--> qui était entre la terre et le ciel, ayant dans sa main son épée nue, tournée contre Yeroushalaim. Et David et les anciens, couverts de sacs, tombèrent sur leurs faces.
21:17	Et David dit à Elohîm : N'est-ce pas moi qui ai ordonné qu'on fasse le dénombrement du peuple ? C'est donc moi qui ai péché et qui ai mal agi, qui ai mal agi, mais ces brebis qu'ont-elles fait ? YHWH, mon Elohîm ! Je te prie que ta main soit contre moi, et contre la maison de mon père, mais qu'elle ne soit pas contre ton peuple, pour le détruire.

### Fin de la plaie après l'offrande de David<!--2 S. 24:18-25.-->

21:18	Alors l'Ange de YHWH ordonna à Gad de dire à David, qu'il monte pour dresser un autel à YHWH, dans l'aire d'Ornan, le Yebousien.
21:19	David donc monta selon la parole que Gad lui avait dite au Nom de YHWH.
21:20	Ornan s'étant retourné, et ayant vu l'Ange, ses quatre fils se cachèrent avec lui. Or Ornan foulait du blé.
21:21	David vint jusqu'à Ornan, et Ornan regarda, et ayant vu David, il sortit de l'aire et se prosterna devant lui, le visage à terre.
21:22	Et David dit à Ornan : Donne-moi la place de cette aire et j'y bâtirai un autel à YHWH. Donne-la-moi pour le prix qu'elle vaut, afin que cette plaie soit arrêtée de dessus le peuple.
21:23	Et Ornan dit à David : Prends-la et que le roi mon seigneur fasse ce qui est bon à ses yeux ! Voici, je donne ces bœufs pour les holocaustes, et ces instruments à fouler du blé pour le bois, et ce blé pour l'offrande ; je donne toutes ces choses.
21:24	Mais le roi David lui répondit : Non, mais j'achèterai, j'achèterai tout cela au prix qu'il vaut, car je ne présenterai pas à YHWH ce qui est à toi, et je n'offrirai pas un holocauste qui ne me coûte rien.
21:25	David donna donc à Ornan pour cette place, 600 sicles d'or de poids.
21:26	Puis il bâtit là un autel à YHWH, et il offrit des holocaustes et des sacrifices d'offrande de paix, et il invoqua YHWH, qui l'exauça par le feu envoyé des cieux sur l'autel de l'holocauste.
21:27	Alors YHWH parla à l'ange, et l'ange remit son épée dans son fourreau.
21:28	En ce temps-là, David, voyant que YHWH l'avait exaucé dans l'aire d'Ornan, le Yebousien, y offrait des sacrifices.
21:29	Or le tabernacle de YHWH, que Moshè avait construit dans le désert, et l'autel des holocaustes, étaient en ce temps-là dans le haut lieu de Gabaon.
21:30	Mais David ne pouvait pas aller devant cet autel pour invoquer Elohîm, parce qu'il avait été terrifié à cause de l'épée de l'Ange de YHWH.

## Chapitre 22

### Préparatifs de David pour la construction du temple

22:1	Et David dit : C'est ici la maison de YHWH Elohîm, et c'est ici l'autel pour les holocaustes d'Israël.
22:2	David ordonna de rassembler les étrangers qui étaient dans le pays d'Israël, et il établit des tailleurs de pierres pour tailler des pierres de taille, pour la construction de la maison d'Elohîm.
22:3	David prépara aussi du fer en abondance, afin d'en faire des clous pour les battants des portes et pour les crampons, du cuivre en quantité telle qu'il n'était pas possible de le peser,
22:4	et du bois de cèdre qu'on ne pouvait compter, parce que les Sidoniens et les Tyriens amenaient à David du bois de cèdre en abondance.
22:5	David dit : Shelomoh, mon fils, est jeune et délicat, et la maison qu'il faut bâtir à YHWH doit être magnifique en excellence, en réputation, et en gloire, dans tous les pays. Je lui préparerai donc maintenant de quoi la bâtir. Ainsi, David prépara, avant sa mort, ces choses en abondance.

### Recommandation de David à Shelomoh

22:6	Puis il appela Shelomoh, son fils, et lui ordonna de bâtir une maison à YHWH, l'Elohîm d'Israël.
22:7	David donc dit à Shelomoh : Mon fils, j'avais à cœur de bâtir une maison au Nom de YHWH, mon Elohîm.
22:8	Mais la parole de YHWH vint à moi, en disant : Tu as répandu beaucoup de sang, et tu as fait de grandes guerres. Tu ne bâtiras pas de maison à mon Nom, parce que tu as répandu beaucoup de sang sur la Terre devant moi.
22:9	Voici, il te naîtra un fils, qui sera un homme de repos, et à qui je donnerai du repos par rapport à tous ses ennemis tout autour, c'est pourquoi son nom sera Shelomoh. Et en son temps, je donnerai la paix et le repos à Israël.
22:10	Ce sera lui qui bâtira une maison à mon Nom. Il sera un fils pour moi et je serai un père pour lui. J'affermirai le trône de son règne sur Israël à jamais.
22:11	Maintenant donc, mon fils, YHWH sera avec toi, et tu prospéreras, et tu bâtiras la maison de YHWH, ton Elohîm, ainsi qu'il l'a déclaré à ton égard.
22:12	Seulement, que YHWH te donne de la sagesse et du discernement, et qu'il t'instruise concernant le gouvernement d'Israël, et comment tu dois garder la torah de YHWH, ton Elohîm.
22:13	Tu prospéreras si tu as soin de mettre en pratique les lois et les ordonnances que YHWH a prescrites à Moshè pour Israël. Fortifie-toi et prends courage, n'aie pas peur et ne t'effraie pas.
22:14	Voici, selon ma petitesse, j'ai préparé pour la maison de YHWH 100 000 talents d'or et mille milliers de talents d'argent. Quant au cuivre et au fer, il est d'un poids incalculable, car il est en abondance. J'ai aussi préparé le bois et les pierres ; et tu y ajouteras ce qu'il faudra.
22:15	Tu as avec toi beaucoup d'ouvriers, des tailleurs de pierres, des artisans de la pierre et du bois et des hommes habiles dans toute espèce d'ouvrages.
22:16	Il y a de l'or et de l'argent, du cuivre et du fer sans nombre. Lève-toi et agis ! YHWH sera avec toi.
22:17	David ordonna aussi à tous les chefs d'Israël d'aider Shelomoh son fils, en disant :
22:18	YHWH, votre Elohîm, n'est-il pas avec vous, et ne vous a-t-il pas donné du repos de tous côtés ? Car il a livré entre mes mains les habitants du pays, et le pays a été soumis devant YHWH, et devant son peuple.
22:19	Maintenant donc, appliquez vos cœurs et vos âmes à rechercher YHWH, votre Elohîm. Levez-vous et bâtissez le sanctuaire de YHWH Elohîm, afin d'amener l'arche de l'alliance de YHWH, et les ustensiles consacrés à Elohîm dans la maison qui doit être bâtie au Nom de YHWH.

## Chapitre 23

### David désigne Shelomoh comme son successeur<!--1 Ch. 28:1.-->

23:1	David étant vieux et rassasié de jours, établit Shelomoh, son fils, pour roi sur Israël.
23:2	Et il rassembla tous les principaux d'Israël, les prêtres et les Lévites.
23:3	On fit le dénombrement des Lévites, depuis l'âge de 30 ans et au-dessus. Leur nombre fut, par tête, par fils, de 38 000 hommes<!--No. 3:25-37.-->.
23:4	Et David dit : Qu'il y en ait parmi eux 24 000 pour vaquer ordinairement à l'œuvre de la maison de YHWH, et 6 000 comme magistrats et juges,
23:5	4 000 comme portiers et 4 000 pour louer YHWH avec des instruments que j'ai faits pour le louer.

### Dénombrement des Lévites<!--No. 3:25-37.-->

23:6	David les divisa en classes d'après les fils de Lévi : Guershon, Kehath et Merari.
23:7	Des Guershonites, il y eut Laedan et Shimeï.
23:8	Les fils de Laedan furent ces 3 : Yechiy'el le premier, puis Zétham, puis Yoel.
23:9	Les fils de Shimeï furent ces 3 : Shelomiyth, Haziel et Haran. Ce sont là les chefs des pères de la famille de Laedan.
23:10	Et les fils de Shimeï furent Yahath, Zina, Yéoush et Beriy`ah. Ce sont là les 4 fils de Shimeï.
23:11	Yahath était le premier et Zina le second ; mais Yéoush et Beriy`ah n'eurent pas beaucoup de fils, c'est pourquoi ils furent comptés pour une seule maison de père dans le dénombrement.
23:12	Des fils de Kehath, il y eut Amram, Yitshar, Hébron et Ouziel, en tout 4.
23:13	Les fils d'Amram furent Aaron et Moshè. Aaron fut séparé lui et ses fils à toujours, pour sanctifier le Saint des saints, pour faire brûler des parfums en présence de YHWH, pour le servir, et pour bénir en son Nom à toujours.
23:14	Et quant à Moshè, homme d'Elohîm, ses fils devaient être comptés de la tribu de Lévi.
23:15	Les fils de Moshè furent Guershom et Éliézer.
23:16	Des fils de Guershom, Shebouel le premier.
23:17	Quant aux fils d'Éliézer, Rechabyah fut le premier. Éliézer n'eut pas d'autres fils, mais les fils de Rechabyah furent très nombreux.
23:18	Des fils de Yitshar, Shelomiyth était le premier.
23:19	Les fils de Hébron furent Yeriyah le premier, Amaryah le second, Yachaziy'el le troisième, Yeqam`am le quatrième.
23:20	Les fils d'Ouziel furent Miykah le premier, Yishshiyah le second.
23:21	Des fils de Merari il y eut Machli et Moushi. Les fils de Machli furent Èl’azar et Kis.
23:22	Èl’azar mourut, et n'eut pas de fils, mais des filles, et les fils de Kis, leurs frères les prirent pour femmes.
23:23	Les fils de Moushi furent Machli, Éder et Yerémoth, eux 3.

### Fonctions des Lévites<!--No. 3:5-12.-->

23:24	Ce sont là les fils de Lévi, selon les maisons de leurs pères, chefs des pères, selon leurs dénombrements qui furent faits en comptant leurs noms, étant comptés chacun par tête ; et ils faisaient l'œuvre du service de la maison de YHWH, depuis l'âge de 20 ans et au-dessus.
23:25	Car David dit : YHWH, l'Elohîm d'Israël, a donné du repos à son peuple, et il établira sa demeure dans Yeroushalaim à toujours.
23:26	Quant aux Lévites, ils n'auront plus à porter le tabernacle ni tous les ustensiles pour son service.
23:27	C'est pourquoi, dans les derniers registres de David, les fils de Lévi furent dénombrés depuis l'âge de 20 ans et au-dessus.
23:28	Car leurs fonctions, aux côtés des fils d'Aaron, pour le service de la maison de YHWH, concernaient les parvis et les chambres, la purification de toutes les choses saintes, l'œuvre du service de la maison d'Elohîm,
23:29	les pains de proposition, la fleur de farine pour l'offrande de grain, les galettes sans levain, tout ce qui se cuit sur la plaque, tout ce qui est rissolé, la petite et grande mesure.
23:30	Ils avaient à se présenter tous les matins et tous les soirs, afin de célébrer et louer YHWH,
23:31	et offrir tous les holocaustes qu'il fallait offrir à YHWH les jours de shabbat, aux nouvelles lunes, et aux fêtes solennelles<!--Ou « temps fixé ». Voir Ge. 1:14.-->, continuellement devant YHWH, selon le nombre et les usages prescrits.
23:32	Ils donnaient leurs soins à la tente d'assignation, au lieu saint, et aux fils d'Aaron, leurs frères, pour le service de la maison de YHWH.

## Chapitre 24

### Vingt-quatre classes de prêtres

24:1	Quant aux fils d'Aaron, voici leurs classes<!--Les vingt-quatre classes de prêtres qui se tenaient devant YHWH dans le temple de Yeroushalaim (Jérusalem) étaient une représentation des vingt-quatre anciens qui se tiennent devant le trône d'Elohîm (Ap. 4:4).-->. Les fils d'Aaron furent Nadab, Abihou, Èl’azar et Ithamar.
24:2	Mais Nadab et Abihou<!--Lé. 10:1-4.--> moururent en présence de leur père, sans avoir de fils. Èl’azar et Ithamar exercèrent la prêtrise.
24:3	Et David, ainsi que Tsadok, l'un des fils d'Èl’azar, et Achimélec, l'un des fils d'Ithamar, les répartit en classes selon leur fonction dans leur service.
24:4	Il se trouva parmi les fils d'Èl’azar plus de chefs que parmi les fils d'Ithamar, et on les répartit ainsi : pour les fils d'Èl’azar, 16 chefs de maisons de pères, et pour les fils d'Ithamar, 8 chefs de maisons de pères.
24:5	Et on les répartit par le sort, les uns avec les autres, car il y avait des princes du lieu saint et des princes d'Elohîm aussi bien parmi les fils d'Èl’azar que parmi les fils d'Ithamar.
24:6	Shema’yah, fils de Netanél, le scribe, qui était de la tribu de Lévi, les mit par écrit devant le roi, les princes du peuple, devant Tsadok le prêtre et Achimélec, fils d'Abiathar, et devant les chefs des pères des prêtres et des Lévites. On tira au sort une maison de père pour Èl’azar et une autre fut tirée pour Ithamar.
24:7	Le premier sort échut à Yehoyariyb, le second à Yekda`yah,
24:8	le troisième à Harim, le quatrième à Seorim,
24:9	le cinquième à Malkiyah, le sixième à Miyamin,
24:10	le septième à Hakkots, le huitième à Abiyah,
24:11	le neuvième à Yéshoua, le dixième à Shekanyah,
24:12	le onzième à Éliashib, le douzième à Yaqiym,
24:13	le treizième à Houppa, le quatorzième à Yeshebab,
24:14	le quinzième à Bilga, le seizième à Immer,
24:15	le dix-septième à Hézir, le dix-huitième à Happitsets,
24:16	le dix-neuvième à Pethachyah, le vingtième à Yehezkel,
24:17	le vingt et unième à Yakîn, et le vingt-deuxième à Gamoul,
24:18	le vingt-troisième à Delayah, le vingt-quatrième à Ma`azyah.
24:19	Tel fut leur classement pour le service qu'ils avaient à faire, lorsqu'ils entraient dans la maison de YHWH, selon qu'il leur avait été ordonné par Aaron, leur père, comme YHWH, l'Elohîm d'Israël, le lui avait ordonné.

### Les chefs des Lévites ; les fils de Kehath et de Merari

24:20	Voici les chefs du reste des Lévites. Des fils d'Amram : Shoubaël ; et des fils de Shoubaël, Yehdiyah.
24:21	De Rechabyah, des fils de Rechabyah, Yishshiyah était le premier.
24:22	Des Yitsharites, Shelomoth ; des fils de Shelomoth, Yahath.
24:23	Des fils d'Hébron, Yeriyah, Amaryah le second ; Yachaziy'el le troisième, Yeqam`am le quatrième.
24:24	Des fils d'Ouziel, Miykah ; des fils de Miykah, Shamir.
24:25	Le frère de Miykah était Yishshiyah ; des fils de Yishshiyah, Zekaryah.
24:26	Des fils de Merari, Machli et Moushi. Des fils de Ya`aziyah, son fils.
24:27	Des fils de Merari, de Ya`aziyah, son fils : Shoham, Zakkour et Ibri.
24:28	De Machli, Èl’azar, qui n'eut pas de fils.
24:29	De Kis, les fils de Kis, Yerachme'el.
24:30	Et des fils de Moushi, Machli, Éder et Yeriymoth. Ce sont là les fils des Lévites, selon les maisons de leurs pères.
24:31	Eux aussi, comme leurs frères, les fils d'Aaron, ils tirèrent au sort devant le roi David, Tsadok et Achimélec, et les chefs des pères des prêtres et des Lévites. Il en fut ainsi pour chaque chef de maison comme pour le moindre de ses frères.

## Chapitre 25

### Dénombrement des chanteurs

25:1	David et les chefs de l'armée mirent à part pour le service ceux des fils d'Asaph, d'Héman et de Yedoutoun qui prophétisaient avec des harpes, des luths et des cymbales. Et voici le nombre des hommes employés pour le service qu'ils avaient à faire.
25:2	Des fils d'Asaph : Zakkour, Yossef, Nethanyah et Ashareéla, fils d'Asaph, sous la conduite d'Asaph, qui prophétisait selon les ordres du roi.
25:3	De Yedoutoun, les six fils de Yedoutoun : Gedalyah, Tseri, Yesha`yah, Chashabyah, Mattithyah et Shimeï, jouaient de la harpe, sous la conduite de leur père Yedoutoun, qui prophétisait en célébrant et louant YHWH.
25:4	D'Héman, les fils d'Héman : Bouqqiyahou, Mattanyah, Ouziel, Shebouel, Yeriymoth, Chananyah, Hanani, Éliatha, Guiddalthi, Romamthi-Ézer, Yoshbeqashah, Mallothi, Hothir, Machazioth.
25:5	Tous ceux-là étaient fils d'Héman, le voyant du roi, qui révélait les paroles d'Elohîm pour en exalter la puissance. Elohîm donna à Héman 14 fils et 3 filles.
25:6	Tous ceux-là étaient employés, sous la conduite de leurs pères, aux cantiques de la maison de YHWH, avec des cymbales, des luths, et des harpes, dans le service de la maison d'Elohîm, selon les ordres du roi donnés à Asaph, à Yedoutoun et à Héman.
25:7	Et leur nombre avec leurs frères, auxquels on avait enseigné les cantiques de YHWH, était de 288, tous très habiles.

### Répartition des chanteurs en vingt-quatre classes

25:8	Et ils tirèrent au sort pour leurs fonctions, petits et grands, maîtres et disciples.
25:9	Et le premier sort échut pour Asaph, à Yossef. Le second à Gedalyah, lui, ses frères et ses fils étaient 12.
25:10	Le troisième à Zakkour, lui, ses fils et ses frères étaient 12.
25:11	Le quatrième à Yitsriy, lui, ses fils et ses frères étaient 12.
25:12	Le cinquième à Nethanyah, lui, ses fils et ses frères étaient 12.
25:13	Le sixième à Bouqqiyahou, lui, ses fils et ses frères étaient 12.
25:14	Le septième à Yesarelah, lui, ses fils et ses frères étaient 12.
25:15	Le huitième à Yesha`yah, lui, ses fils et ses frères étaient 12.
25:16	Le neuvième à Mattanyah, lui, ses fils et ses frères étaient 12.
25:17	Le dixième à Shimeï, lui, ses fils et ses frères étaient 12.
25:18	L'onzième à Azareel, lui, ses fils et ses frères étaient 12.
25:19	Le douzième à Chashabyah, lui, ses fils, et ses frères étaient 12.
25:20	Le treizième à Shoubaël, lui, ses fils et ses frères étaient 12.
25:21	Le quatorzième à Mattithyah, lui, ses fils et ses frères étaient 12.
25:22	Le quinzième à Yerémoth, lui, ses fils et ses frères étaient 12.
25:23	Le seizième à Chananyah, lui, ses fils et ses frères étaient 12.
25:24	Le dix-septième à Yoshbeqashah, lui, ses fils et ses frères étaient 12.
25:25	Le dix-huitième à Hanani, lui, ses fils et ses frères étaient 12.
25:26	Le dix-neuvième à Mallothi, lui, ses fils et ses frères étaient 12.
25:27	Le vingtième à Éliatha, lui, ses fils et ses frères étaient 12.
25:28	Le vingt et unième à Hothir, lui, ses fils et ses frères étaient 12.
25:29	Le vingt-deuxième à Guiddalthi, lui, ses fils et ses frères étaient 12.
25:30	Le vingt-troisième à Machazioth, lui, ses fils et ses frères étaient 12.
25:31	Le vingt-quatrième à Romamthi-Ézer, lui, ses fils et ses frères étaient 12.

## Chapitre 26

### Les classes des portiers

26:1	Et quant aux classes des portiers, il y eut pour les Koréites : Meshelemyah, fils de Koré, d'entre les fils d'Asaph.
26:2	Les fils de Meshelemyah furent Zekaryah, le premier-né, Yediyael le second, Zebadyah le troisième, Yathniy`el le quatrième,
26:3	Élam le cinquième, Yehohanan le sixième et Elyoénaï le septième.
26:4	Les fils d'Obed-Édom furent Shema’yah le premier-né, Yehozabad le second, Yoach le troisième, Sacar le quatrième, Netanél le cinquième,
26:5	Ammiel le sixième, Yissakar le septième, Peulthaï le huitième. Car Elohîm l'avait béni.
26:6	À Shema’yah, son fils, naquirent des fils qui eurent le commandement sur la maison de leur père, parce qu'ils étaient des hommes vaillants et talentueux.
26:7	Les fils donc de Shema’yah furent Othni, Rephaël, Obed, Elzabad et ses frères, fils talentueux, Élihou et Semakyah.
26:8	Tous ceux-là étaient des fils d'Obed-Édom, eux, leurs fils et leurs frères, étaient des hommes pleins de vigueur et de force pour le service. Ils étaient 62 d'Obed-Édom.
26:9	Les fils de Meshelemyah avec ses frères, fils talentueux étaient au nombre de 18.
26:10	Les fils de Hosa, d'entre les fils de Merari, furent Shimri le chef, quoiqu'il ne fût pas l'aîné, néanmoins son père l'établit pour chef ;
26:11	Chilqiyah était le second, Tebalyah le troisième, Zekaryah le quatrième. Tous les fils et frères de Hosa étaient 13.
26:12	À ces classes de portiers, aux chefs de ces hommes et à leurs frères, fut remise la garde pour le service de la maison de YHWH.
26:13	Ils tirèrent au sort pour chaque porte, autant pour le plus petit que pour le plus grand, selon leurs familles.
26:14	Et ainsi, le sort pour la porte vers l'orient échut à Shelemyah. Puis on tira au sort pour Zekaryah, son fils, qui était un sage conseiller, et la porte du côté du nord lui fut échue par le sort.
26:15	Le sort d'Obed-Édom lui échut pour la porte du côté du sud, et la maison des magasins échut à ses fils.
26:16	À Shouppim et à Hosa pour la porte vers l'occident, auprès de la porte de Shalléketh, au chemin montant : une garde vis-à-vis de l'autre garde.
26:17	Il y avait vers l'orient 6 Lévites, vers le nord 4 par jour, vers le sud 4 aussi par jour, et vers la maison des magasins 2 de chaque côté ;
26:18	du côté du faubourg vers l'occident, il y en avait 4 au chemin, et 2 vers le faubourg.
26:19	Ce sont là les classes des portiers pour les fils des Koréites, et pour les fils de Merari.

### Les Lévites commis sur les trésors du temple

26:20	Ceux-ci aussi étaient Lévites : Achiyah commis sur les trésors de la maison d'Elohîm et les trésors des choses consacrées.
26:21	Des fils de Laedan, qui étaient d'entre les fils des Guershonites, du côté de Laedan, d'entre les chefs des pères appartenant à Laedan le Guershonite, Yehiyeliy.
26:22	D'entre les fils de Yehiyeliy : Zétham et Yoel, son frère, commis sur les trésors de la maison de YHWH.
26:23	Pour les Amramites, les Yitsharites, les Hébronites et les Ouzziélites,
26:24	Shebouel, fils de Guershom, fils de Moshè, était commis sur les autres trésors.
26:25	Et quant à ses frères issus d'Éliézer, dont Rechabyah fut fils, dont le fils fut Yesha`yah, dont le fils fut Yoram, dont le fils fut Zicri, dont le fils fut Shelomiyth,
26:26	c'étaient Shelomiyth et ses frères qui gardaient tous les trésors des choses saintes que le roi David, les chefs des pères, les chefs de milliers et de centaines, et les chefs de l'armée avaient consacrées.
26:27	C'était le butin de guerre qu'ils avaient consacré, pour l'entretien de la maison de YHWH.
26:28	Tout ce qu'avait consacré Shemouél, le voyant, Shaoul, fils de Kis, Abner, fils de Ner et Yoab, fils de Tserouyah, toutes les choses consacrées étaient mises sous la main de Shelomiyth et de ses frères.

### Les magistrats et juges en Israël

26:29	Parmi les Yitsharites, Kenanyah et ses fils étaient employés aux affaires extérieures sur Israël pour être magistrats et juges.
26:30	Quant aux Hébronites, Chashabyah et ses frères, fils talentueux, au nombre de 1 700, ils avaient la surveillance d'Israël de l'autre côté du Yarden, vers l'occident, pour toute œuvre qui concernait YHWH, et pour le service du roi.
26:31	Quant aux Hébronites, selon leurs générations dans les familles des pères, Yeriyah fut le chef des Hébronites. On fit une recherche au sujet des Hébronites à la quarantième année du règne de David, et on trouva parmi eux à Ya`azeyr de Galaad, des hommes vaillants et talentueux.
26:32	Ses frères, fils talentueux, étaient au nombre de 2 700 chefs des pères. Le roi David les établit sur les Reoubénites, sur les Gadites, et sur la demi-tribu de Menashè, pour toute œuvre qui concernait Elohîm et pour les affaires du roi.

## Chapitre 27

### Les douze chefs de guerre de David

27:1	Quant aux fils d'Israël, selon leur dénombrement, il y avait des chefs de pères, des chefs de milliers et de centaines, et leurs officiers, qui servaient le roi pour tout ce qui concernait les divisions, leur arrivée et leur départ, mois par mois, pendant tous les mois de l'année, et chaque division était de 24 000 hommes.
27:2	Sur la première division, pour le premier mois, était Yashob`am, fils de Zabdiel. Dans sa division il y avait 24 000 hommes.
27:3	Il était des fils de Pérets, chef de tous les capitaines de l'armée du premier mois.
27:4	Dodaï, l'Achochite, présidait sur la division du deuxième mois, Mikloth, étant l'un des chefs de sa division. Il avait une division de 24 000 hommes.
27:5	Le chef de la troisième armée pour le troisième mois était Benayah, fils de Yehoyada, le prêtre et le capitaine en chef. Dans sa division il y avait 24 000 hommes.
27:6	C'est ce Benayah qui était fort entre les trente, et par-dessus les trente ; et Ammizadab, son fils, était dans sa division.
27:7	Le quatrième pour le quatrième mois était Asaël, frère de Yoab, et Zebadyah son fils, après lui. Dans sa division il y avait 24 000 hommes.
27:8	Le cinquième pour le cinquième mois était le capitaine Shamehouth, le Yizrachite. Dans sa division il y avait 24 000 hommes.
27:9	Le sixième pour le sixième mois était Ira, fils d'Ikkesh, le Tekoïte. Dans sa division il y avait 24 000 hommes.
27:10	Le septième pour le septième mois était Hélets, le Pelonite, des fils d'Éphraïm. Dans sa division il y avait 24 000 hommes.
27:11	Le huitième pour le huitième mois était Sibbecaï, le Houshatite, de la famille des Zérachites. Dans sa division il y avait 24 000 hommes.
27:12	Le neuvième pour le neuvième mois était Abiézer d'Anathoth, des Benyamites. Dans sa division il y avait 24 000 hommes.
27:13	Le dixième pour le dixième mois était Maharaï de Nethopha, de la famille des Zérachites. Dans sa division il y avait 24 000 hommes.
27:14	Le onzième pour le onzième mois était Benayah de Pirathon, des fils d'Éphraïm. Dans sa division il y avait 24 000 hommes.
27:15	Le douzième pour le douzième mois était Heldaï de Nethopha, appartenant à Othniel. Dans sa division il y avait 24 000 hommes.

### Les douze chefs des tribus d'Israël

27:16	Et ceux-ci présidaient sur les tribus d'Israël : Éliézer, fils de Zicri, était le conducteur des Reoubénites. Des Shimonites : Shephatyah, fils de Ma'akah.
27:17	Des Lévites, Chashabyah, fils de Kemouel. De ceux d'Aaron : Tsadok.
27:18	De Yéhouda : Élihou, qui était des frères de David. De ceux de Yissakar : Omri, fils de Miyka'el.
27:19	De ceux de Zebouloun : Yishma`yah, fils d'Obadyah. De ceux de Nephthali : Yeriymoth, fils d'Azriel.
27:20	Des fils d'Éphraïm : Hoshea, fils d'Azazyah. De la demi-tribu de Menashè : Yoel, fils de Pedayah.
27:21	De l'autre demi-tribu de Menashè en Galaad : Yiddo, fils de Zekaryah. De ceux de Benyamin : Ya`asiy'el, fils d'Abner.
27:22	De ceux de Dan : Azareel, fils de Yeroham. Ce sont là les chefs des tribus d'Israël.

### Dénombrement arrêté par YHWH

27:23	Mais David ne fit pas le dénombrement des fils de 20 ans et au-dessous, parce que YHWH avait dit qu'il multiplierait Israël comme les étoiles du ciel.
27:24	Yoab, fils de Tserouyah, avait bien commencé à en faire le dénombrement, mais il n'acheva pas parce que la colère d'Elohîm s'était répandue à cause de cela sur Israël. C'est pourquoi ce dénombrement n'entra pas dans les dénombrements des discours du jour du roi David.

### Les gestionnaires de David

27:25	Or Azmaveth, fils d'Adiel, était commis sur les finances du roi ; mais Yehonathan, fils d'Ouzyah, était commis sur les provisions dans les champs, dans les villes, les villages et les châteaux.
27:26	Et Ezri, fils de Keloub, était commis sur ceux qui travaillaient dans la campagne et cultivaient la terre.
27:27	Et Shimeï de Rama sur les vignes, et Zabdi de Shepham sur ce qui provenait des vignes, et sur les celliers du vin.
27:28	Et Baal-Hanan de Guéder sur les oliviers et sur les figuiers qui étaient à la campagne, et Yoash sur les celliers à huile.
27:29	Shithraï de Saron était commis sur le gros bétail qui paissait en Saron. Shaphath, fils d'Adlaï, sur le gros bétail qui paissait dans les vallées.
27:30	Obil, le Yishmaelite, sur les chameaux ; Yehdiyah de Méronoth, sur les ânesses.
27:31	Yaziz, l'Hagarénien, sur les troupeaux du menu bétail. Tous ceux-là avaient la charge des biens qui appartenaient au roi David.

### Les conseillers de David

27:32	Mais Yehonathan, oncle de David, était conseiller, homme très intelligent et scribe. Yechiy'el, fils de Hacmoni, était avec les fils du roi.
27:33	Achitophel était le conseiller du roi. Houshaï l'Arkien était ami du roi.
27:34	Après Achitophel était Yehoyada, fils de Benayah et Abiathar. Yoab était le chef de l'armée du roi.

## Chapitre 28

### Dernières paroles de David, la royauté remise à Shelomoh<!--1 Ch. 23:2.-->

28:1	David convoqua à Yeroushalaim tous les chefs d'Israël : les chefs des tribus, les chefs des divisions qui servaient le roi, les chefs de milliers et de centaines, ceux qui avaient la charge de tous les biens du roi, et de tout ce qu'il possédait, ses fils avec ses eunuques, les hommes vaillants et talentueux.
28:2	Puis le roi David se leva sur ses pieds, et dit : Mes frères et mon peuple, écoutez-moi ! J'avais à cœur de bâtir une maison de repos pour l'arche de l'alliance de YHWH, et pour le marchepied des pieds de notre Elohîm, et j'ai fait les préparatifs pour la bâtir.
28:3	Mais Elohîm m'a dit : Tu ne bâtiras pas de maison à mon Nom, parce que tu es un homme de guerre et que tu as répandu beaucoup de sang.
28:4	YHWH, l'Elohîm d'Israël, m'a choisi dans toute la maison de mon père pour être roi sur Israël à toujours. Car il a choisi Yéhouda pour conducteur, et de la maison de Yéhouda la maison de mon père, et d'entre les fils de mon père il a pris son plaisir en moi, pour me faire régner sur tout Israël.
28:5	Aussi, entre tous mes fils, car YHWH m'a donné beaucoup de fils, il a choisi Shelomoh, mon fils, pour le faire asseoir sur le trône du royaume de YHWH, sur Israël.
28:6	Et il m'a dit : Shelomoh, ton fils, est celui qui bâtira ma maison et mes parvis, car je me le suis choisi pour fils et je serai pour lui un père.
28:7	Et j'affermirai son règne à toujours s'il s'applique à pratiquer mes commandements et mes ordonnances, comme aujourd'hui.
28:8	Maintenant, aux yeux de tout Israël, de l’assemblée de YHWH et aux oreilles de notre Elohîm, gardez et ayez recours à tous les commandements de YHWH votre Elohîm, afin que vous possédiez ce bon pays, et que vous le fassiez hériter à vos enfants après vous, à jamais.
28:9	Et toi, Shelomoh, mon fils, connais l'Elohîm de ton père, et sers-le avec un cœur droit et une bonne volonté, car YHWH sonde tous les cœurs et connaît toutes les dispositions des pensées. Si tu le cherches, il se laissera trouver par toi, mais si tu l'abandonnes, il te rejettera pour toujours.
28:10	Considère maintenant que YHWH t'a choisi pour bâtir une maison pour son sanctuaire. Fortifie-toi donc et applique-toi à y travailler.
28:11	David donna à Shelomoh, son fils, le modèle du portique, de ses maisons, des chambres du trésor, des chambres hautes, des chambres intérieures et du lieu du propitiatoire.
28:12	Il lui donna le modèle de toutes les choses qui lui avaient été inspirées par l'Esprit qui était avec lui, pour les parvis de la maison de YHWH, pour les chambres d'alentour, pour les trésors de la maison de YHWH et pour les trésors des choses saintes,
28:13	pour les divisions des prêtres et des Lévites, pour toute l'œuvre du service de la maison de YHWH et pour tous les ustensiles du service de la maison de YHWH.
28:14	Pour l'or, avec le poids en or de tous les objets de chaque service, et pour tous les ustensiles en argent avec le poids des ustensiles de chaque service.
28:15	Le poids des chandeliers en or, et de leurs lampes en or, selon le poids de chaque chandelier et de ses lampes, et le poids des chandeliers en argent, selon le poids de chaque chandelier et de ses lampes, selon l'usage de chaque chandelier.
28:16	Et de l'or au poids pour les tables des pains de proposition, pour chaque table, et de l'argent pour les tables en argent.
28:17	Il lui donna le modèle pour les fourchettes, pour les bassins et pour les calices en or pur, le modèle pour les coupes en or, selon le poids de chaque coupe, et de l'argent pour les coupes en argent selon le poids de chaque coupe,
28:18	et le modèle pour l'autel de l'encens en or épuré, avec le poids. Il lui donna encore le modèle du char, des chérubins d'or qui étendent les ailes et qui couvrent l'arche de l'alliance de YHWH.
28:19	Toutes ces choses, dit-il, m'ont été données par écrit, de la part de YHWH, afin d'avoir l'intelligence de tous les ouvrages de ce modèle.

### David demande à Shelomoh de bâtir le temple

28:20	C'est pourquoi David dit à Shelomoh, son fils : Fortifie-toi, prends courage et travaille ! N'aie pas peur et ne t'effraie pas, car YHWH Elohîm, mon Elohîm, sera avec toi. Il ne te délaissera pas, il ne t'abandonnera pas, jusqu'à ce que tu aies achevé tout l'ouvrage du service de la maison de YHWH.
28:21	Et voici, j'ai fait les divisions des prêtres et des Lévites pour tout le service de la maison d'Elohîm. Avec toi, pour tout l'ouvrage, il y a des hommes de bonne volonté et remplis de sagesse pour toutes sortes de services, et les chefs ainsi que tout le peuple sont à tes ordres.

## Chapitre 29

### Offrandes volontaires de David et de tout le peuple

29:1	Puis le roi David dit à toute l'assemblée : Elohîm a choisi un seul de mes fils, Shelomoh, qui est encore jeune et délicat, et l'ouvrage est considérable, car ce palais n'est pas pour un être humain, mais pour YHWH Elohîm.
29:2	Et moi, j'ai préparé de toutes mes forces pour la maison de mon Elohîm, de l'or pour les choses qui doivent être en or, de l'argent pour celles qui doivent être en argent, du cuivre pour celles en cuivre, du fer pour celles en fer, du bois pour celles en bois, des pierres d'onyx et des pierres pour être enchâssées, des pierres d'escarboucle et des pierres de diverses couleurs, toutes sortes de pierres précieuses et quantité d'albâtre.
29:3	Et outre cela, parce que j'ai une grande affection pour la maison de mon Elohîm, je donne pour la maison de mon Elohîm, outre toutes les choses que j'ai préparées pour la maison du sanctuaire, l'or et l'argent que je possède en propre :
29:4	3 000 talents d'or, de l'or d'Ophir, et 7 000 talents d'argent affiné, pour revêtir les murailles de la maison,
29:5	afin qu'il y ait de l'or partout où il faut de l'or, et de l'argent partout où il faut de l'argent, et pour tout l'ouvrage qui se fera par la main des ouvriers. Qui donc est disposé aujourd'hui à remplir volontairement ses mains d'offrandes pour YHWH ?
29:6	Alors les chefs des pères, les chefs des tribus d'Israël, les chefs de milliers et de centaines et les intendants du roi firent volontairement des offrandes.
29:7	Ils donnèrent pour le service de la maison d'Elohîm, 5 000 talents et 10 000 drachmes d'or, 10 000 talents d'argent, 18 000 talents de cuivre et 100 000 talents de fer.
29:8	Ils remirent aussi les pierres que chacun avait, pour le trésor de la maison de YHWH, entre les mains de Yechiy'el, le Guershonite.
29:9	Alors le peuple se réjouit de leurs offrandes volontaires, car ils faisaient de tout leur cœur leurs offrandes volontaires à YHWH, et David en eut une très grande joie.

### Prière de David

29:10	Puis David bénit YHWH en présence de toute l'assemblée, et dit : Ô YHWH, Elohîm d'Israël, notre père ! Tu es béni de tout temps et à toujours.
29:11	Ô YHWH ! C'est à toi qu'appartiennent la magnificence, la puissance, la gloire, l'éternité, et la majesté, car tout ce qui est aux cieux et sur la Terre est à toi, ô YHWH ! Le règne est à toi, et tu t'élèves en souverain au-dessus de toutes choses !
29:12	La richesse et la gloire viennent de toi et tu as la domination sur toutes choses. La force et la puissance sont dans ta main, et à ta main d'agrandir et de fortifier toutes choses.
29:13	Maintenant donc, ô notre Elohîm ! Nous te célébrons et nous louons ton Nom glorieux.
29:14	Mais qui suis-je, et qui est mon peuple, que nous ayons assez pour pouvoir t'offrir ces choses volontairement ? Car toutes choses viennent de toi, et nous te donnons ce que nous avons reçu de ta main.
29:15	Et même nous sommes devant toi des étrangers<!--Voir 1 Pi. 2:11.--> et des habitants, comme ont été tous nos pères. Nos jours sont comme l'ombre sur la Terre, et il n'y a pas d'espérance.
29:16	YHWH, notre Elohîm, toute cette abondance que nous avons préparée pour bâtir une maison à ton saint Nom, est de ta main, et toutes ces choses sont à toi.
29:17	Et je sais, ô mon Elohîm, que c'est toi qui sondes les cœurs, et que tu prends plaisir à la droiture. C'est pourquoi j'ai volontairement offert d'un cœur droit toutes ces choses, et j'ai vu maintenant avec joie que ton peuple, qui se trouve ici, t'a fait son offrande volontairement.
29:18	Ô YHWH ! Elohîm d'Abraham, de Yitzhak et d'Israël, nos pères, conserve à toujours dans le cœur de ton peuple, ces dispositions et ces pensées, et affermis leurs cœurs en toi.
29:19	Donne aussi un cœur droit à Shelomoh, mon fils, afin qu'il garde tes commandements, tes préceptes et tes lois, et qu'il fasse tout ce qui est nécessaire et qu'il bâtisse le palais que j'ai préparé.

### Sacrifices en l'honneur de YHWH ; YHWH élève Shelomoh<!--1 Ch. 23:1 ; 1 R. 2:12, 1:32-37.-->

29:20	Après cela, David dit à toute l'assemblée : Bénissez maintenant YHWH, votre Elohîm ! Et toute l'assemblée bénit YHWH, l'Elohîm de leurs pères. Ils s'inclinèrent et se prosternèrent devant YHWH et devant le roi.
29:21	Et le lendemain de ce jour, ils sacrifièrent des sacrifices à YHWH, et offrirent des holocaustes à YHWH : 1 000 veaux, 1 000 moutons et 1 000 agneaux, avec leurs libations, et des sacrifices en grand nombre pour tous ceux d'Israël.
29:22	Ils mangèrent et burent ce jour-là devant YHWH avec une grande joie et, pour la seconde fois, ils proclamèrent roi Shelomoh, fils de David, et ils l'oignirent comme chef pour YHWH, et Tsadok comme prêtre.
29:23	Shelomoh s'assit donc sur le trône de YHWH pour être roi à la place de David, son père. Il prospéra, car tout Israël lui obéit.
29:24	Et tous les chefs et les hommes vaillants, et même tous les fils du roi David donnèrent la main au roi Shelomoh en signe de soumission.
29:25	Ainsi, YHWH agrandit Shelomoh à un très haut degré aux yeux de tout Israël. Il lui donna un royaume<!--Voir Da. 4:17 ; Mt. 21:43.--> d'une splendeur telle qu'aucun roi avant lui n'en avait connue en Israël.

### Fin du règne de David ; sa mort<!--2 S. 5:4-5 ; 1 R. 2:10-12 ; 1 Ch. 3:4.-->

29:26	David donc, fils d'Isaï, régna sur tout Israël.
29:27	Et les jours qu'il régna sur Israël furent 40 ans : il régna 7 ans à Hébron et 33 ans à Yeroushalaim.
29:28	Puis il mourut dans une heureuse vieillesse, rassasié de jours, de richesses, et de gloire. Et Shelomoh, son fils, régna à sa place.
29:29	Les actions du roi David, tant les premières que les dernières, sont écrites dans le livre de Shemouél le voyant, dans le livre de Nathan le prophète, et dans le livre de Gad le prophète,
29:30	avec tout son règne, ses œuvres et ce qui se passa de son temps, tant sur Israël que sur tous les royaumes du territoire.
