# Yesha`yah (Ésaïe) (Es.)

Signification : Yah a sauvé

Auteur : Yesha`yah (Ésaïe)

Thème : Le Mashiah d'Israël

Date de rédaction : 8ème siècle av. J.-C.

Prophète en Israël, Yesha`yah fut une figure marquante en raison du contenu et de l'impact de son message. Véritable porte-parole d'Elohîm, il parla de la ruine morale d'Israël, de la déportation à Babel (Babylone) et des jugements d'Elohîm sur son peuple. Il prophétisa également sur le retour de l'exil, la restauration finale et la reconstruction de Yeroushalaim (Jérusalem). Plus qu'aucun autre livre, les écrits de Yesha`yah annoncent clairement la naissance du Mashiah (Christ), son service, sa mission rédemptrice, son sacrifice et son futur règne millénaire.

L'autorité et l'exactitude de ses prophéties ont été une source d'édification au fil des siècles.

## Chapitre 1

### Prophéties concernant Yéhouda (Juda)

1:1	La vision de Yesha`yah, fils d'Amots, qu'il a vue sur Yéhouda et Yeroushalaim, au jour d'Ouzyah, de Yotham, d'Achaz, et de Yehizqiyah, rois de Yéhouda.
1:2	Cieux, écoutez ! Et toi, Terre, prête l'oreille ! Car YHWH parle. J'ai nourri des enfants, je les ai élevés, mais ils se sont rebellés contre moi.
1:3	Le bœuf connaît son possesseur, et l'âne la crèche de son maître, mais Israël n'a pas de connaissance, mon peuple n'a pas d'intelligence.
1:4	Ah ! Nation pécheresse, peuple chargé d'iniquités, race de méchants, enfants qui ne font que se corrompre ! Ils ont abandonné YHWH, ils ont irrité par leur mépris le Saint<!--Voir commentaire en Ac. 3:14.--> d'Israël, ils se sont retirés en arrière.
1:5	Pourquoi serez-vous encore frappés ? Vous ajouterez l'apostasie ! La tête entière est malade, et tout le cœur est languissant.
1:6	Depuis la plante du pied jusqu'à la tête, il n'y a en lui rien de sain : blessures, meurtrissures et plaies fraîches n'ont été ni pansées, ni bandées, ni adoucies par l'huile.
1:7	Votre pays n'est que désolation, vos villes sont brûlées par le feu, des étrangers dévorent votre terre sous vos yeux, c'est la désolation comme une destruction d'étrangers.
1:8	Car la fille de Sion est restée comme une cabane dans une vigne, comme une cabane dans un champ de concombres, comme une ville assiégée.
1:9	Si YHWH Sabaoth ne nous avait laissé un petit reste, qui est même bien peu, nous serions comme Sodome, nous ressemblerions à Gomorrhe<!--Ro. 9:29.-->.
1:10	Écoutez la parole de YHWH, chefs de Sodome, prêtez l'oreille à la torah de notre Elohîm, peuple de Gomorrhe !
1:11	Qu'ai-je à faire de la multitude de vos sacrifices ? dit YHWH. Je suis rassasié des holocaustes de béliers et de la graisse des veaux, je ne prends pas plaisir au sang des taureaux, ni des agneaux, ni des boucs<!--1 S. 15:22 ; Os. 8:13 ; Mt. 9:13.-->.
1:12	Quand vous venez pour vous présenter devant ma face, qui a demandé cela de vos mains, que vous fouliez mes parvis ?
1:13	Ne continuez plus à m'apporter de vaines offrandes ! L'encens est une abomination pour moi, quant aux nouvelles lunes, aux shabbats et aux convocations des assemblées, je ne supporte pas la méchanceté avec les assemblées solennelles.
1:14	Mon âme hait vos nouvelles lunes et vos fêtes solennelles : elles sont un fardeau pour moi, je suis las de les supporter.
1:15	C'est pourquoi, quand vous étendez vos mains, je me cache les yeux. Quand vous multipliez vos prières, je ne les exauce pas : vos mains sont pleines de sang<!--Es. 59:1-3 ; Mi. 3:4.-->.
1:16	Lavez-vous, purifiez-vous, ôtez de devant mes yeux la méchanceté de vos actions, cessez de faire le mal !
1:17	Apprenez à faire le bien, recherchez la justice, redressez l'oppresseur, défendez l'orphelin, combattez pour la veuve !

### Invitation à l'obéissance ; restauration de la justice de YHWH

1:18	Venez maintenant, dit YHWH, et débattons nos droits. Si vos péchés sont comme le cramoisi, ils deviendront blancs comme la neige. S'ils sont rouges comme l'écarlate, ils deviendront comme la laine.
1:19	Si vous obéissez volontairement, vous mangerez les bonnes choses du pays.
1:20	Mais si vous refusez d'obéir et si vous êtes rebelles, vous serez dévorés par l'épée, car la bouche de YHWH a parlé.
1:21	Comment la cité fidèle est-elle devenue une prostituée ? Elle était pleine de droiture et la justice y habitait, mais maintenant elle est pleine de meurtriers !
1:22	Ton argent s'est changé en scories, ta boisson est coupée d'eau.
1:23	Les chefs de ton peuple sont rebelles et compagnons des voleurs. Tous aiment les pots-de-vin<!--Les présents.-->, ils courent après les récompenses. Ils ne défendent pas l'orphelin et la cause de la veuve ne vient pas jusqu'à eux.
1:24	C'est pourquoi le Seigneur, YHWH Sabaoth, le Puissant d'Israël dit : Ah ! Je me consolerai en punissant mes adversaires, et je me vengerai de mes ennemis.
1:25	Je retournerai ma main contre toi : je refondrai tes scories comme avec la potasse, et j'ôterai tout ton étain.
1:26	Mais je rétablirai tes juges tels qu'ils étaient autrefois, et tes conseillers tels qu'ils étaient au commencement<!--Dans le royaume messianique, le gouvernement théocratique sera restauré et la fonction des juges sera rétablie (voir livre des Juges (Shoftim) ; Mt. 19:28 ; 1 Co. 6:2-3).-->. Après cela, on t'appellera cité de la justice, ville fidèle.
1:27	Sion sera rachetée par la droiture et ceux qui s'y convertiront seront rachetés par la justice.
1:28	Mais les rebelles et les pécheurs seront détruits ensemble, et ceux qui abandonnent YHWH seront consumés.
1:29	Car on sera honteux à cause des térébinthes que vous avez désirés, et vous rougirez à cause des jardins que vous avez choisis<!--Des cultes idolâtres avaient lieu autour des térébinthes et dans des jardins (De. 16:21 ; Es. 57:4-5, 65:3 ; Jé. 2:20 ; Ez. 20:28 ; Os. 4:13).-->.
1:30	Car vous serez comme le térébinthe dont le feuillage tombe, et comme un jardin qui n'a pas d'eau.
1:31	Le fort sera de l'étoupe et son œuvre une étincelle, et tous les deux brûleront ensemble, et il n'y aura personne pour éteindre ce feu.

## Chapitre 2

### Vision du règne messianique

2:1	Paroles de Yesha`yah, fils d'Amots, ce qu'il a vu au sujet de Yéhouda et de Yeroushalaim.
2:2	Et il arrivera, dans les derniers jours<!--Voir Ge. 49:1-2.-->, que la montagne de la maison de YHWH sera établie au sommet des montagnes. Elle s'élèvera au-dessus des collines et toutes les nations y afflueront.
2:3	Et beaucoup de peuples iront et diront : Venez et montons à la montagne de YHWH, à la maison d'Elohîm de Yaacov ! Il nous enseignera ses voies, et nous marcherons dans ses sentiers. Car la torah sortira de Sion, et la parole de YHWH sortira de Yeroushalaim.
2:4	Il jugera au milieu de beaucoup de peuples, il réprimandera des nations puissantes et lointaines. De leurs épées elles forgeront des hoyaux, de leurs lances des serpes : une nation ne lèvera plus l'épée contre une autre, on n'apprendra plus la guerre.
2:5	Venez, ô maison de Yaacov, et marchons dans la lumière de YHWH.

### L'orgueilleux abaissé au jour de YHWH

2:6	Car tu as rejeté ton peuple, la maison de Yaacov, parce qu'ils se sont remplis d'orient et adonnés à la divination comme les Philistins, et parce qu'ils s'allient aux enfants des étrangers<!--De. 18:8-13 ; Os. 13:2 ; Mi. 5:11-13.-->.
2:7	Son pays est rempli d'argent et d'or, et il n'y a pas de fin à ses trésors. Son pays est rempli de chevaux, et il n'y a pas de fin à ses chars.
2:8	Son pays est rempli de faux elohîm : ils se prosternent devant l'ouvrage de leurs mains et devant ce que leurs doigts ont fabriqué.
2:9	Et l'être humain est courbé, l'homme est humilié, ne leur pardonne pas !
2:10	Entre dans les rochers et cache-toi dans la poussière, à cause de la frayeur de YHWH, et à cause de la gloire de sa majesté<!--Ap. 6:15-16.-->.
2:11	Les yeux hautains des humains seront abaissés et les hommes qui s'élèvent seront humiliés, YHWH sera seul haut élevé en ce jour-là.
2:12	Car il y a un jour assigné par YHWH Sabaoth contre tout homme orgueilleux et hautain, et contre tout homme qui s'élève, afin qu'il soit abaissé,
2:13	contre tous les cèdres du Liban, hauts et élevés, et contre tous les chênes de Bashân,
2:14	contre toutes les hautes montagnes, et contre toutes les collines élevées,
2:15	contre toutes les hautes tours, et contre toutes les murailles fortes,
2:16	contre tous les navires de Tarsis, et contre toutes les peintures de plaisance.
2:17	Et l'arrogance des humains sera humiliée, et les hommes qui s'élèvent seront abaissés :
2:18	YHWH seul sera élevé en ce jour-là. Quant aux faux elohîm, ils tomberont tous.
2:19	Et l'on entrera dans les cavernes des rochers et dans les trous de la Terre<!--Lu. 23:30 ; Ap. 6:14-17.-->, à cause de la frayeur de YHWH et à cause de sa gloire magnifique, quand il se lèvera pour faire trembler la Terre.
2:20	En ce jour-là, les humains jetteront aux taupes et aux chauves-souris leurs faux elohîm d'argent et leurs faux elohîm d'or, qu'ils s'étaient faits pour se prosterner devant eux.
2:21	Ils entreront dans les fentes des rochers et dans les creux des rochers, à cause de la frayeur de YHWH, et à cause de sa gloire magnifique, quand il se lèvera pour faire trembler la Terre.
2:22	Cessez d'être avec l'être humain, dans les narines duquel il n'y a qu'un souffle, car en quoi compte-t-il ?

## Chapitre 3

### Le péché, cause de dissolution nationale

3:1	Car voici, le Seigneur, YHWH Sabaoth, va ôter de Yeroushalaim et de Yéhouda tout appui et toute ressource, toute ressource de pain et toute ressource d'eau,
3:2	l'homme vaillant et l'homme de guerre, le juge et le prophète, le devin et l'ancien,
3:3	le chef de cinquante au visage exalté, le conseiller, le sage en arts magiques ayant du discernement et du charme.
3:4	Je leur donnerai de jeunes hommes pour chefs, et des enfants domineront sur eux.
3:5	Le peuple sera opprimé : l'homme par l'homme, et chacun par son ami. Le jeune homme se comportera avec arrogance envers le vieillard, et l'homme de rien contre l'honorable.
3:6	Même un homme ira jusqu'à saisir son frère dans la maison de son père et lui dira : Tu as un manteau, sois notre chef ! Et prends en main ces ruines !
3:7	Ce jour même il répondra : Je ne suis pas médecin, et dans ma maison il n'y a ni pain ni manteau. Ne m'établissez donc pas chef du peuple.
3:8	Certes Yeroushalaim est renversée, et Yéhouda est tombée, parce que leurs langues et leurs actions sont contre YHWH, pour braver les regards de sa gloire.
3:9	L'aspect de leur visage témoigne contre eux, ils publient leur péché comme Sodome, ils ne le cachent pas. Malheur à leur âme, car ils ont attiré le mal sur eux !
3:10	Dites au juste que du bien lui arrivera, car il mangera le fruit de ses œuvres.
3:11	Malheur au méchant ! Il lui arrivera du mal, car l'œuvre de ses mains lui sera rendue.
3:12	Quant à mon peuple, il a pour oppresseur des enfants, et des femmes dominent sur lui. Mon peuple, ceux qui te conduisent t'égarent, ils corrompent le chemin dans lequel tu marches.
3:13	YHWH se présente pour plaider, il se tient debout pour juger les peuples.
3:14	YHWH entre en jugement avec les anciens de son peuple et avec ses chefs : Vous avez brûlé la vigne ! Ce que vous avez volé au pauvre est dans vos maisons !
3:15	Pourquoi écrasez-vous mon peuple et broyez-vous la face des pauvres ? dit Adonaï YHWH Sabaoth.

### Les filles hautaines de Sion

3:16	YHWH dit aussi : Parce que les filles de Sion sont hautaines, et qu'elles marchent le cou tendu et les yeux pleins de convoitise, parce qu'elles marchent avec une fière démarche faisant du bruit avec leurs pieds,
3:17	Adonaï rendra chauve le sommet de la tête des filles de Sion, YHWH découvrira leur nudité.
3:18	En ce temps-là, Adonaï ôtera l'ornement des anneaux de cheville, et les filets et les croissants,
3:19	les pendants d'oreilles, les bracelets et les voiles,
3:20	les parures de la tête, les chaînettes des pieds et les ceintures, les boîtes à parfum et les amulettes,
3:21	les anneaux et les bagues qui leur pendent sur le nez,
3:22	les vêtements de fête et les larges tuniques, les manteaux et les sacs,
3:23	les miroirs et les chemises fines, les turbans et les voiles légers.
3:24	Et il arrivera qu'au lieu du parfum, il y aura de la puanteur ; au lieu de ceintures, des cordes ; au lieu de cheveux bouclés, des têtes chauves ; au lieu de robes flottantes, des sacs étroits ; et au lieu d'un beau teint, la peau brûlée.
3:25	Tes hommes tomberont par l'épée et ta force par la guerre.
3:26	Et ses portes gémiront et mèneront deuil. Vidée, elle s'assiéra par terre.

## Chapitre 4

### Vision du règne messianique<!--Es. 11:1-16.-->

4:1	Et en ce jour sept femmes saisiront un seul homme, et diront : Nous mangerons notre pain, et nous nous vêtirons de nos habits, mais fais-nous seulement porter ton nom ! Enlève notre opprobre !
4:2	En ce temps-là, le germe de YHWH<!--Yéhoshoua (Jésus) est le « germe » de YHWH (Es. 4:2) et le germe de David (Jé. 23:5 ; Za. 3:8, 6:12). Ce germe a été placé par la vertu du Saint-Esprit dans le sein d'une vierge (Es. 7:14 ; Lu. 1:34-35) et l'enfant qui naquit d'elle fut appelé « Fils d'Elohîm » tout en étant le El Shaddaï (El Tout-Puissant). Il existe de toute éternité en forme d'Elohîm (Jn. 1:1 ; Es. 9:5), mais il a été fait chair pour nous sauver (Jn. 1:14 ; 1 Ti. 3:16). C'est le plus grand des miracles et la démonstration de sa divinité, de sa sagesse et de son amour envers les hommes.--> sera plein de noblesse et de gloire, et le fruit de la Terre plein de grandeur et d'excellence pour les rescapés d'Israël.
4:3	Et il arrivera que ceux qui resteront à Sion, ceux qui seront laissés à Yeroushalaim, seront appelés saints, ceux de Yeroushalaim seront inscrits parmi les vivants<!--Es. 10:20-22 ; Ro. 9:27, 11:5.-->.
4:4	Quand Adonaï aura lavé la souillure des filles de Sion et purifié Yeroushalaim du sang qui est au milieu d'elle, par l'Esprit de jugement et par l'Esprit qui consume,
4:5	alors YHWH créera, sur toute l'étendue du mont Sion et sur ses assemblées, une nuée avec une fumée pendant le jour, et une splendeur de feu flamboyant pendant la nuit, car la gloire se répandra partout.
4:6	Et il y aura un tabernacle pour donner de l'ombre contre la chaleur du jour, pour servir de refuge et d'asile contre la tempête et la pluie<!--Ap. 21:3.-->.

## Chapitre 5

### Israël, vigne de YHWH

5:1	Je chanterai maintenant pour mon bien-aimé le cantique de mon bien-aimé sur sa vigne. La vigne de mon bien-aimé était devenue l'enfant de la corne fertile<!--2 S. 22:3 ; Lu. 1:69.-->.
5:2	Il l'environna d'une haie, en ôta les pierres, et y planta des vignes excellentes. Il bâtit une tour au milieu d'elle, et il y creusa aussi une cuve. Puis il espéra qu'elle produirait des raisins, mais elle a produit des grappes sauvages<!--Lu. 13:6-9.-->.
5:3	Maintenant donc, vous habitants de Yeroushalaim et vous hommes de Yéhouda, jugez, je vous prie, entre moi et ma vigne.
5:4	Qu'y avait-il encore à faire à ma vigne que je ne lui aie fait ? Pourquoi, quand j'ai espéré qu'elle produirait des raisins, a-t-elle produit des grappes sauvages ?
5:5	Maintenant donc je vous dirai ce que je vais faire à ma vigne : j'enlèverai sa haie pour qu'elle soit brûlée, je ferai une brèche dans sa clôture pour qu'elle soit piétinée.
5:6	Et je la réduirai en désert, elle ne sera plus taillée, ni cultivée : les ronces et les épines y croîtront et je donnerai mes ordres aux nuées afin qu'elles ne laissent plus tomber de pluie sur elle.
5:7	Or la maison d'Israël est la vigne de YHWH Sabaoth, et les hommes de Yéhouda sont la plante de ses délices. Il espérait d'eux de la droiture, et voici du saccagement ! De la justice, et voici des cris de détresse !

### Six malheurs en punition de l'infidélité d'Israël

5:8	Malheur à ceux qui ajoutent maison à maison, et qui joignent champ à champ, jusqu'à ce qu'il n'y ait plus d'espace et qu'ils habitent seuls au milieu du pays.
5:9	YHWH Sabaoth a dit à mes oreilles : En vérité, beaucoup de maisons, quoique grandes et belles, deviendront un objet d'épouvante, sans habitants.
5:10	Même dix arpents de vigne ne produiront qu'un bath, et un omer de semence ne produira qu'un épha.
5:11	Malheur à ceux qui se lèvent de bon matin, qui recherchent les boissons fortes, qui demeurent jusqu'au soir, et jusqu'à ce que le vin les échauffe !
5:12	La harpe et le luth, le tambourin, la flûte et le vin sont dans leurs festins, mais ils ne regardent pas l'œuvre de YHWH et ils ne voient pas l'ouvrage de ses mains.
5:13	C'est pourquoi mon peuple sera emmené captif, parce qu'il n'a pas de connaissance<!--2 R. 24:14-16 ; Os. 4:6.-->. Sa gloire, ce sont des hommes affamés, et sa multitude sera desséchée par la soif.
5:14	C'est pourquoi le shéol s'élargit, il ouvre sa gueule outre mesure. Sa magnificence y descend, sa richesse, sa foule bruyante et tous ceux qui s'y réjouissent.
5:15	Les humains seront abattus, les hommes seront humiliés, et les yeux des hautains seront humiliés.
5:16	Et YHWH Sabaoth sera élevé par le jugement, et le El saint sera sanctifié par la justice.
5:17	Les agneaux paîtront comme dans leurs pâturages, et les étrangers dévoreront les champs ruinés des riches.
5:18	Malheur à ceux qui tirent l'iniquité avec des cordes de vanité, et le péché avec les traits d'un chariot,
5:19	et qui disent : Qu'il se hâte, qu'il fasse vite son œuvre, afin que nous la voyions ! Que le conseil du Saint d'Israël s'avance et vienne, afin que nous le connaissions !
5:20	Malheur à ceux qui appellent le mal bien et le bien mal<!--Mi. 7:2.-->, qui font des ténèbres une lumière et de la lumière des ténèbres, qui font de l'amertume une douceur et de la douceur une amertume !
5:21	Malheur à ceux qui sont sages à leurs propres yeux et qui, en face d'eux-mêmes, se croient intelligents !
5:22	Malheur à ceux qui sont forts pour boire le vin et vaillants pour mêler des boissons fortes,
5:23	qui justifient le méchant pour des pots-de-vin et qui ôtent à chacun des justes sa justice.
5:24	C'est pourquoi, comme le flambeau de feu consume le chaume, et la flamme consume l'herbe sèche, ainsi leur racine sera comme la pourriture, et leur fleur sera détruite comme la poussière, parce qu'ils ont rejeté la torah de YHWH Sabaoth, et ils ont méprisé la parole du Saint d'Israël.
5:25	C'est pourquoi la colère de YHWH s'enflamme contre son peuple, il étend sa main sur lui, et il le frappe. Les montagnes tremblent, et leurs cadavres ont été mis en pièces au milieu des rues. Malgré tout cela, sa colère ne se détourne pas, mais sa main est encore étendue.
5:26	Il élève une bannière pour les nations éloignées, il les siffle des extrémités de la Terre : les voici qui se hâtent et arrivent très vite.
5:27	Personne n'est fatigué, personne ne chancelle, personne ne sommeille ni ne dort. La ceinture de leurs reins ne sera pas déliée et la courroie de leurs sandales ne sera pas rompue.
5:28	Leurs flèches sont aiguës et tous leurs arcs tendus. Les sabots de leurs chevaux ressemblent à des cailloux, et les roues de leurs chars à un tourbillon.
5:29	Leur rugissement est comme celui d'une lionne, ils rugissent comme des lionceaux : ils grondent et saisissent la proie, ils l'emportent et personne ne vient à son secours.
5:30	En ce jour-là, on mènera un bruit sur lui, semblable au mugissement de la mer. En regardant la Terre, on ne verra que ténèbres et détresse, et la lumière sera obscurcie par les nuées.

## Chapitre 6

### Révélation de YHWH à Yesha`yah

6:1	L'année de la mort du roi Ouzyah, je vis Adonaï assis sur un trône haut et élevé. Les pans de sa robe remplissaient le temple<!--2 Ch. 26:23.-->.
6:2	Les séraphins se tenaient au-dessus de lui. Chacun d'eux avait six ailes : deux dont ils se couvraient la face, deux dont ils se couvraient les pieds et deux dont ils se servaient pour voler.
6:3	Et ils criaient l'un à l'autre, et disaient : Saint, saint, saint est YHWH Sabaoth ! Toute la Terre est pleine de sa gloire !
6:4	Et les fondements des seuils furent ébranlés par la voix de celui qui criait et la maison fut remplie de fumée.
6:5	Alors je dis : Malheur à moi ! Je suis perdu, car je suis un homme dont les lèvres sont impures, j'habite au milieu d'un peuple dont les lèvres sont impures et mes yeux ont vu le Roi, YHWH Sabaoth<!--Jg. 13:21-22.-->.
6:6	Mais l'un des séraphins vola vers moi, tenant à la main un charbon ardent, qu'il avait pris sur l'autel avec des pincettes.
6:7	Il en toucha ma bouche, et dit : Voici, ceci a touché tes lèvres, c'est pourquoi ton iniquité est ôtée, et la propitiation est faite pour ton péché.
6:8	Puis j'entendis la voix d'Adonaï qui disait : Qui enverrai-je et qui marchera pour nous ? Je répondis : Me voici, envoie-moi !

### Mission de Yesha`yah

6:9	Et il dit : Va et dis à ce peuple<!--Mt. 13:14.--> : Entendez, entendez et ne comprenez pas ; regardez, regardez et ne discernez pas.
6:10	Engraisse le cœur de ce peuple, rends ses oreilles pesantes et bouche-lui les yeux, de peur qu'il ne voie de ses yeux, qu'il n'entende de ses oreilles, que son cœur ne comprenne, qu'il ne se convertisse et ne soit guéri<!--Mt. 13:15 ; Mc. 4:12 ; Jn. 12:40 ; Ac. 28:27.-->.
6:11	Je dis : Jusqu'à quand, Adonaï ? Et il répondit : Jusqu'à ce que les villes soient dévastées et sans habitants, que les maisons soient sans êtres humains, et que la Terre soit dévastée et réduite en désolation ;
6:12	jusqu'à ce que YHWH ait éloigné les êtres humains et qu'une grande partie du pays soit abandonnée.
6:13	Toutefois s'il y reste un dixième des habitants, ils reviendront pour être la proie des flammes. Comme le térébinthe et le chêne, dont la souche reste quand ils sont abattus, la semence sainte en sera la souche<!--Ro. 11:17-25.-->.

## Chapitre 7

### Retsin et Pékach complotent contre Yéhouda

7:1	Et il arriva du temps d'Achaz, fils de Yotham, fils d'Ouzyah, roi de Yéhouda, que Retsin, roi de Syrie, et Pékach, fils de Remalyah, roi d'Israël, montèrent contre Yeroushalaim pour lui faire la guerre, mais ils ne purent l'assiéger.
7:2	Et on fit ce rapport à la maison de David en disant : Les Syriens ont établi leur camp sur le territoire d'Éphraïm. Et le cœur d'Achaz, et le cœur de son peuple furent ébranlés comme les arbres des forêts qui sont ébranlés par le vent.
7:3	Alors YHWH dit à Yesha`yah : Sors maintenant au-devant d'Achaz, toi et Shear-Yashoub, ton fils, vers l'extrémité de l'aqueduc de l'étang supérieur, sur la route du champ du foulon.
7:4	Et dis-lui : Prends garde à toi, sois tranquille ! N'aie pas peur et que ton cœur ne faiblisse pas devant ces deux bouts de tisons fumants, devant l'ardente colère de Retsin et de la Syrie ainsi que du fils de Remalyah !
7:5	Parce que la Syrie, Éphraïm et le fils de Remalyah se sont consultés ensemble pour te faire du mal, en disant :
7:6	Montons contre Yéhouda, assiégeons la ville, battons-la en brèche, et établissons pour roi le fils de Tabeel au milieu d'elle.
7:7	Ainsi parle Adonaï YHWH : Cela ne tiendra pas, cela n'aura pas lieu.
7:8	Car la tête de la Syrie c'est Damas, et le chef de Damas c'est Retsin. Encore 75 ans, Éphraïm sera brisé et ne sera plus un peuple.
7:9	Et la tête d'Éphraïm c'est la Samarie, et le chef de la Samarie c'est le fils de Remalyah. Si vous ne croyez pas, certainement vous ne serez pas affermis.

### Annonce de la naissance d'Immanou-El (Emmanuel)

7:10	Et YHWH parla de nouveau à Achaz, en disant :
7:11	Demande pour toi un signe à YHWH, ton Elohîm, demande-le au plus profond ou sur les sommets, là-haut.
7:12	Et Achaz répondit : Je ne demanderai rien, et je ne tenterai pas YHWH.
7:13	Alors Yesha`yah dit : Écoutez maintenant, ô maison de David ! Est-ce trop peu pour vous de lasser les hommes, que vous lassiez aussi mon Elohîm ?
7:14	C'est pourquoi Adonaï lui-même vous donnera un signe : Voici, la vierge<!--Le mot hébreu « almah » signifie aussi « jeune fille ».--> deviendra enceinte, elle enfantera un fils et appellera son Nom Immanou-El<!--Le nom « Immanou-El » vient de l'hébreu « `Immanuw'el » qui signifie « El est avec nous ». Mt. 28:20 : « Et voici, je suis avec vous tous les jours jusqu'à l'achèvement de l'âge ». Yéhoshoua (Jésus) est Immanou-El, Elohîm avec nous jusqu'à l'achèvement de l'âge (fin des temps).-->.
7:15	Il mangera du lait et du miel, jusqu'à ce qu'il sache rejeter le mal et choisir le bien.
7:16	Mais avant que le garçon sache rejeter le mal et choisir le bien, le sol des deux rois devant qui tu ressens une aversion sera abandonné.

### Prophétie sur l'imminente invasion de Yéhouda<!--2 Ch. 28:1-20.-->

7:17	YHWH fera venir sur toi, sur ton peuple et sur la maison de ton père, par le roi d'Assyrie, des jours tels qu'il n'y en a pas eu de semblable depuis le jour où Éphraïm s'est séparé de Yéhouda.
7:18	Et il arrivera qu'en ce jour-là, YHWH sifflera les mouches qui sont à l'extrémité des ruisseaux d'Égypte, et les abeilles qui sont dans le pays d'Assyrie.
7:19	Elles viendront et se poseront sur toutes les vallées désertes, sur les fentes des rochers, sur tous les buissons et sur tous les pâturages.
7:20	En ce jour-là, Adonaï rasera avec un rasoir qu'il aura loué au-delà du fleuve, avec le roi d'Assyrie, la tête et les poils des pieds, et il enlèvera aussi la barbe<!--2 R. 16:5-9.-->.
7:21	Et il arrivera, en ce jour-là, qu'un homme nourrira une jeune vache et deux brebis.
7:22	Et il arrivera qu'en raison de l'abondance du lait qu'elles produiront, on se nourrira du beurre. Car tous ceux qui seront restés dans le pays se nourriront du beurre et du miel.
7:23	Et il arrivera, en ce jour-là, que tout lieu où il y aura 1 000 vignes, valant 1 000 sicles d'argent, sera réduit en ronces et en épines.
7:24	On y entrera avec des flèches et avec l'arc, car tout le pays ne sera que ronces et épines.
7:25	Quant à toutes les montagnes cultivées à la houe, on ne craindra plus de voir des ronces et des épines. Mais on y lâchera les bœufs, et la brebis en foulera le sol.

## Chapitre 8

### Annonce de la défaite de Damas et de la Samarie

8:1	Et YHWH me dit : Prends un grand rouleau et écris dessus en grosses lettres : Qu'on se dépêche de butiner, qu'on se hâte de piller.
8:2	Et je pris avec moi des témoins fidèles : Ouriyah, le prêtre, et Zekaryah, fils de Berekyah.
8:3	Puis je m'étais approché de la prophétesse ; elle conçut et elle enfanta un fils. Et YHWH me dit : Appelle-le du nom de Maher-Shalal-Chash-Baz<!--« Maher-Shalal-Chash-Baz » signifie « rapide au butin, rapide sur la proie ».-->.
8:4	Car avant que le garçon sache dire : Mon père ! Ma mère ! On enlèvera la puissance de Damas et le butin de Samarie, devant le roi d'Assyrie.
8:5	Et YHWH continua encore de me parler, en disant :
8:6	Parce que ce peuple a rejeté les eaux de Siloé qui coulent doucement, et qu'il s'est réjoui au sujet de Retsin, et du fils de Remalyah,
8:7	à cause de cela, voici, Adonaï va faire monter contre eux les puissantes et grandes eaux du fleuve : le roi d'Assyrie et toute sa gloire. Il s'élèvera partout au-dessus de son lit, et il se répandra sur toutes ses rives.
8:8	Et il pénétrera dans Yéhouda, il débordera et inondera, il atteindra jusqu'au cou. Et les étendues de ses ailes rempliront la largeur de ton pays, ô Immanou-El !

### Exhortation aux disciples de YHWH à rester fidèles

8:9	Soyez méchants, peuples ! Et vous serez brisés. Prêtez l'oreille, toutes les contrées lointaines de la Terre ! Ceignez-vous ! Et vous serez brisés. Ceignez-vous ! Et vous serez brisés.
8:10	Dressez vos plans<!--Le mot hébreu signifie aussi « prendre conseil ».--> et ils échoueront ! Dites une parole, et elle ne s'accomplira pas ! Car El est avec nous.
8:11	Car ainsi m'a parlé YHWH, quand sa main me saisit et qu'il me corrigea afin de ne pas marcher dans la voie de ce peuple, en disant :
8:12	Ne dites pas : conspiration, toutes les fois que ce peuple dit conspiration ! Ne craignez pas ce qu'il craint, ne le redoutez pas !
8:13	C'est YHWH Sabaoth<!--YHWH des armées. Voir 1 S. 1:3.-->, que vous devez sanctifier<!--Voir 1 Pi. 3:15 ; Ap. 19.-->, c'est lui que vous devez craindre et redouter.
8:14	Et il sera un sanctuaire, mais aussi une pierre d'achoppement<!--YHWH s'est présenté comme une pierre d'achoppement et un rocher de scandale. En Es. 44:8, il affirme ne pas connaître d'autre rocher que lui. Yesha`yah n'est pas le seul prophète à qui le Seigneur s'est révélé comme étant une pierre et un rocher. Dans le Ps. 118:22-23, il est dit : « La Pierre que les bâtisseurs avaient rejetée est devenue la principale de l'angle ». Daniye'l et Zekaryah (Zacharie) ont également prophétisé au sujet de cette pierre : « Tu regardais cela, jusqu'à ce qu'une pierre se détacha sans l'aide d'une main, frappa les pieds en fer et en argile de la statue et les brisa. Alors le fer, l'argile, le cuivre, l'argent et l'or, furent brisés ensemble et devinrent comme la paille de l'aire en été, que le vent transporte çà et là et nulle trace n'en fut retrouvée. Mais la pierre qui avait frappé la statue devint une grande montagne et remplit toute la terre. » (Da. 2:34-35). « Car voici, quant à la pierre que j'ai mise devant Yéhoshoua, sur cette pierre qui n'est qu'une, il y a sept yeux. Voici, je graverai moi-même ce qui doit y être gravé, dit YHWH Sabaoth. J'ôterai en un jour l'iniquité de ce pays. » (Za. 3:9). Ces prophéties se sont accomplies en Yéhoshoua ha Mashiah (Jésus-Christ), l'Agneau d'Elohîm qui ôte le péché du monde (Jn. 1:29). Le Seigneur s'est d'ailleurs clairement identifié à la pierre angulaire, affirmant ainsi sa divinité (Lu. 20:17-19). En Mt. 16:18, il s'est présenté comme le rocher inébranlable sur lequel il allait bâtir son Assemblée (Église). De plus, il est à noter que dans le livre d'Apocalypse, l'Agneau possède sept yeux comme la pierre vue par Zekaryah (Ap. 5:6). Ces sept yeux sont aussi les sept lampes du chandelier d'or que Zekaryah et Yohanan avaient également vues (Za. 4:2 ; Ap. 4:5). Or le chiffre 7 symbolise la plénitude et la perfection divines. Yesha`yah prophétisa encore en ces termes : « Voici, je mettrai pour fondement en Sion une pierre, une pierre éprouvée, la pierre angulaire la plus précieuse, pour être un fondement solide. Celui qui croira ne se hâtera pas. » (Es. 28:16). Les écrits de la Nouvelle Alliance attestent l'accomplissement de cette prophétie en Yéhoshoua ha Mashiah, notamment par la bouche de Paulos (Paul) et de Petros (Pierre) : « Ayant été édifiés sur le fondement des apôtres et des prophètes, et Yéhoshoua ha Mashiah lui-même étant la pierre angulaire. » (Ep. 2:20). « Car personne ne peut poser un autre fondement que celui qui est déjà posé, lequel est Yéhoshoua Mashiah. » (1 Co. 3:11). « Vous approchant de lui, Pierre vivante, rejetée en effet par les humains, mais choisie et précieuse devant Elohîm. Vous aussi, comme des pierres vivantes, vous êtes édifiés, maison spirituelle, sainte prêtrise, afin d'offrir des sacrifices spirituels, agréables à Elohîm par Yéhoshoua Mashiah. » (1 Pi. 2:4-5).-->, un rocher de scandale pour les deux maisons d'Israël, un filet et un piège pour les habitants de Yeroushalaim.
8:15	Beaucoup parmi eux trébucheront, ils tomberont et se briseront, ils seront pris au piège et capturés.
8:16	Enveloppe ce témoignage, scelle cette torah parmi mes disciples.
8:17	Je m'attends à YHWH, qui cache sa face à la maison de Yaacov, et je regarde à lui.
8:18	Me voici, avec les enfants que YHWH m'a donnés : nous sommes des signes et des miracles en Israël, de la part de YHWH Sabaoth, qui habite sur la montagne de Sion<!--Hé. 2:13.-->.
8:19	Si l'on vous dit : Consultez ceux qui évoquent les morts et ceux qui ont un esprit de divination, qui grommellent et murmurent ! Un peuple doit-il consulter les morts en faveur des vivants et non pas son Elohîm ?
8:20	À la torah et au témoignage ! Si l'on ne parle pas ainsi, il n'y aura certainement pas d'aurore pour le peuple.
8:21	Il sera errant dans le pays, accablé et affamé, et il arrivera que lorsqu'il aura faim, il s'irritera, maudira son roi et son Elohîm et tournera les yeux en haut ;
8:22	puis il regardera vers la Terre, et voici, il n'y aura que détresse, ténèbres et de sombres angoisses : il sera enfoncé dans l'obscurité.
8:23	Mais les ténèbres ne seront pas autant qu'elles avaient été là où il y a de la détresse. Si au commencement Elohîm affligea légèrement le pays de Zebouloun et le pays de Nephthali, dans l'avenir, il couvrira de gloire la route de la mer, au-delà du Yarden<!--Jourdain.-->, la Galilée des nations.

## Chapitre 9

### Annonce de la naissance et du règne du Mashiah (Christ)

9:1	Le peuple qui marchait dans les ténèbres voit une grande lumière, et la lumière resplendit sur ceux qui habitaient le pays de l'ombre de la mort<!--Mt. 4:15-16.-->.
9:2	Tu multiplies la nation, tu lui accordes de grandes joies, ils se réjouissent devant toi, comme on se réjouit à la moisson, comme on se réjouit quand on partage le butin.
9:3	Car tu as brisé le joug dont il était chargé et le bâton qui frappait ses épaules, la verge de celui qui l'opprimait comme au jour de Madian.
9:4	Car toute sandale de celui qui marche avec bruit et tous les manteaux roulés dans le sang seront livrés à l'incendie, dévorés par le feu.
9:5	Car un enfant nous est né, un Fils nous a été donné<!--Yéhoshoua ha Mashiah (Jésus-Christ) est 100% Elohîm et 100% humain. Il existe depuis toute éternité en tant qu'Elohîm. Il est devenu homme au moment de son incarnation (Ph. 2:5-7).-->, et l'empire reposera sur son épaule : on l'appellera du nom de Miracle<!--Le mot hébreu veut aussi dire « Admirable ».-->, Conseiller, El Gibbor<!--Da. 11:36 ; Je. 32:18. El Gibbor signifie « El Puissant ». Es. 10:21 ; Jo. 22:22 ; 2 S. 22:32.-->, Père d'éternité<!--Philippos (Philippe), disciple de Yéhoshoua ha Mashiah (Jésus-Christ) voulait rencontrer le Père. Il posa au Seigneur cette question « Seigneur, montre-nous le Père, et cela nous suffit » (Jn. 14:8). Yéhoshoua lui répondit : « Je suis depuis si longtemps avec vous et tu ne m'as pas connu, Philippos ! » (Jn. 14:9).-->, Prince de paix,
9:6	pour accroître l'empire, et une paix sans fin au trône de David et à son royaume, pour l'affermir et le soutenir par le droit et par la justice, dès maintenant et à toujours<!--Lu. 1:32-33.-->. Voilà ce que fera le zèle de YHWH Sabaoth.

### Jugement sur le royaume du nord

9:7	Adonaï envoie une parole à Yaacov, et elle tombe sur Israël<!--Ge. 32:28.-->.
9:8	Et tout le peuple en aura connaissance, Éphraïm et les habitants de Samarie, qui disent avec orgueil et avec un cœur hautain :
9:9	Des briques sont tombées ? Nous bâtirons en pierres de taille. Des sycomores ont été coupés ? Nous les changerons en cèdres.
9:10	YHWH élèvera contre eux les ennemis de Retsin, et il armera les ennemis d'Israël :
9:11	la Syrie à l'orient, et les Philistins à l'occident. Ils dévoreront Israël à pleine bouche. Malgré tout cela, sa colère ne se détourne pas et sa main est encore étendue.
9:12	Parce que le peuple ne revient pas à celui qui le frappe, et il ne cherche pas YHWH Sabaoth.
9:13	À cause de cela YHWH retranchera d'Israël en un seul jour la tête et la queue, la branche de palmier et le roseau.
9:14	L'ancien et le magistrat, c'est la tête, et le prophète qui enseigne le mensonge, c'est la queue.
9:15	Ceux qui font croire à ce peuple qu'il est heureux l'égarent<!--1 Ti. 4:1 ; Tit. 1:10.--> et ceux qui se laissent diriger par eux se perdent.
9:16	C'est pourquoi Adonaï ne saurait prendre plaisir à leurs jeunes hommes ni avoir pitié de leurs orphelins et de leurs veuves, car tous sont des athées et des méchants, et toute bouche ne profère que des infamies. Malgré tout cela, sa colère ne s'apaise pas et sa main est encore étendue.
9:17	Car la méchanceté consume comme un feu, elle dévore les ronces et les épines. Elle embrase l'épaisseur de la forêt, d'où s'élèvent des colonnes de fumée.
9:18	À cause de la fureur de YHWH Sabaoth, la Terre est obscurcie, et le peuple est comme la proie du feu : personne n'a compassion de son frère.
9:19	On pille à droite et l'on a faim, on dévore à gauche et l'on n'est pas rassasié. Chacun mange la chair de son bras.
9:20	Menashè dévore Éphraïm, Éphraïm dévore Menashè, et ensemble ils fondent sur Yéhouda. Malgré tout cela, sa colère ne s'apaise pas, et sa main est encore étendue.

## Chapitre 10

10:1	Malheur à ceux qui décrètent des ordonnances iniques, et à ceux qui écrivent pour ordonner l'oppression,
10:2	pour refuser la justice aux pauvres et ravir le droit aux malheureux de mon peuple, afin d'avoir les veuves pour leur butin, et de piller les orphelins !
10:3	Et que ferez-vous au jour du châtiment, et de la ruine éclatante qui viendra de loin ? Vers qui fuirez-vous pour avoir du secours et où laisserez-vous votre gloire<!--Os. 9:7 ; Mt. 24:17-21 ; Lu. 19:41-44.--> ?
10:4	Les uns seront courbés parmi les prisonniers, les autres tomberont parmi les morts. Malgré tout cela, sa colère ne s'apaise pas, et sa main est encore étendue.

### Jugement sur l'Assyrie

10:5	Malheur à l'Assyrie, la verge de ma colère ! Le bâton dans sa main, c'est ma colère.
10:6	Je l'ai envoyé contre une nation athée, je lui ai donné mes ordres contre le peuple de ma colère, pour piller et pour dépouiller, pour le fouler aux pieds comme la boue des rues.
10:7	Mais il n'en juge pas ainsi, et ce n'est pas là la pensée de son cœur, car il a à cœur de détruire et de retrancher des nations, non en petit nombre.
10:8	Car il dit : Mes princes ne sont-ils pas autant de rois ?
10:9	Calno n'est-elle pas comme Carkemish ? Hamath n'est-elle pas comme Arpad ? Et Samarie n'est-elle pas comme Damas ?
10:10	Puisque ma main a soumis les royaumes qui avaient des faux elohîm, où il y avait plus d'images taillées qu'à Yeroushalaim et à Samarie,
10:11	ne traiterai-je pas Yeroushalaim et ses faux elohîm, tout comme j'ai traité Samarie avec ses idoles ?
10:12	Mais il arrivera que, quand Adonaï aura achevé toute son œuvre sur la montagne de Sion et à Yeroushalaim, je punirai le roi d'Assyrie pour le fruit de son cœur orgueilleux, et pour la gloire de ses regards hautains.
10:13	Parce qu'il dit : C'est par la force de ma main que j'ai agi, c'est par ma sagesse, car je suis intelligent. J'ai reculé les bornes des peuples, et j'ai pillé ce qu'ils avaient de plus précieux. Comme un homme vaillant, j'ai fait descendre ceux qui étaient assis.
10:14	Ma main a trouvé les richesses des peuples comme on trouve un nid. Comme on rassemble des œufs délaissés, ainsi ai-je rassemblé toute la Terre. Personne n'a remué l'aile, ni ouvert le bec, ni poussé un cri.
10:15	La hache se glorifie-t-elle envers celui qui s'en sert ? Ou la scie s'élève-t-elle au-dessus de celui qui la manie ? Comme si la verge faisait mouvoir celui qui la lève, et que le bâton se levait comme s'il n'était pas du bois !
10:16	C'est pourquoi Adonaï, YHWH Sabaoth, enverra la maigreur sur ses hommes gras. Et sous sa gloire s'allumera un embrasement, tel l'embrasement d'un feu.
10:17	Car la lumière d'Israël deviendra un feu et son Saint une flamme qui embrasera et consumera ses épines et ses ronces tout en un jour.
10:18	Il consumera la gloire de sa forêt et de ses campagnes, depuis l'âme jusqu'à la chair. Il en sera comme quand celui qui porte la bannière est défait.
10:19	Le reste des arbres de sa forêt pourra être compté, et un garçon en écrirait le nombre.

### Conversion et délivrance du reste d'Israël

10:20	Et il arrivera en ce jour-là, que le reste d'Israël et les rescapés de la maison de Yaacov ne s'appuieront plus sur celui qui les frappait, mais ils s'appuieront avec confiance sur YHWH, le Saint d'Israël.
10:21	Le reste<!--Ro. 9:27.--> retournera, le reste de Yaacov, vers El Gibbor<!--Ge. 49:24 ; Es. 9:5 ; Je. 32:18 ; Mt. 1:23.-->.
10:22	Car quand ton peuple, ô Israël, serait comme le sable de la mer, un reste seulement retournera. La destruction est résolue, elle fera déborder la justice.
10:23	Car la destruction qu'il a résolue, Adonaï YHWH Sabaoth, va l'exécuter au milieu de toute la Terre.
10:24	C'est pourquoi ainsi parle Adonaï YHWH Sabaoth : Mon peuple, qui habite en Sion, n'aie pas peur du roi d'Assyrie ! Il te frappe de la verge et il lève son bâton sur toi comme faisait l'Égypte.
10:25	Mais encore un peu de temps, un peu de temps, et le châtiment cessera, puis ma colère se tournera contre lui pour l'exterminer.
10:26	YHWH Sabaoth lèvera le fouet contre lui tout comme il a frappé Madian au rocher d'Oreb, et il lèvera encore son bâton sur la mer, comme il l'a levé sur le chemin d'Égypte.
10:27	En ce jour-là, son fardeau sera ôté de dessus ton épaule et son joug de dessus ton cou. Et le joug sera détruit en présence de l'onction.

### Défaite des Assyriens<!--Es. 35-36, 37:7.-->

10:28	Il marche sur Ayyat, traverse Migron et il met ses bagages à Micmash.
10:29	Ils passent le gué, ils couchent à Guéba. Ramah est effrayée, Guibea de Shaoul prend la fuite.
10:30	Pousse des cris, fille de Gallim ! Malheur à toi Anathoth ! Prends garde Laïs !
10:31	Madména se disperse, les habitants de Guébim se sauvent en foule.
10:32	Encore un jour d'arrêt à Nob, et il menace de sa main la montagne de la fille de Sion, la colline de Yeroushalaim.
10:33	Voici, le Seigneur, YHWH Sabaoth, brisera les rameaux avec force : ceux qui sont de hautes statures seront coupés, et les plus élevés seront abaissés.
10:34	Et il taillera avec le fer les lieux les plus épais de la forêt, et le Liban tombera sous le Puissant.

## Chapitre 11

### Rétablissement du règne de David par le Mashiah

11:1	Mais une verge sortira du tronc d'Isaï, et un rejeton de ses racines<!--Mt. 1:6-16 ; Lu. 1:31-32 ; Ro. 15:12 ; Ap. 5:5.--> portera du fruit.
11:2	L'Esprit de YHWH reposera sur lui, Esprit de sagesse et de discernement, Esprit de conseil et de force, Esprit de connaissance et de crainte de YHWH<!--Es. 61:1 ; Lu. 4:18.-->.
11:3	Il respirera la crainte de YHWH. Il ne jugera pas d'après ce que ses yeux voient et ne corrigera pas d'après ce que ses oreilles entendent<!--Jé. 11:20 ; Mt. 22:16 ; Ap. 2:23.-->.
11:4	Mais il jugera les pauvres avec justice et corrigera les malheureux de la Terre avec droiture. Il frappera la terre par la verge de sa bouche et fera mourir le méchant par l'Esprit de ses lèvres<!--Job 4:9, 15:30 ; 2 Th. 2:8.-->.
11:5	La justice sera la ceinture de ses reins, et la fidélité, la ceinture de ses flancs<!--Ep. 6:14.-->.
11:6	Le loup habitera avec l'agneau, et le léopard se couchera avec le chevreau. Le veau, le lionceau et le bétail qu'on engraisse seront ensemble, et un petit garçon les conduira.
11:7	La jeune vache paîtra avec l'ourse, leurs petits auront un même gîte, et le lion, comme le bœuf, mangera de la paille<!--Es. 65:25.-->.
11:8	Le nourrisson s'amusera<!--Ou encore « caresser ».--> sur le trou du cobra et l'enfant sevré mettra sa main dans l'antre de la vipère.
11:9	On ne fera plus de mal et on ne détruira plus sur toute ma montagne sainte, car la Terre sera remplie de la connaissance de YHWH, comme les eaux couvrent le fond de la mer.
11:10	Et il arrivera en ce jour-là que la racine<!--Yéhoshoua (Jésus) est la Racine de David. Voir Ro. 15:12 ; Ap. 5:5, 22:16.--> d'Isaï se tiendra debout comme une bannière<!--Voir le dictionnaire en annexe.--> pour les peuples ; les nations la chercheront, et son lieu de repos sera glorieux.

### Établissement du règne du Mashiah

11:11	Et il arrivera en ce jour-là, qu'Adonaï étendra encore sa main une seconde fois pour acquérir le reste de son peuple dispersé en Assyrie, en Égypte, à Pathros, en Éthiopie, à Élam, à Shinear, à Hamath et dans les îles de la mer.
11:12	Il élèvera une bannière parmi les nations, il rassemblera les exilés d'Israël qui auront été chassés, et il recueillera les dispersés de Yéhouda des quatre extrémités de la Terre.
11:13	Et la jalousie d'Éphraïm sera ôtée, et les oppresseurs de Yéhouda seront retranchés ; Éphraïm ne sera plus jaloux de Yéhouda, et Yéhouda n'opprimera plus Éphraïm.
11:14	Mais ils voleront sur l'épaule des Philistins vers la mer, ils pilleront ensemble les fils de l'orient. Édom et Moab seront la proie de leurs mains et les enfants d'Ammon leur obéiront.
11:15	YHWH vouera à l'interdit la langue de la mer d'Égypte, et il lèvera sa main contre le fleuve par la force de son vent, et il le frappera sur les sept rivières, et fera qu'on y marche avec des sandales.
11:16	Et il y aura un chemin pour le reste de son peuple, qui sera échappé de l'Assyrie, comme il y en eut un pour Israël le jour où il remonta du pays d'Égypte.

## Chapitre 12

### Louange au sein du royaume

12:1	Tu diras en ce jour-là : Je te loue, ô YHWH ! Car tu as été irrité contre moi, ta colère s'est apaisée, et tu m'as consolé.
12:2	Voici, El est mon salut<!--Voir commentaire en Ge. 49:18.-->. J'aurai confiance et je ne craindrai rien, car Yah, YHWH est ma force et ma louange. Il est mon Sauveur.
12:3	Et vous puiserez de l'eau avec joie aux sources du salut<!--Jn. 4:10-14.-->,
12:4	et vous direz en ce jour-là : Louez YHWH, invoquez son Nom, publiez ses œuvres parmi les peuples, rappelez que son Nom est une haute retraite !
12:5	Chantez des louanges à YHWH car il a fait des choses magnifiques : cela est connu dans toute la Terre !
12:6	Habitante de Sion, égaye-toi, et réjouis-toi avec chant de triomphe ! Car le Saint d'Israël est grand au milieu de toi.

## Chapitre 13

### YHWH lève une armée

13:1	Prophétie sur Babel, révélée à Yesha`yah, fils d'Amots.
13:2	Élevez la bannière sur la haute montagne, élevez la voix vers eux, faites des signes avec la main, et qu'on entre dans les portes des magnifiques !
13:3	C'est moi qui ai donné des ordres à ceux qui me sont consacrés, j'ai appelé mes hommes vaillants pour exécuter ma colère, ceux qui se réjouissent de ma grandeur.
13:4	Il y a sur les montagnes un bruit d'une multitude, comme celui d'un grand peuple : on entend un tumulte de royaumes, de nations rassemblées. YHWH Sabaoth passe en revue l'armée pour le combat.
13:5	D'un pays éloigné, de l'extrémité des cieux, YHWH vient avec les instruments de sa colère pour détruire tout le pays.

### Jugement de YHWH sur Babel (Babylone)

13:6	Hurlez, car le jour de YHWH<!--Voir commentaire en Za. 14:1.--> est proche, il vient comme un ravage de Shaddaï.
13:7	C'est pourquoi toutes les mains deviennent lâches, et tout cœur d'homme se fond.
13:8	Ils sont épouvantés. Les détresses et les douleurs les saisissent, ils sont en travail comme celle qui enfante. Ils se regardent les uns les autres avec stupeur, leurs visages sont comme des visages enflammés.
13:9	Voici, le jour de YHWH<!--Voir commentaire en Za. 14:1.--> arrive. Jour cruel, jour de colère et d'ardente fureur<!--Mal. 4:1 ; Ap. 19:15.--> qui transformera la Terre en désolation et en exterminera les pécheurs.
13:10	Même les étoiles des cieux et leurs astres ne feront plus briller leur lumière, le soleil s'obscurcira dès son lever, et la lune ne fera plus resplendir sa lueur<!--Joë. 2:31 ; Mt. 24:29 ; Mc. 13:24.-->.
13:11	Je punirai le monde habitable à cause de sa malice, et les méchants à cause de leur iniquité. Je ferai cesser l'orgueil des hautains et j'abaisserai l'arrogance des tyrans.
13:12	Je ferai qu'un homme sera plus précieux que l'or fin, et un être humain plus que l'or d'Ophir.
13:13	C'est pourquoi j'ébranlerai les cieux, et la Terre sera secouée de sa base<!--Ag. 2:6.-->, à cause de la fureur de YHWH Sabaoth, et à cause du jour de son ardente colère.
13:14	Et chacun sera comme un chevreuil qui est chassé, et comme une brebis que personne ne retire, chacun se tournera vers son peuple, chacun fuira vers son pays.
13:15	Quiconque sera trouvé, sera transpercé ; et quiconque s'y sera joint, tombera par l'épée.
13:16	Et leurs petits enfants seront écrasés sous leurs yeux<!--Na. 3:10.-->, leurs maisons seront pillées, et leurs femmes violées.

### YHWH envoie les Mèdes contre Babel

13:17	Voici, je vais susciter contre eux les Mèdes, qui ne font pas cas de l'argent, et qui ne convoitent pas l'or.
13:18	Leurs arcs écraseront les jeunes hommes, et ils seront sans pitié pour le fruit des entrailles, leur œil n'épargnera pas les enfants.
13:19	Ainsi Babel, l'ornement des royaumes, la parure et l'orgueil des Chaldéens, sera comme Sodome et Gomorrhe qu'Elohîm détruisit.
13:20	Elle ne sera plus jamais habitée, elle ne sera pas habitée de génération en génération. Même les Arabes n'y dresseront pas leurs tentes, et les bergers n'y feront plus reposer leurs troupeaux.
13:21	Mais les bêtes sauvages des déserts y prendront leur gîte, et les hiboux rempliront ses maisons, les autruches<!--Littéralement à traduire par « filles de l'autruche ».--> en feront leur demeure, et les boucs y sauteront.
13:22	Les chacals hurleront dans ses palais, et les dragons dans ses maisons de plaisance. Son temps est près d'arriver et ses jours ne se prolongeront pas.

## Chapitre 14

### Chant d'Israël après la chute de Babel

14:1	Car YHWH aura pitié de Yaacov, il choisira encore Israël, et il les rétablira dans leur pays ; les étrangers se joindront à eux et s'attacheront à la maison de Yaacov.
14:2	Et les peuples les prendront, et les ramèneront à leur demeure, et la maison d'Israël les possédera en droit d'héritage dans le pays de YHWH, comme serviteurs et comme servantes ; ils retiendront captifs ceux qui les avaient tenus captifs, et ils domineront sur leurs oppresseurs.
14:3	Et il arrivera qu'au jour où YHWH te fera reposer de ton travail, de ton tourment, et de la dure servitude qui te fut imposée,
14:4	alors tu prononceras ce proverbe contre le roi de Babel, et tu diras : Comment le tyran a cessé ! La cité en or a cessé !
14:5	YHWH a brisé le bâton des méchants, et la verge des dominateurs.
14:6	Celui qui frappait avec fureur les peuples de coups qu'on ne pouvait pas détourner, qui dominait sur les nations avec colère, est poursuivi sans ménagement.
14:7	Toute la Terre jouit du repos et de la paix, on éclate en chants de triomphe à gorge déployée.
14:8	Même les cyprès et les cèdres du Liban se réjouissent de toi, en disant : Depuis que tu es tombé, personne n'est monté pour nous abattre.
14:9	Le shéol dans ses parties inférieures s'agite à ton sujet pour venir à ta rencontre. Il réveille à cause de toi les ombres et il fait lever de leurs sièges tous les principaux de la Terre.
14:10	Tous prennent la parole pour te dire : Toi aussi, tu es sans force comme nous, tu es devenu semblable à nous !
14:11	Ta hauteur est descendue dans le shéol, avec le son de tes luths. Tu es couché sur une couche de vers et la vermine est ta couverture.

### Orgueil, rébellion et chute de Satan

14:12	Comment es-tu tombé du ciel, Lucifer<!--« Porteur de lumière », « le brillant », « l'étoile du matin ».-->, fils de l'aurore ? Toi qui foulais les nations, tu es abattu jusqu'à terre !
14:13	Tu disais en ton cœur : Je monterai aux cieux, je placerai mon trône au-dessus des étoiles de El et je m'assiérai sur la montagne de l'assemblée, du côté d'Aquilon<!--Aquilon est un dieu des vents septentrionaux, froids et violents, dans la mythologie romaine.--> ;
14:14	je monterai au-dessus des hauts lieux des nuées, je serai semblable à Elyon.
14:15	Et cependant tu as été précipité dans le shéol, dans les profondeurs de la fosse<!--Voir commentaire en Ge. 1:1-2.-->.
14:16	Ceux qui te voient fixent sur toi leurs regards, ils te considèrent attentivement, en disant : N'est-ce pas celui qui faisait trembler la Terre, qui ébranlait les royaumes,
14:17	qui transformait le monde habitable en désert, qui détruisait les villes et n'ouvrait pas la maison à ses prisonniers ?

### Babel anéantie

14:18	Tous les rois des nations, oui, tous, reposent avec honneur, chacun dans sa maison.
14:19	Mais toi, tu as été jeté loin de ton sépulcre, comme un rejeton pourri, comme une dépouille de gens tués, transpercés avec l'épée, qu'on jette sous les pierres d'une fosse, comme un cadavre foulé aux pieds.
14:20	Tu ne seras pas rangé comme eux dans le sépulcre, car tu as ravagé ta Terre, tu as tué ton peuple. La race des méchants ne sera plus jamais nommée.
14:21	Préparez le massacre de ses enfants à cause de l'iniquité de leurs pères ! Qu'ils ne se lèvent pas pour prendre possession de la Terre et remplir de villes la face du monde !
14:22	Je m'élèverai contre eux, dit YHWH Sabaoth, et je retrancherai à Babel le nom et le reste qu'elle a, ses descendants et sa postérité<!--Ap. 14:8, 18:2.-->, dit YHWH.
14:23	Je ferai d'elle la possession du porc-épic et un marécage, et je la balayerai avec le balai de la destruction, dit YHWH Sabaoth.

### Jugement sur le roi d'Assyrie

14:24	YHWH Sabaoth l'a juré, en disant : Certainement ce que j'ai décidé arrivera, ce que j'ai résolu s'accomplira.
14:25	Je briserai le roi d'Assyrie dans mon pays, je le foulerai aux pieds sur mes montagnes. Son joug leur sera ôté, et son fardeau sera ôté de dessus leurs épaules.
14:26	C'est là le conseil arrêté contre toute la Terre, c'est là la main étendue sur toutes les nations.
14:27	Car YHWH Sabaoth l'a arrêté en son conseil : qui l'empêchera ? Sa main est étendue : qui la détournera<!--Ec. 7:13.--> ?

### Jugement sur le pays des Philistins

14:28	L'année de la mort du roi Achaz, cette prophétie fut prononcée :
14:29	Ne te réjouis pas, toi pays des Philistins, de ce que la verge de celui qui te frappait est brisée ! Car de la racine du serpent sortira une vipère, et son fruit sera un serpent brûlant qui vole.
14:30	Alors les premiers-nés des pauvres auront de quoi se nourrir, et les pauvres reposeront en sécurité. Mais je ferai mourir de faim ta racine, et ce qui restera de toi sera tué.
14:31	Porte, hurle ! Ville, crie ! Tremble, pays tout entier des Philistins ! Car d'Aquilon vient une fumée, et il ne restera pas un homme dans ses habitations.
14:32	Et que répondra-t-on aux envoyés de cette nation ? On répondra que YHWH a fondé Sion, et que les affligés de son peuple y trouvent un refuge.

## Chapitre 15

### Jugement sur Moab

15:1	Prophétie sur Moab. La nuit même où elle est ravagée, Ar-Moab est détruite ! La nuit même où elle est saccagée, Kir-Moab est détruite !
15:2	Il monte à Bayith et à Dibon, dans les hauts lieux, pour pleurer. Moab est en lamentations sur Nebo et sur Médeba : toutes les têtes sont rasées et toutes les barbes sont coupées.
15:3	On sera couvert de sacs dans les rues ; chacun hurle, fondant en larmes sur ses toits et dans ses places<!--Jé. 48:38.-->.
15:4	Hesbon et Élealé poussent des cris, et l'on entend leur voix jusqu'à Yahats. C'est pourquoi les guerriers de Moab se lamentent, ils ont l'effroi dans l'âme.
15:5	Mon cœur crie à cause de Moab, dont les fugitifs s'enfuient jusqu'à Tsoar, comme une génisse de 3 ans. En effet, ils montent par la montée de Louhith avec des pleurs, et ils jettent des cris de détresse sur le chemin de Choronaïm.
15:6	Même les eaux de Nimrim ne sont que désolations, même le foin est déjà séché, l'herbe est consumée, et il n'y a pas de verdure.
15:7	C'est pourquoi ils emmagasinent les richesses qu'ils ont produites, et ils les transportent au delà du torrent des Saules.
15:8	Car les cris environnent les frontières de Moab, ses lamentations retentissent jusqu'à Églaïm, ses lamentations retentissent jusqu'à Beer-Élim.
15:9	Même les eaux de Dimon sont pleines de sang, car j'ajouterai un surcroît sur Dimon : des lions contre les rescapés de Moab, et le reste du pays.

## Chapitre 16

### Lamentation sur Moab

16:1	Envoyez l'agneau au souverain du pays, envoyez-le du rocher du désert, à la montagne de la fille de Sion.
16:2	Car il arrivera que les filles de Moab seront au passage de l'Arnon, comme un oiseau volant çà et là, comme une nichée chassée de son nid.
16:3	Fais venir le conseil, fais justice ! En plein midi, étends ton ombre comme celle de la nuit, cache les bannis, que les errants ne soient pas découverts !
16:4	Que les bannis de Moab puissent séjourner chez toi ! Sois pour eux un refuge contre le dévastateur ! Car celui qui use d'extorsion cessera, la dévastation finira, celui qui foule le pays sera consumé de dessus la Terre.
16:5	Le trône sera affermi par la bonté. Sur lui siégera avec fidélité, dans la tente de David, un juge cherchant le droit et prompt à faire justice<!--Mi. 4:7 ; Da. 7:14 ; Lu. 1:33 ; Ap. 11:15.-->.
16:6	Nous avons entendu l'orgueil de Moab, le très orgueilleux, sa fierté, son orgueil, son arrogance et ses vains discours.
16:7	C'est pourquoi Moab gémit sur Moab, il gémit tout entier. Vous soupirez pour les fondements de Kir-Haréseth, il n'y aura que des gens blessés à mort.
16:8	Car les campagnes de Hesbon et le vignoble de Sibma languissent. Les maîtres des nations ont foulé ses meilleurs ceps, qui s'étendaient jusqu'à Ya`azeyr, qui couraient çà et là par le désert. Ses rameaux s'étendaient et passaient au-delà de la mer.
16:9	C'est pourquoi je pleure sur la vigne de Sibma, comme sur Ya`azeyr, je vous arrose de mes larmes, ô Hesbon et Élealé ! Car l'ennemi avec des cris s'est jeté sur tes fruits d'été et sur ta moisson.
16:10	Et la joie et l'allégresse se sont retirées des vergers. On ne se réjouit plus et on ne s'égaye plus dans les vignes, le vendangeur ne foule plus dans les cuves, j'ai fait cesser la chanson de la vendange<!--Jé. 48:31-34.-->.
16:11	C'est pourquoi mes entrailles gémissent sur Moab comme une harpe, et mon intérieur sur Kir-Harès.
16:12	Et on voit Moab qui se fatigue sur les hauts lieux. Il entre dans son sanctuaire pour prier mais il ne peut rien obtenir.
16:13	Telle est la parole que YHWH a prononcée depuis longtemps sur Moab.
16:14	Et maintenant YHWH a parlé, en disant : Dans trois ans, comme les années d'un mercenaire, la gloire de Moab sera méprisée, avec toute cette grande multitude. Il en restera très peu, un petit nombre sans aucune puissance.

## Chapitre 17

### Prophétie sur la chute de Damas et de ses alliés

17:1	Prophétie sur Damas. Voici, Damas est détruite pour ne plus être une ville, et elle ne sera qu'un monceau de ruines<!--Jé. 49:23-27.-->.
17:2	Les villes d'Aroër sont abandonnées, elles sont livrées aux troupeaux qui s'y reposent, et il n'y a personne qui les effraie.
17:3	Il n'y aura plus de forteresse en Éphraïm, ni de royaume à Damas et dans le reste de la Syrie. Ils seront comme la gloire des enfants d'Israël, dit YHWH Sabaoth.
17:4	Et il arrivera en ce jour-là que la gloire de Yaacov sera affaiblie et la graisse de sa chair sera fondue.
17:5	Il en sera comme quand le moissonneur cueille les blés, et qu'il moissonne les épis avec son bras<!--Joë. 3:13 ; Mt. 13:24-30.-->, comme quand on ramasse les épis dans la vallée des géants.
17:6	Mais il en restera quelques grappillages, comme quand on secoue l'olivier, et qu'il reste 2 ou 3 olives en haut de la cime, et qu'il y en a 4 ou 5 que l'olivier a produites dans ses branches fruitières, dit YHWH, l'Elohîm d'Israël.
17:7	En ce jour-là, l'être humain regardera vers celui qui l'a fait, et ses yeux se tourneront vers le Saint d'Israël.
17:8	Et il ne regardera plus vers les autels, qui sont l'ouvrage de ses mains, et il ne regardera plus ce que ses doigts ont fabriqué, ni les ashérim<!--Voir commentaire en Ex. 34:13.-->, ni les statues du soleil.
17:9	En ce jour-là, ses villes fortes seront abandonnées à cause des enfants d'Israël, ils seront comme un bois taillis et des rameaux abandonnés, et ce sera un désert.
17:10	Parce que tu as oublié l'Elohîm de ton salut, et que tu ne t'es pas souvenue du rocher<!--Voir commentaire Es. 8:13-14.--> de ta force, à cause de cela tu as transplanté des plantes de plaisance, et tu as planté des ceps étrangers.
17:11	Le jour où tu les plantes, tu les entoures d'une clôture, et le matin tu fais fleurir tes semences. Le jour de la moisson sera faible et un tas de douleur incurable.
17:12	Malheur à la multitude de peuples nombreux ! Ils font un bruit comme le bruit des mers ! Quel tumulte de nations, qui grondent comme grondent les eaux puissantes !
17:13	Les nations font un bruit comme une tempête éclatante de grosses eaux. Mais il les menace, et elles s'enfuient ; elles seront poursuivies comme la balle des montagnes chassée par le vent, et comme une boule poussée par un tourbillon.
17:14	Au temps du soir, voici une terreur soudaine ; mais avant le matin, ils ne sont plus ! C'est là le partage de ceux qui nous dépouillent, et le lot de ceux qui nous pillent.

## Chapitre 18

### Jugement sur l'Éthiopie

18:1	Malheur à la terre qui fait ombre avec des ailes, qui est au-delà des fleuves de l'Éthiopie,
18:2	qui envoie par mer des messagers, dans des navires de papyrus voguant à la surface des eaux ! Allez messagers rapides, vers la nation robuste et vigoureuse, vers le peuple redoutable, depuis là où il est et par delà, la nation puissante qui écrase tout et dont les fleuves ravagent son pays !
18:3	Vous tous, habitants du monde, et vous qui habitez dans le pays, quand la bannière sera élevée sur les montagnes, regardez ! Lorsque le shofar sonnera, écoutez !
18:4	Car ainsi m'a parlé YHWH : Je me tiens tranquillement, et je regarde de ma demeure, par la chaleur de la lumière, et par la vapeur de la rosée, au temps de la chaude moisson.
18:5	Car avant la moisson, quand le bourgeon est à son terme et que la fleur devient un raisin qui mûrit, il coupera les sarments avec des serpes, il enlèvera et retranchera les branches.
18:6	Ils seront tous ensemble abandonnés aux oiseaux de proie des montagnes et aux bêtes de la Terre. Les oiseaux de proie seront sur eux tout le long de l'été, et toutes les bêtes de la Terre y passeront l'hiver.
18:7	En ce temps-là, un présent sera apporté à YHWH Sabaoth, par le peuple robuste et vigoureux, de la part du peuple redoutable depuis là où il est et au-delà, nation puissante et qui écrase tout, et dont le pays est ravagé par ses fleuves. Il sera apporté dans la demeure du Nom de YHWH Sabaoth, sur la montagne de Sion.

## Chapitre 19

### Chute de l'Égypte

19:1	Prophétie sur l'Égypte. Voici, YHWH est monté sur une nuée<!--Ez. 30:3 ; Na. 1:3 ; Ap. 1:7.--> rapide, il entre en Égypte. Les faux elohîm de l'Égypte tremblent devant sa face, et le cœur des Égyptiens se fond au milieu d'elle<!--Jé. 43:12.-->.
19:2	Et j'armerai l'Égyptien contre l'Égyptien, et chacun fera la guerre contre son frère, et chacun contre son ami, ville contre ville, et royaume contre royaume.
19:3	L'esprit de l'Égypte disparaîtra du milieu d'elle, et j'engloutirai son conseil. Ils consulteront les faux elohîm et les enchanteurs, ceux qui évoquent les morts et ceux qui ont un esprit de divination.
19:4	Je livrerai l'Égypte entre les mains d'un seigneur cruel. Un roi féroce dominera sur eux, dit le Seigneur, YHWH Sabaoth.
19:5	Les eaux de la mer tariront, le fleuve séchera et tarira<!--Jé. 51:36.-->.
19:6	Les rivières deviendront puantes, les fleuves d'Égypte baisseront et se dessécheront, les roseaux et les joncs se flétriront.
19:7	Ce sera la nudité le long du fleuve, à l'embouchure du fleuve ! Tout ce qui aura été semé près du fleuve se desséchera, sera emporté : il n'y aura plus rien.
19:8	Et les pêcheurs gémiront, tous ceux qui jettent l'hameçon dans le fleuve mèneront deuil, et ceux qui étendent des filets de pêche sur les eaux languiront.
19:9	Ceux qui travaillent le lin peigné et ceux qui tissent des étoffes blanches seront confus.
19:10	Les fondements du pays seront écrasés, tous les travailleurs salariés auront l'âme triste comme de l'eau stagnante.
19:11	En effet, les chefs de Tsoan ne sont que des fous, les sages d'entre les conseillers de pharaon forment un conseil stupide. Comment osez-vous dire à pharaon : Je suis fils des sages, fils des anciens rois ?
19:12	Où sont-ils maintenant ? Où sont tes sages ? Qu'ils t'annoncent, je te prie, s'ils le savent, ce que YHWH Sabaoth a décrété contre l'Égypte.
19:13	Les chefs de Tsoan sont devenus insensés, les chefs de Noph se sont trompés, les chefs des tribus ont fait errer l’Égypte.
19:14	YHWH a mêlé au milieu d’elle l’esprit qui pervertit<!--1 R. 22:18-22.-->, et ils ont fait errer l’Égypte dans toute son œuvre, comme un homme ivre qui erre dans ce qu’il a vomi.
19:15	Et l'Égypte n'aura pas d'œuvre que puisse faire la tête et la queue, la branche de palmier et le roseau.

### L'Égypte et l'Assyrie dans le royaume du Mashiah

19:16	En ce jour-là, l'Égypte sera comme des femmes : elle sera étonnée et épouvantée à cause de la main de YHWH Sabaoth, quand il élèvera la main contre elle.
19:17	La terre de Yéhouda sera la terreur de l'Égypte : quiconque fera mention d'elle, en sera épouvanté en lui-même, à cause du conseil décrété contre elle par YHWH Sabaoth.
19:18	En ce jour-là, il y aura cinq villes au pays d'Égypte, qui parleront la langue de Canaan, et qui jureront par YHWH Sabaoth ; l'une sera appelée ville de la destruction.
19:19	En ce jour-là, il y aura un autel pour YHWH au milieu du pays d'Égypte, et un monument dressé pour YHWH sur la frontière.
19:20	Et ce sera un signe et un témoignage pour YHWH Sabaoth dans le pays d'Égypte. Car ils crieront à YHWH à cause des oppresseurs, il leur enverra un sauveur, quelqu'un de grand, et il les délivrera<!--Es. 43:11.-->.
19:21	Et YHWH se fera connaître aux Égyptiens, et les Égyptiens connaîtront YHWH en ce jour-là ; ils le serviront, ils offriront des sacrifices et des offrandes, et ils feront des vœux à YHWH et les accompliront.
19:22	Ainsi YHWH frappera les Égyptiens. Il les frappera, mais il les guérira et ils retourneront à YHWH. Il les exaucera et les guérira.
19:23	En ce jour-là, il y aura une grande route de l'Égypte en Assyrie. L'Assyrie viendra en Égypte, et l'Égypte en Assyrie, et l'Égypte servira avec l'Assyrie.
19:24	En ce même temps, Israël sera, lui troisième, uni à l'Égypte et à l'Assyrie, et la bénédiction sera au milieu de la terre.
19:25	YHWH Sabaoth les bénira, en disant : Bénis soient l'Égypte mon peuple, et l'Assyrie œuvre de mes mains, et Israël mon héritage !

## Chapitre 20

### Conquête de l'Égypte et de l'Éthiopie

20:1	L'année où Tharthan, envoyé par Sargon, roi d'Assyrie, vint et combattit contre Asdod, et la prit.
20:2	En ce temps-là, YHWH parla par Yesha`yah, fils d'Amots, et lui dit : Va, délie le sac de dessus tes reins et ôte tes sandales de tes pieds. Il fit ainsi, marchant nu et déchaussé.
20:3	Puis YHWH dit : De même que mon serviteur Yesha`yah marche nu et déchaussé, ce qui sera dans trois ans un signe et un prodige contre l'Égypte et contre l'Éthiopie,
20:4	de même le roi d'Assyrie emmènera de l'Égypte et de l'Éthiopie prisonniers et captifs, les jeunes et les vieux, nus et déchaussés, ayant les hanches découvertes, ce qui sera l'opprobre de l'Égypte<!--2 S. 10:4 ; Es. 3:17 ; Jé. 13:22-26.-->.
20:5	Ils seront effrayés et honteux à cause de l'Éthiopie, leur espérance, et de l'Égypte, leur gloire.
20:6	Et les habitants de cette côte diront en ce jour-là : Voilà ce qu'est devenu le peuple en qui nous espérions, vers qui nous courions chercher du secours, afin d'être délivrés du roi d'Assyrie ! Comment pourrons-nous échapper ?

## Chapitre 21

### Annonce de la conquête de Babel

21:1	Prophétie sur le désert de la mer. Il vient du désert, de la terre redoutable, comme des tourbillons qui s'élèvent au pays du midi pour traverser.
21:2	Une vision cruelle m'a été révélée. Le traître demeure traître, celui qui saccage, saccage toujours. Monte, Élam ! Assiège, Médie ! Je fais cesser tous les soupirs.
21:3	C'est pourquoi mes reins sont remplis de douleur ; les angoisses me saisissent comme les douleurs de celle qui enfante. Je suis tourmenté à cause de ce que j'ai entendu, et j'ai été tout troublé à cause de ce que j'ai vu.
21:4	Mon cœur est agité de toutes parts, la terreur s'empare de moi. La nuit de mes plaisirs devient une nuit de crainte.
21:5	Qu'on dresse la table, que la sentinelle veille, qu'on mange, qu'on boive ! Levez-vous, chefs ! Oignez le bouclier !
21:6	Car ainsi m'a parlé Adonaï : Va, place la sentinelle, et qu'elle rapporte ce qu'elle verra<!--Ez. 33:1-19.-->.
21:7	Et elle vit un char, un couple de cavaliers, un char tiré par des ânes, un char tiré par des chameaux, qu'elle prête attention, avec grande attention.
21:8	Et elle s'écria : C'est un lion ! Adonaï, je me tiens en sentinelle toute la journée et je suis à mon poste toutes les nuits,
21:9	et voici venir le char d'un homme et un couple de cavaliers ! Alors elle parla, et dit : Elle est tombée, elle est tombée, Babel<!--Prophétie sur la chute de Babel. Voir Jé. 50-51 ; Ap. 18.-->, et toutes les images taillées de ses elohîm sont brisées par terre.
21:10	C'est ce que j'ai foulé, et le grain que j'ai battu dans mon aire. Je vous ai annoncé ce que j'ai entendu de YHWH Sabaoth, d'Elohîm d'Israël.

### Prophétie sur Doumah

21:11	Prophétie sur Doumah. On me crie de Séir : Ô sentinelle ! Qu'en est-il de la nuit ? Ô sentinelle ! Qu'en est-il de la nuit ?
21:12	La sentinelle répond : Le matin vient et la nuit aussi. Si vous voulez interroger, interrogez ! Convertissez-vous et venez !

### Jugement sur l'Arabie

21:13	Prophétie contre l'Arabie. Vous passerez la nuit dans la forêt, en Arabie, caravanes de Dedan !
21:14	Les habitants du pays de Théma portent de l'eau à ceux qui ont soif ! Ils viennent au-devant du fugitif avec du pain pour lui.
21:15	Car ils fuient devant les épées, devant l'épée dégainée, devant l'arc tendu, devant l'acharnement de la bataille.
21:16	Car ainsi m'a parlé Adonaï : Encore une année, comme les années d'un mercenaire, et toute la gloire de Kédar prendra fin.
21:17	Et le nombre des vaillants archers, fils de Kédar, qui seront restés diminuera, car YHWH, l'Elohîm d'Israël, a parlé.

## Chapitre 22

### Malédiction sur la vallée des visions, Yeroushalaim (Jérusalem)

22:1	Prophétie sur la vallée des visions. Qu'as-tu donc pour monter avec tout ton peuple sur les toits ?
22:2	Toi ville bruyante, pleine de tumulte, ville joyeuse ! Tes blessés mortellement ne sont pas blessés mortellement par l'épée, ils n'ont pas été tués à la guerre.
22:3	Tous tes chefs prennent la fuite ensemble, ils sont faits prisonniers par les archers. Tes habitants, tous ensemble, sont faits prisonniers tandis qu'ils s'enfuient au loin.
22:4	C'est pourquoi je dis : Détournez de moi vos regards, que je pleure amèrement. Ne vous empressez pas pour me consoler du désastre de la fille de mon peuple.
22:5	Car c'est le jour de trouble, d'oppression et de confusion<!--La. 1:5, 2:2.-->, envoyé par Adonaï YHWH Sabaoth, dans la vallée des visions. Il démolit la muraille et les cris retentissent jusqu'à la montagne.
22:6	Même Élam prend son carquois, il y a des gens montés sur des chars et des cavaliers, Kir découvre le bouclier.
22:7	Et tes plus belles vallées sont remplies de chars, et les cavaliers se rangent tous en bataille à tes portes.
22:8	La couverture de Yéhouda est enlevée. En ce jour-là, tu as regardé vers les armes de la maison de la forêt.
22:9	Vous voyez que les brèches de la cité de David sont nombreuses et vous faites provision d'eau dans le réservoir inférieur.
22:10	Vous faites le dénombrement des maisons de Yeroushalaim, et vous démolissez les maisons pour fortifier la muraille.
22:11	Et vous faites aussi un réservoir d'eau entre les deux murailles, pour les eaux de l'ancien étang. Mais vous ne regardez pas à celui qui a fait ces choses, qui les a formées il y a longtemps.
22:12	Adonaï YHWH Sabaoth vous appelle, en ce jour-là, aux pleurs et aux gémissements, à la calvitie et à ceindre le sac<!--Ez. 7:18 ; Joë. 1:13.-->.
22:13	Et voici il y a de la joie et de l'allégresse ! On tue des bœufs et l'on abat des moutons, on mange la viande et l'on boit du vin : Mangeons et buvons, car demain nous mourrons<!--Es. 56:12 ; 1 Co. 15:32.--> !
22:14	YHWH Sabaoth s'est découvert à mes oreilles : Non, la propitiation pour cette iniquité ne sera jamais faite en votre faveur jusqu'à ce que vous mouriez, a dit Adonaï YHWH Sabaoth.

### Élyakim succède à Shebna

22:15	Ainsi parle Adonaï YHWH Sabaoth : Va, entre chez ce trésorier, chez Shebna, gouverneur du palais.
22:16	Qu'as-tu à faire ici et qu'as-tu ici qui t'appartienne pour que tu te tailles ici un sépulcre ? Il taille un sépulcre en hauteur, il se taille une demeure dans le rocher.
22:17	Voici, ô homme ! YHWH te chassera au loin d'un jet vigoureux, il t'enveloppera, il t'enveloppera.
22:18	Il te fera rouler, rouler, rouler comme une balle sur une terre aux mains larges. Là tu mourras, là seront les chars de ta gloire, ô toi qui es la honte de la maison de ton Seigneur !
22:19	Je te jetterai hors de ton poste, et on t'arrachera de ton service.
22:20	Et il arrivera en ce jour-là que j'appellerai mon serviteur Élyakim, fils de Chilqiyah.
22:21	Je le revêtirai de ta tunique, je le ceindrai de ta ceinture, et je remettrai ton autorité entre ses mains, il sera un père pour les habitants de Yeroushalaim et pour la maison de Yéhouda.
22:22	Et je mettrai la clé de la maison de David sur son épaule : S'il ouvre, personne ne fermera, s'il ferme, personne n'ouvrira<!--La clé de David est le symbole de l'autorité du Mashiah (Es. 9:5 ; Mt. 28:18 ; Ap. 3:7-8).-->.
22:23	Je l'enfoncerai comme un clou dans un lieu sûr, et il sera un trône de gloire pour la maison de son père.
22:24	On suspendra sur lui toute la gloire de la maison de son père, les descendants et les rejetons, tous les ustensiles de petite taille, les ustensiles, les bassins, depuis les ustensiles jusqu'aux outres.
22:25	En ce jour-là, dit YHWH Sabaoth, le clou enfoncé dans un lieu sûr sera enlevé, il sera abattu et tombera, et le fardeau qui était sur lui sera retranché, car YHWH a parlé.

## Chapitre 23

### Effondrement de Tyr

23:1	Prophétie sur Tyr. Hurlez, navires de Tarsis ! Car elle est détruite, il n'y a plus de maisons, on n'y entre plus ! Ceci leur a été révélé du pays de Kittim.
23:2	Habitants de l'île, taisez-vous ! Toi qui étais remplie de marchands de Sidon, et de ceux qui traversaient la mer !
23:3	À travers les grandes eaux, les grains de Shichor, la moisson du Nil était pour elle son revenu ; elle était le marché des nations<!--Ez. 27.-->.
23:4	Sois honteuse, ô Sidon ! Car la mer, la forteresse de la mer, a parlé, en disant : Je n'ai pas eu de douleurs, je n'ai pas enfanté, je n'ai pas nourri de jeunes hommes ni élevé aucune vierge.
23:5	Tout comme à la nouvelle concernant l'Égypte, on sera de même saisi de douleurs à la nouvelle sur Tyr.
23:6	Passez à Tarsis, lamentez-vous, habitants de l'île !
23:7	N'est-ce pas ici votre ville joyeuse ? Elle avait une origine antique et ses propres pieds la mènent séjourner dans un pays étranger.
23:8	Qui a pris ce conseil contre Tyr, celle qui couronnait les siens, dont les marchands étaient des princes, et dont les trafiquants étaient les plus honorables de la Terre<!--Ap. 18:9-18.--> ?
23:9	YHWH Sabaoth a pris ce conseil, pour flétrir l'orgueil de toute la noblesse, et pour avilir tous les honorables de la Terre.
23:10	Traverse ton pays comme une rivière, ô fille de Tarsis ! Il n'y a plus de ceinture.
23:11	Il a étendu sa main sur la mer, il a fait trembler les royaumes. YHWH a ordonné la destruction des forteresses de Canaan.
23:12	Il a dit : Tu ne te livreras plus à la joie, vierge opprimée, fille de Sidon ! Lève-toi, passe au pays de Kittim ! Même là, il n'y aura pas de repos pour toi.
23:13	Voici le pays des Chaldéens, ce peuple n'existait pas : Assour<!--Assour : le second fils de Shem (Ge. 10:22). L'ancêtre des Assyriens.--> l'a fondé pour les gens du désert ; on a dressé ses forteresses, on a élevé ses palais, et il l'a mis en ruine.
23:14	Hurlez, navires de Tarsis ! Car votre force est détruite !
23:15	Et il arrivera en ce jour-là que Tyr tombera dans l'oubli durant 70 ans, selon les jours d'un roi. Mais au bout de 70 ans<!--Jé. 25:11-12.-->, on chantera une chanson à Tyr comme à une femme prostituée :
23:16	Prends la harpe, fais-le tour de la ville, ô prostituée qu'on oublie ! Joue bien de ton instrument, multiplie tes chants afin qu'on se souvienne de toi !
23:17	Et il arrivera à la fin de 70 ans que YHWH visitera Tyr, mais elle retournera à son salaire de prostituée, et elle se prostituera avec tous les royaumes de la Terre, sur la face de la Terre.
23:18	Mais son gain et son salaire de prostituée seront consacrés à YHWH. Ils ne seront ni emmagasinés ni conservés, car son gain sera pour ceux qui habitent en présence de YHWH pour qu'ils mangent à satiété et pour la couverture splendide.

## Chapitre 24

### Désastre après l'invasion babylonienne 

24:1	Voici, YHWH va rendre la Terre vide et la dévaster, il en renversera la face et dispersera ses habitants<!--Ge. 11:1-8.-->.
24:2	Et il en est du prêtre comme du peuple, du maître comme de son serviteur, de la maîtresse comme de sa servante, du vendeur comme de l'acheteur, de celui qui prête comme de celui qui emprunte, du créancier comme du débiteur.
24:3	La Terre est dévastée, elle est dévastée et pillée, pillée, car YHWH a prononcé cet arrêt.
24:4	La Terre pleure, elle se fane. Le monde languit et se fane. Ceux qui sont élevés parmi le peuple de la Terre languissent.
24:5	La Terre a été profanée sous ceux qui l'habitent parce qu'ils ont transgressé la torah, changé les ordonnances et rompu l'alliance éternelle<!--Da. 7:25.-->.
24:6	C'est pourquoi la malédiction dévore la Terre, et ses habitants portent la peine de leurs crimes. C'est pourquoi les habitants de la Terre sont brûlés et il n'en reste qu'un petit nombre.
24:7	Le vin excellent pleure, la vigne languit, et tous ceux qui avaient le cœur joyeux soupirent.
24:8	La joie des tambours a cessé, le bruit de ceux qui s'égayent a pris fin, la joie de la harpe a cessé.
24:9	On ne boit plus de vin en chantant, les boissons fortes sont amères à ceux qui les boivent.
24:10	La ville confuse est en ruine. Toutes les maisons sont fermées, on n'y entre plus.
24:11	On crie dans les rues parce que le vin manque. Toute la joie est tournée en obscurité, l'allégresse de la Terre s'en est allée.
24:12	La désolation est restée dans la ville et la porte est frappée d'une ruine éclatante.
24:13	Car il en sera sur Terre parmi les peuples, comme quand on secoue l'olivier, comme quand on grappille après la vendange.

### Un reste de réchappés célèbre YHWH

24:14	Ils élèvent leur voix, ils se réjouissent avec chant de triomphe. Du côté de la mer, ils célèbrent la majesté de YHWH.
24:15	C'est pourquoi glorifiez YHWH dans les vallées, le Nom de YHWH, l'Elohîm d'Israël, dans les îles de la mer !
24:16	Depuis les extrémités de la Terre nous entendons des chants : Gloire au juste ! Mais moi je dis : Maigreur sur moi ! Maigreur sur moi ! Malheur à moi ! Les traîtres trahissent, les traîtres ont agi avec tromperie en trahissant.

### Manifestation des jugements de YHWH

24:17	La frayeur, la fosse, et le piège sont sur toi, habitant de la Terre !
24:18	Et il arrivera que celui qui fuit au bruit de la peur tombera dans la fosse, et celui qui remonte du milieu de la fosse sera pris dans le piège, car les écluses d'en haut s'ouvrent et les fondements de la Terre tremblent.
24:19	La Terre se brise, elle se brise ! La Terre se rompt, elle se rompt ! La Terre chancelle, elle chancelle !
24:20	La Terre tremble, elle tremble comme un homme ivre, elle vacille comme une hutte. Sa transgression pèse sur elle, elle tombe et ne se relève plus.
24:21	Et il arrivera en ce jour-là, que YHWH punira dans le lieu élevé l'armée d'en haut, et sur la Terre les rois de la Terre.
24:22	Ils seront rassemblés dans une fosse au rassemblement des prisonniers, ils seront enfermés dans une prison et, après un grand nombre de jours, ils seront visités.
24:23	La lune rougira et le soleil sera honteux quand YHWH Sabaoth régnera sur la montagne de Sion et à Yeroushalaim, resplendissant de gloire en présence de ses anciens<!--Mt. 24:29-30 ; 2 Pi. 3:10-12 ; Ap. 6:12.-->.

## Chapitre 25

### Le Royaume de YHWH

25:1	Ô YHWH, tu es mon Elohîm, je t'exalterai, je célébrerai ton Nom, car tu as fait des choses merveilleuses. Tes conseils conçus d'avance se sont fidèlement accomplis.
25:2	Car tu as fait de la ville un monceau de pierres, et de la cité forte une ruine. Le palais des étrangers qui était dans la ville ne sera jamais rebâti.
25:3	C'est pourquoi des peuples puissants te glorifient, la ville des nations redoutables te craint.
25:4	Parce que tu as été la force du faible, la force du misérable dans sa détresse, le refuge contre la tempête, l'ombrage contre la chaleur. Car le souffle des tyrans est comme la tempête qui abat une muraille.
25:5	Comme la chaleur sur un sol desséché, tu rabaisses le vacarme des étrangers. Comme la chaleur sous l'ombre d'un nuage, tu te débarrasses de cris des impitoyables.
25:6	Et YHWH Sabaoth prépare pour tous les peuples sur cette montagne un banquet de choses grasses, un banquet de vins vieux, de choses grasses et moelleuses, et de vins vieux bien purifiés.
25:7	Et il détruira sur cette montagne la couverture qui enveloppe la face de tous les peuples, et le voile qui est étendu sur toutes les nations.
25:8	Il engloutira la mort par sa victoire. Adonaï YHWH essuiera les larmes de tous les visages, et il ôtera l'opprobre de son peuple de toute la Terre, car YHWH a parlé.
25:9	Et l'on dira en ce jour-là : Voici notre Elohîm ! Celui en qui nous avons espéré et qui nous sauve : c'est YHWH, en qui nous avons espéré. Soyons dans l'allégresse, et réjouissons-nous de son salut !
25:10	Car la main de YHWH reposera sur cette montagne et Moab sera foulé aux pieds sous lui, comme on foule la paille pour en faire du fumier.
25:11	Il étendra ses mains au milieu d'eux, comme le nageur étend ses mains pour nager. Mais il abaissera son orgueil, ainsi que l'artifice de ses mains.
25:12	Il abattra, il renversera les fortifications inaccessibles de tes murs, il les jettera à terre jusque dans la poussière.

## Chapitre 26

### Adoration à YHWH

26:1	En ce jour-là, ce cantique sera chanté dans le pays de Yéhouda : Nous avons une ville forte. Il y met le salut<!--Le mot salut vient du mot « Yeshuw'ah ». Cette même racine a donné le prénom Yéhoshoua (Jésus ou Josué) qui signifie YHWH sauve. Yéhoshoua (Jésus) est notre muraille et notre rempart. Dans Ex. 15:2, Moshè (Moïse) identifie YHWH à « Yeshuw'ah » c'est-à-dire à Yéhoshoua (Jésus). Dans 1 Ch. 16:23, il est dit que « Yeshuw'ah » doit être annoncé tous les jours. Dans Ps. 62:2, il est présenté comme Elohîm et le Rocher. Dans Es. 12:2, il est l'Elohîm qui sauve. Yaacov et David avaient mis en lui leur espoir (Ge. 49:18 ; Ps. 119:166). Dans Es. 49:6, il est dit que le salut (« Yeshuw'ah » ou Yéhoshoua) doit être annoncé aux extrémités de la terre, et cela est répété et confirmé en Mt. 28:18-20. Es. 56:1 nous apprend que celui qui vient s'appelle « Yeshuw'ah ». Es. 59:17 le présente comme notre casque, ce qui fait écho au casque du salut en Ep. 6:17. Les murs de la Nouvelle Yeroushalaim (Jérusalem) portent son Nom (Es. 60:18). Ha. 3:8 nous dit que « Yeshuw'ah » montera sur ses chevaux, corroborant le récit de son retour en gloire dans Ap. 19:11-20. « Yeshuw'ah » est notre flambeau selon Es. 62:1 et Ap. 21:23.--> pour muraille et pour rempart.
26:2	Ouvrez les portes, et la nation juste, celle qui garde la fidélité, y entrera.
26:3	Tu gardes dans la paix, dans la paix<!--Voir commentaire en Ge. 2:16-17.-->, celui dont les desseins s'appuient sur toi, parce qu'il se confie en toi<!--Es. 57:19 ; Ph. 4:6-7.-->.
26:4	Confiez-vous en YHWH à perpétuité, car Yah, YHWH est le Rocher<!--Voir commentaire en Es. 8:13-14.--> éternel.
26:5	Car il a abattu ceux qui habitaient les hauteurs, il a abaissé la ville inaccessible, il l'a abaissée jusqu'à terre, il lui a fait toucher la poussière.
26:6	Le pied la piétinera, les pieds des pauvres, sous les pas des faibles.
26:7	Le sentier du juste, c'est la droiture. Toi qui es juste, tu aplanis la route du juste.
26:8	Aussi t'avons-nous attendu, ô YHWH, dans le sentier de tes jugements ! Ton Nom et ton souvenir sont le désir de notre âme.
26:9	Pendant la nuit, mon âme te désire, et dès le point du jour, mon esprit qui est en moi te cherche, car, lorsque tes jugements s'exercent sur la Terre, les habitants du monde apprennent la justice.
26:10	Si l'on fait grâce au méchant, il n'apprend pas la justice, mais il agira méchamment dans le pays de la droiture et il ne regardera pas à la majesté de YHWH.
26:11	YHWH, ta main est levée et ils ne le voient pas ! Ils verront ta jalousie pour le peuple et seront honteux, dévorés par le feu destiné à tes ennemis.
26:12	YHWH, tu mets en nous la paix, car toutes nos œuvres, c'est toi qui les accomplis pour nous.
26:13	YHWH, notre Elohîm, d'autres seigneurs, à part toi, ont dominé sur nous ; c'est par toi seul que nous pouvons faire mention de ton Nom.
26:14	Ils sont morts, ils ne revivront plus, ils ne sont plus que des ombres<!--Vient d'un mot hébreu qui signifie « fantômes de morts, ombres, revenants » ou encore « esprits ».-->, ils ne se relèveront pas car tu les as châtiés et exterminés, et tu as fait périr tout souvenir d'eux<!--Ec. 9:5.-->.
26:15	Tu as augmenté la nation, YHWH ! Tu as augmenté la nation, tu t'es glorifié, tu as reculé toutes les limites du pays.

### Un reste épargné de la colère de YHWH

26:16	YHWH, quand ils étaient dans la détresse, ils t'ont cherché, ils ont répandu leurs chuchotements quand ton châtiment était sur eux.
26:17	Comme une femme enceinte qui est près d'accoucher se tord et crie dans ses douleurs, ainsi avons-nous été devant ta face ô YHWH !
26:18	Nous avons conçu, nous avons été dans les douleurs de l'accouchement et, quand nous accouchons, ce n'est que du vent. Nous n'avons pas accompli le salut de la Terre, et les habitants du monde ne sont pas tombés.
26:19	Tes morts revivront ! Mes cadavres se relèveront ! Réveillez-vous et réjouissez-vous avec des chants de triomphe, vous, habitants de la poussière ! Car ta rosée est comme la rosée de lumière, et la Terre jettera dehors les ombres<!--Os. 13:14 ; Da. 12:2 ; 1 Co. 15:52.-->.
26:20	Va, mon peuple, entre dans tes chambres et ferme ta porte derrière toi<!--Mt. 6:6.--> ! Cache-toi pour un petit moment, jusqu'à ce que la colère soit passée.
26:21	Car voici, YHWH sort de son lieu pour punir l'iniquité des habitants de la Terre<!--Voir Mt. 25:31-46.-->. Alors la Terre découvrira son sang et ne couvrira plus ses tués !

## Chapitre 27

### Israël rétabli

27:1	En ce jour-là, YHWH punira de sa dure, grande et puissante épée le Léviathan<!--Ps. 104:26 ; Job 40:20.-->, le serpent fuyard, le Léviathan, le serpent tortueux, et il tuera le monstre marin qui est dans la mer.
27:2	En ce jour-là, chantez sur la vigne agréable<!--Yesha`yah annonce ici le rétablissement d'Israël. Voir également Ro. 11:1-24.-->.
27:3	C'est moi YHWH qui la garde, je l'arrose à chaque instant, je la garde nuit et jour, afin que personne ne lui fasse du mal.
27:4	Il n'y a pas de fureur en moi. Qu'on me donne des ronces, des épines pour les combattre ! Je marcherai contre elles, je les brûlerai toutes ensemble,
27:5	à moins qu'on n'ait recours à ma protection, qu'on fasse la paix avec moi, qu'on fasse la paix avec moi.
27:6	Dans l'avenir Yaacov prendra racine, Israël fleurira et s'épanouira et remplira la surface du monde de ses fruits.
27:7	L'a-t-il frappé comme il a frappé ceux qui le frappaient ? L'a-t-il tué comme il a tué ceux qui le tuaient ?
27:8	C'est avec mesure que tu l'as combattu en le rejetant, lorsqu'il fut emporté par ton vent sévère, un jour du vent d'orient.
27:9	C'est pourquoi la propitiation de l'iniquité de Yaacov sera faite par ce moyen, et le fruit de tout cela sera l'éloignement de son péché : quand il aura transformé toutes les pierres des autels comme des pierres de chaux réduites en poussière, lorsque les ashérim et les statues consacrées au soleil ne seront plus debout.
27:10	Car la ville fortifiée est solitaire, la demeure agréable est abandonnée et délaissée comme un désert. Le veau en a fait sa pâture, il s'y couche et broute les branches.
27:11	Quand ses rameaux se dessèchent, on les brise et des femmes viennent y mettre le feu. Car c'est un peuple sans discernement<!--De. 32:28 ; Es. 1:3.-->, c'est pourquoi celui qui l'a fait n'a pas eu pitié de lui et celui qui l'a formé ne lui a pas fait grâce.
27:12	Il arrivera en ce jour-là que YHWH secouera, depuis le cours du fleuve jusqu'au torrent d'Égypte, mais vous serez glanés un à un, ô enfants d'Israël !
27:13	Et il arrivera en ce jour-là qu'on sonnera du grand shofar, et ceux qui étaient exilés au pays d'Assyrie, et ceux qui avaient été chassés au pays d'Égypte, reviendront et se prosterneront devant YHWH, sur la sainte montagne, à Yeroushalaim.

## Chapitre 28

### Malheur et captivité d'Éphraïm en Assyrie

28:1	Malheur à la couronne orgueilleuse des ivrognes d'Éphraïm, dont la glorieuse beauté n'est qu'une fleur fanée sur le sommet de la vallée fertile, à ceux qui sont frappés par le vin !
28:2	Voici le fort et le puissant d'Adonaï, semblable à un orage de grêle, à une tempête de destruction, à un orage d'eaux puissantes qui débordent : il le repose à terre à la main.
28:3	Elles seront foulées aux pieds, la couronne orgueilleuse et les ivrognes d'Éphraïm.
28:4	Et la glorieuse beauté qui est sur le sommet de la vallée fertile, ne sera qu'une fleur fanée : ils seront comme les fruits précoces avant l'été, aussitôt que celui qui regarde les voit, à peine sont-ils dans sa main qu'il les dévore.
28:5	En ce jour-là, YHWH Sabaoth sera la couronne de beauté et le diadème de gloire pour le reste de son peuple,
28:6	et l'Esprit de jugement pour celui qui siège au jugement, et la force de ceux qui dans le combat repousseront l'ennemi jusqu'à la porte.
28:7	Mais eux aussi se sont égarés à cause du vin, et ils chancellent à cause des boissons fortes. Le prêtre et le prophète se sont égarés à cause des boissons fortes. Ils sont engloutis par le vin, ils chancellent à cause des boissons fortes, ils s'égarent dans leur vision, ils vacillent dans le jugement.
28:8	Car toutes leurs tables sont couvertes de vomissements et d'ordures, si bien qu'il n'y a plus de place !
28:9	À qui enseigne-t-on la connaissance ? À qui fait-on comprendre l'enseignement ? Est-ce à ceux qu'on vient de sevrer du lait et de retirer de la mamelle ?
28:10	Car c'est commandement sur commandement, commandement sur commandement, règle sur règle, règle sur règle, un peu ici, un peu là<!--Hé. 5:12.-->.
28:11	C'est pourquoi, il parlera à ce peuple par des lèvres qui balbutient et par une langue étrangère<!--Voir Ac. 2:1-47.-->.
28:12	Il leur disait : Voici le repos ! Donnez du repos à celui qui est fatigué ! Voici le soulagement ! Mais ils n'ont pas voulu écouter.
28:13	Ainsi la parole de YHWH sera pour eux commandement sur commandement, commandement sur commandement, règle sur règle, règle sur règle, un peu ici, un peu là, afin qu’en marchant ils tombent à la renverse et se brisent, afin qu'ils tombent dans le piège et qu’ils soient capturés.

### YHWH rompt le pacte du shéol par une pierre angulaire

28:14	C'est pourquoi écoutez la parole de YHWH, vous hommes moqueurs, qui dominez sur ce peuple qui est à Yeroushalaim !
28:15	Car vous dites : Nous avons fait un pacte avec la mort et avec le shéol, nous avons réalisé une vision : quand le fléau qui déborde passera, il ne nous atteindra pas, car nous avons le mensonge pour refuge et nous nous sommes cachés sous la fausseté.
28:16	C'est pourquoi ainsi parle Adonaï YHWH : Voici, je mettrai pour fondement en Sion une pierre<!--Voir commentaire en Es. 8:13-16.-->, une pierre éprouvée, la pierre angulaire la plus précieuse, pour être un fondement solide. Celui qui croira ne se hâtera pas.
28:17	Et j'établirai la droiture comme une règle et la justice comme un fil à plomb. La grêle détruira le refuge du mensonge et les eaux submergeront votre abri.
28:18	Et votre pacte avec la mort sera détruit, votre vision avec le shéol ne tiendra pas. Quand le fléau qui déborde passera, vous serez piétinés par lui.
28:19	Chaque fois qu'il passera, il vous emportera, car matin après matin il passera, pendant le jour et pendant la nuit. Ce sera la terreur que d'en comprendre le bruit.
28:20	Car le lit sera trop court, et on ne pourra pas s'y étendre, et la couverture trop étroite pour s'en envelopper.
28:21	Car YHWH se lèvera comme à la montagne de Peratsim, et il sera ému comme dans la vallée de Gabaon, pour faire son œuvre, son œuvre étrange, et pour faire son service, son service étranger.
28:22	Maintenant donc, ne vous moquez plus, de peur que vos liens ne soient renforcés ! Car j'ai entendu que la destruction est décidée par Adonaï YHWH Sabaoth, contre toute la Terre.
28:23	Prêtez l'oreille et écoutez ma voix ! Soyez attentifs et écoutez mon discours !
28:24	Celui qui laboure pour semer, laboure-t-il tous les jours ? Ne casse-t-il pas et ne rompt-il pas les mottes de sa terre ?
28:25	Quand il en a aplani la surface, n'y répand-il pas le cumin noir<!--Plant à petites graines noires, utilisées comme condiment.--> et n'y jette-t-il pas le cumin ? Ne met-il pas le blé par rangées, l'orge à une place marquée et l'épeautre<!--L'épeautre est une espèce de blé.--> sur les bords ?
28:26	Parce que son Elohîm l'a instruit, et lui a enseigné ce qu'il faut faire.
28:27	Car on ne foule pas le cumin noir avec la herse<!--La herse est un instrument agricole permettant de travailler la terre en surface.--> et on ne tourne pas la roue du chariot sur le cumin, mais on bat le cumin noir avec la verge et le cumin avec le bâton.
28:28	On doit écraser le blé avec lequel on fait le pain, car le laboureur ne le bat pas indéfiniment, et quoiqu'il le foule avec la roue de son chariot, néanmoins il ne l'écrasera pas avec ses chevaux.
28:29	Cela aussi vient de YHWH Sabaoth qui est merveilleux en conseil et grand en sagesse.

## Chapitre 29

### Avertissement d'un châtiment imminent

29:1	Malheur à Ariel<!--Ariel : « Lion de El », nom appliqué à Yeroushalaim (Jérusalem).-->, à Ariel, la ville dont David fit sa demeure ! Ajoutez année à année, qu'on tue des victimes pour les fêtes.
29:2	Mais je mettrai Ariel à l'étroit. Il n'y aura que tristesse et deuil, et elle sera pour moi comme Ariel.
29:3	Car je camperai en cercle contre toi, et je t'assiégerai avec des tours, et je dresserai contre toi des retranchements.
29:4	Tu seras abaissée, tu parleras depuis la terre, ta parole sortira étouffée par la poussière. Ta voix sortira de terre comme l'esprit d'un mort, et ta parole sera comme un murmure sortant de la poussière.
29:5	La multitude de tes étrangers sera comme une fine poussière, la multitude des guerriers sera comme la balle qui passe, et cela sera pour un petit moment.
29:6	Elle sera visitée par YHWH Sabaoth avec des tonnerres, des tremblements de terre, et un grand bruit<!--Za. 14:13-14 ; Ap. 16:18-19.-->, avec la tempête, le tourbillon et avec la flamme d'un feu dévorant.
29:7	Et comme il en est d'un rêve, d'une vision de la nuit, ainsi en sera-t-il de la multitude des nations qui combattront Ariel, de tous ceux qui l'attaqueront, elle et sa forteresse, et qui la serreront de près.
29:8	Il en sera comme d'un homme qui a faim rêve qu'il mange, mais quand il se réveille son âme est vide, ou comme un homme qui a soif rêve qu'il boit, mais quand il se réveille, il est épuisé et son âme est altérée. Il en sera de même pour la multitude de toutes les nations qui combattront contre la montagne de Sion.

### YHWH donne les raisons du châtiment

29:9	Arrêtez-vous et soyez étonnés ! Bouchez les yeux et soyez aveugles ! Ils sont ivres, mais non de vin ; ils chancellent, mais non pas à cause des boissons fortes.
29:10	Car YHWH a répandu sur vous un esprit de profond sommeil<!--Ro. 11:8.-->. Il a fermé vos yeux, les prophètes, il a couvert vos têtes, les principaux voyants.
29:11	Et toute vision est pour vous comme les paroles d'un livre cacheté que l'on donne à un homme de lettres en lui disant : Nous te prions, lis donc cela ! Et qui répond : Je ne le puis, car il est cacheté ;
29:12	puis si on le donne à quelqu'un qui n'est pas un homme de lettres, en lui disant : Nous te prions, lis donc cela ! Et qui répond : Je ne sais pas lire.
29:13	C'est pourquoi Adonaï dit : Parce que ce peuple s'approche de moi de sa bouche et qu'il m'honore de ses lèvres, mais que son cœur est éloigné de moi et que la crainte qu'il a de moi n'est qu'un commandement enseigné par des humains<!--Mt. 15:8-9 ; Mc. 7:6-7.-->,
29:14	à cause de cela, voici, je continuerai à faire à l'égard de ce peuple des merveilles, des merveilles et des miracles, et la sagesse de ses sages périra et le discernement de ceux qui discernent se cachera<!--1 Co. 1:19.-->.
29:15	Malheur à ceux qui cachent profondément leurs desseins pour les dérober à YHWH, et dont les œuvres se font dans les ténèbres, et qui disent : Qui nous voit, et qui nous connaît<!--Es. 47:10 ; Ez. 8:12 ; Ps. 10:11, 94:7.--> ?
29:16	Quelle perversité ! Le potier doit-il être considéré comme l'argile, pour que l'œuvre dise de celui qui l'a faite : Il ne m'a pas faite, pour que le pot dise de son potier : Il n'y connaît rien<!--Ps. 100:3.--> ?

### YHWH rachète Yaacov

29:17	Le Liban ne sera-t-il pas encore dans très peu de temps changé en un Carmel ? Et Carmel ne sera-t-il pas considéré comme une forêt ?
29:18	En ce jour-là, les sourds entendront les paroles du livre, et les yeux des aveugles étant délivrés de l'obscurité et des ténèbres, verront<!--Mt. 11:5 ; Lu. 7:22.-->.
29:19	Les humbles auront joie sur joie en YHWH et les pauvres d'entre les humains se réjouiront dans le Saint d'Israël<!--Mt. 5:3-11.-->.
29:20	Car ce sera la fin de l'oppresseur, le moqueur sera consumé, et tous ceux qui veillaient pour commettre l'iniquité seront retranchés<!--Ap. 20:10.-->,
29:21	ceux qui font pécher les gens par la parole, qui tendent des pièges à celui qui les reprend à la porte, et qui font tomber le juste dans la confusion.
29:22	C'est pourquoi ainsi parle YHWH, lui qui a racheté Abraham, à la maison de Yaacov : Yaacov ne sera plus honteux, et sa face ne pâlira plus.
29:23	Car quand il verra ses fils, ouvrage de mes mains, au milieu de lui, ils sanctifieront mon Nom, ils sanctifieront le Saint de Yaacov, et ils craindront l'Elohîm d'Israël.
29:24	Et ceux dont l'esprit s'était égaré auront du discernement, et ceux qui murmuraient apprendront l'enseignement.

## Chapitre 30

### Mise en garde contre les alliances étrangères

30:1	Malheur aux enfants rebelles qui prennent des conseils qui ne viennent pas de moi, déclare YHWH, et qui se forgent<!--Qui versent des libations avec sacrifice d'alliance.--> des idoles de métal où mon Esprit n'est pas, afin d'ajouter péché sur péché.
30:2	Qui sans avoir interrogé ma bouche, se mettent en route pour descendre en Égypte, afin de se réfugier sous la protection de pharaon et se retirer sous l'ombre de l'Égypte<!--Jé. 42:19.-->.
30:3	Car la protection de pharaon sera pour vous une honte, et le refuge sous l'ombre de l'Égypte votre confusion.
30:4	Car ses princes sont à Tsoan, et ses messagers ont atteint Hanès.
30:5	Tous seront couverts de honte par un peuple qui ne leur apportera aucun profit, ils n'en recevront aucun secours ni aucun profit, il sera leur honte et leur opprobre.
30:6	Les bêtes sont chargées pour aller au midi, ils portent leurs richesses sur le dos des ânons, et leurs trésors sur la bosse des chameaux, vers le peuple qui ne leur profitera pas dans le pays de détresse et d'angoisse, d'où viennent la lionne et le lion, la vipère et le serpent volant.
30:7	Car le secours de l'Égypte n'est que vanité et néant, c'est pourquoi je crie ceci : Leur force est de se tenir tranquille.
30:8	Va maintenant, écris-le sur une tablette avec eux, grave-le dans un livre, afin que ce soit, pour les jours à venir, un témoignage pour toujours et à jamais.
30:9	Car c'est un peuple rebelle, des enfants menteurs, des enfants qui ne veulent pas écouter la torah de YHWH<!--No. 20:3-5 ; De. 9:7 ; Ac. 7:51.-->,
30:10	qui disent aux voyants : Ne voyez pas ! Et aux prophètes : Ne nous prophétisez pas des vérités, mais dites-nous des choses agréables, prophétisez des illusions<!--2 Ti. 4:3-4 ; Mi. 2:6.--> !
30:11	Retirez-vous du chemin, détournez-vous du sentier, éloignez de notre présence le Saint d'Israël<!--Jn. 14:6.-->.
30:12	C'est pourquoi ainsi parle le Saint d'Israël : Parce que vous rejetez cette parole et que vous vous confiez dans l'oppression et dans les détours, et que vous vous êtes appuyés sur ces choses,
30:13	à cause de cela, cette iniquité sera pour vous comme la fente d'une muraille qui va tomber, un renflement dans un mur élevé, dont la ruine vient soudainement, et en un instant.
30:14	Il la brise donc comme on brise un vase de terre que l'on n'épargne pas, et de ses pièces, il ne se trouve pas un tesson pour prendre du feu au foyer ou pour puiser de l'eau à la citerne.

### La confiance en YHWH, la vraie force

30:15	Car ainsi a parlé Adonaï YHWH, le Saint d'Israël : C'est dans le retour à moi et dans le repos que vous serez sauvés ; c'est dans la tranquillité et dans la confiance que sera votre force. Mais vous ne l'avez pas voulu !
30:16	Et vous avez dit : Non ! Mais nous nous enfuirons à cheval ! À cause de cela vous vous enfuirez. Et vous avez dit : Nous monterons sur des chevaux rapides ! À cause de cela ceux qui vous poursuivront seront rapides.
30:17	Vous serez un millier face à la menace d'un seul, et face à la menace de cinq vous prendrez la fuite, jusqu'à ce que vous ne soyez plus qu'un reste semblable à un mât de drapeau au sommet d'une montagne, comme une bannière sur la colline.
30:18	Cependant YHWH attend pour vous faire grâce, et c'est pourquoi il se lèvera pour vous faire miséricorde. Car YHWH est l'Elohîm de jugement : heureux sont tous ceux qui se confient en lui !
30:19	Car le peuple demeurera dans Sion et dans Yeroushalaim. Tu ne pleureras plus ! Tu ne pleureras plus ! Il aura pitié de toi, il aura pitié de toi dès qu'il entendra ton cri ; dès qu'il aura entendu, il t'exaucera.
30:20	Adonaï vous donnera du pain de détresse et de l'eau d'angoisse. Ceux qui t'enseignent ne se cacheront plus et tes yeux verront ceux qui t'enseignent.
30:21	Et tes oreilles entendront la parole de celui qui sera derrière toi, disant : Voici le chemin, marchez-y ! Quand vous irez à droite ou quand vous irez à gauche.
30:22	Et vous tiendrez pour impurs les placages d'argent de vos images taillées et les éphods d'or de vos images de fonte ; tu les jetteras au loin comme un sang impur, et tu leur diras : Hors d'ici !
30:23	Alors il donnera la pluie sur la semence que tu auras semée en terre, et le grain du revenu de la terre sera abondant et bien nourri ; en ce jour-là, ton bétail paîtra dans un pâturage spacieux<!--Jn. 14:6.-->.
30:24	Les bœufs et les ânes qui labourent la terre mangeront le pur fourrage de ce qui aura été vanné avec la pelle et le van.
30:25	Et il y aura des ruisseaux d'eau courante sur toute haute montagne, et sur toute colline haut élevée, au jour de la grande tuerie, quand les tours tomberont.
30:26	La lumière de la lune sera comme la lumière du soleil, et la lumière du soleil sera sept fois plus grande, comme si c'était la lumière de sept jours, le jour où YHWH bandera la blessure de son peuple, et qu'il guérira la blessure de sa plaie.

### Jugement de YHWH sur les Assyriens

30:27	Voici, le Nom de YHWH vient de loin. Sa colère brûle, son incendie est violent. Ses lèvres sont pleines d'indignation, et sa langue est comme un feu dévorant.
30:28	Son Esprit est comme un torrent qui déborde et atteint jusqu'au milieu du cou, pour disperser les nations d'une telle dispersion qu'elles seront réduites à néant, et il est comme une bride aux mâchoires des peuples, qui les fera errer.
30:29	Vous aurez un cantique comme la nuit où l'on célèbre une fête solennelle, vous aurez le cœur joyeux comme celui qui marche au son de la flûte, pour aller à la montagne de YHWH, vers le Rocher d'Israël.
30:30	Et YHWH fera entendre sa voix, pleine de majesté, et il montrera où aura assené son bras dans l'indignation de sa colère, avec une flamme de feu dévorant, avec éclat, tempête, et pierres de grêle.
30:31	Car l'Assyrien, qui frappait du bâton, sera effrayé par la voix de YHWH.
30:32	Et partout où passe le bâton que YHWH lui destine, et qu'il fera tomber sur lui, on entendra les tambourins et les harpes. Dans la bataille, il combattra contre lui à main levée.
30:33	Car Topheth<!--Topheth : lieu pour brûler. Un lieu à l'extrémité sud-est de la vallée de Hinnom au sud de Yeroushalaim (Jérusalem).--> est déjà préparé, il est même prêt pour le roi ! On a fait son bûcher profond et large. Son bûcher c'est du feu et du bois en abondance. Le souffle de YHWH l'allume comme un torrent de soufre.

## Chapitre 31

### Le secours de YHWH préférable à celui de l'Égypte

31:1	Malheur à ceux qui descendent en Égypte pour avoir de l'aide, et qui s'appuient sur les chevaux, et qui mettent leur confiance dans leurs chars parce qu'ils sont nombreux, et dans leurs cavaliers parce qu'ils sont très forts, mais qui ne regardent pas vers le Saint d'Israël, et ne cherchent pas YHWH.
31:2	Lui aussi, cependant, il est sage. Et il fait venir le malheur et ne révoque pas sa parole. Il s'élève contre la maison des méchants et contre l'aide de ceux qui pratiquent la méchanceté.
31:3	Or les Égyptiens sont des humains et non El, leurs chevaux sont chair et non esprit. Quand YHWH étendra sa main, celui qui donne du secours sera renversé, celui à qui le secours est donné tombera, et eux tous ensemble seront consumés.
31:4	Mais ainsi m'a parlé YHWH : Comme le lion, comme le lionceau rugit sur sa proie, et quoiqu'on appelle contre lui un grand nombre de bergers, il ne se laisse ni effrayer par leur cri, ni abaisser par leur bruit. Ainsi YHWH Sabaoth descendra pour combattre en faveur de la montagne de Sion et de sa colline.
31:5	Comme des oiseaux qui volent, ainsi YHWH Sabaoth<!--YHWH des armées.--> défendra Yeroushalaim. Il la protégera et la sauvera, il passera au-dessus d'elle et la délivrera<!--De. 32:11 ; Ps. 91:4 ; Mt. 23:37.-->.
31:6	Retournez vers celui de qui les enfants d'Israël se sont profondément détournés.
31:7	Car en ce jour-là, chacun de vous rejettera ses faux elohîm d'argent et ses faux elohîm d'or qu'il s'est fabriqués de ses mains pécheresses.
31:8	Et l'Assyrien tombera par l'épée qui n'est pas celle d'un homme, et l'épée qui n'est pas celle d'un être humain le dévorera. Il s'enfuira devant l'épée, et ses jeunes hommes seront rendus tributaires.
31:9	Et saisi de frayeur, il s'enfuira à sa forteresse, et ses chefs seront effrayés à cause de la bannière, dit YHWH, qui a son feu dans Sion et son fourneau dans Yeroushalaim.

## Chapitre 32

### La venue de l'Esprit annonce la paix et la justice

32:1	Voici, un roi régnera selon la justice, et les princes gouverneront avec équité.
32:2	Et un homme sera comme une cachette contre le vent et comme un asile contre la tempête ; comme des ruisseaux d'eau dans un pays sec et l'ombre d'un grand rocher dans une terre altérée.
32:3	Alors les yeux de ceux qui voient ne seront pas retenus, et les oreilles de ceux qui entendent seront attentives.
32:4	Et le cœur de ceux qui agissent avec précipitation comprendra la connaissance, et la langue de ceux qui balbutient parlera vite et clairement.
32:5	L'insensé ne sera plus appelé noble et l'on ne dira plus de l'avare qu'il est généreux.
32:6	Car l'insensé profère la folie, et son cœur s'adonne à la méchanceté, pour exécuter son hypocrisie et pour proférer des faussetés contre YHWH, pour rendre vide l'âme de celui qui a faim, et faire tarir la boisson de celui qui a soif<!--Jn. 10:10.-->.
32:7	Les armes de l'avare sont mauvaises. Il donne des conseils pleins de méchancetés pour détruire par des paroles mensongères les affligés, même quand la cause du pauvre est juste<!--2 Pi. 2:3.-->.
32:8	Mais celui qui est généreux ne donne que de généreux conseils et il se lève en faveur de choses généreuses.
32:9	Femmes insouciantes, levez-vous, écoutez ma voix ! Filles confiantes, prêtez l'oreille à ma parole !
32:10	Dans un an et quelques jours, vous tremblerez, vous qui êtes confiantes, car la vendange sera achevée, la récolte n'arrivera plus.
32:11	Vous qui êtes insouciantes, soyez terrifiées ! Vous qui êtes confiantes, tremblez ! Dépouillez-vous, quittez vos habits et ceignez vos reins de sacs !
32:12	On se frappe la poitrine à cause de la vigne abondante en fruits.
32:13	Les épines et les ronces montent sur la terre de mon peuple, même sur toutes les maisons où il y a de la joie et sur la ville joyeuse.
32:14	Car le palais est abandonné, la multitude de la cité est délaissée. Les lieux inaccessibles du pays et les forteresses serviront de cavernes à toujours. Les ânes sauvages y joueront et les troupeaux y paîtront,
32:15	jusqu'à ce que l'Esprit soit répandu d'en haut sur nous<!--Joë. 2:28 ; Za. 12:10 ; Ac. 2:17-18.--> et que le désert devienne un jardin fruitier<!--Ou Carmel.-->, et que le jardin fruitier soit considéré comme une forêt.
32:16	Le jugement résidera dans le désert et la justice habitera dans le jardin fruitier<!--Ou Carmel.-->.
32:17	L'œuvre de la justice sera la paix, et l'ouvrage de la justice, le repos et la sécurité pour toujours.
32:18	Mon peuple habitera dans une demeure de paix, dans des habitations sûres, dans des lieux de repos tranquilles.
32:19	Mais la grêle tombera sur la forêt, et la ville sera entièrement abaissée.
32:20	Heureux vous qui semez sur toutes les eaux, et qui laissez sans entraves le pied du bœuf et de l'âne !

## Chapitre 33

### YHWH se lève

33:1	Malheur à toi qui dépouilles et qui n'as pas été dépouillé, qui pilles et qu'on n'a pas encore pillé ! Quand tu auras fini de dépouiller, tu seras dépouillé. Quand tu auras achevé de piller, on te pillera.
33:2	YHWH, aie pitié de nous ! Nous nous attendons à toi ! Sois leur bras dès le matin et notre délivrance au temps de la détresse !
33:3	Au son du tumulte, les peuples s'enfuient. Quand tu te lèves, les nations se dispersent.
33:4	On recueille votre butin comme la collecte des sauterelles, on s’y rue comme se précipitent les sauterelles.
33:5	YHWH est élevé, car il habite dans les lieux élevés. Il remplit Sion de jugement et de justice<!--Ps. 97:9.-->.
33:6	La fidélité existera dans votre temps, la sagesse et la connaissance seront les richesses du salut. La crainte de YHWH sera votre trésor.
33:7	Voici, ceux d'Ariel poussent des cris au-dehors, les messagers de paix pleurent amèrement.
33:8	Les routes sont réduites en désolation, les passants n'y passent plus. Il a rompu l'alliance, il rejette les villes, il ne fait plus cas des hommes.
33:9	On mène le deuil, la terre languit. Le Liban est honteux et flétri. Le Saron est comme un désert. Le Bashân et le Carmel secouent leur feuillage.
33:10	Maintenant je me lèverai, dit YHWH, maintenant je serai exalté, maintenant je serai élevé.
33:11	Vous avez conçu du foin, et vous enfanterez de la paille. Votre souffle vous dévorera comme le feu.
33:12	Et les peuples deviendront des combustions de chaux, ils seront brûlés au feu comme des épines coupées.
33:13	Vous qui êtes loin, écoutez ce que j'ai fait ! Et vous qui êtes près, connaissez ma force !

### YHWH assure la paix aux justes

33:14	Les pécheurs sont effrayés dans Sion, et le tremblement saisit les athées : Qui de nous pourra séjourner avec le feu dévorant<!--Hé. 12:29.--> ? Qui de nous pourra séjourner avec les flammes éternelles ?
33:15	Celui qui marche dans la justice et qui parle avec droiture, qui rejette le gain injuste acquis par extorsion, et qui secoue ses mains pour ne pas accepter un pot-de-vin, qui bouche ses oreilles pour ne pas entendre des propos sanguinaires, et qui ferme ses yeux pour ne pas voir le mal.
33:16	Celui-là habitera dans des lieux élevés, des forteresses bâties sur le rocher seront sa haute retraite, son pain lui sera donné, et ses eaux ne lui manqueront pas<!--Jn. 4:14, 6:33-35 ; Ap. 21:6.-->.
33:17	Tes yeux contempleront le roi dans sa beauté, et verront la terre de loin.
33:18	Ton cœur méditera sur la terreur : Où est le secrétaire, où est le trésorier ? Où est celui qui tient le compte des tours ?
33:19	Tu ne verras plus le peuple barbare, le peuple au langage inintelligible, à la langue bégayante qu'on ne comprend pas.
33:20	Regarde Sion, la ville de nos fêtes solennelles ! Que tes yeux voient Yeroushalaim, séjour tranquille, tabernacle qui ne sera pas transporté, et dont les pieux ne seront jamais ôtés, et dont les cordages ne seront pas rompus<!--Ap. 21:2.-->.
33:21	C'est là que YHWH est majestueux pour nous : il nous tient lieu de fleuves et de rivières aux mains larges où n'ira pas de navire à rame, où aucun gros navire ne passera.
33:22	Parce que YHWH est notre Juge, YHWH est notre Législateur, YHWH est notre Roi<!--Yéhoshoua ha Mashiah (Jésus-Christ) exerce toutes les fonctions gouvernementales : législatives, exécutives et judiciaires.-->, c'est lui qui vous sauvera.
33:23	Tes cordages sont relâchés : ils ne maintiennent plus le mât sur sa base et ne tendent plus les voiles. Alors la dépouille d'un grand butin est partagée. Même les boiteux pillent le butin.
33:24	Et celui qui fait sa demeure dans la maison ne dit pas : Je suis malade ! Le peuple qui habite en elle reçoit le pardon de son péché.

## Chapitre 34

### Le jugement des nations<!--Ap. 19:17-21.-->

34:1	Approchez-vous nations, pour écouter ! Et vous peuples, soyez attentifs ! Que la Terre et tout ce qui la remplit écoute ! Que le monde habitable et tout ce qui y est produit écoute !
34:2	Car la colère de YHWH est sur toutes les nations, et sa fureur sur toute leur armée : il les voue à l'interdit, il les livre pour être tuées.
34:3	Leurs blessés mortellement sont jetés là, et la puanteur de leurs cadavres se répand et les montagnes se fondent à cause de leur sang.
34:4	Toute l'armée des cieux se décompose. Les cieux sont roulés comme un livre<!--Ap. 6:14.-->, toute leur armée tombe, comme tombe la feuille de la vigne, comme tombe celle du figuier<!--Mt. 24:28 ; Mc. 13:25.-->.
34:5	Parce que mon épée s'est enivrée dans les cieux, voici, elle va descendre en jugement contre Édom, contre le peuple que j'ai voué à l'interdit.
34:6	L'épée de YHWH est pleine de sang, engraissée de graisse, du sang des agneaux et des boucs, et de la graisse des reins de béliers. Car YHWH a un sacrifice à Botsrah et une grande tuerie dans le pays d'Édom.
34:7	Les buffles descendent avec eux et les bœufs avec les taureaux. Leur terre est enivrée de sang et leur poussière engraissée de graisse.
34:8	Car c'est un jour de vengeance pour YHWH, une année de rétribution pour maintenir la cause de Sion<!--Jé. 46:10 ; Joë. 2:2 ; So. 1:15.-->.
34:9	Les torrents d'Édom seront changés en poix, sa poussière en soufre et sa terre deviendra de la poix ardente.
34:10	Elle ne sera éteinte ni le jour ni la nuit. Sa fumée montera éternellement. Elle sera desséchée d'âge en âge, plus jamais personne n'y passera.
34:11	Le pélican et le hérisson en prendront possession, la chouette et le corbeau y habiteront. On étendra sur elle le cordon à mesurer du chaos<!--Vient de l'hébreu « tohuw » qui signifie « informe », « confusion », « désert ». Voir Ge. 1:2.--> et les pierres du vide<!--Vient de l'hébreu « bohuw » qui signifie « vide », « nul », « perdre », « détruire ». Voir Ge. 1:2.-->.
34:12	Ses nobles n'y proclameront plus de royaume, et tous ses princes seront réduits à néant.
34:13	Les épines croîtront dans ses palais, les chardons et les buissons dans ses forteresses. Elle sera la demeure des dragons et le parvis des autruches<!--Littéralement à traduire par « filles de l'autruche ».-->.
34:14	Les bêtes sauvages des déserts rencontreront les bêtes sauvages des îles et les boucs s'y appelleront les uns les autres. C'est là que la Lilith<!--Lilith est le nom d'une déesse de la nuit connue pour être un démon nocturne qui hantait les lieux déserts d'Édom.--> se tiendra tranquille et trouvera son lieu de repos.
34:15	C'est là que le serpent fera son nid, déposera ses œufs, les couvera, et recueillera ses petits à son ombre. En effet, c'est là que se rassembleront les oiseaux de proie, chaque femelle avec sa voisine.
34:16	Consultez le livre de YHWH et lisez ! Il n'en manquera pas un, aucune femelle ne manquera de visiter sa voisine. Car c'est sa bouche qui l'a ordonné et c'est son Esprit qui les rassemblera.
34:17	Car il leur a jeté le sort, et sa main leur a partagé cette terre au cordeau, ils la posséderont toujours, ils l'habiteront d'âge en âge.

## Chapitre 35

### YHWH se révèle et sauve son peuple

35:1	Le désert et la sécheresse se réjouiront ! La région aride exultera et fleurira comme une rose.
35:2	Elle fleurira, elle fleurira et se réjouira avec allégresse et avec des cris de joie. La gloire du Liban lui est donnée, avec la magnificence de Carmel et de Saron. Ils verront la gloire de YHWH et la magnificence de notre Elohîm.
35:3	Fortifiez les mains faibles et affermissez les genoux qui trébuchent<!--Hé. 12:12.-->.
35:4	Dites à ceux qui ont le cœur troublé : Prenez courage, n'ayez pas peur<!--Jn. 14:1, 16:33.--> ! Voici votre Elohîm : la vengeance viendra, la rétribution d'Elohîm. Il viendra lui-même et vous délivrera.
35:5	Alors les yeux des aveugles seront ouverts et les oreilles des sourds seront débouchées.
35:6	Alors le boiteux sautera comme un cerf et la langue du muet chantera en triomphe<!--Mt. 11:4-5.-->. Car des eaux jailliront dans le désert, et des torrents dans la région aride.
35:7	La terre desséchée deviendra un étang et le sol assoiffé deviendra des sources d'eaux. Dans les repaires où des dragons faisaient leur gîte, l'herbe poussera comme les roseaux et le papyrus.
35:8	Il y aura là une grande route, un chemin qu'on appellera le chemin de sainteté. Celui qui est souillé n'y passera pas, mais il sera pour eux seuls. Ceux qui marcheront dans ce chemin et les fous ne s'y égareront pas.
35:9	Là il n'y aura pas de lion, aucune bête féroce n'y montera ni ne s'y trouvera. Mais les rachetés y marcheront.
35:10	Ceux pour qui YHWH aura payé la rançon<!--Yéhoshoua ha Mashiah (Jésus-Christ) est YHWH qui a payé notre rançon (2 S. 7:23 ; Es. 51:11 ; Ps. 130:8 ; Mc. 10:45 ; 1 Ti. 2:6).--> retourneront, ils viendront en Sion avec chant de triomphe et une joie éternelle sera sur leur tête. Ils obtiendront la joie et l'allégresse, la douleur et le gémissement s'enfuiront.

## Chapitre 36

### Invasion de Sanchérib, menaces de Rabshaké<!--2 R. 18:9-37 ; 2 Ch. 32:1-19.-->

36:1	La quatorzième année du roi Hizqiyah, Sanchérib, roi d'Assyrie, monta contre toutes les villes fortes de Yéhouda et les prit<!--2 R. 18:17.-->.
36:2	Puis le roi d'Assyrie envoya de Lakis à Yeroushalaim, vers le roi Hizqiyah, Rabshaké avec une puissante armée. Rabshaké s'arrêta à l'aqueduc de l'étang supérieur, sur le chemin du champ du foulon.
36:3	Alors Élyakim, fils de Chilqiyah, chef de la maison du roi, Shebna, le secrétaire, et Yoach, fils d'Asaph, l'archiviste, sortirent vers lui.
36:4	Rabshaké leur dit : Dites maintenant à Hizqiyah : Ainsi parle le grand roi, le roi d'Assyrie : Quelle est cette confiance que tu as ?
36:5	Je te le dis, ce ne sont là que des paroles, mais il faut pour la guerre de la prudence et de la force. Or maintenant en qui t'es-tu confié pour t'être rebellé contre moi ?
36:6	Voici, tu t'es confié sur ce bâton qui n'est qu'un roseau cassé, sur l'Égypte, qui perce et traverse la main de celui qui s'appuie dessus ! Tel est pharaon, roi d'Égypte, pour tous ceux qui se confient en lui.
36:7	Et si tu me dis : Nous nous confions en YHWH, notre Elohîm. Mais n'est-ce pas lui dont Hizqiyah a ôté les hauts lieux et les autels, en disant à Yéhouda et à Yeroushalaim : Vous vous prosternerez devant cet autel-ci ?
36:8	Maintenant donc, donne des otages au roi d'Assyrie, mon maître : je te donnerai 2 000 chevaux, si tu peux donner autant d'hommes pour monter dessus.
36:9	Et comment ferais-tu détourner la face à un seul gouverneur d'entre les moindres serviteurs de mon maître ? Mais tu te confies en l'Égypte pour les chars et pour les cavaliers.
36:10	Mais suis-je monté sans YHWH dans ce pays pour le détruire ? YHWH m'a dit : Monte contre ce pays et détruis-le !
36:11	Alors Élyakim, Shebna et Yoach dirent à Rabshaké : Nous te prions de parler en araméen à tes serviteurs, car nous le comprenons, mais ne nous parle pas en hébreu aux oreilles du peuple qui est sur la muraille.
36:12	Et Rabshaké répondit : Est-ce vers ton maître et vers toi que mon maître m'a envoyé pour prononcer ces paroles ? N'est-ce pas vers les hommes qui se tiennent sur la muraille pour manger avec vous leurs excréments et boire leur urine ?
36:13	Puis Rabshaké se tint debout et cria à grande voix en hébreu et dit : Écoutez les paroles du grand roi, du roi d'Assyrie !
36:14	Ainsi parle le roi : Qu'Hizqiyah ne vous séduise pas ! Car il ne pourra pas vous délivrer.
36:15	Qu'Hizqiyah ne vous amène pas à vous confier en YHWH, en disant : YHWH nous délivrera, il nous délivrera et cette ville ne sera pas livrée entre les mains du roi d'Assyrie.
36:16	N'écoutez pas Hizqiyah, car ainsi parle le roi d'Assyrie : Faites un traité de paix avec moi pour votre bien, et sortez vers moi ! Chacun de vous mangera de sa vigne et de son figuier, chacun boira de l'eau de sa citerne,
36:17	jusqu'à ce que je vienne, et que je vous emmène dans un pays qui est comme votre pays, un pays de blé et de bon vin, un pays de pain et de vignes.
36:18	Qu'Hizqiyah donc ne vous séduise pas, en disant : YHWH nous délivrera. Les elohîm des nations ont-ils délivré chacun leur pays de la main du roi d'Assyrie ?
36:19	Où sont les elohîm de Hamath et d'Arpad ? Où sont les elohîm de Sepharvaïm ? Ont-ils délivré Samarie de ma main ?
36:20	Qui sont ceux d'entre tous les elohîm de ces pays qui aient délivré leur pays de ma main, pour que YHWH délivre Yeroushalaim de ma main ?
36:21	Mais ils se turent et ne lui répondirent pas un mot, car le roi avait donné cet ordre, en disant : Vous ne lui répondrez pas.

### Hizqiyah (Ézéchias) informé des menaces

36:22	Après cela, Élyakim fils de Chilqiyah, chef de la maison du roi, Shebna, le secrétaire, et Yoach, fils d'Asaph l'archiviste, vinrent auprès d'Hizqiyah, les vêtements déchirés, et lui rapportèrent les paroles de Rabshaké.

## Chapitre 37

### Hizqiyah (Ézéchias) recherche YHWH auprès de Yesha`yah<!--2 R. 19:1-7 ; 2 Ch. 32:20.-->

37:1	Et il arriva qu'aussitôt que le roi Hizqiyah eut entendu ces choses, il déchira ses vêtements, se couvrit d'un sac, et entra dans la maison de YHWH<!--2 R. 19:1-7 ; 2 Ch. 32:20.-->.
37:2	Puis il envoya Élyakim, chef de la maison du roi, et Shebna, le secrétaire, et les plus anciens des prêtres couverts de sacs, vers Yesha`yah, le prophète, fils d'Amots.
37:3	Et ils lui dirent : Ainsi parle Hizqiyah : Ce jour est un jour d'angoisse, de répréhension et de blasphème, car les enfants sont près de sortir du sein maternel, mais il n'y a pas de force pour enfanter.
37:4	Peut-être que YHWH, ton Elohîm, aura entendu les paroles de Rabshaké, que le roi d'Assyrie, son maître, a envoyé pour insulter l'Elohîm vivant, et qu'il le jugera pour les paroles que YHWH, ton Elohîm, a entendues. Fais donc monter une prière pour le reste qui se trouve encore !
37:5	Les serviteurs du roi Hizqiyah vinrent vers Yesha`yah.
37:6	Et Yesha`yah leur dit : Voici ce que vous direz à votre maître : Ainsi parle YHWH : N'aie pas peur face aux paroles que tu as entendues, par lesquelles les serviteurs du roi d'Assyrie m'ont blasphémé.
37:7	Voici, je vais mettre en lui un esprit tel que, sur une nouvelle qu'il entendra, il retournera dans son pays, et je le ferai tomber par l'épée dans son pays.

### Provocation et menace de Sanchérib<!--2 R. 19:8-13 ; 2 Ch. 32:17-19.-->

37:8	Rabshaké s'en retourna donc et trouva le roi d'Assyrie qui combattait contre Libnah, car il avait appris qu'il était parti de Lakis.
37:9	Alors le roi d'Assyrie entendit dire au sujet de Tirhaka, roi d'Éthiopie : Il est sorti pour te faire la guerre. Dès qu'il eut entendu cela, il envoya des messagers à Hizqiyah, en leur disant :
37:10	Vous parlerez ainsi à Hizqiyah, roi de Yéhouda : Que ton Elohîm, auquel tu te confies, ne te séduise pas, en disant : Yeroushalaim ne sera pas livrée entre les mains du roi d'Assyrie.
37:11	Voici, tu as appris ce que les rois d'Assyrie ont fait à tous les pays, en les détruisant entièrement. Et toi, tu serais délivré ?
37:12	Les elohîm des nations que mes pères ont détruites les ont-ils délivrées, Gozan, Charan, Retseph et les fils d'Éden qui sont à Telassar ?
37:13	Où sont le roi de Hamath, le roi d'Arpad, et le roi de la ville de Sepharvaïm, d'Héna et d'Ivva ?

### Prière d'Hizqiyah (Ézéchias) à YHWH<!--2 R. 19:14-19 ; 2 Ch. 32:20.-->

37:14	Et quand Hizqiyah reçut les lettres de la main des messagers et les lut, il monta à la maison de YHWH, et Hizqiyah les déploya devant YHWH.
37:15	Puis Hizqiyah fit sa prière à YHWH, en disant :
37:16	Ô YHWH Sabaoth ! Elohîm d'Israël qui es assis entre les chérubins ! C'est toi qui es le seul Elohîm de tous les royaumes de la Terre, c'est toi qui as fait les cieux et la Terre.
37:17	Ô YHWH ! incline ton oreille et écoute ! Ô YHWH ! ouvre tes yeux et regarde ! Écoute les paroles de Sanchérib, qu'il m'a envoyé dire pour blasphémer l'Elohîm vivant.
37:18	Il est bien vrai, ô YHWH, que les rois d'Assyrie ont détruit tous les pays et leurs contrées,
37:19	et qu'ils ont jeté dans le feu leurs elohîm, mais ce n'étaient pas des elohîm : ils n'étaient que l'œuvre des mains humaines, du bois et de la pierre. Voilà pourquoi ils les ont détruits.
37:20	Maintenant donc, ô YHWH, notre Elohîm ! Délivre-nous de la main de Sanchérib afin que tous les royaumes de la Terre sachent que toi seul es YHWH.

### Yesha`yah transmet la réponse de YHWH<!--2 R. 19:20-34.-->

37:21	Alors Yesha`yah, fils d'Amots, envoya dire à Hizqiyah : Ainsi parle YHWH, l'Elohîm d'Israël : J'ai entendu la prière que tu m'as faite au sujet de Sanchérib, roi d'Assyrie.
37:22	Voici la parole que YHWH a prononcée contre lui : Elle te méprise, elle se moque de toi, la vierge, la fille de Sion. Elle hoche la tête après toi, la fille de Yeroushalaim.
37:23	Contre qui as-tu élevé ta voix, et levé tes yeux en haut ? C'est contre le Saint d'Israël !
37:24	Tu as outragé Adonaï par le moyen de tes serviteurs et tu as dit : Je suis monté avec la multitude de mes chars au sommet des montagnes, aux côtés du Liban. Je couperai les plus élevés de ses cèdres, et ses plus beaux cyprès, et j'atteindrai sa dernière cime et jusqu'à la forêt de son Carmel.
37:25	J'ai creusé des sources et j'en ai bu les eaux. Je tarirai avec la plante de mes pieds tous les fleuves de l'Égypte.
37:26	N'as-tu pas appris, qu'il y a déjà longtemps, j'ai fait cette ville, et que dès les temps anciens je l'ai ainsi formée ? Et maintenant l'aurais-je conservée pour être réduite en désolation, et les villes fortes en monceaux de ruines ?
37:27	Or leurs habitants, étant dénués de force, ont été épouvantés et confus, ils sont devenus comme l'herbe des champs et l'herbe verte, comme le foin des toits et le blé brûlé avant la formation de sa tige.
37:28	Mais je sais quand tu t'assieds, quand tu sors et quand tu entres, et comment tu es furieux contre moi<!--Ps. 139:2.-->.
37:29	Parce que tu es furieux contre moi et que ton insolence est montée à mes oreilles, je mettrai ma boucle à tes narines et mon mors à tes lèvres, et je te ferai retourner par le chemin par lequel tu es venu.
37:30	Et ceci te servira de signe, ô Hizqiyah : on mangera cette année ce que les champs produisent d'eux-mêmes, et la deuxième année ce qui croîtra encore sans semer, mais la troisième année, vous sèmerez, vous moissonnerez, vous planterez des vignes, et vous en mangerez le fruit.
37:31	Et ce qui aura été sauvé de la maison de Yéhouda, ce qui restera, poussera encore des racines vers le bas, et portera du fruit vers le haut.
37:32	Car il sortira de Yeroushalaim un reste, et de la montagne de Sion quelques rescapés, la jalousie de YHWH Sabaoth fera cela.
37:33	C'est pourquoi ainsi parle YHWH sur le roi d'Assyrie : Il n'entrera pas dans cette ville, il n'y jettera aucune flèche, il ne se présentera pas contre elle avec le bouclier, et il ne dressera pas de tertres contre elle.
37:34	Il s'en retournera par le chemin par lequel il est venu, et il n'entrera pas dans cette ville, dit YHWH.
37:35	Car je protégerai cette ville pour la délivrer à cause de moi, et à cause de David, mon serviteur.

### YHWH frappe Sanchérib<!--2 R. 19:35-37 ; 2 Ch. 32:21.-->

37:36	L'Ange de YHWH<!--Ge. 16:7.--> sortit et frappa 185 000 hommes dans le camp des Assyriens. Et quand on se leva le matin, voici, ils étaient tous morts.
37:37	Alors Sanchérib, roi d'Assyrie, se retira, s'en alla et retourna pour résider à Ninive.
37:38	Et il arriva qu'étant prosterné dans la maison de Nisroc<!--Le nom Nisroc signifie « le grand aigle ». C'était une idole de Ninive adorée par Sanchérib, symbolisée par un aigle à figure humaine.-->, son elohîm, Adrammélec et Sharetser, ses fils, le tuèrent avec l'épée ; puis ils s'enfuirent au pays d'Ararat. Et Ésar-Haddon, son fils, régna à sa place.

## Chapitre 38

### Maladie et guérison d'Hizqiyah (Ézéchias)<!--2 R. 20:1-11 ; 2 Ch. 32:24-30.-->

38:1	En ces temps-là, Hizqiyah tomba malade à mourir<!--2 R. 20:1-11 ; 2 Ch. 32:24-30.-->. Et Yesha`yah, le prophète, fils d'Amots, vint auprès de lui, et lui dit : Ainsi parle YHWH : Donne tes ordres à ta maison, car tu vas mourir et tu ne vivras plus.
38:2	Alors Hizqiyah tourna sa face contre la muraille et fit sa prière à YHWH,
38:3	et dit : Oh ! Je te prie YHWH, souviens-toi maintenant je te prie que j'ai marché devant toi dans la vérité, avec un cœur entier, et que j'ai fait ce qui est agréable à tes yeux ! Et Hizqiyah pleura abondamment.
38:4	Puis la parole de YHWH vint à Yesha`yah, en disant :
38:5	Va, et dis à Hizqiyah ainsi parle YHWH, l'Elohîm de David, ton père : J'ai exaucé ta prière, j'ai vu tes larmes. Voici, j'ajouterai à tes jours quinze années.
38:6	Et je te délivrerai de la main du roi d'Assyrie, toi et cette ville, et je défendrai cette ville.
38:7	Et ce signe t'est donné par YHWH, pour te prouver que YHWH accomplira la parole qu'il a prononcée.
38:8	Voici, je ferai retourner de 10 degrés en arrière avec le soleil l'ombre des degrés qui est descendue sur les degrés d'Achaz. Et le soleil retourna de 10 degrés par les degrés par lesquels il était descendu.
38:9	Voici l'écrit d'Hizqiyah, roi de Yéhouda, lorsqu'il fut malade et survécut à sa maladie.
38:10	Je me disais : Quand mes jours sont en repos : Je m'en irai aux portes du shéol. Je suis privé de ce qui restait de mes années.
38:11	Je disais : Je ne verrai plus Yah, Yah sur la terre des vivants. Je ne regarderai plus aucun être humain parmi les habitants du monde !
38:12	Ma génération s'en est allée, elle est transportée loin de moi comme une cabane de berger. Ma vie est coupée, je suis retranché comme la toile que le tisserand détache de sa trame. Du jour à la nuit tu en auras fini<!--Aux versets 12 et 13, le mot qui a été traduit par « fini » est « shalam » : « être dans une alliance de paix, être en paix ».--> avec moi !
38:13	Je me suis contenu jusqu'au matin, mais pareil à un lion, il brisait tous mes os. Du jour à la nuit tu en auras fini avec moi !
38:14	Je murmurais comme la grue et l'hirondelle, je gémissais comme la colombe. Mes yeux se sont lassés à regarder en haut : Ô YHWH, je suis opprimé, sois mon garant !
38:15	Que dirai-je ? Il m'a parlé et lui-même l'a fait. Je m'en irai tout doucement pendant toutes mes années dans l'amertume de mon âme.
38:16	Adonaï, c'est par ces choses qu'on a la vie, et c'est dans toutes ces choses que mon esprit trouve la vie. Ainsi tu me rétabliras et me feras revivre.
38:17	Voici, dans ma paix une amertume, une amertume m'était survenue. Mais tu t'es attaché à mon âme afin qu'elle ne tombe pas dans la fosse de la pourriture, car tu as jeté tous mes péchés derrière ton dos.
38:18	Car le shéol ne te louera pas, la mort ne te célèbrera pas. Ceux qui sont descendus dans la fosse n'espèrent plus en ta fidélité<!--Ps. 115:17.-->.
38:19	Mais le vivant, le vivant est celui qui te célèbre, comme moi aujourd'hui, et c'est le père qui fait connaître aux enfants ta fidélité<!--Pr. 22:6 ; Ep. 6:4.-->.
38:20	YHWH est venu me délivrer, et à cause de cela, nous jouerons sur les instruments mes cantiques, tous les jours de notre vie dans la maison de YHWH.
38:21	Or Yesha`yah avait dit : Qu'on prenne une masse de figues sèches et qu'on en fasse un emplâtre sur l'ulcère et Hizqiyah vivra.
38:22	Et Hizqiyah avait dit : Quel est le signe que je monterai à la maison de YHWH ?

## Chapitre 39

### Hizqiyah (Ézéchias) montre ses richesses aux Babyloniens<!--2 R. 20:12-19.-->

39:1	En ce temps-là<!--2 R. 20:12-19.-->, Mérodac-Baladan, fils de Baladan, roi de Babel, envoya des lettres avec un présent à Hizqiyah, parce qu'il avait appris qu'il avait été malade, et qu'il était guéri.
39:2	Et Hizqiyah en eut de la joie, et il leur montra sa maison du trésor, l'argent, l'or, les aromates, et l'huile précieuse, toute sa maison d'armes et tout ce qui se trouvait dans ses trésors. Il n'y eut rien que Hizqiyah ne leur montra dans sa maison et dans tout son royaume.
39:3	Puis le prophète Yesha`yah vint vers le roi Hizqiyah, et lui dit : Qu'ont dit ces hommes-là, et d'où sont-ils venus vers toi ? Et Hizqiyah répondit : Ils sont venus vers moi d'un pays éloigné, de Babel.
39:4	Puis Yesha`yah dit : Qu'ont-ils vu dans ta maison ? Hizqiyah répondit : Ils ont vu tout ce qui est dans ma maison : il n'y a rien dans mes trésors que je ne leur aie montré.
39:5	Et Yesha`yah dit à Hizqiyah : Écoute la parole de YHWH Sabaoth :
39:6	Voici, les jours viennent où l'on emportera à Babel tout ce qui est dans ta maison, et ce que tes pères ont amassé dans leurs trésors jusqu'à aujourd'hui ; il n'en restera rien, dit YHWH<!--2 R. 24:13, 25:13-15 ; Jé. 20:5.-->.
39:7	Même on prendra de tes fils qui sortiront de toi, et que tu auras engendrés afin qu'ils soient eunuques dans le palais du roi de Babel<!--Da. 1:3-4.-->.
39:8	Et Hizqiyah répondit à Yesha`yah : La parole de YHWH que tu as prononcée est bonne. Et il ajouta : Au moins qu'il y ait paix et sécurité pendant mes jours.

## Chapitre 40

### Un nouveau message pour Yesha`yah

40:1	Consolez, consolez mon peuple, dit votre Elohîm.
40:2	Parlez à Yeroushalaim selon son cœur et criez-lui que son combat est terminé, que son iniquité est expiée, qu'elle a reçu de la main de YHWH le double pour tous ses péchés.
40:3	La voix de celui qui crie dans le désert<!--L'accomplissement de cette prophétie se trouve en Mt. 3:3, où il nous est dit que la voix qui devait crier ces choses était celle de Yohanan le Baptiste (Jean-Baptiste) (voir aussi Mal. 3:1, 4:5-6 ; Mt. 17:10-13).--> : Préparez le chemin de YHWH, aplanissez dans les régions arides un chemin pour notre Elohîm.
40:4	Toute vallée sera comblée, toute montagne et toute colline seront abaissées, et les lieux tortueux seront redressés, et les lieux raboteux seront aplanis.
40:5	Alors la gloire de YHWH sera révélée, et toute chair en même temps la verra, car la bouche de YHWH a parlé.

### La grandeur d'Elohîm échappe à l'être humain

40:6	La voix dit : Crie ! Et on a répondu : Que crierai-je ? Toute chair est de l'herbe et toute sa grâce est comme la fleur d'un champ<!--Ja. 1:10 ; 1 Pi. 1:24-25.-->.
40:7	L'herbe sèche et la fleur tombe, parce que le vent de YHWH souffle dessus. Certainement le peuple est de l'herbe.
40:8	L'herbe sèche et la fleur tombe, mais la parole de notre Elohîm demeure éternellement<!--1 Pi. 1:25.-->.
40:9	Sion, qui annonce de bonnes nouvelles, monte sur une haute montagne ! Yeroushalaim, qui annonce de bonnes nouvelles, élève ta voix avec force ! Élève-la, n'aie pas peur ! Dis aux villes de Yéhouda : Voici votre Elohîm !
40:10	Voici, Adonaï YHWH viendra<!--Yéhoshoua ha Mashiah (Jésus-Christ) est YHWH qui vient (Es. 35:4, 40:10-11, 60:1, 62:11-12, 66:15-16 ; Za. 14:1-7 ; Mt. 24 ; Jn. 14:1-3 ; Ac. 1:10-12 ; Ap. 3:11, 19:11-12, 22:7,12,20).--> contre le fort et son bras dominera sur lui. Voici son salaire est avec lui et sa récompense<!--Ge. 15:1 ; Es. 49:4, 62:11.--> est devant lui.
40:11	Il paîtra son troupeau comme un berger, il rassemblera les agneaux dans ses bras, il les placera dans son sein ; il conduira celles qui allaitent<!--Jn. 10.-->.
40:12	Qui est celui qui a mesuré les eaux avec le creux de sa main, et qui a pris les dimensions des cieux avec la paume, qui a rassemblé toute la poussière de la Terre dans un boisseau, et qui a pesé au crochet les montagnes et les collines à la balance ?
40:13	Qui a dirigé l'Esprit de YHWH ou qui a été son conseiller pour l'enseigner<!--1 Co. 2:16 ; Ro. 11:34.--> ?
40:14	Avec qui a-t-il pris conseil, et qui l'a instruit et lui a enseigné le sentier de jugement ? Qui lui a appris la connaissance ? Qui lui a fait connaître le chemin de l'intelligence ?
40:15	Voici les nations, elles sont comme une goutte qui tombe d'un seau, elles sont considérées comme de la poussière sur une balance ! Voici les îles, elles sont comme une fine poussière qu'il soulève !
40:16	Et le Liban ne suffirait pas pour faire le feu, et les bêtes qui y sont ne seraient pas suffisantes pour l'holocauste.
40:17	Toutes les nations sont devant lui comme un rien, et il ne les considère que comme de la poussière, et comme un néant.
40:18	À qui donc ferez-vous ressembler El ? Et à quelle ressemblance l'égalerez-vous ?
40:19	L'ouvrier fond l'image, et l'orfèvre la couvre d'or, et y soude des chaînettes d'argent.
40:20	Le pauvre qui renonce à faire une offrande choisit un bois qui ne pourrisse pas ; il se cherche un habile ouvrier pour faire une image taillée qui ne bouge pas<!--Es. 44:9-20.-->.
40:21	Ne le savez-vous pas ? Ne l'avez-vous pas entendu ? Cela ne vous a-t-il pas été déclaré dès le commencement ? Ne l'avez-vous pas entendu dès les fondements de la Terre ?
40:22	C'est lui qui est assis au-dessus du cercle de la Terre, dont les habitants sont comme des sauterelles. C'est lui qui étend les cieux comme un voile, il les déploie même comme une tente pour y demeurer.
40:23	C'est lui qui réduit les princes à rien et qui fait des juges de la Terre une vanité<!--Vient de l'hébreu « tohuw » qui signifie « informe, confusion, chose irréelle, vide ». Voir Ge. 1:2.-->.
40:24	Ils ne sont pas même plantés, pas même semés, même leur tronc n'a pas de racine en terre ; il souffle sur eux, et ils sèchent, et le tourbillon les emporte comme de la paille.
40:25	À qui donc me ferez-vous ressembler, et à qui serai-je égalé ? dit le Saint<!--Voir commentaire en Lu. 1:35 ; Ac. 3:14.-->.
40:26	Levez vos yeux en haut et regardez ! Qui a créé ces choses ? C'est lui qui fait sortir leur armée en bon ordre. Il les appelle toutes par leur nom<!--Voir psaumes 147:4.-->. Par la grandeur de son pouvoir et de sa force puissante, pas une ne manque.
40:27	Pourquoi donc dis-tu, ô Yaacov, pourquoi dis-tu, ô Israël : Ma voie est cachée à YHWH et ma cause<!--Vient d'un mot qui signifie aussi jugement, justice, droit, etc.--> passe inaperçue devant mon Elohîm ?
40:28	Ne le sais-tu pas ? Ne l'as-tu pas entendu ? C'est El-Olam<!--El d'éternité.-->, YHWH, qui a créé les extrémités de la Terre. Il ne se fatigue pas, il ne se lasse pas, et il n'y a pas moyen de sonder son intelligence.
40:29	C'est lui qui donne de la force à celui qui est fatigué, et il multiplie la force de celui qui n'a aucune vigueur.
40:30	Les garçons se lassent et se fatiguent, et les jeunes hommes trébuchent, ils trébuchent.
40:31	Mais ceux qui s'attendent à YHWH renouvellent leur force. Ils s'élèvent avec des ailes comme des aigles. Ils courent et ne se fatiguent pas, ils marchent et ne se lassent pas.

## Chapitre 41

### Dénonciation des idoles

41:1	Îles, faites-moi silence ! Que les peuples renouvellent leurs forces, qu'ils s'approchent et qu'ils parlent ! Allons ensemble en justice.
41:2	Qui a réveillé de l'orient la justice ? Qui l'a appelée à ses pieds ? Qui a soumis à son commandement les nations ? Qui lui a donné la domination sur les rois ? Qui les a livrés à son épée comme de la poussière, et à son arc comme de la paille poussée par le vent ?
41:3	Il les a poursuivis, il est passé en paix par le chemin que son pied n'avait jamais foulé.
41:4	Qui est celui qui a opéré et fait ces choses ? C'est celui qui a appelé les âges dès le commencement. Moi, YHWH, je suis le premier, et je suis avec les derniers<!--Ap. 1:8, 21:6, 22:13.-->.
41:5	Les îles le voient, elles sont dans la crainte, les extrémités de la Terre sont effrayées : ils s'approchent, ils viennent.
41:6	Chacun aide son prochain et chacun dit à son frère : Fortifie-toi !
41:7	L'ouvrier encourage le fondeur, celui qui frappe doucement du marteau encourage celui qui frappe sur l'enclume. Il dit de la soudure : Elle est bonne ! Puis il fixe l'idole avec des clous, afin qu'elle ne bouge pas.
41:8	Mais toi, Israël, tu es mon serviteur, et toi, Yaacov, tu es celui que j'ai élu, la race d'Abraham qui m'a aimé !
41:9	Toi, que j'ai pris des extrémités de la Terre et que j'ai appelé parmi ses nobles, à qui j'ai dit : Tu es mon serviteur, je t'ai choisi et je ne te rejette pas<!--De. 7:6 ; Ps. 77:8.--> !
41:10	N'aie pas peur, car je suis avec toi. Ne sois pas inquiet, car je suis ton Elohîm. Je te fortifie, je viens à ton secours et je te soutiens par la main droite de ma justice.
41:11	Voici, tous ceux qui sont indignés contre toi seront honteux et confus ; ils seront réduits à néant, et les hommes qui ont querelle avec toi périront.
41:12	Tu les chercheras et tu ne les trouveras plus, ceux qui te suscitaient querelle ; et ceux qui te font la guerre seront réduits à néant, ils seront comme quelque chose qui n'existe plus.
41:13	Car je suis YHWH, ton Elohîm, qui soutiens ta main droite et qui te dis : Ne crains rien ! C'est moi qui te secours.
41:14	Ne crains rien, toi vermisseau de Yaacov, et vous hommes d'Israël ! Je viens à ton secours dit YHWH, le Saint d'Israël est ton Rédempteur.
41:15	Voici, je fais de toi un traîneau aigu, tout neuf, ayant des dents, tu fouleras les montagnes et les broieras, et tu rendras les collines semblables à de la balle.
41:16	Tu les vanneras, et le vent les emportera, et le tourbillon les dispersera. Mais toi, tu te réjouiras en YHWH, tu te glorifieras dans le Saint d'Israël.
41:17	Les affligés et les pauvres cherchent de l'eau, mais il n'y en a pas, et leur langue est desséchée par la soif. Moi, YHWH, je répondrai à leurs prières. Moi, l'Elohîm d'Israël, je ne les abandonnerai pas<!--Ge. 28:15 ; Jos. 1:5 ; Hé. 13:5.-->.
41:18	Je ferai jaillir des fleuves sur les hauteurs et des fontaines au milieu des vallées. Je ferai du désert des étangs d'eaux et de la terre sèche des sources d'eaux.
41:19	Je ferai croître dans le désert le cèdre, l'acacia, le myrte et l'olivier. Je mettrai dans les régions arides le cyprès, l'orme et le buis ensemble,
41:20	afin qu'on voie, qu'on sache, qu'on pense, et qu'on comprenne que la main de YHWH a fait cela, et que le Saint d'Israël a créé cela.
41:21	Plaidez votre cause, dit YHWH. Produisez vos arguments, dit le Roi de Yaacov.
41:22	Qu'ils s'approchent et qu'ils nous déclarent ce qui doit arriver. Les premiers événements, qu'en était-il ? Expliquez-les et nous y appliquerons notre cœur pour en connaître l'issue ! Ou bien faites-nous entendre les choses à venir !
41:23	Révélez les choses qui doivent arriver plus tard ! et nous saurons que vous êtes des elohîm. Faites seulement quelque chose de bien ou de mal et nous en serons tout étonnés, puis nous regarderons ensemble.
41:24	Voici, vous n'êtes rien et votre œuvre est sans valeur. C'est une abomination que de vous choisir.
41:25	Je l'ai suscité du nord, et il est venu du soleil levant, celui qui proclame mon Nom. Il marche sur les gouverneurs comme sur la boue, et les foule comme le potier foule l'argile.
41:26	Qui l'a révélé dès le commencement pour que nous le sachions, longtemps à l'avance pour que nous puissions dire : Il est juste ? Mais personne ne l'a révélé, personne ne l'a fait entendre, personne n'a entendu vos paroles.
41:27	Moi, le premier j'ai dit à Sion : Les voici ! Les voici ! Et je donnerai à Yeroushalaim un messager qui annoncera de bonnes nouvelles<!--Es. 52:7 ; Ap. 14:6.-->.
41:28	Je regarde : pas un seul homme ! Parmi eux, pas un conseiller que je puisse interroger et qui réponde quelque chose !
41:29	Voici, quant à eux tous, leurs œuvres ne sont que vanité, leurs idoles en métal fondu ne sont que vent et confusion.

## Chapitre 42

### Le Mashiah, serviteur de YHWH

42:1	Voici mon serviteur que je soutiens, mon élu en qui mon âme prend plaisir. J'ai mis mon Esprit sur lui, il fera sortir le jugement pour les nations<!--Mt. 3:17, 17:5 ; Mc. 9:7.-->.
42:2	Il ne criera pas, il n'élèvera pas la voix, il ne la fera pas entendre dans les rues.
42:3	Il ne brisera pas le roseau cassé et il n'éteindra pas le lumignon qui fume<!--Mt. 12:18-20.-->, il fera sortir la justice selon la vérité.
42:4	Il ne sera pas écrasé et ne s'affaiblira pas jusqu'à ce qu'il ait établi la justice sur la Terre, et que les îles espèrent en sa torah.
42:5	Ainsi parle El, YHWH, qui a créé les cieux, et qui les a étendus, qui a aplani la Terre avec ce qu'elle produit, qui donne la respiration au peuple qui est sur elle, et l'esprit à ceux qui y marchent.
42:6	Moi YHWH, je t'ai appelé en justice, et je prendrai ta main et te garderai, et je t'établirai pour être l'alliance du peuple et la lumière des nations<!--Voir commentaire en Ge. 1:3-5.-->,
42:7	pour ouvrir les yeux des aveugles, pour faire sortir du cachot le prisonnier, et de la maison de détention ceux qui demeurent dans les ténèbres.

### Israël n'a pas été attentif à YHWH

42:8	Je suis YHWH, c'est là mon Nom, et je ne donnerai pas ma gloire à un autre, ni ma louange aux images taillées<!--Es. 48:11.-->.
42:9	Voici, les premières choses sont arrivées et je vous en annonce de nouvelles : avant qu'elles germent, je vous les fais entendre.
42:10	Chantez à YHWH un cantique nouveau, et que sa louange éclate aux extrémités de la Terre, vous qui descendez sur la mer, et tout ce qui est en elle, les îles et leurs habitants !
42:11	Que le désert et ses villes élèvent la voix ! Que les villages où habite Kédar et ceux qui habitent dans les rochers éclatent en chant de triomphe ! Qu'ils s'écrient du sommet des montagnes !
42:12	Qu'on donne gloire à YHWH, et qu'on publie sa louange dans les îles !
42:13	YHWH sortira comme un homme vaillant, il réveillera sa jalousie comme un homme de guerre, il poussera des cris de joie, il poussera de grands cris, et il prévaudra sur ses ennemis.
42:14	Dès longtemps je suis resté tranquille, j'ai gardé le silence, je me suis contenu. Je crierai comme celle qui enfante, je soufflerai et je serai haletant à la fois.
42:15	Je réduirai les montagnes et les collines en désert, et j'en dessécherai toute la verdure, je réduirai les fleuves en îles, et je ferai tarir les étangs.
42:16	Je conduirai les aveugles sur un chemin qu'ils ne connaissent pas, je les ferai marcher par des sentiers qu'ils ne connaissent pas. Je changerai devant eux les ténèbres en lumière et les endroits tortueux en terrain plat. Voilà ce que je ferai, et je ne les abandonnerai pas.
42:17	Ils reculeront et seront couverts de honte, ceux qui se confient aux images taillées et qui disent aux images en métal fondu : Vous êtes nos elohîm !
42:18	Sourds, écoutez ! Et vous aveugles, regardez et voyez !
42:19	Qui est aveugle, sinon mon serviteur ? Et qui est sourd comme mon messager que j'envoie ? Qui est aveugle comme celui que j'ai comblé de grâces ? Qui est aveugle comme le serviteur de YHWH ?
42:20	Vous voyez beaucoup de choses, mais vous ne prenez garde à rien. Vous avez les oreilles ouvertes, mais vous n'entendez rien.
42:21	YHWH a pris plaisir, à cause de sa justice, à rendre la torah grande et majestueuse.
42:22	Mais c'est un peuple pillé et dépouillé ! On les a tous pris au piège dans des trous, cachés dans des maisons d'arrêt. Ils sont livrés au pillage et personne ne les délivre ! ils sont dépouillés et il n'y a personne pour dire : Restitue !
42:23	Qui parmi vous prêtera l'oreille à ces choses ? Qui s'y rendra attentif et l'écoutera à l'avenir ?
42:24	Qui a livré Yaacov au pillage et Israël aux pillards<!--Jg. 2:13-16.--> ? N'est-ce pas YHWH, contre lequel nous avons péché ? Car on n'a pas voulu marcher dans ses voies et on n'a pas obéi à sa torah.
42:25	C'est pourquoi il a déversé sur son peuple toute l'ardeur de sa colère et la férocité de la guerre. Elle l'a embrasé de tous côtés, mais il ne l'a pas reconnu. Et il l'a consumé, mais il ne l'a pas pris à cœur.

## Chapitre 43

### YHWH veut racheter Israël

43:1	Mais maintenant ainsi parle YHWH, qui t'a créé, ô Yaacov ! Celui qui t'a formé, ô Israël ! N'aie pas peur, car je te rachète. Je t'appelle par ton nom, tu es à moi !
43:2	Si tu passes par les eaux, je serai avec toi, et les fleuves, ils ne te submergeront pas. Si tu marches dans le feu, tu ne seras pas brûlé, et la flamme ne t'embrasera pas.
43:3	Car je suis YHWH, ton Elohîm, le Saint d'Israël, ton Sauveur. Je donne l'Égypte pour ta rançon, l'Éthiopie et Saba à ta place.
43:4	Parce que tu es précieux à mes yeux, tu es rendu honorable et je t'aime. Je donne des humains à ta place et des peuples pour ta vie.
43:5	N'aie pas peur, car je suis avec toi. Je ferai venir ta postérité de l'orient et je te rassemblerai de l'occident.
43:6	Je dirai au nord : Donne ! Et au midi : Ne retiens pas ! Fais venir mes fils de loin, et mes filles du bout de la Terre,
43:7	tous ceux qui s'appellent de mon Nom<!--Dans les Écritures, le Nom d'Elohîm le plus cité est YHWH. Yéhoshoua (Jésus), dont le nom signifie « YHWH est salut » correspond au nom et à l'identité qu'Elohîm a révélés à tous ceux qui l'ont rencontré quand il était sur cette terre. Dans sa dernière prière à Gethsémané, Yéhoshoua dit : « J'ai manifesté ton Nom aux hommes » (Jn. 17:6), et « Je leur ai fait connaître ton Nom » (Jn. 17:26). Ce Nom n'est autre que le sien puisque Yéhoshoua (YHWH est salut) était et est le Nom d'Elohîm. Moshè (Moïse) n'avait pas reçu la révélation de ce Nom (Ex. 3:13-14) car cette révélation était réservée à l'Assemblée (Église). En tant qu'épouse du Mashiah (Christ), l'Assemblée porte le Nom du Seigneur et bénéficie de l'autorité qu'il confère. Ainsi, Yéhoshoua est le seul Nom par lequel nous pouvons être sauvés (Ac. 4:12). C'est aussi en son Nom que nous devons être baptisés (Ac. 8:16, 19:5), que nous recevons l'exaucement de nos prières (Jn. 14:13-14, 16:24), que nous sommes délivrés de l'ennemi et obtenons la victoire sur le camp de l'ennemi (Mc. 16:17 ; Ph. 2:9-11).-->, que j'ai créés pour ma gloire, que j'ai formés, que j'ai faits.

### YHWH appelle ses témoins

43:8	Amène dehors le peuple aveugle qui a des yeux, et les sourds qui ont des oreilles.
43:9	Que toutes les nations soient réunies ensemble et que les peuples se rassemblent ! Lequel d'entre eux a proclamé cela et nous a fait connaître les choses anciennes ? Qu'ils produisent leurs témoins et qu'ils se justifient, qu'on les entende et qu'on dise : C'est vrai !
43:10	Vous êtes mes témoins<!--Ac. 1:8.-->, dit YHWH, et mon serviteur que j'ai élu, afin que vous connaissiez et que vous croyiez en moi. Comprenez, Moi, lui ! Avant mes faces il n'a pas été formé de El, et il n'y en aura pas après moi.
43:11	Je suis, je suis YHWH, et en dehors de moi il n'y a pas de Sauveur<!--YHWH dit qu'à part lui, il n'y a pas d'autres sauveurs. Or les écrits de la Nouvelle Alliance affirment que Yéhoshoua ha Mashiah (Jésus-Christ) est le seul Sauveur (Lu. 1:67-80 ; Ac. 4:11-12).-->.
43:12	C'est moi qui ai proclamé ce qui devait arriver, qui vous ai sauvés, et qui vous ai fait entendre l'avenir, ce n'est pas un étranger parmi vous. Vous êtes mes témoins, dit YHWH, que je suis El.
43:13	Et même avant que le jour fût, je suis, et il n'y a personne qui puisse délivrer de ma main. Je ferai l'œuvre, qui m'en empêchera ?

### YHWH fera une chose nouvelle, car Yaacov ne l'a pas honoré

43:14	Ainsi parle YHWH, votre Rédempteur<!--Es. 60:16 ; 1 Co. 1:30 ; Ro. 3:24 ; Ep. 1:7.-->, le Saint d'Israël : J'envoie par amour pour vous quelqu'un contre Babel, et je fais descendre tous les fugitifs, et on entendra le cri des Chaldéens sur les navires.
43:15	Je suis YHWH, votre Saint<!--Voir commentaire en Ac. 3:14.-->, le Créateur d'Israël, votre Roi.
43:16	Ainsi parle YHWH, qui donne un chemin dans la mer et un sentier parmi les eaux impétueuses ;
43:17	qui fait sortir les chars et les chevaux, l'armée et ses vaillants guerriers. Ils se couchent tous ensemble pour ne plus se relever, ils sont anéantis, éteints comme une mèche.
43:18	Ne pensez plus aux choses passées, et ne considérez pas les choses anciennes.
43:19	Voici, je m'en vais faire une chose nouvelle<!--2 Co. 5:17.-->, qui paraîtra bientôt, ne la connaîtrez-vous pas ? Je mettrai un chemin dans le désert et des fleuves dans le lieu de désolation.
43:20	Les bêtes des champs me glorifieront, les dragons et les autruches<!--Littéralement à traduire par « filles de l'autruche ».-->, parce que j'aurai mis des eaux dans le désert, et des fleuves dans la solitude, pour abreuver mon peuple que j'ai élu.
43:21	Ce peuple que je me suis formé racontera mes louanges.
43:22	Mais toi, Yaacov, tu ne m'as pas invoqué, car tu t'es lassé de moi, ô Israël !
43:23	Tu ne m'as pas offert le menu bétail de tes holocaustes, et tu ne m'as pas glorifié dans tes sacrifices. Je ne t'ai pas asservi pour me faire des offrandes, et je ne t'ai pas fatigué pour de l'encens.
43:24	Tu ne m'as pas acheté à prix d'argent du roseau aromatique, et tu ne m'as pas rassasié de la graisse de tes sacrifices. Mais tu m'as asservi par tes péchés, et tu m'as peiné par tes iniquités.
43:25	C'est moi, c'est moi qui efface tes transgressions pour l'amour de moi, et je ne me souviendrai plus de tes péchés.
43:26	Réveille ma mémoire, et plaidons ensemble, fais toi-même le compte afin que tu sois justifié !
43:27	Ton premier père a péché, et tes docteurs se sont rebellés contre moi.
43:28	C'est pourquoi j'ai profané les chefs du lieu saint, et j'ai livré Yaacov comme quelqu'un qui est voué à une entière destruction, et Israël à l'opprobre.

## Chapitre 44

### Promesse de l'Esprit, folie de l'idolâtrie

44:1	Écoute maintenant, ô Yaacov, mon serviteur, et toi Israël que j'ai choisi !
44:2	Ainsi parle YHWH, qui t'a fait et formé dès le ventre, celui qui te soutient : Ne crains rien, ô Yaacov, mon serviteur ! Et toi Yeshouroun<!--Yeshouroun : « celui qui est droit ». Il s'agit du nom symbolique d'Israël décrivant son caractère idéal.--> que j'ai élu.
44:3	Car je répandrai des eaux sur celui qui est altéré, et des rivières sur la terre sèche ; je répandrai mon Esprit sur ta postérité, et ma bénédiction sur tes descendants.
44:4	Et ils germeront comme au milieu de l'herbe, comme les saules auprès des courants d'eau.
44:5	L'un dira : Je suis à YHWH ! l'autre se réclamera du nom de Yaacov, et un autre écrira de sa main : Je suis à YHWH ! Et il prendra pour surnom le nom d'Israël.
44:6	Ainsi parle YHWH, le Roi d'Israël et son Rédempteur, YHWH Sabaoth : Je suis le premier, et je suis le dernier. En dehors de moi, il n'y a pas d'Elohîm<!--Ap. 1:8, 21:6, 22:13.-->.
44:7	Et qui, comme moi, a appelé, déclaré et ordonné cela, depuis que j'ai établi le peuple ancien ? Qu'ils déclarent les choses à venir, les choses qui arriveront ci-après !
44:8	Ne vous effrayez pas et n'ayez pas de crainte ! Ne te l'ai-je pas fait entendre, et déclaré dès ce temps-là ? Vous êtes mes témoins : y a-t-il un autre Éloah en dehors de moi ? Certes il n'y a pas d'autre Rocher<!--YHWH dit qu'il ne connaît pas d'autre rocher. Yéhoshoua ha Mashiah (Jésus-Christ) est ce Rocher qui suivait les Hébreux dans le désert (Mt. 16:18 ; 1 Co. 10:1-4. Voir aussi commentaire en Es. 8:14).-->, je n'en connais pas.
44:9	Les ouvriers d'images taillées ne sont tous que vanité, et leurs choses les plus désirables ne sont d'aucun profit. Elles le témoignent elles-mêmes, elles ne voient rien et ne connaissent rien, afin qu'ils soient honteux.
44:10	Mais qui est-ce qui fabrique un el, ou fond une image taillée, pour n'en avoir aucun profit ?
44:11	Voici, tous ses compagnons seront honteux, car tous ces ouvriers ne sont que des humains. Qu'ils se rassemblent tous, qu'ils se tiennent là ! Ils seront effrayés et rendus honteux tous ensemble.
44:12	Le forgeron fabrique une hache sur des charbons, il la façonne à coups de marteau, il la travaille à la force de son bras. Mais dès qu'il a faim, il n'a plus de force, et s'il ne boit pas d'eau il est fatigué.
44:13	L'artisan sur bois étend sa règle, il la dessine avec le stylet, il la façonne au ciseau, il la dessine au compas. Il la façonne en prenant pour modèle un homme, la beauté d'un être humain, afin qu'elle habite dans une maison.
44:14	Il se coupe des cèdres, il prend des cyprès et des chênes qu'il a laissés croître parmi les arbres de la forêt, il plante des pins et la pluie les fait croître.
44:15	L'être humain se sert de ces arbres pour les brûler car il en prend une partie pour se chauffer, il en fait aussi du feu pour cuire son pain. Avec ça il fabrique aussi un el et se prosterne, il en fait une image taillée et l'adore !
44:16	Il en brûle au feu la moitié afin de pouvoir manger de la viande, préparer un rôti et se rassasier. Il l'utilise aussi pour se chauffer en disant : Ah ! Je me chauffe, je vois la flamme !
44:17	Puis avec le reste il fait un el pour être son image taillée ! Il se prosterne devant elle et l'adore, il lui adresse des prières en disant : Délivre-moi, car tu es mon El !
44:18	Ils ne savent et ne discernent rien, car on leur a plâtré les yeux afin qu'ils ne voient pas, et les cœurs pour qu'ils ne comprennent pas.
44:19	Il ne prend rien à cœur<!--So. 2:1 ; 2 Co. 13:5.-->, il n'a ni connaissance ni intelligence pour dire : J'en ai brûlé une partie au feu, et même j'ai cuit du pain sur les charbons, j'ai rôti de la viande et je l'ai mangée, et ferai-je du reste une abomination ? Me prosternerai-je devant ce qui provient d'un arbre ?
44:20	Il se nourrit de cendres, son cœur trompé l'égare, il ne délivrera pas son âme et ne dira pas : N'est-ce pas du mensonge que j'ai dans ma main droite ?

### YHWH rachète son peuple

44:21	Souviens-toi de ces choses, ô Yaacov ! Ô Israël, car tu es mon serviteur. Je t'ai formé, tu es mon serviteur, ô Israël ! je ne t'oublierai pas.
44:22	J'efface tes transgressions comme une nuée épaisse, et tes péchés comme une nuée. Reviens à moi, car je t'ai racheté.
44:23	Ô cieux ! Réjouissez-vous avec chants de triomphe, car YHWH a agi. Parties inférieures de la Terre, poussez des cris de réjouissance ! Montagnes, éclatez de joie avec chant de triomphe ! Et vous aussi forêts et tous les arbres qui êtes en elles ! Parce que YHWH a racheté Yaacov, et s'est manifesté glorieusement en Israël.
44:24	Ainsi parle YHWH, ton Rédempteur, celui qui t'a formé dès le ventre : Je suis YHWH qui ai fait toutes choses, qui seul ai étendu les cieux, et qui par moi-même ai étendu la Terre.
44:25	Je fais échouer les signes des menteurs, je rends insensés les devins, je fais retourner en arrière les sages et je change leur connaissance en folie.
44:26	Je confirme la parole de mon serviteur et j'accomplis le conseil de mes messagers. Je dis de Yeroushalaim : Tu seras encore habitée ! Et aux villes de Yéhouda : Vous serez rebâties ! Et je redresserai ses lieux déserts.
44:27	Je dis aux profondeurs de l'océan : Asséchez-vous ! Je mettrai vos fleuves à sec.

### Prophétie sur le rétablissement d'Israël par Cyrus

44:28	Je dis de Cyrus<!--Yesha`yah prophétisa la destruction de Babel (Babylone) deux siècles avant la réalisation de cet événement qui eut lieu le 5 octobre 539 av. J.-C. Fait remarquable : il précisa même le nom du commandant Cyrus qui dompta le lion babylonien. L'historien Hérodote (480 – 425 av. J.-C.) donnera par ailleurs raison au prophète sur le déroulement de la prise de Babel.--> : Il est mon berger et il accomplira tous mes désirs. Il dira à Yeroushalaim : Tu seras rebâtie ! Et au temple : Tu seras fondé !

## Chapitre 45

### Cyrus suscité par YHWH

45:1	Ainsi parle YHWH à son mashiah, à Cyrus<!--Cyrus le Grand (580 – 530 av. J.-C.). Voir Esd. 1.-->
45:2	que je tiens par la main droite, pour terrasser les nations devant lui, pour délier les reins des rois, pour ouvrir devant lui les portes afin qu'elles ne soient plus fermées.
45:3	Je marcherai devant toi et j'aplanirai les lieux tortueux. Je briserai les portes en cuivre et je mettrai en pièces les barres en fer. Je te donnerai des trésors des ténèbres<!--Lieu caché.-->, des trésors des lieux secrets, afin que tu saches que je suis YHWH, l'Elohîm d'Israël, qui t'appelle par ton nom.
45:4	Pour l'amour de Yaacov, mon serviteur, et d'Israël mon élu, je t'ai appelé par ton nom. Je t'ai donné un titre flatteur alors que tu ne me connaissais pas.

### YHWH, le seul Elohîm

45:5	Je suis YHWH, et il n'y en a pas d'autre. Moi excepté, il n'y a pas d'Elohîm. Je t'ai ceint avant que tu me connaisses,
45:6	afin que l'on sache, du soleil levant au soleil couchant, qu'il n'y a rien en dehors de moi. Je suis YHWH, et il n'y en a pas d'autre.
45:7	Je forme la lumière et je crée les ténèbres, je fais la paix et je crée l'adversité<!--« Mauvais », « mal », « malheur ».-->. Moi, YHWH, je fais toutes ces choses.
45:8	Ô cieux ! Répandez la rosée d'en haut, et que les nuées laissent couler la justice ! Que la Terre s'ouvre afin que le salut y porte du fruit, qu'elle fasse également germer la justice ! Moi, YHWH, je crée ces choses.
45:9	Malheur à celui qui plaide contre celui qui l'a façonné ! Que le vase plaide contre les autres vases de terre ! Mais l'argile dira-t-elle à celui qui l'a façonnée : Que fais-tu ? ou : Ton œuvre n'a pas de mains<!--Jé. 18:6 ; Ro. 9:21.--> !
45:10	Malheur à celui qui dit à son père : Pourquoi engendres-tu ? Et à sa mère : Pourquoi enfantes-tu ?
45:11	Ainsi parle YHWH, le Saint d'Israël, qui est son Créateur : M'interrogerez-vous sur les choses à venir ? Me donnerez-vous des ordres au sujet de mes fils et de l'œuvre de mes mains ?
45:12	C'est moi qui ai fait la Terre et qui ai créé l'être humain sur elle. C'est moi qui ai étendu les cieux de mes mains, et c'est moi qui donne des ordres à toute leur armée.
45:13	C'est moi qui l'ai suscité dans ma justice, et j'aplanirai toutes ses voies. Il rebâtira ma ville, et libérera mes captifs<!--Cyrus le Grand libéra les Juifs après 70 ans de captivité (Esd. 1).-->, sans rançon ni pots-de-vin, dit YHWH Sabaoth.

### Les autres peuples reconnaîtront la main de YHWH sur Israël

45:14	Ainsi parle YHWH : Le produit du travail de l'Égypte, les gains de l'Éthiopie et des Sabéens, ces hommes de grande stature, passeront chez toi et seront à toi. Ils marcheront à ta suite, ils passeront enchaînés. Ils se prosterneront devant toi et te diront en suppliant : Certainement, El est au milieu de toi et il n'y a pas d'autre Elohîm que lui.
45:15	En vérité, tu es le El qui te caches, l'Elohîm d'Israël, le Sauveur.
45:16	Ils sont tous honteux et confus, ils s'en vont tous avec ignominie, les fabricants d'idoles.
45:17	Mais Israël a été sauvé par YHWH, d'un salut éternel. Vous ne serez ni honteux ni confus pour l'éternité et à jamais.
45:18	Car ainsi parle YHWH qui a créé les cieux, lui qui est l'Elohîm qui a formé la Terre, qui l'a faite et qui l'a affermie, qui l'a créée pour qu'elle ne soit pas informe<!--Voir commentaire en Ge. 1:2.-->, qui l'a formée pour qu'elle soit habitée : Je suis YHWH, et il n'y en a pas d'autre.
45:19	Je n'ai pas parlé en secret ni dans un lieu ténébreux de la Terre, je n'ai pas dit à la postérité de Yaacov : Cherchez-moi vainement ! Je suis YHWH, qui prononce ce qui est juste, qui déclare ce qui est droit.
45:20	Rassemblez-vous et venez, approchez-vous ensemble, vous les rescapés des nations ! Ceux qui portent le bois de leur image taillée n'ont aucune connaissance, ils invoquent un el qui ne sauve pas.
45:21	Déclarez-le, et faites-les approcher ! Qu'ils prennent conseil ensemble ! Qui a fait entendre ces choses dès l'origine, et les a déclarées dès longtemps ? N'est-ce pas moi, YHWH ? Et il n'y a pas d'autre Elohîm à part moi. Le El-Tsaddiq<!--Le El Juste.--> et Sauveur, il n'y en a pas d'autre à part moi.
45:22	Vous tous, aux extrémités de la Terre, regardez vers moi et soyez sauvés ! Car je suis Elohîm, et il n'y en a pas d'autre.
45:23	Je le jure par moi-même, la parole est sortie avec justice de ma bouche, et elle ne sera pas révoquée : Tout genou fléchira devant moi, et toute langue jurera par moi<!--Ph. 2:9-11.-->.
45:24	Certainement, on dira de moi : C'est en YHWH seul que se trouvent la justice et la force. À lui viendront, confondus, tous ceux qui s'irritaient contre lui.
45:25	Toute la postérité d'Israël sera justifiée, et elle se glorifiera en YHWH.

## Chapitre 46

### La puissance de YHWH, l'incapacité des idoles

46:1	Bel s'agenouille, Nebo est renversé. Leurs idoles sont mises sur leurs bêtes et sur leur bétail. Vous les portiez jadis, les voilà chargées, devenues un fardeau pour la bête fatiguée !
46:2	Elles se sont courbées, elles se sont inclinées ensemble sur leurs genoux, et ne peuvent échapper au fardeau, et elles-mêmes s'en vont en captivité.
46:3	Écoutez-moi, maison de Yaacov, et vous tous, reste de la maison d'Israël ! Je me suis chargé de vous dès le ventre, et je vous ai portés dès le sein maternel.
46:4	Jusqu'à votre vieillesse, Moi, lui. Je vous soutiendrai jusqu'à votre blanche vieillesse. Je l'ai fait, et je vous porterai encore, je vous soutiendrai et vous sauverai.
46:5	À qui me ferez-vous ressembler pour le faire mon égal ? À qui me comparerez-vous pour que nous soyons semblables ?
46:6	Ils versent l'or de leur bourse, et pèsent l'argent à la balance, et ils engagent un orfèvre pour en faire un el, puis ils se prosternent devant lui et l'adorent !
46:7	Ils le portent, ils le chargent sur les épaules, ils le déposent à sa place et il se tient debout. Il ne bouge pas de son lieu ! Puis on crie vers lui, mais il ne répond pas, et il ne les délivre pas de leur détresse.
46:8	Souvenez-vous de cela et soyez des hommes<!--« Montrer sa virilité », « champion », « grand homme ».--> ! Rappelez-le à votre pensée, ô vous transgresseurs !
46:9	Souvenez-vous des premières choses d'autrefois ! Je suis El, et il n'y en a pas d'autre<!--De. 4:35.-->. Je suis Elohîm et il n'y en a pas comme moi.
46:10	Je déclare dès le commencement ce qui doit arriver à la fin, et longtemps auparavant, les choses qui n'ont pas encore été faites. Je dis : Mon conseil s'accomplira et je ferai tout ce que je désire.
46:11	J'appelle de l'orient l'oiseau de proie, et d'une terre éloignée un homme pour exécuter mon conseil. Oui, j'ai parlé, et je le ferai arriver, je l'ai formé, je l'accomplirai aussi.
46:12	Écoutez-moi, vous qui avez le cœur endurci et qui êtes éloignés de la justice.
46:13	Je fais approcher ma justice, elle n'est pas loin, mon salut, il ne tardera pas. Je mettrai le salut en Sion pour Israël, qui est ma gloire.

## Chapitre 47

### Jugement sur Babel

47:1	Descends, et assieds-toi dans la poussière, vierge, fille de Babel ! Assieds-toi à terre, il n'y a plus de trône pour la fille des Chaldéens ! Car tu ne te feras plus appeler la douce et la délicate.
47:2	Mets la main aux meules et fais moudre la farine ! Relève ton voile<!--Voir Ca. 4:1,3, 6:7.-->, retrousse la traîne de ta robe, découvre tes jambes et traverse les fleuves !
47:3	Ta nudité sera découverte et ta honte sera vue. Je prendrai vengeance, je ne t'affronterai pas comme un être humain.
47:4	Quant à notre Rédempteur, son Nom est YHWH Sabaoth, le Saint d'Israël.
47:5	Assieds-toi en silence et entre dans les ténèbres, fille des Chaldéens, car tu ne te feras plus appeler la maîtresse des royaumes.
47:6	J'ai été embrasé de colère contre mon peuple, j'ai profané mon héritage, c'est pourquoi je les ai livrés entre tes mains, mais tu n'as pas usé de miséricorde envers eux, tu as durement appesanti ton joug sur le vieillard.
47:7	Et tu as dit : Je serai maîtresse pour toujours ! Si bien que tu n'as pas pris ces choses à cœur, tu ne t'es pas souvenue que cela prendrait fin.
47:8	Maintenant donc écoute ceci, toi voluptueuse qui habites avec assurance, et qui dis en ton cœur : C'est moi, et il n'y en a pas d'autre que moi ! Je ne deviendrai pas veuve, et je ne saurai pas ce que c'est que d'être privée d'enfants !
47:9	Mais ces deux choses t'arriveront en un instant, en un jour : la privation d'enfants et le veuvage. Elles viendront sur toi dans leur perfection, malgré le grand nombre de tes sorcelleries, malgré toute la puissance de tes incantations<!--Ap. 18:7-8.-->.
47:10	Et tu t'es confiée dans ta méchanceté, et tu disais : Personne ne me voit ! Ta sagesse et ta connaissance t'ont pervertie, et tu disais en ton cœur : C'est moi, et il n'y en a pas d'autre que moi !
47:11	C'est pourquoi le malheur viendra sur toi sans que tu en connaisses l'aurore, le désastre qui tombera sur toi sera tel, que tu ne pourras pas l'apaiser<!--Vient de l'hébreu « kaphar » qui signifie « couvrir », « purger », « faire une expiation », « réconciliation », « recouvrir de poix » ou encore « propitiation ».-->, la dévastation viendra sur toi soudainement, sans que tu t'en aperçoives.
47:12	Reste donc avec tes incantations, et avec le grand nombre de tes sorcelleries, dans lesquels tu as travaillé dès ta jeunesse ! Peut-être pourras-tu en tirer quelque profit, peut-être sauras-tu faire trembler !
47:13	Tu t'es lassée de la multitude de tes conseils. Qu'ils se présentent donc et qu'ils te sauvent, ces astrologues qui observent les étoiles et le ciel, qui, aux nouvelles lunes, te font connaître ce qui doit t'arriver !
47:14	Voici, ils sont devenus comme de la paille, le feu les consume, ils ne délivreront pas leur vie du pouvoir de la flamme. Il n'y a ni charbon pour se chauffer, ni feu pour s'asseoir en face.
47:15	Ainsi sont-ils pour toi, ceux pour qui tu t'es fatiguée. Ceux avec lesquels tu as trafiqué dès ta jeunesse erreront chacun de son côté comme un vagabond : il n'y a personne pour te sauver.

## Chapitre 48

### YHWH rappelle ses promesses

48:1	Écoutez ceci, maison de Yaacov, vous qui vous appelez du nom d'Israël, vous qui êtes sortis des eaux de Yéhouda, vous qui jurez par le Nom de YHWH et qui faites mention d'Elohîm d'Israël, mais non pas conformément à la vérité et à la justice<!--Jé. 5:2.--> !
48:2	Car ils prennent leur nom de la sainte cité, et ils s'appuient sur l'Elohîm d'Israël, dont le Nom est YHWH Sabaoth<!--Ex. 20:7.-->.
48:3	J'ai déclaré les premières choses dès le commencement, elles sont sorties de ma bouche et je les ai proclamées ; soudain je les ai faites, elles sont arrivées.
48:4	Parce que je savais que tu es obstiné, que ton cou est une barre de fer, et que ton front est en cuivre,
48:5	je t'ai déclaré ces choses dès lors, et je les ai faites entendre avant qu'elles arrivent, de peur que tu ne dises : C'est mon idole qui a fait toutes ces choses, c'est mon image taillée ou mon image en métal fondu qui les a ordonnées.
48:6	Tu l'entends ! Vois tout cela ! Et vous, ne l'annoncerez-vous pas ? Je te fais entendre dès maintenant des choses nouvelles, des choses tenues secrètes, que tu ne connaissais pas.
48:7	Elles sont créées maintenant, et non pas depuis le commencement ; et avant ce jour-ci tu n'en avais rien entendu, afin que tu ne dises pas : Voici, je le savais !
48:8	Tu n'en avais pas entendu parler, tu n'en savais rien et ton oreille n'a pas été ouverte longtemps avant, car je savais que tu trahirais, que tu trahirais ; aussi tu as été appelé transgresseur dès le ventre.
48:9	Pour l'amour de mon Nom, je suspends ma colère, et pour l'amour de ma louange, je retiens mon courroux contre toi, afin de ne pas te retrancher.
48:10	Voici, je t'ai épuré, mais non comme l'argent, je t'ai éprouvé au creuset de l'affliction.
48:11	Pour l'amour de moi, pour l'amour de moi, je le ferai, car comment mon nom serait-il profané ? Je ne donnerai pas ma gloire à un autre.
48:12	Écoute-moi, Yaacov ! Et toi Israël, mon appelé ! Moi, je suis le premier, je suis aussi le dernier.
48:13	Ma main aussi a fondé la Terre et ma droite a étendu les cieux. Quand je les appelle, ils comparaissent ensemble.
48:14	Vous tous, rassemblez-vous et écoutez ! Lequel parmi eux a déclaré ces choses ? Celui que YHWH aime exécutera ce qu'il désire contre Babel, et son bras sera contre les Chaldéens.
48:15	Moi, je suis celui qui ai parlé, je l'ai aussi appelé, je l'ai fait venir et ses desseins réussiront.
48:16	Approchez-vous de moi et écoutez ceci ! Dès le commencement, je n'ai pas parlé en secret, depuis l'origine de ces choses, je suis là. Et maintenant, Adonaï YHWH m'a envoyé avec son Esprit.
48:17	Ainsi parle YHWH, ton Rédempteur, le Saint d'Israël : Je suis YHWH, ton Elohîm, qui t'enseigne pour ton profit, et qui te guide dans le chemin où tu dois marcher.
48:18	Ô ! Si tu étais attentif à mes commandements, ta paix serait comme un fleuve, et ta justice comme les flots de la mer<!--Jos. 1:8 ; Ps. 1:2 ; Jn. 14:21 ; Ja. 1:22.-->.
48:19	Ta postérité deviendrait comme le sable, et les rejetons de tes entrailles comme les grains de sable<!--Ge. 15:5, 22:17, 32:12.-->. Son nom ne serait ni retranché ni détruit de devant ma face.
48:20	Sortez de Babel<!--Voir Je. 51:6 et Ap. 18:4.-->, fuyez loin des Chaldéens ! Publiez ceci avec une voix de chant de triomphe, annoncez-le, portez ceci jusqu'aux extrémités de la Terre, dites : YHWH a racheté son serviteur Yaacov !
48:21	Et ils n'auront pas soif dans les déserts où il les conduira : il fera jaillir pour eux l'eau hors du rocher<!--1 Co. 10:4.-->, même il leur fendra le rocher et les eaux couleront.
48:22	Il n'y a pas de paix pour les méchants, dit YHWH.

## Chapitre 49

### Le Mashiah, la lumière de tous les peuples

49:1	Îles, écoutez-moi ! Soyez attentifs, vous peuples éloignés ! YHWH m'a appelé dès le ventre, il a fait mention de mon nom dès les entrailles de ma mère<!--Jé. 1:5 ; Ps. 139:16.-->.
49:2	Et il a rendu ma bouche semblable à une épée aiguë. Il m'a caché dans l'ombre de sa main, et m'a rendu semblable à une flèche bien polie, il m'a caché dans son carquois.
49:3	Et il m'a dit : Tu es mon serviteur, ô Israël, en qui je me glorifierai.
49:4	Et moi j'ai dit : C'est en vain que j'ai travaillé, c'est pour du vide, pour du vent que j'ai consumé ma force. En vérité, mon droit est auprès de YHWH et ma récompense<!--Ou le salaire. Voir Es. 40:10.--> auprès de mon Elohîm.
49:5	Maintenant donc, YHWH, qui m'a formé dès le ventre pour être à son service, m'a dit que je lui ramène Yaacov, bien qu'Israël ne soit pas rassemblé ; toutefois je serai glorifié aux yeux de YHWH, et mon Elohîm sera ma force.
49:6	Il me dit : C'est peu de chose que tu sois mon serviteur pour relever les tribus de Yaacov et pour ramener les préservés d'Israël : c'est pourquoi je t'ai donné pour lumière aux nations, afin que tu sois mon salut jusqu'aux extrémités de la Terre.
49:7	Ainsi parle YHWH, le Rédempteur, le Saint d'Israël, à celui dont l'âme est méprisée et que la nation regarde comme un être abominable, à l'esclave de ceux qui dominent : Les rois le verront, et se lèveront, et les princes aussi, et ils se prosterneront devant lui, pour l'amour de YHWH, qui est fidèle, et du Saint d'Israël qui t'a élu.
49:8	Ainsi parle YHWH : Je t'ai exaucé au temps de la faveur, et je t'ai secouru au jour du salut. Je te garderai, et je te donnerai pour être l'alliance du peuple, pour relever la terre, afin que tu possèdes les héritages dévastés,
49:9	en disant à ceux qui sont emprisonnés : Sortez ! Et à ceux qui sont dans les ténèbres : Découvrez-vous ! Ils paîtront sur les chemins, et leurs pâturages seront sur tous les lieux élevés.
49:10	Ils n'auront pas faim et ils n'auront pas soif, la terre desséchée et le soleil ne les frapperont plus. Car celui qui a pitié d'eux les guidera, et il les conduira vers des sources d'eaux<!--Ps. 121:6 ; Lu. 1:67-79.-->.
49:11	Et je réduirai toutes mes montagnes en chemins, et mes sentiers seront relevés.
49:12	Les voici, ils viennent de loin : les uns viennent du nord et de l'occident, et les autres du pays de Sinim.
49:13	Ô cieux, réjouissez-vous avec des chants de triomphe ! Et toi, ô Terre, sois dans l'allégresse ! Et vous, ô montagnes, éclatez de joie avec des chants de triomphe ! Car YHWH console son peuple, il a compassion de ceux qu'il a affligés.
49:14	Mais Sion disait : YHWH me délaisse, Adonaï m'oublie !
49:15	Une femme oublie-t-elle son nourrisson ? N'a-t-elle pas pitié du fils de ses entrailles ? Même si elle l'oubliait, moi je ne t'oublierai jamais.
49:16	Voici, je t'ai gravé sur les paumes de mes mains ! Tes murs sont continuellement devant moi.
49:17	Tes fils se hâtent de revenir, mais ceux qui te détruisaient et ceux qui te réduisaient en désert, s'éloignent de toi.
49:18	Lève tes yeux et regarde tout autour : tous se rassemblent, ils viennent à toi. Je suis vivant, dit YHWH, tu te revêtiras de tous comme d'une parure et tu t'en ceindras comme une épouse.
49:19	Car tes déserts, tes ruines, et ton pays détruit seront désormais trop étroits pour ses habitants, et ceux qui t'engloutissaient s'éloigneront.
49:20	Les enfants que tu auras, après avoir perdu les autres, diront encore à tes oreilles : Le lieu est trop étroit pour moi, fais-moi de la place pour que je puisse y demeurer.
49:21	Et tu diras dans ton cœur : Qui me les a engendrés ? vu que j'avais perdu mes enfants et que j'étais stérile. J'étais en exil, mise à l'écart. Qui les a élevés ? J'étais restée seule : ceux-ci, où étaient-ils ?
49:22	Ainsi parle Adonaï YHWH : Voici, je lèverai ma main vers les nations et je dresserai ma bannière vers les peuples, et ils ramèneront tes fils entre leurs bras, et ils porteront tes filles sur les épaules.
49:23	Les rois seront tes nourriciers et leurs princesses tes nourrices. Ils se prosterneront devant toi le visage contre terre, et ils lécheront la poussière de tes pieds. Et tu sauras que je suis YHWH, et que ceux qui se confient en moi ne seront pas confus<!--Ps. 22:5-6, 69:7 ; Ro. 9:33 ; 1 Pi. 2:6.-->.
49:24	Le butin de l'homme vaillant lui sera-t-il enlevé ? Et les captifs du juste seront-ils délivrés ?
49:25	Car ainsi parle YHWH : Même les captifs pris par l'homme vaillant lui seront enlevés et le butin du tyran lui échappera, car je plaiderai moi-même avec ceux qui plaident contre toi et je délivrerai tes enfants.
49:26	Et je ferai manger leur propre chair à ceux qui t'oppriment, et ils s'enivreront de leur sang comme du moût, et toute chair connaîtra que je suis YHWH, ton Sauveur, ton Rédempteur, le Puissant de Yaacov.

## Chapitre 50

### Avertissements de YHWH par son serviteur

50:1	Ainsi parle YHWH : Où est la lettre de divorce par laquelle j'ai répudié votre mère<!--De. 24:1 ; Jé. 3:8 ; Mt. 5:31.--> ? Ou bien, auquel de mes créanciers vous ai-je vendus ? Voici, vous avez été vendus à cause de vos iniquités, et votre mère a été répudiée à cause de vos transgressions.
50:2	Je suis venu : pourquoi ne s'est-il trouvé personne ? J'ai appelé : pourquoi personne n'a-t-il répondu ? Ma main est-elle courte, courte pour racheter<!--No. 11:23 ; Es. 59:1.--> ? Ou n'y a-t-il plus de force en moi pour délivrer ? Voici, par ma menace, je dessèche la mer, je réduis les fleuves en désert ; leurs poissons se corrompent faute d'eau, et ils meurent de soif.
50:3	Je revêts les cieux de noirceur, et je fais d'un sac leur couverture.
50:4	Adonaï YHWH m'a donné la langue de disciple<!--Vient d'un mot hébreu qui signifie « enseigné, appris, traité en disciple, exercé ».--> pour que je sache soutenir par la parole celui qui est fatigué<!--Job 6:14 ; 1 Th. 5:14.-->. Matin après matin, il me réveille, il me réveille pour que j'écoute comme un disciple.
50:5	Adonaï YHWH m'a ouvert l'oreille et je n'ai pas été rebelle, et je ne me suis pas retiré en arrière.
50:6	J'ai exposé mon dos à ceux qui me frappaient et mes joues à ceux qui m'arrachaient la barbe, je n'ai pas caché mon visage aux insultes et aux crachats<!--Mt. 5:39, 26:67 ; Lu. 6:29, 18:32.-->.
50:7	Mais Adonaï YHWH m'a aidé, c'est pourquoi je n'ai pas été humilié, et ainsi j'ai rendu mon visage semblable à un caillou<!--Ez. 3:8-9.-->, car je sais que je ne serai pas confondu.
50:8	Celui qui me justifie est proche : qui plaidera contre moi ? Comparaissons ensemble ! Qui est mon adversaire ? Qu'il s'approche de moi !
50:9	Voici, Adonaï YHWH m'aidera, qui me condamnera ? Voici, tous seront usés comme un vêtement, la teigne les dévorera.
50:10	Qui est celui d'entre vous qui craint YHWH, et qui obéit à la voix de son serviteur ? Que celui qui marche dans les ténèbres, et qui n'a pas de clarté, se confie dans le Nom de YHWH, et qu'il s'appuie sur son Elohîm.
50:11	Voici, vous tous qui allumez le feu, et qui vous ceignez d'étincelles, marchez à la lumière de votre feu et des étincelles que vous avez allumées ! Voici ce que vous aurez de ma main : vous vous coucherez dans la terreur.

## Chapitre 51

### Exhortation à ceux qui recherchent YHWH

51:1	Écoutez-moi, vous qui poursuivez la justice et qui cherchez YHWH ! Regardez au rocher d'où vous avez été taillés, et au creux de la citerne dont vous avez été tirés.
51:2	Regardez à Abraham, votre père, et à Sarah qui vous a enfantés ! Car lui seul je l'ai appelé, je l'ai béni et multiplié<!--Ro. 4:1-16 ; Hé. 11:8-12.-->.
51:3	Car YHWH consolera Sion, il consolera tous ses lieux dévastés, il rendra son désert semblable à Éden, et sa région aride à un jardin de YHWH. En elle sera trouvée la joie et l'allégresse, les actions de grâces et le son de mélodie.
51:4	Écoutez-moi donc attentivement, mon peuple ! Prêtez-moi l'oreille, vous ma nation ! Car la torah sortira de moi, et j'établirai mon jugement pour être la lumière des peuples.
51:5	Ma justice est proche, mon salut va paraître, et mes bras jugeront les peuples. Les îles espéreront en moi, elles se confieront en mon bras.
51:6	Levez les yeux vers les cieux et regardez en bas sur la Terre ! Car les cieux s'évanouiront comme la fumée, et la Terre tombera en lambeaux comme un vêtement, et ses habitants périront pareillement. Mais mon salut demeurera éternellement, et ma justice ne sera pas anéantie.
51:7	Écoutez-moi, vous qui connaissez la justice, peuple dans le cœur duquel est ma torah ! N'ayez pas peur de l'opprobre des hommes et ne soyez pas effrayés devant leurs outrages.
51:8	Car la teigne les rongera comme un vêtement<!--Mt. 6:19 ; Lu. 12:33 ; Ja. 5:2.-->, et la gerce les dévorera comme de la laine. Mais ma justice demeurera toujours, et mon salut d'âge en âge.
51:9	Réveille-toi, réveille-toi, revêts-toi de force, bras de YHWH ! Réveille-toi comme aux jours anciens, aux siècles passés. N'es-tu pas celui qui tailla en pièce l'Égypte, et qui blessa mortellement le dragon ?
51:10	N'est-ce pas toi qui fis tarir la mer, les eaux du grand abîme ? Qui réduisis les lieux les plus profonds de la mer en un chemin afin que les rachetés y passent ?
51:11	Ainsi ceux pour qui YHWH aura payé la rançon retourneront, ils iront à Sion avec chants de triomphe et une allégresse éternelle couronnera leurs têtes. Ils obtiendront la joie et l'allégresse, la douleur et le gémissement s'enfuiront.
51:12	Je suis, je suis celui qui vous console. Qui es-tu pour avoir peur de l'homme mortel qui mourra, et du fils de l'être humain qui deviendra comme du foin ?
51:13	Et tu oublierais YHWH qui t'a fait, qui a étendu les cieux et fondé la Terre ! Et chaque jour tu tremblerais continuellement à cause de la fureur de ton oppresseur parce qu'il s'apprête à détruire ! Où est la fureur de ton oppresseur ?
51:14	Bientôt celui qui est courbé sous les fers sera mis en liberté, afin qu'il ne meure pas dans la fosse, et que son pain ne lui manque pas.
51:15	Car je suis YHWH, ton Elohîm, qui fend la mer et les flots rugissants. YHWH Sabaoth est son Nom.
51:16	Je mets mes paroles dans ta bouche et je te couvre de l'ombre de ma main pour établir les cieux et fonder la Terre, pour dire à Sion : Tu es mon peuple !
51:17	Réveille-toi ! Réveille-toi ! Lève-toi, Yeroushalaim, qui as bu de la main de YHWH la coupe de sa fureur, tu as bu, tu as sucé la lie de la coupe d'étourdissement<!--Ps. 60:5 ; Ap. 14:10.--> !
51:18	Il n'y en a aucun pour la conduire, de tous les enfants qu'elle a enfantés, il n'y en a aucun qui la prenne par la main, de tous les enfants qu'elle a nourris.
51:19	Ces deux choses te sont arrivées, qui te plaindra ? Le ravage et la ruine, la famine et l'épée. Par qui te consolerai-je ?
51:20	Tes enfants en défaillance gisaient aux carrefours de toutes les rues, comme une antilope prise dans les filets, pleins de la fureur de YHWH, de la répréhension de ton Elohîm.
51:21	C'est pourquoi, écoute maintenant ceci, ô affligée, ivre, mais non pas de vin.
51:22	Ainsi parle YHWH, ton Seigneur et ton Elohîm, qui plaide la cause de son peuple : Voici, je prends de ta main la coupe d'étourdissement, la lie de la coupe de ma fureur, tu n'en boiras plus désormais !
51:23	Car je la mettrai dans la main de ceux qui t'ont affligée, et qui disaient à ton âme : Courbe-toi, et nous passerons ! C'est pourquoi tu as exposé ton corps comme la terre, comme une rue pour les passants.

## Chapitre 52

### Le réveil de Yeroushalaim (Jérusalem), la ville sainte

52:1	Réveille-toi, réveille-toi, Sion ! Revêts-toi de ta force ! Yeroushalaim, ville sainte ! Revêts-toi de tes vêtements magnifiques ! Car désormais ni l'incirconcis ni l'impur n'entreront plus chez toi.
52:2	Yeroushalaim, secoue ta poussière, lève-toi, et assieds-toi ! Détache les liens de ton cou, captive, fille de Sion !
52:3	Car ainsi parle YHWH : Vous avez été vendus gratuitement, et vous serez aussi rachetés sans argent.
52:4	Car ainsi parle Adonaï YHWH : Mon peuple descendit jadis en Égypte pour y séjourner, mais les Assyriens l'opprimèrent sans cause.
52:5	Et maintenant, qu'ai-je à faire ici, dit YHWH, quand mon peuple a été enlevé gratuitement ? Ceux qui dominent sur lui le font hurler, dit YHWH. Chaque jour, mon Nom est continuellement méprisé<!--Blasphémé. Voir Ro. 2:24.-->.
52:6	C'est pourquoi mon peuple connaîtra mon Nom. C'est pourquoi il saura, en ce jour-là, que je suis parle : Voici je suis !
52:7	Combien sont beaux sur les montagnes les pieds de celui qui apporte de bonnes nouvelles, qui publie la paix<!--Na. 2:1 ; Ro. 10:15.-->, qui prêche un bon message, qui publie le salut, qui dit à Sion : Ton Elohîm règne !
52:8	Tes sentinelles élèvent leurs voix, elles se réjouissent ensemble avec chants de triomphe car, les yeux dans les yeux, elles voient YHWH revenir à Sion.
52:9	Déserts de Yeroushalaim, éclatez, réjouissez-vous ensemble avec chants de triomphe ! Car YHWH console son peuple, il rachète Yeroushalaim.
52:10	YHWH manifeste le bras de sa sainteté aux yeux de toutes les nations<!--Es. 53:1.-->, et toutes les extrémités de la Terre verront le salut<!--Toutes les extrémités de la terre verront le salut de YHWH, c'est-à-dire Yéhoshoua (Jésus) (Mt. 28:18-20).--> de notre Elohîm.
52:11	Partez, partez, sortez de là ! Ne touchez à rien d'impur ! Sortez du milieu d'elle<!--Jé. 51:45 ; 2 Co. 6:17 ; Ap. 18:4.--> ! Purifiez-vous, vous qui portez les vases de YHWH.
52:12	Car vous ne sortirez pas en hâte, et vous ne marcherez pas en fuyant, car YHWH ira devant vous, et l'Elohîm d'Israël sera votre arrière-garde.

### Le serviteur de YHWH

52:13	Voici, mon serviteur prospérera : il sera haut placé, élevé et exalté à l'extrême.
52:14	De même que beaucoup ont été stupéfaits en le voyant, tant son visage défiguré n'était plus celui d'un homme, son apparence n'était plus celle des fils d'Adam,
52:15	de même il aspergera beaucoup de nations. Et les rois fermeront la bouche devant lui, car ils verront ce qui ne leur avait pas été raconté et ils considéreront ce qu'ils n'avaient pas entendu.

## Chapitre 53

### Le sacrifice du Mashiah, serviteur de YHWH

53:1	Qui a cru à notre prédication ? Et à qui le bras de YHWH<!--Yéhoshoua ha Mashiah (Jésus-Christ) homme est le bras de YHWH. Le bras de YHWH est le symbole de la puissance divine. Cette puissance s'est manifestée dans l'œuvre du Mashiah accomplissant le salut du monde. Le prophète est transporté au moment où le peuple juif, après avoir rejeté son Mashiah, ouvrira enfin les yeux et acceptera celui qu'il a percé (Za. 12:10 ; Ap. 1:7). Voir aussi Jé. 27:4-5, 32:17.--> a-t-il été révélé ?
53:2	Toutefois il s'est élevé devant lui comme une jeune plante, comme un rejeton qui sort d'une terre desséchée. Il n'y avait en lui ni beauté, ni splendeur, quand nous le regardions, ni apparence qui nous le fasse désirer.
53:3	Il était le méprisé et le rejeté des hommes<!--Ps. 22:6-7 ; Mt. 27:27-31 ; Mc. 9:12 ; Jn. 16:32.-->, homme de douleur et sachant ce qu'est la maladie, et nous avons comme caché nos visages devant lui tant il était méprisé et nous ne l'avons pas estimé.
53:4	En vérité, il a porté nos maladies et il s'est chargé de nos douleurs<!--Mt. 8:17 ; 1 Pi. 2:24.-->. Et nous, nous avons estimé qu'il était frappé, battu par Elohîm et humilié.
53:5	Mais il était transpercé pour nos transgressions, brisé pour nos iniquités, le châtiment qui nous apporte la paix est tombé sur lui, et c'est par ses meurtrissures que nous avons la guérison.
53:6	Nous avons tous été errants comme des brebis<!--Petros (Pierre), apôtre de l'Agneau, confirme que le Mashiah est bel et bien le Bon Berger (1 Pi. 2:25).-->, nous nous sommes détournés, chacun suivait son propre chemin, et YHWH a fait venir sur lui l'iniquité de nous tous.
53:7	Opprimé et humilié, il n'a pas ouvert sa bouche<!--Mt. 26:62-63 ; Mc. 15:3-5 ; Jn. 19:9 ; Ac. 8:32-33.-->, semblable à un agneau qu'on mène à la boucherie, à une brebis muette devant celui qui la tond, et il n'a pas ouvert sa bouche.
53:8	Il a été enlevé sous la contrainte et sous le jugement. Qui racontera sa génération ? Car il a été retranché de la terre des vivants, et la plaie lui a été faite à cause des transgressions de mon peuple.
53:9	On lui a donné son sépulcre avec les méchants, et dans sa mort<!--Littéralement « dans ses morts ».-->, il a été avec le riche, quoiqu'il n'ait pas commis de violence, et qu'il n'y ait pas eu de fraude dans sa bouche<!--Mc. 15:28 ; Lu. 23:32-33.-->.
53:10	Toutefois il a plu à YHWH de l'écraser. Il l'a rendu malade. S'il donne son âme en sacrifice de culpabilité, il verra une postérité et prolongera ses jours, et le désir de YHWH prospérera entre ses mains<!--Jé. 23:5.-->.
53:11	Il jouira du travail de son âme et en sera rassasié. Mon serviteur, le juste, justifiera beaucoup de gens par la connaissance qu'ils auront de lui et lui-même portera leurs iniquités.
53:12	C'est pourquoi je lui donnerai une part avec les grands et il partagera le butin avec les puissants : parce qu'il a livré son âme à la mort et qu'il a été mis au rang des transgresseurs, parce qu'il a porté lui-même les péchés de beaucoup et qu'il a intercédé pour les transgresseurs.

## Chapitre 54

### YHWH réhabilite Israël la délaissée

54:1	Réjouis-toi avec chants de triomphe, stérile, toi qui n'enfantes pas, toi qui n'as pas connu les douleurs de l'accouchement ! Éclate de joie avec chant de triomphe et réjouis-toi ! Car les enfants de la délaissée seront plus nombreux que les enfants de celle qui est mariée, dit YHWH<!--Voir Ga. 4:27.-->.
54:2	Élargis l'espace de ta tente, et qu'on étende les couvertures de ton tabernacle : ne retiens rien ! Allonge tes cordages et affermis tes pieux !
54:3	Car tu te répandras à droite et à gauche, et ta postérité possédera les nations et peuplera les villes désertes.
54:4	N'aie pas peur, car tu ne seras pas couverte de honte. Ne sois pas confuse, car tu ne rougiras pas. Au contraire, tu oublieras la honte de ta jeunesse et tu ne te souviendras plus de l'opprobre de ton veuvage.
54:5	Car ton créateur est ton époux : YHWH Sabaoth<!--YHWH des armées.--> est son Nom<!--YHWH, l'Époux de Yeroushalaim (Jérusalem) n'est autre que Yéhoshoua (Jésus) qui épousera la ville sainte à la fin des temps. Voir Ap. 21:1-10.-->. Ton Rédempteur est le Saint d'Israël, il s'appelle l'Elohîm de toute la Terre.
54:6	Car YHWH t'appelle comme une femme délaissée et à l'esprit affligé, comme une femme qu'on a épousée dans la jeunesse, et qui a été répudiée, dit ton Elohîm.
54:7	Je t'avais délaissée pour un petit moment, mais je te rassemblerai avec de grandes compassions.
54:8	Dans une courte colère, je t'avais un moment caché ma face, mais j'aurai compassion de toi avec une bonté éternelle, dit YHWH, ton Rédempteur.
54:9	Car il en sera pour moi comme les eaux de Noah : de même que j'avais juré que les eaux de Noah ne se répandraient plus sur la Terre<!--Ge. 8:21, 9:11.-->, je jure de ne plus m'irriter contre toi, et de ne plus te menacer.
54:10	Quand les montagnes s'éloigneraient, quand les collines chancelleraient, ma bonté ne s'éloignera pas de toi, et mon alliance de paix ne chancellera pas, dit YHWH, qui a compassion de toi.
54:11	Ô affligée, agitée par la tempête, dénuée de consolation, voici, je garnirai tes pierres d'antimoine et je placerai tes fondements sur des saphirs ;
54:12	et je ferai tes créneaux<!--Littéralement « tes soleils ».--> d'agates, et tes portes en pierres d'escarboucle, et toute ton enceinte de pierres précieuses.
54:13	Aussi tous tes enfants seront disciples de YHWH, et grande sera la paix de tes fils.
54:14	Tu seras affermie par la justice. Tu seras loin de l'oppression et tu ne craindras rien ! Tu seras loin de la frayeur, car elle n'approchera pas de toi !
54:15	Voici, on formera des complots, on formera des complots contre toi, mais cela ne viendra pas de moi. Quiconque complotera contre toi tombera à cause de toi<!--Ps. 91:7 ; Ge. 37.-->.
54:16	Voici, c'est moi qui ai créé le forgeron soufflant le charbon au feu, et formant un instrument pour son travail, et j'ai créé aussi le destructeur pour détruire.
54:17	Aucune arme forgée contre toi ne réussira, et toute langue qui se lèvera en jugement contre toi, tu la condamneras<!--Ps. 23:4.-->. Tel est l'héritage des serviteurs de YHWH, et telle est la justice qui leur viendra de moi, dit YHWH.

## Chapitre 55

### Le salut par la grâce d'Elohîm

55:1	Ah ! vous tous qui avez soif, venez aux eaux, même vous qui n'avez pas d'argent ! Venez, achetez et mangez, venez, achetez du vin et du lait sans argent, et sans rien payer !
55:2	Pourquoi dépensez-vous de l'argent pour ce qui ne nourrit pas ? Pourquoi travaillez-vous pour ce qui ne rassasie pas<!--Ro. 14:17.--> ? Écoutez-moi, écoutez-moi et vous mangerez ce qui est bon, et votre âme se délectera de la graisse.
55:3	Inclinez l'oreille et venez à moi<!--Mt. 11:28.-->, écoutez et votre âme vivra ! Je traiterai avec vous une alliance éternelle, celle de la bonté fidèle envers David<!--Ac. 13:34.-->.
55:4	Voici, je l'ai donné comme témoin auprès des peuples, comme chef et dominateur des peuples.
55:5	Voici, tu appelleras des nations que tu ne connais pas, et les nations qui ne te connaissent pas accourront vers toi, à cause de YHWH, ton Elohîm, et du Saint d'Israël, qui t'aura glorifié.
55:6	Cherchez YHWH pendant qu'il se trouve, invoquez-le tandis qu'il est près.
55:7	Que le méchant abandonne sa voie, et l'homme injuste ses pensées ! Qu'il retourne à YHWH qui aura pitié de lui, à notre Elohîm qui pardonne abondamment<!--Jé. 18:11 ; Ez. 33:11 ; Jon. 3:10 ; 1 Ti. 2:1-4 ; 2 Pi. 3:9.-->.
55:8	Car mes pensées ne sont pas vos pensées, et mes voies ne sont pas vos voies, dit YHWH.
55:9	Mais autant les cieux sont élevés au-dessus de la Terre, autant mes voies sont élevées au-dessus de vos voies, et mes pensées au-dessus de vos pensées.
55:10	Car comme la pluie et la neige descendent des cieux et n'y retournent pas sans avoir arrosé la Terre, sans l'avoir fécondée et avoir fait germer ses plantes, sans avoir donné de la semence au semeur et du pain à celui qui mange<!--2 Co. 9:10.-->,
55:11	ainsi en est-il de ma parole qui sort de ma bouche : elle ne retourne pas vers moi sans effet<!--Ou à vide. Voir De. 16:16.-->, sans avoir accompli ce que je désire et amené au succès la chose pour laquelle je l'ai envoyée.
55:12	Car vous sortirez avec joie et vous serez conduits dans la paix. Les montagnes et les collines éclateront de joie avec chants de triomphe devant vous, et tous les arbres des champs battront des mains.
55:13	Au lieu de l'épine s'élèvera le cyprès, au lieu de la ronce croîtra le myrte, ce sera pour YHWH un nom, un signe éternel qui ne sera jamais retranché.

## Chapitre 56

### Exhortation à s'attacher à YHWH

56:1	Ainsi parle YHWH : Observez la justice, faites ce qui est juste, car mon salut ne tardera pas à venir, et ma justice à être révélée.
56:2	Béni est l'homme qui fait cela, et le fils de l'être humain qui s'y tient, observant le shabbat pour ne pas le profaner, et gardant ses mains pour ne faire aucun mal.
56:3	Et que l'enfant de l'étranger qui se joint à YHWH ne parle pas en disant : YHWH me séparera, il me séparera de son peuple ! Et que l'eunuque ne dise pas : Voici, je suis un arbre sec.
56:4	Car ainsi parle YHWH concernant les eunuques : Ceux qui garderont mes shabbats, et qui choisiront les choses auxquelles je prends plaisir, et qui s'attacheront à mon alliance,
56:5	je leur donnerai dans ma maison et dans mes murailles une place et un nom meilleur que le nom de fils ou de filles. Je leur donnerai à chacun un nom éternel qui ne périra jamais<!--Ap. 2:17.-->.
56:6	Et les enfants des étrangers qui se joindront à YHWH pour le servir, pour aimer le Nom de YHWH, pour être ses serviteurs, tous ceux qui garderont le shabbat pour ne pas le profaner et qui s'attacheront à mon alliance<!--Ex. 31:14.-->,
56:7	je les amènerai sur ma montagne sainte, et je les réjouirai dans ma maison de prière. Leurs holocaustes et leurs sacrifices seront agréés sur mon autel, car ma maison sera appelée Maison de prière pour tous les peuples<!--Mt. 21:13 ; Mc. 11:17 ; Lu. 19:46.-->.
56:8	Parole d'Adonaï YHWH, de celui qui rassemble les exilés d'Israël : J'en rassemblerai encore d'autres autour de lui avec ceux qui sont déjà rassemblés.
56:9	Bêtes des champs, bêtes des forêts, venez toutes pour manger !
56:10	Toutes ses sentinelles sont aveugles, elles ne connaissent rien. Ce sont tous des chiens muets, incapables d'aboyer. Ils rêvent, restent couchés, aiment à sommeiller.
56:11	Ce sont des chiens ayant un appétit féroce, ils ne connaissent pas la satiété. Ce sont des bergers qui ne comprennent rien. Tous se tournent vers leur propre voie, chacun vers son gain injuste, jusqu'au bout<!--Mt. 23:24 ; Tit. 1:7-11 ; 1 Pi. 5:2.--> :
56:12	Venez, je vais chercher du vin, et nous nous enivrerons de boissons fortes ! Demain sera comme aujourd'hui, il en reste encore beaucoup !

## Chapitre 57

### YHWH expose la fausseté et défend le juste

57:1	Le juste périt et personne ne le prend à cœur, les gens de bonté sont enlevés sans que personne ne comprenne que face au mal<!--Mi. 7:2 ; Ec. 7:15.--> le juste a été enlevé.
57:2	Il entrera en paix, il reposera sur sa couche, celui qui aura marché dans la droiture<!--Mt. 25:23 ; Lu. 19:17.-->.
57:3	Mais vous, approchez ici, enfants de l'enchanteresse, race de l'adultère et de la prostituée !
57:4	De qui vous êtes-vous moqués ? Contre qui avez-vous ouvert la bouche et tirez-vous la langue ? N'êtes-vous pas des enfants de la transgression, une race de mensonge,
57:5	s'échauffant près des faux elohîm, sous tout arbre vert, égorgeant les enfants dans les vallées, sous les fentes des rochers<!--Lé. 18:21 ; 1 R. 14:23 ; Jé. 2:20, 32:35.--> ?
57:6	C'est parmi les pierres polies des torrents qu'est ta portion. Ce sont elles, ce sont elles qui sont ton lot ! C'est à elles que tu verses des libations, que tu offres des offrandes. Puis-je être consolé de ces choses ?
57:7	Tu dresses ta couche sur les montagnes hautes et élevées. C'est aussi là que tu montes pour offrir des sacrifices.
57:8	Tu mets ton souvenir derrière la porte et les poteaux. Car loin de moi tu découvres ta couche, tu y montes et tu l'élargis. Tu fais alliance avec ceux dont tu aimes la couche, tout en contemplant le monument.
57:9	Tu voyages vers le roi avec de l'huile précieuse et tu ajoutes parfums sur parfums, tu envoies au loin tes messagers, tu t'abaisses jusqu'au shéol.
57:10	Tu te fatigues par la longueur du chemin, et tu ne dis pas : C'est sans espoir ! Tu trouves encore de la vigueur dans ta main, c'est pourquoi tu n'as pas été languissante.
57:11	De qui avais-tu peur et qui craignais-tu, pour que tu aies menti, et que tu ne te sois pas souvenue de moi et ne l'aies pas pris à cœur ? N'ai-je pas gardé le silence, et même depuis longtemps, et tu ne me crains pas.
57:12	Je vais révéler ta justice et tes œuvres, car elles ne te profiteront pas.
57:13	Quand tu cries, que ceux que tu as rassemblés te délivrent ! Mais le vent les emmènera tous, la vanité les enlèvera. Mais celui qui met sa confiance en moi héritera la terre et possédera ma montagne sainte<!--Es. 2:3 ; Ps. 2:6 ; Hé. 12:22.-->.
57:14	On dira : Frayez, frayez, préparez le chemin, enlevez tout obstacle loin du chemin de mon peuple !

### YHWH aime l'homme contrit

57:15	Car ainsi parle celui qui est haut et élevé, qui habite dans l'éternité et dont le Nom est le Saint<!--Voir commentaire en Ac. 3:14.--> : J'habiterai dans les lieux hauts et saints, avec l'homme contrit et humble d'esprit, pour donner la vie à l'esprit des humbles, pour donner la vie à ceux qui ont le cœur brisé<!--Ps. 34:19, 51:19.-->.
57:16	Car je ne contesterai pas sans fin, et je ne serai pas éternellement fâché, car devant moi défaillirait l'esprit, le souffle des êtres que j'ai faits<!--Mi. 7:18 ; Ps. 85:6, 103:9.-->.
57:17	À cause de l'iniquité de ses gains injustes, je me suis irrité et je l'ai frappé. Je me suis caché dans ma colère, et le rebelle a suivi la voie de son cœur.
57:18	J'ai vu ses voies, et toutefois je le guérirai. Je le conduirai et je le comblerai de consolations, lui et ceux qui mènent deuil avec lui.
57:19	Je crée les fruits des lèvres. Paix, paix à celui qui est loin et à celui qui est près ! dit YHWH, car je le guérirai.
57:20	Mais les méchants sont comme la mer agitée, quand elle ne peut se calmer, et dont les eaux rejettent la boue et le bourbier.
57:21	Il n'y a pas de paix pour les méchants, dit mon Elohîm.

## Chapitre 58

### Le vrai et le faux jeûne

58:1	Crie à plein gosier, ne te retiens pas ! Élève ta voix comme un shofar, et annonce à mon peuple ses transgressions et à la maison de Yaacov ses péchés !
58:2	Car ils me cherchent tous les jours, ils prennent plaisir à connaître mes voies. Comme une nation qui aurait pratiqué la justice, et qui n'aurait pas abandonné les ordonnances de son Elohîm, ils me demandent des jugements justes, ils prennent plaisir à s'approcher d'Elohîm, et puis ils disent :
58:3	Pourquoi jeûnons-nous ? Tu ne le vois pas ! Pourquoi affligeons-nous nos âmes ? Tu ne le sais pas ! Voici, le jour de votre jeûne, vous trouvez votre plaisir, et vous oppressez tous vos travailleurs.
58:4	Voici, vous jeûnez pour vous livrer aux querelles et aux disputes, et pour frapper du poing avec méchanceté. Vous ne jeûnez pas comme le veut ce jour, pour que votre voix soit exaucée d'en haut.
58:5	Est-ce là le jeûne que j'ai choisi, que l'être humain afflige son âme un jour ? Est-ce en courbant sa tête comme le jonc et en étendant le sac et la cendre ? Appelleras-tu cela un jeûne et un jour agréable à YHWH ?
58:6	N'est-ce pas plutôt ceci le jeûne que j'ai choisi : que tu détaches les liens de la méchanceté, que tu délies les cordages du joug, que tu laisses aller libres les opprimés, et que l'on rompe toute espèce de joug ?
58:7	N'est-ce pas que tu partages ton pain avec celui qui a faim ? Et que tu fasses venir dans ta maison les affligés errants ? Quand tu vois un homme nu, que tu le couvres, et que tu ne te caches pas de ta propre chair ?

### Bénédiction pour ceux qui pratiquent le bien

58:8	Alors ta lumière éclatera comme l'aurore et ta guérison germera rapidement, ta justice marchera devant toi, et la gloire de YHWH sera ton arrière-garde.
58:9	Alors tu prieras, et YHWH t'exaucera, tu crieras au secours, et il dira : Me voici ! Si tu ôtes du milieu de toi le joug, si tu cesses de lever le doigt et de dire des méchancetés,
58:10	si tu ouvres ton âme à celui qui a faim, si tu rassasies l'âme affligée, ta lumière se lèvera sur les ténèbres et l'obscurité sera comme le midi.
58:11	Et YHWH te conduira continuellement, il rassasiera ton âme dans les grandes sécheresses, il fortifiera tes os, et tu seras comme un jardin arrosé, et comme une source d'eaux dont les eaux ne tarissent pas<!--Jn. 4:14 ; Ap. 21:6.-->.
58:12	Et ceux qui sortiront de toi rebâtiront les lieux déserts depuis longtemps, tu rétabliras les fondements ruinés depuis plusieurs générations. On t'appellera le réparateur des brèches, le restaurateur des chemins pour qu'on y habite.
58:13	Si tu détournes ton pied pendant le shabbat pour ne pas faire ton plaisir en mon saint jour, si tu appelles le shabbat tes délices, et honorable ce qui est saint à YHWH, et si tu l'honores au lieu d'agir selon tes voies, de trouver ce qui te fait plaisir et de proférer une parole,
58:14	alors tu prendras plaisir en YHWH. Je te ferai monter sur les hauteurs du pays et te nourrirai de l'héritage de Yaacov, ton père. Oui, la bouche de YHWH a parlé.

## Chapitre 59

### Le péché sépare de YHWH

59:1	Voici, la main de YHWH n'est pas trop courte pour sauver, ni son oreille trop pesante pour entendre.
59:2	Mais ce sont vos iniquités qui mettent une séparation entre vous et votre Elohîm, ce sont vos péchés qui vous cachent sa face, afin qu'il ne vous entende pas<!--De. 31:17-18 ; Ez. 39:23-24.-->.
59:3	Car vos mains sont souillées de sang, et vos doigts d'iniquité, vos lèvres profèrent le mensonge, et votre langue déclare l'injustice.
59:4	Personne ne crie pour la justice, personne ne plaide pour la vérité. Ils s'appuient sur des choses vaines et disent des faussetés, ils conçoivent le mal et enfantent l'iniquité.
59:5	Ils font éclore des œufs de vipère, et ils tissent des toiles d'araignée. Celui qui mange de leurs œufs meurt et, si l'on en écrase un, il en sort une vipère.
59:6	Leurs toiles ne servent pas à faire des vêtements, et on ne peut se couvrir de leurs œuvres ; leurs œuvres sont des œuvres de méchanceté, et des actes de violence sont dans leurs mains.
59:7	Leurs pieds courent au mal et se hâtent pour répandre le sang innocent. Leurs pensées sont des pensées d'iniquité, le ravage et la ruine sont sur leurs voies.
59:8	Ils ne connaissent pas le chemin de la paix, et il n'y a pas de justice dans leurs voies, ils se sont pervertis dans leurs sentiers, tous ceux qui y marchent ignorent la paix<!--Pr. 1:16, 6:16-19.-->.
59:9	C'est pourquoi le jugement s'est éloigné de nous, et la justice ne parvient pas jusqu'à nous. Nous attendions la lumière et voici les ténèbres, la clarté et nous marchons dans l'obscurité.
59:10	Nous tâtonnons comme des aveugles le long du mur, nous tâtonnons comme ceux qui sont sans yeux. Nous chancelons en plein midi comme la nuit. Tout en étant pleins de vigueur, nous sommes semblables à des morts.
59:11	Nous rugissons tous comme des ours, nous gémissons, nous gémissons comme des colombes. Nous attendions le jugement, et il n'y en a pas, la délivrance, et elle est éloignée de nous.
59:12	Car nos transgressions se sont multipliées devant toi et nos péchés témoignent contre nous. Car nos transgressions sont avec nous et nous connaissons nos iniquités :
59:13	se rebeller et renier YHWH, se détourner de notre Elohîm, parler d'oppression et d'apostasie, concevoir et méditer dans le cœur des paroles mensongères.
59:14	C'est pourquoi le jugement s'est éloigné et la justice se tient éloignée. Car la vérité trébuche sur la place publique, et la droiture ne peut y entrer.
59:15	La vérité a disparu<!--Je. 7:28.--> et quiconque se détourne du mal se fait dépouiller. YHWH l'a vu, et cela a déplu à ses yeux parce qu'il n'y a plus de droiture.

### YHWH cherche un homme, il suscite le Mashiah

59:16	Il voit aussi qu'il n'y a aucun homme, il s'étonne que personne ne se tienne à la brèche, c'est pourquoi son bras lui vient en aide, et sa propre justice lui sert d'appui<!--Es. 53:1, 63:5 ; Ps. 77:15-16 ; Ac. 13:17.-->.
59:17	Car il se revêt de la justice comme d'une cuirasse, et le casque du salut est sur sa tête<!--Ep. 6:14-17.-->. Il se revêt de la vengeance comme d'un vêtement, et se couvre de la jalousie comme d'une robe.
59:18	Selon leurs actes, il rendra à chacun la pareille<!--Jé. 17:10 ; Job 34:11 ; Mt. 16:27 ; Ap. 2:23, 20:13.-->, la fureur à ses adversaires, la rétribution à ses ennemis ; il rendra ainsi la rétribution aux îles.
59:19	Et l'on craindra le Nom de YHWH depuis l'occident et sa gloire depuis le soleil levant. Quand l'ennemi viendra comme un fleuve, l'Esprit de YHWH le mettra en fuite.
59:20	Et le Rédempteur<!--Le Rédempteur qui viendra pour Sion est le Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) (Ro. 11:26). Voir aussi Es. 60:16.--> viendra en Sion, et vers ceux de Yaacov qui se convertiront de leur transgression, dit YHWH.
59:21	Et quant à moi, voici mon alliance que je ferai avec eux, dit YHWH : Mon Esprit qui est sur toi, et mes paroles que j'ai mises dans ta bouche, ne se retireront pas de ta bouche, ni de la bouche de ta postérité, ni de la bouche de la postérité de ta postérité, dit YHWH, dès maintenant et à jamais.

## Chapitre 60

### La gloire de YHWH se lèvera sur Sion

60:1	Lève-toi, sois illuminée, car ta lumière arrive, et la gloire de YHWH se lève sur toi.
60:2	Car voici, l'obscurité couvre la Terre et les ténèbres épaisses couvrent les peuples, mais sur toi YHWH se lève, sur toi sa gloire apparaît.
60:3	Des nations marchent à ta lumière, et des rois à la splendeur qui se lève sur toi<!--Ap. 21:24.-->.
60:4	Lève tes yeux alentour et regarde : ils se rassemblent tous, ils viennent vers toi ; tes fils viennent de loin, et tes filles sont nourries par des nourriciers, étant portées sur les côtés.
60:5	Alors tu verras et tu seras rayonnante, ton cœur tressaillira et se dilatera, quand l'abondance de la mer se tournera vers toi et que les richesses des nations viendront chez toi.
60:6	Tu seras couverte d'une foule de chameaux, des dromadaires de Madian et d'Éphah. Tous ceux de Séba viendront, ils apporteront de l'or et de l'encens, et publieront les louanges de YHWH.
60:7	Les brebis de Kédar se réuniront toutes chez toi, les béliers de Nebayoth seront à ton service, ils monteront sur mon autel et me seront agréables, et je rendrai magnifique la maison de ma gloire.
60:8	Qui sont ceux-là qui volent comme des nuées, comme des colombes vers leur colombier ?
60:9	Car les îles s'attendent à moi, et les navires de Tarsis les premiers, afin d'amener de loin tes enfants, avec leur argent et leur or, à cause du Nom de YHWH, ton Elohîm, et du Saint d'Israël qui te glorifie.
60:10	Les fils des étrangers rebâtiront tes murailles, et leurs rois seront employés à ton service, car je t'ai frappée dans ma colère, mais j'ai eu pitié de toi au temps de mon bon plaisir.
60:11	Tes portes seront continuellement ouvertes, elles ne seront fermées ni nuit ni jour, pour qu'on emmène chez toi les richesses des nations et qu'on introduise leurs rois<!--Ap. 21:25-26.-->.
60:12	Car la nation et le royaume qui ne te serviront pas périront, et ces nations-là seront en ruine, elles seront en ruine.
60:13	La gloire du Liban viendra vers toi, le cyprès, l'orme, et le buis, tous ensemble pour rendre honorable le lieu de mon sanctuaire, et je rendrai glorieux le lieu de mes pieds.
60:14	Mais les enfants de tes oppresseurs viendront vers toi en se courbant, et tous ceux qui te méprisaient se prosterneront à la plante de tes pieds et t'appelleront la ville de YHWH, la Sion du Saint d'Israël.
60:15	Au lieu d'avoir été délaissée et haïe, si bien que personne ne passait par toi, je te mettrai dans une élévation éternelle et dans une joie qui sera de génération en génération.
60:16	Et tu suceras le lait des nations, et tu suceras la mamelle des rois, et tu sauras que je suis YHWH, ton Sauveur, ton Rédempteur<!--Le verbe « ga'al » et le nom correspondant « go'el » ont été traduits respectivement en français par « racheter » et « rédempteur ». Selon la torah de Moshè (Moïse), si quelqu'un perdait son héritage à cause d'une dette ou s'il se vendait comme esclave, lui et ses biens pouvaient être rachetés par un proche parent qui devait payer le prix de la rédemption (Lé. 25:23-55). YHWH se présente comme le Rédempteur par excellence (Es. 49:26 ; Ps. 78:35, 130:7 ; Job 19:25).-->, le Puissant de Yaacov.
60:17	Au lieu du cuivre je ferai venir de l'or, au lieu du fer je ferai venir de l'argent, au lieu du bois, du cuivre, et au lieu des pierres, du fer. Je ferai régner la paix et dominer la justice.
60:18	On n'entendra plus parler de violence dans ton pays ni de ravage et de ruine dans ton territoire. Mais tu appelleras tes murailles : Salut et tes portes : Louange.
60:19	Tu n'auras plus le soleil pour la lumière du jour, et la lueur de la lune ne t'éclairera plus, mais YHWH sera pour toi la lumière éternelle<!--Voir le commentaire en Ge. 1:3.-->, et ton Elohîm sera ta gloire.
60:20	Ton soleil ne se couchera plus, et ta lune ne se retirera plus, car YHWH sera pour toi la lumière éternelle, et les jours de ton deuil seront finis.
60:21	Quant à ton peuple, ils seront tous justes, ils posséderont la terre à toujours, le germe de mes plantes, l'œuvre de mes mains pour y être glorifié<!--Es. 11:1 ; Ro. 15:12 ; Ap. 5:5, 22:16.-->.
60:22	Le petit deviendra un millier, et le moindre deviendra une nation puissante. Je suis YHWH, je hâterai ces choses en leur temps.

## Chapitre 61

### La mission du Mashiah

61:1	L'Esprit d'Adonaï YHWH est sur moi, car YHWH m'a oint pour évangéliser les malheureux. Il m'a envoyé pour guérir ceux qui ont le cœur brisé, pour proclamer aux captifs la liberté, et aux prisonniers l'ouverture de la prison,
61:2	pour publier une année de grâce de YHWH, et le jour de vengeance de notre Elohîm, pour consoler tous ceux qui mènent deuil<!--Lu. 4:14-19.-->,
61:3	pour mettre et donner à ceux de Sion qui mènent deuil un turban au lieu de la cendre, une huile de joie au lieu du deuil, un manteau de louange au lieu d'un esprit abattu<!--Job 29:14 ; Ja. 1:12 ; 1 Co.9:25 ; 2 Ti. 4:8.-->, afin qu'on les appelle les térébinthes de la justice, la plantation de YHWH pour se glorifier.
61:4	Ils rebâtiront les ruines antiques, ils relèveront les désolations du passé, ils renouvelleront les villes ravagées, les lieux dévastés d'âge en âge.
61:5	Et des étrangers s'y tiendront là et feront paître vos troupeaux, et les enfants de l'étranger seront vos laboureurs et vos vignerons.
61:6	Mais vous, vous serez appelés prêtres de YHWH, on vous dira serviteurs de notre Elohîm<!--Ap. 1:6, 5:10.-->. Vous mangerez les richesses des nations et vous vous glorifierez de leur gloire.
61:7	Au lieu de votre honte vous aurez le double. Au lieu de la confusion, ils pousseront des cris de joie en voyant leur portion. C'est ainsi qu'ils posséderont le double dans leur pays et leur joie sera éternelle.
61:8	Car je suis YHWH qui aime la justice, qui hait la rapine et l'iniquité. J'établirai leur œuvre dans la vérité et je traiterai avec eux une alliance éternelle.
61:9	Et leur race sera connue parmi les nations, et ceux qui seront sortis d'eux seront connus parmi les peuples. Tous ceux qui les verront reconnaîtront qu'ils sont la race que YHWH aura bénie.
61:10	Je me réjouirai, je me réjouirai en YHWH, et mon âme sera joyeuse en mon Elohîm, car il m'a revêtu des vêtements du salut, il m'a couvert de la robe de la justice, comme un époux se pare de son turban, comme une épouse s’orne de ses joyaux<!--Os. 2:21-22 ; Ap. 19:7-8.-->.
61:11	Car comme la terre fait éclore son germe, et comme un jardin fait germer ses semences, ainsi Adonaï YHWH fera germer la justice, et la louange en présence de toutes les nations.

## Chapitre 62

### YHWH proclame la restauration d'Israël

62:1	Pour l'amour de Sion, je ne garderai pas le silence, et pour l'amour de Yeroushalaim je ne prendrai pas de repos, jusqu'à ce que sa justice sorte dehors comme une splendeur, et que sa délivrance ne soit allumée comme une lampe.
62:2	Alors les nations verront ta justice, et tous les rois ta gloire, et l'on t'appellera d'un nouveau nom<!--Ap. 2:17.-->, que la bouche de YHWH aura expressément déclaré.
62:3	Tu seras une couronne de gloire dans la main de YHWH, un turban, un turban royal dans la main de ton Elohîm.
62:4	On ne t'appellera plus Délaissée, on n'appellera plus ta terre Dévastation, mais on t'appellera Mon plaisir est en elle, et on appellera ta terre Mariée, car YHWH prend plaisir en toi et ta terre aura un mari.
62:5	Car comme un jeune homme se marie à une vierge, ainsi tes fils se marieront chez toi. Et comme la mariée fait la joie du marié, ainsi tu feras la joie de ton Elohîm.
62:6	Yeroushalaim, j'ai placé des gardes sur tes murailles, tout le jour et toute la nuit, continuellement, ils ne resteront pas silencieux. Vous qui faites mention de YHWH, ne gardez pas le silence !
62:7	Et ne vous arrêtez pas de l'invoquer jusqu'à ce qu'il rétablisse Yeroushalaim et lui rende sa renommée sur la Terre.
62:8	YHWH l'a juré par sa droite et par son bras puissant : Je ne donnerai plus ton blé pour nourriture à tes ennemis, et les enfants des étrangers ne boiront plus ton vin excellent pour lequel tu as travaillé.
62:9	Mais ceux qui auront recueilli le blé le mangeront et loueront YHWH, et ceux qui auront récolté le vin le boiront dans les parvis de mon lieu saint.
62:10	Passez, passez les portes ! Préparez le chemin du peuple ! Frayez, frayez la route, et ôtez-en les pierres ! Élevez une bannière vers les peuples.
62:11	Voici ce que YHWH proclame aux extrémités de la Terre : Dites à la fille de Sion : Voici, ton Sauveur vient<!--De nombreux passages, notamment dans le livre d'Ésaïe (Yesha`yah), présentent Elohîm comme le seul Sauveur (Es. 43:3,11 ; Os. 13:4) qui viendra pour délivrer son peuple (Es. 35:4, 60:1 ; Za. 14:1-7). Yéhoshoua ha Mashiah (Jésus-Christ) a accompli en tous points les prophéties relatives à la venue de YHWH. Elohîm est bel et bien venu sur terre il y a plus de 2000 ans et ce même Elohîm revient bientôt (Ac. 1:11 ; Ap. 1:7).-->, voici, son salaire est avec lui et sa récompense<!--Ge. 15:1 ; Es. 40:10, 49:4.--> marche devant lui.
62:12	Et on les appellera Peuple saint, Rachetés de YHWH<!--1 Pi. 2:9 ; Ap. 5:9.-->. Et toi, on t'appellera Cherchée, Ville non abandonnée.

## Chapitre 63

### Le jour de vengeance du Mashiah<!--Es. 2:10-22 ; Ap. 19:11-21.-->

63:1	Qui est celui-ci qui vient d'Édom, de Botsrah, en vêtements rouges, magnifiquement paré dans ses habits, marchant selon la grandeur de sa force ? C'est moi qui parle avec justice, et qui suis Grand pour sauver.
63:2	Pourquoi tes vêtements sont-ils rouges, et pourquoi tes habits sont comme les habits de ceux qui foulent dans la cuve ?
63:3	J'ai été seul à fouler au pressoir, et parmi les peuples, aucun homme n'était avec moi. Cependant, j'ai marché sur eux dans ma colère, et je les ai foulés dans ma fureur ; et leur sang a rejailli sur mes vêtements, et j'ai souillé tous mes habits.
63:4	Car le jour de la vengeance était dans mon cœur, et l'année de mes rachetés est venue.
63:5	Je regardais, mais il n'y avait personne pour m'aider. J'étais étonné, mais il n'y avait personne pour me soutenir. Alors mon bras m'a sauvé et ma fureur m'a soutenu.
63:6	Ainsi j'ai foulé des peuples dans ma colère, je les ai enivrés dans ma fureur et j'ai abattu leur force par terre.

### Yesha`yah confesse les péchés du peuple

63:7	Je rappellerai les bontés de YHWH, les louanges de YHWH, pour tous les bienfaits<!--« Traiter », « récompenser », « sevrer un enfant », « être sevré », « mûrir », « amener à maturité ».--> que YHWH nous a faits et pour l'abondance des biens qu'il a faits à la maison d'Israël, dans ses compassions et dans la grandeur de ses bontés.
63:8	Car il a dit : Certainement, ils sont mon peuple, des enfants qui ne tricheront pas ! Et il a été pour eux un Sauveur.
63:9	Et dans toutes leurs détresses, il a été en détresse, et l'Ange de sa face les a sauvés<!--Ge. 16:7-10 ; Jg. 6:11-14 ; Za. 1:11.-->. C'est lui-même qui les a rachetés dans son amour et sa miséricorde, et il les a soutenus et portés, tous les jours d'autrefois.
63:10	Mais ils ont été rebelles, et ils ont attristé son Esprit saint<!--Ep. 4:30.-->, c'est pourquoi il est devenu leur ennemi, et il a lui-même combattu contre eux.
63:11	Alors son peuple se souvint des anciens jours de Moshè : Où est celui qui les fit monter de la mer avec le berger de son troupeau ? Où est celui qui mit au milieu d'eux son Esprit saint ;
63:12	qui les dirigea par la droite de Moshè et par son bras glorieux, qui fendit les eaux devant eux pour se faire un nom éternel,
63:13	qui les dirigea à travers les abîmes, comme un cheval dans le désert, sans qu'ils ne bronchent ?
63:14	L'Esprit de YHWH les a menés au repos comme on mène une bête qui descend dans la vallée. C'est ainsi que tu as conduit ton peuple, afin de t'acquérir un nom glorieux.
63:15	Regarde du ciel et vois de ta demeure sainte et glorieuse : où sont ton zèle et ta puissance ? Le son de tes entrailles et tes compassions pour moi ont-ils été contenus ?
63:16	Car tu es notre Père ! Car Abraham ne nous connaît pas, et Israël ignore qui nous sommes. YHWH, c'est toi qui es notre Père, et ton Nom est notre Rédempteur de tout temps.
63:17	Pourquoi nous as-tu fait égarer loin de tes voies, ô YHWH, et endurcis-tu notre cœur pour ne pas te craindre ? Reviens, pour l'amour de tes serviteurs, des tribus de ton héritage !
63:18	Ton peuple saint n'a possédé le pays que peu de temps, nos ennemis ont foulé ton sanctuaire.
63:19	Nous sommes comme ceux que tu ne gouvernes pas depuis longtemps, et sur qui ton Nom n'est pas proclamé. Ô ! Si tu fendais les cieux, et si tu descendais, les montagnes s'ébranleraient devant toi !

## Chapitre 64

### Prière du reste d'Israël à YHWH pour sa délivrance

64:1	Comme le feu allume les broussailles, comme le feu fait bouillir l'eau, tu ferais connaître ton Nom à tes ennemis de sorte que les nations tremblent en ta présence.
64:2	Lorsque tu fis les choses redoutables que nous n'attendions pas, tu descendis et les montagnes tremblèrent devant toi.
64:3	Jamais on n'a appris ni entendu dire, et jamais l'œil n'a vu un autre elohîm que toi agir de cette manière pour ceux qui s'attendent à lui<!--1 Co. 2:9.-->.
64:4	Tu viens à la rencontre de celui qui se réjouit en pratiquant la justice, de ceux qui marchent dans tes voies et qui se souviennent de toi. Voici tu t'es fâché parce que nous avons péché, tes compassions sont éternelles, c'est pourquoi nous serons sauvés.
64:5	Or nous sommes tous devenus comme une chose souillée, et toute notre justice est comme le linge le plus souillé<!--Ap. 19:8.-->. Nous sommes tous flétris comme la feuille et nos iniquités nous emportent comme le vent.
64:6	Il n'y a personne qui invoque ton Nom, qui se réveille pour s'attacher fortement à toi. C'est pourquoi tu nous as caché ta face, et tu nous fais fondre par l'effet de nos iniquités.
64:7	Cependant, ô YHWH, tu es notre Père. Nous sommes l'argile, et c'est toi qui nous as formés, et nous sommes tous l'ouvrage de ta main<!--Es. 29:16, 45:9 ; Jé. 18:6 ; Job 10:9 ; Ro. 9:20-21.-->.
64:8	Ne t'irrite pas à l'extrême, ô YHWH, et ne te souviens pas à toujours de notre iniquité ! Voici, regarde, nous te prions, nous sommes tous ton peuple.
64:9	Tes villes saintes sont devenues un désert. Sion est devenue un désert, et Yeroushalaim une désolation.
64:10	Notre maison sainte et glorieuse, où nos pères te louaient, a été brûlée par le feu, tout ce que nous avions de précieux a été dévasté.
64:11	Après cela, ô YHWH, ne te retiendras-tu pas ? Ne cesseras-tu pas, et nous affligeras-tu à l'excès ?

## Chapitre 65

### Réponse de YHWH

65:1	Je me suis laissé consulter par ceux qui ne me demandaient rien, et je me suis laissé trouver par ceux qui ne me cherchaient pas<!--Mt. 7:7 ; Lu. 11:9.--> ; j'ai dit à la nation qui ne s'appelait pas de mon Nom : Me voici, me voici !
65:2	J'ai tendu mes mains tous les jours vers un peuple rebelle, à celui qui marche dans une mauvaise voie, au gré de ses pensées.
65:3	Vers un peuple qui m'irrite continuellement en face, qui sacrifie dans les jardins, et qui brûle de l'encens sur les autels de briques,
65:4	qui habite dans les sépulcres et passe la nuit dans les cabanes de gardes, qui mange la chair de porc, et ayant dans ses vases le jus des choses abominables ;
65:5	qui dit : Retire-toi, ne m'approche pas, car je suis plus saint que toi ! Ceux-là sont une fumée dans mes narines, un feu ardent tout le jour.
65:6	Voici, cela est écrit devant moi, je ne me tairai pas, mais je leur ferai payer, je leur ferai payer dans leur sein,
65:7	leurs iniquités et les iniquités de leurs pères ensemble, dit YHWH, eux qui ont brûlé de l'encens sur les montagnes et qui m'ont blasphémé sur les collines. C'est pourquoi je mesurerai dans leur sein le salaire de leurs œuvres passées.
65:8	Ainsi parle YHWH : Quand on trouve du vin dans une grappe, on dit : Ne la détruis pas, car il y a là une bénédiction ! J'agirai de la même manière à cause de mes serviteurs, afin de ne pas tous les détruire.
65:9	Je ferai sortir de Yaacov une postérité, et de Yéhouda celui qui prendra possession de mes montagnes : mes élus en prendront possession, mes serviteurs y habiteront.
65:10	Et Saron servira de pâturage au menu bétail, et la vallée d'Acor sera le gîte du gros bétail, pour mon peuple qui m'aura recherché.
65:11	Mais vous, qui abandonnez YHWH et qui oubliez ma montagne sainte, qui dressez la table pour Gad<!--Gad : dieu de la fortune.-->, et qui remplissez une coupe pour Meni<!--Meni : divinité païenne assimilée à la lune et dont le nom signifie « destin, sort » ou « fortune ».-->,
65:12	je vous destine aussi à l'épée, et vous serez tous courbés pour être égorgés. Car j'ai appelé et vous n'avez pas répondu, j'ai parlé et vous n'avez pas écouté. Mais vous avez fait ce qui me déplaît, et vous avez choisi les choses auxquelles je ne prends pas plaisir.
65:13	C'est pourquoi, ainsi parle Adonaï YHWH : Voici, mes serviteurs mangeront, et vous aurez faim. Voici, mes serviteurs boiront, et vous aurez soif. Voici, mes serviteurs se réjouiront, et vous serez honteux.
65:14	Voici, mes serviteurs pousseront des cris de joie à cause du bonheur de leur cœur, mais vous, vous crierez à cause de la douleur de votre cœur et vous hurlerez à cause du brisement de votre esprit.
65:15	Et vous laisserez votre nom à mes élus comme une malédiction. Adonaï YHWH vous fera mourir, mais il donnera à ses serviteurs un autre nom.
65:16	Celui qui se bénira sur la Terre, se bénira par l'Elohîm de l'Amen, et celui qui jurera sur la Terre jurera par l'Elohîm de l'Amen. Car les détresses du passé seront oubliées, et même elles seront cachées à mes yeux.

### De nouveaux cieux et une nouvelle terre

65:17	Car voici, je vais créer de nouveaux cieux et une nouvelle Terre<!--Es. 66:22 ; 2 Pi. 3:13 ; Ap. 21:1.-->. On ne se souviendra plus des choses précédentes, elles ne reviendront plus au cœur.
65:18	Réjouissez-vous plutôt et soyez à toujours dans l'allégresse, à cause de ce que je vais créer, car voici je vais créer Yeroushalaim pour la joie et son peuple pour l'allégresse.
65:19	Je ferai de Yeroushalaim mon allégresse, et de mon peuple ma joie. On n'y entendra plus le bruit des pleurs et le bruit des clameurs.
65:20	Il n'y aura plus désormais ni nourrisson ni vieillard qui n'accomplisse leurs jours. En effet, un enfant qui mourra à 100 ans sera jeune et un enfant de 100 ans qui pèche sera maudit.
65:21	Ils bâtiront des maisons et les habiteront. Ils planteront des vignes et ils en mangeront le fruit.
65:22	Ils ne bâtiront pas des maisons pour qu'un autre les habite, ils ne planteront pas des vignes pour qu'un autre en mange le fruit, car les jours de mon peuple seront comme les jours des arbres, et mes élus jouiront de l'œuvre de leurs mains.
65:23	Ils ne travailleront plus en vain, et ils n'engendreront pas pour une fin soudaine. Car ils seront la postérité des bénis de YHWH, et leur descendance avec eux.
65:24	Et il arrivera qu'avant qu'ils n'appellent, je leur répondrai ; ils parleront encore que je les aurai déjà entendus.
65:25	Le loup et l'agneau paîtront ensemble, le lion comme le bœuf mangeront de la paille, et la poussière sera la nourriture du serpent<!--Es. 2:4, 11:6-7.-->. On ne fera plus de mal et on ne détruira plus sur toute ma montagne sainte, dit YHWH.

## Chapitre 66

### YHWH réprouve l'hypocrisie et agrée ceux qui le craignent

66:1	Ainsi parle YHWH : Le ciel est mon trône, et la Terre est le marchepied de mes pieds<!--Mt. 5:34-35 ; Ac. 7:49.-->. Quelle maison me bâtiriez-vous, et quel serait le lieu de mon repos ?
66:2	Car ma main a fait toutes ces choses, et c'est par moi que toutes ces choses ont eu leur existence, dit YHWH. Mais à qui regarderai-je ? À celui qui est affligé, qui a l'esprit abattu, et qui tremble à ma parole.
66:3	Celui qui tue un bœuf tue un homme, celui qui sacrifie une brebis brise la nuque à un chien, celui qui présente une offrande de grain offre du sang de porc, celui qui fait un mémorial d'encens bénit les idoles : tous ceux-là ont choisi leurs voies, et leur âme trouve du plaisir dans leurs abominations.
66:4	Moi aussi je ferai attention à leurs tromperies, et je ferai venir sur eux les choses qu'ils craignent, parce que j'ai appelé, et personne n'a répondu, parce que j'ai parlé, et qu'ils n'ont pas écouté. Mais ils ont fait ce qui est mal à mes yeux et ils ont choisi les choses auxquelles je ne prends pas de plaisir.
66:5	Écoutez la parole de YHWH, vous qui tremblez à sa parole. Vos frères, qui vous haïssent et qui vous repoussent comme une chose abominable, à cause de mon Nom disent : Que YHWH montre sa gloire et que nous voyions votre joie ! Cependant, c'est eux qui seront couverts de honte.
66:6	Un son éclatant sort de la ville, un son sort du temple, le son de YHWH, qui rend à ses ennemis selon leurs œuvres.

### Israël renaît en un jour

66:7	Elle a enfanté, avant d'éprouver les douleurs de l'accouchement, elle a donné naissance à un enfant mâle, avant que les douleurs de l'enfantement ne lui viennent.
66:8	Qui a déjà entendu une telle chose ? Qui a déjà vu quelque chose de semblable ? Un pays peut-il naître en un jour ? Ou une nation peut-elle naître d'un seul coup<!--Cette prophétie fait allusion à la création de l'État d'Israël le 14 mai 1948.--> ? Pourtant dès que Sion a été en travail, elle a enfanté ses enfants !
66:9	Moi qui fais enfanter les autres, ne ferais-je pas enfanter Sion ? dit YHWH. Moi qui donne de la postérité aux autres, l'empêcherais-je d'enfanter ? dit ton Elohîm.

### Réjouissance à Yeroushalaim (Jérusalem) et consolation

66:10	Réjouissez-vous avec Yeroushalaim, faites d'elle le sujet de votre allégresse, vous tous qui l'aimez ! Vous tous qui menez deuil sur elle, réjouissez-vous avec elle d'une grande joie !
66:11	Afin que vous soyez allaités et rassasiés par le sein de ses consolations, afin que vous suciez le lait et que vous vous délectiez de la plénitude de sa gloire.
66:12	Car ainsi parle YHWH : Voici, je ferai couler vers elle la paix comme un fleuve, et la gloire des nations comme un torrent qui déborde, et vous serez allaités, vous serez portés sur les côtés et caressés sur les genoux.
66:13	Je vous consolerai pour vous apaiser, comme un homme que sa mère caresse pour l'apaiser, vous serez consolés dans Yeroushalaim.

66:14	Vous le verrez et votre cœur se réjouira, et vos os germeront comme l'herbe. YHWH fera connaître sa force à ses serviteurs, mais il sera indigné contre ses ennemis.

### Jugement de YHWH

66:15	Car voici, YHWH viendra avec le feu<!--Voir Ap. 19:12.-->, et ses chars seront comme la tempête, afin de tourner sa colère en fureur, et sa menace en flamme de feu.
66:16	Car YHWH exercera ses jugements contre toute chair par le feu et avec son épée, et le nombre de ceux qui seront mis à mort par YHWH sera grand.
66:17	Ceux qui se sanctifient et se purifient au milieu des jardins, l'un après l'autre, qui mangent de la chair de porc et des choses abominables, comme des souris, seront ensemble consumés, dit YHWH.
66:18	Mais pour moi, voyant leurs œuvres et leurs pensées, le temps est venu de rassembler toutes les nations et les langues. Elles viendront et verront ma gloire.

### Toutes les nations adoreront YHWH

66:19	Car je mettrai un signe en eux et j'enverrai certains de leurs rescapés vers les nations, à Tarsis, à Poul, à Loud, gens tirant de l'arc, à Toubal et à Yavan, et vers les îles lointaines, qui n'ont pas entendu de rumeur à mon sujet et qui n'ont pas vu ma gloire. Ils raconteront ma gloire parmi les nations.
66:20	Et ils amèneront tous vos frères d'entre toutes les nations, sur des chevaux, sur des chars et dans des litières, sur des mulets et sur des dromadaires, en offrande à YHWH, à la montagne sainte, à Yeroushalaim, dit YHWH, comme lorsque les enfants d'Israël apportent l'offrande dans un vase pur, à la maison de YHWH.
66:21	Et même je prendrai aussi parmi eux des prêtres, des Lévites, dit YHWH.
66:22	Car comme les nouveaux cieux et la nouvelle Terre que je vais faire subsisteront devant moi, dit YHWH, ainsi subsistera votre postérité et votre nom.
66:23	Et il arrivera que de nouvelle lune en nouvelle lune, et de shabbat en shabbat, toute chair viendra se prosterner devant ma face, dit YHWH.
66:24	Et quand ils sortiront dehors, ils verront les cadavres des hommes qui se sont rebellés contre moi. Car leur ver ne mourra pas, et leur feu ne s'éteindra pas<!--Mc. 9:48.-->, et ils deviendront une aversion pour toute chair.
